<?php
/*********************************************************************************************/
//																							 //
//                              CMS Vadyus v1.7   (c) 2010 FREEWARE                          //
//	 					  http://www.CMS Vadyus.ru/, info@CMS Vadyus.ru                      //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2007-2010                        //
//                                                                                           //
/*********************************************************************************************/

	function mod_referal_top($module_id){
        $inCore = cmsCore::getInstance();
        $inDB = cmsDatabase::getInstance();
		
		$cfg = $inCore->loadModuleConfig($module_id);
		
		$sql = "SELECT u.id, u.login, u.nickname, u.is_deleted, p.user_id, p.count_ref as count_ref 
				FROM cms_users u
				LEFT JOIN cms_preferal_count p ON p.user_id = u.id
				WHERE u.is_deleted = 0 AND u.is_locked = 0 AND count_ref >=1
				ORDER BY count_ref DESC
				LIMIT ".$cfg['count'];		
		$result = $inDB->query($sql);
		
		$users = array();
		$is_usr = false;
		
		if ($inDB->num_rows($result)){
			
				$is_usr=true;
				
				if (!function_exists('usrImageNOdb')){ //if not included earlier
				include_once($_SERVER['DOCUMENT_ROOT'].'/components/users/includes/usercore.php');
				}
			
				while($usr = $inDB->fetch_assoc($result)){
					$usr['profileurl'] = cmsUser::getProfileURL($usr['login']);
					
					$users[] = $usr;
							}
				}
		
		$smarty = $inCore->initSmarty('modules', 'mod_referal_top.tpl');			
		$smarty->assign('users', $users);
		$smarty->assign('cfg', $cfg);
		$smarty->assign('is_usr', $is_usr);
		$smarty->display('mod_referal_top.tpl');
				
		return true;
	
	}
?>
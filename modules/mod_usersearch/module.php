<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function mod_usersearch($module_id, $cfg){

    cmsCore::loadLanguage('components/users');

    cmsPage::initTemplate('modules', 'mod_usersearch')->
            assign('cfg', $cfg)->
            display('mod_usersearch.tpl');

    return true;

}
?>
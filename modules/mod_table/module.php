<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function mod_table($module_id, $cfg){

    $inCore      = cmsCore::getInstance();
    $inUser      = cmsUser::getInstance();
    $menuid      = $inCore->menuId();
    $full_menu   = $inCore->getMenuStruct();
	$inDB   = cmsDatabase::getInstance();

    if (!isset($cfg['menu'])) { $menu = 'mainmenu'; } else { $menu = $cfg['menu']; }
    if (!isset($cfg['show_home'])) { $cfg['show_home'] = 1; }
    if (!isset($cfg['is_sub_menu'])) { $cfg['is_sub_menu'] = 0; }

    // текущий пункт меню
    $currentmenu = $full_menu[$menuid];

    // результирующий массив меню
    $items = array();

    // id корня меню если обычный вывод меню, $menuid если режим подменю
    if($cfg['is_sub_menu']){

        // в подменю не должно быть ссылки на главную
        $cfg['show_home'] = 0;
        // на главной подменю не можт быть
        if($menuid == 1){
            return false;
        }
        foreach ($full_menu as $item) {
            if($item['NSLeft'] > $currentmenu['NSLeft'] &&
                    $item['NSRight'] < $currentmenu['NSRight'] &&
                    $item['menu'] == $menu &&
                    cmsCore::checkContentAccess($item['access_list']) && $item['published']){
                $items[] = $item;
                // массивы для сортировки
                $nsl[] = $item['NSLeft'];
                $ord[] = $item['ordering'];
            }
        }

    } else {

        foreach ($full_menu as $item) {
            if($item['menu'] == $menu &&
                    cmsCore::checkContentAccess($item['access_list']) && $item['published']){
                $items[] = $item;
                // массивы для сортировки
                $nsl[] = $item['NSLeft'];
                $ord[] = $item['ordering'];
            }
        }
    }

    if(!$items) { return false; }

    // сортируем массив
    array_multisort($nsl, SORT_ASC, $ord, SORT_ASC, $items);
	
    $template = ($cfg['tpl'] ? $cfg['tpl'] : 'mod_table.tpl');
	
	$inDB   = cmsDatabase::getInstance();
	$table_application = $inDB->get_table_desc_id('cms_user_requests_for_payment','status="0"');
	$inf_applications = array();
	for($i=0;$i<10;$i++){
		if(isset($table_application[$i])){
			$inf['id'] = $table_application[$i]['id'];
			if($table_application[$i]['name_system'] != '0'){
				$inf['name_system'] = $table_application[$i]['name_system'];
			}
			else{
				$inf['name_system'] = $inDB->get_field('cms_user_type_payment','id="'.$table_application[$i]['id_paym_syst'].'"','name_sustem');
			}
			$inf['money_off'] = $table_application[$i]['money_off'];
			if($table_application[$i]['garant'] == 1){
				$inf['obmen'] = 'Гарант';
			}
			elseif($table_application[$i]['garant'] == 2){
				$inf['obmen'] = 'ВВ';
			}
			elseif($table_application[$i]['garant'] == 3){
				$inf['obmen'] = 'ГФ';
			}
			/*
			$masivs = unserialize($table_application[$i]['masive_inf']);
			
			foreach($masivs as $result2){
				$lllk['id_sys_give'] = $result2['sustem_id'];
				if(isset($result2['sustem_name']) && isset($result2['sustem_name']) != '0'){
					$lllk['name_sys_give'] = $result2['sustem_name'];
				}else{
					$lllk['name_sys_give'] = $inDB->get_field("cms_user_type_payment","id='".$result2['sustem_id']."'","name_sustem");
				}
				$lllk['money'] = $result2['money'];
				$lllk['kurs'] = str_replace(',', '.', round($result2['kurs'], 3));
				$perem118 = $table_application[$i]['money_off']/$result2['money'];
				$perem118 = str_replace(',', '.', $perem118);
				$lllk['kurs_obr'] = str_replace(',', '.', round($perem118, 3));
				$massiv[] = $lllk;
				unset($perem118);
			}
			$inf['masiv'] = $massiv;*/
			$inf_applications[] = $inf;
		}
	}
	//var_dump($inf_applications);
	//echo '1';
    cmsPage::initTemplate('modules', 'mod_table.tpl')->
            assign('menuid', $menuid)->
			assign('inf_applications', $inf_applications)->
            assign('currentmenu', $currentmenu)->
            assign('menu', $menu)->
            assign('items', $items)->
            assign('last_level', 0)->
            assign('user_id', $inUser->id)->
            assign('is_admin', $inUser->is_admin)->
            assign('cfg', $cfg)->
			display('mod_table.tpl');

    return true;

}

?>
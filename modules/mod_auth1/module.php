<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function mod_auth1($module_id, $cfg){

    $inUser = cmsUser::getInstance();
	
    if ($inUser->id){ return false; }
	
    cmsUser::sessionPut('auth_back_url', cmsCore::getBackURL());
	
    cmsPage::initTemplate('modules', 'mod_auth1')->
	
            assign('cfg', $cfg)->
			
            display('mod_auth1.tpl');

			return true;
}
?>
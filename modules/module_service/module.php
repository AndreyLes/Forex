<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function module_service(){

    $inCore      = cmsCore::getInstance();
    $inUser      = cmsUser::getInstance();
    $menuid      = $inCore->menuId();
    $full_menu   = $inCore->getMenuStruct();
	$inDB   = cmsDatabase::getInstance();

    $payment_table = $inDB->get_table('cms_user_type_payment','on_off="1" AND status="0"');
	$payments_servise = array();
	foreach($payment_table as $result){
		$payment['name_system'] = $result['name_sustem'];
		$payment['payment_admin'] = $result['payment_admin'];
		$payment['tax_money'] = $result['tax_money'] + 0.5;
		$payments_servise[] = $payment;
	}
	//var_dump($payments_servise);
    $template = 'module_service.tpl';

    cmsPage::initTemplate('modules', $template)->
            assign('is_admin', $inUser->is_admin)->
			assign('payments_servise', $payments_servise)->
            assign('cfg', $cfg)->display($template);

    return true;

}

?>
<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/
if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

function content(){

    $inCore = cmsCore::getInstance();
    $inPage = cmsPage::getInstance();
    $inDB   = cmsDatabase::getInstance();
    $inUser = cmsUser::getInstance();

    $model = new cms_model_content();

    define('IS_BILLING', $inCore->isComponentInstalled('billing'));
    if (IS_BILLING) { cmsCore::loadClass('billing'); }

    global $_LANG;

    $id = cmsCore::request('id', 'int', 0);
    $do = $inCore->do;

    $seolink = cmsCore::strClear(urldecode(cmsCore::request('seolink', 'html', '')));

	$page = cmsCore::request('page', 'int', 1);

///////////////////////////////////// VIEW CATEGORY ////////////////////////////////////////////////////////////////////////////////
if ($do=='view'){
	
	$cat = $inDB->getNsCategory('cms_category', $seolink);

	// если не найдена категория и мы не на главной, 404
    if (!$cat && $inCore->menuId() !== 1) { cmsCore::error404(); }

	// Плагины
	$cat = cmsCore::callEvent('GET_CONTENT_CAT', $cat);

	// Неопубликованные показываем только админам
	if (!$cat['published'] && !$inUser->is_admin) { cmsCore::error404(); }

	// Проверяем доступ к категории
	if(!$inCore->checkUserAccess('category', $cat['id']) ){

		cmsCore::addSessionMessage($_LANG['NO_PERM_FOR_VIEW_TEXT'].'<br>'.$_LANG['NO_PERM_FOR_VIEW_RULES'], 'error');
		cmsCore::redirect('/content');

	}

	// если не корень категорий
	if($cat['NSLevel'] > 0){
		$inPage->setTitle($cat['title']);
		$pagetitle = $cat['title'];
		$showdate  = $cat['showdate'];
		$showcomm  = $cat['showcomm'];
		$inPage->addHead('<link rel="alternate" type="application/rss+xml" title="'.htmlspecialchars($cat['title']).'" href="'.HOST.'/rss/content/'.$cat['id'].'/feed.rss">');
	}

	// Если корневая категория
    if ($cat['NSLevel'] == 0){
		$inPage->setTitle($_LANG['CATALOG_ARTICLES']);
		$pagetitle = $_LANG['CATALOG_ARTICLES'];
		$showdate  = 1;
		$showcomm  = 1;
	}

	// Получаем дерево категорий
    $path_list = $inDB->getNsCategoryPath('cms_category', $cat['NSLeft'], $cat['NSRight'], 'id, title, NSLevel, seolink, url');

    if ($path_list){

        foreach($path_list as $pcat){

			if(!$inCore->checkUserAccess('category', $pcat['id'])){
				cmsCore::addSessionMessage($_LANG['NO_PERM_FOR_VIEW_TEXT'].'<br>'.$_LANG['NO_PERM_FOR_VIEW_RULES'], 'error');
				cmsCore::redirect('/content');
			}

            $inPage->addPathway($pcat['title'], $model->getCategoryURL(null, $pcat['seolink']));

        }

    }

	// Получаем подкатегории
    $subcats_list = $model->getSubCats($cat['id']);

	// Привязанный фотоальбом
	$cat_photos = $model->getCatPhotoAlbum($cat['photoalbum']);

	// Получаем статьи
	// Редактор/администратор
	$is_editor = (($cat['modgrp_id'] == $inUser->group_id  && cmsUser::isUserCan('content/autoadd')) || $inUser->is_admin);

	// Условия
	$model->whereCatIs($cat['id']);

	// Общее количество статей
	$total = $model->getArticlesCount($is_editor);

	// Сортировка и разбивка на страницы
    $inDB->orderBy($cat['orderby'], $cat['orderto']);
    $inDB->limitPage($page, $model->config['perpage']);

	// Получаем статьи
    $content_list = $total ?
					$model->getArticlesList(!$is_editor) :
					array(); $inDB->resetConditions();

    $pagebar  = cmsPage::getPagebar($total, $page, $model->config['perpage'], $model->getCategoryURL(null, $cat['seolink'], 0, true));

	$template = ($cat['tpl'] ? $cat['tpl'] : 'com_content_view.tpl');
	
	cmsPage::initTemplate('components', $template)->
            assign('cat', $cat)->
            assign('is_homepage', (bool)($inCore->menuId()==1))->
            assign('showdate', $showdate)->
            assign('showcomm', $showcomm)->
            assign('pagetitle', $pagetitle)->
            assign('subcats', $subcats_list)->
            assign('cat_photos', $cat_photos)->
            assign('articles', $content_list)->
            assign('pagebar', $pagebar)->
            display($template);

}

///////////////////////////////////// READ ARTICLE ////////////////////////////////////////////////////////////////////////////////
if ($do=='read'){
	
	$opt = cmsCore::request('opt', 'str');
	
	$findsql = "SELECT * FROM cms_content WHERE id = '42' AND published = 1";
	$result1 = $inDB->query($findsql) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
		{
			$item1['url'] = 	$model->getArticleURL(null, $item1['seolink']);
			$content_list[] = $item1;
		}
			
	// Получаем статью
	if($seolink != "info/design-standart"){
		$article = $model->getArticle($seolink);
		if (!$article) { cmsCore::error404(); }


		$article = cmsCore::callEvent('GET_ARTICLE', $article);

		if ($inUser->id) {
			$is_admin      = $inUser->is_admin;
			$is_author     = $inUser->id == $article['user_id'];
			$is_author_del = cmsUser::isUserCan('content/delete');
			$is_editor     = ($article['modgrp_id'] == $inUser->group_id && cmsUser::isUserCan('content/autoadd'));
		}

		// если статья не опубликована или дата публикации позже, 404
		if ((!$article['published'] || strtotime($article['pubdate']) > time()) && !$is_admin && !$is_editor && !$is_author) { cmsCore::error404(); }
	}
	if(!$inCore->checkUserAccess('material', $article['id'])){
		cmsCore::addSessionMessage($_LANG['NO_PERM_FOR_VIEW_TEXT'].'<br>'.$_LANG['NO_PERM_FOR_VIEW_RULES'], 'error');
		cmsCore::redirect($model->getCategoryURL(null, $article['catseolink']));
	}

	// увеличиваем кол-во просмотров
	if(@!$is_author){
	    $inDB->setFlag('cms_content', $article['id'], 'hits', $article['hits']+1);
	}

	// Картинка статьи
    $article['image'] = (file_exists(PATH.'/images/photos/medium/article'.$article['id'].'.jpg') ? 'article'.$article['id'].'.jpg' : '');
	// Заголовок страницы
	$article['pagetitle'] = $article['pagetitle'] ? $article['pagetitle'] : $article['title'];
    // Тело статьи в зависимости от настроек
    $article['content'] = $model->config['readdesc'] ? $article['description'].$article['content'] : $article['content'];
	// Дата публикации
	$article['pubdate'] = cmsCore::dateformat($article['pubdate']);
	// Шаблон статьи
	//var_dump($article['tpl']);
	$article['tpl'] = $article['tpl'] ? $article['tpl'] : 'com_content_read.tpl';
	
	if($seolink == "info/design-standart" || $article[id] == 53 ){
		
		$article['tpl'] = 'com_content_design_stndart.tpl';
		$id_application = cmsCore::request('id_application', 'str');
		

		
if($_GET['id_appl'] || $id_application){
	$id_application = $_GET['id_appl'] ? $_GET['id_appl'] : $id_application;
	$inform_application = $inDB->get_fields("cms_user_exchange_service","id='$id_application'","*");
	$status_application['status'] = $inform_application['status'];
	$status_application['name_syst'] = $inDB->get_field("cms_user_type_payment","id='".$inform_application['system_plus']."'","name_sustem");
	$status_application['money'] =  $inform_application['money_plus'];
	$status_application['score_admin'] = $inDB->get_field("cms_user_score","user_id='1' AND type_payment_id='".$inform_application['system_plus']."'","score_num");
	
}
		
if ($opt == "design_standart"){
	$money_user = cmsCore::request('payment_user', 'str');
	$paym_sys_user = cmsCore::request('payment_system_user', 'str');
	$paym_sys_admin = cmsCore::request('payment_system', 'array');
	$user_emaill = cmsCore::request('user_email', 'str');
	$user_name = cmsCore::request('user_name', 'str');
	$user_phone = cmsCore::request('user_phone', 'str');
	$your_phone_code = cmsCore::request('your_phone_code', 'str');
	
	$score_user = cmsCore::request("score_user", "str");
	$score = cmsCore::request('score', 'array');
	
	$relations = $inDB->get_field("cms_user_type_payment","id='$paym_sys_user'","relations");
	$relations = "$relations";
	$lkl = 0;
	
	$opopo = 0;
	foreach($paym_sys_admin as $key=>$resylt){
		$money_baza = $inDB->get_field('cms_user_relation_pc', "system='$paym_sys_user' AND relation_syst='$key'", 'min_money');
		$kurs_sust_1 = $inDB->get_field("cms_user_relation_pc","system='$paym_sys_user' AND relation_syst='$key'","kurs");
		
		$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
		if($money_baza && $money_baza >= $money_admin){
			$opopo = 1;
		}
	}
	
	if($opopo == 1){
		cmsCore::redirect('/info/o-nas.html#min_money');
	}
	
	if($relations && $relations != ""){
		foreach($paym_sys_admin as $key => $resylt){
			$number = ",$key,";
			if(strpos($relations, $number) === FALSE){
				$lkl = 1;
			}
			unset($number);
		}
	}
	
	if($lkl == 0){
	$inf_application['email'] = $user_emaill;
	$inf_application['name'] = $user_name;
	$inf_application['phone'] = $your_phone_code.$user_phone;
	$inf_application['mobtelephone'] = $user_phone;
	$inf_application['code_phone'] = $your_phone_code;
	
	$inf_application['money_user'] = $money_user;
	$inf_application['id_syst'] = $paym_sys_user;
	$inf_application['img'] = $inDB->get_field("cms_user_type_payment","id='$paym_sys_user'","min_img");
	$inf_application['name_system'] = $inDB->get_field("cms_user_type_payment","id='$paym_sys_user'","name_sustem");
	$inf_application['score'] = $score_user;
	foreach($paym_sys_admin as $key => $resylt){

		$commiss = $inDB->get_field('cms_user_relation_pc',"system='$paym_sys_user' AND relation_syst='$key'",'commission');
		
		$kurs_sust_1 = $inDB->get_field("cms_user_relation_pc","system='$paym_sys_user' AND relation_syst='$key'","kurs");
		
		$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
		
		$limit_money = $inDB->get_field("cms_user_type_payment", "id='$key'", "payment_admin");
		if($money_admin > $limit_money){
			cmsCore::redirect('/info/o-nas.html#limit');
		}
		
		$asdasdcc = str_replace(",", ".", $money_admin);
		$money_admin = str_replace(',', '.', round($asdasdcc, 2));
		
		$kurs_system = $kurs_sust_1;
		$all_kurs = $kurs_system + $commiss;
		$all_summ = $money_admin - $money_admin*$commiss*0.01;
		
		$asdasdcc = str_replace(",", ".", $all_summ);
		$all_summ = str_replace(',', '.', round($asdasdcc, 2));
		
		$infa['system_minus'] = $key;
		$infa['score'] = $score[$key];
		$infa['img'] = $inDB->get_field("cms_user_type_payment","id='$key'","min_img");
		$infa['all_summ'] = $all_summ;
		$infa['money_admin'] = $money_admin;
		$infa['name_system'] = $inDB->get_field("cms_user_type_payment","id='$key'","name_sustem");
		$informations[$key] = $infa;
	}
	$inf_application['masive'] = $informations;
	unset($informations);
	}
	else{
	cmsCore::redirect('/info/o-nas.html#critical_error');
	}
}	
	
if ($opt == "registration"){
	
	
	$money_user = cmsCore::request('payment_user', 'str');
	//$money_admin = cmsCore::request('payment_system', 'array');
	$paym_sys_user = cmsCore::request('payment_system_user', 'str');
	$paym_sys_admin = cmsCore::request('payment_system', 'array');
	$user_emaill = cmsCore::request('user_email', 'str');
	$user_name = cmsCore::request('user_name', 'str');
	$user_phone = cmsCore::request('user_phone', 'str');
	$your_phone_code = cmsCore::request('your_phone_code', 'str');
	
	$score_user = cmsCore::request("score_user", "str");
	$score = cmsCore::request('score', 'array');
	
	//$all_summ = cmsCore::request('all_money', 'str');
	//$kurs_system = cmsCore::request('kyrs', 'array');
	//$all_kurs = cmsCore::request('commission', 'str');
	
	$relations = $inDB->get_field("cms_user_type_payment","id='$paym_sys_user'","relations");
	$relations = "$relations";
	$lkl = 0;
	if($relations && $relations != ""){
		foreach($paym_sys_admin as $key => $resylt){
			$number = ",$key,";
			if(strpos($relations, $number) === FALSE){
				$lkl = 1;
			}
			unset($number);
		}
	}
	//echo $lkl;
	if($lkl == 0){
	$red_appl = 0;
	foreach($paym_sys_admin as $key => $resylt){
		if( $resylt > $money_user ){
		$red_appl = 1;
	}
	$commiss = $inDB->get_field('cms_user_relation_pc',"system='$paym_sys_user' AND relation_syst='$key'",'commission');
	
	$kurs_sust_1 = $inDB->get_field("cms_user_relation_pc","system='$paym_sys_user' AND relation_syst='$key'","kurs");
	
	$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
	
	$asdasdcc = str_replace(",", ".", $money_admin);
	$money_admin = str_replace(',', '.', round($asdasdcc, 2));
	
	$limit_money = $inDB->get_field("cms_user_type_payment", "id='$key'", "payment_admin");
	if($money_admin > $limit_money){
		cmsCore::redirect('/info/o-nas.html#limit');
	}
	
	$kurs_system = $kurs_sust_1;
	$all_kurs = $kurs_system + $commiss;
	$all_summ = $money_admin - $money_admin*$commiss*0.01;
	
	$asdasdcc = str_replace(",", ".", $all_summ);
	$all_summ = str_replace(',', '.', round($asdasdcc, 2));
	
	$infa['system_minus'] = $key;
	$infa['all_summ'] = $all_summ;
	$infa['money_admin'] = $money_admin;
	$infa['kurs_system'] = $kurs_system;
	$infa['all_kurs'] = $all_kurs;
	$informations[$key] = $infa;
	
	$paym_syst_name2 = $inDB->get_field("cms_user_type_payment", "id='$key'", "name_sustem");
	
	$message_inf_admin .= ' <li>
								<ul>
									<li>исходящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score[$key].';</li>
								</ul>
							</li>' ;
	$message_inf_user .= '  <li>
								<ul>
									<li>входящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score[$key].';</li>
								</ul>
							</li>' ;
	$message_mail_user .= ' <li>
								<ul>
									<li>входящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score[$key].';</li>
									<li>общая комиссия(%): '.$all_kurs.';</li>
								</ul>
							</li>';
	$message_mail_admin .= ' <li>
								<ul>
									<li>исходящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score[$key].';</li>
									<li>комиссия системы(%): '.$kurs_system.';</li>
									<li>общий курс: '.$all_kurs.';</li>
									<li>средства с комиссией системы(для проверки): '.$money_admin.';</li>
								</ul>
							</li>';
	
	}
	
//--------------------------------- временная регистрация пользователя ------------------------------------	
	
	$ppppq =  strpos($user_emaill, '@');
	$logins = substr($user_emaill,0, $ppppq);
	$arr_1 = array(".", ",");
	$pass = str_replace($arr_1, "_", $logins).rand(101, 9999);
	$arr_2 = array(".", ",", "_", "%", "(",")","[","]");
	$eeeew = str_replace($arr_2, ".", $logins);
	if(stristr($eeeew, '.')){$ppppq =  strpos($eeeew, '.');}
	$login = substr($user_emaill,0, $ppppq);
	$login = substr($login,0, 10)."_".rand(1001, 9999);
	if(!$login){$login = "login_".rand(100001, 999999);}
	//var_dump($login);
	$item['login'] = $login;
    $item['email'] = $user_emaill;
	$item['user_phone'] = $user_phone;
	$item['your_phone_code'] = $your_phone_code; 
    $item['icq']   = '';
    $item['city']  = '';
    $item['nickname']  = $user_name;
    $item['realname1'] = '';
    $item['realname2'] = '';
//var_dump($item);
	//////////////////////////////////////////////
    //////////// РЕГИСТРАЦИЯ /////////////////////
    //////////////////////////////////////////////
	if(!$inUser->id){
    $item['is_locked'] = '1';
    $item['password']  = md5($pass);
    $item['orig_password'] = $pass;
    $item['group_id']  = '1';
    $item['regdate']   = date('Y-m-d H:i:s');
    $item['logdate']   = date('Y-m-d H:i:s');
	
    if (cmsUser::sessionGet('invite_code')){

        $invite_code = cmsUser::sessionGet('invite_code');
        $item['invited_by'] = (int)$users_model->getInviteOwner($invite_code);

        if ($item['invited_by']){ $users_model->closeInvite($invite_code); }

        cmsUser::sessionDel('invite_code');

    } else {
        $item['invited_by'] = 0;
    }

    $item = cmsCore::callEvent('USER_BEFORE_REGISTER', $item);
	
   $item['id'] = $item['user_id'] = $inDB->insert('cms_users', $item);
    if(!$item['id']){ cmsCore::error404(); }

    $inDB->insert('cms_user_profiles', $item);
	//добавляємо рахунки для користувача && створюємо пусті платіжні системи для нового користувача
	
	$payment_system = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	foreach($payment_system as $key=>$infa){
		$inDB->query("INSERT cms_user_score (user_id, type_payment_id, score_num ,score_val, score_in, score_out, num_payment, status) VALUES ('".$item['id']."','".$infa['id']."','0','0','0','0','0','1')");
	}
	
	$inDB->addUserWallet($item['login']);
    cmsCore::callEvent('USER_REGISTER', $item);

    if ($item['is_locked']){

        $model->sendActivationNotice2($pass, $item['id']);
        //cmsPage::includeTemplateFile('special/regactivate.php');
        //cmsCore::halt();

    } else {

        cmsActions::log('add_user', array(
            'object' => '',
            'user_id' => $item['id'],
            'object_url' => '',
            'object_id' => $item['id'],
            'target' => '',
            'target_url' => '',
            'target_id' => 0,
            'description' => ''
        ));

        if ($model->config['send_greetmsg']){ $model->sendGreetsMessage($item['id']); }
        $model->sendRegistrationNotice($pass, $item['id']);

        $back_url = $inUser->signInUser($item['login'], $pass, true);

        //cmsCore::redirect($back_url);

    }
	
	
//----------------------------------------------------------------------------------------------------------
	foreach($score as $key => $result){
		$inDB->query("UPDATE cms_user_score SET score_num='".$result."' WHERE user_id='".$item['id']."' AND type_payment_id='$key'");
	}
	$inDB->query("UPDATE cms_user_score SET score_num='".$score_user."' WHERE user_id='".$item['id']."' AND type_payment_id='$paym_sys_user'");
	
	
	// потрібно взяти тимчасове id нового створеного юзера	
	$id = $item['id'];
	}
	else{ $id = $inUser->id; }
	//var_dump($informations);
	
	$infa_s = serialize($informations);
	
	$time = time();
	
	$sql113 = $inDB->query("INSERT cms_user_exchange_service (user_id, money_plus, system_plus ,infa, user_name, user_email, status, date) VALUES ('$id','$money_user','$paym_sys_user','$infa_s','$user_name','$user_emaill','1','$time')");
	$id_zapusa = $inDB->get_last_id('cms_user_exchange_service');
	$paym_syst_name1 = $inDB->get_field("cms_user_type_payment", "id='$paym_sys_user'", "name_sustem");
	
	//var_dump($user_name." ; ".$user_emaill);
	if($red_appl == 1){
		cmsUser::sendMessage(USER_MASSMAIL, '1', "<p>Подозрительная заявка <b>№ ".$id_zapusa."</b>, обмен через сервис. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a class='podozr_appl' href='/users/1/administration.html#service'>Посмотреть заявку</a></p>");
		
		$inf_mail3 = array();
		$inf_mail3['email'] = $admin_email;
		$inf_mail3['message'] = "<p>Подозрительная заявка <b>№ ".$id_zapusa."</b>, обмен через сервис. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a href='http://".$_SERVER['HTTP_HOST']."/users/1/administration.html#service'>Посмотреть заявку</a></p>";
		$email3 = $model->sendEmail3($inf_mail3, '');
	}
	
	cmsUser::sendMessage(USER_MASSMAIL, '1', "<p><b>Заявка № $id_zapusa на обмен средств через сервис:</b></p>
<ul> <li>входящая сумма : $money_user $;</li> <li>кошелек - $paym_syst_name1;</li><li>номер кошелька - $score_user;</li></ul>
<b>ПС:</b><br><ul>
$message_inf_admin</ul>
<b>от:</b> $user_name; E-mail: $user_emaill;");
	
	$config = cmsConfig::getDefaultConfig(); 
	$admin_email = $config['sitemail'];
	//var_dump($config['sitemail']);
	
	$user_email = $user_emaill;
	
	
	
	$inf_mail = array();
	$inf_mail['email'] = $user_email;
	$inf_mail['message'] = "<p><b>Заявка № $id_zapusa на обмен средств через сервис:</b></p>
		<ul> <li>исходящяя сумма : $money_user $;</li> <li>система оплаты - $paym_syst_name1;</li><li>номер кошелька - $score_user;</li></ul>
		<b>Администратор выберет одну из ПС:</b><br><ul>$message_mail_user </ul>
		<p>Просмотреть статус заявки можна здесь:
		<a href='".$_SERVER['HTTP_HOST']."/info/design-standart.html?id_appl=$id_zapusa'>ПОСМОТРЕТЬ СТАТУС</a></p>
		<p><a>ccылка на ПС(в разработке)</a></p>";
		
	$email = $model->sendEmail3($inf_mail);
	
	$inf_mail2 = array();
	$inf_mail2['email'] = $admin_email;
	$inf_mail2['message'] = "<p><b>Заявка № $id_zapusa на обмен средств через сервис:</b></p>
		<ul> <li>входящая сумма : $money_user $;</li> <li>система оплаты - $paym_syst_name1;</li></ul>
		<b>ПС:</b><br><ul>$message_mail_admin</ul>
		<b>от:</b> $user_name; E-mail: $user_emaill;";
		
	$email2 = $model->sendEmail3($inf_mail2);
	
	$login = $inDB->get_field("cms_users", "id='$id'", "login");
	
	$fff = $model->EventHistory($id, "Обмен через сервис");
	
	//echo $fff;
		cmsCore::redirect('/info/design-standart.html?id_appl='.$id_zapusa.'');
	}
	else{
		cmsCore::redirect('/info/o-nas.html#critical_error');
	}
}
		
	}
    $inPage->setTitle($article['pagetitle']);

	// Получаем дерево категорий
    $path_list = $article['showpath'] ?
						$inDB->getNsCategoryPath('cms_category', $article['leftkey'], $article['rightkey'], 'id, title, NSLevel, seolink, url') :
						array();

    if ($path_list){

        foreach($path_list as $pcat){

			if(!$inCore->checkUserAccess('category', $pcat['id'])){
				cmsCore::addSessionMessage($_LANG['NO_PERM_FOR_VIEW_TEXT'].'<br>'.$_LANG['NO_PERM_FOR_VIEW_RULES'], 'error');
				cmsCore::redirect('/content');
			}

            $inPage->addPathway($pcat['title'], $model->getCategoryURL(null, $pcat['seolink']));

        }

    }

    $inPage->addPathway($article['title']);

    // Мета теги KEYWORDS и DESCRIPTION
    if ($article['meta_keys']){
		$inPage->setKeywords($article['meta_keys']);
	} else {
        if (mb_strlen($article['content'])>30){
            $inPage->setKeywords(cmsCore::getKeywords(cmsCore::strClear($article['content'])));
        }
    }
    if (mb_strlen($article['meta_desc'])){
		$inPage->setDescription($article['meta_desc']);
	}

    // Выполняем фильтры
    $article['content'] = cmsCore::processFilters($article['content']);

	// Разбивка статей на страницы
    $pt_pages = array();
    if(!empty($GLOBALS['pt'])){
        foreach($GLOBALS['pt'] as $num=>$page_title){
            $pt_pages[$num]['title'] = $page_title;
            $pt_pages[$num]['url']   = $model->getArticleURL(null, $article['seolink'], $num+1);
        }
    }

	// Рейтинг статьи
    if($model->config['rating'] && $article['canrate']){

        $karma = cmsKarma('content', $article['id']);
		$karma_points = cmsKarmaFormatSmall($karma['points']);
        $btns = cmsKarmaButtonsText('content', $article['id'], $karma['points'], $is_author);

    }
	//$article['tpl'] = 'com_content_read.tpl';
	
	
	
	//$inPage->setTitle("Обмен через сервис");
	//$user_id=$model->returnId();
	
	
	$id = $inUser->id;
	if($id){
		$inf_user = $inDB->get_fields("cms_users","id='$id'","nickname, email, code_phone, mobtelephone");
		$infa_user['phone'] = $inf_user['mobtelephone'];
		$infa_user['nickname'] = $inf_user['nickname'];
		$infa_user['code_phone'] = $inf_user['code_phone'];
		$infa_user['email'] = $inf_user['email'];
	}
	$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	foreach($payment_sustem as $key => $payment_s){
		$payment['id'] = $payment_s['id'];
		$payment['name'] = $payment_s['name_sustem'];
		$payment['before_score'] = $payment_s['before_score'];
		$payment['number_score'] = $payment_s['number_score'];
		$payment['after_score'] = $payment_s['after_score'];
		$payment['img'] = $payment_s['min_img'];
		$payment['money_admin'] = $payment_s['payment_admin'];
		$payment['select'] = $payment_s['on_off'];
		$payment['relations'] = $payment_s['relations'];
		$payment['tax_money'] = $payment_s['tax_money'];
		if($id){
			$payment['score'] = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='".$payment_s['id']."'","score_num");
		}
		$payments[$payment['id']] = $payment;
	}
	
	/*$table_discount = $inDB->get_table('cms_user_discount','user_id="'.$id.'" OR user_id="0"');
	for($i=0;isset($table_discount[$i]);$i++){
		$discount['id'] = $i;
		$discount['discount'] = $table_discount[$i]['discount'];
		$discounts[] = $discount;
	}*/
	
	$kol_elements = count($payments);
	
	$table_inf_money = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	
	foreach($table_inf_money as $key => $inf_money_s){
		$inf_money['id'] = $inf_money_s['type_payment_id'];
		$inf_money['score_num'] = $inf_money_s['score_num'];
		$inf_money['before_score'] = $inf_money_s['before_score'];
		$inf_money['number_score'] = $inf_money_s['number_score'];
		$inf_money['after_score'] = $inf_money_s['after_score'];
		$inf_money['score_val'] = $inf_money_s['score_val'];
		$inf_money['score_in'] = $inf_money_s['score_in'];
		$inf_money['score_out'] = $inf_money_s['score_out'];
		$inf_money['num_payment'] = $inf_money_s['num_payment'];
		$inf_money['status'] = $inf_money_s['status'];
		$inf_moneys[$inf_money['id']] = $inf_money;
	}
	$inf_moneys = $payments;
	$table_inf_money2 = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	
	foreach($table_inf_money2 as $key => $inf_money_s2){
		$inf_money2['id'] = $inf_money_s2['type_payment_id'];
		$inf_money2['score_num'] = $inf_money_s2['score_num'];
		$inf_money2['score_val'] = $inf_money_s2['score_val'];
		$inf_money2['score_in'] = $inf_money_s2['score_in'];
		$inf_money2['score_out'] = $inf_money_s2['score_out'];
		$inf_money2['num_payment'] = $inf_money_s2['num_payment'];
		$inf_money2['status'] = $inf_money_s2['status'];
		$inf_moneys2[$inf_money2['id']] = $inf_money2;
	}
	$inf_moneys2 = $payments;
	
	//var_dump($inf_moneys2);
	$opt = cmsCore::request('opt', 'str');
	

	
	if($_GET){
		$sys_1 = $_GET['system1'];
		$sys_2 = $_GET['system2'];
	}
 
	$nik_user = $inDB->get_field("cms_users", "id='$id'", 'login');

/*============================================= Биржа обмена топ 10 ================================================*/
	$inUser = cmsUser::getInstance();
	
	
	$application_all = $inDB->get_table('cms_user_requests_for_payment','status="0" AND name_system="0"');
	$masive_appl = array();
	
	foreach($application_all as $key => $application){
		$inf_1['summ'] = $masive_appl[$application['id_paym_syst']]['summ'] + $application['money_off'];
		$inf_1['kol'] = $masive_appl[$application['id_paym_syst']]['kol'] + 1;
		$masive_appl[$application['id_paym_syst']] = $inf_1;
		unset($inf_1);
	}
	
	foreach($masive_appl as $aaa){
		$sort_mass[] = $aaa['summ'];
	}
	$coli4 = count($sort_mass);
	foreach($sort_mass as $key => $result1){
		for($j=0;$j<$coli4-1;$j++){
			if($sort_mass[$j] < $sort_mass[$j+1]){
				$a = $sort_mass[$j];
				$sort_mass[$j] = $sort_mass[$j+1];
				$sort_mass[$j+1] = $a;
			}
		}
	}
	
	$new_mass = array();
	foreach($sort_mass as $key=>$result){
		foreach($masive_appl as $key2=>$result2){
			if($result == $result2['summ']){
				$prov = 0;
				foreach($new_mass as $result3){ if($result3 == $key) $prov = 1; }
				if($prov == 0){
					$new_mass[] = $key;
					$last_masive[$key2] = $result2['summ'];
				}
			}
		}
	}
	/*------------ рахуються кошти всіх заявок по кожній ПС та кількість таких заявок --------------*/
	
	//var_dump($last_masive);
	
	
	
	if($inUser->id) $user_id = $inUser->id;
	$ssssaa = $inDB->get_table("cms_user_requests_for_payment", "status IN ('0', '2')");
	foreach($ssssaa as $ffffr){
		$khjsdfhkj[] = $ffffr['money_off'];
	}
	array_multisort($khjsdfhkj, SORT_NUMERIC, SORT_DESC);
	foreach($khjsdfhkj as $result){
		$rre = 0;
		
		foreach($ssssaa as $key => $ssssww){
			$gg = 0;
			if($rre == 0){
				foreach($table_request_peyment as $result2){
					if($result2['id'] == $ssssww['id']){
						$gg = 1;
					}
				}
				if($ssssww['money_off'] == $result && $rre == 0 && $gg == 0){
					$table_request_peyment[] = $ssssww;
					$rre = 1;
				}
			}
		}
	}
	
	$i=0;
	foreach($last_masive as $key=>$result){
		if($i < 10){
			$prov = 0;
			foreach($table_request_peyment as $result2){
				if($prov == 0){
					if($result2['id_paym_syst'] == $key){
						//echo $result2['id_paym_syst']." == ".$key."<br>";
						$table_request_peyment1[] = $result2;
						$prov = 1;
					}
				}
			}
			$i++;
		}
	}
	$number_mas = count($table_request_peyment1);
	if($number_mas < 10){
		foreach($table_request_peyment as $result){
			if($number_mas < 10){
				$prov = 0;
				foreach($table_request_peyment1 as $result2){
					if($result['id_paym_syst'] == $result2['id_paym_syst']){
						$prov = 1;
					}
				}
				if($prov == 0){
					$table_request_peyment1[] = $result;
					$number_mas++;
				}
			}
		}
	}
	
	//$table_request_peyment = $inDB->get_table("cms_user_requests_for_payment", "status IN ('0', '2')");
		$go_logins = '';
		if($_GET['id']) $go_logins = $_GET['id'];
		foreach($table_request_peyment1 as $key => $t_request_payment){
			$request_payment['id'] = $t_request_payment['id'];
			if($t_request_payment['id'] == $go_logins){
				$request_payment['go_logins'] = 1;
			}
			else{
				$request_payment['go_logins'] = 0;
			}
			$request_payment['garant'] = $t_request_payment['garant'];
			$request_payment['user_id'] = $t_request_payment['user_id'];
			$request_payment['login'] = $inDB->get_field("cms_users", "id='$user_id'", 'login');
			$users_infa = $inDB->get_fields("cms_users", "id='".$t_request_payment['user_id']."'", 'nickname,  regdate ');
			$users_infa['regdate'] = substr($users_infa['regdate'], 0, 10);
			$profile_infa = $inDB->get_fields("cms_user_profiles", "user_id='".$t_request_payment['user_id']."'", 'imageurl, odnokl, g_plus, vk, tviter, fasebook, mail');
			if($profile_infa['imageurl'] == ""){$profile_infa['imageurl'] = 'nopic.jpg';}
			
			$odnokl_cet = "";
			$g_plus_cet = "";
			$vk_cet = "";
			$tviter_cet = "";
			$fasebook_cet = "";
			$mail_cet = "";
			
			if($profile_infa['odnokl'] != ""){ $odnokl_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['g_plus'] != ""){ $g_plus_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['vk'] != ""){ $vk_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -149px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['tviter'] != ""){ $tviter_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -207px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['fasebook'] != ""){ $fasebook_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['mail'] != ""){ $mail_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -90px -127px; width:50px; height:50px; float:left;'></div>";}
			
			$user_name = $inDB->get_field("cms_users", "id='".$t_request_payment['user_id']."'", 'nickname');
			if($user_id == $t_request_payment['user_id']){
				$jjjjjj = "<a style='cursor:default; opacity:0.5;' class='button_svaz' >Связаться</a>";
			}
			else{
				$jjjjjj = "<a href='javascript:void(0)' class='button_svaz' onclick='users.sendMess(".$t_request_payment['user_id'].", 0, this);return false;' title='Новое сообщение: ".$user_name."'>Связаться</a>";
			}
			
			$request_payment['div'] = "
			<img  style='float:left; margin-right: 15px;width: 55px; max-height: 55px;' src='/images/users/avatars/small/".$profile_infa['imageurl']."'>
			<div style='height: 52px;'>
			".$odnokl_cet.$g_plus_cet.$vk_cet.$tviter_cet.$fasebook_cet.$mail_cet."
			</div>
			<div style='text-align: left;margin-top: 6px;'>
				<div>".$users_infa['nickname']."</div>
				<div><span style='color: gray;'>ID пользователя: </span>".$t_request_payment['user_id']."</div>
				<div><span style='color: gray;'>Дата регистрации: </span>".$users_infa['regdate']."</div>
			</div>
			<div style='margin: auto; margin-top: 6px;'>
				".$jjjjjj."
			</div>";
			
			foreach($country_user as $key5 => $country){
				if($country['user_id'] == $t_request_payment['user_id']){
					$request_payment['country'] = $country['country'];
				}
			}
			$a = 0;
			foreach($payment_sustem as $key0 => $payment_s){
				if($t_request_payment['id_paym_syst'] == $payment_s['id']){
					$request_payment['name_user_system'] =	$payment_s['name_sustem'];
					$a = 1;
				}
			}
			if($a == 0){
				$request_payment['name_user_system'] = $inDB->get_field('cms_user_requests_for_payment','id="'.$t_request_payment['id'].'"','name_system');
			}
			$request_payment['money_off'] = $t_request_payment['money_off'];
			$masive_inf = $t_request_payment['masive_inf'];
			$peremennaya = unserialize($masive_inf);
			
			foreach($peremennaya as $key1 => $inform){ 
				foreach($payment_sustem as $key2 => $payment_s){
					if($inform['sustem_id'] == $payment_s['id']){
					$money_on['name_paym_system'] =	$payment_s['name_sustem'];
					}
				}
				$money_on['sustem_id'] = $inform['sustem_id'];
				$money_on['score'] = $inform['score'];
				$money_on['kurs'] = str_replace(',', '.', $inform['kurs']);
				$perem118 = $t_request_payment['money_off']/$inform['money'];
				$perem118 = str_replace(',', '.', $perem118);
				$money_on['kurs_obr'] = str_replace(',', '.', round($perem118, 3));
				$money_on['money'] = $inform['money'];
				$moneys_on[$key1] = $money_on;
				unset($perem118);
			}
			
			$request_payment['masive'] = $moneys_on;
			unset($moneys_on);
			$request_payment['sell_parts'] = $t_request_payment['sell_parts'];
			$request_payment['sms'] = $t_request_payment['sms'];
			
			if($t_request_payment['status'] == "2"){
				$request_payment['status'] = $t_request_payment['status'];
				$time_start = $inDB->get_field("cms_user_application_timer", "zakaz_id='".$request_payment['id']."'", 'time_start');
				$time_nuw = time();
				
				if(($time_nuw - $time_start) < 1800){
					$request_payment['time'] = 30 - floor(($time_nuw - $time_start)/60);
				}
				else{
					$sql115 = "UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$request_payment['id']."' AND status='2'";
					$inDB->query($sql115);
					
					$asdasdass = "DELETE FROM cms_user_application_timer WHERE zakaz_id='".$request_payment['id']."'";
					$inDB->query($dasdass);
					
					$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE (num_appl_2='".$request_payment['id']."' AND user_id_on='$user_id' AND status='0') OR (num_appl_1='".$request_payment['id']."' AND user_id_off='$user_id' AND status='0')");
					
					$id_usersss = $inDB->get_field("cms_user_requests_for_payment","id='".$request_payment['id']."'","user_id");
					
					$model->EventHistory($id_usersss, "Время заяки № ".$request_payment['id']." истекло");
					
					$change = 1;
				}
			}
			
			$request_payments[$key] = $request_payment;
			unset($request_payment['status']);
			unset($perem);
			
		}
		$table_relation_pc = $inDB->get_table('cms_user_relation_pc','status="1"','system');
		if($table_relation_pc){
			
			foreach($table_relation_pc as $result){
				$infa_id[] = $result['system'];
			}
			
			$perem112 = array_unique($infa_id);
			foreach($perem112 as $result){
				$inf_sys = $inDB->get_table('cms_user_relation_pc', "system='$result' AND status='1'");
				foreach($inf_sys as $result2){
					$infa['kurs'] = $result2['kurs'];
					$infa['commission'] = $result2['commission'];
					$infa['min_money'] = $result2['min_money'];
					$inf_relations[$result2['relation_syst']] = $infa;
					unset($relation);
				}
				$inf_relation[$result] = $inf_relations;
				unset($inf_relations);
			}
		}
		//var_dump($article);
/*=============================================================================================================*/
	if($inUser->id) $id_user = $inUser->id;
    cmsPage::initTemplate('components', $article['tpl'])->
            assign('article', $article)->
			assign('inf_relation', $inf_relation)->
			assign('status_application', $status_application)->
            assign('cfg', $model->config)->
            assign('page', $page)->
            assign('is_pages', !empty($GLOBALS['pt']))->
            assign('pt_pages', $pt_pages)->
            assign('inf_application', $inf_application)->
			assign('pretenzia', $content_list)->
            assign('is_admin', $is_admin)->
            assign('is_editor', $is_editor)->
            assign('is_author', $is_author)->
            assign('is_author_del', $is_author_del)->
            assign('tagbar', cmsTagBar('content', $article['id']))->
            assign('karma_points', @$karma_points)->
            assign('karma_votes', @$karma['votes'])->
            assign('karma_buttons', @$btns)->
			assign('request_payments', $request_payments)->
			assign('id_user', $id_user)->
			assign('payments', $payments)->
			assign('id', $id)->
			assign('nik_user', $nik_user)->
			assign('zayavka', $zayavka)->
			assign('sys_1', $sys_1)->
			assign('sys_2', $sys_2)->
			assign('infa_user', $infa_user)->
			assign('verification', $verification)->
			assign('payment_user', $payment_user)->
			assign('payment_system_id', $payment_system_id)->
			assign('vubor_garanta', $vubor_garanta)->
			assign('sell_parts', $sell_parts)->
			assign('information', $information)->
			assign('inf_moneys', $inf_moneys)->
			assign('server_name', $_SERVER[SERVER_NAME])->
			assign('inf_moneys2', $inf_moneys2)->
			assign('user_accounts', $getPamms)->
			assign('pammAccount', $getPammAccount)->
			assign('pammDefault', $getDefaultPammAccount)->
			assign('getPammCustomer', $getPammCustomer)->
			assign('userCash', $userCash)->
            display($article['tpl']);

    // Комментарии статьи
    if($article['published'] && $article['comments'] && $inCore->isComponentInstalled('comments')){
        cmsCore::includeComments();
        comments('article', $article['id']);
    }

}
///////////////////////////////////// ADD ARTICLE //////////////////////////////////////////////////////////////////////////////////
if ($do=='addarticle' || $do=='editarticle'){

	$is_add      = cmsUser::isUserCan('content/add');     // может добавлять статьи
	$is_auto_add = cmsUser::isUserCan('content/autoadd'); // добавлять статьи без модерации

	if (!$is_add && !$is_auto_add){ cmsCore::error404(); }

	// Для редактирования получаем статью и проверяем доступ
    if ($do=='editarticle'){

		// Получаем статью
		$item = $model->getArticle($id);
		if (!$item) { cmsCore::error404(); }

        $pubcats = array();

		// доступ к редактированию админам, авторам и редакторам
		if(!$inUser->is_admin &&
				($item['user_id'] != $inUser->id) &&
				!($item['modgrp_id'] == $inUser->group_id && cmsUser::isUserCan('content/autoadd'))){
			cmsCore::error404();
		}

    }

	// Для добавления проверяем не вводили ли мы данные ранее
	if ($do=='addarticle'){

		$item = cmsUser::sessionGet('article');
		if ($item) { cmsUser::sessionDel('article'); }

        // Категории, в которые разрешено публиковать
        $pubcats = $model->getPublicCats();
        if(!$pubcats) { cmsCore::addSessionMessage($_LANG['ADD_ARTICLE_ERR_CAT'], 'error'); cmsCore::redirectBack(); }

	}

	// не было запроса на сохранение, показываем форму
    if (!cmsCore::inRequest('add_mod')){

        $dynamic_cost = false;

		// Если добавляем статью
        if ($do=='addarticle'){

            $pagetitle = $_LANG['ADD_ARTICLE'];

            $inPage->setTitle($pagetitle);
			$inPage->addPathway($_LANG['USERS'], '/'.str_replace('/', '', cmsUser::PROFILE_LINK_PREFIX));
			$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
			$inPage->addPathway($_LANG['MY_ARTICLES'], '/content/my.html');
			$inPage->addPathway($pagetitle);

            // поддержка биллинга
            if (IS_BILLING){
                $action = cmsBilling::getAction('content', 'add_content');
                foreach($pubcats as $p=>$pubcat){
                    if ($pubcat['cost']){
                        $dynamic_cost = true;
                    } else {
                        $pubcats[$p]['cost'] = $action['point_cost'][$inUser->group_id];
                    }
                }
                cmsBilling::checkBalance('content', 'add_content', $dynamic_cost);
            }

        }

		// Если редактируем статью
        if ($do=='editarticle'){

            $pagetitle = $_LANG['EDIT_ARTICLE'];

            $inPage->setTitle($pagetitle);
			$inPage->addPathway($_LANG['USERS'], '/'.str_replace('/', '', cmsUser::PROFILE_LINK_PREFIX));
			if($item['user_id'] != $inUser->id){
				$user = $inDB->get_fields('cms_users', "id='{$item['user_id']}'", 'login, nickname');
        		$inPage->addPathway($user['nickname'], cmsUser::getProfileURL($user['login']));
			} else {
				$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
			}
			$inPage->addPathway($_LANG['MY_ARTICLES'], '/content/my.html');
            $inPage->addPathway($pagetitle);

            $item['tags']  = cmsTagLine('content', $item['id'], false);
            $item['image'] = (file_exists(PATH.'/images/photos/small/article'.$item['id'].'.jpg') ? 'article'.$item['id'].'.jpg' : '');

            if (!$is_auto_add){
				cmsCore::addSessionMessage($_LANG['ATTENTION'].': '.$_LANG['EDIT_ARTICLE_PREMODER'], 'info');
            }

        }

        $inPage->initAutocomplete();
        $autocomplete_js = $inPage->getAutocompleteJS('tagsearch', 'tags');

        $item = cmsCore::callEvent('PRE_EDIT_ARTICLE', (@$item ? $item : array()));

		cmsPage::initTemplate('components', 'com_content_edit')->
                assign('mod', $item)->
                assign('do', $do)->
                assign('cfg', $model->config)->
                assign('pubcats', $pubcats)->
                assign('pagetitle', $pagetitle)->
                assign('is_admin', $inUser->is_admin)->
                assign('is_billing', IS_BILLING)->
                assign('dynamic_cost', $dynamic_cost)->
                assign('autocomplete_js', $autocomplete_js)->
                display('com_content_edit.tpl');

    }

	// Пришел запрос на сохранение статьи
    if (cmsCore::inRequest('add_mod')){

        $errors = false;

        $article['category_id']  = cmsCore::request('category_id', 'int', 1);
        $article['user_id']      = $item['user_id'] ? $item['user_id'] : $inUser->id;
        $article['title']        = cmsCore::request('title', 'str', '');
        $article['tags']         = cmsCore::request('tags', 'str', '');

        $article['description']  = cmsCore::request('description', 'html', '');
        $article['content']      = cmsCore::request('content', 'html', '');
        $article['description']  = cmsCore::badTagClear($article['description']);
        $article['content']      = cmsCore::badTagClear($article['content']);

        $article['published']    = $is_auto_add ? 1 : 0;
        if ($do=='editarticle'){
           $article['published'] = ($item['published'] == 0) ? $item['published'] : $article['published'];
        }
        $article['pubdate']      = $do=='editarticle' ? $item['pubdate'] : date('Y-m-d H:i');
        $article['enddate']      = $do=='editarticle' ? $item['enddate'] : $article['pubdate'];
        $article['is_end']       = $do=='editarticle' ? $item['is_end'] : 0;
        $article['showtitle']    = $do=='editarticle' ? $item['showtitle'] : 1;

		$article['meta_desc']    = $do=='addarticle' ? mb_strtolower($article['title']) : $inDB->escape_string($item['meta_desc']);
		$article['meta_keys']    = $do=='addarticle' ? $inCore->getKeywords($article['content']) : $inDB->escape_string($item['meta_keys']);

        $article['showdate']     = $do=='editarticle' ? $item['showdate'] : 1;
        $article['showlatest']   = $do=='editarticle' ? $item['showlatest'] : 1;
        $article['showpath']     = $do=='editarticle' ? $item['showpath'] : 1;
        $article['comments']     = $do=='editarticle' ? $item['comments'] : 1;
        $article['canrate']      = $do=='editarticle' ? $item['canrate'] : 1;
        $article['pagetitle']    = '';
        if ($do=='editarticle'){
           $article['tpl']       = $item['tpl'];
        }

        if (mb_strlen($article['title'])<2){ cmsCore::addSessionMessage($_LANG['REQ_TITLE'], 'error'); $errors = true; }
        if (mb_strlen($article['content'])<10){ cmsCore::addSessionMessage($_LANG['REQ_CONTENT'], 'error'); $errors = true; }

		if($errors) {

			// При добавлении статьи при ошибках сохраняем введенные поля
			if ($do=='addarticle'){
				cmsUser::sessionPut('article', $article);
			}

			cmsCore::redirectBack();
		}

        $article['description']  = $inDB->escape_string($article['description']);
        $article['content']      = $inDB->escape_string($article['content']);

        $article = cmsCore::callEvent('AFTER_EDIT_ARTICLE', $article);

		// добавление статьи
        if ($do=='addarticle'){
            $article_id = $model->addArticle($article);
		}

		// загрузка фото
		$file = 'article'.(@$article_id ? $article_id : $item['id']).'.jpg';

		if (cmsCore::request('delete_image', 'int', 0)){
			@unlink(PATH."/images/photos/small/$file");
			@unlink(PATH."/images/photos/medium/$file");
		}

		// Загружаем класс загрузки фото
		cmsCore::loadClass('upload_photo');
		$inUploadPhoto = cmsUploadPhoto::getInstance();
		// Выставляем конфигурационные параметры
		$inUploadPhoto->upload_dir    = PATH.'/images/photos/';
		$inUploadPhoto->small_size_w  = $model->config['img_small_w'];
		$inUploadPhoto->medium_size_w = $model->config['img_big_w'];
		$inUploadPhoto->thumbsqr      = $model->config['img_sqr'];
		$inUploadPhoto->is_watermark  = $model->config['watermark'];
		$inUploadPhoto->input_name    = 'picture';
		$inUploadPhoto->filename      = $file;
		// Процесс загрузки фото
		$inUploadPhoto->uploadPhoto();

		// операции после добавления/редактирования статьи
		// добавление статьи
        if ($do=='addarticle'){

			// Получаем добавленную статью
            $article = $model->getArticle($article_id);

			if (!$article['published']){

				cmsCore::addSessionMessage($_LANG['ARTICLE_PREMODER_TEXT'], 'info');

				// отсылаем уведомление администраторам
				$link = '<a href="'.$model->getArticleURL(null, $article['seolink']).'">'.$article['title'].'</a>';
				$message = str_replace('%user%', cmsUser::getProfileLink($inUser->login, $inUser->nickname), $_LANG['MSG_ARTICLE_SUBMIT']);
				$message = str_replace('%link%', $link, $message);

                cmsUser::sendMessageToGroup(USER_UPDATER, cmsUser::getAdminGroups(), $message);

            } else {

                //регистрируем событие
                cmsActions::log('add_article', array(
                    'object' => $article['title'],
                    'object_url' =>  $model->getArticleURL(null, $article['seolink']),
                    'object_id' =>  $article['id'],
                    'target' => $article['cat_title'],
                    'target_url' => $model->getCategoryURL(null, $article['catseolink']),
                    'target_id' =>  $article['category_id'],
                    'description' => ''
                ));

                if (IS_BILLING){
                    $category_cost = $article['cost']==='' ? false : (int)$article['cost'];
                    cmsBilling::process('content', 'add_content', $category_cost);
                }

				cmsUser::checkAwards($inUser->id);

            }

			cmsCore::addSessionMessage($_LANG['ARTICLE_SAVE'], 'info');

            cmsCore::redirect('/my.html');

        }

		// Редактирование статьи
        if ($do=='editarticle'){

            $model->updateArticle($item['id'], $article, true);

			cmsActions::updateLog('add_article', array('object' => $article['title']), $item['id']);

			if (!$article['published']){

				$link = '<a href="'.$model->getArticleURL(null, $item['seolink']).'">'.$article['title'].'</a>';
				$message = str_replace('%user%', cmsUser::getProfileLink($inUser->login, $inUser->nickname), $_LANG['MSG_ARTICLE_EDITED']);
				$message = str_replace('%link%', $link, $message);

				cmsUser::sendMessageToGroup(USER_UPDATER, cmsUser::getAdminGroups(), $message);

			}

			$mess = $article['published'] ? $_LANG['ARTICLE_SAVE'] : $_LANG['ARTICLE_SAVE'].' '.$_LANG['ARTICLE_PREMODER_TEXT'];
			cmsCore::addSessionMessage($mess, 'info');

            cmsCore::redirect($model->getArticleURL(null, $item['seolink']));

        }

    }
}
///////////////////////// PUBLISH ARTICLE /////////////////////////////////////////////////////////////////////////////
if ($do == 'publisharticle'){

	if (!$inUser->id){ cmsCore::error404(); }

    $article = $model->getArticle($id);
	if (!$article) { cmsCore::error404(); }

    // Редактор с правами на добавление без модерации или администраторы могут публиковать
    if (!(($article['modgrp_id'] == $inUser->group_id) && cmsUser::isUserCan('content/autoadd')) && !$inUser->is_admin) { cmsCore::error404(); }

	$inDB->setFlag('cms_content', $article['id'], 'published', 1);

	cmsCore::callEvent('ADD_ARTICLE_DONE', $article);

    if (IS_BILLING){
        $author = $inDB->get_fields('cms_users', "id='{$article['user_id']}'", '*');
        $category_cost = $article['cost']==='' ? false : (int)$article['cost'];
        cmsBilling::process('content', 'add_content', $category_cost, $author);
    }

    //регистрируем событие
    cmsActions::log('add_article', array(
           'object' => $article['title'],
		   'user_id' => $article['user_id'],
           'object_url' =>  $model->getArticleURL(null, $article['seolink']),
           'object_id' =>  $article['id'],
           'target' => $article['cat_title'],
           'target_url' => $model->getCategoryURL(null, $article['catseolink']),
           'target_id' =>  $article['cat_id'],
           'description' => ''
    ));

	$link = '<a href="'.$model->getArticleURL(null, $article['seolink']).'">'.$article['title'].'</a>';
	$message = str_replace('%link%', $link, $_LANG['MSG_ARTICLE_ACCEPTED']);
    cmsUser::sendMessage(USER_UPDATER, $article['user_id'], $message);

	cmsUser::checkAwards($article['user_id']);

    cmsCore::redirectBack();

}
///////////////////////////////////// DELETE ARTICLE ///////////////////////////////////////////////////////////////////////////////////
if ($do=='deletearticle'){

	if (!$inUser->id){ cmsCore::error404(); }

    $article = $model->getArticle($id);
	if (!$article) { cmsCore::error404(); }

    // права доступа
	$is_author = cmsUser::isUserCan('content/delete') && ($article['user_id'] == $inUser->id);
	$is_editor = ($article['modgrp_id'] == $inUser->group_id) && cmsUser::isUserCan('content/autoadd');

    if (!$is_author && !$is_editor && !$inUser->is_admin) { cmsCore::error404(); }

	if (!cmsCore::inRequest('goadd')){

		$inPage->setTitle($_LANG['ARTICLE_REMOVAL']);
		$inPage->addPathway($_LANG['ARTICLE_REMOVAL']);

		$confirm['title']              = $_LANG['ARTICLE_REMOVAL'];
		$confirm['text']               = $_LANG['ARTICLE_REMOVAL_TEXT'].' <a href="'.$model->getArticleURL(null, $article['seolink']).'">'.$article['title'].'</a>?';
		$confirm['action']             = $_SERVER['REQUEST_URI'];
		$confirm['yes_button']         = array();
		$confirm['yes_button']['type'] = 'submit';
		$confirm['yes_button']['name'] = 'goadd';
		cmsPage::initTemplate('components', 'action_confirm')->
                assign('confirm', $confirm)->
                display('action_confirm.tpl');

	} else {

		$model->deleteArticle($article['id']);

		if ($_SERVER['HTTP_REFERER'] == '/my.html' ) {

			cmsCore::addSessionMessage($_LANG['ARTICLE_DELETED'], 'info');
			cmsCore::redirectBack();

		} else {

			// если удалили как администратор или редактор и мы не авторы статьи, отсылаем сообщение автору
			if (($is_editor || $inUser->is_admin) && $article['user_id'] != $inUser->id){

				$link = '<a href="'.$model->getArticleURL(null, $article['seolink']).'">'.$article['title'].'</a>';
				$message = str_replace('%link%', $link, ($article['published'] ? $_LANG['MSG_ARTICLE_DELETED'] : $_LANG['MSG_ARTICLE_REJECTED']));
				cmsUser::sendMessage(USER_UPDATER, $article['user_id'], $message);

			} else {
				cmsCore::addSessionMessage($_LANG['ARTICLE_DELETED'], 'info');
			}

        	cmsCore::redirect($model->getCategoryURL(null, $article['catseolink']));

		}

	}

}
///////////////////////////////////// MY ARTICLES ///////////////////////////////////////////////////////////////////////////////////
if ($do=='my'){

    if (!cmsUser::isUserCan('content/add')){ cmsCore::error404(); }

    $inPage->setTitle($_LANG['MY_ARTICLES']);
	$inPage->addPathway($_LANG['USERS'], '/'.str_replace('/', '', cmsUser::PROFILE_LINK_PREFIX));
	$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
    $inPage->addPathway($_LANG['MY_ARTICLES']);

	$perpage = 15;

	// Условия
	$model->whereUserIs($inUser->id);

	// Общее количество статей
	$total = $model->getArticlesCount(false);

	// Сортировка и разбивка на страницы
    $inDB->orderBy('con.pubdate', 'DESC');
    $inDB->limitPage($page, $perpage);

	// Получаем статьи
    $content_list = $total ?
					$model->getArticlesList(false) :
					array(); $inDB->resetConditions();

    cmsPage::initTemplate('components', 'com_content_my')->
            assign('articles', $content_list)->
            assign('total', $total)->
            assign('user_can_delete', cmsUser::isUserCan('content/delete'))->
            assign('pagebar', cmsPage::getPagebar($total, $page, $perpage, '/content/my%page%.html'))->
            display('com_content_my.tpl');

}
///////////////////////////////////// BEST ARTICLES ///////////////////////////////////////////////////////////////////////////////////
if ($do=='best'){

    $inPage->setTitle($_LANG['ARTICLES_RATING']);
    $inPage->addPathway($_LANG['ARTICLES_RATING']);

	// Только статьи, за которые можно голосовать
	$inDB->where("con.canrate = 1");

	// Сортировка и разбивка на страницы
    $inDB->orderBy('con.rating', 'DESC');
    $inDB->limitPage(1, 30);

	// Получаем статьи
    $content_list = $model->getArticlesList();

    cmsPage::initTemplate('components', 'com_content_rating')->
            assign('articles', $content_list)->
            display('com_content_rating.tpl');

}

} //function
?>
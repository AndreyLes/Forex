<?php

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

function pars_page($href) {

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $href);
    curl_setopt($curl, CURLOPT_REFERER, $href);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
    $str = curl_exec($curl);
    curl_close($curl);

    // Create a DOM object
    $dom = new simple_html_dom();
    // Load HTML from a string
    $dom->load($str);

    return $dom;
}

function users(){

    $inCore = cmsCore::getInstance();
    $inPage = cmsPage::getInstance();
    $inDB   = cmsDatabase::getInstance();
    $inUser = cmsUser::getInstance();
	
    global $_LANG;

    $model = new cms_model_users();

	// id пользователя
	$id = cmsCore::request('id', 'int', 0);
	// логин пользователя
	$login = cmsCore::strClear(urldecode(cmsCore::request('login', 'html', '')));

	$do   = $inCore->do;
	$page = cmsCore::request('page', 'int', 1);
	
	if(!$inUser->id){
		cmsCore::redirect('/');
	}
	$pagetitle = $inCore->getComponentTitle();

	$inPage->addPathway($pagetitle, '/users');
	$inPage->setTitle($pagetitle);
	$inPage->setDescription($pagetitle);

	// js только авторизованным
	if($inUser->id){
		$inPage->addHeadJS('components/users/js/profile.js');
        $inPage->addHeadJsLang(array('CONFIRM_CLEAN_CAT','CHOOSE_RECIPIENT','SEND_TO_USER','FRIENDSHIP_OFFER','STOP_FRIENDLY','REALY_STOP_FRIENDLY','ENTER_STATUS','HAVE_JUST'));
	}
	
if($do=='banners'){
	$user_id=$model->returnId();
	if(!empty($_FILES)){
		isset($_FILES['file']) ? $get=$model->addNewBanner($_FILES['file'], $user_id, $_POST['link']) : $get=$model->changeBanner($_FILES['file2'], $user_id, $_POST);

		cmsCore::redirect('/users/'.$user_id.'/banners.html#result');
	}
	
	$getImage=$model->getUserBanners($user_id);
	cmsPage::initTemplate('components', 'com_users_banners')->
			assign('getImage', $getImage)->
            display('com_users_banners.tpl');
}
	
if($do=='pamminfo'){
	
	$inPage->setTitle("Биржа ПАММ портфелей");
	$user_id=$model->returnId();
	
	if(isset($_POST['sellPamm'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		
		isset($_POST['price']) ? $model->sellToExchange($_POST) : $model->sellToService($_POST);
		
		if(isset($_POST['price']))
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successExchange');
		else
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successServise');
	}
	
	if(isset($_POST['count'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$user_id=$model->returnId();
		$model->buyAccount($_POST);
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successBuy');
	}
	
	if(isset($_POST['new-price'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$model->changeStatus($_POST);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successChange');
	}
	
	if(isset($_POST['remove-success'])){
		
		$model->removePamms($_POST['remove-success']);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successRemove');
	}
		
	$getPammAccount=$model->getPammAccount();
	$getPammCustomer=$model->getExchangePammNew();
	$getDefaultPammAccount=$model->getDefaultPammNew();
	
	$getPamms=$model->getPammAccounts($user_id);
	
	$userCash=$model->getCash();
	cmsPage::initTemplate('components', 'com_userss_pamminfo')->
			assign('user_accounts', $getPamms)->
			assign('pammAccount', $getPammAccount)->
			assign('pammDefault', $getDefaultPammAccount)->
			assign('getPammCustomer', $getPammCustomer)->
			assign('userCash', $userCash)->
            display('com_users_pamminfo.tpl');
}

//==============================================================================
//=========================пополнение гарантийного фонда==========================
//==============================================================================
if($do == "guarante_fond"){
	$inPage->setTitle("Гарантийный фонд");
	$user_id=$model->returnId();
	
	$table_user_score = $inDB->get_table("cms_user_score", "user_id='$user_id'");
	foreach($table_user_score as $result){
		$syst_status = $inDB->get_field("cms_user_type_payment","id='".$result['type_payment_id']."' AND status='0'","on_off");
		if(($result['score_val'] != '' && $syst_status == '1') || ($result['score_val'] != 0 && $syst_status == '1')){
			$inf_money['id'] = $result['type_payment_id'];
			$inf_money['name_syst'] = $inDB->get_field("cms_user_type_payment","id='".$result['type_payment_id']."'","name_sustem");
			$inf_money['img'] = $inDB->get_field("cms_user_type_payment","id='".$result['type_payment_id']."'","min_img");
			$inf_money['nalog'] =  $inDB->get_field("cms_user_type_payment","id='".$result['type_payment_id']."'","tax_money");
			$inf_money['money'] =  $result['score_val'];
			$inf_money_s[] = $inf_money;
		}
	}
	//var_dump($inf_money_s);
	
	$garant_money = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
	
$opt = cmsCore::request("opt","str");

if($opt == "perevod_gf"){
	$money = cmsCore::request("money","str");
	$id_sys = cmsCore::request("id_sys","str");
	if($id_sys){
		$tax_money = $inDB->get_field("cms_user_type_payment", "id='$id_sys'", "tax_money");
		$money_syst = $inDB->get_field('cms_user_score',"type_payment_id='$id_sys' AND user_id='$user_id'","score_val");
		if($money_syst > $money){
			// со счета уйдет $money
			// на ГФ перейдет $money_perevod
			$inf_score_out = $inDB->get_field("cms_user_score","type_payment_id='$id_sys' AND user_id='$user_id'","score_out");
			$gf = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
			
			$money_perevod = $money*(1 - $tax_money*0.01);
			$money_admin = $money_perevod*0.001;
			$money_perevod = $money_perevod - $money_admin;
			$blabla_car = $inDB->get_field('cms_user_score','user_id="0"','score_val') + $money_admin;
			$money_procent_admin = str_replace(',', '.', $money_procent_admin);
			$inDB->query("UPDATE cms_user_score SET score_val='$money_procent_admin' WHERE user_id='0'");
			$score_val = $money_syst - $money;
			
			$referer = $inDB->get_field("cms_preferal","ref_id_to='$user_id'","ref_id_from");
			if($referer){
				$procent = $inDB->get_field('cms_preferal_accrued','id="2" AND status="1"','procent');
				if($procent){
					$money_referer = $money_perevod - $money_perevod*(1-$procent*0.01);
										
					$money_procent_admin = $inDB->get_field('cms_user_score','user_id="0"','score_val') - $money_referer;
					$money_procent_admin = str_replace(',', '.', $money_procent_admin);
					$inDB->query("UPDATE cms_user_score SET score_val='$money_procent_admin' WHERE user_id='0'");
					
					$money_ref = $inDB->get_field("cms_preferal","ref_id_to='$user_id'","money");
					$money_ref = $money_ref + $money_referer;
					$money_ref = str_replace(',', '.', $money_ref);
					$inDB->query("UPDATE cms_preferal SET money='$money_ref' WHERE ref_id_from='$referer' AND ref_id_to='$user_id'");
				}
			}
			
			$data_report['user_id'] = $user_id;
			$data_report['description'] = 'Перевод средств со щета на гарантийний фонд.';
			$data_report['currency'] = $id_sys;
			$data_report['money'] = $money_perevod;
			$data_report['commission_pc'] = $money*($tax_money*0.01);;
			$data_report['commission_partner'] = $money_referer;
			$model->EventReport($data_report);
			
			$gf_plus = str_replace(',', '.', ($gf + $money_perevod));
			$score_mon_out = $inf_score_out + $money;
			$score_mon_out = str_replace(',', '.', $score_mon_out);
			$money_perevod = $inDB->get_field('cms_user_expenses',"user_id='$user_id' AND expense_type_id='4'", 'expense_inpay') + $money_perevod;
			$money_perevod = str_replace(',', '.', $money_perevod);
			$inDB->query("UPDATE cms_user_score SET score_val='$score_val', score_out='$score_mon_out' WHERE type_payment_id='$id_sys' AND user_id='$user_id'");
			$inDB->query("UPDATE cms_user_expenses SET expense_money='$gf_plus' WHERE user_id='$user_id' AND expense_type_id='4'");
			$inDB->query("UPDATE cms_user_expenses SET expense_inpay='$money_perevod' WHERE user_id='$user_id' AND expense_type_id='4'");
			$model->EventHistory($user_id, 'Перевод средств со щета на гарантийний фонд.');
			cmsCore::redirect('/users/'.$user_id.'/guarante_fond.html#success');
			//var_dump($score_val." ; ".$gf_plus." ; ".$score_mon_out);
		}
		else{
			cmsCore::redirect('/users/'.$user_id.'/guarante_fond.html#critical_error');
		}
	}
}

	cmsPage::initTemplate('components', 'com_users_guarante_fond')->
			assign('user_id', $user_id)->
			assign('inf_money_s', $inf_money_s)->
			assign('garant_money', $garant_money)->
            display('com_users_guarante_fond.tpl');
}

//==============================================================================
//=========================Создание заявки на биржу обмена==========================
//==============================================================================

if($do=='payment_system'){
	
	$inPage->setTitle("Платежные данные");
	$user_id=$model->returnId();
	
	if(isset($_POST['sellPamm'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		
		isset($_POST['price']) ? $model->sellToExchange($_POST) : $model->sellToService($_POST);
		
		if(isset($_POST['price']))
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successExchange');
		else
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successServise');
	}
	
	if(isset($_POST['count'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$user_id=$model->returnId();
		$model->buyAccount($_POST);
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successBuy');
	}
	
	if(isset($_POST['new-price'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$model->changeStatus($_POST);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successChange');
	}
	
	if(isset($_POST['remove-success'])){
		
		$model->removePamms($_POST['remove-success']);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successRemove');
	}
		
	$getPammAccount=$model->getPammAccount();
	$getPammCustomer=$model->getExchangePammNew();
	$getDefaultPammAccount=$model->getDefaultPammNew();
	
	$getPamms=$model->getPammAccounts($user_id);
	
	$userCash=$model->getCash();
	
	$id = $inUser->id;
	$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	foreach($payment_sustem as $key => $payment_s){
		$payment['id'] = $payment_s['id'];
		$payment['name'] = $payment_s['name_sustem'];
		$payment['img'] = $payment_s['min_img'];
		$payment['select'] = $payment_s['on_off'];
		$payment['relations'] = $payment_s['relations'];
		$payments[$payment['id']] = $payment;
	}
	
	$kol_elements = count($payments);
	
	$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$id'");
	
	foreach($table_inf_money as $key => $inf_money_s){
		$inf_money['id'] = $inf_money_s['type_payment_id'];
		$inf_money['score_num'] = $inf_money_s['score_num'];
		$inf_money['score_val'] = $inf_money_s['score_val'];
		$inf_money['score_in'] = $inf_money_s['score_in'];
		$inf_money['score_out'] = $inf_money_s['score_out'];
		$inf_money['num_payment'] = $inf_money_s['num_payment'];
		$inf_money['status'] = $inf_money_s['status'];
		$inf_moneys[$inf_money['id']] = $inf_money;
	}
	
	$verification = $inDB->get_field("cms_users", "id='$user_id'", "verify_stat");
	
	$money_garant = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
	
	$opt = cmsCore::request('opt', 'str');
	
if ($opt == 'save_paym_sys'){

	$errors = false;
	$system_id = cmsCore::request('system_id', 'str');
	$score = cmsCore::request('score', 'str');
	
	$sql22 = "UPDATE vds_user_score SET score_num='$score' WHERE user_id = '$id' 
	AND type_payment_id = '$system_id'";
	$inDB->query($sql22);
	
	$model->EventHistory($id, "Изменения номера счета");
		
	cmsCore::redirect('/users/'.$id.'/payment_system.html#save_paym_sys');
}
	
if($opt == "history_back"){
	
	$vubor_garanta = cmsCore::request("vubor_garanta", 'str');
	$sell_parts = cmsCore::request("sell_parts", 'str');
	$what_ps = cmsCore::request("what_ps", 'str');
	if($vubor_garanta == '3' && $what_ps != "satndart_ps"){
		
		$payment_user = cmsCore::request("payment_user", 'str');
		$payment_system_name = cmsCore::request("payment_system_name", 'str');
		
		
		if($what_ps == "user_ps"){
			$us_sys_name2 = cmsCore::request("us_sys_name2", 'str');
			$kyrs = cmsCore::request("kyrs", 'str');
			$payment_system = cmsCore::request("payment_system", 'str');
			$score = cmsCore::request("score", 'str');
		}
		elseif($what_ps == "user_st_ps"){
			$system_money = cmsCore::request("payment_system", "array");
			$kurs = cmsCore::request("kurs", "str");
			//var_dump($kurs);
			$score = cmsCore::request("score", 'str');
			$us_sys_name2 = cmsCore::request("us_sys_name2", 'str');
			$payment_system_id = $inDB->get_field("cms_user_type_payment","name_sustem='$payment_system_name'","id");
		}
		elseif($what_ps == "standart_user_ps"){
			$system_name = cmsCore::request("system_name", "array");
			$system_money = cmsCore::request("system_money", "array");
			$kurs = cmsCore::request("kurs", "array");
			foreach($system_name as $key => $systems){
				$system_inf['id'] = $inDB->get_field("cms_user_type_payment", "name_sustem='$systems'", 'id');
				$system_inf['kurs'] = $kurs[$key];
				$system_inf['system_money'] = $system_money[$key];
				$information[$system_inf['id']] = $system_inf;
			}
		}
	}
	else{
		$system_name = cmsCore::request("system_name", "array");
		$system_money = cmsCore::request("system_money", "array");
		$kurs = cmsCore::request("kurs", "array");
		$payment_user = cmsCore::request("payment_user", 'str');
		$payment_system_id = cmsCore::request("payment_system_id", 'str');
		
		foreach($system_name as $key => $systems){
			$system_inf['id'] = $inDB->get_field("cms_user_type_payment", "name_sustem='$systems'", 'id');
			$system_inf['kurs'] = $kurs[$key];
			$system_inf['system_money'] = $system_money[$key];
			$information[$system_inf['id']] = $system_inf;
		}
	}
	
}
	$nik_user = $inDB->get_field("cms_users", "id='$id'", 'login');
	
	
	
	
	cmsPage::initTemplate('components', 'com_users_payment_system')->
			assign('payments', $payments)->
			assign('id', $id)->
			assign('what_ps', $what_ps)->
			assign('money_garant', $money_garant)->
			assign('nik_user', $nik_user)->
			assign('verification', $verification)->
			assign('payment_user', $payment_user)->
			assign('payment_system_id', $payment_system_id)->
			assign('vubor_garanta', $vubor_garanta)->
			assign('sell_parts', $sell_parts)->
			assign('information', $information)->
			assign('inf_moneys', $inf_moneys)->
			assign('user_accounts', $getPamms)->
			assign('pammAccount', $getPammAccount)->
			assign('pammDefault', $getDefaultPammAccount)->
			assign('getPammCustomer', $getPammCustomer)->
			assign('userCash', $userCash)->
			assign('us_sys_name2', $us_sys_name2)->
			assign('kyrs', $kyrs)->
			assign('kurs', $kurs)->
			assign('payment_system', $payment_system)->
			assign('score', $score)->
			assign('payment_system_name', $payment_system_name)->
            display('com_users_payment_system.tpl');
}

if($do=='service'){
	
	$inPage->setTitle("Обмен через сервис");
	$user_id=$model->returnId();
	
	if(isset($_POST['sellPamm'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		
		isset($_POST['price']) ? $model->sellToExchange($_POST) : $model->sellToService($_POST);
		
		if(isset($_POST['price']))
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successExchange');
		else
			cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successServise');
	}
	
	if(isset($_POST['count'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$user_id=$model->returnId();
		$model->buyAccount($_POST);
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successBuy');
	}
	
	if(isset($_POST['new-price'])){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#errorCaptcha');
        }
		$model->changeStatus($_POST);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successChange');
	}
	
	if(isset($_POST['remove-success'])){
		
		$model->removePamms($_POST['remove-success']);
		
		cmsCore::redirect('/users/'.$user_id.'/pamminfo.html#successRemove');
	}
		
	$getPammAccount=$model->getPammAccount();
	$getPammCustomer=$model->getExchangePammNew();
	$getDefaultPammAccount=$model->getDefaultPammNew();
	
	$getPamms=$model->getPammAccounts($user_id);
	
	$userCash=$model->getCash();
	
	$id = $inUser->id;
	$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	foreach($payment_sustem as $key => $payment_s){
		$payment['id'] = $payment_s['id'];
		$payment['name'] = $payment_s['name_sustem'];
		$payment['img'] = $payment_s['min_img'];
		$payment['select'] = $payment_s['on_off'];
		$payment['money_admin'] = $payment_s['payment_admin'];
		$payment['relations'] = $payment_s['relations'];
		$payment['tax_money'] = $payment_s['tax_money'];
		$payments[$payment['id']] = $payment;
	}
	
	$kol_elements = count($payments);
	
	$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$id'");
	
	foreach($table_inf_money as $key => $inf_money_s){
		$inf_money['id'] = $inf_money_s['type_payment_id'];
		$inf_money['score_num'] = $inf_money_s['score_num'];
		$inf_money['score_val'] = $inf_money_s['score_val'];
		$inf_money['score_in'] = $inf_money_s['score_in'];
		$inf_money['score_out'] = $inf_money_s['score_out'];
		$inf_money['num_payment'] = $inf_money_s['num_payment'];
		$inf_money['status'] = $inf_money_s['status'];
		$inf_moneys[$inf_money['id']] = $inf_money;
	}
	$proverochka = 0;
	$table_discount = $inDB->get_table('cms_user_discount','user_id="'.$id.'" OR user_id="0"');
	for($i=0;isset($table_discount[$i]);$i++){
		$date_nuv = strtotime(date("Y.m.d"));
		$date_end =  strtotime($table_discount[$i]['end_date']);
		if($date_nuv > $date_end){
			$inDB->query("DELETE FROM cms_user_discount WHERE id='".$table_discount[$i]['id']."'");
			$proverochka = 1;
		}
		$discount['id'] = $i;
		$discount['discount'] = $table_discount[$i]['discount'];
		$discounts[] = $discount;
	}
	
	$table_inf_money2 = $inDB->get_table("cms_user_score", "user_id='1'");
	
	foreach($table_inf_money2 as $key => $inf_money_s2){
		$inf_money2['id'] = $inf_money_s2['type_payment_id'];
		$inf_money2['score_num'] = $inf_money_s2['score_num'];
		$inf_money2['score_val'] = $inf_money_s2['score_val'];
		$inf_money2['score_in'] = $inf_money_s2['score_in'];
		$inf_money2['score_out'] = $inf_money_s2['score_out'];
		$inf_money2['num_payment'] = $inf_money_s2['num_payment'];
		$inf_money2['status'] = $inf_money_s2['status'];
		$inf_moneys2[$inf_money2['id']] = $inf_money2;
	}
	
	$verification = $inDB->get_field("cms_users", "id='$user_id'", "verify_stat");
	
	$infa_user = $inDB->get_fields("cms_users", "id='$user_id'","nickname, email");
	
	$opt = cmsCore::request('opt', 'str');
	
if ($opt == 'inf_service'){
	$zapros = $inDB->get_fields("cms_user_requests_for_payment", "id='".$_POST['id_zapusa']."'", "id_paym_syst, money_off, masive_inf");
	$zayavka['id_syst_usr'] = $zapros['id_paym_syst'];
	$zayavka['money_off'] = $zapros['money_off'];
	$zapros_mas = unserialize($zapros['masive_inf']);
	foreach($zapros_mas as $key => $result){
		$rrr['id_sys'] = $result['sustem_id'];
		$tax_moneys = $inDB->get_field("cms_user_type_payment", "id='".$result['sustem_id']."'", "tax_money");
		$rrr['money_sys'] = $zapros['money_off'] - $zapros['money_off']*$tax_moneys*0.01;
		$rrr_s[] = $rrr;
	}
	$zayavka['massive'] = $rrr_s;
	
	
}
	
	
if ($opt == 'save_paym_sys'){

	$errors = false;
	$system_id = cmsCore::request('system_id', 'str');
	$score = cmsCore::request('score', 'str');
	
	$sql22 = "UPDATE vds_user_score SET score_num='$score' WHERE user_id = '$id' 
	AND type_payment_id = '$system_id'";
	$inDB->query($sql22);
	
	$model->EventHistory($id, "Изменения номера счета");
	
	cmsCore::redirect('/users/'.$id.'/payment_system.html#save_paym_sys');
}

if ($opt == "registration"){
	$money_user = cmsCore::request('payment_user', 'str');
	//$money_admin = cmsCore::request('payment_system', 'array');
	$paym_sys_user = cmsCore::request('payment_system_user', 'str');
	$paym_sys_admin = cmsCore::request('payment_system', 'array');
	$user_emaill = cmsCore::request('user_email', 'str');
	$user_name = cmsCore::request('user_name', 'str');
	$discount_usr = cmsCore::request('discount', 'str');
	//$all_summ = cmsCore::request('all_money', 'str');
	//$kurs_system = cmsCore::request('kyrs', 'array');
	//$all_kurs = cmsCore::request('commission', 'str');
	
	$relations = $inDB->get_field("cms_user_type_payment","id='$paym_sys_user'","relations");
	$relations = "$relations";
	$lkl = 0;
	if($relations && $relations != ""){
		foreach($paym_sys_admin as $key => $resylt){
			$number = ",$key,";
			if(strpos($relations, $number) === FALSE){
				$lkl = 1;
			}
			unset($number);
		}
	}
	$opopo = 0;
	foreach($paym_sys_admin as $key=>$resylt){
		$money_baza = $inDB->get_field('cms_user_relation_pc', "system='$paym_sys_user' AND relation_syst='$key'", 'min_money');
		$kurs_sust_1 = $inDB->get_field("cms_user_type_payment","id='$key'","tax_money");
		
		$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
		if($money_baza && $money_baza >= $money_admin){
			$opopo = 1;
		}
	}
	
	if($opopo == 1){
		cmsCore::redirect('/users/'.$id.'/service.html#min_money');
	}
	
	if($lkl == 0){
	$red_appl = 0;
	foreach($paym_sys_admin as $key => $resylt){ 
	if( $resylt > $money_user ){
		$red_appl = 1;
	}
	$kurs_sust_1 = $inDB->get_field("cms_user_type_payment","id='$key'","tax_money");
	
	$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
	
	$asdasdcc = str_replace(",", ".", $money_admin);
	$money_admin = str_replace(',', '.', round($asdasdcc, 2));
	
	$limit_money = $inDB->get_field("cms_user_type_payment", "id='$key'", "payment_admin");
	if($money_admin > $limit_money){
		cmsCore::redirect('/users/'.$id.'/service.html#limit');
	}
	
	if($discount_usr){
		$discount_nuw =  $inDB->get_field('cms_user_discount','user_id="'.$id.'" OR id="'.$discount_usr.'"',"discount");
		if(!$discount_nuw){
			cmsCore::redirect('/users/'.$id.'/service.html#critical_error');
		}
	}
	
	$kurs_system = $kurs_sust_1;
	$all_kurs = $kurs_system + 0.5;
	if(isset($discount_nuw)){
		//=========================
		//== тут может вычитается скидка со счета админа для скидок, но админу нужно всеравно пополнять счет юзера == 
		//=========================
		$all_summ = $money_admin - $money_admin*0.005;
		$all_summ = $all_summ + $all_summ*0.01*$discount_nuw;
		if($discount_nuw != "0"){
			$inDB->query("DELETE FROM cms_user_discount WHERE id='$discount_usr'");
		}
	}
	else{
		$all_summ = $money_admin - $money_admin*0.005;
	}
	
	$asdasdcc = str_replace(",", ".", $all_summ);
	$all_summ = str_replace(',', '.', round($asdasdcc, 2));
	
	$infa['system_minus'] = $key;
	$infa['all_summ'] = $all_summ;
	$infa['money_admin'] = $money_admin;
	$infa['kurs_system'] = $kurs_system;
	$infa['all_kurs'] = $all_kurs;
	$informations[$key] = $infa;
	
	$paym_syst_name2 = $inDB->get_field("cms_user_type_payment", "id='$key'", "name_sustem");
	$score_aaa = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='$key'","score_num");
	$message_inf_admin .= ' <li>
								<ul>
									<li>исходящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score_aaa.';</li>
								</ul>
							</li>' ;
	$message_inf_user .= '  <li>
								<ul>
									<li>входящяя сумма : '.$all_summ.' $;</li>
									кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score_aaa.';</li>
								</ul>
							</li>' ;
	$message_mail_user .= ' <li>
								<ul>
									<li>входящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score_aaa.';</li>
									<li>общая комиссия(%): '.$all_kurs.';</li>
								</ul>
							</li>';
	$message_mail_admin .= ' <li>
								<ul>
									<li>исходящяя сумма : '.$all_summ.' $;</li>
									<li>кошелек - '.$paym_syst_name2.';</li>
									<li>номер кошелька - '.$score_aaa.';</li>
									<li>комиссия системы(%): '.$kurs_system.';</li>
									<li>общий курс: '.$all_kurs.';</li>
									<li>средства с комиссией системы(для проверки): '.$money_admin.';</li>
								</ul>
							</li>';
	
	}
	
	$config = cmsConfig::getDefaultConfig(); 
	$admin_email = $config['sitemail'];
	
	$infa_s = serialize($informations);
	$time = time();	
	$sql113 = $inDB->query("INSERT cms_user_exchange_service (user_id, money_plus, system_plus ,infa, user_name, user_email, status, date) VALUES ('$id','$money_user','$paym_sys_user','$infa_s','$user_name','$user_emaill','1','$time')");
	
	$paym_syst_name1 = $inDB->get_field("cms_user_type_payment", "id='$paym_sys_user'", "name_sustem");
	$score_paym_1 = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='$paym_sys_user'","score_num");
	//var_dump($user_name." ; ".$user_emaill);
	
	if($red_appl == 1){
		$last_id = $inDB->get_last_id('cms_user_exchange_service');
		cmsUser::sendMessage(USER_MASSMAIL, '1', "<p>Подозрительная заявка <b>№ ".$last_id."</b>, обмен через сервис. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a class='podozr_appl' href='/users/1/administration.html#service'>Посмотреть заявку</a></p>");
		
		$inf_mail3 = array();
		$inf_mail3['email'] = $admin_email;
		$inf_mail3['message'] = "<p>Подозрительная заявка <b>№ ".$last_id."</b>, обмен через сервис. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a href='http://".$_SERVER['HTTP_HOST']."/users/1/administration.html#service'>Посмотреть заявку</a></p>";
		$email3 = $model->sendEmail3($inf_mail3, '');
	}
	$last_id = $inDB->get_last_id('cms_user_exchange_service');
	cmsUser::sendMessage(USER_MASSMAIL, $id, "<p><b>Заявка № $last_id на обмен средств через сервис:</b></p>
<ul> <li>исходящяя сумма : $money_user $;</li> <li>кошелек - $paym_syst_name1;</li><li>номер кошелька - '.$score_paym_1.';</li></ul>
<b>Администратор выберет одну из ПС:</b><br><ul>$message_inf_user</ul>");
	cmsUser::sendMessage(USER_MASSMAIL, '1', "<p><b>Заявка № $last_id на обмен средств через сервис:</b></p>
<ul> <li>входящая сумма : $money_user $;</li> <li>кошелек - $paym_syst_name1;</li><li>номер кошелька - '.$score_paym_1.';</li></ul>
<b>ПС:</b><br><ul>
$message_inf_admin</ul>
<b>от:</b> $user_name; E-mail: $user_emaill;");
	
	$user_email = $user_emaill;
	
	$inf_mail = array();
	$inf_mail['email'] = $user_email;
	$inf_mail['message'] = "<p><b>Заявка № $last_id на обмен средств через сервис:</b></p>
		<ul> <li>исходящяя сумма : $money_user $;</li> <li>система оплаты - $paym_syst_name1;</li><li>номер кошелька - '.$score_paym_1.';</li></ul>
		<b>Администратор выберет одну из ПС:</b><br><ul>$message_mail_user </ul>";
		
	$email = $model->sendEmail3($inf_mail);
	
	$inf_mail2 = array();
	$inf_mail2['email'] = $admin_email;
	$inf_mail2['message'] = "<p><b>Заявка № $last_id на обмен средств через сервис:</b></p>
		<ul> <li>входящая сумма : $money_user $;</li> <li>система оплаты - $paym_syst_name1;</li><li>номер кошелька - '.$score_paym_1.';</li></ul>
		<b>ПС:</b><br><ul>$message_mail_admin </ul>
		<b>от:</b> $user_name; E-mail: $user_emaill;";
		
	$email2 = $model->sendEmail3($inf_mail2);
	
	$login = $inDB->get_field("cms_users", "id='$id'", "login");
	
	$data_report['user_id'] = $id;
	$data_report['description'] = 'Обмен через сервис.';
	$data_report['currency'] = $paym_sys_user;
	$data_report['money'] = $money_user;
	$data_report['commission_pc'] = $kurs_sust_1;
	$data_report['commission_serv'] = 0.5;
	$data_report['commission_partner'] = 0;
	$model->EventReport($data_report);
	
	$model->EventHistory($id, "Обмен через сервис");
	
	cmsCore::redirect('/users/'.$login.'#application_service');
}
else{
	cmsCore::redirect('/users/'.$id.'/service.html#critical_error');
}

}
	$table_relation_pc = $inDB->get_table('cms_user_relation_pc','status="1"','system');
	if($table_relation_pc){
		
		foreach($table_relation_pc as $result){
			$infa_id[] = $result['system'];
		}
		
		$perem112 = array_unique($infa_id);
		foreach($perem112 as $result){
			$inf_sys = $inDB->get_table('cms_user_relation_pc', "system='$result' AND status='1'");
			foreach($inf_sys as $result2){
				$infa['kurs'] = $result2['kurs'];
				$infa['commission'] = $result2['commission'];
				$infa['min_money'] = $result2['min_money'];
				$inf_relations[$result2['relation_syst']] = $infa;
				unset($relation);
			}
			$inf_relation[$result] = $inf_relations;
			unset($inf_relations);
		}
	}
	//var_dump($inf_relation);
	if($_GET){
		$sys_1 = $_GET['system1'];
		$sys_2 = $_GET['system2'];
	}
	if($proverochka == 1){
		cmsCore::redirect('/users/'.$id.'/service.html');
	}
 
	$nik_user = $inDB->get_field("cms_users", "id='$id'", 'login');
	
	cmsPage::initTemplate('components', 'com_users_service')->
		assign('payments', $payments)->
		assign('id', $id)->
		assign('inf_relation', $inf_relation)->
		assign('discounts', $discounts)->
		assign('nik_user', $nik_user)->
		assign('zayavka', $zayavka)->
		assign('sys_1', $sys_1)->
		assign('sys_2', $sys_2)->
		assign('infa_user', $infa_user)->
		assign('verification', $verification)->
		assign('payment_user', $payment_user)->
		assign('payment_system_id', $payment_system_id)->
		assign('vubor_garanta', $vubor_garanta)->
		assign('sell_parts', $sell_parts)->
		assign('information', $information)->
		assign('inf_moneys', $inf_moneys)->
		assign('server_name', $_SERVER[SERVER_NAME])->
		assign('inf_moneys2', $inf_moneys2)->
		assign('user_accounts', $getPamms)->
		assign('pammAccount', $getPammAccount)->
		assign('pammDefault', $getDefaultPammAccount)->
		assign('getPammCustomer', $getPammCustomer)->
		assign('userCash', $userCash)->
		display('com_users_service.tpl');
}

if($do=='registration_service'){
	/*$inPage->setTitle("Платежные данные");
	$user_id=$model->returnId();
	$id = $inUser->id;
	
		$errors = false;
		
		
	$money_user = $_GET['money'];
	$paym_sys_user = $_GET['paym_user'];
	$paym_sys_admin = $_GET['paym_admin'];
	
	$kurs_sust_1 = $inDB->get_field("cms_user_type_payment","id='$paym_sys_admin'","tax_money");
	
	$money_admin = $money_user - $money_user*$kurs_sust_1*0.01;
	
	$asdasdcc = str_replace(",", ".", $money_admin);
	$money_admin = str_replace(',', '.', round($asdasdcc, 2));
	
	$kurs_system = $kurs_sust_1;
	$all_kurs = $kurs_system + 0.5;
	$all_summ = $money_admin - $money_admin*0.005;
	
	$asdasdcc = str_replace(",", ".", $all_summ);
	$all_summ = str_replace(',', '.', round($asdasdcc, 2));
	
	/*echo $money_user."<br>";
	echo $money_admin."<br>";
	echo $paym_sys_user."<br>";
	echo $paym_sys_admin."<br>";
	echo $all_summ."<br>";
	echo $kurs_system."<br>";
	echo $all_kurs."<br>";*/
	/*
	$paym_syst_name1 = $inDB->get_field("cms_user_type_payment", "id='$paym_sys_user'", "name_sustem");
	$paym_syst_name2 = $inDB->get_field("cms_user_type_payment", "id='$paym_sys_admin'", "name_sustem");
	
	$sql113 = $inDB->query("INSERT cms_user_exchange_service (user_id, money_plus, system_plus ,money_minus, system_minus, money_minus_system, kurs_sustem, kurs_all) VALUES ('$id','$money_user','$paym_sys_user','$all_summ','$paym_sys_admin','$money_admin','$kurs_system','$all_kurs')");
	
	cmsUser::sendMessage(USER_MASSMAIL, $id, "<p>Заявка на обмен средств через сервис:</p>
		- исходящяя сумма : $money_user, (система оплаты - $paym_syst_name1);<br>
		- входящая сумма : $all_summ, (система оплаты - $paym_syst_name2);.");
	cmsUser::sendMessage(USER_MASSMAIL, '1', "<p>Заявка на обмен средств через сервис:</p>
		- входящая сумма : $money_user, (система оплаты - $paym_syst_name1);<br>
		- исходящяя сумма : $all_summ, (система оплаты - $paym_syst_name2);.");
	
	$user_email = $inDB->get_field("cms_users", "id='$id'", "email");
	$inf_mail = array();
	$inf_mail['email'] = $user_email;
	$inf_mail['message'] = "<p>Заявка на обмен средств через сервис:</p>
		- входящая сумма : $money_user, (система оплаты - $paym_syst_name1);<br>
		- исходящяя сумма : $all_summ, (система оплаты - $paym_syst_name2);<br>
		- курс системы: $kurs_system, общий курс: $all_kurs;<br>
		- средства с курсом системы(для проверки): $money_admin.";
		
	$email = $model->sendEmail3($inf_mail);
	
	$login = $inDB->get_field("cms_users", "id='$id'", "login");
	
	$fff = $model->EventHistory($id, "Обмен через сервис");
	
	//echo $fff;
	cmsCore::redirect('/users/'.$login.'#application_service');

		
		
	cmsPage::initTemplate('components', 'com_users_registration_service')->
		assign('id', $id)->
	display('com_users_registration_service.tpl');*/
}

if($do=='design'){
	
	$inPage->setTitle("Платежные данные");
	$user_id=$model->returnId();
	$id = $inUser->id;
	
		$errors = false;
	$opt = cmsCore::request('opt', 'str');
if($opt == "registration"){
	
	$vubor_garanta = cmsCore::request("vubor_garanta", 'str');
	$what_ps = cmsCore::request("what_ps", 'str');
	$sell_parts = cmsCore::request("sell_parts", "str");
	
	if($vubor_garanta == '3' && $what_ps != "satndart_ps"){
		
		
		if($what_ps == "user_st_ps"){
			$payment_user = cmsCore::request("payment_user", 'str');
			$u_system_id = cmsCore::request("u_system_id", 'array');
			
			foreach($u_system_id as $key => $systems){
				$u_system_id = $key;
			}
			$u_system_img = $inDB->get_field("cms_user_type_payment", "id='$u_system_id'", 'min_img');
			//var_dump($u_system_img);
			$u_system_name = $inDB->get_field("cms_user_type_payment", "id='$u_system_id'", 'name_sustem');
			$system_id = cmsCore::request("system_id", 'array');
			$kyrs = cmsCore::request("kyrs", 'str');
			$payment_system = cmsCore::request("payment_system", 'array');
			
			$us_sys_name2 = cmsCore::request("us_sys_name2", 'str');
			$payment_system = cmsCore::request("payment_system", 'str');
			$score = cmsCore::request('score', 'str');
			
		}
		elseif($what_ps == "user_ps"){
			$us_sys_name1 = cmsCore::request("us_sys_name1", 'str');
			$payment_user = cmsCore::request("payment_user", 'str');
			$us_sys_name2 = cmsCore::request("us_sys_name2", 'str');
			$kyrs = cmsCore::request("kyrs", 'str');
			$payment_system = cmsCore::request("payment_system", 'str');
			$score = cmsCore::request('score', 'str');
		}
		elseif($what_ps == "standart_user_ps"){
			$us_sys_name1 = cmsCore::request("us_sys_name1", 'str');
			$payment_user = cmsCore::request("payment_user", 'str');
			$system_id = cmsCore::request("system_id", 'array');
			$kyrs = cmsCore::request("kyrs", 'array');
			$payment_system = cmsCore::request("payment_system", 'array');
			
			foreach($system_id as $key => $systems){
				$asdasdcc = str_replace(",", ".", $kyrs[$key]);
				$payment['kurs'] = str_replace(',', '.', round($asdasdcc, 3));
				$payment['name'] = $inDB->get_field("cms_user_type_payment", "id='$key'", 'name_sustem');
				$payment['img'] = $inDB->get_field("cms_user_type_payment", "id='$key'", 'min_img');
				$payment['score'] = $inDB->get_field("cms_user_score", "user_id='$id' AND type_payment_id='$key'", 'score_num');
				$payment['valuta'] = $payment_system[$key];
				$nats_bunk[] = $payment;
			}
		}
		
	}
	else{
		$payment_user = cmsCore::request("payment_user", 'str');
		$u_system_id = cmsCore::request("u_system_id", 'array');
		foreach($u_system_id as $key => $systems){
			$u_system_id = $key;
		}
		$u_system_img = $inDB->get_field("cms_user_type_payment", "id='$u_system_id'", 'min_img');
		$u_system_name = $inDB->get_field("cms_user_type_payment", "id='$u_system_id'", 'name_sustem');
		
		$system_id = cmsCore::request("system_id", 'array');
		$kyrs = cmsCore::request("kyrs", 'array');
		$payment_system = cmsCore::request("payment_system", 'array');
		
		foreach($system_id as $key => $systems){
			$asdasdcc = str_replace(",", ".", $kyrs[$key]);
			$payment['kurs'] = str_replace(',', '.', round($asdasdcc, 3));
			$payment['name'] = $inDB->get_field("cms_user_type_payment", "id='$key'", 'name_sustem');
			$payment['img'] = $inDB->get_field("cms_user_type_payment", "id='$key'", 'min_img');
			$payment['score'] = $inDB->get_field("cms_user_score", "user_id='$id' AND type_payment_id='$key'", 'score_num');
			$payment['valuta'] = $payment_system[$key];
			$nats_bunk[] = $payment;
		}
		//var_dump($nats_bunk);
	}
}	
if($opt == 'success'){
	/*------- отправка сообщения на выплату ------*/
	/* 
		1 - id
		2 - id_user
		3 - гаран\внутрен конверт
		4 - ід системи с которой отдает
		5 - деньги что отдает
		6 - масив (id системы оплаты + счет перевода + курс + деньги )
	
	*/
	$sell_parts = cmscore::request("sell_parts", 'str');
	$vubor_garanta = cmsCore::request("vubor_garanta", 'str');
	if($sell_parts == "on"){$sell_parts = 1;}
	else{$sell_parts = 0;}
	$second = cmsCore::request("second", 'str');
	if($second == "on"){$second = 1;}
	else{$second = 0;}
	$what_ps = cmscore::request("what_ps", 'str');	
	
	if($vubor_garanta == 3 && $what_ps != 'satndart_ps'){
		
		
		$payment_user = cmscore::request("payment_user", 'str');
		$garant_money = $inDB->get_field("cms_user_expenses","user_id='$id' AND expense_type_id='4'","expense_money");
		if($payment_user > $garant_money){
			$login = $inDB->get_field("cms_users","id='$id'","login");
			cmsCore::redirect('/users/'.$login.'#critical_error');
		}
		$payment_system_name = cmscore::request("payment_system_name", 'str');
		$payment_system_id = $model->Create_id_system($payment_system_name);
		
		if($what_ps == 'user_ps'){
			$us_sys_name2 = cmscore::request("us_sys_name2", 'str');
			$us_sys_id = $model->Create_id_system($us_sys_name2);
			$kurs = cmscore::request("kyrs", 'str');
			$score = cmscore::request("score", 'str');
			
			$kurs = str_replace(',', '.', round(str_replace(",", ".", $kurs), 3));
			$system_money = str_replace(',', '.', round(($kurs*$payment_user), 3)); 
			$masiv = array("sustem_id"=>$us_sys_id, "sustem_name"=>$us_sys_name2, "score"=>$score, "kurs"=>$kurs, "money"=>$system_money);
			$masivs[] = $masiv;
						
			$min_array = serialize($masivs);
		}
		elseif($what_ps == 'user_st_ps'){//========================================
			
			$us_sys_name2 = cmscore::request("us_sys_name2", 'str');
			$us_sys_id = $model->Create_id_system($us_sys_name2);
			$kurs = cmscore::request("kyrs", 'str');
			$score = cmscore::request("score", 'str');
			
			$kurs = str_replace(',', '.', round(str_replace(",", ".", $kurs), 3));
			$system_money = str_replace(',', '.', round(($kurs*$payment_user), 3)); 
			
			$masiv = array("sustem_id"=>$us_sys_id, "sustem_name"=>$us_sys_name2, "score"=>$score, "kurs"=>$kurs, "money"=>$system_money);
			$masivs[] = $masiv;
			
			$min_array = serialize($masivs);
		}
		elseif($what_ps == 'standart_user_ps'){
			$score = cmsCore::request("score", "array");
			$system_name = cmsCore::request("system_name", "array");
			$system_money = cmsCore::request("system_money", "array");
			$kurs = cmsCore::request("kurs", "array");
			
			$i = 0;
			foreach($system_name as $key => $systems){
				$system_id = $inDB->get_field("cms_user_type_payment", "name_sustem='$systems'", 'id');
				
				$kurs[$i] = str_replace(',', '.', round(str_replace(",", ".", $kurs[$i]), 3));
				$system_money[$i] = str_replace(',', '.', round(($kurs[$i]*$payment_user), 3)); 
				$masiv = array("sustem_id"=>$system_id, "sustem_name"=>"0", "score"=>$score[$i], "kurs"=>$kurs[$i], "money"=>$system_money[$i]);				
				
				$masivs[$i] = $masiv;
				$i++;
			}
			$min_array = serialize($masivs);
		}
		
		$GF = $inDB->get_field("cms_user_expenses", "user_id='$id' AND expense_type_id='4'", "expense_money");
		$result_money = $GF - ($payment_user);
		$result_money = str_replace(',', '.', $result_money);
		$sql_1 = "UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='$id' AND expense_type_id='4'";
		$inDB->query($sql_1);
		
		$sql113 = "INSERT cms_user_requests_for_payment (user_id, garant, id_paym_syst, name_system, money_off, masive_inf, sell_parts, sms) VALUES ('$id','$vubor_garanta','$payment_system_id','$payment_system_name','$payment_user','$min_array','$sell_parts','$second')";
		$inDB->query($sql113);
	}
	else{
		$score = cmsCore::request("score", "array");
		$system_name = cmsCore::request("system_name", "array");
		$system_money = cmsCore::request("system_money", "array");
		$kurs = cmsCore::request("kurs", "array");
		$payment_user = cmsCore::request("payment_user", 'str');
		$payment_system_id = cmsCore::request("payment_system_id", 'str');
		
		$garant_money = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='$payment_system_id'","score_val");
		if($payment_user > $garant_money){
			$login = $inDB->get_field("cms_users","id='$id'","login");
			cmsCore::redirect('/users/'.$login.'#critical_error');
		}
		$i = 0;
		
		foreach($system_name as $key => $systems){
			$system_id = $inDB->get_field("cms_user_type_payment", "name_sustem='$systems'", 'id');
			
			$kurs[$i] = str_replace(',', '.', round(str_replace(",", ".", $kurs[$i]), 3));
			$system_money[$i] = str_replace(',', '.', round(($kurs[$i]*$payment_user), 3)); 
			$masiv = array("sustem_id"=>$system_id, "score"=>$score[$i], "kurs"=>$kurs[$i], "money"=>$system_money[$i]);				
			
			$masivs[$i] = $masiv;
			$i++;
		}
		$min_array = serialize($masivs);
		
		if($vubor_garanta == 1){
			$score_val = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='$payment_system_id'","score_val");
			$result_money = $score_val-$payment_user;
			$result_money = str_replace(',', '.', $result_money);
			$sql_1 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='$id' AND type_payment_id='$payment_system_id'";
			$inDB->query($sql_1);
		}
		elseif($vubor_garanta == 3){
			$GF = $inDB->get_field("cms_user_expenses", "user_id='$id' AND expense_type_id='4'", "expense_money");
			$result_money = $GF - ($payment_user);
			$result_money = str_replace(',', '.', $result_money);
			$sql_1 = "UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='$id' AND expense_type_id='4'";
			$inDB->query($sql_1);
		}
		$sql113 = "INSERT cms_user_requests_for_payment (user_id, garant, id_paym_syst ,money_off, masive_inf, sell_parts, sms) VALUES ('$id','$vubor_garanta','$payment_system_id','$payment_user','$min_array','$sell_parts','$second')";
	
		$inDB->query($sql113);
	}
	//var_dump($_POST);
	$red_appl = 0;
	if(is_array($_POST['kurs'])){
		foreach($_POST['kurs'] as $resylt){ 
			if( $resylt > 1 ){
				$red_appl = 1;
			}
		}
	}
	else{
		if( $_POST['kurs'] > 1 ){
			$red_appl = 1;
		}
	}
	if($red_appl == 1){
		$config = cmsConfig::getDefaultConfig(); 
		$admin_email = $config['sitemail'];
		
		$last_id = $inDB->get_last_id('cms_user_requests_for_payment');
		cmsUser::sendMessage(USER_MASSMAIL, '1', "<p>Подозрительная заявка <b>№ ".$last_id."</b>, биржа обмена. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a class='podozr_appl' href='/users/1/exchange.html?search_id=$last_id'>Посмотреть заявку</a></p>");
		
		$inf_mail3 = array();
		$inf_mail3['email'] = $admin_email;
		$inf_mail3['message'] = "<p>Подозрительная заявка <b>№ ".$last_id."</b>, биржа обмена. Сумма получения средств превышает сумму отдаваемых средств.</p><p><a href='http://".$_SERVER['HTTP_HOST']."?redirect_url=/users/1/exchange.html?search_id=$last_id'>Посмотреть заявку</a></p>";
		$email3 = $model->sendEmail3($inf_mail3, '');
		
	}
	
	
	
	/*$id;  
	$vubor_garanta;  
	$payment_system_id; 
	$payment_user; */ 
	
	$model->EventHistory($id, "Создание заявки на бирже обмена");
	
	cmsCore::redirect('/users/'.$id.'/exchange.html#success_message');
}

		
		
	cmsPage::initTemplate('components', 'com_users_design_money')->
		assign('id', $id)->
		assign('what_ps', $what_ps)->
		assign('nats_bunk', $nats_bunk)->
		assign('vubor_garanta', $vubor_garanta)->
		assign('sell_parts', $sell_parts)->
		assign('payment_user', $payment_user)->
		assign('u_system_img', $u_system_img)->
		assign('u_system_name', $u_system_name)->
		assign('u_system_id', $u_system_id)->
		assign('pammAccount', $getPammAccount)->
		assign('pammDefault', $getDefaultPammAccount)->
		assign('getPammCustomer', $getPammCustomer)->
		assign('userCash', $userCash)->
		assign('us_sys_name1', $us_sys_name1)->
		assign('us_sys_name2', $us_sys_name2)->
		assign('kyrs', $kyrs)->
		assign('payment_system', $payment_system)->
		assign('score', $score)->
		display('com_users_design_money.tpl');
}

if ($do == 'ordercard'){
	$user_id=$model->returnId();
	if(isset($_POST['email'])){
		$model->sendEmail($_POST);
		cmsCore::redirect('/users/'.$user_id.'/editprofile.html#successSend');
	}
	$userInfo=$model->getUserInfo();
	$birthdate=explode("-",$userInfo['birthdate']);
	$name=explode(" ",$userInfo['nickname']);
	$day=$birthdate[2];
	$monthUser=$birthdate[1];
	$year=$birthdate[0];
	$monthArray=array('01','02','03','04','05','06','07','08','09','10','11','12');
	
	$yearArray=array();
	for($i=1950;$i<2016;$i++){
		$yearArray[]=$i;
	}
	
	$dayArray=array();
	for($i=1;$i<32;$i++){
		($i<10)? $dayArray[]='0'.$i : $dayArray[]=$i;
	}

	cmsPage::initTemplate('components', 'com_users_order')->
			assign('userInfo', $userInfo)->
			assign('userName', $name)->
			assign('monthArray', $monthArray)->
			assign('yearArray', $yearArray)->
			assign('dayArray', $dayArray)->
			assign('day', $day)->
			assign('monthUser', $monthUser)->
			assign('year', $year)->
            display('com_users_order.tpl');

}
//============================================================================//
//========================= Список пользователей  ============================//
//============================================================================//
if ($do == 'view'){

    // если запрещен просмотр всех пользователей, 404
    if($model->config['sw_search'] == 2){
        cmsCore::error404();
    }

    //очищаем поисковые запросы если пришли со другой страницы
    if(!mb_strstr(cmsCore::getBackURL(), '/users')){
        cmsUser::sessionClearAll();
    }

	$stext = array();

	// Возможные входные переменные
	$name    = cmsCore::getSearchVar('name');
	$city    = cmsCore::getSearchVar('city');
	$hobby   = cmsCore::getSearchVar('hobby');
	$gender  = cmsCore::getSearchVar('gender');
	$orderby = cmsCore::request('orderby', 'str', 'regdate');
	$orderto = cmsCore::request('orderto', 'str', 'desc');
	$age_to  = (int)cmsCore::getSearchVar('ageto', 'all');
	$age_fr  = (int)cmsCore::getSearchVar('agefrom', 'all');

	if(!in_array($orderby, array('karma', 'rating'))) { $orderby = 'regdate'; }
	if(!in_array($orderto, array('asc', 'desc'))) { $orderto = 'desc'; }

	// Флаг о показе только онлайн пользователей
	if (cmsCore::inRequest('online')) {
		cmsUser::sessionPut('usr_online', (bool)cmsCore::request('online', 'int'));
		$page = 1;
	}
	$only_online = cmsUser::sessionGet('usr_online');

	if($only_online){
		$stext[] = $_LANG['SHOWING_ONLY_ONLINE'];
	}

	///////////////////////////////////////
	//////////Условия выборки//////////////
	///////////////////////////////////////

	// Добавляем в выборку имя, если оно есть
	if($name){
		$model->whereNameIs($name);
		$stext[] = $_LANG['NAME']." &mdash; ".htmlspecialchars(stripslashes($name));
	}

	// Добавляем в выборку город, если он есть
	if($city){
		$model->whereCityIs($city);
		$stext[] = $_LANG['CITY']." &mdash; ".htmlspecialchars(stripslashes($city));
	}

	// Добавляем в выборку хобби, если есть
	if($hobby){
		$model->whereHobbyIs($hobby);
		$stext[] = $_LANG['HOBBY']." &mdash; ".htmlspecialchars(stripslashes($hobby));
	}
	// Добавляем в выборку пол, если есть
	if($gender){
		$model->whereGenderIs($gender);
		if($gender == 'm'){
			$stext[] = $_LANG['MALE'];
		} else {
			$stext[] = $_LANG['FEMALE'];
		}
	}
	// Добавляем в выборку возраст, более
	if($age_fr){
		$model->whereAgeFrom($age_fr);
		$stext[] = $_LANG['NOT_YOUNG']." $age_fr ".$_LANG['YEARS'];
	}
	// Добавляем в выборку возраст, менее
	if($age_to){
		$model->whereAgeTo($age_to);
		$stext[] = $_LANG['NOT_OLD']." $age_fr ".$_LANG['YEARS'];
	}

	// Считаем общее количество согласно выборки
	$total = $model->getUsersCount($only_online);

	if($total){

		//устанавливаем сортировку
		$inDB->orderBy($orderby, $orderto);

		//устанавливаем номер текущей страницы и кол-во пользователей на странице
		$inDB->limitPage($page, $model->config['users_perpage']);

		// Загружаем пользователей согласно выборки
		$users = $model->getUsers($only_online);

	} else {
        $inDB->resetConditions();
    }
	
	
	
	

	$link['latest']   = '/users';
	$link['positive'] = '/users/positive.html';
	$link['rating']   = '/users/rating.html';

	if($orderby=='regdate') { $link['selected'] = 'latest'; }
	if($orderby=='karma') { $link['selected'] = 'positive'; }
	if($orderby=='rating') { $link['selected'] = 'rating'; }

	cmsPage::initTemplate('components', 'com_users_view')->
            assign('stext', $stext)->
            assign('orderby', $orderby)->
            assign('orderto', $orderto)->
            assign('users', $users)->
            assign('total', $total)->
            assign('only_online', $only_online)->
            assign('gender', $gender)->
            assign('name', stripslashes($name))->
            assign('city', stripslashes($city))->
            assign('hobby', stripslashes($hobby))->
            assign('age_to', $age_to)->
            assign('age_fr', $age_fr)->
            assign('cfg', $model->config)->
            assign('link', $link)->
            assign('pagebar', cmsPage::getPagebar($total, $page, $model->config['users_perpage'], '/users/'.$link['selected'].'%page%.html'))->
            display('com_users_view.tpl');

}

//============================================================================//
//======================= Редактирование профиля  ============================//
//============================================================================//
if ($do=='editprofile'){

	// неавторизованным, не владельцам и не админам тут делать нечего
	if (!$inUser->id || ($inUser->id != $id && !$inUser->is_admin)){ cmsCore::error404(); }

    $usr = $model->getUser($id);
    if (!$usr){ cmsCore::error404(); }
	$opt = cmsCore::request('opt', 'str', 'edit');
    // главного админа может редактировать только он сам
    if($id == 1 && $inUser->id != $id){
        cmsCore::error404();
    }
	$myprofile = ($inUser->id);
	$files = $model->getUserFiles($myprofile);
	if (!$files)
	{
		for($i=0;$i<=3;$i++)
		{
			$sql12 = "INSERT cms_user_files(type,user_id) VALUES ('{$i}','{$inUser->id}')";
				$inDB->query($sql12);
		}
	}
	
	
	
	// показываем форму
	if ($opt == 'edit'){
	
		$inPage->setTitle($_LANG['CONFIG_PROFILE'].' - '.$usr['nickname']);
		$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
		$inPage->addPathway($_LANG['CONFIG_PROFILE']);

		$private_forms = array();
		if(isset($model->config['privforms'])){
			if (is_array($model->config['privforms'])){
				foreach($model->config['privforms'] as $form_id){
					$private_forms = array_merge($private_forms, cmsForm::getFieldsHtml($form_id, $usr['formsdata']));
				}
			}
		}
		
		/*		
		$findsql111 = "SELECT * FROM vds_user_profiles WHERE id = '$id'";
		$result1111 = $inDB->query($findsql111) ;
		$items = array();
		while($item1111 = $inDB->fetch_assoc($result1111))
		{
			$content_list111[] = $item1111;
		}
		*/
		
		
		/*--------------- выборка с таблицы виды платежных данных ----------------*/
		
		$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1'");
		foreach($payment_sustem as $key => $payment_s){
			$payment['id'] = $payment_s['id'];
			$payment['name'] = $payment_s['name_sustem'];
			$payment['before_score'] = $payment_s['before_score'];
			$payment['number_score'] = $payment_s['number_score'];
			$payment['after_score'] = $payment_s['after_score'];
			$payment['img'] = $payment_s['min_img'];
			$payment['select'] = $payment_s['on_off'];
			$payments[$payment['id']] = $payment;
		}
		
		$kol_elements = count($payments);
				
		$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$id'");
		foreach($table_inf_money as $key => $inf_money_s){
			$inf_money['id'] = $inf_money_s['type_payment_id'];
			$inf_money['score_num'] = $inf_money_s['score_num'];  
			$inf_money['score_val'] = $inf_money_s['score_val'];
			$inf_money['score_in'] = $inf_money_s['score_in'];
			$inf_money['score_out'] = $inf_money_s['score_out'];
			$inf_money['num_payment'] = $inf_money_s['num_payment'];
			$inf_money['status'] = $inf_money_s['status'];
			$inf_moneys[$inf_money['id']] = $inf_money;
		}
		$style_cooke = $inDB->get_field('cms_users', "id='$id'", 'color_system');
		if($style_cooke == '0') $style_cooke = 'dark';
		elseif($style_cooke == '1') $style_cooke = 'blue';
		elseif($style_cooke == '2') $style_cooke = 'green';
		//var_dump($payments);
		//$sdfsdfsd = cmsDatabase::getInstance()->get_field('cms_cache', $where, 'cachefile');
		
		cmsPage::initTemplate('components', 'com_users_edit_profile')->
                assign('opt', $opt)->
				assign('style_cooke', $style_cooke)->
				assign('payments', $payments)->
				assign('inf_moneys', $inf_moneys)->
                assign('usr', $usr)->
				assign('usr2', $usr_2)->
                assign('results_file', $files)->
                assign('private_forms', $private_forms)->
                assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
                assign('cfg', $model->config)->
                display('com_users_edit_profile.tpl');
		return;

	}
	if($opt == 'form_upload_photo'){
		$x1 = cmsCore::request('x1','int');
		$x2 = cmsCore::request('x2','int');
		$y1 = cmsCore::request('y1','int');
		$y2 = cmsCore::request('y2','int');
		$directory_img = cmsCore::request('file_name','str');
		$directory_img = substr($directory_img,1);
		list($width, $height) = getimagesize($directory_img);
		$new_width = $x2-$x1 ;
		$new_height = $y2-$y1 ;	
		// ресэмплирование
		$image_p = imagecreatetruecolor($new_width, $new_height);
		$type_img = explode('.',$directory_img);
		$perem3 = end($type_img);
		if($perem3 != 'png'){
			$image = imagecreatefromjpeg($directory_img);
		}
		else{
			$image = imagecreatefrompng($directory_img);
		}
		imagecopyresampled($image_p, $image, 0, 0, $x1, $y1, $new_width, $new_height, $new_width, $new_height);
		// вывод
		$perem1 = explode('/',$_POST['file_name']);
		$perem2 = end($perem1);
		if($perem3 != 'png'){
			imagejpeg($image_p, "images/users/avatars/small/".$perem2, 100);
		}
		else{
			imagepng($image_p, "images/users/avatars/small/".$perem2, 0);
		}
		
		$inDB->query("UPDATE cms_user_profiles SET imageurl = '$perem2' WHERE user_id = '".$inUser->id."' LIMIT 1");
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#changes_avatar');
	}
	// Если сохраняем профиль
	if ($opt == 'save'){
	cmsCore::loadClass('upload_photo');
	
	if(isset($_COOKIE['style'])){
		$style = array();
		$style[0]['title'] = "dark";
		$style[0]['color'] = "0";
		$style[1]['title'] = "blue";
		$style[1]['color'] = "1";
		$style[2]['title'] = 'green';
		$style[2]['color'] = "2";
		
		foreach($style as $styles){
			if($styles['title'] == $_COOKIE['style']){
				$inDB->query("UPDATE cms_users SET color_system='".$styles['color']."' WHERE id='$id'");
			}
		}
	}
	else{
		$inDB->query("UPDATE cms_users SET color_system='0' WHERE id='$id'");
	}
$i = 0;
	
		 foreach ($files as $file1){

		$inUploadPhoto = cmsUploadPhoto::getInstance();
		// Выставляем конфигурационные параметры
		$inUploadPhoto->upload_dir    = PATH.'/images/';
		$inUploadPhoto->dir_medium    = 'users/docs/medium/';
		$inUploadPhoto->dir_small     = 'users/docs/small/';
		$inUploadPhoto->dir_large     = 'users/docs/large/';
		$inUploadPhoto->small_size_w  = $model->config['smallw'];
		$inUploadPhoto->medium_size_w = $model->config['medw'];
		$inUploadPhoto->large_size_w = 	800;
		$inUploadPhoto->medium_size_h = $model->config['medh'];
		$inUploadPhoto->is_watermark  = false;
		$inUploadPhoto->input_name    ="passport{$i}";
		
		$file = $inUploadPhoto->uploadPhoto1();
		if ($file){
			$sql = "UPDATE cms_user_files SET filename = '{$file['filename']}' WHERE user_id ='{$inUser->id}' AND type ='{$i}' ";
		$inDB->query($sql);
}
		$i++;
}		
		$photo1 = cmsCore::request('del_photo_1', 'str');
		if($photo1 == '1'){
			$sql = "UPDATE cms_user_files SET filename = '' WHERE user_id ='{$inUser->id}' AND type ='0' ";
			$inDB->query($sql);
		}
		
		$photo2 = cmsCore::request('del_photo_2', 'str');
		if($photo2 == '1'){
			$sql = "UPDATE cms_user_files SET filename = '' WHERE user_id ='{$inUser->id}' AND type ='1' ";
			$inDB->query($sql);
		}
		$photo3 = cmsCore::request('del_photo_3', 'str');
		if($photo3 == '1'){
			$sql = "UPDATE cms_user_files SET filename = '' WHERE user_id ='{$inUser->id}' AND type ='2' ";
			$inDB->query($sql);
		}
		/* PHOTO */
		
		$errors = false;
		
		$users['first_name'] = cmsCore::request('first_name', 'str');
		$users['old_name'] = cmsCore::request('old_name', 'str');
		$users['gender_user'] = cmsCore::request('your_gender', 'int');
		$users['skype'] = cmsCore::request('your_skype', 'str');
		$users['visible_login'] = cmsCore::request('see_nik_name', 'str');
		$users['code_phone'] = cmsCore::request('your_phone_code', 'str');
		$users['mobtelephone'] = cmsCore::request('your_phone', 'str');
		
		$users['nickname'] = cmsCore::request('nickname', 'str');
		if (mb_strlen($users['nickname'])<2) {
			//cmsCore::addSessionMessage($_LANG['SHORT_NICKNAME'], 'error'); $errors = true;
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#SHORT_NICKNAME');			
		}
		cmsCore::loadModel('registration');
		$modreg = new cms_model_registration();
		if (!$inUser->is_admin){
			if($modreg->getBadNickname($users['ERR_NICK_EXISTS'])) {
				//cmsCore::addSessionMessage($_LANG['ERR_NICK_EXISTS'], 'error'); $errors = true;
				cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#error_old_pass');				
			}
		}

		$profiles['gender'] = cmsCore::request('gender', 'str');
		$profiles['city']   = cmsCore::request('your_city', 'str');
		
		/*$sql = "UPDATE cms_user_profiles SET country='".$profiles['country']."' WHERE id = '$id' ";
		$inDB->query($sql);*/
		
		if (mb_strlen($profiles['city'])>50) {
			//cmsCore::addSessionMessage($_LANG['LONG_CITY_NAME'], 'error'); $errors = true;
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#LONG_CITY_NAME');
		}

		$users['email'] = cmsCore::request('email', 'email');
		if (!$users['email']) {
			//cmsCore::addSessionMessage($_LANG['REALY_ADRESS_EMAIL'], 'error'); $errors = true; 
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#REALY_ADRESS_EMAIL');
		}
		if($usr['email'] != $users['email']){
			$is_set_email = $inDB->get_field('cms_users', "email='{$users['email']}'", 'id');
			if ($is_set_email) {
				//cmsCore::addSessionMessage($_LANG['ADRESS_EMAIL_IS_BUSY'], 'error'); $errors = true; 
				cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#ADRESS_EMAIL_IS_BUSY');
			}
		}

		$profiles['showmail']     = cmsCore::request('showmail', 'int');
		$profiles['email_newmsg'] = cmsCore::request('email_newmsg', 'int');
		$profiles['showbirth']    = cmsCore::request('showbirth', 'int');
		$profiles['description']  = cmsCore::request('description', 'str', '');

		$users['birthdate']    = (int)$_REQUEST['birthdate']['year'].'-'.(int)$_REQUEST['birthdate']['month'].'-'.(int)$_REQUEST['birthdate']['day'];
        $profiles['signature']      = $inDB->escape_string(cmsCore::request('signature', 'html', ''));
        $profiles['signature_html'] = $inDB->escape_string(cmsCore::parseSmiles(cmsCore::request('signature', 'html', ''), true));
		
			
		$users['icq']             = cmsCore::request('icq', 'str', '');
		$profiles['showicq']      = cmsCore::request('showicq', 'int');
		$profiles['cm_subscribe'] = cmsCore::request('cm_subscribe', 'str');
		
		$profiles['country'] = cmsCore::request('your_country', 'str');
		$profiles['street'] = cmsCore::request('your_street', 'str');
		$profiles['home'] = cmsCore::request('your_home', 'str');
		$profiles['flat'] = cmsCore::request('your_flat', 'str');
		$profiles['index_user'] = cmsCore::request('your_index', 'str');
		$profiles['passport_series'] = cmsCore::request('passport_series', 'str');
		$profiles['passport_number'] = cmsCore::request('passport_number', 'str');
		$profiles['passport_date'] = cmsCore::request('passport_date', 'str');
		$profiles['code_region'] = cmsCore::request('code_region', 'int');
		$profiles['issued_by'] = cmsCore::request('issued_by', 'str');
		/*--------------------данные карты-----------------*/
		$profiles['card_number'] = cmsCore::request('card_number', 'int');
		$profiles['date_card'] = cmsCore::request('date_card', 'str');
		
		$user_score = cmsCore::request("peyment", 'array');
		//var_dump($user_score);
		//$asdasdasd = "DELETE FROM cms_user_score WHERE user_id='$id'";
		$inDB->query($asdasdasd);
		for($i=1;$i<=$kol_elements;$i++)
		{
			
		}
		foreach($user_score as $key => $user_scores){
			/*$sql112 = "INSERT cms_user_score(user_id,type_payment_id,score_num, status) VALUES ('$id','$key','$user_scores','1')";
				$inDB->query($sql112);*/
			$inDB->query("UPDATE cms_user_score SET score_num='$user_scores' WHERE user_id='".$inUser->id."' AND type_payment_id='$key'");
		}
		
		
		
		//
		
		/*-------------------------------соц сети-------------------------*/
		$profiles['odnokl'] = cmsCore::request('od', 'str');
		$profiles['g_plus'] = cmsCore::request('g_plus', 'str');
		$profiles['vk'] = cmsCore::request('vk', 'str');
		$profiles['tviter'] = cmsCore::request('tv', 'str');
		$profiles['fasebook'] = cmsCore::request('fb', 'str');
		$profiles['mail'] = cmsCore::request('mail', 'str');
		
		/* if (!preg_match('/^([a-zA-Z]+)$/ui', $profiles['cm_subscribe'])) { $errors = true; } */

		// получаем данные форм
		$profiles['formsdata'] = '';
		if(isset($model->config['privforms'])){
			if (is_array($model->config['privforms'])){
				foreach($model->config['privforms'] as $form_id){
					$form_input  = cmsForm::getFieldsInputValues($form_id);
					$profiles['formsdata'] .= $inDB->escape_string(cmsCore::arrayToYaml($form_input['values']));
					// Проверяем значения формы
					foreach ($form_input['errors'] as $field_error) {
						if($field_error){
							//cmsCore::addSessionMessage($field_error, 'error'); $errors = true;
							cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#error_form');
						}
					}
				}
			}
		}

		if($errors) { cmsCore::redirectBack(); }

		$inDB->update('cms_user_profiles', cmsCore::callEvent('UPDATE_USER_PROFILES', $profiles), $usr['pid']) ;
		$inDB->update('cms_users', cmsCore::callEvent('UPDATE_USER_USERS', $users), $usr['id']) ;
		
		if($inDB->get_field("cms_user_files","user_id='".$usr['id']."' AND type='0'","filename") != "" && $inDB->get_field("cms_users","id='".$usr['id']."'","verify_stat") < 1)	$sql = $inDB->query("UPDATE cms_users SET verify_stat = '1' WHERE id ='".$usr['id']."'");
		
		$model->EventHistory($usr['id'], "Изменение профиля");
						
		//$inDB->update('cms_users', cmsCore::callEvent('UPDATE_USER_USERS', $users), $usr['id']) ;
		//cmsCore::addSessionMessage($_LANG['PROFILE_SAVED'], 'info');
		//cmsCore::redirect(cmsUser::getProfileURL($usr['login']));
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#PROFILE_SAVED');

	}

	if ($opt == 'changepass'){

		$errors = false;

		$oldpass  = cmsCore::request('oldpass', 'str');
		$newpass  = cmsCore::request('newpass', 'str');
		$newpass2 = cmsCore::request('newpass2', 'str');
		

		if ($inUser->password != md5($oldpass)) {
			//cmsCore::addSessionMessage($_LANG['OLD_PASS_WRONG'], 'error'); $errors = true;
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#error_old_pass');
			}
		if ($newpass != $newpass2) { 
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#WRONG_PASS');
		//cmsCore::addSessionMessage($_LANG['WRONG_PASS'], 'error'); $errors = true; 
		}
		if($oldpass && $newpass && $newpass2 && mb_strlen($newpass )<6) {
			cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#PASS_SHORT');
			//cmsCore::addSessionMessage($_LANG['PASS_SHORT'], 'error'); $errors = true; 
			}

		if($errors) { cmsCore::redirectBack(); }

        cmsCore::callEvent('UPDATE_USER_PASSWORD', array('user_id'=>$usr['id'], 'oldpass'=>$oldpass, 'newpass'=>$newpass));
		
		$sql = "UPDATE cms_users SET password='".md5($newpass)."' WHERE id = '$id' AND password='".md5($oldpass)."'";
		$inDB->query($sql);
		//cmsCore::addSessionMessage($_LANG['PASS_CHANGED'], 'info');
		//cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
		
		$model->EventHistory($inUser->id, "Изменение пароля");
		
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#PASS_CHANGED');

	}
	if ($opt == 'protection_user'){

		$errors = false;
		$protection_user = cmsCore::request('protection_user', 'int');
		
		
		$sql2 = "UPDATE cms_users SET protection_user='$protection_user' WHERE id = '$id' ";
		$inDB->query($sql2);
		
		//cmsCore::addSessionMessage($_LANG['PROTECTION_USER'], 'info');
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#PROTECTION_USER');
		
	}
	
	if ($opt == 'avatarka'){
		
			if (!$inUser->id || ($inUser->id && $inUser->id != $id)){ cmsCore::error404(); }

			$inPage->setTitle($_LANG['LOAD_AVATAR']);
			$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
			$inPage->addPathway($_LANG['LOAD_AVATAR']);
			
			if (cmsCore::inRequest('upload')) {

				cmsCore::loadClass('upload_photo');
				$inUploadPhoto = cmsUploadPhoto::getInstance();
				// Выставляем конфигурационные параметры
				$inUploadPhoto->upload_dir    = PATH.'/images/';
				$inUploadPhoto->dir_medium    = 'users/avatars/';
				$inUploadPhoto->dir_small     = 'users/avatars/small/';
				$inUploadPhoto->small_size_w  = $model->config['smallw'];
				$inUploadPhoto->medium_size_w = $model->config['medw'];
				$inUploadPhoto->medium_size_h = $model->config['medh'];
				$inUploadPhoto->is_watermark  = false;
				$inUploadPhoto->input_name    = 'picture';

				$file = $inUploadPhoto->uploadPhoto($inUser->orig_imageurl);
				if(!$file){

					//cmsCore::addSessionMessage('<strong>'.$_LANG['ERROR'].':</strong> '.cmsCore::uploadError().'!', 'error');
					cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#ERROR');

				}

				$sql = "UPDATE cms_user_profiles SET imageurl = '{$file['filename']}' WHERE user_id = '$id' LIMIT 1";
				$inDB->query($sql);
				// очищаем предыдущую запись о смене аватара
				cmsActions::removeObjectLog('add_avatar', $id);
				// выводим сообщение в ленту
				cmsActions::log('add_avatar', array(
					  'object' => '',
					  'object_url' => '',
					  'object_id' => $id,
					  'target' => '',
					  'target_url' => '',
					  'description' => '<a href="'.cmsUser::getProfileURL($inUser->login).'" class="act_usr_ava">
										   <img border="0" src="/images/users/avatars/small/'.$file['filename'].'">
										</a>'
				));

				//cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
				
				$model->EventHistory($inUser->id, "Изменение аватарки");
				
				cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#changes_avatar');

			}
		
	}

}


if ($do=='administration'){

	// неавторизованным, не владельцам и не админам тут делать нечего
	if (!$inUser->id || $inUser->id != 1){ cmsCore::error404(); }

    $usr = $model->getUser($id);
    if (!$usr){ cmsCore::error404(); }
	$opt = cmsCore::request('opt', 'str', 'edit');
    // главного админа может редактировать только он сам
    if($id == 1 && $inUser->id != $id){
        cmsCore::error404();
    }
	$myprofile = ($inUser->id);
	$files = $model->getUserFiles($myprofile);
	if (!$files)
	{
		for($i=0;$i<=3;$i++)
		{
			$sql12 = "INSERT cms_user_files(type,user_id) VALUES ('{$i}','{$inUser->id}')";
				$inDB->query($sql12);
		}
	}
	// показываем форму
	
		$inPage->setTitle($_LANG['ADMINISTRATION'].' - '.$usr['nickname']);
		$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
		$inPage->addPathway($_LANG['CONFIG_PROFILE']);

		$private_forms = array();
		if(isset($model->config['privforms'])){
			if (is_array($model->config['privforms'])){
				foreach($model->config['privforms'] as $form_id){
					$private_forms = array_merge($private_forms, cmsForm::getFieldsHtml($form_id, $usr['formsdata']));
				}
			}
		}
		
//--------------------------------------  вкладка платежные данные	-----------------------------------------		
		$payment_sustem = $inDB->get_table("cms_user_type_payment","status='0'");
		foreach($payment_sustem as $key => $payment_s){
			$payment['id'] = $payment_s['id'];
			$payment['before_score'] = $payment_s['before_score'];
			$payment['number_score'] = $payment_s['number_score'];
			$payment['after_score'] = $payment_s['after_score'];
			$payment['name'] = $payment_s['name_sustem'];
			$payment['money_admin'] = $payment_s['payment_admin'];
			$payment['img'] = $payment_s['min_img'];
			$payment['select'] = $payment_s['on_off'];
			$payment['tax_money'] = $payment_s['tax_money'];
			$payment['relations'] = substr($payment_s['relations'], 1, -1);
			$payments[$payment['id']] = $payment;
		}
		
		$kol_elements = count($payments);
				
		$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$id'");
		foreach($table_inf_money as $key => $inf_money_s){
			$inf_money['id'] = $inf_money_s['type_payment_id'];
			$inf_money['score_num'] = $inf_money_s['score_num'];
			$inf_money['score_val'] = $inf_money_s['score_val'];
			$inf_money['score_in'] = $inf_money_s['score_in'];
			$inf_money['score_out'] = $inf_money_s['score_out'];
			$inf_money['num_payment'] = $inf_money_s['num_payment'];
			$inf_money['status'] = $inf_money_s['status'];
			$inf_moneys[$inf_money['id']] = $inf_money;
		}
		
		$table_inf_system = $inDB->get_table("cms_user_score", "user_id='$id'");
		
		//var_dump($payments);
		//$sdfsdfsd = cmsDatabase::getInstance()->get_field('cms_cache', $where, 'cachefile');
//--------------------------------------  вкладка виды номеров ПС	-----------------------------------------		
		$payment_sustem = $inDB->get_table("cms_user_type_payment","status='0'");
		foreach($payment_sustem as $key => $payment_s){
			$payment['id'] = $payment_s['id'];
			$payment['name'] = $payment_s['name_sustem'];
			$payment['before_score'] = $payment_s['before_score'];
			$payment['number_score'] = $payment_s['number_score'];
			$payment['after_score'] = $payment_s['after_score'];
			$payment['img'] = $payment_s['min_img'];
			$payment['select'] = $payment_s['on_off'];
			$payment['tax_money'] = $payment_s['tax_money'];
			$payment['relations'] = substr($payment_s['relations'], 1, -1);
			$payments2[$payment['id']] = $payment;
		}
		
		$kol_elements2 = count($payments2);
				
		$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$id'");
		foreach($table_inf_money as $key => $inf_money_s){
			$inf_money['id'] = $inf_money_s['type_payment_id'];
			$inf_money['score_num'] = $inf_money_s['score_num'];
			$inf_money['score_val'] = $inf_money_s['score_val'];
			$inf_money['score_in'] = $inf_money_s['score_in'];
			$inf_money['score_out'] = $inf_money_s['score_out'];
			$inf_money['num_payment'] = $inf_money_s['num_payment'];
			$inf_money['status'] = $inf_money_s['status'];
			$inf_moneys2[$inf_money['id']] = $inf_money;
		}
		

		//var_dump($payments2);
		//$sdfsdfsd = cmsDatabase::getInstance()->get_field('cms_cache', $where, 'cachefile');
//-------------------------------------- вкладка  Связь ПС	---------------------------------------
	$table_relation_pc = $inDB->get_table('cms_user_relation_pc','','system');
	if($table_relation_pc){
		
		foreach($table_relation_pc as $result){
			$infa_id[] = $result['system'];
		}
		
		$perem112 = array_unique($infa_id);
		foreach($perem112 as $result){
			$inf_sys = $inDB->get_table('cms_user_relation_pc', "system='$result'");
			foreach($inf_sys as $result2){
				$infa['relation'] = $result2['relation_syst'];
				$relation = $inDB->get_fields('cms_user_type_payment','id="'.$result2['relation_syst'].'" AND status="0"','name_sustem, min_img');
				$infa['relation_name'] = $relation['name_sustem'];
				$infa['relation_img'] = $relation['min_img'];
				$infa['kurs'] = $result2['kurs'];
				$infa['commission'] = $result2['commission'];
				$infa['min_money'] = $result2['min_money'];
				$infa['status'] = $result2['status'];
				if($relation){
					$inf_relations[$result2['relation_syst']] = $infa;
				}
				unset($relation);
			}
			$inf_relation[$result] = $inf_relations;
			unset($inf_relations);
		}
		//var_dump($inf_relation);
	}
	else{
		$inDB->query("INSERT cms_user_relation_pc (system,status) VALUES ('0','1')");
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html');
	}
/*======================================================================================================*/
	$multiplier = 10; // - кількість записів
//--------------------------------------  вкладка верификации	----------------------------------------------
	
	//$table_users = $inDB->get_table("cms_users");
	if(!$_GET['verify']){$_GET['verify'] = 0;}
	$qqqqqw = $inDB->get_table("cms_users","is_locked='0'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	$pagination_verif .= "<div class='pagination'>";
	if($_GET['verify'] == 0){
		$pagination_verif .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=0#verification'><<</a>";
	}
	if($fffg > 8){
		if($_GET['verify'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['verify'] == $i){
					$pagination_verif .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$i#verification'>$number</a>";
				}
			}
			$pagination_verif .= "<span class='' >..</span>";
			$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['verify'] > 4 && $_GET['verify'] < $tttt-4){
			$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=0#verification'>1</a>";
			$pagination_verif .= "<span class='' >..</span>";
			for($i=$_GET['verify']-2; $i<$_GET['verify']+2; $i++){
				$number = $i+1;
				if($_GET['verify'] == $i){
					$pagination_verif .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$i#verification'>$number</a>";
				}
			}
			$pagination_verif .= "<span class='' >..</span>";
			$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['verify'] > $tttt-4){
			$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=0#verification'>1</a>";
			$pagination_verif .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['verify'] == $i){
					$pagination_verif .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$i#verification'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['verify'] == $i){
				$pagination_verif .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$i#verification'>$number</a>";
			}
		}
	}
	if($_GET['verify'] == $tttt){
		$pagination_verif .= "<a class='active' >>></a>";
	}
	else{
		$pagination_verif .= "<a href='/users/".$inUser->id."/administration.html?verify=$tttt#verification'>>></a>";
	}
	$pagination_verif .= "</div>";
	if($fffg == 1) $pagination_verif = "";
	//var_dump($col_elem/$multiplier);
	$aaaas = "SELECT * FROM cms_users WHERE is_locked='0' LIMIT ".$_GET['verify']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_users[] = $item1;
	}
	foreach($table_users as $key => $result){
		$user['id'] = $result['id'];
		$user['login'] = $result['login'];
		$user['name'] = $result['nickname'];
		$user['verif_1'] = $inDB->get_field("cms_user_files","user_id='".$user['id']."' AND type='0'","filename");
		$user['verif_2'] = $inDB->get_field("cms_user_files","user_id='".$user['id']."' AND type='1'","filename");
		$user['verif_3'] = $result['verify_phone'];
		$user['verif_4'] = $result['verify_video'];
		$user['verify_stat'] = $result['verify_stat'];
		
		$users[] = $user;
	}
	//var_dump($users);
	
//--------------------------------------  вкладка скидки	----------------------------------------------
	
	//$table_users = $inDB->get_table("cms_users");
	if(!$_GET['discount']){$_GET['discount'] = 0;}
	$qqqqqw = $inDB->get_table("cms_users","is_locked='0'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	$pagination_discount .= "<div class='pagination'>";
	if($_GET['discount'] == 0){
		$pagination_discount .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=0#discounts'><<</a>";
	}
	if($fffg > 8){
		if($_GET['discount'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['discount'] == $i){
					$pagination_discount .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$i#discounts'>$number</a>";
				}
			}
			$pagination_discount .= "<span class='' >..</span>";
			$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$tttt#discounts'>$tttt</a>";
		}
		elseif($_GET['discount'] > 4 && $_GET['discount'] < $tttt-4){
			$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=0#discounts'>1</a>";
			$pagination_discount .= "<span class='' >..</span>";
			for($i=$_GET['discount']-2; $i<$_GET['discount']+2; $i++){
				$number = $i+1;
				if($_GET['discount'] == $i){
					$pagination_discount .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$i#discounts'>$number</a>";
				}
			}
			$pagination_discount .= "<span class='' >..</span>";
			$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$tttt#discounts'>$tttt</a>";
		}
		elseif($_GET['discount'] > $tttt-4){
			$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=0#discounts'>1</a>";
			$pagination_discount .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['discount'] == $i){
					$pagination_discount .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$i#discounts'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['discount'] == $i){
				$pagination_discount .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$i#discounts'>$number</a>";
			}
		}
	}
	if($_GET['discount'] == $tttt){
		$pagination_discount .= "<a class='active' >>></a>";
	}
	else{
		$pagination_discount .= "<a href='/users/".$inUser->id."/administration.html?discount=$tttt#discounts'>>></a>";
	}
	$pagination_discount .= "</div>";
	if($fffg == 1) $pagination_discount = "";
	//var_dump($col_elem/$multiplier);
	$aaaas = "SELECT * FROM cms_users WHERE is_locked='0' LIMIT ".$_GET['discount']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_discount[] = $item1;
	}
	foreach($table_discount as $key => $result){
		$discount['id'] = $result['id'];
		$discount['login'] = $result['login'];
		$discount['name'] = $result['nickname'];
		$discount['first_name'] = $result['first_name'];
		$discount['email'] = $result['email'];
		
		$discounts[] = $discount;
	}
	//var_dump($users);
//--------------------------------------  вкладка пользовательские ПС ------------------------------------------
	
	//$table_users = $inDB->get_table("cms_users");
		
	$table_user_pc = $inDB->get_table('cms_user_requests_for_payment','name_system!="0"','id_paym_syst,name_system');
	
	$i=1;
	$vrem_masive = array();
	foreach($table_user_pc as $key => $result){$prov = 0;
		foreach($vrem_masive as $result2){ if($result2 == $result['id_paym_syst'])$prov = 1; }
		if($prov == 0){
			$user_pc['id'] = $i;
			$user_pc['id_syst'] = $result['id_paym_syst'];
			$user_pc['name_syst'] = $result['name_system'];
			$user_pc['image'] = $inDB->get_field('cms_user_image_pc','id_paym_syst="'.$result['id_paym_syst'].'"','image');
			
			$i++;
			$users_pc[] = $user_pc;
		}
	}
	//var_dump($users_pc);
//--------------------------------------  вкладка реферальные начисления ------------------------------------------
	
	//$table_users = $inDB->get_table("cms_users");
		
	$table_referal = $inDB->get_table('cms_preferal_accrued');
	
	foreach($table_referal as $key => $result){
		$referal['id'] = $resule['id'];
		$referal['operation'] = $result['name_operation'];
		$referal['proc'] = $result['procent'];
		$referal['status'] = $result['status'];
		$referals[] = $referal;
	}
	//var_dump($users_pc);
	
//--------------------------------------   вкладка истории    -------------------------------------------------	

	//$table_history = $inDB->get_table("cms_user_event_history", "status='1'");
	if(!$_GET['history']){$_GET['history'] = 0;}
	$qqqqqw = $inDB->get_table("cms_user_event_history", "status='1'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	
	$pagination_hist .= "<div class='pagination'>";
	if($_GET['history'] == 0){
		$pagination_hist .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=0#history'><<</a>";
	}
	if($fffg > 8){
		if($_GET['history'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['history'] == $i){
					$pagination_hist .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$i#history'>$number</a>";
				}
			}
			$pagination_hist .= "<span class='' >..</span>";
			$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$tttt#history'>$tttt</a>";
		}
		elseif($_GET['history'] > 4 && $_GET['history'] < $tttt-4){
			$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=0#history'>1</a>";
			$pagination_hist .= "<span class='' >..</span>";
			for($i=$_GET['history']-2; $i<$_GET['history']+2; $i++){
				$number = $i+1;
				if($_GET['history'] == $i){
					$pagination_hist .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$i#history'>$number</a>";
				}
			}
			$pagination_hist .= "<span class='' >..</span>";
			$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$tttt#history'>$tttt</a>";
		}
		elseif($_GET['history'] > $tttt-4){
			$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=0#history'>1</a>";
			$pagination_hist .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['history'] == $i){
					$pagination_hist .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$i#history'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['history'] == $i){
				$pagination_hist .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$i#history'>$number</a>";
			}
		}
	}
	if($_GET['history'] == $tttt){
		$pagination_hist .= "<a class='active' >>></a>";
	}
	else{
		$pagination_hist .= "<a href='/users/".$inUser->id."/administration.html?history=$tttt#history'>>></a>";
	}
	$pagination_hist .= "</div>";
	if($fffg == 1) $pagination_hist = "";
	$aaaas = "SELECT * FROM cms_user_event_history WHERE status='1' LIMIT ".$_GET['history']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_history[] = $item1;
	}
	foreach($table_history as $key => $results){
		$history['id'] = $results['id'];
		$history['user_login'] = $inDB->get_field("cms_users","id='".$results['user_id']."'","login");
		$history['time'] = $results['date_event'];
		$history['event'] = $results['name_event'];
		$history['seconds'] = strtotime($results['date_event']);
		$history_s[] = $history;
	}
	//var_dump($history_s);
//--------------------------------------   вкладка отчет    -------------------------------------------------	

	//$table_history = $inDB->get_table("cms_user_event_history", "status='1'");
	if($_GET['search_id']){ $search_id = $_GET['search_id']; }
	if($_GET['search_name']){ $search_name = $_GET['search_name'] ;}
	if($_GET['search_name_pc']){ $search_name_pc = $_GET['search_name_pc']; }
	if($_GET['search_money']){ $search_money = $_GET['search_money'] ;}
	if($_GET['search_date']){ $search_date = $_GET['search_date'] ;}
	if($_GET['search_com_pc']){ $search_com_pc = $_GET['search_com_pc'] ; }
	if($_GET['search_com_ser']){ $search_com_ser = $_GET['search_com_ser'] ;}
	if($_GET['search_com_ot']){ $search_com_ot = $_GET['search_com_ot'] ;}
	
	
	/*$aaaas = "SELECT * FROM cms_user_reports WHERE status='1' LIMIT ".$_GET['report']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{*/
		$table_report = $inDB->get_table('cms_user_reports','status="1"');
	//}
	foreach($table_report as $key => $results){
		$report['id'] = $results['id'];
		$report['user_login'] = $inDB->get_field("cms_users","id='".$results['user_id']."'","login");
		$report['name_pc'] = $inDB->get_field('cms_user_type_payment', 'id="'.$results['currency'].'"','name_sustem');
		if(!$inDB->get_field('cms_user_type_payment', 'id="'.$results['currency'].'"','name_sustem')){
			$report['name_pc'] = $inDB->get_field('cms_requests_for_payment','id_paym_syst="'.$results['currency'].'"','name_system');
		}
		$users_infa = $inDB->get_fields("cms_users","id='".$results['user_id']."'","*");
		$profile_infa = $inDB->get_fields("cms_user_profiles", "user_id='".$results['user_id']."'", 'imageurl, odnokl, g_plus, vk, tviter, fasebook, mail, country');
		if($profile_infa['imageurl'] == "") $image_user = "nopic.jpg";
		else $image_user = $profile_infa['imageurl'];
		//var_dump($image_user);
		$social_cets .= "<div class='sotc_ceti_s'>";
		
		$odnokl_cet = "";
		$g_plus_cet = "";
		$vk_cet = "";
		$tviter_cet = "";
		$fasebook_cet = "";
		$mail_cet = "";
		
		if($profile_infa['odnokl'] != ""){ $odnokl_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['g_plus'] != ""){ $g_plus_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['vk'] != ""){ $vk_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -149px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['tviter'] != ""){ $tviter_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -207px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['fasebook'] != ""){ $fasebook_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['mail'] != ""){ $mail_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -90px -127px; width:50px; height:50px; float:left;'></div>";}
		
		/*$user_name = $inDB->get_field("cms_users", "id='".$results['user_id']."'", 'nickname');
		if($user_id == $results['user_id']){
			$jjjjjj = "<a style='cursor:default; opacity:0.5;' class='button_svaz' >Связаться</a>";
		}
		else{
			$jjjjjj = "<a href='javascript:void(0)' class='button_svaz' onclick='users.sendMess(".$results['user_id'].", 0, this);return false;' title='Новое сообщение: ".$user_name."'>Связаться</a>";
		}*/
		$content = "";
		if($results['description'] != ""){
			$content = $results['description'];
		}
		
		$report['div'] = "
			<div style='width: 100%; height: 54px;'>
				<img  style='float:left; margin-right: 15px;width: 55px; max-height: 55px;' src='/images/users/avatars/small/".$image_user."'>
				<div style='height: 52px;float: left;'>
				".$odnokl_cet.$g_plus_cet.$vk_cet.$tviter_cet.$fasebook_cet.$mail_cet."
				</div>
			</div>
			<div style='text-align: left;margin-top: 6px;'>
				<div>".$users_infa['nickname']."</div>
				<div><span style='color: gray;'>ID пользователя: </span>".$results['user_id']."</div>
				<div><span style='color: gray;'>Дата регистрации: </span>".$users_infa['regdate']."</div>
				<div style='max-width:400px;'><span style='color: gray;'>Описание события: </span>".$content."</div>
			</div>";
		$report['money'] = round($results['money'], 2);
		$report['date'] = date('d.m.Y', strtotime($results['date']));
		$report['commission_pc'] = round($results['commission_pc'], 2);
		$report['commission_serv'] = round($results['commission_serv'], 2);
		$report['commission_partner'] = round($results['commission_partner'], 2);
		
		if($search_id){
			if(stristr($report['id'], $search_id) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_name){
			if(stristr($report['user_login'], $search_name) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_name_pc){
			if(stristr($report['name_pc'], $search_name_pc) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_money){
			if(stristr($report['money'], $search_money) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_date){
			if(stristr($report['date'], $search_date) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_com_pc){
			if(stristr($report['commission_pc'], $search_com_pc) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_com_ser){
			if(stristr($report['commission_serv'], $search_com_ser) == TRUE) {
				$report_s[] = $report;
			}
		}
		elseif($search_com_ot){
			if(stristr($report['commission_partner'], $search_com_ot) == TRUE) {
				$report_s[] = $report;
			}
		}
		else{
			$report_s[] = $report;
		}
	}
	
	if(!$_GET['report']){$_GET['report'] = 0;}
	for($i=$_GET['report']*$multiplier; $i < $_GET['report']*$multiplier+$multiplier && $report_s[$i];$i++){
		$reports[] = $report_s[$i];
	}
	//$qqqqqw = $inDB->get_table("cms_user_reports", "status='1'");
	
	if($search_id){ $search_inf = "&search_id=".$search_id; }
	elseif($search_name){ $search_inf = "&search_name=".$search_name; }
	elseif($search_name_pc){ $search_inf = "&search_name_pc=".$search_name_pc; }
	elseif($search_money){ $search_inf = "&search_money=".$search_money; }
	elseif($search_date){ $search_inf = "&search_date=".$search_date; }
	elseif($search_com_pc){ $search_inf = "&search_com_pc=".$search_com_pc; }
	elseif($search_com_ser){ $search_inf = "&search_com_ser=".$search_com_ser; }
	elseif($search_com_ot){ $search_inf = "&search_com_ot=".$search_com_ot; }
	
	$col_elem = count($report_s);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	
	$pagination_report .= "<div class='pagination'>";
	if($_GET['report'] == 0){
		$pagination_report .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=0&".$search_inf."#report' ><<</a>";
	}
	if($fffg > 8){
		if($_GET['report'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['report'] == $i){
					$pagination_report .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$i&".$search_inf."#report'>$number</a>";
				}
			}
			$pagination_report .= "<span class='' >..</span>";
			$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$tttt&".$search_inf."#report'>$tttt</a>";
		}
		elseif($_GET['report'] > 4 && $_GET['report'] < $tttt-4){
			$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=0&".$search_inf."#report'>1</a>";
			$pagination_report .= "<span class='' >..</span>";
			for($i=$_GET['report']-2; $i<$_GET['report']+2; $i++){
				$number = $i+1;
				if($_GET['report'] == $i){
					$pagination_report .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$i&".$search_inf."#report'>$number</a>";
				}
			}
			$pagination_report .= "<span class='' >..</span>";
			$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$tttt&".$search_inf."#report'>$tttt</a>";
		}
		elseif($_GET['report'] > $tttt-4){
			$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=0&".$search_inf."#report'>1</a>";
			$pagination_report .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['report'] == $i){
					$pagination_report .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$i&".$search_inf."#report'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['report'] == $i){
				$pagination_report .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$i&".$search_inf."#report'>$number</a>";
			}
		}
	}
	if($_GET['report'] == $tttt){
		$pagination_report .= "<a class='active' >>></a>";
	}
	else{
		$pagination_report .= "<a href='/users/".$inUser->id."/administration.html?report=$tttt&".$search_inf."#report'>>></a>";
	}
	$pagination_report .= "</div>";
	if($fffg == 1) $pagination_report = "";
	//var_dump($report_s);
	
//--------------------------------------   вкладка удалене заявок на бирже   -------------------------------	

	//$table_history = $inDB->get_table("cms_user_event_history", "status='1'");
	if(!$_GET['exchange']){$_GET['exchange'] = 0;}
	$qqqqqw = $inDB->get_table("cms_user_requests_for_payment", "status='0'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	
	$pagination_exchange .= "<div class='pagination'>";
	if($_GET['exchange'] == 0){
		$pagination_exchange .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=0#exchange'><<</a>";
	}
	if($fffg > 8){
		if($_GET['exchange'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['exchange'] == $i){
					$pagination_exchange .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$i#exchange'>$number</a>";
				}
			}
			$pagination_exchange .= "<span class='' >..</span>";
			$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$tttt#exchange'>$tttt</a>";
		}
		elseif($_GET['exchange'] > 4 && $_GET['exchange'] < $tttt-4){
			$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=0#exchange'>1</a>";
			$pagination_exchange .= "<span class='' >..</span>";
			for($i=$_GET['exchange']-2; $i<$_GET['exchange']+2; $i++){
				$number = $i+1;
				if($_GET['exchange'] == $i){
					$pagination_exchange .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$i#exchange'>$number</a>";
				}
			}
			$pagination_exchange .= "<span class='' >..</span>";
			$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$tttt#exchange'>$tttt</a>";
		}
		elseif($_GET['exchange'] > $tttt-4){
			$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=0#exchange'>1</a>";
			$pagination_exchange .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['exchange'] == $i){
					$pagination_exchange .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$i#exchange'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['exchange'] == $i){
				$pagination_exchange .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$i#exchange'>$number</a>";
			}
		}
	}
	if($_GET['exchange'] == $tttt){
		$pagination_exchange .= "<a class='active' >>></a>";
	}
	else{
		$pagination_exchange .= "<a href='/users/".$inUser->id."/administration.html?exchange=$tttt#exchange'>>></a>";
	}
	$pagination_exchange .= "</div>";
	if($fffg == 1) $pagination_exchange = "";
	$aaaas = "SELECT * FROM cms_user_requests_for_payment WHERE status='0' LIMIT ".$_GET['exchange']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_exchange[] = $item1;
	}
	foreach($table_exchange as $key => $results){
		$exchange['id'] = $results['id'];
		$exchange['user_login'] = $inDB->get_field("cms_users","id='".$results['user_id']."'","login");
		$exchange['user_id'] = $results['user_id'];
		$exchange['garant'] = $results['garant'];
		//$exchange['seconds'] = strtotime($results['date_event']);
		$exchange_s[] = $exchange;
	}
	//var_dump($report_s);
//--------------------------------------   вкладка обмен сервис    ----------------------------------------		

	//$table_servise = $inDB->get_table("cms_user_exchange_service", "status='1'");
	if(!$_GET['service']){$_GET['service'] = 0;}
	$qqqqqw = $inDB->get_table("cms_user_exchange_service", "`status` BETWEEN '0' AND '1'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	
	$pagination_serv .= "<div class='pagination'>";
	if($_GET['service'] == 0){
		$pagination_serv .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=0#service'><<</a>";
	}
	if($fffg > 8){
		if($_GET['service'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['service'] == $i){
					$pagination_serv .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$i#verification'>$number</a>";
				}
			}
			$pagination_serv .= "<span class='' >..</span>";
			$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['service'] > 4 && $_GET['service'] < $tttt-4){
			$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=0#verification'>1</a>";
			$pagination_serv .= "<span class='' >..</span>";
			for($i=$_GET['service']-2; $i<$_GET['service']+2; $i++){
				$number = $i+1;
				if($_GET['service'] == $i){
					$pagination_serv .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$i#verification'>$number</a>";
				}
			}
			$pagination_serv .= "<span class='' >..</span>";
			$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['service'] > $tttt-4){
			$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=0#verification'>1</a>";
			$pagination_serv .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['service'] == $i){
					$pagination_serv .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$i#verification'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['service'] == $i){
				$pagination_serv .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$i#service'>$number</a>";
			}
		}
	}
	if($_GET['service'] == $tttt){
		$pagination_serv .= "<a class='active' >>></a>";
	}
	else{
		$pagination_serv .= "<a href='/users/".$inUser->id."/administration.html?service=$tttt#service'>>></a>";
	}
	$pagination_serv .= "</div>";
	if($fffg == 1) $pagination_serv = "";
	$aaaas = "SELECT * FROM cms_user_exchange_service WHERE `status` BETWEEN '0' AND '1' LIMIT ".$_GET['service']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_servise[] = $item1;
	}
	$proverka = 0;
	foreach($table_servise as $key => $result2){
		if($result2['status'] == '0'){
			$service['line'] = 0;
			$time = $result2['date'];
			$service['date'] = date('Y-m-d H:i', $time);
		}
		elseif($result2['status'] == '1'){
			$service['line'] = 1;
			$time = $result2['date'];
			if((time() - $time) < 86400){
				$time_off = (time() - $time);
				$hour = floor($time_off/3600);
				
				$minuts = 59 - floor(($time_off - $hour*3600)/60);
				$service['hour'] = 23 - $hour;
				$service['minuts'] = $minuts;
			}
			else{
				$inDB->query("UPDATE cms_user_exchange_service SET status='3' WHERE id='".$result2['id']."'");
				
				$inf_mail = array();
				$inf_mail['email'] = $inDB->get_field("cms_users","id='".$result2['user_id']."'","email");
				$inf_mail['message'] = "<p>Заявка <b>№ ".$result2['id']."</b> на обмен средств через сервис удалена по истечению времени.</p>";
				$href2 = "<p><a href='http://".$_SERVER['HTTP_HOST']."'>Перейти в личный кабинет</a></p>";
				$email = $model->sendEmail3($inf_mail, $href2);
				
				$inf_mail2 = array();
				$config = cmsConfig::getDefaultConfig(); 
				$inf_mail2['email'] =  $config['sitemail'];
				$inf_mail2['message'] = "<p>Заявка <b>№ ".$result2['id']."</b> на обмен средств через сервис удалена по истечению времени.</p>";
				$href = "<p><a href='http://".$_SERVER['HTTP_HOST']."/users/1/administration.html#service'>Посмотреть все заявки</a></p>";
				$email2 = $model->sendEmail3($inf_mail2, $href);
				
				$model->EventHistory($usr['id'], "Удаление заявки №:".$result2['id']." с обмена стандарт по истечению времени.");
				$proverka = 1;
			}
			
		}
		else{
			$service['line'] = 1;
			$time = $result2['date'];
			$service['date'] = date('Y-m-d H:i', $time);
		}
		$service['id'] = $result2['id'];
		$service['user_login'] = $inDB->get_field("cms_users","id='".$result2['user_id']."'","login");
		$service['money'] = $result2['money_plus'];
		$service['syst_name'] = $inDB->get_field("cms_user_type_payment","id='".$result2['system_plus']."'","name_sustem");
		$service['syst_score'] = $inDB->get_field("cms_user_score","user_id='".$result2['user_id']."' AND type_payment_id='".$result2['system_plus']."'","score_num");
		$infa = unserialize($result2['infa']);
		foreach($infa as $key2 => $result3){
			$masive_inform['name_syst'] = $inDB->get_field("cms_user_type_payment","id='".$result3['system_minus']."'","name_sustem"); 
			$masive_inform['summ'] = $result3['all_summ']; 
			$masive_inform['all_kurs'] = $result3['all_kurs'];
			$masive_inform['score'] = $inDB->get_field("cms_user_score","user_id='".$result2['user_id']."' AND type_payment_id='".$result3['system_minus']."'","score_num"); 
			$mas_inf[] = $masive_inform;
		}
		$service['masive'] = $mas_inf;
		$servises[] = $service;
		unset($mas_inf);
	}
	if($proverka == 1){
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html');
	}
	//var_dump($servises);
//--------------------------------------   вкладка обмен гарант    ----------------------------------------	

	//$table_garant = $inDB->get_table("cms_user_exchsnge_garant","status='1'");
	if(!$_GET['garant']){$_GET['garant'] = 0;}
	$qqqqqw = $inDB->get_table("cms_user_exchsnge_garant", "`status` BETWEEN '0' AND '1'");
	$col_elem = count($qqqqqw);
	$fffg = ceil($col_elem/$multiplier);
	$tttt = $fffg-1;
	
	$pagination_garant .= "<div class='pagination'>";
	if($_GET['garant'] == 0){
		$pagination_garant .= "<a class='active' ><<</a>";
	}
	else{
		$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=0#garant'><<</a>";
	}
	if($fffg > 8){
		if($_GET['garant'] <= 4){
			for($i=0;$i<6; $i++){
				$number = $i+1;
				if($_GET['garant'] == $i){
					$pagination_garant .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$i#verification'>$number</a>";
				}
			}
			$pagination_garant .= "<span class='' >..</span>";
			$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['garant'] > 4 && $_GET['garant'] < $tttt-4){
			$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=0#verification'>1</a>";
			$pagination_garant .= "<span class='' >..</span>";
			for($i=$_GET['garant']-2; $i<$_GET['garant']+2; $i++){
				$number = $i+1;
				if($_GET['garant'] == $i){
					$pagination_garant .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$i#verification'>$number</a>";
				}
			}
			$pagination_garant .= "<span class='' >..</span>";
			$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$tttt#verification'>$tttt</a>";
		}
		elseif($_GET['garant'] > $tttt-4){
			$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=0#verification'>1</a>";
			$pagination_garant .= "<span class='' >..</span>";
			for($i=$fffg-4; $i<$fffg; $i++){
				$number = $i+1;
				if($_GET['garant'] == $i){
					$pagination_garant .= "<a class='active' >$number</a>";
				}
				else{
					$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$i#verification'>$number</a>";
				}
			}
			
		}
	}
	else{
		for($i=0;$i<$fffg; $i++){
			$number = $i+1;
			if($_GET['garant'] == $i){
				$pagination_garant .= "<a class='active' >$number</a>";
			}
			else{
				$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$i#garant'>$number</a>";
			}
		}
	}
	if($_GET['garant'] == $tttt){
		$pagination_garant .= "<a class='active' >>></a>";
	}
	else{
		$pagination_garant .= "<a href='/users/".$inUser->id."/administration.html?garant=$tttt#garant'>>></a>";
	}
	$pagination_garant .= "</div>";
	if($fffg == 1) $pagination_garant = "";
	$aaaas = "SELECT * FROM cms_user_exchsnge_garant WHERE `status` BETWEEN '0' AND '1' LIMIT ".$_GET['garant']*$multiplier.", $multiplier";
	$result1 = $inDB->query($aaaas) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$table_garant[] = $item1;
	}
	foreach($table_garant as $key => $result){
		if($result['status'] == '0'){
			$garant['line'] = 0;
		}
		else{
			$garant['line'] = 1;
		}
		$garant['id'] = $result['id'];
		$garant['login_user_1'] = $inDB->get_field("cms_users","id='".$result['user_id_1']."'","login");
		$garant['money_plus_1'] = $result['money_plus_1'];
		$garant['name_sys_1'] = $inDB->get_field("cms_user_type_payment","id='".$result['system_plus_1']."'","name_sustem");
		$garant['score_1'] = $inDB->get_field("cms_user_score","user_id='".$result['user_id_1']."' AND type_payment_id='".$result['system_plus_1']."'","score_num");
		$garant['login_user_2'] = $inDB->get_field("cms_users","id='".$result['user_id_2']."'","login");
		$garant['money_plus_2'] = $result['money_plus_2'];
		$garant['name_sys_2'] = $inDB->get_field("cms_user_type_payment","id='".$result['system_plus_2']."'","name_sustem");
		$garant['score_2'] = $inDB->get_field("cms_user_score","user_id='".$result['user_id_2']."' AND type_payment_id='".$result['system_plus_2']."'","score_num");
			
		$garants[] = $garant;
	}
	//var_dump($garants);	
	
//-------------------------- если удаляем ПС ------------------------	
	if($opt == "delete_ps"){
		$del_id_pc = cmsCore::request('id_ps','int');
		$name_ps = $inDB->get_field('cms_user_type_payment',"id='$del_id_pc'",'name_sustem');
		$inDB->query("UPDATE cms_user_type_payment SET status='1' WHERE id='$del_id_pc'");
		$model->EventHistory('1', "Удаление ПС \'$name_ps\'.");
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#delete_pc');
	}
//------------------------ если добавляем ПС ------------------------
	if($opt == 'add_new_pc'){
		$name_new_pc = cmsCore::request('name_pc','str');
		$inDB->query("INSERT cms_user_type_payment (name_sustem, payment_admin, before_score, after_score, min_img, on_off, relations, tax_money, status) VALUES ('$name_new_pc' , '0', '', '', '', '0', ',0,' , '0','0')");
		$last_syst_id = $inDB->get_last_id('cms_user_type_payment');
		$table_user = $inDB->get_table('cms_users');
		foreach($table_user as $result){
			$inDB->query("INSERT INTO cms_user_score (user_id, type_payment_id, score_num, score_val, score_in, score_out, num_payment, status) VALUES ('".$result['id']."','$last_syst_id','0','0','0','0','0','0')");
		}
		$model->EventHistory('1', "Добавление новой ПС \'$name_new_pc\'.");
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#add_new_pc');
	}
//----------------- если сохраняем таблицу связи ПС -----------------
	if($opt == 'relation_save'){
		if(isset($_POST['kurs'])) $kurs_arr = $_POST['kurs'];
		if(isset($_POST['commission'])) $commission_arr = $_POST['commission'];
		if(isset($_POST['min_money'])) $min_money_arr = $_POST['min_money'];
		if(isset($_POST['status'])) $status_arr = $_POST['status_val'];
		
		foreach($kurs_arr as $key=>$result){
			$aaa = 0;
			$inf_mass = ',';
			foreach($result as $key2=>$result2){
				if($status_arr[$key][$key2] == '1'){
					$aaa = 1;
					$inf_mass .= $key2.',';
				}
				$massiv['system'] = $key;
				$massiv['relation_syst'] = $key2;
				$massiv['kurs'] = $result2;
				$massiv['commission'] = $commission_arr[$key][$key2];
				$massiv['min_money'] = $min_money_arr[$key][$key2];
				if(isset($status_arr[$key][$key2])){
					$massiv['status'] = $status_arr[$key][$key2];
				}
				else{
					$massiv['status'] = '0';
				}
				$isset_zap = $inDB->get_field('cms_user_relation_pc',"system='$key' AND relation_syst='$key2'",'id');
				if($isset_zap){
					$inDB->query("UPDATE cms_user_relation_pc SET system='".$massiv['system']."', relation_syst='".$massiv['relation_syst']."', kurs='".$massiv['kurs']."', commission='".$massiv['commission']."', min_money='".$massiv['min_money']."', status='".$massiv['status']."' WHERE id='$isset_zap'");
				}
				else{
					$inDB->query("INSERT cms_user_relation_pc (system,relation_syst,kurs,commission,min_money,status) VALUES ('".$massiv['system']."','".$massiv['relation_syst']."','".$massiv['kurs']."','".$massiv['commission']."','".$massiv['min_money']."','".$massiv['status']."')");
				}
			//	var_dump($status_arr[$key][$key2]." = ".$massiv['status']); echo "<br>";
			}
			if($aaa == 1){
				$inDB->query("UPDATE cms_user_type_payment SET relations='$inf_mass' WHERE id='$key'");
			}
			else{
				$inDB->query("UPDATE cms_user_type_payment SET relations=',0,' WHERE id='$key'");
			}
			//$inf2_mass[] = $inf_mass;
			unset($inf_mass);
		}
		
		//var_dump($inf2_mass);
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#relation_save');
	}
//------- усли удаляем заявку с биржи ------------	
	if($opt == 'del_appl_exch'){
		$id_appl = cmsCore::request('id_appl','str');
		$message_user = cmsCore::request('message_user','str');
		$inf_appl = $inDB->get_fields('cms_user_requests_for_payment','id="'.$id_appl.'"','user_id, garant, id_paym_syst, money_off');
		if($inf_appl['garant'] == '3'){
			$GF = $inDB->get_field("cms_user_expenses", 'user_id="'.$inf_appl['user_id'].'" AND expense_type_id="4"', 'expense_money');
			$result_money = $GF + ($inf_appl['money_off']);
			$result_money = str_replace(',', '.', $result_money);
			$sql_1 = "UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='".$inf_appl['user_id']."' AND expense_type_id='4'";
			$inDB->query($sql_1);
		}
		elseif($inf_appl['garant'] == '1'){
			$score_val = $inDB->get_field("cms_user_score","user_id='".$inf_appl['user_id']."' AND type_payment_id='".$inf_appl['id_paym_syst']."'","score_val");
			$result_money = $score_val+$inf_appl['money_off'];
			$result_money = str_replace(',', '.', $result_money);
			$sql_1 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='".$inf_appl['user_id']."' AND type_payment_id='".$inf_appl['id_paym_syst']."'";
			$inDB->query($sql_1);
		}
		$query_100 = "DELETE FROM cms_user_requests_for_payment WHERE id='$id_appl'";
		$inDB->query($query_100);
		
		cmsUser::sendMessage(USER_MASSMAIL, $inf_appl['user_id'], "<p>Ваша заявка № $id_appl на бирже обмена удалена, средства зачислены обрато на ваш счет.</p><p><b>Причина:</b></p><p>".$message_user."</p>");
		
		$inf_mail = array();
		$inf_mail['email'] = $inDB->get_field('cms_users','id="'.$inf_appl['user_id'].'"','email');
		$inf_mail['message'] = "<p>Ваша заявка № $id_appl на бирже обмена удалена, средства зачислены обрато на ваш счет.</p><p><b>Причина:</b></p><p>".$message_user."</p>";
		$href = '<p><a href="http://'.$_SERVER['HTTP_HOST'].'">Перейти на сайт</a></p>';	
		$email = $model->sendEmail3($inf_mail, $href);
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#exchange_delate');
	}
//----------- если меняем % на реферальные начисления -------------------
	if($opt == "referals"){
		$status = cmsCore::request('on_off2', 'array');
		$procent = cmsCore::request('procent', 'array');
		$statuss = cmsCore::request('status', 'array');
		
		foreach($procent as $key=>$result){
			$aa = $key + 1;
			$inDB->query("UPDATE cms_preferal_accrued SET status='".$statuss[$key]."', procent='$result' WHERE id='$aa'");
		}
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#save_referal');
	}
//----------- Если удаляем все отчеты ------------------
	if($opt == "del_report"){
		$inDB->query("UPDATE cms_user_reports SET status='0' WHERE status='1'");
		cmsCore::redirect('/users/'.$inDB->id.'/administration.html#report_delete');
	}
//------- Если удаляем всю историю  --------
	if ($opt == 'del_history'){
		
		$inDB->query("UPDATE cms_user_event_history SET status='0' WHERE status='1'");
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#history_delete');
	}

// ---------- если додается скидка -------------
	if($opt == 'add_discount'){
		$id_usr = cmsCore::request('id_user', 'str');
		$discount = cmsCore::request('discount', 'str');
		$message_user = cmsCore::request('message_user', 'str');
		$date = date("Y.m.d");
		$end_date = cmsCore::request('last_date', 'str');
		$inDB->query("INSERT cms_user_discount (user_id, date, discount, end_date, status) VALUES ('$id_usr','$date','$discount','$end_date','1')");
		
		cmsUser::sendMessage(USER_MASSMAIL, $id_usr, "<p>Вам добавлена скидка в размере $discount%. Будьте счастливы.</p><p><b>Причина:</b></p><p>".$message_user."</p><p>Действует до ".$end_date."</p>");
		
		$inf_mail = array();
		$inf_mail['email'] = $inDB->get_field('cms_users','id="'.$id_usr.'"','email');
		$inf_mail['message'] = "<p>Вам добавлена скидка в размере $discount%.</p><p><b>Причина:</b></p><p>".$message_user."</p><p>Действует до ".$end_date."</p>";
		$href = '<p><a href="http://'.$_SERVER['HTTP_HOST'].'">Перейти на сайт</a></p>';	
		$email = $model->sendEmail3($inf_mail, $href);
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#add_discount');
	}
// ---------- если додается акция -------------
	if($opt == 'add_all_action'){
		$id_usr = 1;//cmsCore::request('id_user', 'str');
		$discount = cmsCore::request('discount', 'str');
		$message_user = cmsCore::request('message_user', 'str');
		$date = date("Y.m.d");
		$end_date = cmsCore::request('last_date', 'str');
		$inDB->query("INSERT cms_user_discount (user_id, date, discount, end_date, status) VALUES ('0','$date','$discount','$end_date','1')");
		
		cmsUser::sendMessageToGroup(USER_MASSMAIL, $id_usr, "<p>Новая акция, скидка в размере $discount%. Будьте счастливы.</p><p><b>Причина:</b></p><p>".$message_user."</p><p>Действует до ".$end_date."</p>");
		
		$inf_mail = array();
		$inf_mail['email'] = $inDB->get_field('cms_users','id="'.$id_usr.'"','email');
		$inf_mail['message'] = "<p>Вам добавлена скидка в размере $discount%.</p><p><b>Причина:</b></p><p>".$message_user."</p><p>Действует до ".$end_date."</p>";
		$href = '<p><a href="http://'.$_SERVER['HTTP_HOST'].'">Перейти на сайт</a></p>';	
		//$email = $model->sendEmail3($inf_mail, $href);
		
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#add_discount');
	}
//---------------- если добавляем изображение к временной пс --------------
	if ($opt == 'change_img_user_ps'){
		
		$inPage->setTitle($_LANG['LOAD_AVATAR']);
		$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
		$inPage->addPathway($_LANG['LOAD_AVATAR']);
		if (cmsCore::inRequest('upload')) {

			cmsCore::loadClass('upload_photo');
			$inUploadPhoto = cmsUploadPhoto::getInstance();
			
			// Выставляем конфигурационные параметры
			$inUploadPhoto->upload_dir    = PATH.'/images/';
			$inUploadPhoto->dir_medium    = 'users/payment_system/';
			$inUploadPhoto->medium_size_w = $model->config['medw'];
			$inUploadPhoto->medium_size_h = $model->config['medh'];
			$inUploadPhoto->is_watermark  = false;
			$inUploadPhoto->input_name    = 'imageps';
			
			$file = $inUploadPhoto->uploadPhoto();
			
			if(!$file){
				cmsCore::redirect('/users/'.$inUser->id.'/administration.html#ERROR');
			}
			$system_id = cmsCore::request('id_sustem', 'str');
			$name_system = cmsCore::request('name_system', 'str');
			$query_101 = "DELETE FROM cms_user_image_pc WHERE id_paym_syst='$system_id'";
			$inDB->query($query_101);
			$inDB->query("INSERT cms_user_image_pc (id_paym_syst, name_syst, image) VALUES ('$system_id','$name_system','/images/users/payment_system/{$file['filename']}')");
			
			
			$model->EventHistory($usr['id'], "Добавление изображения к пользовательской ПС \"$name_system\"");
			
			cmsCore::redirect('/users/'.$inUser->id.'/administration.html#IMAGE_SAVED2');

		}
			
	}
////-------------------------- Если изменяем время действия пароля --------------------------
	if($opt == 'control_pass'){
		$reload_pass = cmsCore::request('reload_pass','str');
		$status = cmsCore::request('status','int');
		if($reload_pass && $status){
			$inDB->query("UPDATE cms_user_config SET value='$reload_pass', status='$status' WHERE name='reload_pass'");
		}
	}
////-------------------------- Если изменяем изображение платежной системы --------------------------
	if ($opt == 'change_img_ps'){
		
		$x1 = cmsCore::request('x1','int');
		$x2 = cmsCore::request('x2','int');
		$y1 = cmsCore::request('y1','int');
		$y2 = cmsCore::request('y2','int');
		$directory_img = cmsCore::request('file_name','str');
		$directory_img = substr($directory_img,1);
		list($width, $height) = getimagesize($directory_img);
		$new_width = $x2-$x1 ;
		$new_height = $y2-$y1 ;	
		// ресэмплирование
		$image_p = imagecreatetruecolor($new_width, $new_height);
		$type_img = explode('.',$directory_img);
		$perem3 = end($type_img);
		if($perem3 != 'png'){
			$image = imagecreatefromjpeg($directory_img);
		}
		else{
			$image = imagecreatefrompng($directory_img);
			if(!$image){
				$image = imagecreatefromjpeg($directory_img);
				$perem3 = 'jpg';
			}
		}
		imagecopyresampled($image_p, $image, 0, 0, $x1, $y1, $new_width, $new_height, $new_width, $new_height);
		// вывод
		$perem1 = explode('/',$_POST['file_name']);
		$perem2 = end($perem1);
		//var_dump($x1, $y1,$new_width, $new_height, $width, $height); 
		/*header('Content-Type: image/jpeg');
		if($perem3 != 'png'){
			imagejpeg($image_p, null, 100);
		}
		else{
			imagepng($image_p, null, 0);
		}
		exit;*/
		unlink("images/users/payment_system/".$perem2);
		if($perem3 != 'png'){
			imagejpeg($image_p, "images/users/payment_system/".$perem2, 100);
		}
		else{
			imagepng($image_p, "images/users/payment_system/".$perem2, 0);
		}
		
		$system_id = cmsCore::request('id_sustem', 'str');
		
		
		$inDB->query("UPDATE cms_user_type_payment SET min_img='/images/users/payment_system/$perem2' WHERE id='$system_id'");
		
		
		$name_ps = $inDB->get_field("cms_user_type_payment", "id='$system_id'", "name_sustem");
		$model->EventHistory($usr['id'], "Изменение изображения ПС \"$name_ps\"");
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#IMAGE_SAVED');
		
				
	}
////-------------------------- Если сохраняем профиль --------------------------
	if ($opt == 'save'){
	cmsCore::loadClass('upload_photo');

	$i = 0;
			
		$errors = false;
		
		$users['first_name'] = cmsCore::request('first_name', 'str');
		$profiles['country'] = cmsCore::request('your_country', 'str');
		$profiles['street'] = cmsCore::request('your_street', 'str');
		
		$admin_money = cmsCore::request("money_admin", 'array');
		$user_score = cmsCore::request("peyment", 'array');
		$system_name = cmsCore::request("name", 'array');
		$on_off = cmsCore::request("on_off", 'array');
		$relations = cmsCore::request("relations", 'array');
		$tax_money = cmsCore::request("tax_money", "array");
		/*var_dump($user_score);
		echo "<br>-----------------<br>";
		var_dump($system_name);
		echo "<br>-----------------<br>";
		var_dump($on_off);*/
		//$asdasdasd = "DELETE FROM cms_user_score WHERE user_id='$id'";
		//$inDB->query($asdasdasd);
		
		foreach($user_score as $key => $user_scores){
			if($relations[$key] != ""){
				$opop = str_replace('.', ',', $relations[$key]);
				$opo = ",";
				$opo .= $relations[$key];
				$opo .= ",";
			}
			else{
				$opo = "";
			}
			$inDB->query("UPDATE cms_user_score SET score_num='$user_scores' WHERE user_id='".$inUser->id."' AND type_payment_id='$key'");
			if($on_off[$key] == 'on') $qqqqqww = 1;else $qqqqqww = 0;
			$inDB->query("UPDATE cms_user_type_payment SET name_sustem='".$system_name[$key]."', on_off='".$qqqqqww."', tax_money='".$tax_money[$key]."', relations='$opo', payment_admin='".$admin_money[$key]."' WHERE id='$key'");
			unset($qqqqqww);
			unset($opo);
		}
		
		$model->EventHistory($usr['id'], "Изменение платежных данных");
						
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#PROFILE_SAVED');

	}
	if($opt == "save_inf_score"){
		$before_score = cmsCore::request("before_score", 'array');
		$after_score = cmsCore::request("after_score", 'array');
		$number_score = cmsCore::request("number_score", 'array');
		//var_dump($after_score);
		foreach($before_score as $key => $before_scores){
			$inDB->query("UPDATE cms_user_type_payment SET before_score='".$before_score[$key]."', after_score='".$after_score[$key]."', number_score='".$number_score[$key]."' WHERE id='$key'");
		}
		
		$model->EventHistory($usr['id'], "Изменение платежных данных");
						
		cmsCore::redirect('/users/'.$inUser->id.'/administration.html#payment_inf_scores');
		
		
	}
	
	$reload_pass = $inDB->get_fields('cms_user_config','name=\'reload_pass\'', 'value, status');
	
	cmsPage::initTemplate('components', 'com_users_administration')->
		assign('opt', $opt)->
		assign('reload_pass', $reload_pass)->
		assign('referals', $referals)->
		assign('users_pc', $users_pc)->
		assign('inf_relation', $inf_relation)->
		assign('payments', $payments)->
		assign('payments2', $payments2)->
		assign('inf_moneys2', $inf_moneys2)->
		assign('reports', $reports)->
		assign('users', $users)->
		assign('pagination_discount', $pagination_discount)->
		assign('pagination_garant', $pagination_garant)->
		assign('pagination_exchange', $pagination_exchange)->
		assign('pagination_report', $pagination_report)->
		assign('pagination_serv', $pagination_serv)->
		assign('pagination_hist', $pagination_hist)->
		assign('pagination_verif', $pagination_verif)->
		assign('discounts', $discounts)->
		assign('garants', $garants)->
		assign('search_id', $search_id)->
		assign('search_name', $search_name)->
		assign('search_name_pc', $search_name_pc)->
		assign('search_money', $search_money)->
		assign('search_date', $search_date)->
		assign('search_com_pc', $search_com_pc)->
		assign('search_com_ser', $search_com_ser)->
		assign('search_com_ot', $search_com_ot)->
		assign('exchange_s', $exchange_s)->
		assign('history_s', $history_s)->
		assign('report_s', $report_s)->
		assign('servises', $servises)->
		assign('inf_moneys', $inf_moneys)->
		assign('usr', $usr)->
		assign('usr2', $usr_2)->
		assign('results_file', $files)->
		assign('private_forms', $private_forms)->
		assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
		assign('cfg', $model->config)->
		display('com_users_administration.tpl');
}
//============================================================================//
//============================= Просмотр всех предложений на заявку стандарт ============================//
//============================================================================//
if($do == "see_deals"){
	$inPage->setTitle("Подтверждение обмена");
	$user_id=$model->returnId();
	$id = $inUser->id;
	$login = $inDB->get_field("cms_users", "id='$user_id'", "login");
	$usr = $model->getUser($login);
	
	$errors = false;
	$number_aplication = $_GET['id'];
	if($number_aplication){
		$table_temporary_standart = $inDB->get_table("cms_user_temporary_standart", "system_id='$number_aplication'");
		foreach($table_temporary_standart as $result){
		
		$table_user_online = $inDB->get_fields("cms_users","id='".$result['user_id']."'","nickname, first_name, login");
		
		$user_id_offline = $result['user_id'];
				
		$profile_infa = $inDB->get_fields("cms_user_profiles", "user_id='".$result['user_id']."'", 'imageurl, odnokl, g_plus, vk, tviter, fasebook, mail, country');
		if($profile_infa['imageurl'] == "") $image_user = "nopic.jpg";
		else $image_user = $profile_infa['imageurl'];
		//var_dump($image_user);
		$social_cets .= "<div class='sotc_ceti_s'>";
		
		if($profile_infa['odnokl'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['g_plus'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['vk'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -149px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['tviter'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -207px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['fasebook'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left;'></div>";}
		if($profile_infa['mail'] != ""){ $social_cets .= "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -90px -127px; width:50px; height:50px; float:left;'></div>";}
		$social_cets .= "</div>";
		
		$infa_standart_application['infa'] = $profile_infa;
		$infa_standart_application['user_id_off'] = $user_id_offline;
		$infa_standart_application['image'] = $image_user;
		$infa_standart_application['sotc_set'] = $social_cets;
		$infa_standart_application['inf_user'] = $table_user_online;
		$inf_stand_appl[] = $infa_standart_application;
		
		unset($infa_standart_application);
		unset($profile_infa);
		unset($user_id_offline);
		unset($login_off);
		unset($table_user_online);
		unset($image_user);
		unset($social_cets);
		}
		//var_dump($inf_stand_appl);
	}
	else{
		cmsCore::redirect('/users/'.$login);
	}
	
	cmsPage::initTemplate('components', 'com_users_see_deals')->
		assign('opt', $opt)->
		assign('usr', $usr)->
		assign('number_aplication', $number_aplication)->
		assign('login', $login)->
		assign('inf_stand_appl', $inf_stand_appl)->
		assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
		assign('cfg', $model->config)->
		display('com_users_see_deals.tpl');
}
//============================================================================//
//============================= Подтверждение заявки с промокодами длс ГФ - ГФ ============================//
//============================================================================//

if($do=='gf_gf_exchange'){
	$inPage->setTitle("Подтверждение обмена");
	$user_id=$model->returnId();
	$id = $inUser->id;
	$login = $inDB->get_field("cms_users", "id='$user_id'", "login");
	$usr = $model->getUser($login);
	
	$errors = false;
	
	$opt = cmsCore::request("opt", "str");
	$number_aplication = cmsCore::request("id_zayavki","str");
	
if($number_aplication){
	$id_zapusa = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND  status='0'","id");
	$inf_application_timer = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication'","*");
	
	$inf_application1 = $inDB->get_fields("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id']."'","*");
	$inf_application2 = $inDB->get_fields("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id_2']."'","*");
	
	$id_zapusa_2 = $inDB->get_field("cms_user_code_protection","num_appl='".$inf_application_timer['zakaz_id_2']."' AND  status='0'","id");
// -------------------- визначення часу (таймер) ----------------------
	$time_start = $inDB->get_field("cms_user_application_timer", "zakaz_id='$number_aplication'", 'time_start');
	$time_nuw = time();
	//$time_nuw = 1440746247;
	if(($time_nuw - $time_start) < 1800){
		$time_out = 30 - floor(($time_nuw - $time_start)/60);
	}
	else{
		if(!$time_start){cmsCore::redirect('/users/'.$login);}
		$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
		$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
		
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
				
		$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa_2' AND status='0'");
		
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");
		
		$model->EventHistory($id, "Время заявок №: $number_aplication и №: ".$inf_application_timer['zakaz_id_2']." истекло.");
		
		cmsCore::redirect('/users/'.$login.'#time_off');
				
	}
	//================================== место для $opt ==============================================
if($opt == 'first_cheack'){
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_off='1' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 1 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
}

if($opt == "code_prot_1"){
	$code = cmsCore::request('hev_code', "str");
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET code_on='$code' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET code_off='$code' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 2 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
}

if($opt == "code_2"){
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET verification_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET verification_off='1' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 3 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	
	cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Код протекции для системы № ".$syst_2.": ".$infa_2['code_off']);
	//cmsCore::redirect("/users/".$id."/gf_gf_exchange.html");
}
if($opt == "dell_obmen"){
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
	
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
			
	$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa' AND status='0'");
	$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa_2' AND status='0'");
	
	$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
	$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");

	$model->EventHistory($id, "Отмена обмена заявки №: $number_aplication и № ".$inf_application_timer['zakaz_id_2'].".");
	
	cmsCore::redirect('/users/'.$login.'#otmena_obmen');
}
	//================================================================================================
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$bd_first_cheack = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_on");
		$click_us_2 = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_off");
	}
	
	if($bd_first_cheack == '0'){
		$infa_application = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication' AND user_id='$id'","*");
		$result_infa['id'] = $number_aplication;
		if($inf_application1['name_system'] != '0'){
			$result_infa['name_system1'] = $inf_application1['name_system'];
		}
		else{
			$result_infa['name_system1'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
		}
		if($inf_application2['name_system'] != '0'){
			$result_infa['name_system2'] = $inf_application2['name_system'];
		}
		else{
			$result_infa['name_system2'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
		}
		$result_infa['img_syst1'] = $inDB->get_field('cms_user_image_pc',"id_paym_syst='".$inf_application1['id_paym_syst']."'",'image');
		$result_infa['img_syst2'] = $inDB->get_field('cms_user_image_pc','id_paym_syst="'.$inf_application2['id_paym_syst'].'"','image');
		
		$result_infa['money_off'] = $infa_application['money_minus'];
		$result_infa['money_on'] = $infa_application['money_plus'];
		//var_dump($result_infa);
	}
	elseif($bd_first_cheack == '1'){
		if($click_us_2 == '1'){
		$code = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on, code_off");
		$infa_application = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication' AND user_id='$id'","*");
		if($code['code_on'] == '0'){
			$infa_on['score_num'] = $infa_application['score_num'];
			if($inf_application1['name_system'] != '0'){
				$infa_on['name_system'] = $inf_application1['name_system'];
			}
			else{
				$infa_on['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
			}
			$infa_on['money_off'] = $infa_application['money_minus'];
			//var_dump($infa_on);
		}
		elseif($code['code_on'] != '0' && $code['code_off'] == '0'){
			$expectation = 1;
		}	
		elseif($code['code_on'] != '0' && $code['code_off'] != '0'){
			$last_cheack = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","verification_on, verification_off");
			if($last_cheack['verification_on'] == '0'){
				if($inf_application2['name_system'] != '0'){
					$code_podtv['name_system_in'] = $inf_application2['name_system'];
				}
				else{
					$code_podtv['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
				}
				$code_podtv['money_in'] = $infa_application['money_plus'];
				//var_dump($code_podtv);
			}
			elseif($last_cheack['verification_on'] == '1' && $last_cheack['verification_off'] == '0'){
				$expectation = 1;
			}
			elseif($last_cheack['verification_on'] == '1' && $last_cheack['verification_off'] == '1'){
				if($inf_application2['name_system'] != '0'){
					$heve_money['name_system_in'] = $inf_application2['name_system'];
				}
				else{
					$heve_money['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
				}
				$heve_money['code'] = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_off");
				
				$otpravka_message = $inDB->get_field("cms_user_code_protection","id='$id_zapusa'","status");
				if($otpravka_message == '0'){
					cmsUser::sendMessage(USER_MASSMAIL, $id, "Код протекции для системы ".$heve_money['name_system_in'].": ".$heve_money['code']);
					$inf_mail2 = array();
					$inf_mail2['email'] = $inDB->get_field("cms_users","id='$id'","email");
					$inf_mail2['message'] = "<p><b>Система:</b>".$heve_money['name_system_in']."</p>
						<p><b>Код протекции:</b>".$heve_money['code']."</p>";
					$email2 = $model->sendEmail3($inf_mail2);
					
					$infa_2 = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off, code_on");
					$infa_application = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
					if($inf_application1['name_system'] != '0'){
						$syst_2 = $inf_application1['name_system'];
					}
					else{
						$syst_2 = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
					}
					
					cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Код протекции для системы  ".$syst_2.": ".$infa_2['code_on']);
					$inf_mail3 = array();
					$inf_mail3['email'] = $inDB->get_field("cms_users","id='".$infa_2['user_id_off']."'","email");
					$inf_mail3['message'] = "<p><b>Система:</b>".$syst_2."</p>
						<p><b>Код протекции:</b>".$infa_2['code_on']."</p>";
					$email3 = $model->sendEmail3($inf_mail3);
				}
			}
		}
		}
		else{$expectation = 1;}
	}
	
	if($heve_money){
		//================================================================================
		//================ % ставка на счет админа  0.1 % ============================
		//==============================================================================
		$procent_admin = 0.001;
		$inf_on_user = $inDB->get_fields("cms_user_replace_applications","id_application='$number_aplication'","*");
		$inf_off_user = $inDB->get_fields("cms_user_replace_applications","id_application='".$inf_application_timer['zakaz_id_2']."'","*");
		
		if($inf_on_user['status'] == '1'){
			$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$number_aplication' AND status='2'");
			$money_1 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application1['user_id']."'","expense_money");
			$money_admin = $inf_application_timer['money_minus']*$procent_admin;
			$money_pl = $inf_application_timer['money_minus'] - $money_admin;
			$result_money = $money_1 + $money_pl;
		//	echo $result_money."<br>";
			$tftftf = $inDB->get_field('cms_user_score','user_id="0"','score_val');
			$money_admin = $money_admin + $tftftf;
			$money_admin = str_replace(',', '.', $money_admin);
			$result_money = str_replace(',', '.', $result_money);
			$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
			$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE expense_type_id='4' AND user_id='".$inf_application1['user_id']."'");
		}
		elseif($inf_on_user['status'] == '0'){
			$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_on_user['money_off']."', masive_inf='".$inf_on_user['masive']."', status='0' WHERE id='$number_aplication' AND status='2'");
			$money_1 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application1['user_id']."'","expense_money");
			$money_admin = $inf_application_timer['money_minus']*$procent_admin;
			$money_pl = $inf_application_timer['money_minus'] - $money_admin;
			$result_money = $money_1 + $money_pl;
			//	echo $result_money."<br>";
			$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
			$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
			$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE expense_type_id='4' AND user_id='".$inf_application1['user_id']."'");
		}
		
		if($inf_off_user['status'] == '1'){
			$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
			$money_2 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application2['user_id']."'","expense_money");
			$money_admin2 = $inf_application_timer['money_plus']*$procent_admin;
			$money_pl2 = $inf_application_timer['money_plus'] - $money_admin2;
			$result_money2 = $money_2 + $money_pl2;
		//	echo $result_money2."<br>";
			$money_admin2 = $money_admin2 + $inDB->get_field('cms_user_score','user_id="0"','score_val');
			$money_admin2 = str_replace(',', '.', $money_admin2);
			$result_money2 = str_replace(',', '.', $result_money2);
			$inDB->query("UPDATE cms_user_score SET score_val='$money_admin2' WHERE user_id='0' ");
			$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money2' WHERE expense_type_id='4' AND user_id='".$inf_application2['user_id']."'");
		}
		elseif($inf_off_user['status'] == '0'){
			$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_off_user['money_off']."', masive_inf='".$inf_off_user['masive']."', status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
			$money_2 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application2['user_id']."'","expense_money");
			$money_admin2 = $inf_application_timer['money_plus']*$procent_admin;
			$money_pl2 = $inf_application_timer['money_plus'] - $money_admin2;
			$result_money2 = $money_2 + $money_pl2;
		//	echo $result_money2."<br>";
			$money_admin2 = $money_admin2 + $inDB->get_field('cms_user_score','user_id="0"','score_val');
			$money_admin2 = str_replace(',', '.', $money_admin2);
			$result_money2 = str_replace(',', '.', $result_money2);
			$inDB->query("UPDATE cms_user_score SET score_val='$money_admin2' WHERE user_id='0' ");
			$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money2' WHERE expense_type_id='4' AND user_id='".$inf_application2['user_id']."'");
		}
		
		$data_report['user_id'] = $inf_application1['user_id'];
		$data_report['description'] = 'Обмен ГФ - ГФ.';
		$data_report['currency'] = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
		$data_report['money'] = $inf_application_timer['money_minus'];
		$data_report['commission_serv'] = $inf_application_timer['money_minus']*$procent_admin;
		$data_report['commission_partner'] = 0;
		$model->EventReport($data_report);
		
		$data_report2['user_id'] = $inf_application2['user_id'];
		$data_report2['description'] = 'Обмен ГФ - ГФ.';
		$data_report2['currency'] = $inDB->get_field("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id_2']."'","id_paym_syst");
		$data_report2['money'] = $inf_application_timer['money_plus'];
		$data_report2['commission_serv'] = $inf_application_timer['money_plus']*$procent_admin;
		$data_report2['commission_partner'] = 0;
		$model->EventReport($data_report2);
		
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
				
		$inDB->query("UPDATE cms_user_code_protection SET status='1' WHERE id='$id_zapusa' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET status='1' WHERE id='$id_zapusa_2' AND status='0'");
		
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");
						
		$model->EventHistory($id, "Заявка №: $number_aplication выполнена успешно.");
			
	}
}
else{
	cmsCore::redirect('/users/'.$login);
}
	cmsPage::initTemplate('components', 'com_users_gf_gf_exchange')->
		assign('opt', $opt)->
		assign('time_out', $time_out)->
		assign('heve_money', $heve_money)->
		assign('infa_on', $infa_on)->
		assign('code_podtv', $code_podtv)->
		assign('expectation', $expectation)->
		assign('result_infa', $result_infa)->
		assign('id_zapusa', $id_zapusa)->
		assign('id_zapusa_2', $id_zapusa_2)->
		assign('usr', $usr)->
		assign('number_aplication', $number_aplication)->
		assign('num_ap_2', $inf_application_timer['zakaz_id_2'])->
		assign('login', $login)->
		assign('inf_stand_appl', $inf_stand_appl)->
		assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
		assign('cfg', $model->config)->
		display('com_users_gf_gf_exchange.tpl');
	
}

//============================================================================//
//============================= Подтверждение заявки с промокодами длс ГФ - гарант ============================//
//============================================================================//

if($do=='gf_garant_exchange'){
	$inPage->setTitle("Подтверждение обмена");
	$user_id=$model->returnId();
	$id = $inUser->id;
	$login = $inDB->get_field("cms_users", "id='$user_id'", "login");
	$usr = $model->getUser($login);
	
	$errors = false;
	
	$opt = cmsCore::request("opt", "str");
	$number_aplication = cmsCore::request("id_zayavki","str");
if($number_aplication){
	$id_zapusa = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND  status='0'","id");
	$inf_application_timer = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication'","*");
	
	$inf_application1 = $inDB->get_fields("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id']."'","*");
	$inf_application2 = $inDB->get_fields("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id_2']."'","*");
	
	$id_zapusa_2 = $inDB->get_field("cms_user_code_protection","num_appl='".$inf_application_timer['zakaz_id_2']."' AND  status='0'","id");
// -------------------- визначення часу (таймер) ----------------------
	$time_start = $inDB->get_field("cms_user_application_timer", "zakaz_id='$number_aplication'", 'time_start');
	$time_nuw = time();
	//$time_nuw = 1440746247;
	if(($time_nuw - $time_start) < 1800){
		$time_out = 30 - floor(($time_nuw - $time_start)/60);
	}
	else{
		if(!$time_start){cmsCore::redirect('/users/'.$login);}
		$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
		$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
		
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
				
		$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa_2' AND status='0'");
		
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");
		
		$model->EventHistory($id, "Время заявок №: $number_aplication и №: ".$inf_application_timer['zakaz_id_2']." истекло.");
		
		cmsCore::redirect('/users/'.$login.'#time_off');
				
	}
	//================================== место для $opt ==============================================
if($opt == 'first_cheack'){
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_off='1' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 1 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
}

if($opt == "code_prot_1"){
	$code = cmsCore::request('hev_code', "str");
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET code_on='$code' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET code_off='$code' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 2 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
}
if($opt == "code_money"){
	$code = cmsCore::request('hev_code', "str");
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET code_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET code_off='1' WHERE num_appl='".$inf_application2['id']."' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		$num_application2 = $inDB->get_field("cms_user_requests_for_payment", "user_id='$user_ids' AND status='2'","id");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 2 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/gf_gf_exchange.html'>
		<input type='hidden' name='id_zayavki' value='".$num_application2."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
}


if($opt == "dell_obmen"){
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
	
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
			
	$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa' AND status='0'");
	$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa_2' AND status='0'");
	
	$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
	$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");

	$model->EventHistory($id, "Отмена обмена заявки №: $number_aplication и № ".$inf_application_timer['zakaz_id_2'].".");
	
	cmsCore::redirect('/users/'.$login.'#otmena_obmen');
}
	//================================================================================================
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$bd_first_cheack = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_on");
		$click_us_2 = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_off");
	}
	
		
	if($bd_first_cheack == '0'){
		$infa_application = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication' AND user_id='$id'","*");
		$result_infa['id'] = $number_aplication;
		if($inf_application1['name_system'] != '0'){
			$result_infa['name_system1'] = $inf_application1['name_system'];
		}
		else{
			$result_infa['name_system1'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
		}
		if($inf_application2['name_system'] != '0'){
			$result_infa['name_system2'] = $inf_application2['name_system'];
		}
		else{
			$result_infa['name_system2'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
		}
		
		$result_infa['img_syst1'] = $inDB->get_field('cms_user_image_pc',"id_paym_syst='".$inf_application1['id_paym_syst']."'",'image');
		$result_infa['img_syst2'] = $inDB->get_field('cms_user_image_pc','id_paym_syst="'.$inf_application2['id_paym_syst'].'"','image');
		
		$result_infa['money_off'] = $infa_application['money_minus'];
		$result_infa['money_on'] = $infa_application['money_plus'];
		//var_dump($result_infa);
	}
	elseif($bd_first_cheack == '1'){
		if($click_us_2 == '1'){
			
		if($inf_application1['garant'] == '3'){
			$code = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on, code_off");
			$infa_application = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication' AND user_id='$id'","*");
			if($code['code_on'] == '0'){
				$infa_on['score_num'] = $infa_application['score_num'];
				if($inf_application1['name_system'] != '0'){
					$infa_on['name_system'] = $inf_application1['name_system'];
				}
				else{
					$infa_on['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
				}
				$infa_on['money_off'] = $infa_application['money_minus'];
				//var_dump($infa_on);
			}
			elseif($code['code_on'] != '0' && $code['code_off'] == '0'){
				$expectation = 1;
			}	
			elseif($code['code_on'] != '0' && $code['code_off'] != '0'){
				if($inf_application2['name_system'] != '0'){
					$heve_money['name_system_in'] = $inf_application2['name_system'];
				}
				else{
					$heve_money['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
				}
				$heve_money['code'] = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_off");
				
				$heve_money_garant = 1;
				
				$otpravka_message = $inDB->get_field("cms_user_code_protection","id='$id_zapusa'","status");
				if($otpravka_message == '0'){
					cmsUser::sendMessage(USER_MASSMAIL, $id, "Средства поступили на сайт, на Вашу систему ".$heve_money['name_system_in'].".");
					$inf_mail2 = array();
					$inf_mail2['email'] = $inDB->get_field("cms_users","id='$id'","email");
					$inf_mail2['message'] = "<p>Средства поступили на сайт, на Вашу систему <b>".$heve_money['name_system_in']."</b></p>";
					$email2 = $model->sendEmail3($inf_mail2);
					
					$infa_2 = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off, code_on");
					$infa_application = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
					if($inf_application1['name_system'] != '0'){
						$syst_2 = $inf_application1['name_system'];
					}
					else{
						$syst_2 = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
					}
					
					cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Код протекции для системы  ".$syst_2.": ".$infa_2['code_on']);
					$inf_mail3 = array();
					$inf_mail3['email'] = $inDB->get_field("cms_users","id='".$infa_2['user_id_off']."'","email");
					$inf_mail3['message'] = "<p><b>Система:</b>".$syst_2."</p>
						<p><b>Код протекции:</b>".$infa_2['code_on']."</p>";
					$email3 = $model->sendEmail3($inf_mail3);
				}
			}
		}
		elseif($inf_application1['garant'] == '1'){
			$code = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on, code_off");
			$infa_application = $inDB->get_fields("cms_user_application_timer","zakaz_id='$number_aplication' AND user_id='$id'","*");
			if($code['code_on'] == '0'){
				if($inf_application2['name_system'] != '0'){
					$infa_off['name_system'] = $inf_application2['name_system'];
				}
				else{
					$infa_off['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
				}
				$infa_off['money_on'] = $infa_application['money_plus'];
				//
			}
			elseif($code['code_on'] != '0' && $code['code_off'] == '0'){
				$expectation = 1;
			}	
			elseif($code['code_on'] != '0' && $code['code_off'] != '0'){
				if($inf_application2['name_system'] != '0'){
					$heve_money['name_system_in'] = $inf_application2['name_system'];
				}
				else{
					$heve_money['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
				}
				$heve_money['code'] = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_off");
				
				$otpravka_message = $inDB->get_field("cms_user_code_protection","id='$id_zapusa'","status");
				if($otpravka_message == '0'){
					cmsUser::sendMessage(USER_MASSMAIL, $id, "Код протекции для системы ".$heve_money['name_system_in'].": ".$heve_money['code']);
					$inf_mail2 = array();
					$inf_mail2['email'] = $inDB->get_field("cms_users","id='$id'","email");
					$inf_mail2['message'] = "<p><b>Система:</b>".$heve_money['name_system_in']."</p>
						<p><b>Код протекции:</b>".$heve_money['code']."</p>";
					$email2 = $model->sendEmail3($inf_mail2);
					
					$infa_2 = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off, code_on");
					$infa_application = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
					if($inf_application1['name_system'] != '0'){
						$syst_2 = $inf_application1['name_system'];
					}
					else{
						$syst_2 = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
					}
					
					cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Средства поступили на сайт, на Вашу систему ".$syst_2.".");
					$inf_mail3 = array();
					$inf_mail3['email'] = $inDB->get_field("cms_users","id='".$infa_2['user_id_off']."'","email");
					$inf_mail3['message'] = "<p>Средства поступили на сайт, на Вашу систему <b>".$syst_2."</b></p>";
					$email3 = $model->sendEmail3($inf_mail3);
				}
			}
		}
		//var_dump($infa_off);
		
		}
		else{$expectation = 1;}
	}
	
	if($heve_money){
		//=============================================================================
		//========================= % ставка для админа =============================
		//===========================================================================
		$procent_admin = 0.001;
		$inf_on_user = $inDB->get_fields("cms_user_replace_applications","id_application='$number_aplication'","*");
		$inf_off_user = $inDB->get_fields("cms_user_replace_applications","id_application='".$inf_application_timer['zakaz_id_2']."'","*");
		
		if($inf_application1['garant'] == '3'){//вернуть деньги на гарант + переписать заявку
			if($inf_on_user['status'] == '1'){
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$number_aplication' AND status='2'");
				$money_1 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application1['user_id']."'","expense_money");
				$money_admin = $inf_application_timer['money_minus']*$procent_admin;
				$money_pl = $inf_application_timer['money_minus'] - $money_admin;
				$result_money = $money_1 + $money_pl;
				//	echo $result_money."<br>";
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$result_money = str_replace(',', '.', $result_money);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
			
				$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE expense_type_id='4' AND user_id='".$inf_application1['user_id']."'");
			}
			elseif($inf_on_user['status'] == '0'){
				$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_on_user['money_off']."', masive_inf='".$inf_on_user['masive']."', status='0' WHERE id='$number_aplication' AND status='2'");
				$money_1 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application1['user_id']."'","expense_money");
				
				$money_admin = $inf_application_timer['money_minus']*$procent_admin;
				$money_pl = $inf_application_timer['money_minus'] - $money_admin;
				$result_money = $money_1 + $money_pl;
				//	echo $result_money."<br>";
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$result_money = str_replace(',', '.', $result_money);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE expense_type_id='4' AND user_id='".$inf_application1['user_id']."'");
			}
			
			if($inf_off_user['status'] == '1'){
				
				$money_admin = $inf_application_timer['money_plus']*$procent_admin;
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
			}
			elseif($inf_off_user['status'] == '0'){
				
				$money_admin = $inf_application_timer['money_plus']*$procent_admin;
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_off_user['money_off']."', masive_inf='".$inf_off_user['masive']."', status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
			}
		}
		elseif($inf_application1['garant'] == '1'){// переписать заявку
			if($inf_on_user['status'] == '1'){
				
				$money_admin = $inf_application_timer['money_minus']*$procent_admin;
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$number_aplication' AND status='2'");
			}
			elseif($inf_on_user['status'] == '0'){
				
				$money_admin = $inf_application_timer['money_minus']*$procent_admin;
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_on_user['money_off']."', masive_inf='".$inf_on_user['masive']."', status='0' WHERE id='$number_aplication' AND status='2'");
			}
			
			if($inf_off_user['status'] == '1'){
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
				$money_2 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application2['user_id']."'","expense_money");
				
				$money_admin = $inf_application_timer['money_plus']*$procent_admin;
				$money_pl = $inf_application_timer['money_plus'] - $money_admin;
				$result_money2 = $money_2 + $money_pl;
				//	echo $result_money."<br>";
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$result_money2 = str_replace(',', '.', $result_money2);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
			
				$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money2' WHERE expense_type_id='4' AND user_id='".$inf_application2['user_id']."'");
			}
			elseif($inf_off_user['status'] == '0'){
				$inDB->query("UPDATE cms_user_requests_for_payment SET money_off='".$inf_off_user['money_off']."', masive_inf='".$inf_off_user['masive']."', status='0' WHERE id='".$inf_application_timer['zakaz_id_2']."' AND status='2'");
				$money_2 = $inDB->get_field("cms_user_expenses","expense_type_id='4' AND user_id='".$inf_application2['user_id']."'","expense_money");
				
				$money_admin = $inf_application_timer['money_plus']*$procent_admin;
				$money_pl = $inf_application_timer['money_plus'] - $money_admin;
				$result_money2 = $money_2 + $money_pl;
				//	echo $result_money."<br>";
				$money_admin = $money_admin + $inDB->get_field('cms_user_score','user_id="0"','score_val');
				$money_admin = str_replace(',', '.', $money_admin);
				$result_money2 = str_replace(',', '.', $result_money2);
				$inDB->query("UPDATE cms_user_score SET score_val='$money_admin' WHERE user_id='0' ");
				
				$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money2' WHERE expense_type_id='4' AND user_id='".$inf_application2['user_id']."'");
			}
		}
		
		$data_report['user_id'] = $inf_application1['user_id'];
		$data_report['description'] = 'Обмен ГФ - гарант.';
		$data_report['currency'] = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
		$data_report['money'] = $inf_application_timer['money_minus'];
		$data_report['commission_serv'] = $inf_application_timer['money_minus']*$procent_admin;
		$data_report['commission_partner'] = 0;
		$model->EventReport($data_report);
		
		$data_report2['user_id'] = $inf_application2['user_id'];
		$data_report2['description'] = 'Обмен ГФ - гарант.';
		$data_report2['currency'] = $inDB->get_field("cms_user_requests_for_payment","id='".$inf_application_timer['zakaz_id_2']."'",'id_paym_syst');
		$data_report2['money'] = $inf_application_timer['money_plus'];
		$data_report2['commission_serv'] = $inf_application_timer['money_plus']*$procent_admin;
		$data_report2['commission_partner'] = 0;
		$model->EventReport($data_report2);
		
		
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$inf_application_timer['zakaz_id_2']."'");
				
		$inDB->query("UPDATE cms_user_code_protection SET status='1' WHERE id='$id_zapusa' AND status='0'");
		$inDB->query("UPDATE cms_user_code_protection SET status='1' WHERE id='$id_zapusa_2' AND status='0'");
		
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='$number_aplication'");
		$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$inf_application_timer['zakaz_id_2']."'");
		
		$model->EventHistory($id, "Заявка №: $number_aplication выполнена успешно.");
			
	}
}
else{
	cmsCore::redirect('/users/'.$login);
}
	cmsPage::initTemplate('components', 'com_users_gf_garant_exchange')->
		assign('opt', $opt)->
		assign('heve_money_garant', $heve_money_garant)->
		assign('time_out', $time_out)->
		assign('heve_money', $heve_money)->
		assign('infa_off', $infa_off)->
		assign('infa_on', $infa_on)->
		assign('code_podtv', $code_podtv)->
		assign('expectation', $expectation)->
		assign('result_infa', $result_infa)->
		assign('id_zapusa', $id_zapusa)->
		assign('id_zapusa_2', $id_zapusa_2)->
		assign('usr', $usr)->
		assign('number_aplication', $number_aplication)->
		assign('num_ap_2', $inf_application_timer['zakaz_id_2'])->
		assign('login', $login)->
		assign('inf_stand_appl', $inf_stand_appl)->
		assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
		assign('cfg', $model->config)->
		display('com_users_gf_garant_exchange.tpl');
	
}

//============================================================================//
//============================= Подтверждение заявки с промокодами длс обмена стандарт ============================//
//============================================================================//

if($do=='promo_code'){
	$inPage->setTitle("Подтверждение обмена");
	$user_id=$model->returnId();
	$id = $inUser->id;
	$login = $inDB->get_field("cms_users", "id='$user_id'", "login");
	$usr = $model->getUser($login);
	
	$errors = false;
	$opt = cmsCore::request("opt", "str");
	$number_aplication = cmsCore::request("id_zayavki","str");
	
	$what_user = cmsCore::request("what_user", "str");
	$id_scka = cmsCore::request("id_appl","str");
if($what_user ){
	$inDB->query("UPDATE cms_user_code_protection SET click_syst_num='$id_scka' WHERE num_appl='$number_aplication' AND  status='0'");
}
if($number_aplication){
	$id_appl_mas = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND  status='0'","click_syst_num");
	
	$id_zapusa = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND  status in (0,2)","id");
// -------------------- визначення часу (таймер) ----------------------
	$time_start = $inDB->get_field("cms_user_application_timer", "zakaz_id='$number_aplication'", 'time_start');
	$time_nuw = time();
	//$time_nuw = 1440746247;
	if(($time_nuw - $time_start) < 1800){
		$time_out = 30 - floor(($time_nuw - $time_start)/60);
	}
	else{
		if(!$time_start){cmsCore::redirect('/users/'.$login);}
		$inDB->query("UPDATE cms_user_standart_application SET status='1' WHERE system_id='$number_aplication'");
	
		$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
		
		$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
				
		$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa'");
		
		$model->EventHistory($id, "Время заявки №: $number_aplication истекло.");
		
		cmsCore::redirect('/users/'.$login.'#time_off');
				
	}
// ------------- запис в таблицю заявок першого підтвердження --------------
if($opt == 'first_cheack'){
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 1 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	elseif($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off")){
		$inDB->query("UPDATE cms_user_code_protection SET first_cheack_off='1' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 1 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	
	
	//cmsCore::redirect("/users/".$id."/promo_code.html");
}

if($opt == "code_prot_1"){
	$code = cmsCore::request('hev_code', "str");
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET code_on='$code' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 2 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	elseif($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off")){
		$inDB->query("UPDATE cms_user_code_protection SET code_off='$code' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 2 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	//cmsCore::redirect("/users/".$id."/promo_code.html");
}

if($opt == "code_2"){
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$inDB->query("UPDATE cms_user_code_protection SET verification_on='1' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 3 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	elseif($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off")){
		$inDB->query("UPDATE cms_user_code_protection SET verification_off='1' WHERE num_appl='$number_aplication' AND status='0'");
		$user_ids = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on");
		cmsUser::sendMessage(USER_MASSMAIL, $user_ids, "Другой пользователь подтвердил 3 уровень обмена <br>
		<form method='post' action='/users/".$user_ids."/promo_code.html'>
		<input type='hidden' name='id_zayavki' value='".$number_aplication."'>
		<input type='submit' value='перейти к обмену'>
		</form>");
	}
	
	cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Код протекции для системы № ".$syst_2.": ".$infa_2['code_off']);
	//cmsCore::redirect("/users/".$id."/promo_code.html");
}
if($opt == "dell_obmen"){
	$inDB->query("UPDATE cms_user_standart_application SET status='1' WHERE system_id='$number_aplication'");
	
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='$number_aplication' AND status='2'");
	
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
			
	$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE id='$id_zapusa'");

	$model->EventHistory($id, "Отмена обмена заявки №: $number_aplication.");
	
	cmsCore::redirect('/users/'.$login.'#otmena_obmen');
}	
	//$first_cheack = cmsCore::request("what_user","str");
	if($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on")){
		$bd_first_cheack_on = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_on");
		$click_us_2 = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_off");
	}
	elseif($number_aplication && $id == $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off")){
		$bd_first_cheack_off = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_off");
		$click_us_2 = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","first_cheack_on");
	}
		
if($bd_first_cheack_on == '0'){
	$infa_application = $inDB->get_fields("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst, money_off, id");
	$result_infa['id'] = $infa_application['id'];
	$result_infa['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$infa_application['id_paym_syst']."'","name_sustem");
	$result_infa['money_off'] = $infa_application['money_off'];
	$result_infa['syst_id'] = $infa_application['id_paym_syst'];
	//var_dump($result_infa);
}
elseif($bd_first_cheack_off == '0'){
	$perem333 = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","masive_inf");
	$perem333 = unserialize($perem333);
	foreach($perem333 as $key => $result){
		if($id_appl_mas == $result['sustem_id']){
			
			$result_infa['id'] = $number_aplication;
			$result_infa['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$result['sustem_id']."'","name_sustem");
			$result_infa['money_off'] = $result['money'];
		}
	}
	$result_infa['syst_id'] = $result['sustem_id'];
	//var_dump($result_infa);	
}
elseif($bd_first_cheack_on == '1'){
	if($click_us_2 == '1'){
	$code_on = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on, code_off");
	$infa_application = $inDB->get_fields("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst, money_off, id");
	if($code_on['code_on'] == '0'){
		$infa_on['score_num'] = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='".$infa_application['id_paym_syst']."'","score_num");
		$infa_on['name_system'] = $inDB->get_field("cms_user_type_payment","id='".$infa_application['id_paym_syst']."'","name_sustem");
		$infa_on['money_off'] = $infa_application['money_off'];
		$infa_on['syst_id'] = $infa_application['id_paym_syst'];
	}
	elseif($code_on['code_on'] != '0' && $code_on['code_off'] == '0'){
		$expectation = 1;
	}	
	elseif($code_on['code_on'] != '0' && $code_on['code_off'] != '0'){
		$last_cheack = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","verification_on, verification_off");
		if($last_cheack['verification_on'] == '0'){
			$code_podtv['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='$id_appl_mas'","name_sustem");
			$perem333 = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","masive_inf");
			$perem333 = unserialize($perem333);
			foreach($perem333 as $key => $result){
				if($id_appl_mas == $result['sustem_id']){
					$code_podtv['money_in'] = $result['money'];
				}
			}
			//var_dump($code_podtv);
		}
		elseif($last_cheack['verification_on'] == '1' && $last_cheack['verification_off'] == '0'){
			$expectation = 1;
		}
		elseif($last_cheack['verification_on'] == '1' && $last_cheack['verification_off'] == '1'){
			$heve_money['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='$id_appl_mas'","name_sustem");
			$heve_money['code'] = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_off");
			
			$otpravka_message = $inDB->get_field("cms_user_code_protection","id='$id_zapusa'","status");
			if($otpravka_message == '0'){
			cmsUser::sendMessage(USER_MASSMAIL, $id, "Код протекции для системы № ".$heve_money['name_system_in'].": ".$heve_money['code']);
			$inf_mail2 = array();
			$inf_mail2['email'] = $inDB->get_field("cms_users","id='$id'","email");
			$inf_mail2['message'] = "<p><b>Система:</b>".$heve_money['name_system_in']."</p>
				<p><b>Код протекции:</b>".$heve_money['code']."</p>";
			$email2 = $model->sendEmail3($inf_mail2);
			
			$infa_2 = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_off, code_on");
			$infa_application = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
			$syst_2 = $inDB->get_field("cms_user_type_payment","id='$infa_application'","name_sustem");
			
			cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_off'], "Код протекции для системы № ".$syst_2.": ".$infa_2['code_on']);
			$inf_mail3 = array();
			$inf_mail3['email'] = $inDB->get_field("cms_users","id='".$infa_2['user_id_off']."'","email");
			$inf_mail3['message'] = "<p><b>Система:</b>".$syst_2."</p>
				<p><b>Код протекции:</b>".$infa_2['code_on']."</p>";
			$email3 = $model->sendEmail3($inf_mail3);
			}
			
			$perem333 = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","masive_inf");
			$perem333 = unserialize($perem333);
			foreach($perem333 as $key => $result){
				if($id_appl_mas == $result['sustem_id']){
					$money_off = $result['money'];
				}
			}
			
			$data_report['user_id'] = $id;
			$data_report['description'] = 'Обмен внутренняя верификация.';
			$data_report['currency'] = $id_appl_mas;
			$data_report['money'] = $money_off;
			$model->EventReport($data_report);
			
			$data_report2['user_id'] = $infa_2['user_id_on'];
			$data_report2['description'] = 'Обмен внутренняя верификация.';
			$data_report2['currency'] = $infa_application;
			$data_report2['money'] = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","money_off");
			$model->EventReport($data_report2);
			
		}
	}
	}
	else{$expectation = 1;}
}
elseif($bd_first_cheack_off == '1'){
	if($click_us_2 == '1'){
	$code_off = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on, code_off");
	
	if($code_off['code_off'] == '0'){
		$infa_off['score_num'] = $inDB->get_field("cms_user_score","user_id='$id' AND type_payment_id='$id_appl_mas'","score_num");
		$infa_off['name_system'] = $inDB->get_field("cms_user_type_payment","id='$id_appl_mas'","name_sustem");
		$perem333 = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","masive_inf");
		$perem333 = unserialize($perem333);
		foreach($perem333 as $key => $result){
			if($id_appl_mas == $result['sustem_id']){
				$infa_off['money_off'] = $result['money'];
			}
		}
		$infa_off['syst_id'] = $id_appl_mas;
	}
	elseif($code_off['code_off'] != 0 && $code_off['code_on'] == 0){
		$expectation = 1;
	}	
	elseif($code_off['code_off'] != 0 && $code_off['code_on'] != 0){
		$last_cheack = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","verification_on, verification_off");
		if($last_cheack['verification_off'] == '0'){
			$infa_application = $inDB->get_fields("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst, money_off, id");
			$code_podtv['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='".$infa_application['id_paym_syst']."'","name_sustem");
			$code_podtv['money_in'] = $infa_application['money_off'];
		}
		elseif($last_cheack['verification_off'] == '1' && $last_cheack['verification_on'] == '0'){
			$expectation = 1;
		}
		elseif($last_cheack['verification_off'] == '1' && $last_cheack['verification_on'] == '1'){
			$infa_application = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","id_paym_syst");
			$heve_money['name_system_in'] = $inDB->get_field("cms_user_type_payment","id='$infa_application'","name_sustem");
			$heve_money['code'] = $inDB->get_field("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","code_on");
			$otpravka_message = $inDB->get_field("cms_user_code_protection","id='$id_zapusa'","status");
			if($otpravka_message == '0'){
			cmsUser::sendMessage(USER_MASSMAIL, $id, "Код протекции для системы № ".$heve_money['name_system_in'].": ".$heve_money['code']);
			$inf_mail2 = array();
			$inf_mail2['email'] = $inDB->get_field("cms_users","id='$id'","email");
			$inf_mail2['message'] = "<p><b>Система:</b>".$heve_money['name_system_in']."</p>
				<p><b>Код протекции:</b>".$heve_money['code']."</p>";
			$email2 = $model->sendEmail3($inf_mail2);
			
			$infa_2 = $inDB->get_fields("cms_user_code_protection","num_appl='$number_aplication' AND status='0'","user_id_on, code_off");
			$syst_2 = $inDB->get_field("cms_user_type_payment","id='$id_appl_mas'","name_sustem");
			
			cmsUser::sendMessage(USER_MASSMAIL, $infa_2['user_id_on'], "Код протекции для системы № ".$syst_2.": ".$infa_2['code_off']);
			$inf_mail3 = array();
			$inf_mail3['email'] = $inDB->get_field("cms_users","id='".$infa_2['user_id_on']."'","email");
			$inf_mail3['message'] = "<p><b>Система:</b>".$syst_2."</p>
				<p><b>Код протекции:</b>".$infa_2['code_off']."</p>";
			$email3 = $model->sendEmail3($inf_mail3);
			}
			
			$data_report['user_id'] = $id;
			$data_report['description'] = 'Обмен внутренняя верификация.';
			$data_report['currency'] = $infa_application;
			$data_report['money'] = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","money_off");
			$model->EventReport($data_report);
			
			$perem333 = $inDB->get_field("cms_user_requests_for_payment","id='$number_aplication'","masive_inf");
			$perem333 = unserialize($perem333);
			foreach($perem333 as $key => $result){
				if($id_appl_mas == $result['sustem_id']){
					$money_off = $result['money'];
				}
			}
			
			$data_report2['user_id'] = $infa_2['user_id_on'];
			$data_report2['description'] = 'Обмен внутренняя верификация.';
			$data_report2['currency'] = $id_appl_mas;
			$data_report2['money'] = $money_off;
			$model->EventReport($data_report2);
			
		}
	}
	}
	else{$expectation = 1;}	
	
}
if($heve_money){
	
	$user_off_on = $inDB->get_fields("cms_user_code_protection","id='$id_zapusa'","user_id_on, user_id_off");
	
	$inDB->query("UPDATE cms_user_standart_application SET status='2' WHERE system_id='$number_aplication'");

	$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$number_aplication' AND status='2'");
	
	$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='$number_aplication'");
			
	$inDB->query("UPDATE cms_user_code_protection SET status='1' WHERE id='$id_zapusa'");
	
	$model->EventHistory($id, "Заявка №: $number_aplication выполнена успешно.");
		
}
}
else{
	cmsCore::redirect('/users/'.$login);
}	

	cmsPage::initTemplate('components', 'com_users_promo_code')->
		assign('usr', $usr)->
		assign('expectation', $expectation)->
		assign('heve_money', $heve_money)->
		assign('number_aplication', $number_aplication)->
		assign('result_infa', $result_infa)->
		assign('name_paym_system_minus', $name_paym_system_minus)->
		assign('name_paym_system_plus', $name_paym_system_plus)->
		assign('sustem_id', $sustem_id)->
		assign('time_out', $time_out)->
		assign('num_application', $num_application)->
		assign('code_podtv', $code_podtv)->
		assign('id_zapusa', $id_zapusa)->
		assign('wait_inf', $wait_inf)->
		assign('infa_on', $infa_on)->
		assign('infa_off', $infa_off)->
		display('com_users_promo_code.tpl');
}
//============================================================================//
//============================= Просмотр профиля  ============================//
//============================================================================//
if ($do=='profile'){
	

	if(isset($_POST['buyDef'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$login.'#errorCaptcha');
        }
		
		$model->buyAccount($_POST);
		cmsCore::redirect('/users/'.$login.'#successBuy');
	}
	if(isset($_POST['count'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$login.'#errorCaptcha');
        }
		
		isset($_POST['price']) ? $model->sellToExchange($_POST) : $model->sellToService($_POST);
		
		if(isset($_POST['price']))
			cmsCore::redirect('/users/'.$login.'#successExchange');
		else
			cmsCore::redirect('/users/'.$login.'#successServise');
	}

	if(isset($_POST['new-price'])){
		
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$login.'#errorCaptcha');
        }
		
		$model->changeStatus($_POST);
		
		cmsCore::redirect('/users/'.$login.'#successChange');
	}
	
	if(isset($_POST['remove-success'])){
		
		$model->removePamms($_POST['remove-success']);
		
		//cmsCore::redirect('/users/'.$login.'#successRemove');
	}
	
    $inPage->addHeadJsLang(array('NEW_POST_ON_WALL','CONFIRM_DEL_POST_ON_WALL'));

	// если просмотр профиля гостям запрещен
	if (!$inUser->id && !$model->config['sw_guest']) {
        cmsUser::goToLogin();
	}

		
    if(is_numeric($login)) { cmsCore::error404(); }

    $usr = $model->getUser($login);
    if (!$usr){ cmsCore::error404(); }
	$user_id = $usr['id'];
    $myprofile  = ($inUser->id == $usr['id']);

    $inPage->setTitle($usr['nickname']);
    $inPage->addPathway($usr['nickname']);

	// просмотр профиля запрещен
    if (!cmsUser::checkUserContentAccess($usr['allow_who'], $usr['id'])){
        cmsPage::initTemplate('components', 'com_users_not_allow')->
                assign('is_auth', $inUser->id)->
                assign('usr', $usr)->
                display('com_users_not_allow.tpl');
        return;
    }
	// Профиль удален
    if ($usr['is_deleted']){
        cmsPage::initTemplate('components', 'com_users_deleted.tpl')->
                assign('usr', $usr)->
            
                assign('is_admin', $inUser->is_admin)->
                assign('others_active', $inDB->rows_count('cms_users', "login='{$usr['login']}' AND is_deleted=0", 1))->
                display('com_users_deleted.tpl');
        return;
    }

	// Данные о друзьях
	$usr['friends_total'] = cmsUser::getFriendsCount($usr['id']);
	$usr['friends']		  = cmsUser::getFriends($usr['id']);
	// очищать сессию друзей если в своем профиле и количество друзей из базы не совпадает с количеством друзей в сессии
	if ($myprofile && sizeof($usr['friends']) != $usr['friends_total']) { cmsUser::clearSessionFriends(); }
	// обрезаем список
	$usr['friends'] = array_slice($usr['friends'], 0, 6);
	// выясняем друзья ли мы с текущим пользователем
    $usr['isfriend'] = !$myprofile ? cmsUser::isFriend($usr['id']) : false;

	// награды пользователя
    $usr['awards'] = $model->config['sw_awards'] ? $model->getUserAwards($usr['id']) : false;

	// стена
	if($model->config['sw_wall']){
		$inDB->limitPage(1, $model->config['wall_perpage']);
        $usr['wall_html'] = cmsUser::getUserWall($usr['id'], 'users', $myprofile, $inUser->is_admin);
	}

	// можно ли пользователю изменять карму
    $usr['can_change_karma'] = $model->isUserCanChangeKarma($usr['id']) && $inUser->id;


	// Фотоальбомы пользователя
    if ($model->config['sw_photo']){
        $usr['albums']       = $model->getPhotoAlbums($usr['id'], $usr['isfriend'], !$inCore->isComponentEnable('photos'));
        $usr['albums_total'] = sizeof($usr['albums']);
        $usr['albums_show']  = 6;
        if ($usr['albums_total']>$usr['albums_show']){
            array_splice($usr['albums'], $usr['albums_show']);
        }
    }

    $usr['board_count']    = $model->config['sw_board'] ?
								$inDB->rows_count('cms_board_items', "user_id='{$usr['id']}' AND published=1") : 0;
    $usr['comments_count'] = $model->config['sw_comm'] ?
								$inDB->rows_count('cms_comments', "user_id='{$usr['id']}' AND published=1") : 0;
	$usr['forum_count']    = $model->config['sw_forum'] ?
								$inDB->rows_count('cms_forum_posts', "user_id = '{$usr['id']}'") : 0;
	$usr['files_count']    = $model->config['sw_files'] ?
								$inDB->rows_count('cms_user_files', "user_id = '{$usr['id']}'") : 0;

	$cfg_reg = $inCore->loadComponentConfig('registration');
	$usr['invites_count'] = ($inUser->id && $myprofile && $cfg_reg['reg_type'] == 'invite') ?
								$model->getUserInvitesCount($inUser->id) : 0;

	$usr['blog'] = $model->config['sw_blogs'] ?
								$inDB->get_fields('cms_blogs', "user_id = '{$usr['id']}' AND owner = 'user'", 'title, seolink') : false;

    $usr['form_fields'] = array();
	if (is_array($model->config['privforms'])){
		foreach($model->config['privforms'] as $form_id){
			$usr['form_fields'] = array_merge($usr['form_fields'], cmsForm::getFieldsValues($form_id, $usr['formsdata']));
		}
	}
	
    $plugins = $model->getPluginsOutput($usr);
	
	/* $pamms = $model->pamms();
	$pamms_related = $model->pamms_related(); */
	
	/* user_expenses - счета пользователя */
	$user_expenses = $inDB->get_table("cms_user_expenses", "user_id = '{$inUser->id}'", "*");
	
	foreach($user_expenses as $key => $expense){
		$expense_info = $inDB->get_fields('cms_user_expense_type', "id = '{$expense['expense_type_id']}' AND expense_status = '1'", "expense_title, expense_code, expense_status");
		$expense['expense_title'] 	= $expense_info['expense_title'];
		$expense['expense_code'] 	= $expense_info['expense_code'];
		$expense['expense_outpay'] 	/=100;
		$expense['expense_inpay'] 	/=100;
		$expense['expense_money'] 	/=100;
		$expense['expense_status'] 	= $expense_info['expense_status'];
		$expense['currency'] 		= $inDB->get_field('cms_currency', "id = '{$expense['currency_id']}' AND status = '1'", "title");
		//echo "<pre>"; var_dump($expense);
		$expenses[$expense['expense_code']] = $expense;
	}
	$infa_gf = $inDB->get_fields("cms_user_expenses","user_id='{$inUser->id}' AND expense_type_id='4'","*");
	$inf_gf['title'] = $inDB->get_field('cms_user_expense_type',"id='4'","expense_title");
	$inf_gf['outpay'] = $infa_gf['expense_outpay'];
	$inf_gf['inpay'] = $infa_gf['expense_inpay'];
	$inf_gf['money'] = $infa_gf['expense_money'];
	//echo "<pre>"; var_dump($expenses);
	
	/* достаем памм портфели юзера*/
	
	$pamm_relations = $inDB->get_table("cms_pamm_relations", "user_id = '{$inUser->id}'", "*");
	//echo "<pre style='color: red;'>"; var_dump($pamm_relations); echo "</pre>";
	
	foreach($pamm_relations as $key => $relation){
		/*echo "<pre style='color: green;'>"; var_dump($relation); echo "</pre>";
		echo "<pre style='color: red;'>"; var_dump($pamm); echo "</pre>";*/
		$pamm = $inDB->get_fields('cms_pamms', "id = {$relation['pamm_id']}", '*');
		$phpdate = strtotime($pamm['end_date']);
		$pamm['end_date'] = date( 'd.m.y', $phpdate );
		$pamm['currency'] = $inDB->get_field('cms_currency', "id = '{$pamm['currency_id']}' AND status = '1'", "title");
		$pamm['outpay'] /=100;
		$pamm['inpay'] /=100;
		$pamm['money'] /=100;
		$pamm['capitalization'] /=100;
		$pamm['income'] /=100;
		$user_pamms[$key]['relation'] = $relation;
		$user_pamms[$key]['pamm_info'] = $pamm;
	}
	
	/* достаем все памм портфели */
	
	$pamms_info = $inDB->get_table("cms_pamms", "1 = 1", "*");
	
	foreach($pamms_info as $key => $pamm){
		$phpdate = strtotime($pamm['end_date']);
		$pamm['end_date'] = date( 'd.m.y', $phpdate );
		$pamm['currency'] = $inDB->get_field('cms_currency', "id = '{$pamm['currency_id']}' AND status = '1'", "title");
		$pamm['outpay'] /=100;
		$pamm['inpay'] /=100;
		$pamm['money'] /=100;
		$pamm['capitalization'] /=100;
		$pamm['income'] /=100;
		$all_pamms[] = $pamm;
	}
	//echo "<pre>"; var_dump($user_pamms); echo "</pre>";
	
	/* PARSER */
	
	/*cmsCore::includeFile('includes/simple_html_dom.php');
	defined( 'MAX_FILE_SIZE' ) || define( 'MAX_FILE_SIZE', 600000 );
	
	defined( 'DEFAULT_TARGET_CHARSET' ) || define( 'DEFAULT_TARGET_CHARSET', 'UTF-8' );
	defined( 'DEFAULT_BR_TEXT' ) || define( 'DEFAULT_BR_TEXT', "\x0d\x0a" );
	defined( 'DEFAULT_SPAN_TEXT' ) || define( 'DEFAULT_SPAN_TEXT', "\x20" );
	
	ini_set("max_execution_time", "20000");
	ini_set('allow_url_fopen','1');
	
	/**/
	
	//$html = new simple_html_dom();
	//$html->load_file('http://preparat.org.ua/index.php');
	/*
	$url = "http://fx-trend.com/pamm_indexes/";
	$url = "http://panteon-finance.com/pammfunds/Detail/1";
	$html = file_get_html($url);
	
	//$html = pars_page($url); // загружаем страницу в переменную
	echo "<pre>"; var_dump($html->outertext);
	if($html->innertext!=''){
		$ol_spisok = $html->find('.csn_message_list li');
	}
	//var_dump($ol_spisok);
	foreach($ol_spisok as $li){
		var_dump($li->outertext);
	}*/
	/* -------- */
	$zapros_standart_obmen = $inDB->get_table("cms_user_temporary_standart","user_owner='$user_id'");
	if($zapros_standart_obmen){
		$id_prov = 0;
		foreach($zapros_standart_obmen as $result ){
			if($result['system_id'] != $id_prov){
				$inf_standart['id'] = $result['system_id'];
				$id_prov = $result['system_id'];
				$provisional_application[] = $inf_standart;
			}
		}
	}
	
	
	$standart_application = $inDB->get_table("cms_user_standart_application", "user_id='".$inUser->id."' AND status='0'");
	if($standart_application){
		foreach($standart_application as $result){
			$vvv = $inDB->get_fields("cms_user_requests_for_payment","id='".$result['system_id']."'","id, garant, id_paym_syst, money_off, masive_inf, user_id");
			
			$time_start = $inDB->get_field("cms_user_application_timer","zakaz_id='".$vvv['id']."'","time_start");
			
			$stand_appl['id'] = $vvv['id'];
			$stand_appl['user_id'] = $vvv['user_id'];
			$stand_appl['garant'] = $vvv['garant'];
			$stand_appl['id_paym_syst'] = $vvv['id_paym_syst'];
			$stand_appl['name_sys_return'] = $inDB->get_field("cms_user_type_payment","id='".$vvv['id_paym_syst']."'","name_sustem");
			$stand_appl['money_off'] = $vvv['money_off'];
			$masivs = unserialize($vvv['masive_inf']);
			
			foreach($masivs as $result2){
				$lllk['id_sys_give'] = $result2['sustem_id'];
				if(isset($result2['sustem_name']) && $result2['sustem_name'] != '0'){
					$lllk['name_sys_give'] = $result2['sustem_name'];
				}else{
					$lllk['name_sys_give'] = $inDB->get_field("cms_user_type_payment","id='".$result2['sustem_id']."'","name_sustem");
				}
				$lllk['money'] = $result2['money'];
				$lllk['kurs'] = $result2['kurs'];
				$perem118 = $stand_appl['money_off']/$result2['money'];
				$perem118 = str_replace(',', '.', $perem118);
				$lllk['kurs_obr'] = str_replace(',', '.', round($perem118, 3));
				$massiv[] = $lllk;
				unset($perem118);
			}
			$stand_appl['masiv'] = $massiv;
			
			//var_dump($stand_appl);
			
			$id_in_table = $inDB->get_field("cms_user_code_protection", "(num_appl_2='".$infa['zakaz_id']."' AND user_id_on='$user_id' AND status='0') OR (num_appl_1='".$infa['zakaz_id']."' AND user_id_off='$user_id' AND status='0')", "user_id_on");
			
			$stand_appl['id_zapusa'] = $inDB->get_field("cms_user_code_protection", "num_appl='".$vvv['id']."' AND status='0'", "id");
			$prov_click = $inDB->get_field("cms_user_code_protection", "num_appl='".$vvv['id']."' AND status='0'", "click_syst_num");
			if( $prov_click == '0'){
				$stand_appl['id_click'] = 0;
			}
			else{
				$stand_appl['id_click'] = $prov_click;
			}
			$time_nuw = time();
			
			if(($time_nuw - $time_start) < 1800){
				$stand_appl['time'] = 30 - floor(($time_nuw - $time_start)/60);
			}
			else{
				$inDB->query("UPDATE cms_user_standart_application SET status='1' WHERE system_id='".$vvv['id']."'");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$vvv['id']."'");
				
				$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$vvv['id']."'");
						
				$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$vvv['id']."' AND status='0'");
				
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='1' WHERE system_id='".$vvv['id']."'");
				
				$model->EventHistory($inUser->id, "Время заявки №: ".$vvv['id']." истекло.");
				
				$change = 1;
			}
			$applications[] = $stand_appl;
		}
	}
	
	$gf_gf_application = $inDB->get_table("cms_user_application_timer", "user_id='".$inUser->id."' AND type='1'");
	if($gf_gf_application){
		foreach($gf_gf_application as $result){
			
			$time_start = $result['time_start'];
			
			$gf_gf_appl['id'] = $result['zakaz_id'];
			$gf_gf_appl['id_2'] = $result['zakaz_id_2'];
			
			$inf_application1 = $inDB->get_fields("cms_user_requests_for_payment","id='".$result['zakaz_id']."'","*");
			$inf_application2 = $inDB->get_fields("cms_user_requests_for_payment","id='".$result['zakaz_id_2']."'","*");
			if($inf_application1['name_system'] != '0'){
				$gf_gf_appl['name_paym_syst_1'] = $inf_application1['name_system'];
			}
			else{
				$gf_gf_appl['name_paym_syst_1'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
			}
			if($inf_application2['name_system'] != '0'){
				$gf_gf_appl['name_paym_syst_2'] = $inf_application2['name_system'];
			}
			else{
				$gf_gf_appl['name_paym_syst_2'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
			}
			$gf_gf_appl['money_off'] = $result['money_minus'];
			$gf_gf_appl['money_on'] = $result['money_plus'];
			
			$gf_gf_appl['kurs'] = str_replace(",",".", (round(($result['money_plus']/$result['money_minus']),3)));
			$gf_gf_appl['obr_kurs'] = str_replace(",",".", (round(($result['money_minus']/$result['money_plus']),3)));
			$gf_gf_appl['id_zapusa'] = $inDB->get_field("cms_user_code_protection", "num_appl='".$result['zakaz_id']."' AND status='0'", "id");
			$gf_gf_appl['id_zapusa_2'] = $inDB->get_field("cms_user_code_protection", "num_appl='".$result['zakaz_id_2']."' AND status='0'", "id");
			
			
			$time_nuw = time();
			
			if(($time_nuw - $time_start) < 1800){
				$gf_gf_appl['time'] = 30 - floor(($time_nuw - $time_start)/60);
			}
			else{
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$result['zakaz_id']."'");
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$result['zakaz_id_2']."'");
				
				$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$result['zakaz_id']."'");
				$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$result['zakaz_id_2']."'");
						
				$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$result['zakaz_id']."' AND status='0'");
				$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$result['zakaz_id_2']."' AND status='0'");
				
				$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$result['zakaz_id']."'");
				$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$result['zakaz_id_2']."'");
				
				$model->EventHistory($inUser->id, "Время заявок №: ".$result['zakaz_id']." и №: ".$result['zakaz_id_2']." истекло.");
				
				$change = 1;
			}
			$application_gf_gf[] = $gf_gf_appl;
		}
	}
	
	$gf_garant_application = $inDB->get_table("cms_user_application_timer", "user_id='".$inUser->id."' AND type='2'");
	if($gf_garant_application){
		foreach($gf_garant_application as $result){
			
			$time_start = $result['time_start'];
			
			$gf_garant_appl['id'] = $result['zakaz_id'];
			$gf_garant_appl['id_2'] = $result['zakaz_id_2'];
			
			$inf_application1 = $inDB->get_fields("cms_user_requests_for_payment","id='".$result['zakaz_id']."'","*");
			$inf_application2 = $inDB->get_fields("cms_user_requests_for_payment","id='".$result['zakaz_id_2']."'","*");
			if($inf_application1['name_system'] != '0'){
				$gf_garant_appl['name_paym_syst_1'] = $inf_application1['name_system'];
			}
			else{
				$gf_garant_appl['name_paym_syst_1'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application1['id_paym_syst']."'","name_sustem");
			}
			if($inf_application2['name_system'] != '0'){
				$gf_garant_appl['name_paym_syst_2'] = $inf_application2['name_system'];
			}
			else{
				$gf_garant_appl['name_paym_syst_2'] = $inDB->get_field("cms_user_type_payment","id='".$inf_application2['id_paym_syst']."'","name_sustem");
			}
			$gf_garant_appl['money_off'] = $result['money_minus'];
			$gf_garant_appl['money_on'] = $result['money_plus'];
			
			$gf_garant_appl['kurs'] = str_replace(",",".", (round(($result['money_plus']/$result['money_minus']),3)));
			$gf_garant_appl['obr_kurs'] = str_replace(",",".", (round(($result['money_minus']/$result['money_plus']),3)));
			$gf_garant_appl['id_zapusa'] = $inDB->get_field("cms_user_code_protection", "num_appl='".$result['zakaz_id']."' AND status='0'", "id");
			$gf_garant_appl['id_zapusa_2'] = $inDB->get_field("cms_user_code_protection", "num_appl='".$result['zakaz_id_2']."' AND status='0'", "id");
			
			
			$time_nuw = time();
			
			if(($time_nuw - $time_start) < 1800){
				$gf_garant_appl['time'] = 30 - floor(($time_nuw - $time_start)/60);
			}
			else{
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$result['zakaz_id']."'");
				$inDB->query("UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$result['zakaz_id_2']."'");
				
				$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$result['zakaz_id']."'");
				$inDB->query("DELETE FROM cms_user_application_timer WHERE zakaz_id='".$result['zakaz_id_2']."'");
						
				$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$result['zakaz_id']."' AND status='0'");
				$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$result['zakaz_id_2']."' AND status='0'");
				
				$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$result['zakaz_id']."'");
				$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$result['zakaz_id_2']."'");
				
				$model->EventHistory($inUser->id, "Время заявок №: ".$result['zakaz_id']." и №: ".$result['zakaz_id_2']." истекло.");
				
				$change = 1;
			}
			$application_gf_garant[] = $gf_garant_appl;
		}
	}
	
	if($change){
		cmsCore::redirect('/users/'.$login);
	}
	
	$garant_money = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
$opt = cmsCore::request("opt", "str");
	
if($opt == "application_standart"){
	$user_priem_id = cmsCore::request("user_id","str");
	$payment_priem_id = cmsCore::request("payment_id","str");
	
	$table_standart = $inDB->get_table('cms_user_temporary_standart',"user_owner='$user_id'");
	$id_prov_1 = 0;
	$id_prov_2 = array();
	$id_prov_2[] = 0;
	
	foreach($table_standart as $result){
		//var_dump($result['user_id']." ; ".$user_priem_id);
		if($result['user_id'] == $user_priem_id && $id_prov_1 != $user_priem_id){
			cmsUser::sendMessage(USER_MASSMAIL, $result['user_id'], "Владелец заявки № $payment_priem_id согласился с Вами начать обмен;");
			$id_prov_1 = $user_priem_id;
			$id_prov_2[] = $user_priem_id;
		}
		else{
			$dd = 0;
			foreach($id_prov_2 as $result2){
				if($result['user_id'] == $result2){
					$dd = 1;
				}
			}
			if($dd == 0){
			cmsUser::sendMessage(USER_MASSMAIL, $result['user_id'], "Владелец заявки № $payment_priem_id начал обмен с другим пользователем;");	
			}
			else{
				$id_prov_2[] = $result['user_id'];
			}
		}
	}
	
	$inDB->query("DELETE FROM cms_user_temporary_standart WHERE system_id='$payment_priem_id'");
	$inDB->query("INSERT cms_user_standart_application (user_id, system_id, status) VALUES ('$user_priem_id','$payment_priem_id','0')");
	$inDB->query("INSERT cms_user_standart_application (user_id, system_id, status) VALUES ('$user_id','$payment_priem_id','0')");
	$inDB->query("UPDATE cms_user_requests_for_payment SET status='2' WHERE id='$payment_priem_id'");
	$time_start = time();
	$inDB->query("INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus ,money_minus, id_sys_plus, money_plus, time_start) VALUES ('$user_id','$payment_priem_id','','','','','$time_start')");
	
	$sql1133 = $inDB->query("INSERT cms_user_code_protection (num_appl, user_id_on, user_id_off, code_on, code_off ,status) VALUES ('$payment_priem_id','$user_id','$user_priem_id','0','0','0')");
	
	cmsCore::redirect('/users/'.$login);
	
}
	
if($opt == "save_change"){
	/*------- изменение заявки на выплату ------*/
	/* 
		1 - id
		2 - id_user
		3 - 
		4 - ід системи с которой отдает
		5 - деньги что отдает
		6 - масив (id системы оплаты + счет перевода + курс + деньги )
	
	*/
	
	$score = cmsCore::request("score_s", "array");//
	$system_name = cmsCore::request("name_paym_system", "array");//
	$system_money = cmsCore::request("money", "array");//
	$kurs = cmsCore::request("kurs_s", "array");//
	$payment_user = cmsCore::request("user_money", 'str');//
	$payment_system_ids = cmsCore::request("payment_system_id", 'str');
	$payment_system_id = $inDB->get_field("cms_user_type_payment", "name_sustem='$payment_system_ids'", 'id');//
	$sell_parts = cmscore::request("sell_parts", 'str');//
	$id_table = cmscore::request("idshka", 'str');
	$i = 0;
	/*
	1 - id системы оплаты; 
	2 - счет перевода;
	3 - курс;
	4 - деньги;
	*/
	foreach($system_name as $key => $systems){
		$system_id = $inDB->get_field("cms_user_type_payment", "name_sustem='$systems'", 'id');
		$kurs[$i] = str_replace(',', '.', round(str_replace(",", ".", $kurs[$i]), 3)); 
		$system_money[$i] = str_replace(',', '.', round(($kurs[$i]*$payment_user), 3));
		$masiv = array("sustem_id"=>$system_id, "score"=>$score[$i], "kurs"=>$kurs[$i], "money"=>$system_money[$i]);				
		
		$masivs[$i] = $masiv;
		$i++;
	}
	$min_array = serialize($masivs);
	if($sell_parts == "on"){$sell_parts = 1;}else{$sell_parts = 0;}
	
	$inf_application = $inDB->get_fields("cms_user_requests_for_payment","id='$id_table' AND user_id='".$usr['id']."'","id_paym_syst, money_off, garant");
	if($inf_application['garant'] == '1'){
		$score_val = $inDB->get_field("cms_user_score","user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'","score_val");
		
		$result_money = $score_val+$inf_application['money_off'];
		$result_money = str_replace(',', '.', $result_money);
				
		$sql_2 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'";
		$inDB->query($sql_2);
		
		$score_val = $inDB->get_field("cms_user_score","user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'","score_val");
		
		$result_money = $score_val-$payment_user;
		$result_money = str_replace(',', '.', $result_money);
		$sql_1 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='$id' AND type_payment_id='$payment_system_id'";
		$inDB->query($sql_1);
	}
	elseif($inf_application['garant'] == '3'){
		$GF = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
		
		$result_money = $GF + $inf_application['money_off'];
		$pluse_money = $inf_application['money_off'];
		$result_money = str_replace(',', '.', $result_money);
		$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='$user_id' AND expense_type_id='4'");
		
		$GF = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
		
		$result_money = $GF - ($payment_user);
		$result_money = str_replace(',', '.', $result_money);
		$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='$user_id' AND expense_type_id='4'");
		
	}
	$payment_user = str_replace(',', '.', $payment_user);
	$sql113 = "UPDATE cms_user_requests_for_payment SET money_off='$payment_user', masive_inf='$min_array', sell_parts='$sell_parts' WHERE id='$id_table' AND user_id='".$usr['id']."'";
	
	$zapros = $inDB->query($sql113);
	
	$model->EventHistory($usr['id'], "Заяка № $id_table изменена");
	
	if($zapros == true){cmsCore::redirect('/users/'.$usr['login'].'#save_change');}
	
}
if($opt == "del_application"){
	$id_application = cmsCore::request("id_application", 'str');
	
	$inf_application = $inDB->get_fields("cms_user_requests_for_payment","id='$id_application'","garant, id_paym_syst, money_off");
	if($inf_application['garant'] == '1'){
		$score_val = $inDB->get_field("cms_user_score","user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'","score_val");
		$result_money = $score_val+$inf_application['money_off'];
		$result_money = str_replace(',', '.', $result_money);
		$sql_2 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'";
		$inDB->query($sql_2);
	}
	elseif($inf_application['garant'] == '3'){
		$GF = $inDB->get_field("cms_user_expenses", "user_id='$user_id' AND expense_type_id='4'", "expense_money");
		$result_money = $GF + ($inf_application['money_off']);
		//echo $result_money;
		$result_money = str_replace(',', '.', $result_money);
		$inDB->query("UPDATE cms_user_expenses SET expense_money='$result_money' WHERE user_id='$user_id' AND expense_type_id='4'");
	}
	
	$asdasdass = "DELETE FROM cms_user_requests_for_payment WHERE id='$id_application'";
	$inDB->query($asdasdass);
	
	$model->EventHistory($usr['id'], "Заяка № $id_application удалена");
	
	cmsCore::redirect('/users/'.$usr['login'].'#dell_application');
}
	$getPamms=$model->getPammAccounts($usr['id']);
	$getDefaultPammAccount=$model->getDefaultPammNew();
	$userCash=$model->getCash();
	
		$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
		foreach($payment_sustem as $key => $payment_s){
			$payment['id'] = $payment_s['id'];
			$payment['name'] = $payment_s['name_sustem'];
			$payment['img'] = $payment_s['min_img'];
			$payment['select'] = $payment_s['on_off'];
			$payments[$payment['id']] = $payment;
		}
		
		$paument_id = $usr['id'];		
		$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$paument_id'");
		foreach($table_inf_money as $key => $inf_money_s){
			$inf_money['id'] = $inf_money_s['type_payment_id'];
			$inf_money['score_num'] = $inf_money_s['score_num'];
			$inf_money['score_val'] = $inf_money_s['score_val'];
			$inf_money['score_in'] = $inf_money_s['score_in'];
			$inf_money['score_out'] = $inf_money_s['score_out'];
			$inf_money['num_payment'] = $inf_money_s['num_payment'];
			$inf_money['status'] = $inf_money_s['status'];
			$inf_moneys[$inf_money['id']] = $inf_money;
		}
		
		
		$table_request_peyment = $inDB->get_table("cms_user_requests_for_payment", "user_id='$paument_id' AND status=0");
		foreach($table_request_peyment as $key => $t_request_payment){
			$request_payment['id'] = $t_request_payment['id'];
			$request_payment['garant'] = $t_request_payment['garant'];
			
			$request_payment['money_score'] = $inDB->get_field("cms_user_score","user_id='".$usr['id']."' AND type_payment_id='".$t_request_payment['id_paym_syst']."'","score_val");
			
			foreach($payment_sustem as $key0 => $payment_s){
				if($t_request_payment['id_paym_syst'] == $payment_s['id']){
				$request_payment['name_user_system'] =	$payment_s['name_sustem'];
				}
			}
			$request_payment['money_off'] = $t_request_payment['money_off'];
			$masive_inf = $t_request_payment['masive_inf'];
			$peremennaya = unserialize($masive_inf);
			
			foreach($peremennaya as $key1 => $inform){ 
				foreach($payment_sustem as $key2 => $payment_s){
					if($inform['sustem_id'] == $payment_s['id']){
					$money_on['name_paym_system'] =	$payment_s['name_sustem'];
					}
				}
				$money_on['sustem_id'] = $inform['sustem_id'];
				$money_on['score'] = $inform['score'];
				$money_on['kurs'] = $inform['kurs'];
				$money_on['kurs_obr'] = str_replace(',', '.', round($t_request_payment['money_off']/$inform['money'], 3));
				$money_on['money'] = $inform['money'];
				$moneys_on[$key1] = $money_on;
				
			}
			
			$request_payment['masive'] = $moneys_on;
			unset($moneys_on);
			$request_payment['sell_parts'] = $t_request_payment['sell_parts'];
			$request_payment['sms'] = $t_request_payment['sms'];
			$request_payments[$key] = $request_payment;
		}
		$verification = $inDB->get_field("cms_users","id='".$usr['id']."'","verify_stat");
		
    cmsPage::initTemplate('components', 'com_users_profile.tpl')->
            assign('usr', $usr)->
			assign('inf_gf', $inf_gf)->
			assign('application_gf_garant', $application_gf_garant)->
			assign('application_gf_gf', $application_gf_gf)->
			assign('verification', $verification)->
			assign('garant_money', $garant_money)->
			assign('provisional_application', $provisional_application)->
			assign('applications', $applications)->
			assign('inf_hidden_on', $inf_hidden_on)->
			assign('kurss', $kurss)->
			assign('request_payments', $request_payments)->
			assign('payments', $payments)->
			assign('inf_moneys', $inf_moneys)->
			assign('user_accounts', $getPamms)->
			assign('userCash', $userCash)->
			assign('pammDefault', $getDefaultPammAccount)->
			assign('exchange_pamm', $model->getExchangePamms($usr['id']))->
            assign('usr_money', $usr['money'])->
            assign('plugins', $plugins)->
            assign('cfg', $model->config)->
			assign('ids',  $usr['id'])->
			assign('expenses',  $expenses)->
			assign('user_pamms',  $user_pamms)->
			assign('all_pamms',  $all_pamms)->
            assign('myprofile', $myprofile)->
            assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
            assign('is_admin', $inUser->is_admin)->
            assign('is_auth', $inUser->id)->
            display('com_users_profile.tpl');

}
if($do=='exchange'){
	$inPage->setTitle("Биржа обмена");
	$user_id=$model->returnId();
	$login = $inDB->get_field("cms_users", "id='$user_id'", "login");
	$usr = $model->getUser($login);
	
	$errors = false;
	$opt = cmsCore::request("opt", "str");
	$opt2 = cmsCore::request("opt2", "str");
	

	$getPamms=$model->getPammAccounts($usr['id']);
	$getDefaultPammAccount=$model->getDefaultPammNew();
	$userCash=$model->getCash();
	
		$payment_sustem = $inDB->get_table("cms_user_type_payment", "on_off='1'");
		foreach($payment_sustem as $key => $payment_s){
			$payment['id'] = $payment_s['id'];
			$payment['name'] = $payment_s['name_sustem'];
			$payment['img'] = $payment_s['min_img'];
			$payment['select'] = $payment_s['on_off'];
			$payments[$payment['id']] = $payment;
		}
		$country_user = $inDB->get_table("cms_user_profiles");
		
		$paument_id = $usr['id'];		
		$table_inf_money = $inDB->get_table("cms_user_score", "user_id='$paument_id'");
		foreach($table_inf_money as $key => $inf_money_s){
			$inf_money['id'] = $inf_money_s['type_payment_id'];
			$inf_money['score_num'] = $inf_money_s['score_num'];
			$inf_money['score_val'] = $inf_money_s['score_val'];
			$inf_money['score_in'] = $inf_money_s['score_in'];
			$inf_money['score_out'] = $inf_money_s['score_out'];
			$inf_money['num_payment'] = $inf_money_s['num_payment'];
			$inf_money['status'] = $inf_money_s['status'];
			$inf_moneys[$inf_money['id']] = $inf_money;
		}
		
	if($_GET['search_id']){ $search_id = $_GET['search_id']; }
	if($_GET['search_have']){ $search_have = $_GET['search_have'] ;}
	if($_GET['search_give']){ $search_give = $_GET['search_give']; }
	if($_GET['search_kurs']){ $search_kurs = $_GET['search_kurs'] ;}
	if($_GET['search_country']){ $search_country = $_GET['search_country'] ;}
	if($_GET['search_part']){ $search_part = $_GET['search_part'] ; }
	if($_GET['search_garant']){ $search_garant = $_GET['search_garant'] ;}
	if($_GET['r']){ $records = $_GET['r'] ;}
	
	$table_request_peyment = $inDB->get_table("cms_user_requests_for_payment", "status IN ('0', '2')"); 
/*---------------------------------------------------------------------------------------------------------*/
		foreach($table_request_peyment as $key => $t_request_payment){
		//	var_dump($t_request_payment['id']);
			$request_payment['id'] = $t_request_payment['id'];
			$request_payment['garant'] = $t_request_payment['garant'];
			$request_payment['user_id'] = $t_request_payment['user_id'];
			$request_payment['login'] = $inDB->get_field("cms_users", "id='$user_id'", 'login');
			$users_infa = $inDB->get_fields("cms_users", "id='".$t_request_payment['user_id']."'", 'nickname,  regdate ');
			$users_infa['regdate'] = substr($users_infa['regdate'], 0, 10);
			$profile_infa = $inDB->get_fields("cms_user_profiles", "user_id='".$t_request_payment['user_id']."'", 'imageurl, odnokl, g_plus, vk, tviter, fasebook, mail');
			if($profile_infa['imageurl'] == ""){$profile_infa['imageurl'] = 'nopic.jpg';}
			
			$odnokl_cet = "";
			$g_plus_cet = "";
			$vk_cet = "";
			$tviter_cet = "";
			$fasebook_cet = "";
			$mail_cet = "";
			
			if($profile_infa['odnokl'] != ""){ $odnokl_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['g_plus'] != ""){ $g_plus_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['vk'] != ""){ $vk_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -149px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['tviter'] != ""){ $tviter_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -207px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['fasebook'] != ""){ $fasebook_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left;'></div>";}
			if($profile_infa['mail'] != ""){ $mail_cet = "<div style='background: url(/templates/_default_/images/sic-seti-icon.jpg) no-repeat -90px -127px; width:50px; height:50px; float:left;'></div>";}
			
			$user_name = $inDB->get_field("cms_users", "id='".$t_request_payment['user_id']."'", 'nickname');
			if($user_id == $t_request_payment['user_id']){
				$jjjjjj = "<a style='cursor:default; opacity:0.5;' class='button_svaz' >Связаться</a>";
			}
			else{
				$jjjjjj = "<a href='javascript:void(0)' class='button_svaz' onclick='users.sendMess(".$t_request_payment['user_id'].", 0, this);return false;' title='Новое сообщение: ".$user_name."'>Связаться</a>";
			}
			
			$request_payment['div'] = "
			<img  style='float:left; margin-right: 15px;width: 55px; max-height: 55px;' src='/images/users/avatars/small/".$profile_infa['imageurl']."'>
			<div style='height: 52px;'>
			".$odnokl_cet.$g_plus_cet.$vk_cet.$tviter_cet.$fasebook_cet.$mail_cet."
			</div>
			<div style='text-align: left;margin-top: 6px;'>
				<div>".$users_infa['nickname']."</div>
				<div><span style='color: gray;'>ID пользователя: </span>".$t_request_payment['user_id']."</div>
				<div><span style='color: gray;'>Дата регистрации: </span>".$users_infa['regdate']."</div>
			</div>
			<div style='margin: auto; margin-top: 6px;'>
				".$jjjjjj."
			</div>";
			
			foreach($country_user as $key5 => $country){
				if($country['user_id'] == $t_request_payment['user_id']){
					$request_payment['country'] = $country['country'];
				}
			}
			//var_dump($t_request_payment['garant']); 
			if($t_request_payment['garant'] == '3'){
				if($t_request_payment['name_system'] == '0'){
					foreach($payment_sustem as $key0 => $payment_s){
						if($t_request_payment['id_paym_syst'] === $payment_s['id']){
						$request_payment['name_user_system'] =	$payment_s['name_sustem'];
						}
					}
				}
				else{
					$request_payment['name_user_system'] =	$t_request_payment['name_system'];
				}
			}
			else{
				foreach($payment_sustem as $key0 => $payment_s){
					if($t_request_payment['id_paym_syst'] === $payment_s['id']){
					$request_payment['name_user_system'] =	$payment_s['name_sustem'];
					}
				}
			}
			$request_payment['money_off'] = $t_request_payment['money_off'];
			$masive_inf = $t_request_payment['masive_inf'];
			$peremennaya = unserialize($masive_inf);
			
			foreach($peremennaya as $key1 => $inform){
				
				if($t_request_payment['garant'] == '3'){
					if($inform['sustem_name'] && $inform['sustem_name'] != '0'){
						$money_on['name_paym_system'] =	$inform['sustem_name'];
					}
					else{
						foreach($payment_sustem as $key2 => $payment_s){
							if($inform['sustem_id'] == $payment_s['id']){
							$money_on['name_paym_system'] =	$payment_s['name_sustem'];
							}
						}
					}
				}
				else{
					foreach($payment_sustem as $key2 => $payment_s){
						if($inform['sustem_id'] == $payment_s['id']){
						$money_on['name_paym_system'] =	$payment_s['name_sustem'];
						}
					}
				}
				
				$money_on['sustem_id'] = $inform['sustem_id'];
				$money_on['score'] = $inform['score'];
				$money_on['kurs'] = str_replace(',', '.', $inform['kurs']);
				$perem118 = $t_request_payment['money_off']/$inform['money'];
				$perem118 = str_replace(',', '.', $perem118);
				$money_on['kurs_obr'] = str_replace(',', '.', round($perem118, 3));
				$money_on['money'] = $inform['money'];
				$moneys_on[$key1] = $money_on;
				unset($perem118);
			}
			
			$request_payment['masive'] = $moneys_on;
			unset($moneys_on);
			$request_payment['sell_parts'] = $t_request_payment['sell_parts'];
			$request_payment['sms'] = $t_request_payment['sms'];
			
			if($t_request_payment['status'] == "2"){
				$request_payment['status'] = $t_request_payment['status'];
				$time_start = $inDB->get_field("cms_user_application_timer", "zakaz_id='".$request_payment['id']."'", 'time_start');
				$time_nuw = time();
				
				if(($time_nuw - $time_start) < 1800){
					$request_payment['time'] = 30 - floor(($time_nuw - $time_start)/60);
				}
				else{
					$inDB->query("UPDATE cms_user_standart_application SET status='1' WHERE system_id='".$request_payment['id']."'");
					
					$sql115 = "UPDATE cms_user_requests_for_payment SET status='0' WHERE id='".$request_payment['id']."' AND status='2'";
					$inDB->query($sql115);
					
					$asdasdass = "DELETE FROM cms_user_application_timer WHERE zakaz_id='".$request_payment['id']."'";
					$inDB->query($dasdass);
					
					$inDB->query("UPDATE cms_user_code_protection SET status='3' WHERE num_appl='".$request_payment['id']."' status='0'");
					
					$inDB->query("DELETE FROM cms_user_replace_applications WHERE id_application='".$request_payment['id']."'");
										
					$id_usersss = $inDB->get_field("cms_user_requests_for_payment","id='".$request_payment['id']."'","user_id");
					
					$model->EventHistory($id_usersss, "Время заяки № ".$request_payment['id']." истекло");
					
					$change = 1;
				}
			}
			
			if($search_id){
				if(stristr($request_payment['id'], $search_id) == TRUE) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_have){
				if(stristr($request_payment['name_user_system'], $search_have) == TRUE || stristr($request_payment['money_off'], $search_have) == TRUE) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_give){
				$peremennaya = 0;
				foreach($request_payment['masive'] as $perem){
					if(stristr($perem['name_paym_system'], $search_give) == TRUE || stristr($perem['money'], $search_give)) {
						$peremennaya = 1;
					}
				}
				if($peremennaya == 1) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_kurs){
				$peremennaya = 0;
				foreach($request_payment['masive'] as $perem){
					if(stristr($perem['kurs'], $search_kurs) == TRUE || stristr($perem['kurs_obr'], $search_kurs)) {
						$peremennaya = 1;
					}
				}
				if($peremennaya == 1) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_country){
				if(stristr($request_payment['country'], $search_country) == TRUE ) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_part){
				$fsdf = 1;
				if($request_payment['sell_parts'] == 0) {$fsdf = 2;}
				if($fsdf ==  $search_part) {
					$request_payments[$key] = $request_payment;
				}
			}
			elseif($search_garant){
				if(stristr($request_payment['garant'], $search_garant) == TRUE ) {
					$request_payments[$key] = $request_payment;
				}
				 
			}
			else{$request_payments[$key] = $request_payment;}
			unset($request_payment['status']);
			unset($perem);
			
		}
		
		foreach($request_payments as $peremen){
			$masive_peym[] = $peremen;
		}
		if(!$records){$number_of_records = 10;}
		else{
			if($records != 10 ){
				if($records != 20){
					if($records != 50 ){
						$number_of_records = 10;
					}
					else{
						$number_of_records = $records;
					}
				}
				else{
					$number_of_records = $records;
				}
			}
			else{
				$number_of_records = $records;
			}
		}
		if(!$_GET['num'] || $_GET['num'] < 1){ $_GET['num'] = 1; }
		//$number_of_records = 1; // - количнство записей на странице
		$number_of_pagination = 7; // - количнство видимых цифр на странице
		$pagination_numbers = ceil(count($request_payments)/$number_of_records);
		if($_GET['num'] > $pagination_numbers){ $_GET['num'] = $pagination_numbers; }
		for($i=0; $i < $pagination_numbers; $i++){
			$pagination_number[$i] = $i + 1;
		}
		$href_pagination .= '<style>
								.label_message {
									display:none;
								}
							</style>';
		$search_inf = "";
		if($search_id){ $search_inf = "&search_id=".$search_id; }
		elseif($search_have){ $search_inf = "&search_have=".$search_have; }
		elseif($search_give){ $search_inf = "&search_give=".$search_give; }
		elseif($search_kurs){ $search_inf = "&search_kurs=".$search_kurs; }
		elseif($search_country){ $search_inf = "&search_country=".$search_country; }
		elseif($search_part){ $search_inf = "&search_part=".$search_part; }
		elseif($search_garant){ $search_inf = "&search_garant=".$search_garant; }
		if($pagination_numbers > 1){
			if($_GET['num'] == 1){$href_pagination .= '<a class="active"><<</a>';}
			else{
				if($_GET['num'] != 1){
					$asdasdd = $_GET['num']-1;
					$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num='.$asdasdd.$search_inf.'" ><<</a>';
				}
			}
		}
		$w = 2;
		if($pagination_numbers > 1){
			if($_GET['num'] == 1){$href_pagination .= '<a class="active"  >1</a>';}
			else{
				$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num=1'.$search_inf.'" >1</a>';
			}
		}
		if($pagination_numbers >= 2){
			if($_GET['num'] == 2){$href_pagination .= '<a class="active"  >2</a>';}
			else{
				$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num=2'.$search_inf.'" >2</a>';
			}
		}
		if($pagination_numbers >= 6 && $_GET['num'] > 5){$href_pagination .= '<a class="active2">..</a>';}
		foreach($pagination_number as $num_p){
			if($num_p > 2 && $num_p < $pagination_numbers){
			if($w < $number_of_pagination){
				//$pagination_numbers
				if($_GET['num'] < $num_p+round($number_of_pagination/2, 0)-1){
					if($_GET['num'] == $num_p){
						$href_pagination .= '<a class="active"  >'.$num_p.'</a>';
					}
					else{
						$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num='.$num_p.$search_inf.'" >'.$num_p.'</a>';
					}
					$w++;
				}
			}
			}			
		}
		if($_GET['num'] < $pagination_numbers-3){$href_pagination .= '<a class="active2">..</a>';}
		if($pagination_numbers >= 3){
			if($_GET['num'] == $pagination_numbers){$href_pagination .= '<a class="active"  >'.$pagination_numbers.'</a>';}
			else{
				$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num='.$pagination_numbers.$search_inf.'" >'.$pagination_numbers.'</a>';
			}
		}
		if($pagination_numbers > 1){
			if($_GET['num'] == $pagination_numbers){$href_pagination .= '<a class="active">>></a>';}
			else{
				if($_GET['num'] != $pagination_numbers){
					$asdasdd = $_GET['num']+1;
					$href_pagination .= '<a href="/users/'.$user_id.'/exchange.html?num='.$asdasdd.$search_inf.'" >>></a>';
				}
			}
		}
		if($_GET['num']){
			$num = $_GET['num'] - 1;
			if($number_of_records > count($request_payments) - $num*$number_of_records){
				$j = count($request_payments) - $num*$number_of_records + $number_of_records*$num;
			}
			else{
				$j = $number_of_records*$num + $number_of_records;
			}
			for($i = $number_of_records*$num; $i < $j; $i++){
				$request_payments_s[] = $masive_peym[$i];
			}
		}
		else{
			for($i = 0; $i < $number_of_records; $i++){
				$request_payments_s[] = $masive_peym[$i];
			}
		}

$number_page = "";
if($_GET['num']){
	$number_page = "?num=".$_GET['num'];
}
$number_of_record = "&r=".$number_of_records;
		
		if($change){
			cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record);
		}		

if($opt == "del_application"){
	$id_application = cmsCore::request("id_application", 'str');
	
	$inf_application = $inDB->get_fields("cms_user_requests_for_payment","id='$id_application'","garant, id_paym_syst, money_off");
	if($inf_application['garant'] == '1'){
		$score_val = $inDB->get_field("cms_user_score","user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'","score_val");
		$result_money = $score_val+$inf_application['money_off'];
		$result_money = str_replace(',', '.', $result_money);
		$sql_2 = "UPDATE cms_user_score SET score_val='$result_money' WHERE user_id='$user_id' AND type_payment_id='".$inf_application['id_paym_syst']."'";
		$inDB->query($sql_2);
	}
	elseif($inf_application['garant'] == '3'){
		
	}
	
	$asdasdass = "DELETE FROM cms_user_requests_for_payment WHERE id='$id_application'";
	$inDB->query($asdasdass);
	
	$model->EventHistory($user_id, "Заяка № $id_application удалена");
	
	cmsCore::redirect('/users/'.$user_id.'/exchange.html#dell_application');
}	



if($opt == "click_zayavka"){
		
	$inf_hidden_1on = cmsCore::request("inf_hidden_on", "array");//
	$inf_hidden_2off = cmsCore::request("inf_hidden_off", "array");//
	/*var_dump($inf_hidden_1on);
	echo "<br>";
	var_dump($inf_hidden_2off);*/
	$time_start = time();
	$user_online['user_id'] = $inf_hidden_1on['0'];
	$sql113 = "INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus ,money_minus, id_sys_plus, money_plus, time_start) VALUES ('".$inf_hidden_1on['0']."','".$inf_hidden_1on['1']."','".$inf_hidden_1on['2']."','".$inf_hidden_1on['3']."','".$inf_hidden_1on['4']."','".$inf_hidden_1on['5']."','$time_start')";
	$inDB->query($sql113);
	
	$sql114 = "INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus ,money_minus, id_sys_plus, money_plus, time_start) VALUES ('".$inf_hidden_2off['0']."','".$inf_hidden_2off['1']."','".$inf_hidden_2off['2']."','".$inf_hidden_2off['3']."','".$inf_hidden_2off['4']."','".$inf_hidden_2off['5']."','$time_start')";
	$inDB->query($sql114);
	
	$sql115 = "UPDATE cms_user_requests_for_payment SET status='2' WHERE id='".$inf_hidden_1on['1']."'";
	$inDB->query($sql115);
	
	$sql116 = "UPDATE cms_user_requests_for_payment SET status='2' WHERE id='".$inf_hidden_2off['1']."'";
	$inDB->query($sql116);
	
	$model->EventHistory($user_id, "Запрос на обмн заявки № ".$inf_hidden_1on['1']."");
	
	$id_usersss = $inDB->get_field("cms_user_requests_for_payment","id='".$inf_hidden_2off['1']."'","user_id");
	
	$model->EventHistory($id_usersss, "Запрос на обмн заявки № ".$inf_hidden_2off['1']."");
	
	$user_email = $inDB->get_field("cms_users", "id='".$inf_hidden_2off['0']."'", "email");
	
	$inf_mail = array();
	$inf_mail['email'] = $user_email;
	$inf_mail['message'] = "В личном кабинете появился запрос к заявке №: ".$inf_hidden_2off['1'];
	$email = $model->sendEmail2($inf_mail);
	cmsUser::sendMessage(USER_MASSMAIL, $inf_hidden_2off['0'], $inf_mail['message']);
	for($i=0;$i<8;$i++){
		$code_protection1 .= rand(0, 9);	
	}
	for($i=0;$i<8;$i++){
		$code_protection2 .= rand(0, 9);	
	}
	
	//$sql1133 = $inDB->query("INSERT cms_user_code_protection (num_appl_1, num_appl_2, user_id_on, user_id_off, code1, code2 ,status) VALUES ('".$inf_hidden_2off['1']."','".$inf_hidden_1on['1']."','".$inf_hidden_1on['0']."','".$inf_hidden_2off['0']."','','','0')");
	
	cmsCore::redirect('/users/'.$usr['login']);
}

if($opt == "request_change"){
	if($opt2 == "change_kurs"){
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#error');
        }
		else{
		$change_kurs = cmsCore::request("change_kurs", "str");
		$id_zayavki = cmsCore::request("id_zayavki", "str");
		$obmen_id = cmsCore::request("obmen", "str");
		
		$num_in_cycles = cmsCore::request("num_in_cycle", "int");
		$polya = $inDB->get_fields('cms_user_requests_for_payment', "id = '$id_zayavki'", 'masive_inf, money_off');
		$polya_masive = unserialize($polya['masive_inf']);
		//var_dump($polya_masive);
		
		foreach($polya_masive as $key=>$polya_up){
			if($key == $num_in_cycles){
				$polya_s['sustem_id'] = $polya_up['sustem_id'];
				$polya_s['score'] = $polya_up['score'];
				$polya_s['kurs'] = str_replace(',', '.', round(str_replace(",", ".", $change_kurs), 3));
				$polya_s['money'] = str_replace(',', '.', round($polya['money_off']*$change_kurs, 3));
				$polya_save[$key] = $polya_s;
			}
			else{
				$polya_s['sustem_id'] = $polya_up['sustem_id'];
				$polya_s['score'] = $polya_up['score'];
				$polya_s['kurs'] = str_replace(",", ".", $polya_up['kurs']);
				$polya_s['money'] = $polya_up['money'];
				$polya_save[$key] = $polya_s;
			}
		}
		$polya_save = serialize($polya_save);
		$sql116 = "UPDATE cms_user_requests_for_payment SET masive_inf='$polya_save' WHERE id='$id_zayavki'";
		$zapros4 = $inDB->query($sql116);
		
		$model->EventHistory($user_id, "Заявка № ".$id_zayavki." изменена");
		}
	}

	
	$score = cmsCore::request("score_s", "array");//
	$system_name = cmsCore::request("name_paym_system", "array");//
	foreach($system_name as $key=>$sys_nam){
		$system_id[$key] = $inDB->get_field("cms_user_type_payment", "name_sustem='$sys_nam'", 'id');
	}
	$system_money = cmsCore::request("money", "array");//
	$garant = cmsCore::request("garant", "str");
	$payment_user = cmsCore::request("user_money", 'str');//
	$payment_system_ids = cmsCore::request("payment_system_id", 'str');
	$payment_system_id = $inDB->get_field("cms_user_type_payment", "name_sustem='$payment_system_ids'", 'id');//
	$sell_parts = cmscore::request("sell_parts", 'str');//
	$id_table = cmscore::request("idshka", 'str');
	
	$kurs_get = $inDB->get_fields('cms_user_requests_for_payment', "id = '$id_table'", 'masive_inf');
	$kurs_get = unserialize($kurs_get['masive_inf']);
	foreach($kurs_get as $key10=>$kurs_s){
		$kurs[$key10] = str_replace(",", ".", $kurs_s['kurs']);
	}
	$i = 0;
	if($garant == "2"){
		//var_dump($_SERVER['HTTP_ORIGIN']);
		
		$table_user_online = $inDB->get_fields("cms_users","id='$user_id'","nickname, first_name");
		
		$profile_infa = $inDB->get_fields("cms_user_profiles", "user_id='$user_id'", 'imageurl, odnokl, g_plus, vk, tviter, fasebook, mail, country');
		if($profile_infa['imageurl'] != "") $image_user = $profile_infa['imageurl'];
		else $image_user = "nopic.jpg";
		
		$user_id_offline = $inDB->get_field("cms_user_requests_for_payment","id='$id_table'","user_id");
		$user_login_offline = $inDB->get_field("cms_users","id='$user_id_offline'", "login");
		cmsUser::sendMessage(USER_MASSMAIL, $user_id, "<p><b>Вы подали заявку на обмен средств через внутреннюю конвертацию</b></p><p><b>Заявка на бирже обмена №: $id_table</b></p><p>Ожидайте ответа.</p>");
		
		cmsUser::sendMessage(USER_MASSMAIL, $user_id_offline, "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p>");
		
		$id_owner = $inDB->get_field("cms_user_requests_for_payment","id='$id_table'","user_id");
		$inDB->query("INSERT cms_user_temporary_standart (user_owner, user_id, system_id, date) VALUE ('$id_owner','$user_id', '$id_table', '".date('Y-m-d H:i:s')."')");
		
		$inf_mail = array();
		$inf_mail['email'] = $user_email_online;
		$inf_mail['message'] = "<p><b>Вы подали заявку на обмен средств через внутреннюю конвертацию</b></p><p><b>Заявка на бирже обмена №: $id_table</b></p><p>Если заявку подтвердят, вам прийдет сообщение в ЛК и на почту.</p>";
		
		$email = $model->sendEmail3($inf_mail);
		
		$inf_mail2 = array();
		$inf_mail2['email'] = $user_email_offline;
		$inf_mail2['message'] = "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p><p>Подробная информация в <b><a href='".$_SERVER['HTTP_ORIGIN']."'>личном кабинете</a></b></p>";
		
		$email2 = $model->sendEmail3($inf_mail2);
		
		cmsCore::redirect('/users/'.$user_id.'/exchange.html#zapros_standart');
		
	}
	$id_user_off = $inDB->get_field("cms_user_requests_for_payment", "id='$id_table'", 'user_id');
	
	$infa_zayav = $inDB->get_table("cms_user_requests_for_payment", "status='0' AND user_id = '$user_id'");
	$isset_name1 = 0;
	$isset_name2 = 0;
	$isset_name3 = 0;
	$isset_name4 = 0;
	$isset_name5 = 0;
	$isset_name6 = 0;
	/*  берет первое совпадение с одинаковыми названиями систем  */
	//var_dump($infa_zayav);
	/*=========================================================================================================*/
	/* =============================== проверка на статус заявки в базе ====================================== */
	/*=========================================================================================================*/
	
	$status_application = $inDB->get_field("cms_user_requests_for_payment","id='$id_table'","status");
	if($status_application == '0'){
	foreach($infa_zayav as $key => $infa_zayav_s){
		$masive_inf = $infa_zayav_s['masive_inf'];
		$peremennaya_10 = unserialize($masive_inf);
		//var_dump($peremennaya_10);
		if($infa_zayav_s['garant'] == '3'){
			if($infa_zayav_s['name_system'] != "0"){
				$user_sustem_name = $infa_zayav_s['name_system'];
			}
			else{
				$user_sustem_name = $inDB->get_field("cms_user_type_payment", "id='".$infa_zayav_s['id_paym_syst']."'", 'name_sustem');
			}
		}
		else{
			$user_sustem_name = $inDB->get_field("cms_user_type_payment", "id='".$infa_zayav_s['id_paym_syst']."'", 'name_sustem');
		}
	//foreach($peremennaya as $key0=>$foreach_inf){
	$inf_click = $inDB->get_field('cms_user_requests_for_payment', "id = '$id_table'", 'masive_inf');
	$inf_click = unserialize($inf_click);
	foreach($inf_click as $key0=>$inf_click_s){
		//echo $infa_zayav_s['id_paym_syst']."=?".$user_sustem_name;
		
		if($model->Create_id_system($system_name[$key0]) == $model->Create_id_system($user_sustem_name)){ 			
			$isset_name1 = 1; 
			
			$sum_garant = 0.999;
			
			foreach($peremennaya_10 as $key11=>$foreach_inf){
				
			$peremennaya = 	$peremennaya_10;
			if($infa_zayav_s['garant'] == '3'){
				if($peremennaya[$key11]['sustem_name'] && $peremennaya[$key11]['sustem_name'] != "0"){
					$payment_system_name = $peremennaya[$key11]['sustem_name'];
				}
				else{
					$payment_system_name = $inDB->get_field("cms_user_type_payment", "id='".$peremennaya[$key11]['sustem_id']."'", 'name_sustem');
				}
			}
			else{
				$payment_system_name = $inDB->get_field("cms_user_type_payment", "id='".$peremennaya[$key11]['sustem_id']."'", 'name_sustem');
			}
			
			if($model->Create_id_system($payment_system_ids) == $model->Create_id_system($payment_system_name)){
				$isset_name2 = 1;
	
// если ГФ - ГФ или ГФ гарант
			if(($garant == '3' && $infa_zayav_s['garant'] == '3') || ($garant == '3' && $infa_zayav_s['garant'] == '1')){ $isset_name3 = 1;
				if($kurs[$key0] == round($infa_zayav_s['money_off']/$peremennaya[$key11]['money'], 3)){
					
//================== нужно добавить условие или статусы 0 у заявок!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				
		/*	========================  Все условия для обмена выполнены	===================================	*/
					//курсы валют активной/пасивной кнопки 
				$active_kurs = round($infa_zayav_s['money_off']/$peremennaya[$key11]['money'], 3);
				$pasive_kurs = round($peremennaya[$key11]['money']/$infa_zayav_s['money_off'], 3);
				//echo $infa_zayav_s['money_off']." ? ".$payment_user*$active_kurs."<br>";
				if($infa_zayav_s['money_off'] < $payment_user*$active_kurs){
					//изменить заявку юзера юзера что онлайн 
					$off_money = $infa_zayav_s['money_off']*$pasive_kurs;
					//echo $off_money." - отнимаем сумму<br>";
					
					$minus_off_money = $payment_user-$off_money;
					$minus_off_money = str_replace(',', '.', round($minus_off_money, 3));
					$peremennaya22 = $inDB->get_field("cms_user_requests_for_payment", "id=$id_table", 'masive_inf');
					$peremennaya22 = unserialize($peremennaya22);
					foreach($peremennaya22 as $key=>$peremennaya_up){
						$peremennaya_s['sustem_id'] = $peremennaya_up['sustem_id'];
						$peremennaya_s['score'] = $peremennaya_up['score'];
						$peremennaya_s['kurs'] = str_replace(",", ".", $peremennaya_up['kurs']);
						$peremennaya_s['money'] = ($minus_off_money)*($peremennaya_up['kurs']);
						$peremennaya_s['money'] = str_replace(',', '.', round($peremennaya_s['money'], 3));
						$peremennaya_save[$key] = $peremennaya_s;
					}
					
					$peremennaya_save = serialize($peremennaya_save);
					
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('$id_table','$minus_off_money','$peremennaya_save','0')");
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('".$infa_zayav_s['id']."','0','0','1')");
					
				}
				elseif($infa_zayav_s['money_off'] > $payment_user*$active_kurs){
					$off_money = $payment_user;//*$active_kurs;
					// изменить заявку текущего юзера что оффлайн (на чью нажимаем)
					//echo $off_money." - отнимаем сумму<br>";
					$minus_off_money = $infa_zayav_s['money_off']-$off_money*$active_kurs;
					$minus_off_money = str_replace(',', '.', round($minus_off_money, 3));
					foreach($peremennaya as $key=>$peremennaya_up){
						$peremennaya_s['sustem_id'] = $peremennaya_up['sustem_id'];
						$peremennaya_s['score'] = $peremennaya_up['score'];
						$peremennaya_s['kurs'] = str_replace(",", ".", $peremennaya_up['kurs']);
						$peremennaya_s['money'] = ($minus_off_money)*$pasive_kurs;
						$peremennaya_s['money'] = str_replace(',', '.', round($peremennaya_s['money'], 3));
						$peremennaya_save[$key] = $peremennaya_s;
					}
				/*	echo "пасивная заявка<br>";
					echo ($active_kurs)."*".$payment_user." - курс * деньги активной заявки<br>";
					echo $infa_zayav_s[id]." - что обновляется(онлайн)<br>";
					echo $id_table." - что оффлайн<br>";
					echo $minus_off_money." - минус деньги<br>";
					var_dump($peremennaya_save);*/
					
					$peremennaya_save = serialize($peremennaya_save);
					
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('$id_table','0','0','1')");
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('".$infa_zayav_s['id']."','$minus_off_money','$peremennaya_save','0')");
					
				}
				elseif($infa_zayav_s['money_off'] == $payment_user*$active_kurs){
					$off_money = $payment_user;
					$sql118 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$infa_zayav_s[id]'";
					//$inDB->query($sql118);
					$sql119 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$id_table'";
					//$inDB->query($sql119);
					
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('$id_table','0','0','1')");
					$inDB->query("INSERT cms_user_replace_applications (id_application, money_off, masive, status) VALUES ('".$infa_zayav_s['id']."','0','0','1')");
				}
		// если ГФ ГФ
				if($garant == '3' && $infa_zayav_s['garant'] == '3'){
					$peremennaya_1 = $inDB->get_field("cms_user_requests_for_payment","id='$id_table'","masive_inf");
					$peremennaya_1 = unserialize($peremennaya_1);
					foreach($peremennaya_1 as $key=>$peremennaya_up2){
						if($peremennaya_up2['sustem_id'] == $infa_zayav_s['id_paym_syst']){
							if($peremennaya_up2['sustem_name'] && $peremennaya_up2['sustem_name'] != '0'){
							$score_1_us = $peremennaya_up2['score'];
							}
							else{
								$score_1_us = $inDB->get_field("cms_user_score","user_id='$id_user_off' AND type_payment_id='".$peremennaya_up2['sustem_id']."'","score_num");
							}
						}
					}
					if($peremennaya[$key11]['score'] && $peremennaya[$key11]['score'] != '0'){
						$score_2_us = $peremennaya[$key11]['score'];
					}
					else{
						$score_2_us = $inDB->get_field("cms_user_score","user_id='$id_user_off' AND type_payment_id='".$peremennaya[$key11]['sustem_id']."'","score_num");
					}
					$money_a = $off_money*$active_kurs;
					$time_start = time();
					$inDB->query("INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus, money_minus, id_sys_plus, money_plus, time_start, type, zakaz_id_2, score_num) VALUES ('$user_id','".$infa_zayav_s['id']."','".$infa_zayav_s['id_paym_syst']."','$money_a','".$peremennaya[$key11]['sustem_id']."','$off_money','$time_start','1','$id_table','$score_1_us')");
					$inDB->query("INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus, money_minus, id_sys_plus, money_plus, time_start, type, zakaz_id_2, score_num) VALUES ('$id_user_off','".$id_table."','".$peremennaya[$key11]['sustem_id']."','$off_money','".$infa_zayav_s['id_paym_syst']."','$money_a','$time_start','1','".$infa_zayav_s['id']."','$score_2_us')");
					
					$inDB->query("UPDATE cms_user_requests_for_payment SET status='2' WHERE id='".$infa_zayav_s['id']."'");
					$inDB->query("UPDATE cms_user_requests_for_payment SET status='2' WHERE id='$id_table'");
					
					$inDB->query("INSERT cms_user_code_protection (num_appl, user_id_on, user_id_off, code_on, code_off ,status) VALUES ('".$infa_zayav_s['id']."','$user_id','$id_user_off','0','0','0')");
					
					$inDB->query("INSERT cms_user_code_protection (num_appl, user_id_on, user_id_off, code_on, code_off ,status) VALUES ('$id_table','$id_user_off','$user_id','0','0','0')");
					
					$model->EventHistory($user_id, "Обмен средств заявoк № ".$infa_zayav_s['id']." и № $id_table");
					$login1 = $inDB->get_field("cms_users","id='$user_id'","login");
					$login2 = $inDB->get_field("cms_users","id='$id_user_off'","login");
					
					cmsUser::sendMessage(USER_MASSMAIL, $id_user_off, "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p>");
					
					$inf_mail2 = array();
					$inf_mail2['email'] = $user_email_offline;
					$inf_mail2['message'] = "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p><p>Подробная информация в <b><a href='".$_SERVER['HTTP_ORIGIN']."/users/".$login2."'>личном кабинете</a></b></p>";
					
					$email2 = $model->sendEmail3($inf_mail2);
					
					cmsCore::redirect('/users/'.$login1.'#');
					
				}
				// если ГФ - гарант
				elseif($garant == '3' && $infa_zayav_s['garant'] == '1'){
					$money_a = $off_money*$active_kurs;
					$peremennaya_1 = $inDB->get_field("cms_user_requests_for_payment","id='$id_table'","masive_inf");
					$peremennaya_1 = unserialize($peremennaya_1);
					foreach($peremennaya_1 as $key=>$peremennaya_up2){
						if($peremennaya_up2['sustem_id'] == $infa_zayav_s['id_paym_syst']){
							if($peremennaya_up2['sustem_name'] && $peremennaya_up2['sustem_name'] != '0'){
							$score_1_us = $peremennaya_up2['score'];
							}
							else{
								$score_1_us = $inDB->get_field("cms_user_score","user_id='$id_user_off' AND type_payment_id='".$peremennaya_up2['sustem_id']."'","score_num");
							}
						}
					}
					if($peremennaya[$key11]['score'] && $peremennaya[$key11]['score'] != '0'){
						$score_2_us = $peremennaya[$key11]['score'];
					}
					else{
						$score_2_us = $inDB->get_field("cms_user_score","user_id='$user_id' AND type_payment_id='".$peremennaya[$key11]['sustem_id']."'","score_num");
					}
					$time_start = time();
					$inDB->query("INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus, money_minus, id_sys_plus, money_plus, time_start, type, zakaz_id_2, score_num) VALUES ('$user_id','".$infa_zayav_s['id']."','".$infa_zayav_s['id_paym_syst']."','$money_a','".$peremennaya[$key11]['sustem_id']."','$off_money','$time_start','2','$id_table','$score_1_us')");
					$inDB->query("INSERT cms_user_application_timer (user_id, zakaz_id, id_sys_minus, money_minus, id_sys_plus, money_plus, time_start, type, zakaz_id_2, score_num) VALUES ('$id_user_off','".$id_table."','".$peremennaya[$key11]['sustem_id']."','$off_money','".$infa_zayav_s['id_paym_syst']."','$money_a','$time_start','2','".$infa_zayav_s['id']."','$score_2_us')");
					
					$inDB->query("UPDATE cms_user_requests_for_payment SET status='2' WHERE id='".$infa_zayav_s['id']."'");
					$inDB->query("UPDATE cms_user_requests_for_payment SET status='2' WHERE id='$id_table'");
					
					$inDB->query("INSERT cms_user_code_protection (num_appl, user_id_on, user_id_off, code_on, code_off ,status) VALUES ('".$infa_zayav_s['id']."','$user_id','$id_user_off','0','0','0')");
					
					$inDB->query("INSERT cms_user_code_protection (num_appl, user_id_on, user_id_off, code_on, code_off ,status) VALUES ('$id_table','$id_user_off','$user_id','0','0','0')");
					
					$model->EventHistory($user_id, "Обмен средств заявoк № ".$infa_zayav_s['id']." и № $id_table");
					$login1 = $inDB->get_field("cms_users","id='$user_id'","login");
					$login2 = $inDB->get_field("cms_users","id='$id_user_off'","login");
					
					cmsUser::sendMessage(USER_MASSMAIL, $id_user_off, "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p>");
					
					$inf_mail2 = array();
					$inf_mail2['email'] = $user_email_offline;
					$inf_mail2['message'] = "<p><b>На Вашу заявку<b> № $id_table </b>есть предложение на обмен средств</b></p><p>Подробная информация в <b><a href='".$_SERVER['HTTP_ORIGIN']."/users/".$login2."'>личном кабинете</a></b></p>";
					
					$email2 = $model->sendEmail3($inf_mail2);
					
					cmsCore::redirect('/users/'.$login1.'#');
					//echo "Обмн ГФ пока не реализован";
				}
		
					
//=================================================				
				}
				else{
					$kurs_user_off = str_replace(',', '.', round($payment_user/$inf_click_s['money'], 3));
					$num_in_cycle = $key11;// $key с цикла
					$kurs_user_on = $peremennaya[$key11]['kurs'];
					$id_zamov = $infa_zayav_s[id];
					$obmen = $id_table;
					$isset_name4 = 1;
				}
			}
// если гарант
			elseif($garant == '1' && $garant == $infa_zayav_s['garant']){ $isset_name3 = 1;
	//условие продать частями	$sell_parts	
			if( count($system_name) > 1  && $sell_parts == "-" ){$isset_name5 = 1;}
			else{
				
		if($kurs[$key0] == round($infa_zayav_s['money_off']/$peremennaya[$key11]['money'], 3)){
			
	/*										Все условия для обмена выполнены								*/		
		
		
				//курсы валют активной/пасивной кнопки 
				$active_kurs = round($infa_zayav_s['money_off']/$peremennaya[$key11]['money'], 3);
				$pasive_kurs = round($peremennaya[$key11]['money']/$infa_zayav_s['money_off'], 3);
	// !!!!!!!!!!!!!!!!!!!!!! неправильно считает $off_money  !!!!!!!!!!!!!!!!!!!!!!!	
			if($infa_zayav_s['money_off'] < $payment_user*$active_kurs){
				$off_money = $infa_zayav_s['money_off']*$pasive_kurs;
			//	echo $off_money."<br>";
				//изменить заявку юзера c которым обмен ($id_user_off) уменьшить суму "есть" на значение 
				//$off_money $id_user_off  $id_table
				$minus_off_money = $payment_user-$off_money;
				$minus_off_money = str_replace(',', '.', round($minus_off_money, 3));
				$peremennaya22 = $inDB->get_field("cms_user_requests_for_payment", "id=$id_table", 'masive_inf');
				$peremennaya22 = unserialize($peremennaya22);
				foreach($peremennaya22 as $key=>$peremennaya_up){
					$peremennaya_s['sustem_id'] = $peremennaya_up['sustem_id'];
					$peremennaya_s['score'] = $peremennaya_up['score'];
					$peremennaya_s['kurs'] = str_replace(",", ".", $peremennaya_up['kurs']);
					$peremennaya_s['money'] = ($minus_off_money)*($peremennaya_up['kurs']);
					$peremennaya_s['money'] = str_replace(',', '.', round($peremennaya_s['money'], 3));
					$peremennaya_save[$key] = $peremennaya_s;
				}
				/*echo "активная заявка<br>";
				echo $id_table." - id что обновляется<br>";
				echo ($infa_zayav_s[id])." - id что статус = 1<br>";
				echo $pasive_kurs."*".$infa_zayav_s['money_off']." - курс * деньги пасивной заявки<br>";
				echo $minus_off_money."<br>";
				var_dump($peremennaya_save);
				*/
				$peremennaya_save = serialize($peremennaya_save);
				if($minus_off_money == 0 || $minus_off_money == "0"){
					$sql119 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$id_table'";
					$inDB->query($sql119);
				}
				else{
					$sql113 = "UPDATE cms_user_requests_for_payment SET money_off='$minus_off_money', masive_inf='$peremennaya_save' WHERE id='$id_table'";
					$inDB->query($sql113);
				}
				$sql118 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$infa_zayav_s[id]'";
				$inDB->query($sql118);
				
				$model->EventHistory($id_user_off, "Обмен средств заявки № $id_table");
				$model->EventHistory($user_id, "Обмен средств заявки № ".$infa_zayav_s['id']."");
			}
			elseif($infa_zayav_s['money_off'] > $payment_user*$active_kurs){
				$off_money = $payment_user;
				// изменить заявку текущего юзера уменьшить суму "есть" на значение $off_money $user_id $infa_zayav_s[id]
				$minus_off_money = $infa_zayav_s['money_off']-$off_money*$active_kurs;
				$minus_off_money = str_replace(',', '.', round($minus_off_money, 3));
				foreach($peremennaya as $key=>$peremennaya_up){
					$peremennaya_s['sustem_id'] = $peremennaya_up['sustem_id'];
					$peremennaya_s['score'] = $peremennaya_up['score'];
					$peremennaya_s['kurs'] = str_replace(",", ".", $peremennaya_up['kurs']);
					$peremennaya_s['money'] = ($minus_off_money)*$pasive_kurs;
					$peremennaya_s['money'] = str_replace(',', '.', round($peremennaya_s['money'], 3));
					$peremennaya_save[$key] = $peremennaya_s;
				}
				/*echo "пасивная заявка<br>";
				echo ($active_kurs)."*".$payment_user." - курс * деньги активной заявки<br>";
				echo $infa_zayav_s[id]." - что обновляется<br>";
				echo $id_table." - что статус = 1<br>";
				echo $minus_off_money."<br>";
				var_dump($peremennaya_save);
				*/
				$peremennaya_save = serialize($peremennaya_save);
				if($minus_off_money == 0 || $minus_off_money == "0"){
					$sql118 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$infa_zayav_s[id]'";
					$inDB->query($sql118);
				}
				else{
					$sql113 = "UPDATE cms_user_requests_for_payment SET money_off='$minus_off_money', masive_inf='$peremennaya_save' WHERE id='$infa_zayav_s[id]'";
					$inDB->query($sql113);
				}
				$sql119 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$id_table'";
				$inDB->query($sql119);
				
				$model->EventHistory($id_user_off, "Обмен средств заявки № $id_table");
				$model->EventHistory($user_id, "Обмен средств заявки № ".$infa_zayav_s['id']."");
			}
			else{ $off_money = $payment_user; 
				$sql118 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$infa_zayav_s[id]'";
				$inDB->query($sql118);
				$sql119 = "UPDATE cms_user_requests_for_payment SET status='1' WHERE id='$id_table'";
				$inDB->query($sql119);
				
				$model->EventHistory($id_user_off, "Обмен средств заявки № $id_table");
				$model->EventHistory($user_id, "Обмен средств заявки № ".$infa_zayav_s['id']."");
			}
	// изменения для текущего юзера
					$polya_off = $inDB->get_fields('cms_user_score', "user_id = '$user_id' AND type_payment_id = '".$infa_zayav_s['id_paym_syst']."'", 'score_val, score_in, score_out');
					$polya_on = $inDB->get_fields('cms_user_score', "user_id = '$user_id' AND type_payment_id = '".$peremennaya[$key11]['sustem_id']."'", 'score_val, score_in, score_out');
					// разница после -
					//$difference_on_user = $polya_off['score_val']-$off_money*$active_kurs; 
					$p_sc_out_on_usr = $polya_off['score_out'] + $off_money*$active_kurs;// добавить в score_out где id = $infa_zayav_s['id_paym_syst']
					//$infa_zayav_s['id_paym_syst']."<br>"; 
				// разница после  +
					$summ_on_user = $polya_on['score_val']+$off_money*$sum_garant;
					$p_sc_in_on_usr = $polya_on['score_in'] + $off_money*$sum_garant;
					$money_admin_plus = $inDB->get_field("cms_user_score", "user_id = '0'", "score_val");
					$money_admin_plus = $money_admin_plus + $off_money*(1 - $sum_garant);
					/*echo $user_id." - user_id<br>";
					echo "this_user - ;<br>";
					echo $infa_zayav_s['id_paym_syst']." - id_paym_system<br>";
					echo $polya_off['score_val']." - ".$off_money."*".$active_kurs."<br>";
					echo $difference_on_user." - score_val<br>";
					echo $p_sc_out_on_usr." - score_out<br>";
					echo "------------------<br>this_user + ;<br>";
					echo $peremennaya[$key11]['sustem_id']." - id_paym_system<br>";
					echo $off_money." <br>";
					echo $summ_on_user." - score_val<br>";
					echo $p_sc_in_on_usr." - score_in<br>";*/
					//$peremennaya[$key11]['sustem_id']."<br>";
				// разница после -
					$sql117 = "UPDATE cms_user_score SET score_out='$p_sc_out_on_usr' WHERE user_id='$user_id' AND type_payment_id='".$infa_zayav_s['id_paym_syst']."'";
					$zapros1 = $inDB->query($sql117);
				// разница после +
					$sql114 = "UPDATE cms_user_score SET score_val='$summ_on_user', score_in='$p_sc_in_on_usr' WHERE user_id='$user_id' AND type_payment_id='".$peremennaya[$key11]['sustem_id']."'";
					$zapros2 = $inDB->query($sql114);
	// изменения для юзера с которым обмен
					$polya_off = $inDB->get_fields('cms_user_score', "user_id = '$id_user_off' AND type_payment_id = '".$peremennaya[$key11]['sustem_id']."'", 'score_val, score_in, score_out');
					//$difference_off_user = $polya_off['score_val']-$off_money;// разница после -
					$p_sc_out_off_usr = $polya_off['score_out'] + $off_money;// добавить в score_out
					//$peremennaya[$key11]['sustem_id']."<br>";
					
					$polya_on = $inDB->get_fields('cms_user_score', "user_id = '$id_user_off' AND type_payment_id = '".$infa_zayav_s['id_paym_syst']."'", 'score_val, score_in, score_out');
					$summ_off_user = $polya_on['score_val'] + ($off_money*$active_kurs)*$sum_garant;// разница после  +
					$p_sc_in_off_usr = $polya_on['score_in'] + ($off_money*$active_kurs)*$sum_garant;// добавить в score_in
					$money_admin_plus = $money_admin_plus + ($off_money*$active_kurs)*(1 - $sum_garant);
					//$infa_zayav_s['id_paym_syst'];
					/*echo "------------------<br>";
					echo $id_user_off." - user_id<br>";
					echo "------------------<br>2 -- ;<br>";
					echo $peremennaya[$key11]['sustem_id']." - id_paym_system<br>";
					echo $polya_off['score_val']." - ".$off_money."<br>";
					echo $difference_off_user." - score_val<br>";
					echo $p_sc_out_off_usr." - score_out<br>";
					echo "------------------<br>2 ++ ;<br>";
					echo $infa_zayav_s['id_paym_syst']." - id_paym_system<br>";
					echo $off_money*$active_kurs."<br>";
					echo $summ_off_user." - score_val<br>";
					echo $p_sc_in_off_usr." - score_in<br>";
					*/
					// разница после -
					$sql115 = "UPDATE cms_user_score SET score_out='$p_sc_out_off_usr' WHERE user_id='$id_user_off' AND type_payment_id='".$peremennaya[$key11]['sustem_id']."'";
					$zapros3 = $inDB->query($sql115);
					// разница после +
					$sql116 = "UPDATE cms_user_score SET score_val='$summ_off_user', score_in='$p_sc_in_off_usr' WHERE user_id='$id_user_off' AND type_payment_id='".$infa_zayav_s['id_paym_syst']."'";
					$zapros4 = $inDB->query($sql116);
					// сумма + админу
					$sql1154 = "UPDATE cms_user_score SET score_val='$money_admin_plus' WHERE user_id='0'";
					$zapros5 = $inDB->query($sql1154);
					/* данные для отправки */
					
					if($zapros1 == true && $zapros2 == true && $zapros3 == true && $zapros4 == true && $zapros5 == true){
						
					//текущий юзер
					$name_user_1 = $inDB->get_field("cms_users", "id='$user_id'", "nickname");
					$name_paym_sys_1 = $inDB->get_field("cms_user_type_payment", "id='".$peremennaya[$key11]['sustem_id']."'", "name_sustem");
					$score_sys_1 = $inDB->get_field("cms_user_score", "user_id='$user_id' AND type_payment_id='".$peremennaya[$key11]['sustem_id']."'", "score_num");
					$money_1 = $off_money*$sum_garant;
					
					//юзер 2 с которым обмен
					
					$name_user_2 = $inDB->get_field("cms_users", "id='$id_user_off'", "nickname");
					$name_paym_sys_2 = $inDB->get_field("cms_user_type_payment", "id='".$infa_zayav_s['id_paym_syst']."'", "name_sustem");
					$score_sys_2 = $inDB->get_field("cms_user_score", "user_id='$id_user_off' AND type_payment_id='".$infa_zayav_s['id_paym_syst']."'", "score_num");
					$money_2 = ($off_money*$active_kurs)*$sum_garant;
					
					$inDB->query("INSERT cms_user_exchsnge_garant (user_id_1, money_plus_1, system_plus_1, score_1, user_id_2, money_plus_2, system_plus_2, score_2, status) VALUES ('$user_id','$money_1','".$peremennaya[$key11]['sustem_id']."','$score_sys_1','$id_user_off','$money_2','".$infa_zayav_s['id_paym_syst']."','$score_sys_2','1')");
					
					$data_report['user_id'] = $user_id;
					$data_report['description'] = 'Обмен гарант - гарант.';
					$data_report['currency'] = $peremennaya[$key11]['sustem_id'];
					$data_report['money'] = $money_1;
					$data_report['commission_serv'] = $off_money*(1 - $sum_garant);
					$model->EventReport($data_report);
					
					$data_report2['user_id'] = $id_user_off;
					$data_report2['description'] = 'Обмен гарант - гарант.';
					$data_report2['currency'] = $infa_zayav_s['id_paym_syst'];
					$data_report2['money'] = $money_2;
					$data_report2['commission_serv'] = ($off_money*$active_kurs)*(1 - $sum_garant);
					$model->EventReport($data_report2);
						
						cmsUser::sendMessage(USER_MASSMAIL, '1', "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>
						<b>- Обмен средств заявки</b> № ".$infa_zayav_s['id']."<br>
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>");
						
						cmsUser::sendMessage(USER_MASSMAIL, $user_id, "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств вашей заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>
						<b>- Обмен средств встречной заявки</b> № ".$infa_zayav_s['id']."<br>
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>");
						
						cmsUser::sendMessage(USER_MASSMAIL, $id_user_off, "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств вашей заявки</b> № ".$infa_zayav_s['id']."
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>
						<b>- Обмен средств встречной заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>");
						
						$inf_mail1 = array(); $inf_mail2 = array(); $inf_mail3 = array();
						
						$inf_mail1['email'] = $inDB->get_field("cms_users", "id='$user_id'", "email");
						$inf_mail1['message'] = "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств вашей заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>
						<b>- Обмен средств встречной заявки</b> № ".$infa_zayav_s['id']."<br>
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>";
							
						$email1 = $model->sendEmail3($inf_mail1);
						
						$inf_mail2['email'] = $inDB->get_field("cms_users", "id='$id_user_off'", "email");
						$inf_mail2['message'] = "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств вашей заявки</b> № ".$infa_zayav_s['id']."
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>
						<b>- Обмен средств встречной заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>";
							
						$email2 = $model->sendEmail3($inf_mail2);
						
						$config = cmsConfig::getDefaultConfig(); 
						$inf_mail3['email'] = $config['sitemail'];
						$inf_mail3['message'] = "<p><b>Обмен заявок на бирже обмена:</b></p>
						<b>- Обмен средств заявки</b> № $id_table;<br>
						* имя пользователя: $name_user_1;<br>
						* название ПС: $name_paym_sys_1;<br>
						* номер кошелька: $score_sys_1;<br>
						* к-во средств: $money_1 $;<br>
						<b>- Обмен средств заявки</b> № ".$infa_zayav_s['id']."<br>
						* имя пользователя: $name_user_2;<br>
						* название ПС: $name_paym_sys_2;<br>
						* номер кошелька: $score_sys_2;<br>
						* к-во средств: $money_2 $;<br>";
							
						$email3 = $model->sendEmail3($inf_mail3);
						
						cmsCore::redirect('/users/'.$user_id.'/exchange.html#obmen');
					}
				}
				else{// когда разные курсы
					$kurs_user_off = str_replace(',', '.', round($payment_user/$inf_click_s['money'], 3));
					$num_in_cycle = $key11;// $key с цикла
					$kurs_user_on = $peremennaya[$key11]['kurs'];
					$id_zamov = $infa_zayav_s[id];
					$obmen = $id_table;
					$isset_name4 = 1;
					 
				}
			}
			}
		}}
	}
		unset($peremennaya); 
		unset($key0);
		unset($foreach_inf);
		}
		unset($key); 
		unset($infa_zayav_s);
		unset($key);
		unset($masive_inf);
	}
	}
	else{
		cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#application_change');
	}
		
	if($isset_name1 == 1){
		if($isset_name2 == 1){
			if($isset_name3 == 1 || $isset_name6 == 1){
				if($isset_name5 == 1){
					cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#no_conditions');
				}
			
			}
			else{
				cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#no_garant');
			}
		}
		else{
			cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#no_application');
		}
	}
	else{
		cmsCore::redirect('/users/'.$user_id.'/exchange.html'.$number_page.$search_inf.$number_of_record.'#no_application');
	}
	
}
$time_start = date('Y/m/d H : i', time());
if($isset_name4 == 1){
	cmsPage::initTemplate('components', 'com_users_exchange')->
		assign('id', $user_id)->
		assign('time_start', $time_start)->
		assign('href_pagination', $href_pagination)->
		assign('search_id', $search_id)->
		assign('search_have', $search_have)->
		assign('search_give', $search_give)->
		assign('search_kurs', $search_kurs)->
		assign('search_country', $search_country)->
		assign('search_garant', $search_garant)->
		assign('search_part', $search_part)->
		assign('id_zamov', $id_zamov)->
		assign('num_in_cycle', $num_in_cycle)->
		assign('obmen', $obmen)->
		assign('kurs_user_off', $kurs_user_off)->
		assign('kurs_user_on', $kurs_user_on)->
		assign('request_payments', $request_payments_s)->
		assign('pammAccount', $getPammAccount)->
		assign('pammDefault', $getDefaultPammAccount)->
		assign('getPammCustomer', $getPammCustomer)->
		assign('userCash', $userCash)->
		assign('records', $number_of_records)->
		display('com_users_exchange.tpl');
        return;
	}
	
if($isset_name6 == 1){
	cmsPage::initTemplate('components', 'com_users_exchange')->
		assign('href_pagination', $href_pagination)->
		assign('id', $user_id)->
		assign('time_start', $time_start)->
		assign('inf_visible', $inf_visible)->
		assign('inf_hidden_on', $inf_hidden_on)->
		assign('inf_hidden_off', $inf_hidden_off)->
		assign('search_garant', $search_garant)->
		assign('search_part', $search_part)->
		assign('search_id', $search_id)->
		assign('search_have', $search_have)->
		assign('search_give', $search_give)->
		assign('search_kurs', $search_kurs)->
		assign('search_country', $search_country)->
		assign('obmen_id', $obmen_id)->
		assign('request_payments', $request_payments_s)->
		assign('pammAccount', $getPammAccount)->
		assign('pammDefault', $getDefaultPammAccount)->
		assign('getPammCustomer', $getPammCustomer)->
		assign('userCash', $userCash)->
		assign('records', $number_of_records)->
		display('com_users_exchange.tpl');
        return;
}


		
		//$id_table, kurs_user_off, kurs_user_on
	cmsPage::initTemplate('components', 'com_users_exchange')->
		assign('href_pagination', $href_pagination)->
		assign('id', $user_id)->
		assign('time_start', $time_start)->
		assign('search_garant', $search_garant)->
		assign('search_part', $search_part)->
		assign('search_id', $search_id)->
		assign('search_have', $search_have)->
		assign('search_give', $search_give)->
		assign('search_kurs', $search_kurs)->
		assign('search_country', $search_country)->
		assign('obmen_id', $obmen_id)->
		assign('request_payments', $request_payments_s)->
		assign('pammAccount', $getPammAccount)->
		assign('pammDefault', $getDefaultPammAccount)->
		assign('getPammCustomer', $getPammCustomer)->
		assign('userCash', $userCash)->
		assign('records', $number_of_records)->
		display('com_users_exchange.tpl');
}

if ($do=='info'){

    $inPage->addHeadJsLang(array('NEW_POST_ON_WALL','CONFIRM_DEL_POST_ON_WALL'));

	// если просмотр профиля гостям запрещен
	if (!$inUser->id && !$model->config['sw_guest']) {
        cmsUser::goToLogin();
	}

    if(is_numeric($login)) { cmsCore::error404(); }

    $usr = $model->getUser($inUser->id);
    if (!$usr){ cmsCore::error404(); }

    $myprofile  = ($inUser->id == $usr['id']);

    $inPage->setTitle($usr['nickname']);
    $inPage->addPathway($usr['nickname']);

	// просмотр профиля запрещен
    if (!cmsUser::checkUserContentAccess($usr['allow_who'], $usr['id'])){
        cmsPage::initTemplate('components', 'com_users_not_allow')->
                assign('is_auth', $inUser->id)->
                assign('usr', $usr)->
                display('com_users_not_allow.tpl');
        return;
    }
	// Профиль удален
    if ($usr['is_deleted']){
        cmsPage::initTemplate('components', 'com_users_deleted.tpl')->
                assign('usr', $usr)->
                assign('is_admin', $inUser->is_admin)->
                assign('others_active', $inDB->rows_count('cms_users', "login='{$usr['login']}' AND is_deleted=0", 1))->
                display('com_users_deleted.tpl');
        return;
    }

	// Данные о друзьях
	$usr['friends_total'] = cmsUser::getFriendsCount($usr['id']);
	$usr['friends']		  = cmsUser::getFriends($usr['id']);
	// очищать сессию друзей если в своем профиле и количество друзей из базы не совпадает с количеством друзей в сессии
	if ($myprofile && sizeof($usr['friends']) != $usr['friends_total']) { cmsUser::clearSessionFriends(); }
	// обрезаем список
	$usr['friends'] = array_slice($usr['friends'], 0, 6);
	// выясняем друзья ли мы с текущим пользователем
    $usr['isfriend'] = !$myprofile ? cmsUser::isFriend($usr['id']) : false;

	// награды пользователя
    $usr['awards'] = $model->config['sw_awards'] ? $model->getUserAwards($usr['id']) : false;

	// стена
	if($model->config['sw_wall']){
		$inDB->limitPage(1, $model->config['wall_perpage']);
        $usr['wall_html'] = cmsUser::getUserWall($usr['id'], 'users', $myprofile, $inUser->is_admin);
	}

	// можно ли пользователю изменять карму
    $usr['can_change_karma'] = $model->isUserCanChangeKarma($usr['id']) && $inUser->id;

	// Фотоальбомы пользователя
    if ($model->config['sw_photo']){
        $usr['albums']       = $model->getPhotoAlbums($usr['id'], $usr['isfriend'], !$inCore->isComponentEnable('photos'));
        $usr['albums_total'] = sizeof($usr['albums']);
        $usr['albums_show']  = 6;
        if ($usr['albums_total']>$usr['albums_show']){
            array_splice($usr['albums'], $usr['albums_show']);
        }
    }

    $usr['board_count']    = $model->config['sw_board'] ?
								$inDB->rows_count('cms_board_items', "user_id='{$usr['id']}' AND published=1") : 0;
    $usr['comments_count'] = $model->config['sw_comm'] ?
								$inDB->rows_count('cms_comments', "user_id='{$usr['id']}' AND published=1") : 0;
	$usr['forum_count']    = $model->config['sw_forum'] ?
								$inDB->rows_count('cms_forum_posts', "user_id = '{$usr['id']}'") : 0;
	$usr['files_count']    = $model->config['sw_files'] ?
								$inDB->rows_count('cms_user_files', "user_id = '{$usr['id']}'") : 0;

	$cfg_reg = $inCore->loadComponentConfig('registration');
	$usr['invites_count'] = ($inUser->id && $myprofile && $cfg_reg['reg_type'] == 'invite') ?
								$model->getUserInvitesCount($inUser->id) : 0;

	$usr['blog'] = $model->config['sw_blogs'] ?
								$inDB->get_fields('cms_blogs', "user_id = '{$usr['id']}' AND owner = 'user'", 'title, seolink') : false;

    $usr['form_fields'] = array();
	if (is_array($model->config['privforms'])){
		foreach($model->config['privforms'] as $form_id){
			$usr['form_fields'] = array_merge($usr['form_fields'], cmsForm::getFieldsValues($form_id, $usr['formsdata']));
		}
	}

    $plugins = $model->getPluginsOutput($usr);

    cmsPage::initTemplate('components', 'com_users_info.tpl')->
            assign('usr', $usr)->
            assign('plugins', $plugins)->
            assign('cfg', $model->config)->
            assign('myprofile', $myprofile)->
            assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
            assign('is_admin', $inUser->is_admin)->
            assign('is_auth', $inUser->id)->
            display('com_users_info.tpl');

}

if ($do=='invite'){
	
	$findsql = "SELECT * FROM cms_content WHERE id = '43' AND published = 1";
	$result1 = $inDB->query($findsql) ;
	$items = array();
	while($item1 = $inDB->fetch_assoc($result1))
	{
		$content_list[] = $item1;
	}
	
    if (cmsCore::inRequest('send_invite')){
	
		if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }
		
        //$invite_email = cmsCore::request('invite_email', 'email', '');
        $invite_email = cmsCore::request('invite_email', 'array', '');
        $invite_fio = cmsCore::request('fio', 'array', '');
		
        //if (!$invite_email) { cmsCore::redirectBack(); }
        if (!$invite_email || empty($invite_email) || empty($invite_email[0])) {
			//cmsCore::addSessionMessage($_LANG['INVITE_ERROR'], 'error'); cmsCore::redirectBack();
			cmsCore::redirect('/users/'.$inUser->id.'/invite.html#INVITE_ERROR');
		}
		
		foreach($invite_email as $em){
			$invite_email_arr .= $em . ", ";
		}
		$invite_email_arr = rtrim($invite_email_arr, ', ');
		
        //if ($model->sendInvite($inUser->id, $invite_email)){
        if ($model->sendInvite_array($inUser->id, $invite_email, $invite_fio)){
		
            //cmsCore::addSessionMessage(sprintf($_LANG['INVITE_SENDED'], $invite_email), 'success');
            //cmsCore::addSessionMessage(sprintf($_LANG['INVITE_SENDED'], $invite_email_arr), 'success');
			cmsCore::redirect('/users/'.$inUser->id.'/invite.html#INVITE_SENDED');
        } else {
		
            //cmsCore::addSessionMessage($_LANG['INVITE_ERROR'], 'error');
			cmsCore::redirect('/users/'.$inUser->id.'/invite.html#INVITE_ERROR');
        }
		cmsCore::redirect('/users/'.$inUser->id.'/invite.html');
        //cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
		
    }

	//var_dump($_POST);// exit();
    $inPage->addHeadJsLang(array('NEW_POST_ON_WALL','CONFIRM_DEL_POST_ON_WALL'));

	// если просмотр профиля гостям запрещен
	if (!$inUser->id && !$model->config['sw_guest']) {
        cmsUser::goToLogin();
	}

    if(is_numeric($login)) { cmsCore::error404(); }

    $usr = $model->getUser($inUser->id);
    if (!$usr){ cmsCore::error404(); }

    $myprofile  = ($inUser->id == $usr['id']);

    $inPage->setTitle($usr['nickname']);
    $inPage->addPathway($usr['nickname']);

	// просмотр профиля запрещен
    if (!cmsUser::checkUserContentAccess($usr['allow_who'], $usr['id'])){
        cmsPage::initTemplate('components', 'com_users_not_allow')->
                assign('is_auth', $inUser->id)->
                assign('usr', $usr)->
        
                display('com_users_not_allow.tpl');
        return;
    }
	// Профиль удален
    if ($usr['is_deleted']){
        cmsPage::initTemplate('components', 'com_users_deleted.tpl')->
                assign('usr', $usr)->
                assign('is_admin', $inUser->is_admin)->
                assign('others_active', $inDB->rows_count('cms_users', "login='{$usr['login']}' AND is_deleted=0", 1))->
                display('com_users_deleted.tpl');
        return;
    }

	// Данные о друзьях
	$usr['friends_total'] = cmsUser::getFriendsCount($usr['id']);
	$usr['friends']		  = cmsUser::getFriends($usr['id']);
	// очищать сессию друзей если в своем профиле и количество друзей из базы не совпадает с количеством друзей в сессии
	if ($myprofile && sizeof($usr['friends']) != $usr['friends_total']) { cmsUser::clearSessionFriends(); }
	// обрезаем список
	$usr['friends'] = array_slice($usr['friends'], 0, 6);
	// выясняем друзья ли мы с текущим пользователем
    $usr['isfriend'] = !$myprofile ? cmsUser::isFriend($usr['id']) : false;

	// награды пользователя
    $usr['awards'] = $model->config['sw_awards'] ? $model->getUserAwards($usr['id']) : false;

	// стена
	if($model->config['sw_wall']){
		$inDB->limitPage(1, $model->config['wall_perpage']);
        $usr['wall_html'] = cmsUser::getUserWall($usr['id'], 'users', $myprofile, $inUser->is_admin);
	}

	// можно ли пользователю изменять карму
    $usr['can_change_karma'] = $model->isUserCanChangeKarma($usr['id']) && $inUser->id;

	// Фотоальбомы пользователя
    if ($model->config['sw_photo']){
        $usr['albums']       = $model->getPhotoAlbums($usr['id'], $usr['isfriend'], !$inCore->isComponentEnable('photos'));
        $usr['albums_total'] = sizeof($usr['albums']);
        $usr['albums_show']  = 6;
        if ($usr['albums_total']>$usr['albums_show']){
            array_splice($usr['albums'], $usr['albums_show']);
        }
    }

    $usr['board_count']    = $model->config['sw_board'] ?
								$inDB->rows_count('cms_board_items', "user_id='{$usr['id']}' AND published=1") : 0;
    $usr['comments_count'] = $model->config['sw_comm'] ?
								$inDB->rows_count('cms_comments', "user_id='{$usr['id']}' AND published=1") : 0;
	$usr['forum_count']    = $model->config['sw_forum'] ?
								$inDB->rows_count('cms_forum_posts', "user_id = '{$usr['id']}'") : 0;
	$usr['files_count']    = $model->config['sw_files'] ?
								$inDB->rows_count('cms_user_files', "user_id = '{$usr['id']}'") : 0;

	$cfg_reg = $inCore->loadComponentConfig('registration');
	$usr['invites_count'] = ($inUser->id && $myprofile && $cfg_reg['reg_type'] == 'invite') ?
								$model->getUserInvitesCount($inUser->id) : 0;

	$usr['blog'] = $model->config['sw_blogs'] ?
								$inDB->get_fields('cms_blogs', "user_id = '{$usr['id']}' AND owner = 'user'", 'title, seolink') : false;

    $usr['form_fields'] = array();
	if (is_array($model->config['privforms'])){
		foreach($model->config['privforms'] as $form_id){
			$usr['form_fields'] = array_merge($usr['form_fields'], cmsForm::getFieldsValues($form_id, $usr['formsdata']));
		}
	}
	$proverochka = 0;
	$discount_table = $inDB->get_table('cms_user_discount','user_id="'.$usr['id'].'" OR user_id="0"');
	for($i=0;isset($discount_table[$i]);$i++){
		$date_nuv = strtotime(date("Y.m.d"));
		$date_end =  strtotime($discount_table[$i]['end_date']);
		if($date_nuv > $date_end){
			$inDB->query("DELETE FROM cms_user_discount WHERE id='".$discount_table[$i]['id']."'");
			$proverochka = 1;
		}
		$discount['id'] = $i + 1;
		$discount['discount'] = $discount_table[$i]['discount'];
		$discount['date'] = date('d.m.Y', strtotime($discount_table[$i]['date']));
		$discount['end_date'] = date('d.m.Y', strtotime($discount_table[$i]['end_date']));
		$discounts[] = $discount;
	}
	
	$referal_table = $inDB->get_table('cms_preferal','ref_id_from="'.$usr['id'].'"');
	$all_money = 0;
	foreach($referal_table as $result){
		$referal['id_client'] = $result['ref_id_to'];
		$referal['country'] = $inDB->get_field('cms_user_profiles','user_id="'.$result['ref_id_to'].'"','country');
		$referal['date_reg'] = date('d.m.Y', strtotime($result['time']));
		$referal['money'] = $result['money'];
		$referals[] = $referal;
		$all_money = $all_money + $result['money'];
	}

    $plugins = $model->getPluginsOutput($usr);
	$user_id2=$model->returnId();
	if($proverochka == 1){
		cmsCore::redirect('/users/'.$usr['id'].'/invite.html');
	}
    cmsPage::initTemplate('components', 'com_users_invites.tpl')->
            assign('all_money', $all_money)->
			assign('discounts', $discounts)->
			assign('usr', $usr)->
            assign('user_id', $user_id2)->
            assign('plugins', $plugins)->
            assign('cfg', $model->config)->
			assign('referals', $referals)->
            assign('myprofile', $myprofile)->
            assign('article', $content_list)->
            assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
            assign('is_admin', $inUser->is_admin)->
            assign('is_auth', $inUser->id)->
			        assign('login', $inUser->login)->
            display('com_users_invites.tpl');

}
// PAMM счета
if ($do=='pamm'){

    $inPage->addHeadJsLang(array('NEW_POST_ON_WALL','CONFIRM_DEL_POST_ON_WALL'));

	// если просмотр профиля гостям запрещен
	if (!$inUser->id && !$model->config['sw_guest']) {
        cmsUser::goToLogin();
	}

    if(is_numeric($login)) { cmsCore::error404(); }

    $usr = $model->getUser($inUser->id);
    if (!$usr){ cmsCore::error404(); }

    $myprofile  = ($inUser->id == $usr['id']);

    $inPage->setTitle($usr['nickname']);
    $inPage->addPathway($usr['nickname']);

	// просмотр профиля запрещен
    if (!cmsUser::checkUserContentAccess($usr['allow_who'], $usr['id'])){
        cmsPage::initTemplate('components', 'com_users_not_allow')->
                assign('is_auth', $inUser->id)->
                assign('usr', $usr)->
                display('com_users_not_allow.tpl');
        return;
    }
	// Профиль удален
    if ($usr['is_deleted']){
        cmsPage::initTemplate('components', 'com_users_deleted.tpl')->
                assign('usr', $usr)->
                assign('is_admin', $inUser->is_admin)->
                assign('others_active', $inDB->rows_count('cms_users', "login='{$usr['login']}' AND is_deleted=0", 1))->
                display('com_users_deleted.tpl');
        return;
    }

	// Данные о друзьях
	$usr['friends_total'] = cmsUser::getFriendsCount($usr['id']);
	$usr['friends']		  = cmsUser::getFriends($usr['id']);
	// очищать сессию друзей если в своем профиле и количество друзей из базы не совпадает с количеством друзей в сессии
	if ($myprofile && sizeof($usr['friends']) != $usr['friends_total']) { cmsUser::clearSessionFriends(); }
	// обрезаем список
	$usr['friends'] = array_slice($usr['friends'], 0, 6);
	// выясняем друзья ли мы с текущим пользователем
    $usr['isfriend'] = !$myprofile ? cmsUser::isFriend($usr['id']) : false;

	// награды пользователя
    $usr['awards'] = $model->config['sw_awards'] ? $model->getUserAwards($usr['id']) : false;

	// стена
	if($model->config['sw_wall']){
		$inDB->limitPage(1, $model->config['wall_perpage']);
        $usr['wall_html'] = cmsUser::getUserWall($usr['id'], 'users', $myprofile, $inUser->is_admin);
	}

	// можно ли пользователю изменять карму
    $usr['can_change_karma'] = $model->isUserCanChangeKarma($usr['id']) && $inUser->id;

	// Фотоальбомы пользователя
    if ($model->config['sw_photo']){
        $usr['albums']       = $model->getPhotoAlbums($usr['id'], $usr['isfriend'], !$inCore->isComponentEnable('photos'));
        $usr['albums_total'] = sizeof($usr['albums']);
        $usr['albums_show']  = 6;
        if ($usr['albums_total']>$usr['albums_show']){
            array_splice($usr['albums'], $usr['albums_show']);
        }
    }

    $usr['board_count']    = $model->config['sw_board'] ?
								$inDB->rows_count('cms_board_items', "user_id='{$usr['id']}' AND published=1") : 0;
    $usr['comments_count'] = $model->config['sw_comm'] ?
								$inDB->rows_count('cms_comments', "user_id='{$usr['id']}' AND published=1") : 0;
	$usr['forum_count']    = $model->config['sw_forum'] ?
								$inDB->rows_count('cms_forum_posts', "user_id = '{$usr['id']}'") : 0;
	$usr['files_count']    = $model->config['sw_files'] ?
								$inDB->rows_count('cms_user_files', "user_id = '{$usr['id']}'") : 0;

	$cfg_reg = $inCore->loadComponentConfig('registration');
	$usr['invites_count'] = ($inUser->id && $myprofile && $cfg_reg['reg_type'] == 'invite') ?
								$model->getUserInvitesCount($inUser->id) : 0;

	$usr['blog'] = $model->config['sw_blogs'] ?
								$inDB->get_fields('cms_blogs', "user_id = '{$usr['id']}' AND owner = 'user'", 'title, seolink') : false;

    $usr['form_fields'] = array();
	if (is_array($model->config['privforms'])){
		foreach($model->config['privforms'] as $form_id){
			$usr['form_fields'] = array_merge($usr['form_fields'], cmsForm::getFieldsValues($form_id, $usr['formsdata']));
		}
	}

    $plugins = $model->getPluginsOutput($usr);

    cmsPage::initTemplate('components', 'com_users_pamm.tpl')->
            assign('usr', $usr)->
            assign('plugins', $plugins)->
            assign('cfg', $model->config)->
            assign('myprofile', $myprofile)->
            assign('cfg_forum', $inCore->loadComponentConfig('forum'))->
            assign('is_admin', $inUser->is_admin)->
            assign('is_auth', $inUser->id)->
            display('com_users_pamm.tpl');

}


//============================================================================//
//============================= Список сообщений  ============================//
//============================================================================//
if ($do=='messages'){

	if (!$model->config['sw_msg']) { cmsCore::error404(); }

	if (!$inUser->id || ($inUser->id != $id && !$inUser->is_admin)){ cmsUser::goToLogin(); }

	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }

	$inPage->setTitle($_LANG['MY_MESS']);
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway($_LANG['MY_MESS'], '/users/'.$id.'/messages.html');

	include 'components/users/messages.php';
}

//============================================================================//
//=========================== Отправка сообщения  ============================//
//============================================================================//
if ($do=='sendmessage'){

	if (!$model->config['sw_msg']) { cmsCore::halt(); }

    if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::halt(); }

	if (!$inUser->id || ($inUser->id==$id &&
							!cmsCore::inRequest('massmail') &&
							!cmsCore::request('send_to_group', 'int', 0))){ cmsCore::halt(); }

	if(!cmsCore::inRequest('gosend')){

		$replyid = cmsCore::request('replyid', 'int', 0);

		if ($replyid){

			$msg = $model->getReplyMessage($replyid, $inUser->id);
			if(!$msg) { cmsCore::halt(); }

		}

        $inPage->setRequestIsAjax();

		cmsPage::initTemplate('components', 'com_users_messages_add')->
                assign('msg', isset($msg) ? $msg : array())->
                assign('is_reply_user', $replyid)->
                assign('id', $id)->
                assign('bbcodetoolbar', cmsPage::getBBCodeToolbar('message'))->
                assign('smilestoolbar', cmsPage::getSmilesPanel('message'))->
                assign('groups', $inUser->is_admin ? cmsUser::getGroups(true) : array())->
                assign('friends', cmsUser::getFriends($inUser->id))->
                assign('id_admin', $inUser->is_admin)->
                display('com_users_messages_add.tpl');

		cmsCore::jsonOutput(array('error' => false,
								  'html'  => ob_get_clean()
								));

	}

	if(cmsCore::inRequest('gosend')){

		//if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }

        // Кому отправляем
        $usr = cmsUser::getShortUserData($id);
        if (!$usr) { cmsCore::halt(); }

		$message = cmsCore::parseSmiles(cmsCore::request('message', 'html', ''), true);

		if (mb_strlen($message)<2){
			cmsCore::jsonOutput(array('error' => true, 'text' => $_LANG['ERR_SEND_MESS']));
		}

        $output = cmsCore::callEvent('USER_SEND_MESSEDGE', array('text'=>$message, 'to_id'=>$id));

        $message = $output['text'];
        $id      = $output['to_id'];

		$send_to_group = cmsCore::request('send_to_group', 'int', 0);
		$group_id      = cmsCore::request('group_id', 'int', 0);

		//
		// Обычная отправка (1 получатель)
		//
		if (!cmsCore::inRequest('massmail') && !$send_to_group){

			//отправляем сообщение
			$msg_id = cmsUser::sendMessage($inUser->id, $id, $message);
			// отправляем уведомление на email если нужно
			$model->sendNotificationByEmail($id, $inUser->id, $msg_id);

			cmsCore::jsonOutput(array('error' => false, 'text' => $_LANG['SEND_MESS_OK']));

		}

		//
		// далее идут массовые рассылки, доступные только админам
		//
		if (!$inUser->is_admin){ cmsCore::halt(); }

		// отправить всем: получаем список всех пользователей
		if (cmsCore::inRequest('massmail')) {

			$userlist = cmsUser::getAllUsers();
			// проверяем что есть кому отправлять
			if (!$userlist){
				cmsCore::jsonOutput(array('error' => false, 'text' => $_LANG['ERR_SEND_MESS']));
			}
			$count = array();
			// отправляем всем по списку
			foreach ($userlist as $usr){
				$count[] = cmsUser::sendMessage(USER_MASSMAIL, $usr['id'], $message);
			}

			cmsCore::jsonOutput(array('error' => false, 'text' => sprintf($_LANG['SEND_MESS_ALL_OK'], sizeof($count))));

		}

		// отправить группе: получаем список членов группы
		if ($send_to_group) {

			$count = cmsUser::sendMessageToGroup(USER_MASSMAIL, $group_id, $message);
			$success_msg = sprintf($_LANG['SEND_MESS_GROUP_OK'], $count, cmsUser::getGroupTitle($group_id));

			cmsCore::jsonOutput(array('error' => false, 'text' => $success_msg));

		}

	}

}
//============================================================================//
//============================= Удаление сообщения  ==========================//
//============================================================================//
if ($do=='delmessage'){

    if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::halt(); }

    if (!$model->config['sw_msg']) { cmsCore::halt(); }

    if (!$inUser->id) { cmsCore::halt(); }

    $msg = $inDB->get_fields('cms_user_msg', "id='$id'", '*');
    if (!$msg){ cmsCore::halt(); }

    $can_delete = ($inUser->id == $msg['to_id'] || $inUser->id == $msg['from_id']) ? true : false;
    if(!$can_delete && !$inUser->is_admin){ cmsCore::halt(); }

    // Сообщения с from_id < 0
    if ($msg['from_id'] < 0){
        $inDB->query("DELETE FROM cms_user_msg WHERE id = '$id' LIMIT 1");
        $info_text = $_LANG['MESS_NOTICE_DEL_OK'];
    }
    // мне сообщение от пользователя
    if ($msg['to_id']==$inUser->id && $msg['from_id'] > 0){
        $inDB->query("UPDATE cms_user_msg SET to_del=1 WHERE id='{$id}'");
        $info_text = $_LANG['MESS_DEL_OK'];
    }
    // от меня сообщение
    if ($msg['from_id']==$inUser->id && !$msg['is_new']){
        $inDB->query("UPDATE cms_user_msg SET from_del=1 WHERE id='{$id}'");
        $info_text = $_LANG['MESS_DEL_OK'];
    }
    // отзываем сообщение
    if ($msg['from_id']==$inUser->id && $msg['is_new']){
        $inDB->query("DELETE FROM cms_user_msg WHERE id = '$id' LIMIT 1");
        $info_text = $_LANG['MESS_BACK_OK'];
    }
    // удаляем сообщения, которые удалены с двух сторон
    $inDB->query("DELETE FROM cms_user_msg WHERE to_del=1 AND from_del=1");

    cmsCore::jsonOutput(array('error' => false, 'text' => $info_text));

}
//============================================================================//
//=========================== Удаление сообщений  ============================//
//============================================================================//
if ($do=='delmessages'){

	if (!$model->config['sw_msg']) { cmsCore::error404(); }

    if ($inUser->id != $id && !$inUser->is_admin){ cmsCore::error404(); }

    $usr = cmsUser::getShortUserData($id);
    if (!$usr) { cmsCore::error404(); }

    $opt = cmsCore::request('opt', 'str', 'in');

    if($opt == 'notices'){

        $inDB->query("DELETE FROM cms_user_msg WHERE to_id = '{$id}' AND from_id < 0");

    } else {

        $del_flag = $opt=='in' ? 'to_del' : 'from_del';
        $id_flag  = $opt=='in' ? 'to_id' : 'from_id';

        $inDB->query("UPDATE cms_user_msg SET {$del_flag}=1 WHERE {$id_flag}='{$id}'");
        $inDB->query("DELETE FROM cms_user_msg WHERE to_del=1 AND from_del=1");

    }

    cmsCore::addSessionMessage($_LANG['MESS_ALL_DEL_OK'], 'info');

	cmsCore::redirectBack();

}
//============================================================================//
//============================= Загрузка аватара  ============================//
//============================================================================//
if ($do=='avatar'){

	if (!$inUser->id || ($inUser->id && $inUser->id != $id)){ cmsCore::error404(); }

	$inPage->setTitle($_LANG['LOAD_AVATAR']);
	$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
	$inPage->addPathway($_LANG['LOAD_AVATAR']);

	if (cmsCore::inRequest('upload')) {

		cmsCore::loadClass('upload_photo');
		$inUploadPhoto = cmsUploadPhoto::getInstance();
		// Выставляем конфигурационные параметры
		$inUploadPhoto->upload_dir    = PATH.'/images/';
		$inUploadPhoto->dir_medium    = 'users/avatars/';
		$inUploadPhoto->dir_small     = 'users/avatars/small/';
		$inUploadPhoto->small_size_w  = $model->config['smallw'];
		$inUploadPhoto->medium_size_w = $model->config['medw'];
		$inUploadPhoto->medium_size_h = $model->config['medh'];
		$inUploadPhoto->is_watermark  = false;
		$inUploadPhoto->input_name    = 'picture';

		$file = $inUploadPhoto->uploadPhoto($inUser->orig_imageurl);

		if(!$file){

			cmsCore::addSessionMessage('<strong>'.$_LANG['ERROR'].':</strong> '.cmsCore::uploadError().'!', 'error');
			cmsCore::redirect('/users/'.$id.'/avatar.html');

		}

		$sql = "UPDATE cms_user_profiles SET imageurl = '{$file['filename']}' WHERE user_id = '$id' LIMIT 1";
		$inDB->query($sql);
		// очищаем предыдущую запись о смене аватара
		cmsActions::removeObjectLog('add_avatar', $id);
		// выводим сообщение в ленту
		cmsActions::log('add_avatar', array(
			  'object' => '',
			  'object_url' => '',
			  'object_id' => $id,
			  'target' => '',
			  'target_url' => '',
			  'description' => '<a href="'.cmsUser::getProfileURL($inUser->login).'" class="act_usr_ava">
								   <img border="0" src="/images/users/avatars/small/'.$file['filename'].'">
								</a>'
		));

		//cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html');

	} else {

		cmsPage::initTemplate('components', 'com_users_avatar_upload')->
                assign('id', $id)->
                display('com_users_avatar_upload.tpl');

	}
}
//============================================================================//
//============================= Библиотека аватаров  =========================//
//============================================================================//
if ($do=='select_avatar'){

	if (!$inUser->id || ($inUser->id && $inUser->id != $id)){ cmsCore::error404(); }

	$avatars_dir     = PATH."/images/users/avatars/library";
	$avatars_dir_rel = "/images/users/avatars/library";

	$avatars_dir_handle = opendir($avatars_dir);
	$avatars            = array();

	while ($nextfile = readdir($avatars_dir_handle)){
		if(($nextfile!='.')&&($nextfile!='..')&&( mb_strstr($nextfile, '.gif') || mb_strstr($nextfile, '.jpg') || mb_strstr($nextfile, '.jpeg') || mb_strstr($nextfile, '.png')  ) ){
			$avatars[] = $nextfile;
		}
	}

	closedir($avatars_dir_handle);

	if (!cmsCore::inRequest('set_avatar')){

		$inPage->setTitle($_LANG['SELECT_AVATAR']);
		$inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
		$inPage->addPathway($_LANG['SELECT_AVATAR']);

		$perpage = 20;

		$total   = sizeof($avatars);
		$avatars = array_slice($avatars, ($page-1)*$perpage, $perpage);

		cmsPage::initTemplate('components', 'com_users_avatars')->
                assign('userid', $id)->
                assign('avatars', $avatars)->
                assign('avatars_dir', $avatars_dir_rel)->
                assign('page', $page)->
                assign('perpage', $perpage)->
                assign('pagebar', cmsPage::getPagebar($total, $page, $perpage, '/users/%user_id%/select-avatar-%page%.html', array('user_id'=>$id)))->
                display('com_users_avatars.tpl');

	} else {

		$avatar_id = cmsCore::request('avatar_id', 'int', 0);
		$file      = $avatars[$avatar_id];

		if (file_exists($avatars_dir.'/'.$file)){

			$uploaddir 	  = PATH.'/images/users/avatars/';
			$realfile     = $file;
			$filename 	  = md5($realfile . '-' . $id . '-' . time()).'.jpg';
			$uploadfile	  = $avatars_dir . '/' . $realfile;
			$uploadavatar = $uploaddir . $filename;
			$uploadthumb  = $uploaddir . 'small/' . $filename;

			if ($inUser->orig_imageurl && $inUser->orig_imageurl != 'nopic.jpg'){
				@unlink(PATH.'/images/users/avatars/'.$inUser->orig_imageurl);
				@unlink(PATH.'/images/users/avatars/small/'.$inUser->orig_imageurl);
			}

			cmsCore::includeGraphics();
			copy($uploadfile, $uploadavatar);
			@img_resize($uploadfile, $uploadthumb, $model->config['smallw'], $model->config['smallw']);

			$sql = "UPDATE cms_user_profiles SET imageurl = '$filename' WHERE user_id = '$id' LIMIT 1";
			$inDB->query($sql);

			// очищаем предыдущую запись о смене аватара
			cmsActions::removeObjectLog('add_avatar', $id);
			// выводим сообщение в ленту
			cmsActions::log('add_avatar', array(
				  'object' => '',
				  'object_url' => '',
				  'object_id' => $id,
				  'target' => '',
				  'target_url' => '',
				  'description' => '<a href="'.cmsUser::getProfileURL($inUser->login).'" class="act_usr_ava">
										<img border="0" src="/images/users/avatars/small/'.$filename.'">
									</a>'
			));

		}

		//cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
		cmsCore::redirect('/users/'.$inUser->id.'/editprofile.html#changes_avatar');

	}

}
//============================================================================//
//======================== Работа с фотографиями  ============================//
//============================================================================//
if ($do=='photos'){

    if (!$model->config['sw_photo']) { cmsCore::error404(); }

    $pdo = cmsCore::request('pdo', 'str', '');

    include 'components/users/photos.php';

}
//============================================================================//
//============================= Друзья пользователя  =========================//
//============================================================================//
if ($do=='friendlist'){

	if (!$inUser->id) { cmsUser::goToLogin(); }

	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }

	$perpage = 10;

	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway($_LANG['FRIENDS']);
	$inPage->setTitle($_LANG['FRIENDS']);

	// все друзья
	$friends = cmsUser::getFriends($usr['id']);
	// их общее количество
	$total = count($friends);
	// получаем только нужных на странице
	$friends = array_slice($friends, ($page-1)*$perpage, $perpage);

    cmsPage::initTemplate('components', 'com_users_friends')->
            assign('friends', $friends)->
            assign('usr', $usr)->
            assign('myprofile', ($id == $inUser->id))->
            assign('total', $total)->
            assign('pagebar', cmsPage::getPagebar($total, $page, $perpage, 'javascript:centerLink(\'/users/'.$id.'/friendlist%page%.html\')'))->
            display('com_users_friends.tpl');

}

//============================================================================//
//============================= Запрос на дружбу  ============================//
//============================================================================//
if ($do == 'addfriend'){

    if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::halt(); }

	if (!$inUser->id || $inUser->id == $id) { cmsCore::halt(); }

    $usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::halt(); }

    cmsUser::clearSessionFriends();

	if(cmsUser::isFriend($id)){ cmsCore::jsonOutput(array('error' => true, 'text' => $_LANG['YOU_ARE_BE_FRIENDS'])); }

    // проверяем был ли ранее запрос на дружбу
    // если был, то делаем accept запросу
    $is_need_accept_id = cmsUser::getFriendFieldId($id, 0, 'to_me');
    if($is_need_accept_id){

        $inDB->query("UPDATE cms_user_friends SET is_accepted = 1 WHERE id = '{$is_need_accept_id}'");
        //регистрируем событие
        cmsActions::log('add_friend', array(
            'object' => $inUser->nickname,
            'user_id' => $usr['id'],
            'object_url' => cmsUser::getProfileURL($inUser->login),
            'object_id' => $is_need_accept_id,
            'target' => '',
            'target_url' => '',
            'target_id' => 0,
            'description' => ''
        ));

        cmsCore::callEvent('USER_ACCEPT_FRIEND', $id);

        cmsCore::jsonOutput(array('error' => false, 'text' => $_LANG['ADD_FRIEND_OK'] . $usr['nickname']));

    }

    // Если пользователь пытается добавиться в друзья к
    // пользователю, к которому уже отправил запрос
    if(cmsUser::getFriendFieldId($id, 0, 'from_me')){
        cmsCore::jsonOutput(array('error' => true, 'text' => $_LANG['ADD_TO_FRIEND_SEND_ERR']));
    }

    // Мы вообще не друзья с пользователем, создаем запрос
    cmsUser::addFriend($id);

    cmsUser::sendMessage(USER_UPDATER, $id, sprintf($_LANG['RECEIVED_F_O'],
        cmsUser::getProfileLink($inUser->login, $inUser->nickname),
        '<a class="ajaxlink" href="javascript:void(0)" onclick="users.acceptFriend('.$inUser->id.', this);return false;">'.$_LANG['ACCEPT'].'</a>',
        '<a class="ajaxlink" href="javascript:void(0)" onclick="users.rejectFriend('.$inUser->id.', this);return false;">'.$_LANG['REJECT'].'</a>'));

    cmsCore::jsonOutput(array('error' => false, 'text' => $_LANG['ADD_TO_FRIEND_SEND']));

}
//============================================================================//
//============================= Прекращение дружбы  ==========================//
//============================================================================//
if ($do == 'delfriend'){

    if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::halt(); }

	if (!$inUser->id || $inUser->id == $id){ cmsCore::halt(); }

    $usr = cmsUser::getShortUserData($id);
    if (!$usr) { cmsCore::error404(); }

    if(cmsUser::getFriendFieldId($id)){

        $is_accepted_friend = cmsUser::isFriend($id);

        if(cmsUser::deleteFriend($id)){

            // Если подтвержденный друг
            if($is_accepted_friend){
                cmsCore::jsonOutput(array('error' => false, 'text' => $usr['nickname'] . $_LANG['DEL_FRIEND']));
            } else {
                cmsCore::jsonOutput(array('error' => false, 'text' => $_LANG['REJECT_FRIEND'].$usr['nickname']));
            }

        } else {
            cmsCore::halt();
        }

    } else {

        cmsCore::halt();

    }

}
//============================================================================//
//============================= История кармы  ===============================//
//============================================================================//
if ($do=='karma'){

	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }

	$inPage->setTitle($_LANG['KARMA_HISTORY']);
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway($_LANG['KARMA_HISTORY']);
	
	cmsPage::initTemplate('components', 'com_users_karma')->
            assign('karma', $model->getUserKarma($usr['id']))->
            assign('usr', $usr)->
            display('com_users_karma.tpl');
				
}

/* ************************************************ */
if ($do=='outpay'){
	
	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }
	
	$outpay_money = 0;
	/**/
	$user_expenses = $inDB->get_table("cms_user_expenses", "user_id = '{$inUser->id}'", "*");
	
	foreach($user_expenses as $key => $expense){
		$expense_info = $inDB->get_fields('cms_user_expense_type', "id = '{$expense['expense_type_id']}' AND expense_status = '1'", "expense_title, expense_code, expense_status");
		$expense['expense_title'] 	= $expense_info['expense_title'];
		$expense['expense_code'] 	= $expense_info['expense_code'];
		$expense['expense_outpay'] 	= $expense['expense_outpay']/100;
		$expense['expense_inpay'] 	= $expense['expense_inpay']/100;
		$expense['expense_money'] 	= $expense['expense_money']/100;
		$expense['expense_status'] 	= $expense_info['expense_status'];
		$expense['currency'] 		= $inDB->get_field('cms_currency', "id = '{$expense['currency_id']}' AND status = '1'", "title");
		//echo "<pre>"; var_dump($expense);
		$expenses[$expense['expense_code']] = $expense;
	}
	//echo "<pre>"; var_dump($expenses);
	
	if (isset($_POST['outpay_money'])){
		// проверяем каптчу
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$inUser->id.'/outpay.html#error');
        }
		else{
			$_POST['outpay_money'] = str_replace(",", ".", $_POST['outpay_money']);
			$outpay_money = floatval($_POST['outpay_money']);
			if($expenses['invest']['expense_money'] >= $outpay_money){
				
				/* берем сумму с инвестиционного счета */
				$invest_expense_money = floatval($expenses['invest']['expense_money'] - $outpay_money);
				$update_array['expense_money'] = $invest_expense_money * 100;
				$update_array['expense_outpay'] = floatval($expenses['invest']['expense_outpay'] + $outpay_money) * 100;
				$where_array['expense_type_id'] = '2';
				$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				/* закидываем на главный счет */
				$main_expense_money = floatval($expenses['main']['expense_money'] + $outpay_money);
				$update_array['expense_money'] = $main_expense_money * 100;
				$where_array['expense_type_id'] = '1';
				//echo "<pre>"; var_dump($main_expense_money); echo "</pre>";
				$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				cmsUser::sessionPut('outpay_money', $outpay_money); // переменная с переводимой суммой для отображения во всплывающем окне
				
				cmsCore::redirect('/users/'.$inUser->id.'/outpay.html#success');
			}
			else{
				cmsCore::redirect('/users/'.$inUser->id.'/outpay.html#error_money');
			}
		}
	}
	
	if(cmsUser::sessionGet('outpay_money')){
		$outpay_money = cmsUser::sessionGet('outpay_money');
		cmsUser::sessionPut('outpay_money', 0);
	}
	
	$inPage->setTitle("Заявка на вывод средств");
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway("Заявка на вывод средств");
	cmsPage::initTemplate('components', 'com_users_outpay')->
            assign('usr', $usr)->
            assign('outpay_money', $outpay_money)->
			assign('expenses', $expenses)->
			assign('ids',  $usr['login'])->
            display('com_users_outpay.tpl');
}


/* ------------------ Лицевой счёт ------------------- */


if ($do=='investglav'){

	/* print_r($_POST['ik_am']); */
	//$usr = $model->getUser($inUser->login);
	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }
	
	$user_main_expense = $inDB->get_fields("cms_user_expenses", "expense_type_id = 1", "*");
	//var_dump(floatval($user_main_expense["expense_money"]));
	
	$user_main_expense['expense_outpay'] 	= $user_main_expense['expense_outpay']/100;
	$user_main_expense['expense_inpay'] 	= $user_main_expense['expense_inpay']/100;
	$user_main_expense['expense_money'] 	= $user_main_expense['expense_money']/100;
	
	if (isset($_POST['ik_am'])){
		
		$expense_money = $user_main_expense["expense_money"] + (int)$_POST['ik_am'];
		
		$update_array['expense_money'] = $expense_money * 100;
		$where_array['expense_type_id'] = '1';
		
		$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
		unset($update_array); unset($where_array);
		
		/* $inDB->query($sql12); */
		cmsCore::redirectBack();
	}
	
	$inPage->setTitle("Пополнение лицевого счета");
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway("Пополнение лицевого счета");
	cmsPage::initTemplate('components', 'com_users_invest')->
            assign('karma', $model->getUserKarma($usr['id']))->
            assign('user_main_expense', $user_main_expense)->
			assign('ids',  $usr['login'])->
            display('com_users_investglav.tpl');
		
}

/* ------------------ Инвестиционный счёт ------------------- */

if ($do=='invest'){

	//$usr = $model->getUser($inUser->login);
	
	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }
	
	$invest_money = 0;
	/**/
	$user_expenses = $inDB->get_table("cms_user_expenses", "user_id = '{$inUser->id}'", "*");
	
	foreach($user_expenses as $key => $expense){
		$expense_info = $inDB->get_fields('cms_user_expense_type', "id = '{$expense['expense_type_id']}' AND expense_status = '1'", "expense_title, expense_code, expense_status");
		$expense['expense_title'] 	= $expense_info['expense_title'];
		$expense['expense_code'] 	= $expense_info['expense_code'];
		$expense['expense_outpay'] 	= $expense['expense_outpay']/100;
		$expense['expense_inpay'] 	= $expense['expense_inpay']/100;
		$expense['expense_money'] 	= $expense['expense_money']/100;
		$expense['expense_status'] 	= $expense_info['expense_status'];
		$expense['currency'] 		= $inDB->get_field('cms_currency', "id = '{$expense['currency_id']}' AND status = '1'", "title");
		//echo "<pre>"; var_dump($expense);
		$expenses[$expense['expense_code']] = $expense;
	}
	//echo "<pre>"; var_dump($expenses);
	
	if (isset($_POST['invest_money'])){
		// проверяем каптчу
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect('/users/'.$inUser->id.'/invest.html#error');
        }
		else{
			$_POST['invest_money'] = str_replace(",", ".", $_POST['invest_money']);
			$invest_money = floatval($_POST['invest_money']);
			//echo "<pre>"; var_dump($invest_money); echo "</pre>";
			if($expenses['main']['expense_money'] >= $invest_money){
				/* берем сумму с главного счета */
				$main_expense_money = floatval($expenses['main']['expense_money'] - $invest_money);
				$update_array['expense_money'] = $main_expense_money * 100;
				$where_array['expense_type_id'] = '1';
				//echo "<pre>"; var_dump($main_expense_money); echo "</pre>";
				$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				/* добавляем сумму на инвестиционный счет */
				$invest_expense_money = floatval($expenses['invest']['expense_money'] + $invest_money);
				$update_array['expense_money'] = $invest_expense_money * 100;
				$update_array['expense_inpay'] = floatval($expenses['invest']['expense_inpay'] + $invest_money) * 100;
				$where_array['expense_type_id'] = '2';
				$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				cmsUser::sessionPut('invest_money', $invest_money);
				
				cmsCore::redirect('/users/'.$inUser->id.'/invest.html#success');
			}
			else{
				cmsCore::redirect('/users/'.$inUser->id.'/invest.html#error_money');
			}
		}
	}
	
	if(cmsUser::sessionGet('invest_money')){
		$invest_money = cmsUser::sessionGet('invest_money');
		cmsUser::sessionPut('invest_money', 0);
	}
	
	//$inPage->setTitle($_LANG['KARMA_HISTORY']);
	$inPage->setTitle("Пополнение инвестиционного счета");
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway("Пополнение инвестиционного счета");
	cmsPage::initTemplate('components', 'com_users_invest')->
            assign('usr', $usr)->
            assign('invest_money', $invest_money)->
			assign('expenses', $expenses)->
			assign('ids',  $usr['login'])->
            display('com_users_invest.tpl');
}

if ($do=='invest_pamm'){

	$usr = $model->getUser($inUser->login);
	$pamm_id = cmsCore::request('pamm_id', 'int', 1);
	
	/* user_expenses - счета пользователя */
	$user_expenses = $inDB->get_table("cms_user_expenses", "user_id = '{$inUser->id}'", "*");
	
	foreach($user_expenses as $key => $expense){
		$expense_info = $inDB->get_fields('cms_user_expense_type', "id = '{$expense['expense_type_id']}' AND expense_status = '1'", "expense_title, expense_code, expense_status");
		$expense['expense_title'] 	= $expense_info['expense_title'];
		$expense['expense_code'] 	= $expense_info['expense_code'];
		$expense['expense_outpay'] 	/=100;
		$expense['expense_inpay'] 	/=100;
		$expense['expense_money'] 	/=100;
		$expense['expense_status'] 	= $expense_info['expense_status'];
		$expense['currency'] 		= $inDB->get_field('cms_currency', "id = '{$expense['currency_id']}' AND status = '1'", "title");
		//echo "<pre>"; var_dump($expense);
		$expenses[$expense['expense_code']] = $expense;
	}
	//echo "<pre>"; var_dump($expenses);
	
	/* достаем нужный памм портфель юзера*/
	
	/*echo "<pre style='color: green;'>"; var_dump($relation); echo "</pre>";
	echo "<pre style='color: red;'>"; var_dump($pamm); echo "</pre>";*/
	$pamm = $inDB->get_fields('cms_pamms', "id = {$pamm_id}", '*');
	$phpdate = strtotime($pamm['end_date']);
	$pamm['end_date'] = date( 'd.m.y', $phpdate );
	$pamm['currency'] = $inDB->get_field('cms_currency', "id = '{$pamm['currency_id']}' AND status = '1'", "title");
	$pamm['outpay'] /=100;
	$pamm['inpay'] /=100;
	$pamm['money'] /=100;
	$pamm['capitalization'] /=100;
	$pamm['income'] /=100;
	$user_pamm = $pamm;
	//echo "<pre>"; var_dump($user_pamm); echo "</pre>";
	
	if (isset($_POST['invest_money'])){
		// проверяем каптчу
		if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::redirect("/users/".$inUser->id."/invest_pamm{$pamm_id}.html#error");
        }
		else{
			$_POST['invest_money'] = str_replace(",", ".", $_POST['invest_money']);
			$invest_money = floatval($_POST['invest_money']);
			//echo "<pre>"; var_dump($invest_money); echo "</pre>";
			
			if($expenses['invest']['expense_money'] >= $invest_money){
				/* берем сумму с инвестиционного счета */
				$invest_expense_money = floatval($expenses['invest']['expense_money'] - $invest_money);
				$update_array['expense_money'] = $invest_expense_money * 100;
				$update_array['expense_outpay'] = floatval($expenses['invest']['expense_outpay'] + $invest_money) * 100;
				$where_array['expense_type_id'] = '2';
				$inDB->update_where("cms_user_expenses", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				/* зачисляем сумму на памм-портфель */
				$pamm_money = floatval($user_pamm['money'] + $invest_money);
				$update_array['money'] = $pamm_money * 100;
				$update_array['inpay'] = floatval($user_pamm['inpay'] + $pamm_money)*100;
				$where_array['id'] = $user_pamm['id'];
				//echo "<pre>"; var_dump($pamm_money); echo "</pre>";
				$inDB->update_where("cms_pamms", $update_array, $where_array); // обновляем данные о счете
				unset($update_array); unset($where_array);
				
				
				cmsUser::sessionPut('invest_money', $invest_money);
				
				cmsCore::redirect("/users/".$inUser->id."/invest_pamm{$pamm_id}.html#success");
			}
			else{
				cmsCore::redirect("/users/".$inUser->id."/invest_pamm{$pamm_id}.html#error_money");
			}
		}
	}
	
	if(cmsUser::sessionGet('invest_money')){
		$invest_money = cmsUser::sessionGet('invest_money');
		cmsUser::sessionPut('invest_money', 0);
	}
	
	
	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }
	
	$inPage->setTitle("Пополнение ПАММ - портфеля");
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway("Пополнение ПАММ - портфеля");
	cmsPage::initTemplate('components', 'com_users_invest')->
            assign('usr', $usr)->
            assign('user_pamm', $user_pamm)->
            assign('invest_money', $invest_money)->
			assign('expenses', $expenses)->
			assign('ids',  $usr['login'])->
            display('com_users_invest_pamm.tpl');
}



//============================================================================//
//============================= Изменение кармы  =============================//
//============================================================================//
if ($do=='votekarma'){

    if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') { cmsCore::halt(); }

	if (!$inUser->id){ cmsCore::halt(); }

	$points = (cmsCore::request('sign', 'str', 'plus')=='plus' ? 1 : -1);
    $to     = cmsCore::request('to', 'int', 0);

    $user = cmsUser::getShortUserData($to);
    if (!$user) { cmsCore::halt(); }

	if (!$model->isUserCanChangeKarma($to)){ cmsCore::halt(); }

	cmsCore::halt(cmsUser::changeKarmaUser($to, $points));

}
//============================================================================//
//======================= Наградить пользователя  ============================//
//============================================================================//
if ($do=='giveaward'){

    if (!$inUser->is_admin) { cmsCore::error404(); }

	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }

	$inPage->setTitle($_LANG['AWARD_USER']);
	$inPage->addPathway($usr['nickname'], cmsUser::getProfileURL($usr['login']));
	$inPage->addPathway($_LANG['AWARD']);

	if(!cmsCore::inRequest('gosend')){

		cmsPage::initTemplate('components', 'com_users_awards_give')->
                assign('usr', $usr)->
                assign('awardslist', cmsUser::getAwardsImages())->
                display('com_users_awards_give.tpl');

	} else {

		$award['title']       = cmsCore::request('title', 'str', $_LANG['AWRD']);
		$award['description'] = cmsCore::request('description', 'str', '');
		$award['imageurl']    = cmsCore::request('imageurl', 'str', '');
        $award['from_id']     = $inUser->id;
        $award['id'] = 0;

        cmsUser::giveAward($award, $id);

		cmsCore::redirect(cmsUser::getProfileURL($usr['login']));

	}

}
//============================================================================//
//============================= Удаление награды  ============================//
//============================================================================//
if ($do=='delaward'){

	$aw = $inDB->get_fields('cms_user_awards', "id = '$id'", '*');
    if (!$aw){ cmsCore::error404(); }

	if (!$inUser->id || ($inUser->id!=$aw['user_id'] && !$inUser->is_admin)){ cmsCore::error404(); }

	$inDB->delete('cms_user_awards', "id = '$id'", 1);

	cmsActions::removeObjectLog('add_award', $id);

	cmsCore::redirectBack();

}
//============================================================================//
//============================= Награды на сайте  ============================//
//============================================================================//
if ($do=='awardslist'){

	$inPage->setTitle($_LANG['SITE_AWARDS']);
	$inPage->addPathway($_LANG['SITE_AWARDS']);

	$awards = cmsUser::getAutoAwards();
    if (!$awards){ cmsCore::error404(); }

    foreach ($awards as $aw) {

        //Перебираем все награды и ищем пользователей с текущей наградой
        $sql =  "SELECT u.id as id, u.nickname as nickname, u.login as login, IFNULL(p.gender, 'm') as gender
                 FROM cms_user_awards aw
                 LEFT JOIN cms_users u ON u.id = aw.user_id
                 LEFT JOIN cms_user_profiles p ON p.user_id = u.id
                 WHERE aw.award_id = '{$aw['id']}'";
        $rs = $inDB->query($sql);
        $aw['uhtml'] = '';
        if ($inDB->num_rows($rs)){

            while ($user = $inDB->fetch_assoc($rs)){
                $aw['uhtml'] .= cmsUser::getGenderLink($user['id'], $user['nickname'], $user['gender'], $user['login']).', ';
            }

            $aw['uhtml'] = rtrim($aw['uhtml'], ', ');

        } else {
            $aw['uhtml'] = $_LANG['NOT_USERS_WITH_THIS_AWARD'];
        }

        $aws[] = $aw;

    }

	cmsPage::initTemplate('components', 'com_users_awards_site')->
            assign('aws', $aws)->
            display('com_users_awards_site.tpl');

}
//============================================================================//
//============================= Удаление профиля  ============================//
//============================================================================//
if ($do == 'delprofile'){

	// неавторизованным тут делать нечего
	if (!$inUser->id) { cmsCore::error404(); }

	// есть ли удаляемый профиль
	$data = cmsUser::getShortUserData($id);
	if (!$data) { cmsCore::error404(); }

	// владелец профиля или админ
	if($inUser->is_admin){
		// могут ли администраторы удалять профиль
		if (!cmsUser::isAdminCan('admin/users', cmsUser::getAdminAccess())) { cmsCore::error404(); }
		// администратор сам себя не удалит
		if ($inUser->id == $data['id']){ cmsCore::error404(); }
	} else {
		// удаляем только свой профиль
		if ($inUser->id != $data['id']){ cmsCore::error404(); }
	}

	if (isset($_POST['csrf_token'])){

		if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }

		$model->deleteUser($id);

		if (!$inUser->is_admin){
			session_destroy();
			cmsCore::redirect('/logout');
		} else {
			cmsCore::addSessionMessage($_LANG['DELETING_PROFILE_OK'], 'info');
			cmsCore::redirect('/users');
		}

	} else {

		$inPage->setTitle($_LANG['DELETING_PROFILE']);
		$inPage->addPathway($data['nickname'], $inUser->getProfileURL($data['login']));
		$inPage->addPathway($_LANG['DELETING_PROFILE']);

		$confirm['title'] = $_LANG['DELETING_PROFILE'];
		$confirm['text'] = '<p>'.$_LANG['REALLY_DEL_PROFILE'].'</p>';
		$confirm['action'] = '/users/'.$id.'/delprofile.html';
		$confirm['yes_button'] = array();
		$confirm['yes_button']['type'] = 'submit';
		cmsPage::initTemplate('components', 'action_confirm.tpl')->
                assign('confirm', $confirm)->
                display('action_confirm.tpl');

	}

}
//============================================================================//
//============================ Восстановить профиль  =========================//
//============================================================================//
if ($do=='restoreprofile'){

    if (!$inUser->is_admin) { cmsCore::error404(); }

	$usr = cmsUser::getShortUserData($id);
	if (!$usr) { cmsCore::error404(); }

	$inDB->query("UPDATE cms_users SET is_deleted = 0 WHERE id = '$id'") ;

	cmsCore::redirectBack();

}
//============================================================================//
//============================= Файлы пользователей  =========================//
//============================================================================//
if ($do=='files'){

    if (!$model->config['sw_files']) { cmsCore::error404(); }

    $fdo = cmsCore::request('fdo', 'str', '');

    include 'components/users/files.php';

}

//============================================================================//
//================================  Инвайты  =================================//
//============================================================================//
if ($do=='invites'){

    $reg_cfg = $inCore->loadComponentConfig('registration');
    if ($reg_cfg['reg_type'] != 'invite') { cmsCore::error404(); }

    $invites_count = $model->getUserInvitesCount($inUser->id);
    if (!$invites_count) { cmsCore::error404(); }
	
    if (!cmsCore::inRequest('send_invite')){

        $inPage->addPathway($inUser->nickname, cmsUser::getProfileURL($inUser->login));
        $inPage->addPathway($_LANG['MY_INVITES']);

        cmsPage::initTemplate('components', 'com_users_invites')->
                assign('invites_count', $invites_count)->
                assign('login', $inUser->login)->				
                display('com_users_invites.tpl');

        return;

    }

    if (cmsCore::inRequest('send_invite')){
	
		if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }
	
        $invite_email = cmsCore::request('invite_email', 'email', '');
        if (!$invite_email) { cmsCore::redirectBack(); }

        if ($model->sendInvite($inUser->id, $invite_email)){

            cmsCore::addSessionMessage(sprintf($_LANG['INVITE_SENDED'], $invite_email), 'success');

        } else {

            cmsCore::addSessionMessage($_LANG['INVITE_ERROR'], 'error');

        }

        cmsCore::redirect(cmsUser::getProfileURL($inUser->login));

    }

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
?>
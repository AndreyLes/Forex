<?
	define('PATH', $_SERVER['DOCUMENT_ROOT']);
	include(PATH.'/core/ajax/ajax_core.php');

    cmsCore::loadModel('users');
    $model   = new cms_model_users();
	$id = cmsCore::request('id', 'str');	
	
	$model->DelApplicationService($id);
	
	$inDB= cmsDatabase::getInstance();
	
	$user_id = $inDB->get_field("cms_user_exchange_service","id='$id'","user_id");
	$inf_mail = array();
	$inf_mail['email'] = $inDB->get_field("cms_users","id='$user_id'","email");
	$inf_mail['message'] = "<p>Заявка <b>№ $id</b> на обмен средств через сервис удалена по истечению времени.</p>";
	$href2 = "<p><a href='http://".$_SERVER['HTTP_HOST']."'>Перейти в личный кабинет</a></p>";
	$email = $model->sendEmail3($inf_mail, $href2);
	
	$inf_mail2 = array();
	$config = cmsConfig::getDefaultConfig(); 
	$inf_mail2['email'] =  $config['sitemail'];
	$inf_mail2['message'] = "<p>Заявка <b>№ $id</b> на обмен средств через сервис удалена по истечению времени.</p>";
	$href = "<p><a href='http://".$_SERVER['HTTP_HOST']."/users/1/administration.html#service'>Посмотреть все заявки</a></p>";
	$email2 = $model->sendEmail3($inf_mail2, $href);
	$model->EventHistory(1, "Удаление заявки №: $id с обмена стандарт по истечению времени.");
	
	echo $id;
	

?>
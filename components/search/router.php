<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

    function routes_search(){

        $routes[] = array(
                            '_uri'  => '/^search\/tag\/(.+)\/page([0-9]+).html$/i',
                            'do'    => 'tag',
                            1       => 'query',
                            2       => 'page'
                         );

        $routes[] = array(
                            '_uri'  => '/^search\/tag\/(.+)$/i',
                            'do'    => 'tag',
                            1       => 'query'
                         );

        return $routes;

    }

?>

<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/
if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

function registration(){

    $inCore = cmsCore::getInstance();
    $inPage = cmsPage::getInstance();
    $inDB   = cmsDatabase::getInstance();
    $inUser = cmsUser::getInstance();
    $inConf = cmsConfig::getInstance();

	
	
	
    $model = new cms_model_registration();
	
		
    cmsCore::loadModel('users');
    $users_model = new cms_model_users();

    global $_LANG;

	$do = $inCore->do;

//============================================================================//
if ($do=='sendremind'){

    $inPage->setTitle($_LANG['REMINDER_PASS']);
    $inPage->addPathway($_LANG['REMINDER_PASS']);
	
    if (!cmsCore::inRequest('goremind')){

        cmsPage::initTemplate('components', 'com_registration_sendremind')->
                display('com_registration_sendremind.tpl');

    } else {

        if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }

        $email = cmsCore::request('email', 'email', '');
        if(!$email) { cmsCore::addSessionMessage($_LANG['ERR_EMAIL'], 'error'); cmsCore::redirectBack(); }

        $usr = cmsUser::getShortUserData($email);
        if(!$usr) {
            cmsCore::addSessionMessage($_LANG['ADRESS'].' "'.$email.'" '.$_LANG['NOT_IN_OUR_BASE'], 'error');
            cmsCore::redirectBack();
        }

        $usercode = md5($usr['id'] . '-' . $usr['login'] . '-' . $usr['password'] . '-' . $usr['logdate'].PATH);
        $newpass_link = HOST.'/registration/remind/' . $usercode;

        $mail_message = $_LANG['HELLO'].', ' . $usr['nickname'] . '!'. "\n\n";
        $mail_message .= $_LANG['REMINDER_TEXT'].' "'.$inConf->sitename.'".' . "\n\n";
        $mail_message .= $_LANG['YOUR_LOGIN'].': ' .$usr['login']. "\n\n";
        $mail_message .= $_LANG['NEW_PASS_LINK'].":\n" .$newpass_link . "\n\n";
        $mail_message .= $_LANG['LINK_EXPIRES']. "\n\n";
        $mail_message .= $_LANG['SIGNATURE'].', '. $inConf->sitename . ' ('.HOST.').' . "\n";
        $mail_message .= date('d-m-Y (H:i)');

        $inCore->mailText($email, $inConf->sitename.' - '.$_LANG['REMINDER_PASS'], $mail_message);

        cmsCore::addSessionMessage($_LANG['NEW_PAS_SENDED'], 'info');
		
		cmsCore::redirect('/users');
		

    }

}

//============================================================================//
if ($do=='remind'){

    $usercode = cmsCore::request('code', 'str', '');
    //проверяем формат кода
    if (!preg_match('/^([a-z0-9]{32})$/ui', $usercode)) { cmsCore::error404(); }

    //получаем пользователя
    $user = $inDB->get_fields('cms_users',
            "MD5(CONCAT(id,'-',login,'-',password,'-',logdate,'".$inDB->escape_string(PATH)."')) = '{$usercode}'", '*');
    if (!$user){ cmsCore::error404(); }

    if (cmsCore::inRequest('submit')){

        if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }

        $errors = false;

        $pass  = cmsCore::request('pass', 'str', '');
        $pass2 = cmsCore::request('pass2', 'str', '');

        if(!$pass) { cmsCore::addSessionMessage($_LANG['TYPE_PASS'], 'error'); $errors = true; }
        if($pass && !$pass2) { cmsCore::addSessionMessage($_LANG['TYPE_PASS_TWICE'], 'error'); $errors = true; }
        if($pass && $pass2 && mb_strlen($pass)<6) { cmsCore::addSessionMessage($_LANG['PASS_SHORT'], 'error'); $errors = true; }
        if($pass && $pass2 && $pass != $pass2) { cmsCore::addSessionMessage($_LANG['WRONG_PASS'], 'error'); $errors = true; }

        if ($errors){ cmsCore::redirectBack(); }

        $md5_pass = md5($pass);

        $inDB->query("UPDATE cms_users SET password = '{$md5_pass}', logdate = NOW() WHERE id = '{$user['id']}'");

        cmsCore::addSessionMessage($_LANG['CHANGE_PASS_COMPLETED'], 'info');

        $back_url = $inUser->signInUser($user['login'], $pass, true);

		cmsCore::redirect($back_url);

    }

    $inPage->setTitle($_LANG['RECOVER_PASS']);
    $inPage->addPathway($_LANG['RECOVER_PASS']);

    cmsPage::initTemplate('components', 'com_registration_remind')->
            assign('cfg', $model->config)->
            assign('user', $user)->
            display('com_registration_remind.tpl');

}

//============================================================================//
if ($do=='register'){
	
    if(!cmsUser::checkCsrfToken()) { cmsCore::error404(); }

    // регистрация закрыта
    if(!$model->config['is_on']){
        cmsCore::error404();
    }
    // регистрация по инвайтам
    if ($model->config['reg_type']=='invite'){
        if (!$users_model->checkInvite(cmsUser::sessionGet('invite_code'))) {
            cmsCore::error404();
        }
    }

    $errors = false;

    // получаем данные
    $item['login'] = cmsCore::request('login', 'str', '');
    $item['email'] = cmsCore::request('email', 'email');
    $item['icq']   = cmsCore::request('icq', 'str', '');
    $item['city']  = cmsCore::request('city', 'str', '');
    $item['nickname']  = cmsCore::request('nickname', 'str', '');
    $item['realname1'] = cmsCore::request('realname1', 'str', '');
    $item['realname2'] = cmsCore::request('realname2', 'str', ''); 

    $item['check1'] = cmsCore::request('check1', 'str', '');
    $item['check2'] = cmsCore::request('check2', 'str', '');
    $item['check3'] = cmsCore::request('check3', 'str', '');
    $pass  = cmsCore::request('pass', 'str', '');
    $pass2 = cmsCore::request('pass2', 'str', '');

    // проверяем логин
    if(mb_strlen($item['login'])<2 ||
            mb_strlen($item['login'])>15 ||
            is_numeric($item['login']) ||
            !preg_match("/^([a-zA-Z0-9])+$/ui", $item['login'])) {

        cmsCore::addSessionMessage($_LANG['ERR_LOGIN'], 'error'); $errors = true;

    }
	if(!$item['check1']){
	
			cmsCore::addSessionMessage($_LANG['CHECKS'], 'error'); $errors = true;
	
	}
    // проверяем пароль
    if(!$pass) { cmsCore::addSessionMessage($_LANG['TYPE_PASS'], 'error'); $errors = true; }
    if($pass && !$pass2) { cmsCore::addSessionMessage($_LANG['TYPE_PASS_TWICE'], 'error'); $errors = true; }
    if($pass && $pass2 && mb_strlen($pass)<6) { cmsCore::addSessionMessage($_LANG['PASS_SHORT'], 'error'); $errors = true; }
    if($pass && $pass2 && $pass != $pass2) { cmsCore::addSessionMessage($_LANG['WRONG_PASS'], 'error'); $errors = true; }

    // Проверяем nickname или имя и фамилию
    if($model->config['name_mode']=='nickname'){
        if(!$item['nickname']) { cmsCore::addSessionMessage($_LANG['TYPE_NICKNAME'], 'error'); $errors = true; }
    } else {
        if(!$item['realname1']) { cmsCore::addSessionMessage($_LANG['TYPE_NAME'], 'error'); $errors = true; }
        if(!$item['realname2']) { cmsCore::addSessionMessage($_LANG['TYPE_SONAME'], 'error'); $errors = true; }
        $item['nickname'] = trim($item['realname1']) . ' ' . trim($item['realname2']);
		
		$i_1=preg_match('/^[A-Za-z]/', $item['realname1']);
		$i_2=preg_match('/^[A-Za-z]/', $item['realname2']);
		if($i_1 != 0){
			cmsCore::addSessionMessage($_LANG['BAG_NAME'], 'error'); $errors = true;
		}
		if($i_2 != 0){
			cmsCore::addSessionMessage($_LANG['BAG_FIRST_NAME'], 'error'); $errors = true;
		}
	}
	
	
	
	
/*     if (mb_strlen($item['nickname'])<2) { cmsCore::addSessionMessage($_LANG['SHORT_NICKNAME'], 'error'); $errors = true; }
	 if (mb_strlen($item['nickname1'])<2) { cmsCore::addSessionMessage($_LANG['SHORT_NICKNAME'], 'error'); $errors = true; }
    if($model->getBadNickname($item['nickname'])){
        cmsCore::addSessionMessage($_LANG['ERR_NICK_EXISTS'], 'error'); $errors = true;
    }
if($model->getBadNickname($item['nickname1'])){
        cmsCore::addSessionMessage($_LANG['ERR_NICK_EXISTS'], 'error'); $errors = true;
    } */
    // Проверяем email
    if(!$item['email']) { cmsCore::addSessionMessage($_LANG['ERR_EMAIL'], 'error'); $errors = true; }

    // День рождения
    list($item['bday'], $item['bmonth'], $item['byear']) = array_values(cmsCore::request('birthdate', 'array_int', array()));
    $item['birthdate'] = sprintf('%04d-%02d-%02d', $item['byear'], $item['bmonth'], $item['bday']);

    // получаем данные конструктора форм
    $item['formsdata'] = '';
    if(isset($users_model->config['privforms'])){
        if (is_array($users_model->config['privforms'])){
            foreach($users_model->config['privforms'] as $form_id){
                $form_input  = cmsForm::getFieldsInputValues($form_id);
                $item['formsdata'] .= $inDB->escape_string(cmsCore::arrayToYaml($form_input['values']));
                // Проверяем значения формы
                foreach ($form_input['errors'] as $field_error) {
                    if($field_error){ cmsCore::addSessionMessage($field_error, 'error'); $errors = true; }
                }
            }
        }
    }

    // Проверяем каптчу
  if(!cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) { cmsCore::addSessionMessage($_LANG['ERR_CAPTCHA'], 'error'); $errors = true; } 

    // проверяем есть ли такой пользователь
    $user_exist = $inDB->get_fields('cms_users', "(login LIKE '{$item['login']}' OR email LIKE '{$item['email']}') AND is_deleted = 0", 'id, login, email');
    if($user_exist){
        if($user_exist['login'] == $item['login']){
            cmsCore::addSessionMessage($_LANG['LOGIN'].' "'.$item['login'].'" '.$_LANG['IS_BUSY'], 'error'); $errors = true;
        } else {
            cmsCore::addSessionMessage($_LANG['EMAIL_IS_BUSY'], 'error'); $errors = true;
        }
    }

    // В случае ошибок, возвращаемся в форму
    if($errors){
        cmsUser::sessionPut('item', $item);
		cmsCore::redirect('/registration');
    }

    //////////////////////////////////////////////
    //////////// РЕГИСТРАЦИЯ /////////////////////
    //////////////////////////////////////////////

    $item['is_locked'] = $model->config['act'];
    $item['password']  = md5($pass);
    $item['orig_password'] = $pass;
    $item['group_id']  = $model->config['default_gid'];
    $item['regdate']   = date('Y-m-d H:i:s');
    $item['logdate']   = date('Y-m-d H:i:s');

    if (cmsUser::sessionGet('invite_code')){

        $invite_code = cmsUser::sessionGet('invite_code');
        $item['invited_by'] = (int)$users_model->getInviteOwner($invite_code);

        if ($item['invited_by']){ $users_model->closeInvite($invite_code); }

        cmsUser::sessionDel('invite_code');

    } else {
        $item['invited_by'] = 0;
    }

    $item = cmsCore::callEvent('USER_BEFORE_REGISTER', $item);

    $item['id'] = $item['user_id'] = $inDB->insert('cms_users', $item);
    if(!$item['id']){ cmsCore::error404(); }

    $inDB->insert('cms_user_profiles', $item);
	//добавляємо рахунки для користувача && створюємо пусті платіжні системи для нового користувача
	
	$payment_system = $inDB->get_table("cms_user_type_payment", "on_off='1' AND status='0'");
	foreach($payment_system as $key=>$infa){
		$inDB->query("INSERT cms_user_score (user_id, type_payment_id, score_num ,score_val, score_in, score_out, num_payment, status) VALUES ('".$item['id']."','".$infa['id']."','0','0','0','0','0','1')");
	}
	// записываем цветовую схему для текущего юзера
	if(isset($_COOKIE['style'])){
		$style = array();
		$style[0]['title'] = "dark";
		$style[0]['color'] = "0";
		$style[1]['title'] = "blue";
		$style[1]['color'] = "1";
		$style[2]['title'] = 'green';
		$style[2]['color'] = "2";
		
		foreach($style as $styles){
			if($styles['title'] == $_COOKIE['style']){
				$inDB->query("UPDATE cms_users SET color_system='".$styles['color']."' WHERE id='".$item['id']."'");
			}
		}
	}
	else{
		$inDB->query("UPDATE cms_users SET color_system='0' WHERE id='$id'");
	}
	
	$inDB->addUserWallet($item['login']);
    cmsCore::callEvent('USER_REGISTER', $item);

    if ($item['is_locked']){

        $model->sendActivationNotice($pass, $item['id']);
        cmsPage::includeTemplateFile('special/regactivate.php');
        cmsCore::halt();

    } else {

        cmsActions::log('add_user', array(
            'object' => '',
            'user_id' => $item['id'],
            'object_url' => '',
            'object_id' => $item['id'],
            'target' => '',
            'target_url' => '',
            'target_id' => 0,
            'description' => ''
        ));

        if ($model->config['send_greetmsg']){ $model->sendGreetsMessage($item['id']); }
        $model->sendRegistrationNotice($pass, $item['id']);

        $back_url = $inUser->signInUser($item['login'], $pass, true);

//        cmsCore::redirect($back_url);

    }

}

//============================================================================//
if ($do=='view'){

    $pagetitle = $inCore->getComponentTitle();

    $inPage->setTitle($pagetitle);
    $inPage->addPathway($pagetitle);
    $inPage->addHeadJsLang(array('WRONG_PASS'));

    // Если пользователь авторизован, то не показываем форму регистрации, редирект в профиль.
    if ($inUser->id && !$inUser->is_admin) {
        if ($inCore->menuId() == 1) { return; } else {  cmsCore::redirect(cmsUser::getProfileURL($inUser->login)); }
    }

$test = $_SESSION['icms']['ref'];



    $userget = $users_model->getUser($test);

    $correct_invite = (cmsUser::sessionGet('invite_code') ? true : false);

    if ($model->config['reg_type']=='invite' && cmsCore::inRequest('invite_code')){

        $invite_code    = cmsCore::request('invite_code', 'str', '');
        $correct_invite = $users_model->checkInvite($invite_code);

        if ($correct_invite) {
            cmsUser::sessionPut('invite_code', $invite_code);
        } else {
            cmsCore::addSessionMessage($_LANG['INCORRECT_INVITE'], 'error');
        }

    }

    $item = cmsUser::sessionGet('item');
    if($item){ cmsUser::sessionDel('item'); }

    $private_forms = array();
    if(isset($users_model->config['privforms'])){
        if (is_array($users_model->config['privforms'])){
            foreach($users_model->config['privforms'] as $form_id){
                $private_forms = array_merge($private_forms, cmsForm::getFieldsHtml($form_id, array(), true));
            }
        }
    }
	if($inUser->id){
		$cooke_param = $inDB->get_field('cms_users','id="'.$inUser->id.'"','color_system');
		if(isset($cooke_param) && $cooke_param == '0'){
			$cooke_param = 'dark';
		}
		elseif($cooke_param && $cooke_param == '1'){
			$cooke_param = 'blue';
		}
		elseif($cooke_param && $cooke_param == '2'){
			$cooke_param = 'green';
		}
	}
	else{
		$cooke_param = 'dark';
	}
	$user = cmsUser::getShortUserData('1');
	
    cmsPage::initTemplate('components', 'com_registration')->
            assign('cfg', $model->config)->
            assign('item', $item)->
			assign('cooke_param', $cooke_param)->
            assign('pagetitle', $pagetitle)->
            assign('correct_invite', $correct_invite)->
            assign('private_forms', $private_forms)->
            assign('user', $userget['nickname'])->
            assign('online', $userget['flogdate'])->
			
            display('com_registration.tpl');

}

//============================================================================//
if ($do=='activate'){
	
    $code = cmsCore::request('code', 'str', '');
    if (!$code) { cmsCore::error404(); }

    $user_id = $inDB->get_field('cms_users_activate', "code = '$code'", 'user_id');
    if (!$user_id){ cmsCore::error404(); }
	$dfdf = $model->sendActivationAfter($user_id);
    $inDB->query("UPDATE cms_users SET is_locked = 0 WHERE id = '$user_id'");
    $inDB->query("DELETE FROM cms_users_activate WHERE code = '$code'");

	
	//var_dump($dfdf);
	
    cmsCore::callEvent('USER_ACTIVATED', $user_id);

    if ($model->config['send_greetmsg']){ $model->sendGreetsMessage($user_id); }

    // Регистрируем событие
    cmsActions::log('add_user', array(
            'object' => '',
            'user_id' => $user_id,
            'object_url' => '',
            'object_id' => $user_id,
            'target' => '',
            'target_url' => '',
            'target_id' => 0,
            'description' => ''
    ));

    //cmsCore::addSessionMessage($_LANG['ACTIVATION_COMPLETE'], 'info');
	cmsCore::redirect('/login#ACTIVATION_COMPLETE');
    cmsUser::goToLogin();

}

//============================================================================//

if ($do=='auth'){

    //====================//
    //==  разлогивание  ==//
    if(cmsCore::inRequest('logout')) {

        $inUser->logout();
        cmsCore::redirect('/');

    }

    //====================//
    //==  авторизация  ==//
    if( !cmsCore::inRequest('logout') ) {

        // флаг неуспешных авторизаций
        $anti_brute_force = cmsUser::sessionGet('anti_brute_force');

        $login = cmsCore::request('login', 'str', '');
        $passw = cmsCore::request('pass', 'str', '');
		$option = cmsCore::request("opt","str");
		$go_logins = cmsCore::request('go_logins', 'str', '');
        $remember_pass = cmsCore::inRequest('remember');
		$redirect_url = cmsCore::request('url_redirect','str');
        // если нет логина или пароля, показываем форму входа
        if (!$login || !$passw){

            if($inUser->id && !$inUser->is_admin) { cmsCore::redirect('/'); }

            $inPage->setTitle($_LANG['SITE_LOGIN']);
            $inPage->addPathway($_LANG['SITE_LOGIN']);

            cmsPage::initTemplate('components', 'com_registration_login')->
                    assign('cfg', $model->config)->
                    assign('anti_brute_force', $anti_brute_force)->
                    assign('is_sess_back', cmsUser::sessionGet('auth_back_url'))->
                    display('com_registration_login.tpl');

            if(!mb_strstr(cmsCore::getBackURL(), 'login')){
                cmsUser::sessionPut('auth_back_url', cmsCore::getBackURL());
            }

            return;

        }
		
    	if(!mb_strstr(@$_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST'])) { cmsCore::error404(); }

        // Проверяем каптчу
        if($anti_brute_force && !cmsCore::checkCaptchaCode(cmsCore::request('code', 'str'))) {
            cmsCore::addSessionMessage($_LANG['ERR_CAPTCHA'], 'error');
            cmsCore::redirect('/login');
        }
		
		cmsUser::sessionDel('anti_brute_force');
		if(isset($_SESSION['bak_urlka'])){
			$redirect_url = $_SESSION['bak_urlka'];
		}
		if(isset($redirect_url)){
			$back_url = $inUser->signInUser($login, $passw, $remember_pass,0, $redirect_url);
		}
		else{
			$back_url = $inUser->signInUser($login, $passw, $remember_pass);
		}
		
		if($back_url && $bak_urlka != '/auth/error.html'){
			$id = $inDB->get_field('cms_users','login=\''.$login.'\'','id');
			$model2 = new cms_model_users();
			$usr = $model2->getUser($id);
			$inf_pass = $inDB->get_fields('cms_users','login=\''.$login.'\'', 'password, date_password, status_password, send_letter');
			$inf_time_pass = $inDB->get_field('cms_user_config',"name='reload_pass' AND status='1'", 'value');
			
			if($inf_pass['status_password'] != '1' && $inf_time_pass){
				if((strtotime(date('Y-m-d')) - strtotime($inf_pass['date_password'])) >= ($inf_time_pass*86400+86400) && $inf_pass['send_letter'] < 3){
					cmsUser::sendMessage(USER_MASSMAIL, $id, "<p>Время действия старого пароля истекло.</p><p>Перейдите по <a syule='color: #0088cc;' href='/users/".$id."/editprofile.html#change_password'>ссылке</a> для смены пароля.</p>");
					
					$inf_mail = array();
					$inf_mail['email'] = $usr['email'];
					$inf_mail['message'] = "<p>Время действия старого пароля истекло.</p><p>Перейдите по <a href='http://".$_SERVER['HTTP_HOST']."?redirect_url=/registration/change_login'>ссылке</a> для смены пароля.</p>";
						
					$email = $model2->sendEmail3($inf_mail,'');
					$inf_pass['send_letter']++;
					$inDB->query("UPDATE cms_users SET send_letter='" . $inf_pass['send_letter'] . "' WHERE login='$login'");
					cmsCore::redirect($back_url);
				}
				cmsCore::redirect('registration/change_login');
			}
			elseif($inf_time_pass){
				if(!$inf_pass['date_password']){
					$inDB->query("UPDATE cms_users SET date_password='" . date('Y-m-d') . "' WHERE login='$login'");
					cmsCore::redirect($back_url);
				}
				if((strtotime(date('Y-m-d')) - strtotime($inf_pass['date_password'])) > $inf_time_pass*86400 ){
					$inDB->query("UPDATE cms_users SET status_password='0' WHERE login='$login'");
					cmsCore::redirect('registration/change_login');
				}
			}
		}
		
		if($option == 'about_us'){
			cmsCore::redirect('/info/o-nas.html?id='.$go_logins.'#kab_blockk_s');
		}
		else{
			cmsCore::redirect($back_url);
		}
       

    }

}
//--------------------------------------------------------
if ($do=='change_login'){

    //====================//
    //==  разлогивание  ==//
    if(cmsCore::inRequest('logout')) {

        $inUser->logout();
        cmsCore::redirect('/');

    }

    //====================//
    //==  авторизация  ==//
    if( !cmsCore::inRequest('logout') ) {
		$model = new cms_model_users();
		$usr = $model->getUser($inUser->id);
		$id = $inUser->id;
        $oldpass  = cmsCore::request('oldpass', 'str');
		$newpass  = cmsCore::request('newpass', 'str');
		$newpass2 = cmsCore::request('newpass2', 'str');
		
        // если нет логина или пароля, показываем форму входа
        if (!$oldpass || !$newpass || !$newpass2){

            if($inUser->id && !$inUser->is_admin) { cmsCore::redirect('/'); }

            $inPage->setTitle($_LANG['SITE_LOGIN']);
            $inPage->addPathway($_LANG['SITE_LOGIN']);

            cmsPage::initTemplate('components', 'com_registration_change_login')->
                    assign('cfg', $model->config)->
                    assign('is_sess_back', cmsUser::sessionGet('auth_back_url'))->
                    display('com_registration_change_login.tpl');

            if(!mb_strstr(cmsCore::getBackURL(), 'login')){
                cmsUser::sessionPut('auth_back_url', cmsCore::getBackURL());
            }

            return;

        }
		
    	if ($inUser->password != md5($oldpass)) {
			//cmsCore::addSessionMessage($_LANG['OLD_PASS_WRONG'], 'error'); $errors = true;
			cmsCore::redirect('/registration/change_login#error_old_pass');
			}
		if ($newpass != $newpass2) { 
			cmsCore::redirect('/registration/change_login#WRONG_PASS');
		//cmsCore::addSessionMessage($_LANG['WRONG_PASS'], 'error'); $errors = true; 
		}
		if($oldpass && $newpass && $newpass2 && mb_strlen($newpass )<6) {
			cmsCore::redirect('registration/change_login#PASS_SHORT');
			//cmsCore::addSessionMessage($_LANG['PASS_SHORT'], 'error'); $errors = true; 
		}
		if($inUser->password == md5($newpass))
			cmsCore::redirect('/registration/change_login#error_new_old_pass');
		
		if($errors) { cmsCore::redirectBack(); }

        cmsCore::callEvent('UPDATE_USER_PASSWORD', array('user_id'=>$usr['id'], 'oldpass'=>$oldpass, 'newpass'=>$newpass));
		
		$sql = "UPDATE cms_users SET password='".md5($newpass)."', status_password='1', date_password='". date('Y-m-d') ."', send_letter='0' WHERE id = '$id' AND password='".md5($oldpass)."'";
		$inDB->query($sql);
		//cmsCore::addSessionMessage($_LANG['PASS_CHANGED'], 'info');
		//cmsCore::redirect(cmsUser::getProfileURL($inUser->login));
		
		$model->EventHistory($inUser->id, "Изменение пароля");
		
		cmsUser::sendMessage(USER_MASSMAIL, $id, "<p>Спасибо, что изменили пароль.</p><p>Ваш логин: <b>".$usr['login']."</b>;<br>Ваш новый пароль:<b>$newpass</b></p>");
		
		$inf_mail = array();
		$inf_mail['email'] = $usr['email'];
		$inf_mail['message'] = "<p>Спасибо, что изменили пароль.</p><p>Ваш логин: <b>".$usr['login']."</b>;\nВаш новый пароль:<b>$newpass</b>\n<a href='http://".$_SERVER['HTTP_HOST']."?redirect_url=/users/".$usr['login']."'>Перейти на сайт</a></p>";
			
		$email = $model->sendEmail3($inf_mail,'');
		
		cmsCore::redirect('/users/'.$usr['login'].'#change_pass');
       

    }

}


//============================================================================//
if ($do=='autherror'){

    cmsUser::sessionPut('anti_brute_force', 1);
    cmsPage::includeTemplateFile('special/autherror.php');
    cmsCore::halt();

}

//============================================================================//

}
?>

<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

    function routes_actions(){

        $routes[] = array(
                            '_uri'  => '/^actions\/delete\/([0-9]+)$/i',
                            'do'    => 'delete',
                            1       => 'id'
                         );

        $routes[] = array(
                            '_uri'  => '/^actions\/my_friends$/i',
                            'do'    => 'view_user_feed'
                         );

        $routes[] = array(
                            '_uri'  => '/^actions\/page\-([0-9]+)$/i',
                            'do'    => 'view',
                            1       => 'page'
                         );

        return $routes;

    }

?>

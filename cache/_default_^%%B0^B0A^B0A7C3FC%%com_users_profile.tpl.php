<?php /* Smarty version 2.6.28, created on 2016-02-10 17:49:30
         compiled from com_users_profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_profile.tpl', 1, false),array('function', 'add_css', 'com_users_profile.tpl', 104, false),array('function', 'captcha2', 'com_users_profile.tpl', 1740, false),array('modifier', 'count', 'com_users_profile.tpl', 1035, false),)), $this); ?>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/datepicker/jquery-1.10.2.js'), $this);?>

<?php echo '
	<script>
		/*$(document).ready(function(){
			url = "http://fx-trend.com/pamm_indexes/";
			//text = $(url).find(".csn_message_list li").text();
			var mypage = $.ajax(\'http://fx-trend.com/pamm_indexes/\');
			mypage.done(function (data) {
				$(data).find(\'.csn_message_list li\').each(function(){
					res+=$(this).text()+\'<br/>\';
				})
				(\'#underline\').html(res);
			});
			
			//alert(url_text);time_off
		});*/
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == \'#save_change\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Изменения сохранены.");
				document.location.hash = "";
			}
			if(hash == \'#change_pass\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Пароль изменен.");
				document.location.hash = "";
			}
			if(hash == \'#critical_error\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Вы пытаетесь обмануть систему перевода средств. При повторном обмане Ваш аккаунт будет заблокирован.");
				document.location.hash = "";
			}
			if(hash == \'#obmen_application\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Обмен перемещен в истории.");
				document.location.hash = "";
			}
			if(hash == \'#time_off\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Время подтверждения истекло. Заявка перемещена на биржу обмена.");
				document.location.hash = "";
			}
			if(hash == \'#waiting_for_confirmation\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Ожидание подтверждения встречной заявки.");
				document.location.hash = "";
			}
			if(hash == \'#application_service\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Запрос на обмен отправлен.");
				document.location.hash = "";
			}
			if(hash == \'#dell_application\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Заявка удалена.");
				document.location.hash = "";
			}
			if(hash == \'#obmet_success\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("обмен успешно произведен.");
				document.location.hash = "";
			}
			if(hash == \'#otmena_obmen\'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Обмен отменен.");
				document.location.hash = "";
			}
		/*удаление хеша при клике*/
		$(\'body\').on("click", \'.modal-backdrop\', function(){
			document.location.hash = "";
		});
		$(\'.close\').click(function(){document.location.hash = "";});
				
		});
		
	</script>
'; ?>


<div id="modal_success_all" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success_all" role="button" data-toggle="modal" id='modal_success_all_btn'></a>

<div id="modal1" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 400px;
	
border: 5px solid #ccc;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='margin-top: 10px;
display: inline-block;'>
<span style='display: inline-block;margin-top: 140px;margin-left: 26px;font-size: 20px;'>Данный раздел находиться в стадии разработки.</span>
	</div>
</div>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/tabs/jquery.ui.min.js'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => "components/users/js/profile.js"), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/jquery/tabs/tabs.css'), $this);?>

<h4 style='margin:0;padding-left:40px; margin-bottom: 20px;'>
	Мои счета
	<div class="td_buttonu" style="float: right;"><a href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/service.html">Обмен через сервис</a></div>
</h4>
<input type="hidden" name="myCash" value="<?php echo $this->_tpl_vars['userCash']; ?>
" />
<div id='underline'>
	<span>
	
	</span>
</div>
<div id='section-div'>
	<section>
		<img src='/images/alarm.png'/> <span>- все операции завершены </span> 
	</section>
	<section>
		<img src='/images/alarm-red.png'/> <span>- все операции завершены </span> 
	</section>
</div>
<!-- Лицевой счет -->

<div class='kab-block' style="position:relative;">
	<header>
		<span style="float:left;"><?php echo $this->_tpl_vars['expenses']['main']['expense_title']; ?>
</span>
		<div  class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<div class='first_table'>
		<div id="first_table_div">
		<span>
			№<?php echo $this->_tpl_vars['expenses']['main']['id']; ?>

		</span>
		</div>
		<div>
		<img src='/images/alarm.png'/><a href=''><span style='color:#030303'>История</span></a>
		</div>
	</div>
	<div class='second_table'>
		<table class='lych_table' style=''>
			<tr>
				<td><span style='float:left'>Валюта:</span>
					<span class='dotted'></span>
				</td>
				<td>
					<?php echo $this->_tpl_vars['expenses']['main']['currency']; ?>

				</td>
			</tr>
				<tr>
				<td>
					Общий бонус:
				</td>
				<td>
				<?php echo $this->_tpl_vars['expenses']['main']['expense_money']; ?>

				</td>
			</tr>
				<tr>
				<td><!--span style='float:left'>
					Бонусы:</span><span class='dotted'></span-->
					
				</td>
				<td>
				
				</td>
			</tr>
		</table>
	
		<ul class='lyc_schet'>
			<li><a href='#modal1' role="button" data-toggle="modal">Операции со счетами</a></li>
			<li><a href='#modal1' role="button" data-toggle="modal">Вывести средства</a></li>
			<!--li><a href=''>Управление бонусами</a></li-->
		</ul>
	
		<a id="button_payment" href='/users/<?php echo $this->_tpl_vars['ids']; ?>
/investglav.html' role="button" data-toggle="modal" class='button_box'><span>Пополнить СЧЕТ</span></a>
		
		<div  class="payment_sustem">
			<ul style=" list-style: none; float: left;">
				<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['system']):
?>
				<?php if ($this->_tpl_vars['inf_moneys'][$this->_tpl_vars['system']['id']]['score_num'] != ''): ?>
				<li >
					<a title="<?php echo $this->_tpl_vars['system']['name']; ?>
" href=""><img src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></a>
				</li>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</ul>
			<button data-inf="0" style="display:none;float: left; width:90px; margin-top: 70px; margin-left: 50px;" id="payment_more">Показать</button>
		</div>
		
	
		
	</div>
</div>
<?php echo '
<script>
num_item = 0;
show_item = 0;
$(\'.payment_sustem li\').each(function(){
	if(num_item >= 3){ show_item = 1; }
	num_item++;
	//if()
	
	
	
});
if(show_item == 1){
	$(\'.payment_sustem #payment_more\').css(\'display\', \'block\');
	$(\'.payment_sustem #payment_more\').click(function(){
		data_inf = $(this).attr(\'data-inf\')
		height_animation = (num_item - 3)*33;
		if(data_inf == \'0\'){
			$(\'#button_payment\').animate({\'margin-top\': "+="+height_animation}, 1000);
			$(\'.payment_sustem #payment_more\').animate({\'margin-top\': "+="+height_animation}, 1000);
			$(\'.second_table\').animate({height: "+="+height_animation}, 1000);
			$(\'.first_table\').animate({height: "+="+height_animation}, 1000);
			$(\'.first_table #first_table_div\').animate({height: "+="+height_animation}, 1000);
			$(\'.payment_sustem li\').css(\'display\', \'block\');
			$(this).attr(\'data-inf\', \'1\');
			$(this).text(\'Скрыть\');
		}
		if(data_inf == \'1\'){
			$(\'#button_payment\').animate({\'margin-top\': "-="+height_animation}, 1000);
			$(\'.payment_sustem #payment_more\').animate({\'margin-top\': "-="+height_animation}, 1000);
			$(\'.second_table\').animate({height: "-="+height_animation}, 1000);
			$(\'.first_table\').animate({height: "-="+height_animation}, 1000);
			$(\'.first_table #first_table_div\').animate({height: "-="+height_animation}, 1000);
			$(this).attr(\'data-inf\', \'0\');
			$(this).text(\'Показать\');
		}
	});
}


</script>
'; ?>


<!-- Инвестиционный счет -->



<div class='kab-block' style="position:relative;">
	<header>
		<span style="float:left;"><?php echo $this->_tpl_vars['inf_gf']['title']; ?>
</span>
		<div  class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<div class='first'>
		<div>
		<span>
			№4
		</span>
		</div>
		<div>
		<img src='/images/alarm.png'/><a href=''><span style='color:#030303'>История</span></a>
		</div>
	</div>
	<div class='second'>
	<table class='lych_table' style=''>
		<!--tr>
			<td><span style='float:left'>Инвестировано:</span>
				<span class='dotted1'></span>
			</td>
			<td>
				<?php echo $this->_tpl_vars['result']['currency']; ?>

			</td>
		</tr-->
			<tr>
		<td><span style='float:left'>Выведено:</span>
				<span class='dotted2'></span>
			</td>
			<td>
				<?php echo $this->_tpl_vars['inf_gf']['outpay']; ?>

			</td>
		</tr>
			<tr>
			<td><span style='float:left'>
				Начислено:</span><span class='dotted2'></span>
				
			</td>
			<td>
				<?php echo $this->_tpl_vars['inf_gf']['inpay']; ?>

			</td>
		</tr>
				<tr>
			<td><span style='float:left'>
				Всего на счету:</span><span class='dotted1'></span>
				
			</td>
			<td>
				<?php echo $this->_tpl_vars['inf_gf']['money']; ?>

			</td>
		</tr>
	</table>
	
	<ul class='lyc_schet1' style='margin-left:170px;'>
		<li class='schet' style='list-style:none;'><a href='/users/<?php echo $this->_tpl_vars['ids']; ?>
/outpay.html'>Перевести на лицевой счет</a></li>
	</ul>
		<a href='/users/<?php echo $this->_tpl_vars['ids']; ?>
/invest.html' style='margin-top:99px' class='button_box'><span>Пополнить СЧЕТ</span></a>
	</div>
</div>


<!--запросы на заявки через стандарт-->
<?php if ($this->_tpl_vars['provisional_application']): ?>
<?php echo '
<script>
	$(\'.see_obmen\').click(function(){
		
	});
</script>
'; ?>

<div class='kab-block' style="position:relative;overflow: inherit;">
	<header>
		<span style="float:left;">Заявки c предложениями на обмен стандарт</span>
		<div  class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<table style="background:none !important; widht:100%;" class="pamm-info last-import" >
			<thead style="z-index: 100;">
				<tr>
					<td style="position:relative" >№ заявки</td>
					<td style="position:relative" >информация</td>
					<td>Действие</td>
				</tr>
			</thead>
			<tbody >
			<?php $_from = $this->_tpl_vars['provisional_application']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
				<tr>
					<td style="padding: 10px 0;"><?php echo $this->_tpl_vars['result']['id']; ?>
</td>
					<td data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
">На Вашу заявку есть предложения на обмен</td>
					<td class="td_buttonu">
						<a class="see_obmen" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/see_deals.html?id=<?php echo $this->_tpl_vars['result']['id']; ?>
">Просмотреть предложения</a>
					</td>
					
				</tr>				
			<?php endforeach; endif; unset($_from); ?>
			</tbody>
	</table>
</div>	

<?php endif; ?>
<!-- текущие заявки -->
<?php if ($this->_tpl_vars['applications'] || $this->_tpl_vars['application_gf_gf'] || $this->_tpl_vars['application_gf_garant']): ?>
<?php echo '
<script>
$(document).ready(function(){

	$(\'#grid2 .obr_timer\').each(function(){	
		var time = $(this).attr(\'data-time\');
		$(this).text(time);
	});
	
	$(\'#grid2 .obr_timer2\').each(function(){	
		var time = $(this).attr(\'data-time\');
		$(this).text(time);
	});
	$(\'#grid2 .obr_timer3\').each(function(){	
		var time = $(this).attr(\'data-time\');
		$(this).text(time);
	});
	
	intervalID = setInterval(function(){ tiktak(); }, 60000);
	intervalID2 = setInterval(function(){ tiktak2(); }, 60000);
	intervalID3 = setInterval(function(){ tiktak3(); }, 60000);
	
	
	$(\'.otmena_obmen\').click(function(){
		$(\'#dell_obmen input[name="id_zayavki"]\').val($(this).attr(\'data-id\'));
		//alert($(\'#dell_obmen input[name="id_zayavki"]\').val());
		$(\'#dell_obmen\').submit();
	});
	
});


function tiktak(){
var timer_off = 0;
	$(\'.obr_timer\').not(\'.off\').each(function(){
		timer_off++;
		var time = $(this).text();
		if(time == 1){
			$(this).addClass(\'off\');
application_ajax($(this).attr(\'data-id\'), $(this).parent().attr(\'data-zapusa\'));
		}
		else{
			time--;
			$(this).attr(\'data-time\', time);
			$(this).text(time);
		}
	});
	if(timer_off == 0){
		clearInterval(intervalID);
	}
}
function tiktak2(){
//alert(\'1\');
var timer_off2 = 0;
	$(\'.obr_timer2\').not(\'.off\').each(function(){
		timer_off2++;
		var time = $(this).text();
		if(time == 1){
			$(this).addClass(\'off\');
			application_ajax_2($(this).attr(\'data-id1\'), $(this).attr(\'data-id2\'), $(this).parent().attr(\'data-zapusa1\'), $(this).parent().attr(\'data-zapusa2\'));
		}
		else{
			time--;
			$(this).attr(\'data-time\', time);
			$(this).text(time);
		}
	});
	if(timer_off2 == 0){
		clearInterval(intervalID2);
	}
}
function tiktak3(){
var timer_off2 = 0;
	$(\'.obr_timer3\').not(\'.off\').each(function(){
		timer_off2++;
		var time = $(this).text();
		if(time == 1){
			$(this).addClass(\'off\');
			application_ajax_3($(this).attr(\'data-id1\'), $(this).attr(\'data-id2\'), $(this).parent().attr(\'data-zapusa1\'), $(this).parent().attr(\'data-zapusa2\'));
		}
		else{
			time--;
			$(this).attr(\'data-time\', time);
			$(this).text(time);
		}
	});
	if(timer_off2 == 0){
		clearInterval(intervalID3);
	}
}

function application_ajax(id, id_z){
	jQuery.ajax({
		url:     \'/components/users/ajax/change_application2.php\', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: { id_sust_1: id,
				id_zapusa: id_z},
		success: function(response) {
			$(\'.obr_timer\').each(function(){
				if(response == $(this).attr(\'data-id\')){
					$(this).parent().parent().parent().remove();
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$(\'.obr_timer\').each(function(){
				if(response == $(this).attr(\'data-id\')){
				
				}
			});
		}
	 });
}
function application_ajax_2(id1, id2, id_z1, id_z2){
	jQuery.ajax({
		url:     \'/components/users/ajax/change_application3.php\', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: { id_sust_1: id1,
				id_zapusa1: id_z1,
				id_sust_2: id2,
				id_zapusa2: id_z2},
		success: function(response) {
			$(\'.obr_timer2\').each(function(){
				if(response == $(this).attr(\'data-id1\')){
					$(this).parent().parent().parent().remove();
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$(\'.obr_timer2\').each(function(){
				if(response == $(this).attr(\'data-id1\')){
				
				}
			});
		}
	 });
}
function application_ajax_3(id1, id2, id_z1, id_z2){
	jQuery.ajax({
		url:     \'/components/users/ajax/change_application3.php\', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: { id_sust_1: id1,
				id_zapusa1: id_z1,
				id_sust_2: id2,
				id_zapusa2: id_z2},
		success: function(response) {
			$(\'.obr_timer3\').each(function(){
				if(response == $(this).attr(\'data-id1\')){
					$(this).parent().parent().parent().remove();
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$(\'.obr_timer3\').each(function(){
				if(response == $(this).attr(\'data-id1\')){
				
				}
			});
		}
	 });
}

</script>
<style>
	.zebra_li span {
		margin: 5px 0;
		display: block;
		padding: 3px 0;
	}
</style>
'; ?>

<div class="dell_obmen" style="display:none;">
	<form action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/gf_gf_exchange.html" id="dell_obmen" method="post">
		<input type="hidden" name="opt" value="dell_obmen">
		<input type="hidden" name="id_zayavki" value="">
	</form>
</div>
<div class='kab-block' style="position:relative;overflow: inherit;">
	<header>
		<span style="float:left;">Заявки ожидающие подтверждения</span>
		<div  class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<table style="background:none !important" class="pamm-info last-import" id="grid2" >
			<thead style="z-index: 100;">
				<tr>
					<th data-type="number" data-name="id" data-click="2" style="position:relative" >№ заявки ↑↓</th>
					<th data-type="string" data-name="have" data-click="1" style="position:relative" >Отдаю: ↑↓</th>
					<th data-type="string" data-name="give" data-click="1" style="min-width: 220px;">Получаю: ↑↓</th>
					<td>Курс/<br>обратный курс</td>
					<td>Действие</td>
					<td>Время до<br>окончания</td>
				</tr>
			</thead>
			<tbody >
			<?php if ($this->_tpl_vars['applications']): ?>
			<?php $_from = $this->_tpl_vars['applications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
				<tr>
					<td style="padding: 10px 0;"><?php echo $this->_tpl_vars['result']['id']; ?>
</td>
					<td data-id="<?php echo $this->_tpl_vars['result']['id_paym_syst']; ?>
"><?php echo $this->_tpl_vars['result']['name_sys_return']; ?>
 : <?php echo $this->_tpl_vars['result']['money_off']; ?>
 USD</td>
					<td>
						<ul style="list-style:none; padding: 0px;">
						<?php $_from = $this->_tpl_vars['result']['masiv']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result2']):
?>
							<li class="zebra_li" data-id="<?php echo $this->_tpl_vars['result2']['id_sys_give']; ?>
">
								<span class="name_paym_system_s"><?php echo $this->_tpl_vars['result2']['name_sys_give']; ?>
 : <?php echo $this->_tpl_vars['result2']['money']; ?>
 USD </span>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
					</td>
					<td>
						<ul style="list-style:none; padding: 0px;">
						<?php $_from = $this->_tpl_vars['result']['masiv']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result2']):
?>
							<li class="zebra_li" data-id="<?php echo $this->_tpl_vars['result2']['id_sys_give']; ?>
">
								<span style="text-align: center;"><?php echo $this->_tpl_vars['result2']['kurs']; ?>
/<?php echo $this->_tpl_vars['result2']['kurs_obr']; ?>
</span>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
					</td>
					<td class="td_buttonu">
						<?php if ($this->_tpl_vars['result']['user_id'] == $this->_tpl_vars['ids']): ?>
							<a class="otpravka">обмен</a>
						<form style="display:none;" method="post" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/promo_code.html">
							<input type="hidden" name="id_zayavki" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
						</form>
						<?php elseif ($this->_tpl_vars['result']['id_click'] == '0'): ?>
						<ul style="list-style:none; padding: 0px;">
						<?php $_from = $this->_tpl_vars['result']['masiv']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result2']):
?>
							<li class="zebra_li" data-id="<?php echo $this->_tpl_vars['result2']['id_sys_give']; ?>
">
								<a class="otpravka">обмен</a>
						<form style="display:none;" method="post" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/promo_code.html">
							<input type="hidden" name="what_user" value="user_off">
							<input type="hidden" name="id_zayavki" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
							<input type="hidden" name="id_appl" value="<?php echo $this->_tpl_vars['result2']['id_sys_give']; ?>
">
						</form>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
						<?php else: ?>
							<a class="otpravka">обмен</a>
							<form style="display:none;" method="post" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/promo_code.html">
								<input type="hidden" name="id_zayavki" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
							</form>
						<?php endif; ?>
					</td>
					<td>
						<div class="clock_back" style="margin:auto;" data-zapusa="<?php echo $this->_tpl_vars['result']['id_zapusa']; ?>
" >
							<div class="fon_timer"></div>
							<div class="obr_timer" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-time="<?php echo $this->_tpl_vars['result']['time']; ?>
"></div>
						</div>
					</td>
				</tr>				
			<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['application_gf_gf']): ?>
			<?php $_from = $this->_tpl_vars['application_gf_gf']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
				<tr>
					<td style="padding: 10px 0;"><?php echo $this->_tpl_vars['result']['id']; ?>
</td>
					<td ><?php echo $this->_tpl_vars['result']['name_paym_syst_1']; ?>
 : <?php echo $this->_tpl_vars['result']['money_off']; ?>
 USD</td>
					<td><?php echo $this->_tpl_vars['result']['name_paym_syst_2']; ?>
 : <?php echo $this->_tpl_vars['result']['money_on']; ?>
 USD</td>
					<td><?php echo $this->_tpl_vars['result']['kurs']; ?>
/<?php echo $this->_tpl_vars['result']['obr_kurs']; ?>
</td>
					<td class="td_buttonu">
						<a class="otpravka">обмен</a>
						<form style="display:none;" method="post" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/gf_gf_exchange.html">
							<input type="hidden" name="id_zayavki" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
						</form>
						<a class="otmena_obmen" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
">oтменить</a>
						
					</td>
					<td>
						<div class="clock_back" style="margin:auto;" data-zapusa1="<?php echo $this->_tpl_vars['result']['id_zapusa']; ?>
" data-zapusa2="<?php echo $this->_tpl_vars['result']['id_zapusa_2']; ?>
" >
							<div class="fon_timer"></div>
							<div class="obr_timer2" data-id1="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-id2="<?php echo $this->_tpl_vars['result']['id_2']; ?>
" data-time="<?php echo $this->_tpl_vars['result']['time']; ?>
"></div>
						</div>
					</td>
				</tr>				
			<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['application_gf_garant']): ?>
			<?php $_from = $this->_tpl_vars['application_gf_garant']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
				<tr>
					<td style="padding: 10px 0;"><?php echo $this->_tpl_vars['result']['id']; ?>
</td>
					<td ><?php echo $this->_tpl_vars['result']['name_paym_syst_1']; ?>
 : <?php echo $this->_tpl_vars['result']['money_off']; ?>
 USD</td>
					<td><?php echo $this->_tpl_vars['result']['name_paym_syst_2']; ?>
 : <?php echo $this->_tpl_vars['result']['money_on']; ?>
 USD</td>
					<td><?php echo $this->_tpl_vars['result']['kurs']; ?>
/<?php echo $this->_tpl_vars['result']['obr_kurs']; ?>
</td>
					<td class="td_buttonu">
						<a class="otpravka">обмен</a>
						<form style="display:none;" method="post" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/gf_garant_exchange.html">
							<input type="hidden" name="id_zayavki" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
						</form>
						<a class="otmena_obmen" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
">oтменить</a>
					</td>
					<td>
						<div class="clock_back" style="margin:auto;" data-zapusa1="<?php echo $this->_tpl_vars['result']['id_zapusa']; ?>
" data-zapusa2="<?php echo $this->_tpl_vars['result']['id_zapusa_2']; ?>
" >
							<div class="fon_timer"></div>
							<div class="obr_timer3" data-id1="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-id2="<?php echo $this->_tpl_vars['result']['id_2']; ?>
" data-time="<?php echo $this->_tpl_vars['result']['time']; ?>
"></div>
						</div>
					</td>
				</tr>				
			<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
			</tbody>
	</table>
</div>
<?php echo '
<script>
	$(\'.otpravka\').click(function(){
		$(this).parent().find(\'form\').submit();
	});
</script>
'; ?>

<?php endif; ?>
<div id="button_exchange">
	<div class="td_buttonu" style="float:left;"><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/service.html">Обмен через сервис</a></div>
	<div class="td_buttonu" style="float:right;"><a href="<?php echo $this->_tpl_vars['usr']['id']; ?>
/payment_system.html">Создать заявку на бирже</a></div>
</div>
<!-- Платежные данные -->
<div style=" border: 1px solid black; position:relative; border-top: none;" class='kab-block' >
	<header>
		<span style="float:left;">Платежные данные</span>
		<div class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
		<span style="float:right; margin-right:50px;"><a class="a_payment_syst" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/editprofile.html#payment_inf">настройка платежных систем</a></span>
	</header>
	<div class="user_payment_system">
		<ul>
		<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
			<?php if ($this->_tpl_vars['inf_moneys'][$this->_tpl_vars['system']['id']]['score_num'] != ''): ?>
			<li data-inf="0" data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
">
				<span style="float:left; width: 55px;"><?php echo $this->_tpl_vars['kurss'][$this->_tpl_vars['system']['id']]['user_curs']; ?>
</span>
				<img style="float:left;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
">
				<span style="float: right; text-align: left; padding-left: 0px; width: 105px;"><?php echo $this->_tpl_vars['system']['name']; ?>
</span>
			</li>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</ul>
	</div>
	<div class="payment_system">
		<ul>
		<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
			<li data-inf="0" data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
" data-user="<?php echo $this->_tpl_vars['usr']['id']; ?>
">
				<span style="float:left; width: 60px;"><?php echo $this->_tpl_vars['kurss'][$this->_tpl_vars['system']['id']]['forex_curs']; ?>
</span>
				<div style="width:105px; float:left;"><img style="float:left;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></div>
				<span style="float:left;"><?php echo $this->_tpl_vars['system']['name']; ?>
</span>
				<div class="button_hidden">
					<a style="margin-top:0px; margin-bottom:4px;" href="<?php echo $this->_tpl_vars['usr']['id']; ?>
/payment_system.html" >Купить на бирже</a>
					<a class="servise_href" href="" >Купить у сервиса</a>
				</div>
			</li>
		<?php endforeach; endif; unset($_from); ?>
		</ul>
	</div>
	<!--div class="reserve">
	</div-->
</div>
<?php echo '
<style>
	.payment_system li:hover {
		background: #FFE4C4;
	}
</style>
<script>
inf_system_paym = 0;
$(\'.user_payment_system li\').click(function(){
	$(\'.user_payment_system li\').each(function(){
		if($(this).attr(\'data-inf\') == \'1\'){
			inf_system_paym = 1;
			$(this).removeClass(\'active\');
		}
	});
	
	if($(this).attr(\'data-inf\') == \'1\'){
		$(\'.payment_system li\').each(function(){
			if($(this).attr(\'data-inf\') == \'1\'){
				$(this).find(\'.button_hidden\').css(\'display\', \'none\');
				$(this).removeClass(\'active\');
			}
		});
		$(this).removeClass(\'active\');
		$(this).attr(\'data-inf\', \'0\');
	}
	else{
		$(\'.user_payment_system li\').each(function(){
			$(this).attr(\'data-inf\', \'0\');
		});
		$(\'.payment_system li\').each(function(){
				if($(this).attr(\'data-inf\') == \'1\'){
					$(this).find(\'.button_hidden\').css(\'display\', \'block\');
					$(this).addClass(\'active\');
				}
			});
		$(this).addClass(\'active\');
		$(this).attr(\'data-inf\', \'1\');
	}
	
	//if(inf_system_paym != \'1\'){
		//alert($(this).attr(\'data-inf\'));
		if($(this).attr(\'data-inf\') == \'0\'){
			
		//}
	}
	
//inf_system_paym = 0;	
});
hidden_button = 0;
$(\'.payment_system li\').click(function(){
	$(\'.user_payment_system li\').each(function(){
		if($(this).attr(\'data-inf\') == \'1\'){hidden_button = 1; system1 = $(this).attr(\'data-id\');}
	});
	if(hidden_button == \'1\'){
		$(\'.payment_system li\').each(function(){
			if($(this).attr(\'data-inf\') == \'1\'){
				$(this).removeClass(\'active\');
				$(this).find(\'.button_hidden\').css(\'display\', \'none\');
			}
		});
		if($(this).attr(\'data-inf\') == \'1\'){
		$(this).find(\'.button_hidden\').css(\'display\', \'none\');
		$(this).removeClass(\'active\');
		$(this).attr(\'data-inf\', \'0\');
		}
		else{
				$(\'.payment_system li\').each(function(){
					$(this).attr(\'data-inf\', \'0\');
				});
				$(this).find(\'.button_hidden\').css(\'display\', \'block\');
				$(this).find(\'.button_hidden .servise_href\').attr(\'href\', $(this).attr(\'data-user\')+\'/service.html?system1=\'+system1+\'&system2=\'+$(this).attr(\'data-id\'));
				$(this).addClass(\'active\');
				$(this).attr(\'data-inf\', \'1\');
			}
	}
	else{
		$(\'.payment_system li\').each(function(){
			$(this).removeClass(\'active\');
		});
	}
	hidden_button = 0;
});

$(document).ready(function(){
	
	
	var grid = document.getElementById(\'grid\');
 
    grid.onclick = function(e) {
      if (e.target.tagName != \'TH\') return;

      // Если TH -- сортируем
	  sortGrid(e.target.cellIndex, e.target.getAttribute(\'data-type\'), e.target.getAttribute(\'data-name\'), e.target.getAttribute(\'data-click\'));
	  
    };

    function sortGrid(colNum, type, name, click) {
	
	$(\'#grid thead th\').each(function(){
		if($(this).attr("data-name") == name){
			if($(this).attr("data-click") == \'1\'){
				$(this).attr("data-click", "2");
			}
			else{$(this).attr("data-click", "1");}
		}
	});
	
      var tbody = grid.getElementsByTagName(\'tbody\')[0];

      // Составить массив из TR
      var rowsArray = [].slice.call(tbody.rows);

      // определить функцию сравнения, в зависимости от типа
      var compare;

      switch (type) {
        case \'number\':
			
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].innerHTML - rowA.cells[colNum].innerHTML;
			  };
			}
          break;
        case \'string\':
		
			if(click == "1"){
			  compare = function(rowA, rowB) {
			  	return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
          break;
		case \'masive\':
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].getAttribute(\'data-title\') - rowB.cells[colNum].getAttribute(\'data-title\');
				
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].getAttribute(\'data-title\') - rowA.cells[colNum].getAttribute(\'data-title\');
			  };
			}
		
		  break;
      }

      // сортировать
      rowsArray.sort(compare);

      // Убрать tbody из большого DOM документа для лучшей производительности
      grid.removeChild(tbody);

      // добавить результат в нужном порядке в TBODY
      // они автоматически будут убраны со старых мест и вставлены в правильном порядке
      for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
      }

      grid.appendChild(tbody);

    }
	
$(\'.zebra_table\').hover(
	function(){
		$(this).addClass("border_hover");
	},
	function(){
		$(this).removeClass("border_hover");
	}
);
});
</script>
'; ?>

<!-- система заявок на обмен -->
<div class='kab-block' style="position:relative;">
	<header >
		<span style="float: left; margin-bottom: 10px;">Мои заявки на обмен валюты</span>
		<div class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<div class="my-pamms" style="display: table; width: 100%;" >
		
		<table class="pamm-info last-import" id="grid" data-varif="<?php echo $this->_tpl_vars['verification']; ?>
">
			<thead style="z-index: 100;">
				<tr>
					<th data-type="number" data-name="id" data-click="2" style="position:relative" >№ заявки ↑↓</th>
					<th data-type="string" data-name="have" data-click="1" style="position:relative" >Есть: ↑↓</th>
					<th data-type="masive" data-name="give" data-click="1" style="min-width: 205px;">Надо: ↑↓</th>
					<th data-type="masive" data-name="kurs" data-click="1">Курс / Обратный курс ↑↓</th>
					<th data-type="string" data-name="garant" data-click="1">* ↑↓</th>
					<th data-type="masive" data-name="part" data-click="2" style="position:relative" >Продать частями ↑↓</th>
					<td>Действие</td>
				</tr>
			</thead>
			<tbody >
		<?php $_from = $this->_tpl_vars['request_payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['result']):
?>
			
				<tr id="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
" class="zebra_table" >
					<td><?php echo $this->_tpl_vars['result']['id']; ?>
</td>
					<td><?php echo $this->_tpl_vars['result']['name_user_system']; ?>
: <?php echo $this->_tpl_vars['result']['money_off']; ?>
 USD</td>
					<td class="td_li zebra_fon1" data-title="<?php echo count($this->_tpl_vars['result']['masive']); ?>
">
						<ul style="list-style:none; padding: 0px;">
						<?php $_from = $this->_tpl_vars['result']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['infa']):
?>
							<li class="zebra_liggg" data-id="<?php echo $this->_tpl_vars['tid']; ?>
">
								<span class="name_paym_system_s"><?php echo $this->_tpl_vars['infa']['name_paym_system']; ?>
: <?php echo $this->_tpl_vars['infa']['money']; ?>
 USD </span>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
					</td>
					<td class="td_li zebra_fon2" data-title="<?php echo count($this->_tpl_vars['result']['masive']); ?>
">
						<ul style="list-style:none; padding: 0px;">
						<?php $_from = $this->_tpl_vars['result']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['infa']):
?>
							<li class="zebra_liggg" data-id="<?php echo $this->_tpl_vars['tid']; ?>
">
								<span style="text-align: center;"><?php echo $this->_tpl_vars['infa']['kurs']; ?>
/<?php echo $this->_tpl_vars['infa']['kurs_obr']; ?>
</span>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
					</td>
					<td><?php if ($this->_tpl_vars['result']['garant'] == '1'): ?>Гарант<?php elseif ($this->_tpl_vars['result']['garant'] == '2'): ?>Внутренняя конвертация<?php elseif ($this->_tpl_vars['result']['garant'] == '3'): ?>Гарантийный фонд<?php endif; ?></td>
					<td data-title="<?php echo $this->_tpl_vars['result']['sell_parts']; ?>
"><?php if ($this->_tpl_vars['result']['sell_parts'] == '1'): ?>+<?php else: ?>-<?php endif; ?></td>
					<td class="td_buttonu">
						<a class="button_change" >Изменить</a>
						<a class="button_del" >Убрать</a>
						<?php if ($this->_tpl_vars['result']['garant'] == '1' || $this->_tpl_vars['result']['garant'] == '3'): ?><a class="button_buy_serv" >Купить у сервиса</a><?php else: ?><?php endif; ?>
						<div class="hiddenn_inf_del">
							<form action="" method="post" >
							<input type="hidden" name="opt" value="del_application">
							<input type="hidden" name="id_application" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
							<span>Вы точно хотите удалить заявку № <?php echo $this->_tpl_vars['result']['id']; ?>
 ?<span>
							<p style="margin-top: 10px;">
								<input type="submit" value="Удалить"> <input class="otmenit" type="button" value="Отменить"> 
							</p>
							</form>
						</div>
						<div class="hidden_information">
							<form action="" style="width:800px;" method="post" class="form_change" onsubmit="return validation_change(<?php echo $this->_tpl_vars['result']['id']; ?>
)" data-garant="<?php echo $this->_tpl_vars['result']['garant']; ?>
" <?php if ($this->_tpl_vars['result']['garant'] == '1'): ?>data-money="<?php echo $this->_tpl_vars['result']['money_score']; ?>
"<?php endif; ?> data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
">
							<input type="hidden" name="opt" value="save_change">
							<input type="hidden" name="idshka" value="<?php echo $this->_tpl_vars['result']['id']; ?>
">
							<input type="hidden" name="payment_system_id" value="<?php echo $this->_tpl_vars['result']['name_user_system']; ?>
">
							<div class="max_money"><span>Максимальное к-во средств:<?php if ($this->_tpl_vars['result']['garant'] == '1'): ?><?php echo $this->_tpl_vars['result']['money_score']; ?>
 $
							<?php elseif ($this->_tpl_vars['result']['garant'] == '2'): ?>
								<?php if ($this->_tpl_vars['verification'] == '1'): ?> 25 $
								<?php elseif ($this->_tpl_vars['verification'] == '2'): ?> 50 $
								<?php elseif ($this->_tpl_vars['verification'] == '3'): ?> 75 $
								<?php elseif ($this->_tpl_vars['verification'] == '4'): ?> 100 $
								<?php endif; ?>
							<?php elseif ($this->_tpl_vars['result']['garant'] == '3'): ?>
							<?php echo $this->_tpl_vars['garant_money']; ?>
 $
							<?php endif; ?></span></div>
							<div class="user_ss" style="margin: auto;margin-bottom: 10px;">
								Отдаю:<br><?php echo $this->_tpl_vars['result']['name_user_system']; ?>
: 
								<input type="text" class="hhhhhhh" name="user_money" value="<?php echo $this->_tpl_vars['result']['money_off']; ?>
">
							</div>
							<div class="system_ss" style="width: 75%;margin: auto;margin-bottom: 15px;">
								<ul style="list-style:none; padding: 0px;">
									<li style="border-bottom: 1px dashed #ccc; padding-bottom: 5px;">Получаю:</li>
								<?php $_from = $this->_tpl_vars['result']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['infa']):
?>
									<li style="border-bottom: 1px dashed #ccc; padding-bottom: 5px;">
									<label style="display: block;"><?php echo $this->_tpl_vars['infa']['name_paym_system']; ?>
:</label>
									<p style="float:left;">
										<input type="hidden" name="name_paym_system[]" value="<?php echo $this->_tpl_vars['infa']['name_paym_system']; ?>
">
										<input type="hidden" name="score_s[]" value="<?php echo $this->_tpl_vars['infa']['score']; ?>
">
										<span>курс: </span><input data-id="<?php echo $this->_tpl_vars['tid']; ?>
" class="dddddd" type="text" name="kurs_s[]" value="<?php echo $this->_tpl_vars['infa']['kurs']; ?>
">
									</p>
									<p style="float:left;">
										<span class="name_paym_system_s" >валюта:</span>
										<input type="text" data-id="<?php echo $this->_tpl_vars['tid']; ?>
" class="cccccc" name="money[]" value="<?php echo $this->_tpl_vars['infa']['money']; ?>
" >
									</p>
									<div style="    margin-left: -25px;">
									<div style=" width: 600px;">
										<span>номер рахунку: </span>
										<?php if ($this->_tpl_vars['result']['garant'] == '3'): ?>
											<span><?php echo $this->_tpl_vars['infa']['score']; ?>
</span>
										<?php else: ?>
										<a class="href_editprofile" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/editprofile.html#payment_inf"><?php echo $this->_tpl_vars['infa']['score']; ?>

											<input type="button" style="margin-left: 10px; border: none; background-color: #D71411; color: #fff; font-weight: bold;" value="Редактировать">
										</a>
										<?php endif; ?>
									</div>
										<?php if ($this->_tpl_vars['result']['garant'] == '30'): ?>
									<div style=" width: 300px;">
										<span>Гарантийный фонд:</span>
										<a class="href_editprofile" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/guarante_fond.html"><?php echo $this->_tpl_vars['garant_money']; ?>

											<input type="button" style="margin-left: 10px; border: none; background-color: #D71411; color: #fff; font-weight: bold;" value="Повысить Гарантийный фонд">
										</a>
									</div>
									</div>
										<?php endif; ?>
									</li>
								<?php endforeach; endif; unset($_from); ?>
								</ul>
							</div>
							<div style="margin-left: 130px;width: 25%;float: left;">
							<label id="sell_parts_l" style="float:left; margin-right: 10px; margin-left: 38px;">Продать частями</label>
							<label style="float:left; margin-top: 2px; <?php if ($this->_tpl_vars['result']['sell_parts'] == 'on'): ?>background-position:-1px -34px;<?php else: ?>background-position:-1px -4px;<?php endif; ?>" id="sell_parts"></label>
							<input type="checkbox" <?php if ($this->_tpl_vars['result']['sell_parts'] == 'on'): ?> checked <?php endif; ?> name="sell_parts">
							</div>
							<input type="submit" value="Подтвердить">
							</form>
						</div>
					</td>
				</tr>
				
			
			
		<?php endforeach; endif; unset($_from); ?>
		</tbody>
		</table>
		<div style="border:1px solid; display:table; width:700px;margin-bottom:20px;">
		<a class="exchange-link" style="float: right; margin-right: 5px;" data-id="<?php echo $this->_tpl_vars['ids']; ?>
" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/exchange.html"><span>Биржа обмена</span></a>
		</div>
	</div>
</div>
<div id="garant_money" data-fond="<?php echo $this->_tpl_vars['garant_money']; ?>
"></div>
<div class="hidden_information_2">
	<form id="otpravka_to_servise" action="/users/<?php echo $this->_tpl_vars['ids']; ?>
/service.html" method="post">
		<input type="hidden" name="opt" value="inf_service">
		<input type="hidden" name="id_zapusa" value="">
	</form>
</div>
<?php echo '


<script>
$(\'.zebra_fon1\').each(function(){
		jjjg = 0;
		$(this).find(\'ul li\').each(function(){
			jjjg++;
		});
		if(jjjg == \'1\'){
			$(this).find(\'li\').css("background","none");
		}
	});
	$(\'.zebra_fon2\').each(function(){
		jjjg = 0;
		$(this).find(\'ul li\').each(function(){
			jjjg++;
		});
		if(jjjg == \'1\'){
			$(this).find(\'li\').css("background","none");
		}
	});
	function validation_change(id_priem){
		ffff = 0;
		$(\'#grid tr .form_change\').each(function(){
			if($(this).attr(\'data-id\') == id_priem){
				id_sys = $(this).attr("data-id");
				if(garant_sys[id_sys] == \'1\'){
					money_1 = parseFloat($(this).find(\'.hhhhhhh\').val());
					money_2 = parseFloat(money[id_sys]);
					if(money_1 > money_2){
						$(this).find(\'.user_ss\').addClass(\'red_input\');
						//alert($(this).find(\'.hhhhhhh\').val());
						ffff = 1;
					}
				}
				else if(garant_sys[id_sys] == \'2\'){
					sum = parseFloat($(this).find(\'.hhhhhhh\').val());
					if(varification == \'1\'){
						if(sum > 26){
							$(this).find(\'.hhhhhhh\').css("border","1px solid red");
							ffff = 1;
						}
					}
					else if(varification == \'2\'){
						if(sum > 51){
							$(this).find(\'.hhhhhhh\').css("border","1px solid red");
							ffff = 1;
						}
					}
					else if(varification == \'3\'){
						if(sum > 76){
							$(this).find(\'.hhhhhhh\').css("border","1px solid red");
							ffff = 1;
						}
					}
					else if(varification == \'4\'){
						if(sum > 101){
							$(this).find(\'.hhhhhhh\').css("border","1px solid red");
							ffff = 1;
						}
					}
					else{
						ffff = 1;
					}
				}
				else if(garant_sys[id_sys] == \'3\'){
					hhhhf = parseFloat($(this).find(\'.hhhhhhh\').val());
					garant_money = parseFloat(garant_money);
					if(hhhhf > garant_money){
						$(this).find(\'.hhhhhhh\').css("border","1px solid red");
						ffff = 1;
					}
				}
			}
		});
		if(ffff == 1){ return false; }
	}
$(document).ready(function(){
	
	varification = $(\'#grid\').attr(\'data-varif\');
	
	money = [];
	garant_sys = [];
	
	$(\'#grid .form_change\').each(function(){
		num = $(this).attr(\'data-id\');
		
		money[num] = $(this).attr(\'data-money\');
		$(this).attr(\'data-money\', \'\');
		
		garant_sys[num] = $(this).attr(\'data-garant\');
		$(this).attr(\'data-garant\', \'\');
		
	});
	
	garant_money = $(\'#garant_money\').attr(\'data-fond\');
	$(\'#garant_money\').attr(\'data-fond\', "");
	
	$(\'.button_buy_serv\').click(function(){
		id_ska = $(this).parent().parent().attr(\'data-id\');
		$(\'.hidden_information_2 input[name="id_zapusa"]\').val(id_ska);
		$(\'#otpravka_to_servise\').submit();
	});
	
	$(\'#modal_success2\').on(\'click\', \'.otmenit\', function(){
		$(this).parent().parent().parent().parent().parent().parent().parent().find(\'.close\').click();
	});
	$(\'#modal_success\').on(\'click\', \'#sell_parts_l\', function(){ $(\'#modal_success\').find(\'#sell_parts\').click(); });
	$(\'#modal_success\').on(\'click\', \'#sell_parts\', function(){
		sell_parts = $(this).parent().find(\'input\').prop("checked");
		if(sell_parts == false){
			$(this).css(\'background-position\', \'-1px -34px\');
			$(this).parent().find(\'input\').click();
		}
		else{
			$(this).css(\'background-position\', \'-1px -4px\');
			$(this).parent().find(\'input\').click();
		}	
	});
	/* удаление заявки */
	$(\'.button_del\').click(function(){
		infa = $(this).parent().find(\'.hiddenn_inf_del\').html();
		
		//alert(infa);
		$(\'#modal_success2 #text_span\').html(infa);
		$(\'#modal_success_btn2\').click();
	});
	/* изменение заявки */
	$(\'.button_change\').click(function(){
		infa = $(this).parent().find(\'.hidden_information\').html();
		//alert(infa);
		$(\'#modal_success #text_span\').html(infa);
		$(\'#modal_success_btn\').click();
	});
	$(\'#modal_success\').on(\'keyup keydown\', \'.hhhhhhh\', function(){
		count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, \'\');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, \'\');
			
		Value_1 = $(this).val();
		Value_1 = Value_1.replace(",", \'.\');
		$(\'.hhhhhhh\').val(Value_1);
		$(\'.system_ss .dddddd\').each(function(){
			ssss = $(this).attr(\'data-id\');
			kurs_num = $(this).val();
			
			if(kurs_num != \'\' && Value_1 != ""){
				result = Value_1*kurs_num;
				$(\'.system_ss .cccccc\').each(function(){
					if(ssss == $(this).attr(\'data-id\')){ 
						$(this).val(result);
						this.value = Math.round(parseFloat(this.value)*100)/100;
					}
				});
			}
			else{
				$(\'.system_ss .cccccc\').each(function(){
					if(ssss == $(this).attr(\'data-id\')){ 
						$(this).val(\'\');
					}
				});
			}
		});
		
	});
	$(\'#modal_success\').on(\'focus\', \'.dddddd\', function(){
		$(this).on(\'keyup keydown\', function(){
			count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, \'\');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.,]/g, \'\');
		
			Value_1 = $(this).parent().parent().parent().parent().parent().find(\'.user_ss \').find(\'input[type="text"]\').val();
			Value_1=Value_1.replace(",", \'.\');
			kurs_num = $(this).val();
			kurs_num = kurs_num.replace(",", \'.\');
			eeee = $(this).attr(\'data-id\');
			if(kurs_num != \'\' && Value_1 != ""){
				result = Value_1*kurs_num;
				$(\'.system_ss .cccccc\').each(function(){
					
					if(eeee == $(this).attr(\'data-id\')){ 
						$(this).val(result); 
						this.value = Math.round(parseFloat(this.value)*100)/100;
					}
				});
			}
			else{
				$(\'.system_ss .cccccc\').each(function(){
					if(eeee == $(this).attr(\'data-id\')){ 
						$(this).val(""); 
					}
				});
			}
		
		
		});
	});
	$(\'#modal_success\').on(\'focus\', \'.cccccc\', function(){
		$(this).on(\'keyup keydown\', function(){
			count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, \'\');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.,]/g, \'\');
			
			Value_1 = $(this).parent().parent().parent().parent().parent().find(\'.user_ss \').find(\'input[type="text"]\').val();
			Value_1=Value_1.replace(",", \'.\');
			money = $(this).val();
			money = money.replace(",", \'.\');
			eeee = $(this).attr(\'data-id\');
			if(money != \'\' && Value_1 != ""){
				result = money/Value_1;
				$(\'.system_ss .dddddd\').each(function(){
					if(eeee == $(this).attr(\'data-id\')){ 
						$(this).val(result); 
						
					}
				});
			}
			else{
				$(\'.system_ss .dddddd\').each(function(){
					if(eeee == $(this).attr(\'data-id\')){ 
						$(this).val(""); 
					}
				});
			}
		
		
		});
	});
	var hash = document.location.hash;
	$(\'#grid tbody tr\').each(function(){
				if(\'#\'+$(this).attr("data-id") == hash){
					$(this).find(\'.td_buttonu .button_change\').trigger(\'click\');
				}
			});	
});
</script>
'; ?>


<div id="modal_success" class="modal hide fade" style='width: 900px; margin-left: -450px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center; max-height: 600px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block;  font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>
<div id="modal_success2" class="modal hide fade" style='width: 600px; margin-left: -300px;'>
	<div class="modal-body" style='padding-top:15px; border: 5px solid #ccc; text-align: center; max-height: 600px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin:10px 0; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success2" role="button" data-toggle="modal" id='modal_success_btn2'></a>
<!-- Демонстрационный счет -->
<!--div class='kab-block'>
	<header>
		<span>Демонстрационный счет</span>
	</header>
	
	<div class='demo'>
		<div></div><span>У Вас пока нет демонстрационного счета.</span>
		<a href='#modal1' role="button" data-toggle="modal" style='margin-top: 5px; margin-bottom: 5px;' class='button_box'><span>Создать СЧЕТ</span></a>
	</div>
</div-->
<!--div class='kab-block' style="position:relative;">
	<header>
		<span>Мои ПАММ - портфели</span>
	</header>
	<!--<input style="position:absolute;top:0;" type="button" name="watchPamms" data-id="<?php echo $this->_tpl_vars['ids']; ?>
" value="Купить/Продать" />>
	<div class="my-pamms" style="display: table; width: 100%;" >
		<?php if (count($this->_tpl_vars['user_accounts']) == 0): ?>
			<p>У Вас пока нет ПАММ портфелей.</p>
		<?php endif; ?>
		<?php $_from = $this->_tpl_vars['user_accounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['result']):
?>
			<table class="pamm-info last-import">
				<thead>
					<tr>
						<td>Название</td>
						<td>Доход за месяц,$</td>
						<td>Общий доход,$</td>
						<td>Коммисия,%</td>
						<td>Цена,$</td>
						<td>Количество,шт.</td>
						<td>Действие</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $this->_tpl_vars['key']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['profitPerMonth']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['totalProfit']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['tax']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['price']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['count']; ?>
</td>
						<td>
							<a class="sell-pamm" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-count="<?php echo $this->_tpl_vars['result']['count']; ?>
">Продать</a>
						</td>
					</tr>
				</tbody>
			</table>
		<?php endforeach; endif; unset($_from); ?>
		<div style="border:1px solid; display:table; width:700px;margin-bottom:20px;">
		<a class="exchange-link" data-id="<?php echo $this->_tpl_vars['ids']; ?>
"><span>Купить на бирже</span></a>
		<a class="service-link" data-id="<?php echo $this->_tpl_vars['ids']; ?>
"><span>Купить у сервиса</span></a>
		</div>
	</div>
	
	<?php if (count($this->_tpl_vars['exchange_pamm']) > 0): ?>
	<header>
		<span>Мои ПАММ - портфели выставлены на биржу </span>
	</header>
	<?php endif; ?>
	<div class="exchange-pamms" >
		<?php $_from = $this->_tpl_vars['exchange_pamm']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['result']):
?>
			<table class="pamm-sold-info">
				<thead>
					<tr>
						<td>Название</td>
						<td>Доход за месяц,$</td>
						<td>Общий доход,$</td>
						<td>Коммисия,%</td>
						<td>Цена,$</td>
						<td>Количество,шт.</td>
						<td>Действие</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $this->_tpl_vars['result']['title']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['profitPerMonth']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['totalProfit']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['tax']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['price']; ?>
</td>
						<td><?php echo $this->_tpl_vars['result']['count']; ?>
</td>
						<td>
							<a class="change-pamms" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-count="<?php echo $this->_tpl_vars['result']['count']; ?>
" data-price="<?php echo $this->_tpl_vars['result']['price']; ?>
">Изменить</a>	
							<a class="remove-pamms" data-name="<?php echo $this->_tpl_vars['result']['title']; ?>
" data-id="<?php echo $this->_tpl_vars['result']['id']; ?>
" data-count="<?php echo $this->_tpl_vars['result']['count']; ?>
">Убрать</a>
						</td>
					</tr>
				</tbody>
			</table>
		<?php endforeach; endif; unset($_from); ?>
	</div>
	<!-- вывод памм портфелей юзера -->
	<?php $_from = $this->_tpl_vars['user_pamms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['pamm']):
?>
	
	<!--div class='pamms_div'>
		<div id='name_div'>
			<a href='/users/<?php echo $this->_tpl_vars['is_auth']; ?>
/pamm.html'> <img src='<?php echo $this->_tpl_vars['pamm']['pamm_info']['image_url']; ?>
'/>
			<span><?php echo $this->_tpl_vars['pamm']['pamm_info']['title']; ?>
</span>
			</a>
		</div>
		
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td style='width:135px;border:1px solid #CCCCCC;border-top: 0;'>
					<table style='width:100%'>
						<tr><td style='height: 60px;'><span id='invest_nymber'>№<?php echo $this->_tpl_vars['pamm']['pamm_info']['id']; ?>
</span></td></tr>
						<tr><td style='background:#F5F5F5;height:64px;'> <object type="application/x-shockwave-flash" data="/includes/cabinet-menu-4.swf" height="40" style="visibility: visible;margin-left: 9px;" width='45'><param name="wmode" value="opaque" /></object><a class='invest_A' href=''><span style=''>История</span></a></td></tr>
					</table>
				</td>
				
				<td style='width:180px;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<p style='text-align: center;'>Прибыль проекта:</p>
					<table style='margin-left: 10px;'>
						<tr>
							<td>
								<span class='pamm_span'>недельная:</span>
								<span class='dotted2'></span>
							</td>
							<td class="red_td">
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['week']; ?>
%
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>месячная:</span>
								<span class='dotted2'></span>
							</td>
							<td class="orange_td">
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['month']; ?>
%
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>общая:</span>
								<span class='dotted2'></span>	
							</td>
							<td class="green_td">
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['general']; ?>
%
							</td>
						</tr>
					</table>
				</td>
				
				<td style='width:180px;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<p style='text-align: center;'>Информация по счету:</p>
					<table style='margin-left: 10px;'>
						<tr>
							<td>
								<span class='pamm_span'>Валюта:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['currency']; ?>

							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>Общий баланс:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['money']; ?>

							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>Доходность:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								<?php echo $this->_tpl_vars['pamm']['pamm_info']['income']; ?>

							</td>
						</tr>
					</table>
				</td>
				<td style='border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<!--a href='/users/<?php echo $this->_tpl_vars['ids']; ?>
/outpay.html' style='color:#000;padding-left:34px' class='money'>Вывести средства</a-->
					<!--a href='#modal1' role="button" data-toggle="modal" style='color:#000;padding-left:34px' class='money'>Вывести средства</a>
					<a href='/users/<?php echo $this->_tpl_vars['ids']; ?>
/invest_pamm<?php echo $this->_tpl_vars['pamm']['pamm_info']['id']; ?>
.html' style='font-weight:normal; position:relative; top:0px;' class='button_box'><span>Пополнить СЧЕТ</span></a>
				</td>
			</tr>
			<tr>
				<td colspan='4' style='text-align:center;background:#CCCCCC;border-right:1px solid #CCCCCC;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>Дата окончания инвестиционного периода <span class="end_date"><?php echo $this->_tpl_vars['pamm']['pamm_info']['end_date']; ?>
</span></td>
			</tr>
		</table>
	
	</div>
	<?php endforeach; endif; unset($_from); ?>
</div-->
<!--div class='kab-block'>
	<header>
		<span>Рейтинг ПАММ - портфелей</span>
	</header>
	
	<!-- вывод всех памм портфелей >
	
	<div class="sort_pamms">
		<span>Сортировать</span>
		<div><a>Тип торговли</a></div>
		<div><a>Доходность</a></div>
		<div><a>Брокер</a></div>
	</div>
	<?php $_from = $this->_tpl_vars['all_pamms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['pamm']):
?>
	<div class='pamms_div'>
		<div id='name_div'>
			<a href='/users/<?php echo $this->_tpl_vars['is_auth']; ?>
/pamm.html'> <img src='<?php echo $this->_tpl_vars['pamm']['image_url']; ?>
'/>
			<span><?php echo $this->_tpl_vars['pamm']['title']; ?>
</span>
			</a>
		</div>
		
		<table cellspacing="0" cellpadding="0" style="border: 1px solid #ccc;">
			<tr>
				<td rowspan="3" style='width:135px;border:1px solid #CCCCCC;border-top: 0;' class="capit">
					<div>Капитализация ПАММ-портфеля:</div>
					<span><?php echo $this->_tpl_vars['pamm']['capitalization']; ?>
 <?php echo $this->_tpl_vars['pamm']['currency']; ?>
</span>
				</td>
				<td colspan="3" style=''>
					<div class="pamm_income">Доходность ПАММ-портфеля</div>
				</td>
			</tr>
			<tr>
				<td style='width:180px;'>
					<div class="income_head">За последнюю неделю</div>
					<div class="income_value red_td"><?php echo $this->_tpl_vars['pamm']['week']; ?>
 %</div>
				</td>
				
				<td style='width:180px;'>
					<div class="income_head">За последний месяц</div>
					<div class="income_value green_td"><?php echo $this->_tpl_vars['pamm']['month']; ?>
 %</div>
				</td>
				<td style='width:180px;' class="last_td">
					<div class="income_head">За все время</div>
					<div class="income_value green_td"><?php echo $this->_tpl_vars['pamm']['general']; ?>
 %</div>
				</td>
			</tr>
			<tr>
				<td style='width:180px;'>
					<a class="income_btn" href='/users/<?php echo $this->_tpl_vars['is_auth']; ?>
/pamm.html'>Детальная информация о проекте</a>
				</td>
				
				<td style='width:180px;'>
					<a class="income_btn forum_" href='/stati/forum.html'><span>Обсудить на форуме</span></a>
				</td>
				<td style='width:180px;' class="last_td">
					<a class="income_btn" href="/users/<?php echo $this->_tpl_vars['ids']; ?>
/invest_pamm<?php echo $this->_tpl_vars['pamm']['id']; ?>
.html">Пополнить ПАММ - портфель</a>
				</td>
			</tr>
		</table>
	
	</div>
	<?php endforeach; endif; unset($_from); ?>
</div-->

<div id="modal_success_sell" class="modal hide fade" style='width: 680px;left:45%;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<!--<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Продажа</span>
		<br />-->
		<input style="margin-left:18px;" type="button" id="exchangeLink" value="Продажа ПАММ портфеля на биржу" />
		<input style="margin-left:7px;" type="button" id="serviceLink" value="Продажа ПАММ портфеля сервису" />
	</div>
</div>
<a href="#modal_success_sell" role="button" data-toggle="modal" id='modal_success_btn'></a>


<div id="modal_success_sell_exchange" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Продать на бирже</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
			<form method="post" >
				<table style="margin:auto;">
					<thead>
						<tr>
							<td>Цена за единицу,$</td>
							<td>Количество,шт.</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input type="number" min="0.01"  step="0.01" name="price" class="price pamm-input" required />
							</td>
							<td>
								<input type="number" min="1" name="count" class="count pamm-input" required />
							</td>
						</tr>
						<?php echo smarty_function_captcha2(array(), $this);?>

						<tr style="position:relative;top:10px;">
							<td colspan="2">
								<input type="submit" style="margin-top:10px;" value="Подтверждение" />
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="id" />
			</form>
	</div>
</div>
<a href="#modal_success_sell_exchange" role="button" data-toggle="modal" id='modal_success_btn_exchange'></a>


<div id="modal_success_sell_service" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Продать на сервис</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
			<form method="post" >
				<table style="margin:auto;">
					<thead>
						<tr>
							<td colspan="2">Количество,шт.</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2">
								<input type="number" class="pamm-input count" min="1"  name="count"  required  />
							</td>
						</tr>
						<?php echo smarty_function_captcha2(array(), $this);?>

						<tr style="position:relative;top:10px;">
							<td colspan="2">
								<input type="submit" style="margin-top:10px;" value="Подтверждение" />
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="id" />
			</form>
	</div>
</div>
<a href="#modal_success_sell_service" role="button" data-toggle="modal" id='modal_success_btn_service'></a>

<div id="changePammsModal" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title" style="left:47%;">Изменить</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
		<form method="post">
			<table style="margin:auto;">
				<thead>
					<tr>
						<td>
							Цена за единицу,$
						</td>
						<td>
							Количество,шт.
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="number" class="pamm-input" name="new-price" min="0.01" step="0.01" />
						</td>
						<td>
							<input type="number" class="pamm-input" name="change-count" min="1" />
						</td>
					</tr>
					<?php echo smarty_function_captcha2(array(), $this);?>

					<tr style="position:relative;top:10px;">
						<td colspan="2">
							<input type="submit" style="margin-top:10px;" value="Подтверждение" />
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="change-id" />
		</form>
	</div>
</div>
<a href="#changePammsModal" role="button" data-toggle="modal" class="view-change-pamms"></a>

<div id="removePammsModal" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<!--<span style='position: relative; top: 30%;'>-->
		<span style='position: absolute;left:0;width:100%;top:25%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Убрать <span id="pammName"></span> с биржи?</span>
		<form method="post" id="removePammAccount">
			<input style="width:42px;" type="submit" value="Да" />
			<input type="button" id="btnCnl" value="Нет" />
			<input type="hidden" name="remove-success" />
		</form>
	</div>
</div>
<a href="#removePammsModal" role="button" data-toggle="modal" class="remove-change-pamms"></a>


<div id="modal_success_new" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span id="textChange" style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"></span>
	</div>
</div>
<a href="#modal_success_new" role="button" data-toggle="modal" id='modal_success_btn_new'></a>


<div id="modal_success_new_default" class="modal hide fade" style='width: 900px;left:36%;'>
	<div class="modal-body" style='border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Купить стандартные ПАММ портфели </span>
		<span style='position: relative; top: 45%;'>
		<span id="textChange" style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"></span>
		<div style="margin-top:36px;">
		<table style="width:100%;margin-top:18px;" class="alert-buy">
			<thead>
				<tr>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PAMM_NAME']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PROFIT_MONTH']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PROFIT_TOTAL']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PAMM_TAX']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PAMM_PRICE']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PAMM_COUNT']; ?>
</p>
					</td>
					<td>
						<p><?php echo $this->_tpl_vars['LANG']['PAMM_ACTION']; ?>
</p>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['pammDefault']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['res']):
?>
					<tr>
						<td>
							<p><?php echo $this->_tpl_vars['res']['title']; ?>
</p>
						</td>
						<td>
							<p><?php echo $this->_tpl_vars['res']['profitPerMonth']; ?>
</p>
						</td>
						<td>
							<p><?php echo $this->_tpl_vars['res']['profitTotal']; ?>
</p>
						</td>
						<td>
							<p><?php echo $this->_tpl_vars['res']['tax']; ?>
</p>
						</td>
						<td>
							<p><?php echo $this->_tpl_vars['res']['price']; ?>
</p>
						</td>
						<td>
							<p><?php echo $this->_tpl_vars['res']['count']; ?>
</p>
						</td>
						<td>
							<a data-count="<?php echo $this->_tpl_vars['res']['count']; ?>
" data-type="<?php echo $this->_tpl_vars['res']['type']; ?>
" data-price="<?php echo $this->_tpl_vars['res']['price']; ?>
" data-name="<?php echo $this->_tpl_vars['res']['title']; ?>
" data-id="<?php echo $this->_tpl_vars['res']['id']; ?>
" class="buy">Купить</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
		</table>
		</div>
	</div>
</div>
<a href="#modal_success_new_default" role="button" data-toggle="modal" id='modal_success_btn_new_default'></a>


<div id="modal_success_buy" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<span class="message-title"> Купить - <span></span></span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
		<form method="post">
			<table style="margin:auto;">
				<thead>
					<tr>
						<td colspan="2">
							Количество
						</td>
					</tr>
				</thead>
				<thead>
					<tr>
						<td colspan="2">
							<input type="number" class="pamm-input" min="1" name="count" />
						</td>
					</tr>
					<?php echo smarty_function_captcha2(array(), $this);?>

					<tr style="position:relative;top:10px;">
						<td colspan="2">
							<input type="submit" value="Купить" style="margin-top:10px;" />
						</td>
					</tr>
				</thead>
			</table>
			<input type="hidden" name="id" />
			<input type="hidden" name="buyDef" value="1" />
			<input type="hidden" name="type" />
		</form>
	</div>
</div>
<a href="#modal_success_buy" role="button" data-toggle="modal" id='modal_success_btn_buy'></a>
<?php echo '
	<script>
		$(document).ready(function(){
			//повідомлення про виконання операцій
			var hash = document.location.hash;
			if(hash == \'#successBuy\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Покупка успешно выполнена");
				document.location.hash = "";
			}
			if(hash == \'#successChange\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Статус успешно изменен");
				document.location.hash = "";
			}
			if(hash == \'#successRemove\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Портфель убран из продажи");
				document.location.hash = "";
			}
			if(hash == \'#successExchange\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Портфель выставлен на биржу");
				document.location.hash = "";
			}
			if(hash == \'#errorCaptcha\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Неверный код с картинки");
				document.location.hash = "";
			}
			if(hash == \'#successServise\'){
				$("#modal_success_btn_new").click();
				$(\'#textChange\').text("Портфель продан сервису");
				document.location.hash = "";
			}
			$(\'#btnCnl\').on(\'click\', function(){
				$(\'button.close\').click();
			});
		})
		
		//продаж паммів
		$(\'.sell-pamm\').on(\'click\', function(){
			$(\'input[name="count"].count\').attr(\'max\',$(this).attr(\'data-count\'));
			$(\'input[name="id"]\').val($(this).attr(\'data-id\'));
			$(\'input[name="count"]\').val(\'1\');
			$(\'input.price\').val(\'0.01\');
			$(\'#exchange, #service\').slideUp(\'fast\');
			$(\'#modal_success_btn\').click();
		});
		//продаж на біржу
		$(\'#exchangeLink\').on(\'click\', function(){
			$(\'button.close\').click();
			setTimeout(function(){
				$(\'#modal_success_btn_exchange\').click();
			},1000);
		});
		//продаж на сервіс
		$(\'#serviceLink\').on(\'click\', function(){
			$(\'button.close\').click();
			setTimeout(function(){
				$(\'#modal_success_btn_service\').click();
			},1000);
		});

		$(\'input[value="cancel"]\').on(\'click\', function(){
			$(this).parent().slideUp(\'fast\');
		})
		/*$(\'.exchange-link\').on(\'click\', function(){
			location=""+$(this).attr(\'data-id\')+"/pamminfo.html";
		});
		
		$(\'.service-link\').on(\'click\', function(){
			$(\'#modal_success_btn_new_default\').click();
		});*/
		
		$(\'.buy\').on(\'click\', function(){
			$(\'.close\').click();
			setTimeout(1000);
			$(\'.limit-message\').remove();
			$(\'input[type="number"]\').val("1");
			$(\'input[name="id"]\').attr(\'value\',$(this).attr(\'data-id\'));
			$(\'input[name="type"]\').attr(\'value\',$(this).attr(\'data-type\'));
			var userCash=parseInt($(\'input[name="myCash"]\').val());
			var maxItemBuy=(userCash/100)/parseFloat($(this).attr(\'data-price\').trim());
			
			if(maxItemBuy.toString().indexOf(\'.\')!=-1){
				var roundCount=parseInt(maxItemBuy.toString().substring(0,maxItemBuy.toString().indexOf(\'.\')));
			}else{
				var roundCount=parseInt(maxItemBuy);
			}
			
			var count=parseInt($(this).attr(\'data-count\').trim());
			//обмеження по наявності коштів
			if(roundCount<count){
				count=roundCount;
				$(\'input[name="count"]\').after("<p class=\'limit-message\'>Ваших средств достаточно на "+count+" штук.</p>");
			}
				
			
			$(\'input[type="number"]\').attr(\'max\',count);
			$("#modal_success_btn_buy").click();
			$(\'#modal_success_buy .message-title > span\').text($(this).attr(\'data-name\'));
			
			
		});
		//$(\'.last-import:last\').css(\'border-bottom\',\'1px solid\');
	</script>
'; ?>
<?php /* Smarty version 2.6.28, created on 2015-12-30 17:01:10
         compiled from com_users_service.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_service.tpl', 1, false),)), $this); ?>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/datepicker/jquery-1.10.2.js'), $this);?>

<?php echo '
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == \'#save_paym_sys\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Номер счета сохранен.");
				document.location.hash = "";
			}
			if(hash == \'#min_money\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Нарушено условие минимальной суммы.");
				document.location.hash = "";
			}
			if(hash == \'#limit\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Cумма средств превышает лимит обмена.");
				document.location.hash = "";
			}
			if(hash == \'#critical_error\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Вы пытаетесь обмануть систему перевода средств. При повторном обмане Ваш аккаунт будет заблокирован.");
				document.location.hash = "";
			}
		});
	</script>
<style>
.priem_inf_each {
    height: 80px;
}
</style>
'; ?>

<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;padding-top: 10px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Обмен через сервис
	</h4>
	<div id='underline'><span></span></div>
</div>
<div id="big_payment_system">
<?php echo '
<script>
	
</script>
'; ?>

<!-- Платежные данные -->
<div style=" border: 1px solid black; margin-top: 20px; width: 99.8%;" class='kab-block'>
	<div class="user_payment_system">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю</span>
		<ul>
		<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
			<?php if ($this->_tpl_vars['inf_moneys'][$this->_tpl_vars['system']['id']]['score_num'] != '' && $this->_tpl_vars['inf_moneys2'][$this->_tpl_vars['system']['id']]['score_num'] != ''): ?>
			<li data-inf="0" data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
" data-relations="<?php echo $this->_tpl_vars['system']['relations']; ?>
" data-tax="<?php echo $this->_tpl_vars['system']['tax_money']; ?>
">
				<div style="width:105px; margin:0px 10px; float:left;"><img style="float:left;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></div>
				<span style="float:left; text-align: left; padding-left: 0px; width: 105px;"><?php echo $this->_tpl_vars['system']['name']; ?>
</span>
				<label ></label>
				<input type="checkbox" >
				<a style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</ul>
	</div>
	<div class="payment_system">
		<div style="height:40px;">
			<span style="display: block; float: left; padding:10px;">Получаю</span>
			<span style="float:right; margin-right:7px;">Лимит на обмен</span>
		</div>
		<ul>
		<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
			<?php if ($this->_tpl_vars['inf_moneys2'][$this->_tpl_vars['system']['id']]['score_num'] != ''): ?>
			<li data-inf="0" data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
" data-tax="<?php echo $this->_tpl_vars['system']['tax_money']; ?>
">
			
				<div style="width:105px; margin:0px 10px; float:left;"><img style="float:left;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></div>
				<span style="float:left; width: 105px;"><?php echo $this->_tpl_vars['system']['name']; ?>
</span>
				<label class="clicable"></label>
				<input type="checkbox" >
				<span class="money_admin"><?php echo $this->_tpl_vars['system']['money_admin']; ?>
</span><span style="float:left; ">USD</span>
			<a class="clicable" style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</ul>
	</div>
	<!--div class="reserve">
	</div-->
</div>
<form method="post" action="" onsubmit="return validateForm()">
<div style="display:none;" data-id="<?php echo $this->_tpl_vars['id']; ?>
" class="verification"><?php echo $this->_tpl_vars['verification']; ?>
</div>
<input type="hidden" name="opt" value="registration">
<div class="priem_system" >
	<div class="user_s">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю:</span>
		<div>
		
		</div>
	</div>
	<div class="system_s">
		<span style="width:100%; display: block; float: none; padding:10px;">Получаю:</span>
		<div>
		
		</div>
	</div>
</div>

<div class="priem_system" style="margin-top:20px;">
	<div style="padding: 10px;">
		<span >Персональные данные:</span>
	</div>
	<div class="personal_inf">
		<span>Ваше имя:</span>
		<input type="text" name="user_name" value="<?php echo $this->_tpl_vars['infa_user']['nickname']; ?>
">
	</div>
	<div class="personal_inf">
		<span>Ваш E-mail:</span>
		<input type="text" name="user_email" value="<?php echo $this->_tpl_vars['infa_user']['email']; ?>
">
	</div>
</div>

<div class="commission">
	<div>
		<span>Комиссия сервиса(%):</span>
		<input type="text" name="commission" value="0.5" readonly>
	</div>
	<div>
		<span>Использовать скидку:</span>
		<select name="discount" style="margin-top: 7px;">
			<option disabled selected>Виберите скидку</option>
			<?php $_from = $this->_tpl_vars['discounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result']):
?>
				<option value="<?php echo $this->_tpl_vars['result']['id']; ?>
"><?php echo $this->_tpl_vars['result']['discount']; ?>
 %</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
	</div>
	<!--div>
		<span>Всего:</span>
		<input type="text" name="all_money" readonly>
	</div-->
</div>
<div style="text-align: center;">
	<input style="text-transform: uppercase;" id="save_btn" type="submit" value="обменять">
	<input style="color: #fff;text-transform: uppercase;" type="button" onclick="location.href='/users/<?php echo $this->_tpl_vars['nik_user']; ?>
'" id="delbtn2" value="Назад">
</div>
</form>

</div>
<div class="transfer_funds" data-server="<?php echo $this->_tpl_vars['server_name']; ?>
" data-system1="<?php echo $this->_tpl_vars['sys_1']; ?>
" data-system2="<?php echo $this->_tpl_vars['sys_2']; ?>
"></div>


<?php echo '
<style>
	.personal_inf {
		float: left;
		padding: 10px;
		width: 47%;
	}
	.personal_inf span {
	    margin-right: 10px;
	}
	.personal_inf input {
	    padding-left: 3px;
	}
	.commission span {
		width: 160px;
		display: block;
		float: left;
		padding-top: 12px;
	}
</style>
<script>

function validateForm(){
	var foo;
	//var money_admin = $
	if($(\'.user_s .priem_inf_each\').html() == foo && $(\'.system_s .priem_inf_each\').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите платежные системы!");
		return false;
	}
	else if($(\'.user_s .priem_inf_each\').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите систему выплаты!");
		return false;
	}
	else if($(\'.system_s .priem_inf_each\').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Заполните необходимые поля!");
		return false;
	}
	else if($(\'.priem_system input[name="user_email"]\').val() == "" || $(\'.priem_system input[name="user_name"]\').val() == ""){
		$("#modal_success_btn").click();
		$("#text_span").text("Заполните необходимые поля!");
		return false;
	}
	else{
		proverka = 0;
		$(\'.dddddd\').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		$(\'.cccccc\').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		if($(\'.hhhhhhh\').val() == "" || $(\'.hhhhhhh\').val() == "0" || proverka == 1){
			$("#modal_success_btn").click();
			$("#text_span").text("Заполните необходимые поля!");
			return false;
		}
	}
	
	/*var money_qqq = $(\'.hhhhhhh\').val();
	money_qqq = money_qqq.replace(",", \'.\');
	if($(\'input[name="payment_user"]\').attr(\'data-id\') == \'9\'){	//для webmoney   /
		var forma_payment = \'<form id="pay" name="pay" method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp" accept-charset="windows-1251" >\'+
		\'<input type="hidden" name="LMI_PAYMENT_AMOUNT" value="\'+money_qqq+\'" />\'+
		\'<input type="hidden" name="LMI_PAYMENT_DESC" value="тестовый платеж" />\'+
		\'<input type="hidden" name="LMI_PAYMENT_NO" value="1">\'+
		\'<input type="hidden" name="LMI_PAYEE_PURSE" value="Z145179295679" />\'+
		\'<input type="hidden" name="LMI_SIM_MODE" value="0" />\'+ //  параметр отвечяющий за тестовый платеж /
		\'<input type="hidden" name="LMI_SUCCESS_URL" value="http://\'+$(\'.transfer_funds\').attr(\'data-server\')+\'/registration_service.html?money=\'+money_qqq+\'&paym_user=\'+$(\'input[name="payment_user"]\').attr(\'data-id\')+\'&paym_admin=\'+$(\'input[name="payment_system_admin"]\').val()+\'" />\'+
		\'</form>\';
		$(\'.transfer_funds\').html(forma_payment);
		$(\'#pay\').submit();
		return false;
	}
	else if($(\'input[name="payment_user"]\').attr(\'data-id\') == \'13\'){ //   для яндекс.деньги   
		var forma_payment = \'<form id="pay" name="pay" action="https://money.yandex.ru/eshop.xml" method="post"> \'+
		\'<input name="shopId" value="1234" type="hidden"/> \'+
		\'<input name="scid" value="4321" type="hidden"/> \'+
		\'<input name="sum" value="100.50" type="hidden"> \'+
		\'<input name="customerNumber" value="abc000" type="hidden"/> \'+
		\'<input type="hidden" name="shopSuccessURL" value="http://\'+$(\'.transfer_funds\').attr(\'data-server\')+\'/registration_service.html?money=\'+money_qqq+\'&paym_user=\'+$(\'input[name="payment_user"]\').attr(\'data-id\')+\'&paym_admin=\'+$(\'input[name="payment_system_admin"]\').val()+\'" />\'+
		\'<input name="shopSuccessURL" value="PC" type="hidden"/> \'+
		\'</form>\';
		$(\'.transfer_funds\').html(forma_payment);
		$(\'#pay\').submit();
		return false;
	}
	else if($(\'input[name="payment_user"]\').attr(\'data-id\') == \'6\'){
		var forma_payment = \'<form method="get" action="https://w.qiwi.ru/setInetBill_utf.do" target="iframeName" >\'+
		\'<input name="from" value="5794">\'+
		\'<input name="to" value="9151111111">\'+
		\'<input name="summ" value="0.01">\'+
		\'<input name="com" value="test">\'+
		\'<input name="iframe" value="true">\'+
		\'<input type="submit">\'+
		\'</form>\'+
		\'<iframe name="iframeName" frameborder="0" width="550" height="700"></iframe>\';
		$(\'.transfer_funds\').html(forma_payment);
		return false;
	}
	else{}*/
}


	
$(document).ready(function(){
	
	relation_mass = [];
	$(\'.user_payment_system li\').each(function(){
		num = $(this).attr(\'data-id\');
		
		relation_mass[num] = $(this).attr(\'data-relations\');
		$(this).attr(\'data-relations\', "");
		
	});
	relation_mass2 = [];
	'; ?>

	<?php $_from = $this->_tpl_vars['inf_relation']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
	<?php echo 'relation_mass2['; ?>
<?php echo $this->_tpl_vars['tid']; ?>
<?php echo '] = [];'; ?>

		<?php $_from = $this->_tpl_vars['result']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result2']):
?>
			<?php echo 'relation_mass2['; ?>
<?php echo $this->_tpl_vars['tid']; ?>
<?php echo ']['; ?>
<?php echo $this->_tpl_vars['tid2']; ?>
<?php echo '] = \''; ?>
<?php echo $this->_tpl_vars['result2']['min_money']; ?>
<?php echo '\';'; ?>

		<?php endforeach; endif; unset($_from); ?>
	<?php endforeach; endif; unset($_from); ?>
	<?php echo '
	varification = $(\'.verification\').text();
	$(\'.verification\').text(\'\');
	/*--------- клик на системы оплаты юзера ---------*/
	$(\'.user_payment_system a\').click(function(){
		$(this).parent().find(\'label\').click();
	});
	$(\'.payment_system a.clicable\').click(function(){
		$(this).parent().find(\'label.clicable\').click();
	});
	$(\'.user_payment_system label\').click(function(){
		$ttttr = 0;
		$(\'.payment_system li\').each(function(){
			$(this).css("display", "block");
		});
		data_checed = $(this).parent().find(\'input\').prop("checked");
		if(data_checed == false){
			$(this).css(\'background-position\', \'-1px -34px\');
			$(this).parent().find(\'input\').click();
			$(this).parent().addClass(\'active\');
		}
		else{
			$(this).css(\'background-position\', \'-1px -4px\');
			$(this).parent().find(\'input\').click();
			$(this).parent().removeClass(\'active\');
			$ttttr = 1;
		}
		
		user_payment_system_div = "";
		label_id = $(this).parent().attr(\'data-id\');
		$(\'.user_payment_system li\').each(function(){
			if($(this).attr(\'data-id\') != label_id){
				if($(this).hasClass(\'active\')){
					$(this).find(\'label\').css(\'background-position\', \'-1px -4px\');
					$(this).find(\'input\').click();
					$(this).removeClass(\'active\');
				}
			}
		});
		bbb = 0;
		$(\'.user_payment_system input\').each(function(){
			if($(this).prop("checked") == true){
				img_system = $(this).parent().find(\'div\').html();
				text_system = $(this).parent().find(\'div + span\').text();
				payment_user_ = $(this).parent().attr(\'data-id\');
				user_payment_system_div = \'<div class="priem_inf_each"><div style="width:105px; margin:0px 10px; float:left;">\'+img_system+\'</div><input data-id="\'+payment_user_+\'" name="u_system_id[\'+payment_user_+\']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input type="text" style="width: 180px;" class="hhhhhhh" autocomplete="off" data-id="\'+payment_user_+\'" name="payment_user" id="payment_user_\'+payment_user_+\'"><input type="hidden" name="payment_system_user" value="\'+payment_user_+\'"></div>\';
				//alert(user_payment_system_div);
				bbb = 1;
			}
		});
		
		/*                скрипт для ограничения отображения ПС (из БД) при обмене валют              */
		id_prov = $(this).parent().attr(\'data-id\');
		relations = relation_mass[id_prov];
		if(relations && relations != "" && bbb == 1){
			$(\'.payment_system li\').each(function(){
				ffff = relations.indexOf(","+$(this).attr(\'data-id\')+",");
				if(ffff == \'-1\'){
					$(this).css("display", "none");
					$(this).removeClass(\'clicable\');
					$(this).find(\'a\').removeClass(\'clicable\');
					$(this).find(\'label\').removeClass(\'clicable\');


					$(this).find(\'a\').css("cursor", "default");
					$(this).css("opacity", "0.3");
				}
				else{
					$(this).css("display", "block");
					$(this).addClass(\'clicable\');
					$(this).find(\'a\').addClass(\'clicable\');
					$(this).find(\'label\').addClass(\'clicable\');
					$(this).css("opacity", "1");
					$(this).find(\'a\').css("cursor", "pointer");
				}
			});
		}
		else{
			$(\'.payment_system li\').each(function(){
				$(this).addClass(\'clicable\');
				$(this).find(\'a\').addClass(\'clicable\');
				$(this).find(\'label\').addClass(\'clicable\');
				$(this).css("opacity", "1");
				$(this).find(\'a\').css("cursor", "pointer");
			});
		}
		
		
		$(\'.payment_system li.active\').each(function(){
			$(this).find(\'label\').click();
		});
		$(\'.system_s div\').html(\'\');
		//////////////////
		if($ttttr == 1){
			$(\'.payment_system li.active\').each(function(){
				//$(this).find(\'label\').click();
				$(this).removeClass(\'active\');
				$(this).find(\'label\').css("background-position", "-1px -4px");
				$(this).find(\'input[type="checkbox"]\').click();
			});
		}
		////////////////////
		$(\'.user_s div\').html(user_payment_system_div);
	});
	/*--------- клик на системы оплаты общие ---------*/
	$(\'.payment_system label.clicable\').click(function(){
		proverka_id = 0;
		proverka_active = 0;
		math_random = $(this).parent().attr(\'data-tax\');
		id_payment_system = $(this).parent().attr(\'data-id\');
		name_payment_system = $(this).parent().find(\'span\').text();
		$(\'.user_payment_system li\').each(function(){
			if(id_payment_system == $(this).attr(\'data-id\')){proverka_id = 1;}
			if($(this).hasClass(\'active\')){ proverka_active = 1;}
		});
		if(proverka_active == 1){
		if(proverka_id == 1){
			data_checed = $(this).parent().find(\'input\').prop("checked");
			if(data_checed == false){
				$(this).css(\'background-position\', \'-1px -34px\');
				$(this).parent().find(\'input\').click();
				$(this).parent().addClass(\'active\');
			}
			else{
				$(this).css(\'background-position\', \'-1px -4px\');
				$(this).parent().find(\'input\').click();
				$(this).parent().removeClass(\'active\');
			}
			payment_system_div = "";
			// -------------- ограничения для выбора одной ПС --------------
			/*label_id = $(this).parent().attr(\'data-id\');
			$(\'.payment_system li\').each(function(){
				if($(this).attr(\'data-id\') != label_id){
					if($(this).hasClass(\'active\')){
						$(this).find(\'label\').css(\'background-position\', \'-1px -4px\');
						$(this).find(\'input\').click();
						$(this).removeClass(\'active\');
					}
				}
			});*/
			//
			$(\'.payment_system input\').each(function(){
				if($(this).prop("checked") == true){
					var money_admin = $(this).parent().find(\'.money_admin\').text();
					img_system = $(this).parent().find(\'div\').html();
					text_system = $(this).parent().find(\'div + span\').text();
					payment_user_ = $(this).parent().attr(\'data-id\');
					kkk = 0;
					$(\'.system_s .priem_inf_each\').each(function(){
						if($(this).attr(\'data-id\') == payment_user_){ 
							kkk = 1;
							kurs_1 = $(this).find(\'.dddddd\').val();
							mon_1 = $(this).find(\'.cccccc\').val();
						}
					});
					
					bbbb = 
			//  общая комиссия
					kommission = parseFloat(0.5) + parseFloat(math_random);
					kommission = parseFloat(kommission).toFixed(2);
			// можна считать комиссию если 1 ПС
					//$(\'.commission input[name="commission"]\').val(kommission);
					
					value_money = \'\';
					if($(\'.user_s .priem_inf_each\') && $(\'.user_s .priem_inf_each\').find(\'.hhhhhhh\').val() != \'\' ){
						Value_1 = $(\'.user_s .priem_inf_each\').find(\'.hhhhhhh\').val();
						Value_1 = Value_1.replace(",", \'.\');
						procent_standart = Value_1*0.005;
						value_money = Value_1 - (Value_1*math_random*0.01);
						oll_summ = value_money - procent_standart;
			// можна считать деньги если 1 ПС
						//$(\'.commission input[name="all_money"]\').val(oll_summ);
					}
					var opopo = $(\'.user_s input[name="payment_system_user"]\').val();
					var min_summ = \'\';
					if(relation_mass2[opopo][$(this).parent().attr(\'data-id\')]){
						min_summ = \'<span class="min_money">Минимальная сумма: \'+relation_mass2[opopo][$(this).parent().attr(\'data-id\')]+\' USD</span>\'
						//alert(min_summ);
					}
				
					if(kkk != 1){
						payment_system_div += \'<div class="priem_inf_each" data-id="\'+payment_user_+\'"><label style="float:left; margin-right: 7px;">комиссия<br>на вывод(%)</label><input autocomplete="off" class="dddddd" style="width:50px; float:left;margin-top: 5px;" type="text" name="kyrs[\'+payment_user_+\']" data-id="payment_user_\'+payment_user_+\'" value="\'+math_random+\'"  readonly><div style="width:105px; margin:0px 10px; float:left;">\'+img_system+\'</div><input data-id="\'+payment_user_+\'" name="system_id[\'+payment_user_+\']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input style="border: 1px solid #ccc;width: 150px;" type="text" class="cccccc" name="payment_system[\'+payment_user_+\']" data-id="payment_user_\'+payment_user_+\'" readonly value="\'+value_money+\'"><span style="display: block;margin-top: 6px;" class="admin_money">лимит: \'+money_admin+\' USD</span>\'+min_summ+\'<input type="hidden" name="payment_system_admin" value="\'+payment_user_+\'"><div style="border-bottom: 1px dashed #ccc; width: 100%; height: 5px; margin-bottom: 20px; " ></div></div>\';
					//alert(payment_system_div);
					}
					else{
						payment_system_div += \'<div class="priem_inf_each" data-id="\'+payment_user_+\'"><label style="float:left; margin-right: 7px;">комиссия<br>на вывод(%)</label><input autocomplete="off" class="dddddd" value="\'+kurs_1+\'" style="width:50px; float:left;margin-top: 5px;" type="text" name="kyrs[\'+payment_user_+\']" data-id="payment_user_\'+payment_user_+\'"  readonly><div style="width:105px; margin:0px 10px; float:left;">\'+img_system+\'</div><input data-id="\'+payment_user_+\'" name="system_id[\'+payment_user_+\']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input style="border: 1px solid #ccc;width: 150px;" type="text" class="cccccc" value="\'+mon_1+\'" name="payment_system[\'+payment_user_+\']" data-id="payment_user_\'+payment_user_+\'"  readonly ><span  class="admin_money">лимит: \'+money_admin+\' USD</span>\'+min_summ+\'<div style="border-bottom: 1px dashed #ccc; width: 100%; height: 5px; margin-bottom: 20px; "></div></div>\';
					}
				}
			});
			
			$(\'.system_s div\').html(payment_system_div);
		}
		else{
			$("#modal_success_btn").click();
			$("#text_span").html(\'<div style="margin-bottom:10px;">Добавление текущей систимы оплаты(\'+name_payment_system+\')</div><form id="form_paym_syst" name="form_paym_syst" method="post" action=""><div><input type="hidden" name="opt" value="save_paym_sys" /><input type="hidden" name="system_id" value="\'+id_payment_system+\'" /><input type="text" id="score" name="score"> <input type="submit" value="Записать номер счета"></div></form>\');
		}
		}
	});
	
	$(\'.user_s \').on(\'click\', \'input[type="checkbox"]\', function(){
		zzz = $(this).attr(\'data-id\');
		$(\'.user_payment_system li\').each(function(){
			if($(this).attr(\'data-id\') == zzz){ $(this).find(\'label\').click();}
		});
	});
	$(\'.system_s \').on(\'click\', \'input[type="checkbox"]\', function(){
		zzz = $(this).attr(\'data-id\');
		$(\'.payment_system li\').each(function(){
			if($(this).attr(\'data-id\') == zzz){ $(this).find(\'label\').click();}
		});
	});
	
	
	
	$(\'.user_s \').on(\'keyup keydown\', \'input[type="text"]\', function(){
		
		count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, \'\');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, \'\');
		Value_1 = $(this).val();
		Value_1 = Value_1.replace(",", \'.\');
		$(\'.system_s .dddddd\').each(function(){
			ssss = $(this).attr(\'data-id\');
			kurs_num = $(this).val();
			if(kurs_num != \'\' && Value_1 != ""){
				procent_standart = Value_1*0.005;
				result = Value_1 - (Value_1*kurs_num*0.01);
				oll_summ = result - procent_standart;
				oll_summ = parseFloat(oll_summ).toFixed(2);
//$(\'.commission input[name="all_money"]\').val(oll_summ);
				$(\'.system_s .cccccc\').each(function(){
					if(ssss == $(this).attr(\'data-id\')){ 
						$(this).val(result);
						this.value = Math.round(parseFloat(this.value)*100)/100;
					}
				});
			}
			else{
				$(\'.system_s .cccccc\').each(function(){
					if(ssss == $(this).attr(\'data-id\')){ 
						$(this).val(\'\');
					}
				});
			}
		});
		
	});
	
	if($(\'.transfer_funds\').attr(\'data-system1\') != "" && $(\'.transfer_funds\').attr(\'data-system2\') != ""){
		var syst_1 = $(\'.transfer_funds\').attr(\'data-system1\');
		var syst_2 = $(\'.transfer_funds\').attr(\'data-system2\');
		$(\'.user_payment_system li\').each(function(){
			if($(this).attr(\'data-id\') == syst_1){
				$(this).find(\'a\').click();
			}
		});
		$(\'.payment_system li\').each(function(){
			if($(this).attr(\'data-id\') == syst_2){
				$(this).find(\'a\').click();
			}
		});
	}
	
});
</script>
'; ?>


<?php if ($this->_tpl_vars['zayavka']): ?>
<div class="inf_birza" data-id="<?php echo $this->_tpl_vars['zayavka']['id_syst_usr']; ?>
" data-mon="<?php echo $this->_tpl_vars['zayavka']['money_off']; ?>
">
<?php $_from = $this->_tpl_vars['zayavka']['massive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['infa']):
?>
<div data-id="<?php echo $this->_tpl_vars['infa']['id_sys']; ?>
" data-sum="<?php echo $this->_tpl_vars['infa']['money_sys']; ?>
"></div>
<?php endforeach; endif; unset($_from); ?>
</div>
<?php echo '
<script>
	$(document).ready(function(){
		$(\'.user_payment_system li\').each(function(){
			if($(\'.inf_birza\').attr(\'data-id\') == $(this).attr(\'data-id\')){
				$(this).find(\'a\').click();
			}
		});
		
		$(\'.hhhhhhh\').val($(\'.inf_birza\').attr(\'data-mon\'));
		
		$(\'.inf_birza div\').each(function(){
			id_ee = $(this).attr(\'data-id\');
			$(\'.payment_system li\').each(function(){
				if($(this).attr(\'data-id\') == id_ee){
					$(this).find(\'a\').click();
				}
			});
		});
		
		$(\'.inf_birza div\').each(function(){
			money_ee = $(this).attr(\'data-sum\');
			id_ee = $(this).attr(\'data-id\');
			$(\'.system_s .priem_inf_each\').each(function(){
				if($(this).attr(\'data-id\') == id_ee){
					$(this).find(\'.cccccc\').val(money_ee);
				}
			});
		});
	});
</script>
'; ?>

<?php endif; ?>
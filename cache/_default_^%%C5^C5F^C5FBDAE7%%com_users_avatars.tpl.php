<?php /* Smarty version 2.6.28, created on 2016-02-09 18:14:09
         compiled from com_users_avatars.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'com_users_avatars.tpl', 8, false),)), $this); ?>
<div class="con_heading"><?php echo $this->_tpl_vars['LANG']['SELECTING_AVATAR']; ?>
</div>
<div class="con_text"><?php echo $this->_tpl_vars['LANG']['CLICK_ON_AVATAR_TEXT']; ?>
:</div>

<table class="" style="margin-top:15px;margin-bottom:15px;" cellpadding="5" width="100%" border="0">
    <?php $this->assign('col', '1'); ?>
    <?php $_from = $this->_tpl_vars['avatars']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['avatar_id'] => $this->_tpl_vars['avatar']):
?>
        <?php if ($this->_tpl_vars['col'] == 1): ?> <tr> <?php endif; ?>
            <?php echo smarty_function_math(array('equation' => "(x-1)*y + z",'x' => $this->_tpl_vars['page'],'y' => $this->_tpl_vars['perpage'],'z' => $this->_tpl_vars['avatar_id'],'assign' => 'avatar_id'), $this);?>

            <td width="25%" valign="middle" align="center">
                    <a href="/users/<?php echo $this->_tpl_vars['userid']; ?>
/select-avatar/<?php echo $this->_tpl_vars['avatar_id']; ?>
" title="<?php echo $this->_tpl_vars['LANG']['SELECT_AVATAR']; ?>
">
                        <img src="<?php echo $this->_tpl_vars['avatars_dir']; ?>
/<?php echo $this->_tpl_vars['avatar']; ?>
" border="0" />
                    </a>
            </td>
        <?php if ($this->_tpl_vars['col'] == 4): ?> </tr> <?php $this->assign('col', '1'); ?> <?php else: ?> <?php echo smarty_function_math(array('equation' => "x + 1",'x' => $this->_tpl_vars['col'],'assign' => 'col'), $this);?>
 <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>

    <?php if ($this->_tpl_vars['col'] > 1): ?>
        <td colspan="<?php echo smarty_function_math(array('equation' => "x - y + 1",'x' => $this->_tpl_vars['col'],'y' => 4), $this);?>
">&nbsp;</td></tr>
    <?php endif; ?>
</table>

<?php echo $this->_tpl_vars['pagebar']; ?>
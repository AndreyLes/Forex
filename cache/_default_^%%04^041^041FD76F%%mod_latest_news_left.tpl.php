<?php /* Smarty version 2.6.28, created on 2016-02-08 17:50:59
         compiled from mod_latest_news_left.tpl */ ?>
<?php if ($this->_tpl_vars['cfg']['is_pag']): ?>
	<?php echo '
	<script type="text/javascript">
		function conPage(page, module_id){
            $(\'div#module_ajax_\'+module_id).css({opacity:0.4, filter:\'alpha(opacity=40)\'});
			$.post(\'/modules/mod_latest/ajax/latest.php\', {\'module_id\': module_id, \'page\':page}, function(data){
				$(\'div#module_ajax_\'+module_id).html(data);
                $(\'div#module_ajax_\'+module_id).css({opacity:1.0, filter:\'alpha(opacity=100)\'});
			});

		}
    </script>
	'; ?>

<?php endif; ?>
<?php if (! $this->_tpl_vars['is_ajax']): ?><div id="module_ajax_<?php echo $this->_tpl_vars['module_id']; ?>
"><?php endif; ?>

<table id='newstable' >
<tr><td style='text-align:center;font-weight: bold;
font-size: 18px;'>Новости</td></tr>
<?php $_from = $this->_tpl_vars['articles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['aid'] => $this->_tpl_vars['article']):
?>
	<tr>
      <td>
	    <a  href="<?php echo $this->_tpl_vars['article']['url']; ?>
" target="_self"><?php echo $this->_tpl_vars['article']['title']; ?>
</a>
	</td>
    
	</tr>
<?php endforeach; endif; unset($_from); ?>
<tr>
	<td style='text-align:center'><a class='allnews' href='/novosti'>Показать все новости</a></td>
</tr>
</table>
<?php if ($this->_tpl_vars['cfg']['is_pag'] && $this->_tpl_vars['pagebar_module']): ?>
    <div class="mod_latest_pagebar"><?php echo $this->_tpl_vars['pagebar_module']; ?>
</div>
<?php endif; ?>
<?php if (! $this->_tpl_vars['is_ajax']): ?></div><?php endif; ?>
<?php /* Smarty version 2.6.28, created on 2016-02-10 17:49:33
         compiled from com_users_administration.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_administration.tpl', 1, false),array('function', 'add_css', 'com_users_administration.tpl', 2, false),)), $this); ?>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/tabs/jquery.ui.min.js'), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/jquery/tabs/tabs.css'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/datepicker/jquery-1.10.2.js'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/datepicker/jquery-ui.js'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/jquery.maskedinput.js'), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/image_resider/css/imgareaselect-default.css'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => 'includes/image_resider/scripts/jquery.imgareaselect.pack.js'), $this);?>


<?php echo '
	<script type="text/javascript">
		$(function(){$(".uitabs").tabs();});
		$(window).load(function() {
			$(function() {
				$( "#date_priem" ).datepicker({dateFormat: "yy-mm-dd"});
				$( "#date_card" ).datepicker({dateFormat:\'yy/mm\'});
			});
		});	
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == \'#success\'){
				$("#modal_success_btn").click();
				document.location.hash = "";
			}
			if(hash == \'#delete_pc\'){
				$("#modal_success_btn").click();
				$("#text_span").text("ПС удалена");
				document.location.hash = "";
			}
			if(hash == \'#add_new_pc\'){
				$("#modal_success_btn").click();
				$("#text_span").text("ПС создана. Для введения ПС на сайте, проведите настройку в админимтрировании.");
				document.location.hash = "";
			}
			if(hash == \'#error_old_pass\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Старый пароль введен неверно.");
				document.location.hash = "";
			}
			if(hash == \'#PROFILE_SAVED\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Профиль успешно сохранен.");
				document.location.hash = "";
			}
			if(hash == \'#IMAGE_SAVED\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Изображение изменено.");
				document.location.hash = "";
			}
			if(hash == \'#IMAGE_SAVED2\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Изображение добавлено.");
				document.location.hash = "users_pc";
				$(\'a[href="#users_pc"]\').click();
			}
			if(hash == \'#relation_save\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Информация о связях ПС сохранена.");
				document.location.hash = "#relation_pc";
				$(\'a[href="#relation_pc"]\').click();
			}
			if(hash == \'#payment_inf_scores\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Информация о счетах сохранена.");
				document.location.hash = "#payment_inf_score";
				$(\'a[href="#payment_inf_score"]\').click();
			}
			if(hash == \'#save_referal\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Реферальные начисления изменены.");
				document.location.hash = "#fereral";
				$(\'a[href="#fereral"]\').click();
			}
			if(hash == \'#report_delete\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Отчеты удалены.");
				document.location.hash = "";
			}
			if(hash == \'#history_delete\'){
				$("#modal_success_btn").click();
				$("#text_span").text("История удалена.");
				document.location.hash = "";
			}
			if(hash == \'#add_discount\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Скидка добавлена.");
				document.location.hash = "discounts";
				$(\'a[href="#discounts"]\').click();
			}
			if(hash == \'#exchange_delate\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Заявка удалена.");
				document.location.hash = "exchange";
				$(\'a[href="#exchange"]\').click();
			}
			
			
			$(\'#payment_inf .change_img_ps\').click(function(){
				var img = $(this).parent().html();
				$("#modal_success2 .img_priem").html(img);
				$("#modal_success2 .img_priem").find(\'img\').removeAttr(\'style\');
				$("#modal_success2 input[name=\'id_sustem\']").val($(this).attr(\'data-id\'));
				$("#modal_success2 .img_priem .top").remove();
				
				$("#modal_success_btn2").click();
				$(\'#background_modal\').fadeIn();
			});
			$(\'.otm_change_img\').click(function(){
				$(\'.modal-backdrop\').click();
			});
			
			$(\'#add_new_pc\').click(function(){
				$(\'#modal_success_btn11\').click();
			});
			
			$(\'#payment_inf .change_img_ps \').each(function(){
				if($(this).attr(\'src\') == ""){
					$(this).attr(\'src\', \'/images/icons/add.gif\');
				}
			});
			
		});
		
$(document).ready(function (e) {
$(\'#modal_success2\').on(\'click\',\'.td_buttonu\',function(){
	$(\'#uploadimage #file\').click();
});
$("#uploadimage").on(\'submit\',(function(e) {
	e.preventDefault();
	$("#message").empty();
	$.ajax({
		url: "/components/users/ajax/ajax_php_img_pc.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data)   // A function to be called if request succeeds
		{
			
			if(data != \'error\'){
			$(\'#form_upload_photo input[type="submit"]\').removeAttr(\'disabled\');
			$(".img_priem .change_img_ps ").attr(\'src\',data);
			$(\'#form_upload_photo input[name="file_name"]\').val(data);
			blabla();
			}
			else{
				$(\'#avatarka #message\').text(\'Данное изображение не подходит поформату или размеру.\');
				console.log(data);
				console.log(\'Данное изображение не подходит по формату или размеру.\');
				$(\'#form_upload_photo input[type="submit"]\').attr(\'disabled\',\'disabled\');
			}
		}
	});
}));

// Function to preview image after validation
$(function() {
$("#file").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$(\'.img_priem .change_img_ps \').attr(\'src\',\'noimage.png\');
$("#message").html("<p id=\'error\'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id=\'error_message\'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
	$("#file").css("color","green");
	$(\'.img_priem .change_img_ps \').attr(\'src\', e.target.result);
	$(\'#uploadimage .submit\').click();
};
$(\'#background_modal\').click(function(){
	$(\'#background_modal\').fadeOut();
	if($(\'div\').is(\'.imgareaselect-outer\')){
		$(\'.img_priem img.change_img_ps \').imgAreaSelect({
			remove: true
		});
	}
	$(\'#modal_success2 .close\').click();
});
$(\'#modal_success2 .close, .otm_change_img, .modal-backdrop\').click(function(){
	$(\'#background_modal\').fadeOut();
	if($(\'div\').is(\'.imgareaselect-outer\')){
		$(\'.img_priem img.change_img_ps \').imgAreaSelect({
			remove: true
		});
	}
});
});
function blabla(){
	$(\'.img_priem img.change_img_ps \').imgAreaSelect({
		x1: 0, y1: 0, x2: 39, y2: 39,
		maxWidth: 105, maxHeight: 40,			
		onSelectEnd: function (img, selection) {
			$(\'input[name="x1"]\').val(selection.x1);
			$(\'input[name="y1"]\').val(selection.y1);
			$(\'input[name="x2"]\').val(selection.x2);
			$(\'input[name="y2"]\').val(selection.y2);            
		}			
	});
}
function validateXY(){
	var x1 = $(\'#form_upload_photo input[name="x1"]\').val();
	var x2 = $(\'#form_upload_photo input[name="x2"]\').val();
	var y1 = $(\'#form_upload_photo input[name="y1"]\').val();
	var y2 = $(\'#form_upload_photo input[name="y2"]\').val();
	var file_name = $(\'#form_upload_photo input[name="file_name"]\').val();
	if(x1 == "" || x2 == "" || y1 == "" || y2 == "" || file_name == "" || x1 == x2 || y1 == y2){
		return false;
	}
};
		
	</script>
	
'; ?>


<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div id="modal_success11" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
		<div><form enctype="multipart/form-data" method="post" id='form_add_pc' onsubmit="return validationAddPC()">
			<p>Добавление новой ПС</p>
			<div style="float: left; margin-top: 30px; margin-left: 60px;">
				<input type="hidden" name="opt" value="add_new_pc">
				<span>Введите название ПС <input type="text" name="name_pc"></span>
			</div>
			<div style="width: 310px; height: 30px; float: right; margin: 50px 105px 10px 0;">
				<input style="margin-right:25px;" type="submit" value="Добавить">
				<input type="button" value="Отменить" class="otm_change_img">
			</div>
			</form>
		</div>
	</div>
</div>
<a href="#modal_success11" role="button" data-toggle="modal" id='modal_success_btn11'></a>

<div id="modal_success2" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
		<div>
		<div class="img_priem"></div>
		<div style="display:none;">
		<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
			<div id="selectImage">
			<input type="file" name="file" id="file" required />
			<input style="display:none;" type="submit" value="Upload" class="submit" />
			</div>
		</form>
		<h4 id='loading' ></h4>
		<div id="message"></div>
		</div>
		<form id="form_upload_photo" enctype="multipart/form-data" method="post" onsubmit="return validateXY();">
			<div style="float: left; margin-top: 30px; margin-left: 25px;">
				<input type="hidden" name="opt" value="change_img_ps">
				<input name="upload" type="hidden" value="1"/>
				<input type="hidden" name="id_sustem">
				<input style="display:none;" type="file" name="imageps" id="change_image">
				<div class="td_buttonu"><a>Выбрать изображение</a></div>
			</div>
			<input type="hidden" name="x1" value="" />
			<input type="hidden" name="y1" value="" />
			<input type="hidden" name="x2" value="" />
			<input type="hidden" name="y2" value="" />
			<input type="hidden" name="file_name" value="" />
			<div style="width: 500px; height: 30px; float: right; margin: 25px 10px 25px 0px;">
				<div style="murgin-bottom:10px;">Рекомендовано изображение должно быть не больше 300х200 px</div>
				<input style="margin-right:25px;" type="submit" name="submit" disabled value="Изменить изображение">
				<input type="button" value="Отменить" class="otm_change_img">
			</div>
		</form>
		</div>
	</div>
</div>
<a href="#modal_success2" role="button" data-toggle="modal" id='modal_success_btn2'></a>
<div id='usr'>
<div class="head_edit"><?php echo $this->_tpl_vars['LANG']['ADMINISTRATION']; ?>
</div>
<div id="profiletabs" class="uitabs">
    <ul id="tabs">
        <li><a href="#payment_inf"><span><?php echo $this->_tpl_vars['LANG']['PAYMENT_INFORMATION']; ?>
</span></a></li>
		<li><a href="#payment_inf_score"><span><?php echo $this->_tpl_vars['LANG']['PAYMENT_INFORMATION_SCORE']; ?>
</span></a></li>
		<li><a href="#relation_pc"><span><?php echo $this->_tpl_vars['LANG']['RELATION_PC']; ?>
</span></a></li>
		<li><a href="#verification"><span><?php echo $this->_tpl_vars['LANG']['VERIFICATION']; ?>
</span></a></li>
		<li><a href="#history"><span><?php echo $this->_tpl_vars['LANG']['HISTORY_ALL']; ?>
</span></a></li>
		<li><a href="#service"><span><?php echo $this->_tpl_vars['LANG']['EXCHANGE_SERVICE']; ?>
</span></a></li>
		<li><a href="#garant"><span><?php echo $this->_tpl_vars['LANG']['EXCHANGE_GARANT']; ?>
</span></a></li>
		<li><a href="#report"><span><?php echo $this->_tpl_vars['LANG']['REPORT']; ?>
</span></a></li>
		<li><a href="#exchange"><span><?php echo $this->_tpl_vars['LANG']['DELATE_APPLICATION']; ?>
</span></a></li>
		<li><a href="#discounts"><span><?php echo $this->_tpl_vars['LANG']['DISCOUNT']; ?>
</span></a></li>
		<li><a href="#users_pc"><span><?php echo $this->_tpl_vars['LANG']['USERS_PC']; ?>
</span></a></li>
		<li><a href="#fereral"><span><?php echo $this->_tpl_vars['LANG']['REFERAL']; ?>
</span></a></li>
		<li><a href="#control_pass"><span><?php echo $this->_tpl_vars['LANG']['CONTROL_PASS']; ?>
</span></a></li>
	</ul>
	<form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="opt" value="save" />
        <!-- платежные данные -->
		<div id="payment_inf">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['PAYMENT_INFORMATION']; ?>

						<div style="float:right;"class="td_buttonu"><a style="padding: 4px 10px;" id="add_new_pc">Добавление новой ПС</a></div>
					</h4>
				</div>
				
			</div>
			<table  border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;width: 100%;'>
			<thead>
				<tr class='info'>
						<td  style="text-align: center;">
							<span >Название ПС</span><br />
						</td>
						<td style="text-align: center;" class="prosto_class">
							<span>Средства<br>админа</span>
						</td>
						<td style="text-align: center;">
							<span>Отображать ПС</span>
						</td>
						<td style="text-align: center;">
							<span>Налог ПС</span>
						</td>
						<td style="text-align: center;">
							<span>Счет ПС</span>
						</td>
						<td style="text-align: center;">
							<span><?php echo $this->_tpl_vars['LANG']['PAMM_ACTION']; ?>
</span>
						</td>
						<!--td style="text-align: center;">
							<span>ID ПС</span>
						</td>
						<td style="text-align: center; width:100px;">
							<span>Открытые<br>ПС для<br>'стандарт'</span>
						</td-->
					</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
					<tr class='info'>
						<td valign="top">
							<div style="width:105px; border:none !important; float:left;text-align: center;" data-inf="Кликните по картинке, чтобы изменить ее." data-class="top" class="hint_img"><img data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
"   class="change_img_ps " style="max-width:105px; height:40px;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></div>
							<input style=" width: 150px !important; float:left;margin-left:5px; padding-left:5px;margin-top: 9px;" name="name[<?php echo $this->_tpl_vars['system']['id']; ?>
]" id="name[$system.id]" type="text" class="text-input" value="<?php echo $this->_tpl_vars['system']['name']; ?>
"/>
						</td>
						<td style="text-align:center;"class="prosto_class">
							<input type="text" name="money_admin[<?php echo $this->_tpl_vars['system']['id']; ?>
]" id="money_admin_<?php echo $this->_tpl_vars['system']['id']; ?>
" class="text-input" style="width: 70px !important; text-align: center;" value="<?php echo $this->_tpl_vars['system']['money_admin']; ?>
">
						</td>
						<td >
							<input type="checkbox" name="on_off[<?php echo $this->_tpl_vars['system']['id']; ?>
]" <?php if ($this->_tpl_vars['system']['select'] == '1'): ?>checked<?php endif; ?>>
							<label class="clicable_label" <?php if ($this->_tpl_vars['system']['select'] == '1'): ?>style="background-position: -1px -34px;"<?php else: ?>style="background-position: -1px -4px;"<?php endif; ?>></label>
						</td>
						<td width="100px;" style="text-align:center;">
							<input type="text" name="tax_money[<?php echo $this->_tpl_vars['system']['id']; ?>
]" id="tax_money_<?php echo $this->_tpl_vars['system']['id']; ?>
" class="text-input" style="width: 60px !important; text-align: center;" value="<?php echo $this->_tpl_vars['system']['tax_money']; ?>
">
						</td>
						<td style="text-align: center;">
							<input data-number='<?php echo $this->_tpl_vars['system']['number_score']; ?>
' data-before="<?php echo $this->_tpl_vars['system']['before_score']; ?>
" data-after="<?php echo $this->_tpl_vars['system']['after_score']; ?>
" name="peyment[<?php echo $this->_tpl_vars['system']['id']; ?>
]" id="peyment[$system.id]" type="text" class="text-input score_num" style=" width: 150px !important; " value="<?php echo $this->_tpl_vars['inf_moneys'][$this->_tpl_vars['system']['id']]['score_num']; ?>
"/>
						</td>
						<td style="text-align: center;" class="td_buttonu">
							<a class="delete_pc" data-id="<?php echo $this->_tpl_vars['system']['id']; ?>
" data-name="<?php echo $this->_tpl_vars['system']['name']; ?>
">Удалить</a>
						</td>
						<!--td style="text-align:center;">
							<span><?php echo $this->_tpl_vars['system']['id']; ?>
</span>
						</td>
						<td>
							<input title='<?php echo $this->_tpl_vars['LANG']['RELATIONS_HOVER']; ?>
' type="text" name="relations[<?php echo $this->_tpl_vars['system']['id']; ?>
]" id="relations_<?php echo $this->_tpl_vars['system']['id']; ?>
" class="text-input" style="width: 100px !important; text-align: center;" value="<?php echo $this->_tpl_vars['system']['relations']; ?>
">
						</td-->
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
			</table>
		<div class="save_profile" style="margin-top: 12px;text-align:center" id="submitform">
			<button name="save"  type="submit" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
		</div>
		</div>
	</form>
<div id="modal_success10" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Вы точно хотите удалить ПС <span id="del_pc_name" style="color:red;"></span>?
			<p style="text-align:center;">
			<form method="post" id="form_del_pc">
			<input type="hidden" name="opt" value="delete_ps">
			<input type="hidden" name="id_ps" value="">
			<div class="td_buttonu"><input style="margin-right: 25px;" type="submit" value="Удалить" ><input type="button" value="Отменить" class="otm_del_pc"></div>
			</form>
			</p>
		</span>
	</div>
</div>
<a href="#modal_success10" role="button" data-toggle="modal" id='modal_success_btn10'></a>
	<?php echo '
	<script>
	
		$(document).ready(function(){
			$(\'.otm_del_pc\').click(function(){
				$(\'#modal_success10 .close\').click();
			});
			$(\'.delete_pc\').click(function(){
				$(\'#form_del_pc input[name="id_ps"]\').val($(this).attr(\'data-id\'));
				$(\'#del_pc_name\').text($(this).attr(\'data-name\'));
				$(\'#modal_success_btn10\').click();
			});
			$(\'#payment_inf .score_num\').each(function(){
				length_number = $(this).attr(\'data-number\');
				ghghg = "";
				for(i=0; i<length_number;i++){
					ghghg += "9";
				}
				$(this).attr("placeholder", $(this).attr(\'data-before\')+ghghg+$(this).attr(\'data-after\'));
				$(this).mask($(this).attr(\'data-before\')+ghghg+$(this).attr(\'data-after\'));
			});
		});
	</script>
	'; ?>

	<!-- информация про вид номера кошелька -->
		<div id="payment_inf_score">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['PAYMENT_INFORMATION_SCORE']; ?>

					</h4>
				</div>
			</div>
			<form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
			<input type="hidden" name="opt" value="save_inf_score">
			<table  border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
			<thead>
				<tr class='info'>
					<td  style="text-align: center;">
						<span >Название ПС</span><br />
					</td>
					<td style="text-align: center;">
						<span>приставка слева<br>от номера</span>
					</td>
					<td style="text-align: center;">
						<span>к-во цифр<br>(без приставки)</span>
					</td>
					<td style="text-align: center;">
						<span>приставка справа<br>от номера</span>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['payments2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
					<tr class='info'>
						<td >
							<div style="width:105px; float:left;"><img style="max-width:105px; height:40px;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"></div>
							<span style="margin-left:10px; display:table; margin-top:10px;"><?php echo $this->_tpl_vars['system']['name']; ?>
</span>
						</td>
						<td width="150px;">
							<input style="width:100px !important;" type="text" name="before_score[<?php echo $this->_tpl_vars['system']['id']; ?>
]" value="<?php echo $this->_tpl_vars['system']['before_score']; ?>
">
						</td>
						<td width="150px;" style="text-align:center;">
							<input style="width:100px !important;" type="text" name="number_score[<?php echo $this->_tpl_vars['system']['id']; ?>
]" value="<?php echo $this->_tpl_vars['system']['number_score']; ?>
">
						</td>
						<td width="150px;" style="text-align:center;">
							<input style="width:100px !important;" type="text" name="after_score[<?php echo $this->_tpl_vars['system']['id']; ?>
]" value="<?php echo $this->_tpl_vars['system']['after_score']; ?>
">
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
			</table>
		<div class="save_profile" style="margin-top: 12px;text-align:center" id="submitform">
			<button name="save_inf_score"  type="submit" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
		</div>
		</form>
		</div>
	
	<!-- информация про Связь между ПС  -->
		<div id="relation_pc">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['RELATION_PC']; ?>
  (Если не выбрана ни одна ПС, для обмена будут доступны все)
					</h4>
				</div>
			</div>
			<form id="" name="" enctype="multipart/form-data" method="post" action="" onsubmit="return zapus_status();">
				<input type="hidden" name="opt" value="relation_save">
				<div class="save_profile" style="margin-top: 12px;text-align:center" id="submitform">
					<button name="save_inf_score"  type="submit" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
				</div>
				<table class="relation_pc" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px; width:100%'>
				<?php $_from = $this->_tpl_vars['payments2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system']):
?>
					<tr class='info'>
						<td >
							<div class="top_block zebra_table">
								<div><span><?php echo $this->_tpl_vars['system']['id']; ?>
</span></div>
								<div><img style="max-width:105px; height:40px;padding-right: 20px;" src="<?php echo $this->_tpl_vars['system']['img']; ?>
"><span><?php echo $this->_tpl_vars['system']['name']; ?>
</span></div>
							</div>
							<div class="center_block">
							<?php if ($this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']]): ?>
								<?php $_from = $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
									<?php if ($this->_tpl_vars['result']['status'] != '0'): ?>
									<div>
										<ul style="list-style:none; padding:0;">
										<li><img style="max-width:105px; height:40px;" src="<?php echo $this->_tpl_vars['result']['relation_img']; ?>
"></li>
										<li>Курс: <?php echo $this->_tpl_vars['result']['kurs']; ?>
</li>
										<li>Комиссия: <?php echo $this->_tpl_vars['result']['commission']; ?>
</li>
										<li>Мин. сумма: <?php echo $this->_tpl_vars['result']['min_money']; ?>
</li>
										</ul>
									</div>
									<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
							<?php endif; ?>
								<div class="td_buttonu"><a>Редактировать</a></div>
							</div>
							<div class="list_block">
								<table border="0" cellspacing="0" cellpadding="5">
								<thead>
								<tr style="height: 50px; font-size:17px !important;" class='info'>
									<td>ПС</td>
									<td>Курс</td>
									<td>Комиссия</td>
									<td>Мин. сумма</td>
									<td>Статус</td>
								</tr>
								</thead>
								<tbody>
								<?php if ($this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']]): ?>
								<?php $_from = $this->_tpl_vars['payments2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid3'] => $this->_tpl_vars['result2']):
?>
									<?php if ($this->_tpl_vars['result2']['id'] != $this->_tpl_vars['system']['id']): ?>
										<tr>
										<?php if ($this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]): ?>
										<td><img style="max-width:105px;height:40px;" src="<?php echo $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['relation_img']; ?>
"></td>
										<td><input type="text" name="kurs[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" value="<?php echo $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['kurs']; ?>
"></td>
										<td><input type="text" name="commission[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" value="<?php echo $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['commission']; ?>
"></td>
										<td><input type="text" name="min_money[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" value="<?php echo $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['min_money']; ?>
"></td>
										<td><input type="hidden" name="status_val[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ><input type="checkbox" name="status[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" <?php if ($this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['status'] == '1'): ?>checked<?php endif; ?> value="<?php echo $this->_tpl_vars['inf_relation'][$this->_tpl_vars['tid']][$this->_tpl_vars['tid3']]['status']; ?>
"><label class="2clicable_label2"></label></td>
										<?php else: ?>
										<td><img style="max-width:105px;height:40px;" src="<?php echo $this->_tpl_vars['result2']['img']; ?>
"></td>
										<td><input type="text" name="kurs[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="text" name="commission[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="text" name="min_money[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="hidden" name="status_val[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ><input type="checkbox" name="status[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ><label class="2clicable_label2"></label></td>
										</tr>
										<?php endif; ?>
									<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
								<?php else: ?>
								<?php $_from = $this->_tpl_vars['payments2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid3'] => $this->_tpl_vars['result2']):
?>
									<?php if ($this->_tpl_vars['result2']['id'] != $this->_tpl_vars['system']['id']): ?>
										<tr>
										<td><img style="max-width:105px;height:40px;" src="<?php echo $this->_tpl_vars['result2']['img']; ?>
"></td>
										<td><input type="text" name="kurs[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="text" name="commission[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="text" name="min_money[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ></td>
										<td><input type="hidden" name="status_val[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ><input type="checkbox" name="status[<?php echo $this->_tpl_vars['system']['id']; ?>
][<?php echo $this->_tpl_vars['result2']['id']; ?>
]" ><label class="2clicable_label2"></label></td>
										</tr>
									<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
								<?php endif; ?>
								</tbody>
								</table>
								<div class="td_buttonu"><a style='width: 130px;margin: 10px auto;'>Свернуть</a></div>
							</div>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
				</table>
				<div class="save_profile" style="margin-top: 12px;text-align:center" id="submitform">
					<button name="save_inf_score"  type="submit" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
				</div>
			</form>
		</div>
<?php echo '
<script>
	function zapus_status(){
		$(\'.relation_pc .list_block input[type="checkbox"]\').each(function(){
			if($(this).prop("checked") == true){
				$(this).parent().find(\'input[type="hidden"]\').val(\'1\');
			}
			else{
				$(this).parent().find(\'input[type="hidden"]\').val(\'0\');
			}
		});
	}
	$(document).ready(function(){
		$(\'.relation_pc .center_block .td_buttonu a\').click(function(){
			if ($(this).parent().parent().parent().find(\'.list_block\').is(":hidden")) {
				$(this).parent().parent().parent().find(\'.list_block\').slideDown("slow");
				$(this).text(\'Свернуть\');
			} else {
				$(this).parent().parent().parent().find(\'.list_block\').slideUp("slow");
				$(this).text(\'Редактировать\');
			}
		});
		$(\'.relation_pc .list_block .td_buttonu a\').click(function(){
			if ($(this).parent().parent().parent().find(\'.list_block\').is(":hidden")) {
				$(this).parent().parent().parent().find(\'.list_block\').slideDown("slow");
			} else {
				$(this).parent().parent().parent().find(\'.list_block\').slideUp("slow");
			}
		});
		
		$(\'.2clicable_label2\').each(function(){
			if($(this).parent().find(\'input[type="checkbox"]\').prop("checked") == true){
				$(this).css(\'background-position\', \'-1px -34px\');
			}
		});
		$(\'.2clicable_label2\').click(function(){
			var data_checed = $(this).parent().find(\'input[type="checkbox"]\').prop("checked");
			if(data_checed == false){
				$(this).css(\'background-position\', \'-1px -34px\');
				$(this).parent().find(\'input[type="checkbox"]\').click();
			}
			else{
				$(this).css(\'background-position\', \'-1px -4px\');
				$(this).parent().find(\'input[type="checkbox"]\').click();
			}
			
		});
	});
</script>
'; ?>
		
<!-- вкладка верификации -->
		<div id="verification">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['VERIFICATION_USERS']; ?>

					</h4>
				</div>
			</div>
			<table class="table_verif" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;' id="grid">
			<thead>
				<tr class='info'>
						<th data-type="number" data-name="id" data-click="2" valign="top" style="text-align: center;">
							№(id) ↑↓</th>
						<th data-type="string" data-name="login" data-click="1" style="text-align: center;">
							<?php echo $this->_tpl_vars['LANG']['LOGIN']; ?>
 ↑↓</th>
						<th data-type="string" data-name="name" data-click="1" style="text-align: center;">
							<?php echo $this->_tpl_vars['LANG']['NAME']; ?>
 ↑↓</th>
						<td style="padding-top: 13px;text-align: center;" valign="top">
							<span><?php echo $this->_tpl_vars['LANG']['LVL_VERIFICATION']; ?>
 (1)</span>
						</td>
						<td style="padding-top: 13px;text-align: center;" valign="top">
							<span><?php echo $this->_tpl_vars['LANG']['LVL_VERIFICATION']; ?>
 (2)</span>
						</td>
						<td style="padding-top: 13px;text-align: center;" valign="top">
							<span><?php echo $this->_tpl_vars['LANG']['LVL_VERIFICATION']; ?>
 (3)</span>
						</td>
						<td style="padding-top: 13px;text-align: center;" valign="top">
							<span><?php echo $this->_tpl_vars['LANG']['LVL_VERIFICATION']; ?>
 (4)</span>
						</td>
						<td style="padding-top: 13px;text-align: center;" valign="top">
							<span><?php echo $this->_tpl_vars['LANG']['PAMM_ACTION']; ?>
</span>
						</td>
					</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['systems']):
?>
					<tr class='info verification'>
						<td  class="uder_id" data-uder_id="<?php echo $this->_tpl_vars['systems']['id']; ?>
">
							<?php echo $this->_tpl_vars['systems']['id']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['systems']['login']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['systems']['name']; ?>

						</td>
						<td style="text-align:center;">
							<?php if ($this->_tpl_vars['systems']['verif_1'] != ""): ?>
							<div class="photo">
								<a style="float:left;" href='/images/users/docs/large/<?php echo $this->_tpl_vars['systems']['verif_1']; ?>
' class='photobox cboxElement'><img src="/images/users/docs/small/<?php echo $this->_tpl_vars['systems']['verif_1']; ?>
">
								</a>
							</div>
							<?php else: ?>
							<div class="text">
								Фото<br>отсутствует
							</div>
							<?php endif; ?>
							<div>
								<input type="checkbox" name="verif_1" <?php if ($this->_tpl_vars['systems']['verify_stat'] >= 1): ?>checked<?php endif; ?>>
								<label class="clicable_label" <?php if ($this->_tpl_vars['systems']['verify_stat'] >= 1): ?> style="background-position: -1px -34px;"<?php else: ?>style="background-position: -1px -4px;"<?php endif; ?>></label>
							</div>
						</td>
						<td style="text-align:center;">
							<?php if ($this->_tpl_vars['systems']['verif_2'] != ""): ?>
							<div class="photo">
								<a style="float:left;" href='/images/users/docs/large/<?php echo $this->_tpl_vars['systems']['verif_2']; ?>
' class='photobox cboxElement'><img src="/images/users/docs/small/<?php echo $this->_tpl_vars['systems']['verif_2']; ?>
">
								</a>
							</div>
							<?php else: ?>
							<div class="text">
								Фото<br>отсутствует
							</div>
							<?php endif; ?>
							<div>
								<input type="checkbox" name="verif_2" <?php if ($this->_tpl_vars['systems']['verify_stat'] >= 2): ?>checked<?php endif; ?>>
								<label class="clicable_label" <?php if ($this->_tpl_vars['systems']['verify_stat'] >= 2): ?> style="background-position: -1px -34px;"<?php else: ?>style="background-position: -1px -4px;"<?php endif; ?>></label>
							</div>
						</td>
						<td style="text-align:center;">
							<div class="text">
								Подтверждение<br>по телефону
							</div>
							<div>
								<input type="checkbox" name="verif_3" <?php if ($this->_tpl_vars['systems']['verif_3'] == '1'): ?>checked<?php endif; ?>>
								<label class="clicable_label" <?php if ($this->_tpl_vars['systems']['verif_3'] == '1'): ?> style="background-position: -1px -34px;" <?php else: ?>style="background-position: -1px -4px;"<?php endif; ?>></label>
							</div>
						</td>
						<td style="text-align:center;">
							<div class="text">
								Видео<br>подтверждение
							</div>
							<div>
								<input type="checkbox" name="verif_4" <?php if ($this->_tpl_vars['systems']['verif_4'] == '1'): ?>checked<?php endif; ?>>
								<label class="clicable_label" <?php if ($this->_tpl_vars['systems']['verif_4'] == '1'): ?>style="background-position: -1px -34px;"<?php else: ?>style="background-position: -1px -4px;"<?php endif; ?>></label>
							</div>
						</td>
						<td class="td_buttonu">
							<a class="button_save"><?php echo $this->_tpl_vars['LANG']['SAVENG']; ?>
</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
				</tbody>
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_verif']; ?>
</div>
		</div>
<!-- история -->
		<div id="history">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['HISTORY_DEVELOPMENTS']; ?>

					</h4>
				</div>
			</div>
		
			<table class="table_history" border="0" cellspacing="0" cellpadding="5" id="grid2">
			<thead >
				<tr class='info'>
					<th data-type="number" data-name="id" data-click="2" width="50" style="text-align: center;">№ ↑↓</th>
					<th data-type="string" data-name="login" data-click="1" width="100" style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['LOGIN']; ?>
 ↑↓</th>
					<th data-type="masive" data-name="date" data-click="2" width="180" style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['DATE']; ?>
 ↑↓</th>
					<th data-type="string" data-name="event" data-click="1" width="400" style="padding-top: 13px;text-align: center;" ><?php echo $this->_tpl_vars['LANG']['EVENT']; ?>
 ↑↓</th>
					<td width="130" style="padding-top: 13px;text-align: center;" valign="top" class="td_buttonu">
						<?php if ($this->_tpl_vars['history_s']): ?><a class="button_del_all_hist"><span>Очистить<br>всю историю</span></a><?php endif; ?>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['history_s']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<tr class='info history_table'>
						<td width="50" class="id_history" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

						</td>
						<td width="100">
							<?php echo $this->_tpl_vars['system_s']['user_login']; ?>

						</td>
						<td data-title="<?php echo $this->_tpl_vars['system_s']['seconds']; ?>
" width="180">
							<?php echo $this->_tpl_vars['system_s']['time']; ?>

						</td>
						<td width="400">
							<?php echo $this->_tpl_vars['system_s']['event']; ?>

						</td>
						<td width="130" class="td_buttonu">
							<a class="button_del_one_hist">Удалить</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
				</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_hist']; ?>
</div>
		</div>
<!-- гарант -->
		<div id="garant">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['APPLICATION_GARANT']; ?>

					</h4>
				</div>
			</div>
			
			<table class="table_garant" border="0" cellspacing="0" cellpadding="5" style="margin-top:10px; width: 100%;">
			<thead >
				<tr class='info'>
					<td  style="text-align: center;">
						<span >№</span><br />
					</td>
					<td width="65" style="text-align: center;">
						<span >login<br>(user 1)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >средства<br>что нужно<br>переслать<br>(user 1)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >Номер кошелька(user 1)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >login<br>(user 2)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >средства<br>что нужно<br>переслать<br>(user 2)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >Номер кошелька<br>(user 2)</span><br />
					</td>
					<td  style="text-align: center;">
						<span >Действие</span><br />
					</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['garants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<?php if ($this->_tpl_vars['system_s']['line'] == '1'): ?>
					<tr class='info history_table zebra_table'>
						<td  class="id_garant" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

							<div></div>
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['login_user_1']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['name_sys_1']; ?>
: <?php echo $this->_tpl_vars['system_s']['money_plus_1']; ?>
$
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['score_1']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['login_user_2']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['name_sys_2']; ?>
: <?php echo $this->_tpl_vars['system_s']['money_plus_2']; ?>
$
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['score_2']; ?>

						</td>
						<td class="td_buttonu" style="position:inherit">
							<a class="aaaasssssss"></a>
							<a class="<?php if ($this->_tpl_vars['system_s']['line'] == '0'): ?>line_servisesss<?php else: ?>garant_button<?php endif; ?>">Подтвердить<br>заявку</a>
						</td>
					</tr>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<?php $_from = $this->_tpl_vars['garants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<?php if ($this->_tpl_vars['system_s']['line'] == '0'): ?>
					<tr class='info history_table zebra_table'>
						<td  class="id_garant" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

							<div class="line_servise2"></div>
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['login_user_1']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['name_sys_1']; ?>
: <?php echo $this->_tpl_vars['system_s']['money_plus_1']; ?>
$
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['score_1']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['login_user_2']; ?>

						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['name_sys_2']; ?>
: <?php echo $this->_tpl_vars['system_s']['money_plus_2']; ?>
$
						</td>
						<td>
							<?php echo $this->_tpl_vars['system_s']['score_2']; ?>

						</td>
						<td class="td_buttonu" style="position:inherit">
							<a class="aaaasssssss"></a>
							<a class="<?php if ($this->_tpl_vars['system_s']['line'] == '0'): ?>line_servisesss<?php else: ?>garant_button<?php endif; ?>">Скрыть<br>заявку</a>
						</td>
					</tr>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_garant']; ?>
</div>
		</div>
<!-- сервис -->
		<div id="service">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['APPLICATION_SERVICE']; ?>

					</h4>
				</div>
			</div>
			
			<table class="table_service" border="0" cellspacing="0" cellpadding="5" style="margin-top:10px;    width: 100%;">
			<thead>
				<tr class='info'>
						<td  style="text-align: center;">
							<span >№</span><br />
						</td>
						<td  style="text-align: center;">
							<span>Логин</span>
						</td>
						<td style="text-align:center;">
							<span>Время создания<br>заявки</span>
						</td>
						<td  style="text-align: center;">
							<span>Средства что пришли</span>
						</td>
						<td  style="padding-top: 13px;text-align: center;" >
							<span>Средства что<br>нужно переслать<br>(если несколько<br>ПС выберите<br>сами 1 ПС)</span>
						</td>
						<td  style="padding-top: 13px;text-align: center;" >
							<span>Курс</span>
						</td>
						<td  style="padding-top: 13px;text-align: center;" >
							<span>Номер кошелька</span>
						</td>
						<td  style="padding-top: 13px;text-align: center;"  >
							<span>Действие</span>
						</td>
					</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['servises']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_ss']):
?>
					<?php if ($this->_tpl_vars['system_ss']['line'] == '1'): ?>
					<tr class='info history_table zebra_table'>
						<td class="id_service" data-id="<?php echo $this->_tpl_vars['system_ss']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_ss']['id']; ?>

							<div></div>
							
						</td>
						<td >
							<?php echo $this->_tpl_vars['system_ss']['user_login']; ?>

						</td>
						<td >
							Осталось: <span data-hour="<?php echo $this->_tpl_vars['system_ss']['hour']; ?>
" class="hour"><?php if ($this->_tpl_vars['system_ss']['hour'] < 10): ?>0<?php echo $this->_tpl_vars['system_ss']['hour']; ?>
<?php else: ?><?php echo $this->_tpl_vars['system_ss']['hour']; ?>
<?php endif; ?></span>:<span data-minuts='<?php echo $this->_tpl_vars['system_ss']['minuts']; ?>
' data-id="<?php echo $this->_tpl_vars['system_ss']['id']; ?>
" class="minuts"><?php if ($this->_tpl_vars['system_ss']['minuts'] < 10): ?>0<?php echo $this->_tpl_vars['system_ss']['minuts']; ?>
<?php else: ?><?php echo $this->_tpl_vars['system_ss']['minuts']; ?>
<?php endif; ?></span>
						</td>
						<td >
							<?php echo $this->_tpl_vars['system_ss']['syst_name']; ?>
: <?php echo $this->_tpl_vars['system_ss']['money']; ?>
$<br>
							<span style="color:#777;">номер кошелека:</span><br> <?php echo $this->_tpl_vars['system_ss']['syst_score']; ?>

						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['name_syst']; ?>
</span>: <span><?php echo $this->_tpl_vars['result']['summ']; ?>
</span><br><span></span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['all_kurs']; ?>
</span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['score']; ?>
</span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_buttonu" style="position:inherit">
							<a class="aaaasssssss"></a>
							<a class="servise_button">
							Подтвердить<br>заявку
							</a>
						</td>
					</tr>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<?php $_from = $this->_tpl_vars['servises']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_ss']):
?>
					<?php if ($this->_tpl_vars['system_ss']['line'] == '0'): ?>
					<tr class='info history_table zebra_table'>
						<td class="id_service" data-id="<?php echo $this->_tpl_vars['system_ss']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_ss']['id']; ?>

							<div class="line_servise3"></div>
							
						</td>
						<td >
							<?php echo $this->_tpl_vars['system_ss']['user_login']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_ss']['date']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_ss']['syst_name']; ?>
: <?php echo $this->_tpl_vars['system_ss']['money']; ?>
$
						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['name_syst']; ?>
</span>: <span><?php echo $this->_tpl_vars['result']['summ']; ?>
</span><br><span></span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['all_kurs']; ?>
</span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_li">
							<ul style="list-style:none; padding: 0px;">
							<?php $_from = $this->_tpl_vars['system_ss']['masive']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid2'] => $this->_tpl_vars['result']):
?>
								<li class=""><span><?php echo $this->_tpl_vars['result']['score']; ?>
</span></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
						<td class="td_buttonu" style="position:inherit">
							<a class="aaaasssssss"></a>
							<a class="line_servisess">
							Скрыть<br>заявку
							</a>
						</td>
					</tr>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_serv']; ?>
</div>
		</div>
<!-- отчет -->
		<div id="report">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['HISTORY_DEVELOPMENTS']; ?>

					</h4>
				</div>
			</div>
		
			<table class="table_report" style="width: 100%;" border="0" cellspacing="0" cellpadding="5" id="">
			<thead >
				<tr class='info border_bottom_none'>
					<td data-type="number" data-name="id" data-click="2"  style="text-align: center;">№</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['NAME']; ?>
</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['NAME_PC']; ?>
</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['MONEY_REPORT']; ?>
 (USD)</td>
					<td data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['DATE']; ?>
</td>
					<td data-type="string" data-name="event" data-click="1"  style="padding-top: 13px;text-align: center;" ><?php echo $this->_tpl_vars['LANG']['COMMISSION_PC']; ?>
 (USD)</td>
					<td data-type="string" data-name="event" data-click="1"  style="padding-top: 13px;text-align: center;" ><?php echo $this->_tpl_vars['LANG']['COMMISSION_SERVICE']; ?>
 (USD)</td>
					<td data-type="string" data-name="event" data-click="1"  style="padding-top: 13px;text-align: center;" ><?php echo $this->_tpl_vars['LANG']['PARTNER_OTCHISL']; ?>
 (USD)</td>
					
					<td width="130" style="padding-top: 13px;text-align: center;" valign="top" class="td_buttonu">
						<?php if ($this->_tpl_vars['reports']): ?><a class="button_del_all_report"><span>Удалить<br>все отчеты</span></a><?php endif; ?>
					</td>
				</tr>
				<tr class='info border_top_none'>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_id" value="<?php echo $this->_tpl_vars['search_id']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_name" value="<?php echo $this->_tpl_vars['search_name']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_name_pc" value="<?php echo $this->_tpl_vars['search_name_pc']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_money" value="<?php echo $this->_tpl_vars['search_money']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_date" value="<?php echo $this->_tpl_vars['search_date']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_com_pc" value="<?php echo $this->_tpl_vars['search_com_pc']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_com_ser" value="<?php echo $this->_tpl_vars['search_com_ser']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-id="<?php echo $this->_tpl_vars['id']; ?>
" name="search_com_ot" value="<?php echo $this->_tpl_vars['search_com_ot']; ?>
" style="width:90%; height: 15px;">
					</td>
					<td><a class="sbros_filter" href="administration.html?#report">Сброс<br>фильтров</a></td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['reports']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<tr class='info report_table hint_inf' data-inf="<?php echo $this->_tpl_vars['system_s']['div']; ?>
" data-class="top">
						<td  class="id_report" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['user_login']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['name_pc']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['money']; ?>

						</td>
						<td data-title="<?php echo $this->_tpl_vars['system_s']['seconds']; ?>
" >
							<?php echo $this->_tpl_vars['system_s']['date']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['commission_pc']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['commission_serv']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['commission_partner']; ?>

						</td>
						<td  class="td_buttonu">
							<a class="button_del_one_report" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">Удалить</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_report']; ?>
</div>
		</div>
<!-- удаление заявок на бирже -->
		<div id="exchange">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['HISTORY_DEVELOPMENTS']; ?>

					</h4>
				</div>
			</div>
		
			<table class="table_exchange" border="0" style="width: 100%;" cellspacing="0" cellpadding="5" id="">
			<thead >
				<tr class='info'>
					<td data-type="number" data-name="id" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['number_application']; ?>
</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['NAME']; ?>
</td>
					<td data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['TYPE_PC']; ?>
</td>
					<td  style="padding-top: 13px;text-align: center;" valign="top" class="td_buttonu"><?php echo $this->_tpl_vars['LANG']['PAMM_ACTION']; ?>
</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['exchange_s']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<tr class='info exchange_table'>
						<td class="id_history" data-id="<?php echo $this->_tpl_vars['system_s']['user_id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['user_login']; ?>

						</td>
						<td >
							<?php if ($this->_tpl_vars['system_s']['garant'] == '1'): ?>Гарант<?php elseif ($this->_tpl_vars['system_s']['garant'] == '2'): ?>Внутренняя конвертация<?php elseif ($this->_tpl_vars['system_s']['garant'] == '3'): ?>Гарантийный фонд<?php endif; ?>
						</td>
						<td class="td_buttonu">
							<a class="button_del_one_appl" data-appl="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">Удалить</a>
							
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_exchange']; ?>
</div>
		</div>
<!-- скидки -->
		<div id="discounts">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
											</h4>
				</div>
			</div>
		
			<table class="table_history" style="width:100%;" border="0" cellspacing="0" cellpadding="5" id="">
			<thead >
				<tr class='info'>
					<th data-type="number" data-name="id" data-click="2"  style="text-align: center;">№</th>
					<th data-type="string" data-name="login" data-click="1"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['LOGIN']; ?>
</th>
					<th data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['NAME']; ?>
</th>
					<th data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['FIRST_NAME']; ?>
</th>
					<th data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['EMAIL']; ?>
</th>
					<td  style="padding-top: 13px;text-align: center;" valign="top" class="td_buttonu"><a class="add_all_action">Добавить акцию</a></td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['discounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<tr class='info history_table'>
						<td  class="id_history" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['login']; ?>

						</td>
						<td data-title="<?php echo $this->_tpl_vars['system_s']['seconds']; ?>
" width="180">
							<?php echo $this->_tpl_vars['system_s']['name']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['first_name']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['email']; ?>

						</td>
						<td  class="td_buttonu">
							<a class="button_discount" data-name="<?php echo $this->_tpl_vars['system_s']['name']; ?>
" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">Добавить скидку</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
				
			</table>
			<div class="pagin"><?php echo $this->_tpl_vars['pagination_discount']; ?>
</div>
		</div>
<!-- Пользовательские ПС -->
		<div id="users_pc">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
											</h4>
				</div>
			</div>
		
			<table class="table_users_pc" style="width:100%;" border="0" cellspacing="0" cellpadding="5" id="">
			<thead >
				<tr class='info'>
					<td data-type="number" data-name="id" data-click="2"  style="text-align: center;">№</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;">ID</td>
					<td data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['PAMM_NAME']; ?>
</td>
					<td data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['IMAGE']; ?>
</td>
					<td  style="padding-top: 13px;text-align: center;" valign="top" class="td_buttonu"><?php echo $this->_tpl_vars['LANG']['PAMM_ACTION']; ?>
</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['users_pc']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['system_s']):
?>
					<tr class='info users_pc_table'>
						<td  class="id_history" data-id="<?php echo $this->_tpl_vars['system_s']['id']; ?>
">
							<?php echo $this->_tpl_vars['system_s']['id']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['system_s']['id_syst']; ?>

						</td>
						<td data-title="<?php echo $this->_tpl_vars['system_s']['seconds']; ?>
" width="180">
							<?php echo $this->_tpl_vars['system_s']['name_syst']; ?>

						</td>
						<td class="this_img" data-id="<?php echo $this->_tpl_vars['system_s']['id_syst']; ?>
">
							<?php if ($this->_tpl_vars['system_s']['image']): ?>
							<img data-id="<?php echo $this->_tpl_vars['system_s']['id_syst']; ?>
"  style="max-width:105px; height:40px;" src="<?php echo $this->_tpl_vars['system_s']['image']; ?>
">
							<?php else: ?>
							<span>Изображение отсутствует</span>
							<?php endif; ?>
						</td>
						<td  class="td_buttonu">
							<a class="change_img_user_ps" data-name="<?php echo $this->_tpl_vars['system_s']['name_syst']; ?>
" data-id="<?php echo $this->_tpl_vars['system_s']['id_syst']; ?>
">Добавить изображение</a>
							<a class="del_img_user_pc" data-id="<?php echo $this->_tpl_vars['system_s']['id_syst']; ?>
">Удалить изображение</a>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
				
			</table>
			
		</div>
<!-- Реферальные начисления -->
		<div id="fereral">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						Реферальные начисления
					</h4>
				</div>
			</div>
			<form method="post" id="referal_form">
			<input type="hidden" name="opt" value="referals">
			<table class="table_referals" style="width:100%;" border="0" cellspacing="0" cellpadding="5" id="">
			<thead >
				<tr class='info'>
					<td data-type="number" data-name="id" data-click="2"  style="text-align: center;">ID</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;">Операция</td>
					<td data-type="string" data-name="login" data-click="1"  style="text-align: center;">% для рефера</td>
					<td data-type="masive" data-name="date" data-click="2"  style="text-align: center;"><?php echo $this->_tpl_vars['LANG']['STATUS']; ?>
</td>
				</tr>
			</thead>
			<tbody>
				<?php $_from = $this->_tpl_vars['referals']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
					<tr class='info referals_table'>
						<td  class="id_history">
							<?php echo $this->_tpl_vars['tid']; ?>

						</td>
						<td >
							<?php echo $this->_tpl_vars['result']['operation']; ?>

						</td>
						<td >
							<input style="width: 50px;text-align: center;padding: 2px;" type="text" name="procent[<?php echo $this->_tpl_vars['tid']; ?>
]" value='<?php echo $this->_tpl_vars['result']['proc']; ?>
'>
						</td>
						<td >
							<input type="hidden" name="status[<?php echo $this->_tpl_vars['tid']; ?>
]">
							<input class="pflybwf" type="checkbox" name="on_off2[<?php echo $this->_tpl_vars['tid']; ?>
]" <?php if ($this->_tpl_vars['result']['status'] == '1'): ?>checked<?php endif; ?>>
							<label class="clicable_label2" style='<?php if ($this->_tpl_vars['result']['status'] == '0'): ?>background-position: -1px -4px;<?php else: ?>background-position: -1px -34px;<?php endif; ?>'></label>
						</td>
					</tr>
				<?php endforeach; endif; unset($_from); ?>
			</tbody>
				
			</table>
			<div class="save_referal" style="margin-top: 12px;text-align:center" id="submitform">
				<button name="save"  type="button" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
			</div>
			</form>
		</div>
<!-- Управление паролями -->
		<div id="control_pass">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						<?php echo $this->_tpl_vars['LANG']['CONTROL_PASS']; ?>

					</h4>
				</div>
			</div>
			<form method="post" id="control_pass_form">
			<input type="hidden" name="opt" value="control_pass">
			<table class="table_password" border="0" cellspacing="0" cellpadding="5" id="">
			<tbody>
				<tr class='info referals_table'>
					<td  class="id_history" style="padding-right:20px;">
						Интервал смены пароля(дни)
					</td>
					<td >
						<input type="text" style="text-align:center; width:70px;" name='reload_pass' value="<?php echo $this->_tpl_vars['reload_pass']['value']; ?>
">
					</td>
					<td >
						<input type="hidden" name="status">
						<input class="pflybwf" type="checkbox" name="on_off2" <?php if ($this->_tpl_vars['reload_pass']['status'] == '1'): ?>checked<?php endif; ?>>
						<label class="clicable_label2" style='<?php if ($this->_tpl_vars['reload_pass']['status'] == '0'): ?>background-position: -1px -4px;<?php else: ?>background-position: -1px -34px;<?php endif; ?>'></label>
					</td>
				</tr>
			</tbody>
				
			</table>
			<div class="save_control_pass" style="margin-top: 12px;text-align:center" id="submitform">
				<button name="save"  type="button" id="save_btn"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
			</div>
			</form>
		</div>
</div>
</div>
<div id="modal_success5" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
		<div><form enctype="multipart/form-data" method="post" >
			<div class="img_priem"></div>
			<div style="float: left; margin-top: 30px; margin-left: 60px;">
				<input type="hidden" name="opt" value="change_img_user_ps">
				<input name="upload" type="hidden" value="1"/>
				<input name="name_system" type="hidden" value=""/>
				<input type="hidden" name="id_sustem">
				<input type="file" name="imageps" id="imageps">
			</div>
			<div style="width: 192px; height: 30px; float: right; margin: 50px 154px 10px 0;">
				<input type="submit" value="Изменить изображение">
			</div>
			</form>
		</div>
	</div>
</div>
<a href="#modal_success5" role="button" data-toggle="modal" id='modal_success_btn5'></a>

<div id="modal_success3" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Удаление заявки № <span id="numb_appl"></span></span>
		<div><form class="del_exchange_application" method="post" >
			<div style="float: left; margin-top: 30px; margin-left: 60px;">
				<input type="hidden" name="opt" value="del_appl_exch">
				<input name="id_appl" type="hidden" value=""/>
				<textarea name="message_user" placeholder="Напишите причину удаления заявки" style="resize:none; width:400px; height:120px;"></textarea>
			</div>
			<div style="width: 192px; height: 30px; float: right; margin: 25px 154px 10px 0;">
				<input type="submit" value="Удалить заявку">
			</div>
			</form>
		</div>
	</div>
</div>
<a href="#modal_success3" role="button" data-toggle="modal" id='modal_success_btn3'></a>	

<div id="modal_success4" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Добавление скидки пользлвателю <span id="name_user"></span></span>
		<div><form class="add_discounts" method="post" >
			<div style="float: left; margin-top: 30px; margin-left: 60px;">
				<input type="hidden" name="opt" value="add_discount">
				<input name="id_user" type="hidden" value=""/>
				<textarea name="message_user" placeholder="Комментарий скидки" style="margin-bottom: 20px;resize:none; width:400px; height:120px;"></textarea>
				<div>
					<span style="float:left; margin-right:10px;">Введите % скидки:</span>
					<input style="float:left; width:100px;" type="text" name="discount">
				</div>
				<div>
					<span style="float:left; margin-right:10px;">Введите дату окончания скидки</span>
					<input style="float:left; width:125px;" type="date" name="last_date">
				</div>
			</div>
			<div style="width: 192px; height: 30px; float: right; margin: 25px 154px 10px 0;">
				<input type="submit" value="Добавить скидку">
			</div>
			</form>
		</div>
	</div>
</div>
<a href="#modal_success4" role="button" data-toggle="modal" id='modal_success_btn4'></a>

<form class="dell_all_hist" method="post"><input type="hidden" name="opt" value="del_history"></form>
<form class="dell_all_report" method="post"><input type="hidden" name="opt" value="del_report"></form>
<?php echo '
	<script>
	$(document).ready(function(){
		$(\'.hint_inf\').hover(
			function(){
				heasd = $(\'.hint_inf .top\').height()*(-1)-10;
				$(\'.hint_inf .top\').css(\'margin-top\', heasd);
			},
			function(){}
		);
		$(\'.save_referal button\').click(function(){
			$(\'.pflybwf\').each(function(){
				if($(this).prop("checked") == true){
					var status = 1;
				}
				else{
					var status = 0;
				}
				$(this).parent().find(\'input[type="hidden"]\').val(status);
			});
			
			
			$(\'#referal_form\').submit();
		});
		$(\'.save_control_pass button\').click(function(){
			$(\'.pflybwf\').each(function(){
				if($(this).prop("checked") == true){
					var status = 1;
				}
				else{
					var status = 0;
				}
				$(this).parent().find(\'input[type="hidden"]\').val(status);
			});
			
			
			$(\'#control_pass_form\').submit();
		});
		$(\'.change_img_user_ps\').click(function(){
			var img = $(this).parent().parent().find(\'td.this_img\').html();
			$("#modal_success5 .img_priem").html(img);
			$("#modal_success5 input[name=\'id_sustem\']").val($(this).attr(\'data-id\'));
			$("#modal_success5 input[name=\'name_system\']").val($(this).attr(\'data-name\'));
			$("#modal_success_btn5").click();
		});
		$(\'.add_all_action\').click(function(){
			$(\'.add_discounts input[name="opt"]\').val(\'add_all_action\');
			$(\'#modal_success4 #text_span\').text("Добавление новой акции");
			$(\'#modal_success_btn4\').click();
		});
		$(\'.button_discount\').click(function(){
			$(\'#name_user\').text($(this).attr(\'data-name\'));
			$(\'.add_discounts input[name="id_user"]\').val($(this).attr(\'data-id\'));
			$(\'#modal_success_btn4\').click();
		});
		$(\'.border_top_none\').on("keyup keydown", \'input[type="text"]\', function(e){
			if(e.keyCode==13){
				window.location.href = "/users/1/administration.html?"+$(this).attr(\'name\')+"="+$(this).val()+"#report";
			}
		});
		$(\'.button_del_all_report\').click(function(){
			$(\'.dell_all_report.\').submit();
		});
		$(\'.button_del_one_appl\').click(function(){
			$(\'.del_exchange_application input[name="id_appl"]\').val($(this).attr("data-appl"));
			$(\'#numb_appl\').text($(this).attr("data-appl"));
			$(\'#modal_success_btn3\').click();
		});
		$(\'.del_img_user_pc\').click(function(){
			var id11 = $(this).attr(\'data-id\');
			jQuery.ajax({
				url:     \'/components/users/ajax/delate_img_user_pc.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: { id: id11},
				success: function(response) {
					$(\'#users_pc .this_img\').each(function(){
						if(response == $(this).attr(\'data-id\')){
							$(this).html("<span>Изображение отсутствует</span>");
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				$(\'.obr_timer\').each(function(){
						alert(\'Ошибка!\');
					});
				}
			});
		});
	});
	/*$(\'.zebra_table\').not(\'.zebra_table2\').hover(
		function(){
			$(this).addClass("border_hover");
		},
		function(){
			$(this).removeClass("border_hover");
		}
	);*/
	function tiktak(){
			var timer_off = 0;
		$(\'.minuts\').not(\'.off\').each(function(){
			timer_off++;
			var time = $(this).attr(\'data-minuts\');
			var hour = $(this).parent().find(\'.hour\').attr(\'data-hour\');
			
			if(time == 1 && hour == 0){
				$(this).addClass(\'off\');
				application_ajax($(this).attr(\'data-id\'));
			}
			else if(time == 1 && hour != 0){
				console.log(hour);
				time = 60;
				hour--;
				if(time < 10){
					$(this).attr(\'data-minuts\', time);
					$(this).text(\'0\'+time);
				}
				else{
					$(this).attr(\'data-minuts\', time);
					$(this).text(time);
				}
				
				if(hour < 10){
					$(this).parent().find(\'.hour\').attr(\'data-hour\', hour);
					$(this).parent().find(\'.hour\').text(\'0\'+hour);
				}
				else{
					$(this).parent().find(\'.hour\').attr(\'data-hour\', hour);
					$(this).parent().find(\'.hour\').text(hour);
				}
			}
			else{
				time--;
				if(time < 10){
					$(this).attr(\'data-minuts\', time);
					$(this).text(\'0\'+time);
				}
				else{
					$(this).attr(\'data-minuts\', time);
					$(this).text(time);
				}
				
			}
		});
		
		if(timer_off == 0){
			clearInterval(intervalID);
		}
	}
		
function application_ajax(id){
	jQuery.ajax({
		url:     \'/components/users/ajax/del_application_service.php\', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: { id: id},
		success: function(response) {
			
			$(\'.minuts\').each(function(){
				if(response == $(this).attr(\'data-id\')){
					$(this).parent().parent().html("");
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$(\'.obr_timer\').each(function(){
				alert(\'Ошибка!\');
			});
		}
	 });
}
	
$(document).ready(function(){

		$(\'#service .minuts\').each(function(){
			
		});
		intervalID = setInterval(function(){ tiktak(); }, 60000);
		
		$(\'#tabs a\').click(function(){
			location.hash = $(this).attr(\'href\');
			jQuery(\'html, body\').animate({scrollTop: "0px"},1); 
		});
		
		$(\'#click_grid2\').on(\'click\', "td",function(){
			if($(this).attr(\'data-inf\')){
			rrraa = $(this).attr(\'data-inf\');
				$(\'#grid2 thead th\').each(function(){
					if($(this).attr(\'data-name\') == rrraa){
						$(this).click();
					}
				});
			}
		});
		
		$(\'.button_del_one_report\').click(function(){
			id_report = $(this).attr("data-id");
			jQuery.ajax({
				url:     \'/components/users/ajax/delete_report.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: {id_report: id_report, status: 0},
				success: function(response) {
					$(\'.table_report .id_report\').each(function(){
						//alert(response);
						if($(this).attr(\'data-id\') == response){
							$(this).parent().remove();
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				alert(\'Ошибка оплаты заявки!\');
				}
			 });
		});
		
		$(\'.line_servisess\').click(function(){
			id_service = $(this).parent().parent().find(\'.id_service\').attr("data-id");
			jQuery.ajax({
				url:     \'/components/users/ajax/delete_application_service.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: {id_service: id_service, status: 0},
				success: function(response) {
					$(\'.table_service .id_service\').each(function(){
						//alert(response);
						if($(this).attr(\'data-id\') == response){
							$(this).parent().text(\'\');
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				alert(\'Ошибка оплаты заявки!\');
				}
			 });
		});
		
		$(\'.servise_button\').click(function(){
			id_service = $(this).parent().parent().find(\'.id_service\').attr("data-id");
			servise_func(id_service, 1);
		});
		
		function servise_func(id_service, st){
			jQuery.ajax({
				url:     \'/components/users/ajax/delete_application_service.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: {id_service: id_service, status: st},
				success: function(response) {
					$(\'.table_service .id_service\').each(function(){
						if($(this).attr(\'data-id\') == response){
							//alert(response);
							$(this).find(\'div\').addClass("line_servise");
							$(this).parent().find(\'.aaaasssssss\').addClass("fon_servise");
							$(this).parent().find(\'.servise_button\').removeClass("servise_button");
							$(this).parent().css("opacity", "0.5");
							$(this).parent().addClass("zebra_table2");
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				alert(\'Ошибка оплаты заявки!\');
				}
			 });
		}
		
		$(\'.line_servisesss\').click(function(){
			id_garant = $(this).parent().parent().find(\'.id_garant\').attr("data-id");
			jQuery.ajax({
				url:     \'/components/users/ajax/delete_application_garant.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: {id_garant: id_garant, status: 0},
				success: function(response) {
					//alert(response);
					$(\'.table_garant .id_garant\').each(function(){
						if($(this).attr(\'data-id\') == response){
							$(this).parent().text(\'\');
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				alert(\'Ошибка оплаты заявки!\');
				}
			 });
		});
		
		$(\'.garant_button\').click(function(){
			id_garant = $(this).parent().parent().find(\'.id_garant\').attr("data-id");
			
			garant_func(id_garant, 1);
		});
		
		function garant_func(id_garant, st){
			jQuery.ajax({
				url:     \'/components/users/ajax/delete_application_garant.php\', //Адрес подгружаемой страницы
				type:     "POST", //Тип запроса
				dataType: "html", //Тип данных
				data: {id_garant: id_garant, status: st},
				success: function(response) {
					$(\'.table_garant .id_garant\').each(function(){
						if($(this).attr(\'data-id\') == response){
							$(this).find(\'div\').addClass("line_servise");
							$(this).parent().find(\'.aaaasssssss\').addClass("fon_servise");
							$(this).parent().find(\'.garant_button\').removeClass("garant_button");
							$(this).parent().css("opacity", "0.5");
							$(this).parent().addClass("zebra_table2");
						}
					});
				},
				error: function(response) {  
				//Если ошибка
				alert(\'Ошибка оплаты заявки!\');
				}
			 });
		}
	
		$(\'.button_save\').click(function(){
			if($(this).parent().parent().find(\'input[name="verif_1"]\').prop("checked") == true){verif_1 = 1;}
			else{verif_1 = 0;}
			if($(this).parent().parent().find(\'input[name="verif_2"]\').prop("checked") == true){verif_2 = 1;}
			else{verif_2 = 0;}
			if($(this).parent().parent().find(\'input[name="verif_3"]\').prop("checked") == true){verif_3 = 1;}
			else{verif_3 = 0;}
			if($(this).parent().parent().find(\'input[name="verif_4"]\').prop("checked") == true){verif_4 = 1;}
			else{verif_4 = 0;}
			user_id = $(this).parent().parent().find(\'.uder_id\').attr("data-uder_id");
			verifycation_ajax(user_id, verif_1, verif_2, verif_3, verif_4);
		});
		
		$(\'.button_del_one_hist\').click(function(){
			id_history = $(this).parent().parent().find(\'.id_history\').attr(\'data-id\');
			history_one_ajax(id_history);
		});
		
		$(\'.button_del_all_hist\').click(function(){
			$("#modal_success_btn").click();
			$("#text_span").html("<p>Вы точно хотите удалить всю историю?</p><p><div class=\'td_buttonu\' style=\'position:inherit\'><a class=\'button_del_all_hist_2\' style=\'float: left;padding: 3px 10px;\'> Удалить</a> <a class=\'back_qqqq\' style=\'float: right;padding: 3px 10px;\'>Отменить</a></div></p>");
			
		});
		$(\'body\').on(\'click\', ".back_qqqq", function(){
			$(\'#modal_success .close\').click();
		});
		$(\'body\').on(\'click\', ".button_del_all_hist_2", function(){
			$(\'.dell_all_hist\').submit();
		});
	});
	
	function verifycation_ajax(us_id, v_1, v_2, v_3, v_4){
		jQuery.ajax({
			url:     \'/components/users/ajax/change_verification.php\', //Адрес подгружаемой страницы
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: {user_id: us_id, verif_1: v_1, verif_2: v_2, verif_3: v_3, verif_4: v_4},
			success: function(response) {
				$("#modal_success_btn").click();
				$("#text_span").text("Уровень верификации изменен до "+response+".");
			},
			error: function(response) {  
			//Если ошибка
			alert(\'Ошибка изменения верификации!\');
			}
		 });
	}
	function history_one_ajax(id){
		jQuery.ajax({
			url:     \'/components/users/ajax/change_history_one.php\', //Адрес подгружаемой страницы
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: {id: id},
			success: function(response) {
				$(\'.table_history .id_history\').each(function(){
					if($(this).attr(\'data-id\') == response){
						$(this).parent().text("");
					}
				});
			},
			error: function(response) {  
			//Если ошибка
			alert(\'Ошибка Очистки истории!\');
			}
		 });
	}	
		
		$(\'.info .clicable_label2\').click(function(){
			var data_checed2 = $(this).parent().find(\'input[type="checkbox"]\').prop("checked");
			//alert(data_checed2);
			if(data_checed2 == false){
				$(this).css(\'background-position\', \'-1px -34px\');
				$(this).parent().find(\'input[type="checkbox"]\').click();
			}
			else{
				$(this).css(\'background-position\', \'-1px -4px\');
				$(this).parent().find(\'input[type="checkbox"]\').click();
			}
			
		});
	
		$(\'.info .clicable_label\').click(function(){
			data_checed = $(this).parent().find(\'input\').prop("checked");
			if(data_checed == false){
				$(this).css(\'background-position\', \'-1px -34px\');
				$(this).parent().find(\'input\').click();
			}
			else{
				$(this).css(\'background-position\', \'-1px -4px\');
				$(this).parent().find(\'input\').click();
			}
			
		});
		var hash = document.location.hash;
			if(hash == \'#successSend\'){
				$("#modal_success_btn").click();
				document.location.hash = "";
			}
	
		$(function(){
			$( \'#tabs li\' ).click( function(){
				rel = $( this ).attr( "rel" );
				if(!rel){
					$(\'#submitform\').show();
				} else {
					$(\'#submitform\').hide();
				}
			});
		});
		
$(document).ready(function(){
	//////////// 	сортировка		/////////////////////////////////////
	/* сортировка 1  */
	var grid = document.getElementById(\'grid\');
 
    grid.onclick = function(e) {
      if (e.target.tagName != \'TH\') return;

      // Если TH -- сортируем
	  sortGrid(e.target.cellIndex, e.target.getAttribute(\'data-type\'), e.target.getAttribute(\'data-name\'), e.target.getAttribute(\'data-click\'));
	  
    };

    function sortGrid(colNum, type, name, click) {
	
	$(\'#grid thead th\').each(function(){
		if($(this).attr("data-name") == name){
			if($(this).attr("data-click") == \'1\'){
				$(this).attr("data-click", "2");
			}
			else{$(this).attr("data-click", "1");}
		}
	});
	
      var tbody = grid.getElementsByTagName(\'tbody\')[0];

      // Составить массив из TR
      var rowsArray = [].slice.call(tbody.rows);

      // определить функцию сравнения, в зависимости от типа
      var compare;

      switch (type) {
        case \'number\':
			
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].innerHTML - rowA.cells[colNum].innerHTML;
			  };
			}
          break;
        case \'string\':
		if(click == "1"){
			  compare = function(rowA, rowB) {
			  	return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
          break;
		case \'masive\':
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].getAttribute(\'data-title\') - rowB.cells[colNum].getAttribute(\'data-title\');
				
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].getAttribute(\'data-title\') - rowA.cells[colNum].getAttribute(\'data-title\');
			  };
			}
		  break;
      }

      // сортировать
      rowsArray.sort(compare);

      // Убрать tbody из большого DOM документа для лучшей производительности
      grid.removeChild(tbody);

      // добавить результат в нужном порядке в TBODY
      // они автоматически будут убраны со старых мест и вставлены в правильном порядке
      for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
      }

      grid.appendChild(tbody);

    }
	
	/* сортировка для истории (2) */
	var grid2 = document.getElementById(\'grid2\');
 
    grid2.onclick = function(e) {
      if (e.target.tagName != \'TH\') return;

      // Если TH -- сортируем
	  sortGrid2(e.target.cellIndex, e.target.getAttribute(\'data-type\'), e.target.getAttribute(\'data-name\'), e.target.getAttribute(\'data-click\'));
	  
    };

    function sortGrid2(colNum, type, name, click) {
	
	$(\'#grid2 thead th\').each(function(){
		if($(this).attr("data-name") == name){
			if($(this).attr("data-click") == \'1\'){
				$(this).attr("data-click", "2");
			}
			else{$(this).attr("data-click", "1");}
		}
	});
	
      var tbody = grid2.getElementsByTagName(\'tbody\')[0];

      // Составить массив из TR
      var rowsArray = [].slice.call(tbody.rows);

      // определить функцию сравнения, в зависимости от типа
      var compare;

      switch (type) {
        case \'number\':
			
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].innerHTML - rowA.cells[colNum].innerHTML;
			  };
			}
          break;
        case \'string\':
		
			if(click == "1"){
			  compare = function(rowA, rowB) {
			  	return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
          break;
		case \'masive\':
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].getAttribute(\'data-title\') - rowB.cells[colNum].getAttribute(\'data-title\');
				
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].getAttribute(\'data-title\') - rowA.cells[colNum].getAttribute(\'data-title\');
			  };
			}
		
		  break;
      }

      // сортировать
      rowsArray.sort(compare);

      // Убрать tbody из большого DOM документа для лучшей производительности
      grid2.removeChild(tbody);

      // добавить результат в нужном порядке в TBODY
      // они автоматически будут убраны со старых мест и вставлены в правильном порядке
      for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
      }

      grid2.appendChild(tbody);

    }
	
	////////////////////////////////////////////////////////////////////////////////////
});		
		
	</script>
'; ?>
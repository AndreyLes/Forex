<?php /* Smarty version 2.6.28, created on 2015-12-30 17:50:39
         compiled from com_users_messages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'profile_url', 'com_users_messages.tpl', 110, false),array('modifier', 'escape', 'com_users_messages.tpl', 123, false),)), $this); ?>
<?php if ($this->_tpl_vars['friends'] || $this->_tpl_vars['is_admin']): ?>
    <div class="float_bar">
        <a href="javascript:void(0)" class="new_link" onclick="users.sendMess(0, 0, this);return false;" title="<?php echo $this->_tpl_vars['LANG']['NEW_MESS']; ?>
:"><span class="ajaxlink"><?php echo $this->_tpl_vars['LANG']['WRITE']; ?>
</span></a>
    </div>
<?php endif; ?>
<div class="con_heading"><?php echo $this->_tpl_vars['LANG']['MY_MESS']; ?>
</div>
		<div class="usr_msgmenu_tabs">
			<?php if ($this->_tpl_vars['opt'] == 'in'): ?>
				<span class="usr_msgmenu_active in_span"><?php echo $this->_tpl_vars['page_title']; ?>
 <?php if ($this->_tpl_vars['new_messages']['messages']): ?>(<?php echo $this->_tpl_vars['new_messages']['messages']; ?>
)<?php endif; ?></span>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-history.html"><?php echo $this->_tpl_vars['LANG']['DIALOGS']; ?>
</a>
				<a class="usr_msgmenu_link out_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-sent.html"><?php echo $this->_tpl_vars['LANG']['SENT']; ?>
</a>
				<a class="usr_msgmenu_link notices_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-notices.html"><?php echo $this->_tpl_vars['LANG']['NOTICES']; ?>
 <?php if ($this->_tpl_vars['new_messages']['notices']): ?>(<?php echo $this->_tpl_vars['new_messages']['notices']; ?>
)<?php endif; ?></a>
				
			<?php elseif ($this->_tpl_vars['opt'] == 'out'): ?>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages.html"><?php echo $this->_tpl_vars['LANG']['INBOX']; ?>
</a>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-history.html"><?php echo $this->_tpl_vars['LANG']['DIALOGS']; ?>
</a>
				<span class="usr_msgmenu_active out_span"><?php echo $this->_tpl_vars['page_title']; ?>
</span>
				<a class="usr_msgmenu_link notices_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-notices.html"><?php echo $this->_tpl_vars['LANG']['NOTICES']; ?>
 <?php if ($this->_tpl_vars['new_messages']['notices']): ?>(<?php echo $this->_tpl_vars['new_messages']['notices']; ?>
)<?php endif; ?></a>
		
			<?php elseif ($this->_tpl_vars['opt'] == 'notices'): ?>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages.html"><?php echo $this->_tpl_vars['LANG']['INBOX']; ?>
</a>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-history.html"><?php echo $this->_tpl_vars['LANG']['DIALOGS']; ?>
</a>
				<a class="usr_msgmenu_link out_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-sent.html"><?php echo $this->_tpl_vars['LANG']['SENT']; ?>
</a>
				<span class="usr_msgmenu_active notices_span"><?php echo $this->_tpl_vars['page_title']; ?>
 <?php if ($this->_tpl_vars['new_messages']['notices']): ?>(<?php echo $this->_tpl_vars['new_messages']['notices']; ?>
)<?php endif; ?></span>
			<?php elseif ($this->_tpl_vars['opt'] == 'history'): ?>
				<a class="usr_msgmenu_link in_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages.html"><?php echo $this->_tpl_vars['LANG']['INBOX']; ?>
</a>
				<span class="usr_msgmenu_active in_span"><?php echo $this->_tpl_vars['page_title']; ?>
</span>
			
				<a class="usr_msgmenu_link out_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-sent.html"><?php echo $this->_tpl_vars['LANG']['SENT']; ?>
</a>
				<a class="usr_msgmenu_link notices_link" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-notices.html"><?php echo $this->_tpl_vars['LANG']['NOTICES']; ?>
 <?php if ($this->_tpl_vars['new_messages']['notices']): ?>(<?php echo $this->_tpl_vars['new_messages']['notices']; ?>
)<?php endif; ?></a>
			
				
				
			<?php endif; ?>
		</div>
<div class="usr_msgmenu_bar" style='height:20px'>
   <div style='padding-right: 10px;float: left;border-right: 1px dashed #999999;'> <strong style='font-size:15px;'><?php echo $this->_tpl_vars['LANG']['MESS_INBOX']; ?>
:</strong> <span id="msg_count" style='font-size:15px;'><?php echo $this->_tpl_vars['msg_count']; ?>
</span></div>
	<form style='width:40%;float:left;margin-bottom:0px' id='message_img'>
		<input type='image' src='/images/extrennyj.png' name=''/><span>Экстренные</span>
		<input type='image' src='/images/important.png' name=''/><span>Важные</span>
		<input type='image' src='/images/info.png' name=''/><span>Информационные </span>
	</form>
<?php if (( $this->_tpl_vars['opt'] != 'history' ) && $this->_tpl_vars['msg_count'] > 0): ?>
    <div style="    width: 125px; float: left;"><a href="javascript:void(0)" onclick="users.cleanCat('/users/<?php echo $this->_tpl_vars['id']; ?>
/delmessages-<?php echo $this->_tpl_vars['opt']; ?>
.html');return false;"><?php echo $this->_tpl_vars['LANG']['CLEAN_CAT']; ?>
</a></div>
<?php endif; ?>   

<?php if ($this->_tpl_vars['opt'] == 'history'): ?>
	<div style="width: 200px; float: left; margin-left: 10px;">
        <form action="" id="history" method="post">
            <select name="with_id" id="with_id" style="width:200px;height: 20px;" onchange="changeFriend();">
                <option value="0"><?php echo $this->_tpl_vars['LANG']['FRIEND_FOR_DIALOGS']; ?>
</option>
					<?php if ($this->_tpl_vars['interlocutors']): ?>
						<?php echo $this->_tpl_vars['interlocutors']; ?>

					<?php endif; ?>
				<option <?php if ($this->_tpl_vars['whith_id'] == '999999999999'): ?>selected <?php endif; ?> value="999999999999">Показать все</option>	
			</select>
        </form>
    </div>
<?php echo '
	<script type="text/javascript">
		function changeFriend(){
            fr_id = $("#with_id option:selected").val();
            if(fr_id != 0) {
                $("#history").attr("action", \'/users/'; ?>
<?php echo $this->_tpl_vars['id']; ?>
<?php echo '/messages-history\'+fr_id+\'.html\');
                $(\'#history\').submit();
            }
        }
	</script>
'; ?>

<?php elseif ($this->_tpl_vars['opt'] == 'in'): ?>
	<div style="width: 200px; float: left; margin-left: 10px;">
        <form action="" id="history" method="post">
            <select name="with_id" id="with_id" style="width:200px;height: 20px;" onchange="changeFriend();">
                <option disabled value="0" ><?php echo $this->_tpl_vars['LANG']['FRIEND_FOR_DIALOGS']; ?>
</option>
					<?php if ($this->_tpl_vars['interlocutors']): ?>
						<?php echo $this->_tpl_vars['interlocutors']; ?>

					<?php endif; ?>
				<option <?php if ($this->_tpl_vars['whith_id'] == 'all'): ?>selected <?php endif; ?> value="all">Показать все</option>
            </select>
        </form>
    </div>
<?php echo '
	<script type="text/javascript">
		
        function changeFriend(){
            fr_id = $("#with_id option:selected").val();
            if(fr_id != 0) {
                $("#history").attr("action", \'/users/'; ?>
<?php echo $this->_tpl_vars['id']; ?>
<?php echo '/messages.html\');
                $(\'#history\').submit();
            }
        }
	</script>
'; ?>
	
<?php endif; ?>
</div>
<?php if ($this->_tpl_vars['records']): ?>
    <?php $_from = $this->_tpl_vars['records']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['record']):
?>
    <div class="usr_msg_entry" style='margin-left:0px;' data-id="<?php echo $this->_tpl_vars['record']['id']; ?>
" id="usr_msg_entry_id_<?php echo $this->_tpl_vars['record']['id']; ?>
">
        <table cellspacing="4">
        <tr>
			<td style='border-right: dashed 1px #999999;padding-left: 5px;padding-right: 5px;width: 20px;padding-left: 8px;padding-right: 9px;'>
				<input type='checkbox' id='checkbox-id<?php echo $this->_tpl_vars['tid']; ?>
' value='v1' name='check1'/><label for="checkbox-id<?php echo $this->_tpl_vars['tid']; ?>
"></label>
			</td>
			<td style='border-right: dashed 1px #999999;padding-left: 11px;padding-right: 11px;'>
				<img src='/images/info.png'/>
			</td>
            <td width="70" height="70" style='border-right: dashed 1px #999999;padding-left:18px;padding-right:18px' valign="middle" style='' align="center">
			<?php echo $this->_tpl_vars['record']['authorlink']; ?>

                <?php if ($this->_tpl_vars['record']['sender_id'] > 0): ?>
                    <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['record']['author_login']), $this);?>
"><img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['record']['user_img']; ?>
" /></a>
                <?php else: ?>
                    <img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['record']['user_img']; ?>
" />
                <?php endif; ?>
                <div style="margin: 4px 0 0 0;"><?php echo $this->_tpl_vars['record']['online_status']; ?>
</div>
            </td>
            <td width="" valign="top" style='width: 60%;'>
				<span class="usr_msg_date" ><?php echo $this->_tpl_vars['record']['fpubdate']; ?>
</span>
				<div class="message_text" data-user_id="<?php echo $this->_tpl_vars['id']; ?>
" data-id="<?php echo $this->_tpl_vars['record']['id']; ?>
" style="padding:6px"><?php echo $this->_tpl_vars['record']['message']; ?>
</div>
			</td>
			<td  valign="top">
			<table>
				<tr>
				<td style='width:98px;'><?php if ($this->_tpl_vars['record']['from_id'] != $this->_tpl_vars['user_id']): ?><a href="javascript:void(0)" class="msg_reply" onclick="users.sendMess('<?php echo $this->_tpl_vars['record']['from_id']; ?>
', '<?php echo $this->_tpl_vars['record']['id']; ?>
', this);return false;" title="<?php echo $this->_tpl_vars['LANG']['NEW_MESS']; ?>
: <?php echo ((is_array($_tmp=$this->_tpl_vars['record']['author'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"><span class="otvet"><?php echo $this->_tpl_vars['LANG']['REPLY']; ?>
</span></a>
			<?php endif; ?></td><td>
			<a class="msg_history" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-history<?php echo $this->_tpl_vars['record']['from_id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['HISTORY']; ?>
</a>
			</td>
			<!--<a class="msg_delete" href="javascript:void(0)" onclick="users.deleteMessage('<?php echo $this->_tpl_vars['record']['id']; ?>
')"><span class="ajaxlink"></span></a>-->
				</tr>
			</table>
			</td>
        </tr>
        </table>
    </div>
	<div data-id="<?php echo $this->_tpl_vars['record']['id']; ?>
" class="child_block">
	<?php if ($this->_tpl_vars['record']['children']): ?>
	
		<?php $_from = $this->_tpl_vars['record']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
			<div class="usr_msg_entry" <?php if ($this->_tpl_vars['result']['rfom_id'] != $this->_tpl_vars['id']): ?>style='margin-left:20px; background:#f6f6f6;'<?php else: ?>style='margin-left:20px; background:#ccc;'<?php endif; ?> data-parent="<?php echo $this->_tpl_vars['result']['parent_id']; ?>
" id="usr_msg_entry_id_<?php echo $this->_tpl_vars['result']['id']; ?>
">
				<table cellspacing="4">
				<tr>
					<td style='border-right: dashed 1px #999999;padding-left: 5px;padding-right: 5px;width: 20px;padding-left: 8px;padding-right: 9px;'>
						<input type='checkbox' id='checkbox-id<?php echo $this->_tpl_vars['tid']; ?>
' value='v1' name='check1'/><label for="checkbox-id<?php echo $this->_tpl_vars['tid']; ?>
"></label>
					</td>
					<td style='border-right: dashed 1px #999999;padding-left: 11px;padding-right: 11px;'>
						<img src='/images/info.png'/>
					</td>
					<td width="70" height="70" style='border-right: dashed 1px #999999;padding-left:18px;padding-right:18px' valign="middle" style='' align="center">
					<?php echo $this->_tpl_vars['result']['authorlink']; ?>

						<?php if ($this->_tpl_vars['result']['sender_id'] > 0): ?>
							<a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['result']['author_login']), $this);?>
"><img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['result']['user_img']; ?>
" /></a>
						<?php else: ?>
							<img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['result']['user_img']; ?>
" />
						<?php endif; ?>
						<div style="margin: 4px 0 0 0;"><?php echo $this->_tpl_vars['result']['online_status']; ?>
</div>
					</td>
					<td width="" valign="top" style='width: 60%;'>
						<span class="usr_msg_date" ><?php echo $this->_tpl_vars['result']['fpubdate']; ?>
</span>
						<div class="message_text" data-user_id="<?php echo $this->_tpl_vars['id']; ?>
" data-id="<?php echo $this->_tpl_vars['result']['parent_id']; ?>
" style="padding:6px"><?php echo $this->_tpl_vars['result']['message']; ?>
</div>
					</td>
					<td  valign="top">
					<table>
						<tr>
						<td style='width:98px;'><?php if ($this->_tpl_vars['result']['from_id'] != $this->_tpl_vars['user_id']): ?><a href="javascript:void(0)" class="msg_reply" onclick="users.sendMess('<?php echo $this->_tpl_vars['result']['from_id']; ?>
', '<?php echo $this->_tpl_vars['result']['id']; ?>
', this);return false;" title="<?php echo $this->_tpl_vars['LANG']['NEW_MESS']; ?>
: <?php echo ((is_array($_tmp=$this->_tpl_vars['result']['author'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"><span class="otvet"><?php echo $this->_tpl_vars['LANG']['REPLY']; ?>
</span></a>
					<?php endif; ?></td><td>
					<a class="msg_history" href="/users/<?php echo $this->_tpl_vars['id']; ?>
/messages-history<?php echo $this->_tpl_vars['result']['from_id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['HISTORY']; ?>
</a>
					</td>
					<!--<a class="msg_delete" href="javascript:void(0)" onclick="users.deleteMessage('<?php echo $this->_tpl_vars['result']['id']; ?>
')"><span class="ajaxlink"></span></a>-->
						</tr>
					</table>
					</td>
				</tr>
				</table>
			</div>
			
		<?php endforeach; endif; unset($_from); ?>
	
	<?php endif; ?>
	</div>
    <?php endforeach; endif; unset($_from); ?>
    <?php echo $this->_tpl_vars['pagebar']; ?>

<?php else: ?>
	<p style="padding:20px 10px"><?php echo $this->_tpl_vars['LANG']['NOT_MESS_IN_CAT']; ?>
</p>
<?php endif; ?>
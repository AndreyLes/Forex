<?php /* Smarty version 2.6.28, created on 2016-02-10 18:08:22
         compiled from com_registration_change_login.tpl */ ?>
<?php echo '
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == \'#error_old_pass\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Старый пароль введен неверно.");
				document.location.hash = "";
			}
			if(hash == \'#WRONG_PASS\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Пароли не совпали.");
				document.location.hash = "";
			}
			if(hash == \'#PASS_SHORT\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Пароль должен быть не менее 6 символов!");
				document.location.hash = "";
			}
			if(hash == \'#error_new_old_pass\'){
				$("#modal_success_btn").click();
				$("#text_span").text("Новий пароль не должен совпадать со старым!");
				document.location.hash = "";
			}
		});
	function ValidateForm(){
		$(\'.login_form input\').each(function(){
			$(this).css(\'border\',\'2px inset\');
		});
		var oldpass = $(\'#oldpass\').val();
		var newpass = $(\'#newpass\').val();
		var newpass2 = $(\'#newpass2\').val();
		var prov = 0;
		if(oldpass == ""){ $(\'#oldpass\').css(\'border\',\'2px inset red\'); prov = 1; }
		if(newpass == ""){ $(\'#newpass\').css(\'border\',\'2px inset red\'); prov = 1; }
		if(newpass2 == ""){ $(\'#newpass2\').css(\'border\',\'2px inset red\'); prov = 1; }
		if(prov == 1){ return false; }
	}
	</script>
'; ?>

<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;padding-top: 10px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>

<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div style='width:716px;'>

<h1 class="con_heading"><?php echo $this->_tpl_vars['LANG']['SITE_CHANGE_PASS']; ?>
</h1>
<p style="text-align: center;
    font-size: 16px;
    margin: 10px 0 15px 0;">Старый пароль устарел. Пожалуйста измените пароль.</p>
<form method="post" action="" onsubmit="return ValidateForm();">
<table border="0" cellpadding="5" cellspacing="0" width="100%" class="login_form" >
    <tr>
		<td width="150" valign="top">
			<strong><?php echo $this->_tpl_vars['LANG']['OLD_PASS']; ?>
:</strong>
		</td>
		<td valign="top">
			<input name="oldpass" autocomplete="off" type="password" id="oldpass" class="text-input" size="30" />
		</td>
	</tr>
	<tr>
		<td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['NEW_PASS']; ?>
:</strong></td>
		<td valign="top"><input style="width: 206px !important; padding-left: 0px;" name="newpass" type="password"  id="newpass" class="passes text-input" size="30" />
		
		<a id='showpass'><?php echo $this->_tpl_vars['LANG']['SEE_PASSWORD']; ?>
</a></td>
		
	</tr>
	<tr>
		<td></td>
		<td>
			<ul id='galki'>
				<li><span>Минимум 2 латинские буквы</span></li>
				<li><span>Минимум 2 цифры</span></li>
				<li><span>Минимум 10 символов</span></li>
				<li><span>Максимум 16 символов</span></li>
			</ul>
		</td>
	</tr>
	<tr>
		<td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['NEW_PASS_REPEAT']; ?>
:</strong></td>
		<td valign="top">
		<input name="newpass2" type="password" class="passes text-input" id="newpass2" size="30" /><span id='galkun'></span></td>
	</tr>
</table>
<div style="text-align:center;margin-top: 15px;"><button id="save2" type="submit" name="submit" >Сохранить</button></div>
</form>
</div>
<script type="text/javascript">
    <?php echo '
    $(document).ready(function(){
        $(\'.login_form #login_field\').focus();
		
		$(\'#showpass\').click(function(){
			type_pass = $(\'#newpass\').attr(\'type\');
			if(type_pass == \'password\'){
				$(\'#newpass\').attr(\'type\',\'text\');
				$(\'#showpass\').text(\'Спрятать пароль\');
			}
			else{
				$(\'#newpass\').attr(\'type\',\'password\');
				$(\'#showpass\').text(\'Показать пароль\');
			}
		});
		
		$(\'#newpass\').focusout(function(){
			$(\'#galki li\').removeClass(\'galochka\');
			
			var test = $(\'#newpass\').val();
			var errors = 0;
			if (--test.split(/[a-z]/).length>1)
			{
				$(\'#galki li:first-child\').addClass(\'galochka\');
			}
			else{
			  var errors = 1;
			}
				
			if (--test.split(/[0-9]/).length>1)
				{
					$(\'#galki li:nth-child(2)\').addClass(\'galochka\');
				}
			else
				{
				 var errors = 1;
				}
			
			if (--test.split(/[a-zA-z0-9]/).length>9 && --test.split(/[a-zA-z0-9]/).length<17)
				{
					$(\'#galki li:nth-child(3)\').addClass(\'galochka\');
				}
			else
				{
					 var errors = 1;
				}
			if (--test.split(/[a-zA-z0-9]/).length<17 && --test.split(/[a-zA-z0-9]/).length>1)
				{
					$(\'#galki li:last-child\').addClass(\'galochka\');
				}
			else	
				{
					var errors = 1;
				}
				var test2 = $(\'#newpass2\').val();
			if(errors ==true)
			{
				$(\'#save2\').prop(\'disabled\',true);
			}
		});
    });
    '; ?>

</script>
<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

$_LANG['MOD_NEWSCOUNT']              = 'Количество для показа';

$_LANG['MOD_VIEW_TYPE']              = 'Показывать как';
$_LANG['MOD_VIEW_TYPE_OPT_TABLE']    = 'Таблица';
$_LANG['MOD_VIEW_TYPE_OPT_HR_TABLE'] = 'Горизонтальная таблица';
$_LANG['MOD_VIEW_TYPE_OPT_LIST']     = 'Список';

$_LANG['MOD_MAXCOOL']                = 'Количество колонок';

?>
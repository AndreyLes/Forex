<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }
/* 
 * Created by Firs Yuriy
 * e-mail: firs.yura@gmail.com
 * site: firs.org.ua
 */
$_LANG['WHOONLINE_USERS']           = 'Пользователей';
$_LANG['WHOONLINE_GUESTS']          = 'Гостей';
$_LANG['WAS_TODAY']                 = 'Сегодня были';
$_LANG['NOBODY_TODAY']              = 'Сегодня зарегистрированные пользователи не посещали сайт';

?>
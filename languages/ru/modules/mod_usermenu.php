<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

$_LANG['USERMENU_MESS']                ='Сообщения';
$_LANG['USERMENU_BALANCE']             ='Баланс';
$_LANG['USERMENU_MY_BLOG']             ='Блог';
$_LANG['USERMENU_DANNYE']             ='Персональные данные';
$_LANG['USERMENU_OPER']            		='Операции со счетом';
$_LANG['USERMENU_DRUG']            		='Пригласить друга';
$_LANG['USERMENU_PHOTOS']              ='Фото';
$_LANG['USERMENU_ARTICLES']            ='Статьи';
$_LANG['USERMENU_ADD_VIDEO']           ='Добавить ролик';
$_LANG['USERMENU_AUDIO']               ='Аудиозаписи';
$_LANG['USERMENU_CHANNEL']             ='Канал';
$_LANG['USERMENU_ADMININTER']          ='Админка';
$_LANG['USERMENU_ADD_ARTICLE']         ='Написать';
$_LANG['USERMENU_EXIT']                ='Выход';
$_LANG['USERMENU_ADMINISTRATION']      ='Администрирование';




?>
<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<?php ob_start(); ?>
<form action="index.php?view=components&do=config&id=<?php echo $id; ?>" method="post" name="optform" target="_self" id="form1">
{tab=<?php echo $_LANG["GENERAL"]; ?>}
<div class="sliderkit-panel">
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr valign="top">
            <td><?php echo $_LANG["LICENSE_KEY"]; ?></td>
            <td>
                <input name="license_key" type="text" id="license_key" value="<?php echo $model->config['license_key']; ?>" />
            <?php if (!$model->checkLicense(FALSE)){ ?>
                <div id="get_license_key">
                    email <input name="email" type="text" id="email" value="<?php echo $model->config['license_email']; ?>" />
                    <input type="checkbox" value="1" name="is_mirror" /> <?php echo $_LANG["MIRROR"]; ?>
                </div>
            <?php } ?>
            </td>
        </tr>
        <tr>
            <td width="80%"><?php echo $_LANG["COMPONENT_URL"]; ?></td>
            <td width="20%"><input name="component_url" type="text" value="<?php echo $model->config['component_url']; ?>" /></td>
        </tr>
        <tr>
            <td width="80%"><?php echo $_LANG["COMPONENT_TRACKS_URL"]; ?></td>
            <td width="20%"><input name="component_tracks_url" type="text" value="<?php echo $model->config['component_tracks_url']; ?>" /></td>
        </tr>
        <tr>
            <td width="80%"><?php echo $_LANG["COMPONENT_SINGER_URL"]; ?></td>
            <td width="20%"><input name="component_singer_url" type="text" value="<?php echo $model->config['component_singer_url']; ?>" /></td>
        </tr>
        <tr>
            <td width="80%"><?php echo $_LANG["COMPONENT_ALBUM_URL"]; ?></td>
            <td width="20%"><input name="component_album_url" type="text" value="<?php echo $model->config['component_album_url']; ?>" /></td>
        </tr>
        <tr>
            <td width="80%"><?php echo $_LANG["COMPONENT_GENRE_URL"]; ?></td>
            <td width="20%"><input name="component_genre_url" type="text" value="<?php echo $model->config['component_genre_url']; ?>" /></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["main_section"]; ?></strong></td>
            <td>
                <select name="main_section" id="main_section">
                    <option value="tracks" <?php if ($model->config['main_section'] == "tracks"){ echo 'selected="selected"'; } ?>><?php echo $_LANG["MUSICS"]; ?></option>
                    <option value="singers" <?php if ($model->config['main_section'] == "singers"){ echo 'selected="selected"'; } ?>><?php echo $_LANG["SINGERS"]; ?></option>
                    <option value="albums" <?php if ($model->config['main_section'] == "albums"){ echo 'selected="selected"'; } ?>><?php echo $_LANG["ALBUMS"]; ?></option>
                    <option value="genres" <?php if ($model->config['main_section'] == "genres"){ echo 'selected="selected"'; } ?>><?php echo $_LANG["GENRES"]; ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SEO_URL"]; ?></strong></td>
            <td>
                <select name="seo_url" id="seo_url">
                    <option value="1" <?php if ($model->config['seo_url'] == 1){ echo 'selected="selected"'; } ?>><?php echo $_LANG["SEO_URL_1"]; ?></option>
                    <option value="2" <?php if ($model->config['seo_url'] == 2){ echo 'selected="selected"'; } ?>><?php echo $_LANG["SEO_URL_2"]; ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="50%"><strong><?php echo $_LANG["SHOW_CATS"]; ?>:</strong></td>
            <td>
                <input name="show_cats" type="radio" value="1" <?php if ($model->config['show_cats']==1) {echo 'checked';}?>><?php echo $_LANG["YES"]; ?>
                <input name="show_cats" type="radio" value="0"<?php if ($model->config['show_cats']==0) {echo 'checked';}?>><?php echo $_LANG["NO"]; ?>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SHOW_SUBCATS"]; ?>:</strong></td>
            <td>
                <input name="show_subcats" type="radio" value="1" <?php if ($model->config['show_subcats']==1) {echo 'checked';}?>><?php echo $_LANG["YES"]; ?>
                <input name="show_subcats" type="radio" value="0"<?php if ($model->config['show_subcats']==0) {echo 'checked';}?>><?php echo $_LANG["NO"]; ?>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SHOW_CAT_TRACK"]; ?>:</strong></td>
            <td>
                <input name="show_cat_track" type="radio" value="1" <?php if ($model->config['show_cat_track']==1) {echo 'checked';}?>><?php echo $_LANG["YES"]; ?>
                <input name="show_cat_track" type="radio" value="0"<?php if ($model->config['show_cat_track']==0) {echo 'checked';}?>><?php echo $_LANG["NO"]; ?>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["FILESTYPE"]; ?>:</strong><br><span class="hinttext"><?php echo $_LANG["FILESTYPE_DESC"]; ?></span></td>
            <td><input name="filestype" id="filestype" value="<?php echo $model->config['filestype']; ?>"></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUSIC_COUNT"]; ?>:</strong><br><span class="hinttext"><?php echo $_LANG["THIS_DO"]; ?></span></td>
            <td><input name="perpage" type="text" id="perpage" value="<?php echo $model->config['perpage'];?>" size="5" /> <?php echo $_LANG["SHT"]; ?></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["RATING"]; ?>:</strong><br><span class="hinttext"><?php echo $_LANG["RATE"]; ?>.</span></td>
            <td><input name="rate" type="text" id="rate" value="<?php echo $model->config['rate'];?>" size="5" /> <?php echo $_LANG["SHT_RATE"]; ?></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["GET_MUSIC_TEXT"]; ?>:</strong><br><span class="hinttext"><?php echo $_LANG["GET_MUSIC_TEXT_DESC"]; ?></span></td>
            <td width="110">
                <input name="get_music_text" type="radio" value="1" <?php if ($model->config['get_music_text']==1) {echo 'checked';}?> onclick="$('#music_text_grab').show();" /><?php echo $_LANG["YES"]; ?>
                <input name="get_music_text" type="radio" value="0"<?php if ($model->config['get_music_text']==0) {echo 'checked';}?> onclick="$('#music_text_grab').hide();" /><?php echo $_LANG["NO"]; ?>
            </td>
        </tr>
        <tr id="music_text_grab" <?php if (!$model->config['get_music_text'] or $model->config['get_music_text'] == 0){ ?> style="display:none;" <?php } ?>>
            <td><strong><?php echo $_LANG["GET_MUSIC_PROVIDER"]; ?>:</strong></td>
            <td>
                <select name="get_music_text_provider" id="get_music_provider">
                    <option value="vkontakte" <?php if($model->config['get_music_text_provider']=="vkontakte"){ echo "selected=\"selected\"";} ?>>vkontakte</option>
                    <option value="ololo" <?php if($model->config['get_music_text_provider']=="ololo"){ echo "selected=\"selected\"";} ?>>ololo</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["PAYMENT"]; ?>:</strong></td>
            <td>
                <select name="payment">
                    <option value=""<?php if (!$model->config['payment']){ echo ' selected="selected"'; } ?>>off</option>
                    <option value="biling"<?php if ($model->config['payment'] == 'biling'){ echo ' selected="selected"'; } ?>>biling</option>
                </select>
            </td>
        </tr>
    </table>
</div>
{tab=<?php echo $_LANG["SINGERS"]; ?>}
<div class="sliderkit-panel">
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td width="80%"><strong><?php echo $_LANG["SINGER_PERPAGE"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_PERPAGE_DESC"]; ?></span></td>
            <td width="20%"><input name="singer_perpage" type="text" id="singer_perpage" value="<?php echo $model->config['singer_perpage'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["STYLE"]; ?>: </strong></td>
            <td>
                <script type="text/javascript">
                    function sinstylechange(){
                        $('#sin_style_1').toggle();
                        $('#sin_cha_style_1').toggle();
                        $('#sin_style_2').toggle();
                        $('#sin_cha_style_2').toggle();
                    }
                    function albstylechange(){
                        $('#alb_style_1').toggle();
                        $('#alb_cha_style_1').toggle();
                        $('#alb_style_2').toggle();
                        $('#alb_cha_style_2').toggle();
                    }
                </script>
                <select name="sin_list_style" id="sin_list_style" style="width:99%;" onchange="sinstylechange();">
                    <option value="style_1" <?php if($model->config['sin_list_style']=="style_1"){ echo "selected=\"selected\"";} ?>>Первый стиль</option>
                    <option value="style_2" <?php if($model->config['sin_list_style']=="style_2"){ echo "selected=\"selected\"";} ?>>Второй стиль</option>
                </select>
            </td>
        </tr>
        <tr id="sin_style_1" <?php if ($model->config['sin_list_style'] != "style_1"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MAXCOLS"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_MAXCOLS"]; ?></span></td>
            <td><input name="singer_maxcols" type="text" id="singer_maxcols" value="<?php echo $model->config['singer_maxcols'];?>"/></td>
        </tr>
        <tr id="sin_cha_style_1" <?php if ($model->config['sin_list_style'] != "style_1"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MAXCOLS"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_CHANNEL_MAXCOLS"]; ?></span></td>
            <td><input name="singer_channel_maxcols" type="text" id="singer_channel_maxcols" value="<?php echo $model->config['singer_channel_maxcols'];?>"/></td>
        </tr>
        <tr id="sin_style_2" <?php if ($model->config['sin_list_style'] != "style_2"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MARGIN_LEFT"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_MAXCOLS"]; ?></span></td>
            <td><input name="singer_margin_left" type="text" id="singer_margin_left" value="<?php echo $model->config['singer_margin_left'];?>"/></td>
        </tr>
        <tr id="sin_cha_style_2" <?php if ($model->config['sin_list_style'] != "style_2"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MARGIN_LEFT"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_CHANNEL_MAXCOLS"]; ?></span></td>
            <td><input name="singer_channel_margin_left" type="text" id="singer_channel_margin_left" value="<?php echo $model->config['singer_channel_margin_left'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SINGER_ORDERBY"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["SINGER_ORDERBY_DESC"]; ?></span></td>
            <td>
                <select name="singer_orderby" id="singer_orderby" style="width:99%">
                    <option value="singer_name" <?php if($model->config['singer_orderby']=="singer_name"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_NAME"]; ?></option>
                    <option value="listen" <?php if($model->config['singer_orderby']=="listen"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_LISTENED"]; ?></option>
                    <option value="rate_value" <?php if($model->config['singer_orderby']=="rate_value"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_VOTES"]; ?></option>
                    <option value="mus" <?php if($model->config['singer_orderby']=="mus"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_MUSIC_COUNT"]; ?></option>
                    <option value="pubdate" <?php if($model->config['singer_orderby']=="pubdate"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_DATE"]; ?></option>
                </select>
                <select name="singer_orderto" id="singer_orderto" style="width:99%">
                    <option value="DESC" <?php if ($model->config['singer_orderto'] == "DESC"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["DESC"]; ?></option>
                    <option value="ASC" <?php if ($model->config['singer_orderto'] == "ASC"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["ASC"]; ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MEDW"]; ?>: </strong></td>
            <td><input name="singer_medw" type="text" id="singer_medw" value="<?php echo $model->config['singer_medw'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MEDH"]; ?>: </strong></td>
            <td><input name="singer_medh" type="text" id="singer_medh" value="<?php echo $model->config['singer_medh'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SMALLW"]; ?>: </strong></td>
            <td><input name="singer_smallw" type="text" id="singer_smallw" value="<?php echo $model->config['singer_smallw'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SMALLH"]; ?>: </strong></td>
            <td><input name="singer_smallh" type="text" id="singer_smallh" value="<?php echo $model->config['singer_smallh'];?>"/></td>
        </tr>
    </table>
</div>
{tab=<?php echo $_LANG["ALBUMS"]; ?>}
<div class="sliderkit-panel">
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td width="80%"><strong><?php echo $_LANG["ALBUM_PERPAGE"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_PERPAGE_DESC"]; ?>,</span></td>
            <td width="20%"><input name="album_perpage" type="text" id="album_perpage" value="<?php echo $model->config['album_perpage'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["STYLE"]; ?>: </strong></td>
            <td>
                <select name="alb_list_style" id="alb_list_style" style="width:99%;" onchange="albstylechange();">
                    <option value="style_1" <?php if($model->config['alb_list_style']=="style_1"){ echo "selected=\"selected\"";} ?>>Первый стиль</option>
                    <option value="style_2" <?php if($model->config['alb_list_style']=="style_2"){ echo "selected=\"selected\"";} ?>>Второй стиль</option>
                </select>
            </td>
        </tr>
        <tr id="alb_style_1" <?php if ($model->config['alb_list_style'] != "style_1"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MAXCOLS"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_MAXCOLS"]; ?></span></td>
            <td><input name="album_maxcols" type="text" id="album_maxcols" value="<?php echo $model->config['album_maxcols'];?>"/></td>
        </tr>
        <tr id="alb_style_1" <?php if ($model->config['alb_list_style'] != "style_1"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MAXCOLS"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_CHANNEL_MAXCOLS"]; ?></span></td>
            <td><input name="album_channel_maxcols" type="text" id="album_channel_maxcols" value="<?php echo $model->config['album_channel_maxcols'];?>"/></td>
        </tr>
        <tr id="alb_style_2" <?php if ($model->config['alb_list_style'] != "style_2"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MARGIN_LEFT"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_MAXCOLS"]; ?></span></td>
            <td><input name="album_margin_left" type="text" id="album_margin_left" value="<?php echo $model->config['album_margin_left'];?>"/></td>
        </tr>
        <tr id="alb_style_2" <?php if ($model->config['alb_list_style'] != "style_2"){ echo "style=\"display:none;\""; } ?>>
            <td><strong><?php echo $_LANG["MARGIN_LEFT"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_CHANNEL_MAXCOLS"]; ?></span></td>
            <td><input name="album_channel_margin_left" type="text" id="album_channel_margin_left" value="<?php echo $model->config['album_channel_margin_left'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["ALBUM_ORDERBY"]; ?>: </strong><br><span class="hinttext"><?php echo $_LANG["ALBUM_ORDERBY_DESC"]; ?></span></td>
            <td>
                <select name="album_orderby" id="album_orderby" style="width:99%">
                    <option value="album_name" <?php if($model->config['album_orderby']=="album_name"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_NAME"]; ?></option>
                    <option value="listen" <?php if($model->config['album_orderby']=="listen"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_LISTENED"]; ?></option>
                    <option value="rate_value" <?php if($model->config['album_orderby']=="rate_value"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_VOTES"]; ?></option>
                    <option value="mus" <?php if($model->config['album_orderby']=="mus"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_MUSIC_COUNT"]; ?></option>
                    <option value="pubdate" <?php if($model->config['album_orderby']=="pubdate"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["BY_DATE"]; ?></option>
                </select>
                <select name="album_orderto" id="album_orderto" style="width:99%">
                    <option value="DESC" <?php if ($model->config['album_orderto'] == "DESC"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["DESC"]; ?></option>
                    <option value="ASC" <?php if ($model->config['album_orderto'] == "ASC"){ echo "selected=\"selected\"";} ?>><?php echo $_LANG["ASC"]; ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MEDW"]; ?>: </strong></td>
            <td><input name="album_medw" type="text" id="album_medw" value="<?php echo $model->config['album_medw'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MEDH"]; ?>: </strong></td>
            <td><input name="album_medh" type="text" id="album_medh" value="<?php echo $model->config['album_medh'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SMALLW"]; ?>: </strong></td>
            <td><input name="album_smallw" type="text" id="album_smallw" value="<?php echo $model->config['album_smallw'];?>"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["SMALLH"]; ?>: </strong></td>
            <td><input name="album_smallh" type="text" id="album_smallh" value="<?php echo $model->config['album_smallh'];?>"/></td>
        </tr>
    </table>
</div>
{tab=<?php echo $_LANG["PLAYER"]; ?>}
<div class="sliderkit-panel">
    <table width="100%">
        <tr>
            <td width="50%">
                <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
                    <tr>
                        <td><strong><?php echo $_LANG["APLAYER"]; ?>:</strong><br/></td>
                        <td width="250px">
                            <select name="audio_player">
                                <?php echo $aplayer_options; ?>
                            </select>
                        </td>
                    </tr>
                    <?php if ($model->player->audio_list_skin){ ?>
                    <tr>
                        <td><strong><?php echo $_LANG["AUDIO_LIST_SKIN"]; ?>:</strong><br/></td>
                        <td>
                            <select name="audio_list_skin" onchange="viewimg('<?php echo $model->config['audio_player']; ?>', 'list', 'audio');" style="width:100px"> 
                                <?php echo $model->getSkinDirFiles($model->config['audio_player'], "list"); ?>
                            </select>
                            <?php if ($model->player->audio_list_size){ ?>
                            W:<input name="audio_list_width" type="text" value="<?php echo $model->config['audio_list_width']; ?>" style="width:30px"> H:<input name="audio_list_heigth" type="text" value="<?php echo $model->config['audio_list_heigth']; ?>" style="width:30px">
                            <?php } ?>
                        </td>
                    </tr>
                    <?php }
                          if ($model->player->audio_one_skin){ ?>
                    <tr>
                        <td><strong><?php echo $_LANG["AUDIO_ONE_SKIN"]; ?>:</strong></td>
                        <td>
                            <select name="audio_one_skin" onchange="viewimg('<?php echo $model->config['audio_player']; ?>', 'one', 'audio');" style="width:100px">
                                <?php echo $model->getSkinDirFiles($model->config['audio_player'], "one"); ?>
                            </select>
                            <?php if ($model->player->audio_one_size){ ?>
                            W:<input name="audio_one_width" type="text" value="<?php echo $model->config['audio_one_width']; ?>" style="width:30px"> H:<input name="audio_one_heigth" type="text" value="<?php echo $model->config['audio_one_heigth']; ?>" style="width:30px">
                            <?php } ?>
                        </td>
                    </tr>
                    <?php }
                          if ($model->player->audio_pl_skin){ ?>
                    <tr>
                        <td><strong><?php echo $_LANG["AUDIO_PLAYLIST_SKIN"]; ?>:</strong></td>
                        <td>
                            <select name="audio_pl_skin" onchange="viewimg('<?php echo $model->config['audio_player']; ?>', 'pl', 'audio');" style="width:100px">
                                <?php echo $model->getSkinDirFiles($model->config['audio_player'], "pl"); ?>
                            </select>
                            <?php if ($model->player->audio_pl_size){ ?>
                            W:<input name="audio_pl_width" type="text" value="<?php echo $model->config['audio_pl_width']; ?>" style="width:30px"> H:<input name="audio_pl_heigth" type="text" value="<?php echo $model->config['audio_pl_heigth']; ?>" style="width:30px">
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
                    <tr>
                        <td><strong><?php echo $_LANG["VPLAYER"]; ?>:</strong><br/></td>
                        <td width="250px">
                            <select name="video_player">
                                <?php echo $vplayer_options; ?>
                            </select>
                        </td>
                    </tr>
                    <?php if ($model->vplayer->vplayer_skin){ ?>
                    <tr>
                        <td><strong><?php echo $_LANG["VPLAYER_SKIN"]; ?></strong></td>
                        <td>
                            <select name="video_one_skin" onchange="viewimg('<?php echo $model->config['video_player']; ?>', 'one', 'video');" style="width:100px">
                                <?php echo $model->getSkinDirFiles($model->config['video_player'], "one", "video"); ?>
                            </select>
                            W:<input name="video_one_width" type="text" value="<?php echo $model->config['video_one_width']; ?>" style="width:30px"> H:<input name="video_one_heigth" type="text" value="<?php echo $model->config['video_one_heigth']; ?>" style="width:30px">
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </td>
            <td align="left" valign="center">
                <div id="view_img" align="left"></div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function viewimg(player, skin, type){
            $("#view_img").html('<img src="/components/music/players/' + type + '/' + player + '/skins/' + skin + '/img/'+$('select[name='+type+'_'+skin+'_skin]').val()+'.jpg">');
        }
        viewimg('<?php echo $model->config['audio_player']; ?>', 'one', 'audio');
    </script>
</div>
{tab=<?php echo $_LANG["LIMITATIONS"]; ?>}
<div class="sliderkit-panel">
    <table width="" border="0" cellpadding="10" cellspacing="0">
        <tr>
            <td width="300px"></td>
            <td>
                <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
                    <tr style="background-color: #ccc;" align="center">
                        <?php
                        foreach ($user_groups as $group){ 
                            echo '<td width="100px"><strong';
                            if ($group['is_admin']){
                                echo ' style="color: red">';
                            }else if($group['id'] == 0){
                                echo ' style="color: yellow">';
                            }else{ echo '>'; }
                            echo $group['title'];
                            echo '</strong></td>';
                        }
                        ?>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" border="0" cellpadding="10" cellspacing="0" class="proptable">
                    <?php foreach($model->getGroupAccessArray() as $key => $g_access){ ?>
                    <tr>
                        <td width="300px"><strong><?php echo $_LANG["ACCESS_T_".$key]; ?></strong><br /><span class="hinttext"><?php echo $_LANG["ACCESS_D_".$key]; ?></span></td>
                        <?php foreach ($user_groups as $group){ ?>
                        <td width="100px" align="center">
                            <?php 
                            if ($group['id'] != 0 or $g_access['is_guest'] == 1){
                                if ($g_access['type'] == "select"){
                                    echo '<select ';
                                }else{
                                    echo '<input ';
                                }
                                echo 'name="group_access['.$group['id'].']['.$key.']" ';
                                if ($g_access['type'] == "select"){
                                    echo '>';
                                    foreach ($g_access['options'] as $option){
                                        $s = '';
                                        if ($model->config['group_access_'.$group['id']][$key] == $option['value']){
                                            $s = ' selected="selected"';
                                        }
                                        echo '<option value="'.$option['value'].'"'.$s.'>'.$option['title'].'</option>';
                                    }
                                    echo '</select>';
                                }else{
                                    echo 'type="'.$g_access['type'].'" ';
                                }
                                if ($g_access['type'] == "checkbox"){
                                    echo ' value="1" ';
                                    if ($model->config['group_access_'.$group['id']][$key] == 1){
                                        echo ' checked="checked" ';
                                    }
                                    echo '/>';
                                }else if ($g_access['type'] == "text"){
                                    echo ' value="'.$model->config['group_access_'.$group['id']][$key].'" />';
                                }
                            }else{
                                echo $_LANG["NOT_FOR_GUESTS"];
                            }
                            ?>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
    </table>
</div>
{tab=<?php echo $_LANG["MUSIC_LIST"]; ?>}
<div class="sliderkit-panel">
    <table width="" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td width="50%"><strong><?php echo $_LANG["MUS_DOWNLOAD"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_download" type="radio" value="1" <?php if ($model->config['mus_download']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_download" type="radio" value="0"<?php if ($model->config['mus_download']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_COMMENT"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_comment" type="radio" value="1" <?php if ($model->config['mus_comment']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_comment" type="radio" value="0"<?php if ($model->config['mus_comment']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_SHARE"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_share" type="radio" value="1" <?php if ($model->config['mus_share']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_share" type="radio" value="0"<?php if ($model->config['mus_share']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_SINGER"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_singer" type="radio" value="1" <?php if ($model->config['mus_singer']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_singer" type="radio" value="0"<?php if ($model->config['mus_singer']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_ALBUM"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_album" type="radio" value="1" <?php if ($model->config['mus_album']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_album" type="radio" value="0"<?php if ($model->config['mus_album']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_GENRE"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_genre" type="radio" value="1" <?php if ($model->config['mus_genre']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_genre" type="radio" value="0"<?php if ($model->config['mus_genre']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_CHANNEL"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_channel" type="radio" value="1" <?php if ($model->config['mus_channel']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_channel" type="radio" value="0"<?php if ($model->config['mus_channel']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_PUBDATE"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_pubdate" type="radio" value="1" <?php if ($model->config['mus_pubdate']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_pubdate" type="radio" value="0"<?php if ($model->config['mus_pubdate']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_LISTEN"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_listen" type="radio" value="1" <?php if ($model->config['mus_listen']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_listen" type="radio" value="0"<?php if ($model->config['mus_listen']==0) {echo 'checked';}?>>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["MUS_RATING"]; ?></strong></td>
            <td>
                <?php echo $_LANG["YES"]; ?><input name="mus_rating" type="radio" value="1" <?php if ($model->config['mus_rating']==1) {echo 'checked';}?>>
                    <?php echo $_LANG["NO"]; ?><input name="mus_rating" type="radio" value="0"<?php if ($model->config['mus_rating']==0) {echo 'checked';}?>>
            </td>
        </tr>
    </table>
</div>
{tab=<?php echo $_LANG["PROVIDERS"]; ?>}
<div class="sliderkit-panel">
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td>
                <strong><?php echo $_LANG["MSG_SEND"]; ?>: </strong><br/>
                <span class="hinttext"><?php echo $_LANG["MSG_SEND_DESC"]; ?></span>
            </td>
            <td width="250px"><input name="msg_send" type="text" id="msg_send" value="<?php echo $model->config['msg_send'];?>" size="15"/></td>
        </tr>
    </table>
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <script type="text/javascript">
            function dellogin(){
                $.ajax({
                    type: "POST",
                    url: "index.php?view=components&do=config&id=<?php echo $id; ?>&opt=del_vk_akk",
                    data: "usr_vk_id="+$('select[name=usr_vk_id]').val(),
                    success: function(msg){
                        if (msg.length>4){
                            alert(msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        </script>
        <tr>
            <td colspan="2" align="right"><b>VKONTAKTE</b></td>
        </tr>
        <tr>
            <td colspan="2">
                <p><?php echo $_LANG["INSTRUKCIJA"]; ?> <a href="http://Vadyusmusic.ru/instrukcija-po-dabovleniyu-saita-v-prilozhenija-vk-com.html" target="_blank"><?php echo $_LANG["HERE"]; ?></a>.</p>
            </td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["VK_APP_ID"]; ?>:</strong></td>
            <td width="250px"><input name="vk_app_id" type="text" id="vk_app_id" value="<?php echo $model->config['vk_app_id'];?>" style="width:250px"style="width:250px"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["VK_APP_SECRET"]; ?>:</strong></td>
            <td><input name="vk_app_secret" type="text" id="vk_app_secret" value="<?php echo $model->config['vk_app_secret'];?>" style="width:250px"/></td>
        </tr>
        <tr>
            <td>
                <?php if ($model->config['vk_app_id'] and $model->config['vk_app_secret']){ ?>
                <a href="http://oauth.vk.com/authorize?client_id=<?php echo $model->config['vk_app_id']; ?>&scope=audio,video,offline&display=popup&redirect_uri=<?php echo HOST; ?>/components/music/api/vkapi.php&response_type=code" target="_blank"><input type="button" value="Разрешить доступ" /></a>
                <?php } ?>
            </td>
            <td valign="top" align="right">
                <select name="usr_vk_id" size="5" style="width:100%">
                    <?php echo $vk_logins_list; ?>
                </select><br/>
                <input type="button" value="<?php echo $_LANG["DEL"]; ?>" onclick="dellogin();"/>
            </td>
        </tr>
    </table>
    <table width="50%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td colspan="2" align="right"><b>ZAYCEV</b></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["LOGIN"]; ?> (zaycev.net): </strong></td>
            <td width="250px"><input name="zaycev_login" type="text" id="zaycev_login" value="<?php echo $model->config['zaycev_login'];?>" size="15"/></td>
        </tr>
        <tr>
            <td><strong><?php echo $_LANG["PASSWORD"]; ?> (zaycev.net): </strong></td>
            <td><input name="zaycev_pass" type="password" id="zaycev_pass" value="<?php echo $model->config['zaycev_pass'];?>" size="15"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input name="zaycev_vip" type="checkbox" value="1" <?php if ($model->config['zaycev_vip']==1) {echo 'checked';}?>> <?php echo $_LANG["ZAYCEV_VIP"]; ?>
            </td>
        </tr>
    </table>
</div>
{tab=SEO}
<div class="sliderkit-panel">
    <table width="60%" border="0" cellpadding="10" cellspacing="0" class="proptable">
        <tr>
            <td><strong>title</strong> (<b>/music</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music</b>)</td>
            <td><strong>meta description</strong> (<b>/music</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="main_pagetitle" value="<?php echo $model->config['main_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="main_keywords" style="width:300px" rows="2"><?php echo $model->config['main_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="main_description" style="width:300px" rows="2"><?php echo $model->config['main_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/ЧПУ_ПЕСНИ</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/ЧПУ_ПЕСНИ</b>)</td>
            <td><strong>meta description</strong> (<b>/music/ЧПУ_ПЕСНИ</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="music_pagetitle" value="<?php echo $model->config['music_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="music_keywords" style="width:300px" rows="2"><?php echo $model->config['music_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="music_description" style="width:300px" rows="2"><?php echo $model->config['music_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%title% - название песни, %singer% - имя исполнителя, %album% - название альбома, %genre% - название жанра</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/singers</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/singers</b>)</td>
            <td><strong>meta description</strong> (<b>/music/singers</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="singers_pagetitle" value="<?php echo $model->config['singers_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="singers_keywords" style="width:300px" rows="2"><?php echo $model->config['singers_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="singers_description" style="width:300px" rows="2"><?php echo $model->config['singers_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/ЧПУ_КАТЕГОРИИ</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/ЧПУ_КАТЕГОРИИ</b>)</td>
            <td><strong>meta description</strong> (<b>/music/ЧПУ_КАТЕГОРИИ</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="cat_pagetitle" value="<?php echo $model->config['cat_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="cat_keywords" style="width:300px" rows="2"><?php echo $model->config['cat_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="cat_description" style="width:300px" rows="2"><?php echo $model->config['cat_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%title% - название категории, %page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/singer/ЧПУ_ИСПОЛНИТЕЛЯ</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/singer/ЧПУ_ИСПОЛНИТЕЛЯ</b>)</td>
            <td><strong>meta description</strong> (<b>/music/singer/ЧПУ_ИСПОЛНИТЕЛЯ</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="singer_pagetitle" value="<?php echo $model->config['singer_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="singer_keywords" style="width:300px" rows="2"><?php echo $model->config['singer_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="singer_description" style="width:300px" rows="2"><?php echo $model->config['singer_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%singer% - имя исполнителя</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/albums</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/albums</b>)</td>
            <td><strong>meta description</strong> (<b>/music/albums</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="albums_pagetitle" value="<?php echo $model->config['albums_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="albums_keywords" style="width:300px" rows="2"><?php echo $model->config['albums_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="albums_description" style="width:300px" rows="2"><?php echo $model->config['albums_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/album/ЧПУ_АЛЬБОМА</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/album/ЧПУ_АЛЬБОМА</b>)</td>
            <td><strong>meta description</strong> (<b>/music/album/ЧПУ_АЛЬБОМА</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="album_pagetitle" value="<?php echo $model->config['album_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="album_keywords" style="width:300px" rows="2"><?php echo $model->config['album_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="album_description" style="width:300px" rows="2"><?php echo $model->config['album_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%album% - название альбома</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/genres</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/genres</b>)</td>
            <td><strong>meta description</strong> (<b>/music/genres</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="genres_pagetitle" value="<?php echo $model->config['genres_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="genres_keywords" style="width:300px" rows="2"><?php echo $model->config['genres_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="genres_description" style="width:300px" rows="2"><?php echo $model->config['genres_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/genres/genre-id</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/genres/genre-id</b>)</td>
            <td><strong>meta description</strong> (<b>/music/genres/genre-id</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="genre_pagetitle" value="<?php echo $model->config['genre_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="genre_keywords" style="width:300px" rows="2"><?php echo $model->config['genre_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="genre_description" style="width:300px" rows="2"><?php echo $model->config['genre_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%genre% - название жанра, %page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/channel/ЛОГИН</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/channel/ЛОГИН</b>)</td>
            <td><strong>meta description</strong> (<b>/music/channel/ЛОГИН</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="channel_pagetitle" value="<?php echo $model->config['channel_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="channel_keywords" style="width:300px" rows="2"><?php echo $model->config['channel_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="channel_description" style="width:300px" rows="2"><?php echo $model->config['channel_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%login% - ссылка канала (автора канала), %nickname% - название канала (имя автора канала)</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/tracks/letter/БУКВА</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/tracks/letter/БУКВА</b>)</td>
            <td><strong>meta description</strong> (<b>/music/tracks/letter/БУКВА</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="music_letter_pagetitle" value="<?php echo $model->config['music_letter_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="music_letter_keywords" style="width:300px" rows="2"><?php echo $model->config['music_letter_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="music_letter_description" style="width:300px" rows="2"><?php echo $model->config['music_letter_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%letter% - первая буква названия песни</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/singers/letter/БУКВА</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/singers/letter/БУКВА</b>)</td>
            <td><strong>meta description</strong> (<b>/music/singers/letter/БУКВА</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="singers_letter_pagetitle" value="<?php echo $model->config['singers_letter_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="singers_letter_keywords" style="width:300px" rows="2"><?php echo $model->config['singers_letter_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="singers_letter_description" style="width:300px" rows="2"><?php echo $model->config['singers_letter_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%letter% - первая буква имени исполнителя</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/top-100</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/top-100</b>)</td>
            <td><strong>meta description</strong> (<b>/music/top-100</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="top_100_pagetitle" value="<?php echo $model->config['top_100_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="top_100_keywords" style="width:300px" rows="2"><?php echo $model->config['top_100_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="top_100_description" style="width:300px" rows="2"><?php echo $model->config['top_100_description'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="hinttext">%title% - название выбранного рейтинга, %page% - номер страницы</span>
            </td>
        </tr>
        <tr>
            <td><strong>title</strong> (<b>/music/search</b>)</td>
            <td><strong>meta keywords</strong> (<b>/music/search</b>)</td>
            <td><strong>meta description</strong> (<b>/music/search</b>)</td>
        </tr>
        <tr valign="top">
            <td>
                <input name="search_pagetitle" value="<?php echo $model->config['search_pagetitle']; ?>" />
            </td>
            <td>
                <textarea name="search_keywords" style="width:300px" rows="2"><?php echo $model->config['search_keywords'] ?></textarea>
            </td>
            <td>
                <textarea name="search_description" style="width:300px" rows="2"><?php echo $model->config['search_description'] ?></textarea>
            </td>
        </tr>
    </table>
</div>
{/tabs}
<?php echo jwTabs(ob_get_clean()); ?>
<p>
    <input name="opt" type="hidden" value="saveconfig" />
    <input name="save" type="submit" id="save" value="<?php echo $_LANG["SAVE"]; ?>" />
    <input name="back" type="button" id="back" value="<?php echo $_LANG["CANSEL"]; ?>" onclick="window.location.href='index.php?view=components';"/>
</p>
</form>
<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<form action="<?php echo $base_uri; ?>" method="GET" id="filter_form">
    <input type="hidden" name="view" value="components" />
    <input type="hidden" name="do" value="config" />
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php
        if ($opt == "albums"){
            echo "<input type=\"hidden\" name=\"opt\" value=\"albums\" />";
        }else{
            echo "<input type=\"hidden\" name=\"opt\" value=\"singers\" />";
        }
    ?>
    <table class="toolmenu" cellpadding="5" border="0" width="100%" style="margin-bottom: 2px; font-size:11px; vertical-align:middle;" id="filterpanel">
        <tr>
            <td width="110">
                <select name="orderby" style="width:110px" onchange="$('#filter_form').submit()">
                    <option value="name" <?php if($orderby=='name'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_NAME"]; ?>
                    </option>
                    <option value="listen" <?php if($orderby=='listen'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_LISTENED"]; ?>
                    </option>
                    <option value="rate_value" <?php if($orderby=='rate_value'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_VOTES"]; ?>
                    </option>
                    <option value="music_count" <?php if($orderby=='music_count'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_MUSIC_COUNT"]; ?>
                    </option>
                    <option value="pubdate" <?php if($orderby=='pubdate'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_DATE"]; ?>
                    </option>
                </select>
            </td>
            <td width="115">
                <select name="orderto" style="width:115px" onchange="$('#filter_form').submit()">
                    <option value="asc" <?php if($orderto=='asc'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["ASC"]; ?>
                    </option>
                    <option value="desc" <?php if($orderto=='desc'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["DESC"]; ?>
                    </option>
                </select>
            </td>
            <td></td>
        </tr>
    </table>
</form>
<table id="listTable" class="tablesorter" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:0px">
    <thead>
        <tr>
            <th class="pane" width="75"><?php echo $_LANG["DATE"]; ?></th>
            <th class="pane" width=""><?php echo $_LANG["NAME"]; ?></th>
            <th class="pane" width="50"><?php echo $_LANG["MUSIC_COUNT"]; ?></th>
            <th class="pane" width="85"><?php echo $_LANG["DO"]; ?></th>
        </tr>
    </thead>
    <?php if ($total){ ?>
        <tbody>
            <?php foreach($items as $num=>$item){ ?>
                <tr id="<?php echo $item['id']; ?>" class="item_tr">
                    <td style="font-size:9px"><?php echo $item['pubdate']; ?></td>
                    <td>
                        <a href="?view=components&do=config&id=<?php echo $id; ?>&opt=<?php if($opt == "albums"){ echo "view_album&album_id=".$item['id']; }else{ echo "view_singer&singer_id=".$item['id']; } ?>"><?php echo $item['title']; ?></a>
                    </td>
                    <td>
                        <?php echo $item['music_count']; ?>
                    </td>
                    <td align="right">
                        <div style="padding-right: 3px;">
                            <a title="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" href="<?php echo $item['seolink'];?>" target="_blank">
                                <img border="0" hspace="2" alt="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" src="images/actions/search.gif"/>
                            </a>
                            <a title="<?php echo $_LANG["EDIT"]; ?>" href="?view=components&do=config&id=<?php echo $id; ?>&opt=<?php if ($opt=="albums"){echo "edit_album";}else{echo "edit_singer";}?>&item_id=<?php echo $item['id']; ?>">
                                <img border="0" hspace="2" alt="<?php echo $_LANG["EDIT"]; ?>" src="images/actions/edit.gif"/>
                            </a>
                            <a title="<?php echo $_LANG["DEL"]; ?>" onclick="jsmsg('<?php echo $_LANG["DEL"]; ?> <?php echo htmlspecialchars($item['title']); ?>?', '?view=components&do=config&id=<?php echo $id; ?>&opt=<?php if ($opt=="albums"){echo "delete_album";}else{echo "delete_singer";}?>&item_id=<?php echo $item['id']; ?>')" href="#">
                                <img border="0" hspace="2" alt="<?php echo $_LANG["DEL"]; ?>" src="images/actions/delete.gif"/>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    <?php } else { ?>
        <tbody>
            <td colspan="7" style="padding-left:5px">
                <div style="padding:15px;padding-left:0px">
                    <?php
                        if ($opt == "albums"){
                            echo $_LANG["ALBUMS_NOT_FOUND"];
                        }else{
                            echo $_LANG["SINGERS_NOT_FOUND"];
                        }
                    ?>
                </div>
            </td>
        </tbody>
    <?php } ?>
</table>
<script type="text/javascript">highlightTableRows("listTable","hoverRow","clickedRow");</script>
<?php
    if ($pages>1){
        echo cmsPage::getPagebar($total, $page, $perpage, $base_uri.'&page=%page%');
    }
?>
<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<table  id="listTable" class="tablesorter" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:0px">
    <thead>
        <tr>
            <th class="lt_header"><?php echo $_LANG["NAME"]; ?></th>
            <th class="lt_header"><?php echo $_LANG["DO"]; ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($items as $item){ ?>
        <tr class="item_tr">
            <td>
                <div id="genre_name<?php echo $item['id']; ?>"><?php echo $item['genre_name']; ?></div>
            </td>
            <td>
                <a href="#" onclick="editgenre(<?php echo $item['id']; ?>,<?php echo $id; ?>);" title="<?php echo $_LANG["EDIT"]; ?>">
                    <img border="0" hspace="2" alt="<?php echo $_LANG["EDIT"]; ?>" src="images/actions/edit.gif"/>
                </a>
                <a href="#" onclick="deletegenre(<?php echo $item['id']; ?>,<?php echo $id; ?>);" title="<?php echo $_LANG["DEL"]; ?>">
                    <img src="images/actions/delete.gif" alt="<?php echo $_LANG["DEL"]; ?>">
                </a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
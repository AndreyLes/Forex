<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<form id="addform" name="addform" method="post" action="index.php?view=components&do=config&id=<?php echo $id;?>" enctype="multipart/form-data">
    <table class="proptable" width="100%" cellpadding="15" cellspacing="2">
        <tr>
            <!-- главная ячейка -->
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td><strong><?php echo $_LANG["NAME"]; ?></strong></td>
                        <td rowspan="2" width="50" align="center" valign="bottom">
                            <div style="padding:5px;border:dotted 1px #ccc;margin-left:10px;margin-right:10px;">
                                <img src="/images/music/<?php echo $mod['icon']; ?>" />
                            </div>
                        </td>
                        <td width="310">
                            <strong><?php echo $_LANG["ICON"]; ?><span style="color:gray">(32x32)</span></strong>
                        </td>
                        <td width="190" style="padding-left:6px">
                            <strong><?php echo $_LANG["TEMPLATE"]; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td><input name="title" type="text" id="title" style="width:99%" value="<?php echo $mod['title'];?>" /></td>
                        <td>
                            <input name="Filedata" type="file" />
                            <a href="http://www.iconsearch.ru/" target="_blank"><?php echo $_LANG["ICON_SEARCH"]; ?>...</a>
                        </td>
                        <td style="padding-left:6px"><input name="template" type="text" style="width:98%" value="<?php echo $mod['template'];?>" /></td>
                    </tr>
                </table>
                <div style="margin-top:12px"><strong>Родительская категория</strong></div>
                <div style="margin-top:7px">
                    <select name="parent_id" id="parent_id" style="width:99%;" multuple="1">
                        <option value="<?php echo $rootid; ?>" <?php if ($mod['parent_id'] == $rootid || !isset($mod['parent_id'])){ echo "selected";} ?>>-- <?php echo $_LANG["ROOT_CAT"]; ?> --</option>
                        <?php
                            if (isset($mod['parent_id'])){
                                echo $inCore->getListItemsNS("cms_music_category", $mod['parent_id']);
                            }else{
                                echo $inCore->getListItemsNS("cms_music_category");
                            }
                        ?>
                    </select>
                    <input type="hidden" name="old_parent_id" value="<?php echo $mod['parent_id']; ?>" />
                </div>
                <div style="margin-top:12px"><strong><?php echo $_LANG["DESCRIPTION"]; ?></strong></div>
                <div><textarea name="description" style="width:97%" rows="5" id="description"><?php echo $mod['description'] ?></textarea></div>
            </td>
            <!-- боковая ячейка -->
            <td width="300" valign="top" style="background:#ECECEC;">
            <?php ob_start(); ?>
                {tab=<?php echo $_LANG["PUBLICATION"]; ?>}
                <?php if ($opt=='edit_cat') {  ?>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px">
                        <tr>
                            <td width="20">
                                <input type="checkbox" name="update_seolink" id="update_seolink" value="1" onclick="$('.url_cat').slideToggle('fast');" />
                            </td>
                            <td><label for="update_seolink"><strong><?php echo $_LANG["UPDATE_SEOLINK"]; ?></strong></label></td>
                        </tr>
                    </table>
                <div class="url_cat" style="display:none;"><?php echo $_LANG["ATTENTION"]; ?></div>
                <?php } ?>
                <div style="margin-top:5px; <?php if ($opt=='edit_cat') {  ?>display:none;<?php } ?>" class="url_cat">
                    <strong><?php echo $_LANG["URL"]; ?></strong><br/>
                    <div style="color:gray"><?php echo $_LANG["GENERATED_FROM_TITLE"]; ?></div>
                    <div>
                        <input type="text" name="url" value="<?php echo $mod['url']; ?>" style="width:99%"/>
                    </div>
                </div>
                <table>
                    <tr>
                        <td><strong><?php echo $_LANG["ORDER"]; ?> №</strong></td>
                        <td>
                            <?php if (!$mod['ordering']){ $mod['ordering'] = 1; } ?>
                            <input name="ordering" type="text" id="ordering" style="width:50px" value="<?php echo $mod['ordering'];?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $_LANG["SONGS_ON_PAGE"]; ?></strong></td>
                        <td>
                            <input name="perpage" type="text" id="perpage" style="width:50px" value="<?php echo $mod['perpage'];?>" />
                        </td>
                    </tr>
                </table>
                <div style="margin-top:20px"><strong><?php echo $_LANG["SORT_SONGS"]; ?></strong></div>
                <div>
                    <select name="orderby" id="orderby" style="width:99%">
                        <option value="hits" <?php if ($mod['orderby'] == 'hits'){ echo "selected";} ?>><?php echo $_LANG["BY_POPULARITY"]; ?></option>
                        <option value="name" <?php if ($mod['orderby'] == 'name'){ echo "selected";} ?>><?php echo $_LANG["BY_NAME"]; ?></option>
                        <option value="pubdate" <?php if ($mod['orderby'] == 'pubdate' or !isset($mod['orderby'])){ echo "selected";} ?>><?php echo $_LANG["BY_DATE"]; ?></option>
                    </select>
                    <select name="orderto" id="orderto" style="width:99%">
                        <option value="DESC" <?php if ($mod['orderto'] == 'DESC' or !isset($mod['orderto'])){ echo "selected";} ?>><?php echo $_LANG["DESC"]; ?></option>
                        <option value="ASC" <?php if ($mod['orderto'] == 'ASC'){ echo "selected";} ?>><?php echo $_LANG["ASC"]; ?></option>
                    </select>
                </div>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="checklist">
                    <tr>
                        <td width="20"><input type="checkbox" name="published" id="published" value="1" <?php if ($mod['published'] || $opt=='add_cat') { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="published"><strong><?php echo $_LANG["PUBLISH_CAT"]; ?></strong></label></td>
                    </tr>
                    <tr>
                        <td width="20"><input type="checkbox" name="is_public" id="is_public" value="1" <?php if ($mod['is_public'] or $opt=="add_cat") { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="is_public"><strong><?php echo $_LANG["ACCEPT_MUSIC_FROM_USERS"]; ?></strong></label></td>
                    </tr>
                    <tr>
                        <td width="20"><input type="checkbox" name="showdesc" id="showdesc" value="1" <?php if ($mod['showdesc']) { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="showdesc"><strong><?php echo $_LANG["SHOW_DESCRIPTION"]; ?></strong></label></td>
                    </tr>
                    <tr>
                        <td width="20"><input type="checkbox" name="showrss" id="showrss" value="1" <?php if ($mod['showrss']) { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="showrss"><strong><?php echo $_LANG["SHOW_RSS"]; ?></strong></label></td>
                    </tr>
                </table>
                {tab=<?php echo $_LANG["ACCESS"]; ?>}
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="checklist" style="margin-top:5px">
                    <tr>
                        <td width="20" valign="top">
                            <?php
                                $style  = 'disabled="disabled"';
                                $public = 'checked="checked"';
                                if ($opt == 'edit_cat'){
                                    $sql2 = "SELECT * FROM cms_content_access WHERE content_id = ".$mod['id']." AND content_type = 'music'";
                                    $result2 = dbQuery($sql2);
                                    $ord = array();
                                    if (mysql_num_rows($result2)){
                                        $public = '';
                                        $style = '';
                                        while ($r = mysql_fetch_assoc($result2)){
                                            $ord[] = $r['group_id'];
                                        }
                                    }
                                }
                            ?>
                            <input name="is_access" type="checkbox" id="is_access" onclick="checkGroupList();" value="1" <?php echo $public; ?> />
                        </td>
                        <td><label for="is_access"><strong><?php echo $_LANG["SHARE"]; ?></strong></label></td>
                    </tr>
                </table>
                <div style="padding:5px">
                    <span class="hinttext">
                        <?php echo $_LANG["IF_OBSERVED"].","; echo $_LANG["CAT_NAME"]; echo $_LANG["IF_OBSERVED_NEXT"]; ?>
                    </span>
                </div>
                <div style="margin-top:10px;padding:5px;padding-right:0px;" id="grp">
                    <div>
                        <strong><?php echo $_LANG["SHOW_GROUPS"]; ?>:</strong><br />
                        <span class="hinttext">
                            <?php echo $_LANG["SELECT_MULTIPLE"]; ?>
                        </span>
                    </div>
                    <div>
                    <?php
                        echo '<select style="width: 99%" name="showfor[]" id="showin" size="6" multiple="multiple" '.$style.'>';
                        $sql    = "SELECT * FROM cms_user_groups";
                        $result = dbQuery($sql) ;
                        if (mysql_num_rows($result)){
                            while ($item=mysql_fetch_assoc($result)){
                                if(!$item['is_admin']){
                                    echo '<option value="'.$item['id'].'"';
                                    if ($opt=='edit_cat'){
                                        if (inArray($ord, $item['id'])){
                                            echo 'selected';
                                        }
                                    }
                                    echo '>';
                                    echo $item['title'].'</option>';
                                }
                            }
                        }
                        echo '</select>';
                    ?>
                    </div>
                </div>
                {tab=<?php echo $_LANG["SEO"]; ?>}
                <table style="width:100%">
                    <tr>
                        <td>
                            <div style="margin-top:5px">
                                <strong><?php echo $_LANG["TITLE"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["TITLE_NAME"]; ?></span>
                            </div>
                            <div>
                                <input name="pagetitle" type="text" id="pagetitle" style="width:99%" value="<?php echo $mod['pagetitle'] ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:20px">
                                <strong><?php echo $_LANG["META_KEYS"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["TAGS_DESC_OTHER"]; ?></span>
                            </div>
                            <div>
                                <textarea name="meta_keys" style="width:97%" rows="2" id="meta_keys"><?php echo $mod['meta_keys'] ?></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:20px">
                                <strong><?php echo $_LANG["DESCRIPTION"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["META_DESC_DESC"]; ?></span>
                            </div>
                            <div>
                                <textarea name="meta_desc" style="width:97%" rows="4" id="meta_desc"><?php echo $mod['meta_desc'] ?></textarea>
                            </div>
                        </td>
                    </tr>
                </table>
                {/tabs}
                <?php echo jwTabs(ob_get_clean()); ?>
            </td>
        </tr>
    </table>
    <p>
        <input name="add_mod" type="submit" id="add_mod" value="<?php if ($do=='add_cat') { echo $_LANG["CREATE_CAT"]; } else { echo $_LANG["SAVE_CAT"]; } ?>" />
        <input name="back" type="button" id="back" value="<?php echo $_LANG["CANSEL"]; ?>" onclick="window.history.back();"/>
        <input name="opt" type="hidden" id="opt" <?php if ($opt=='add_cat') { echo 'value="submit_cat"'; } else { echo 'value="update_cat"'; } ?> />
        <?php
            if ($opt=='edit_cat'){
                echo '<input name="item_id" type="hidden" value="'.$mod['id'].'" />';
            }
        ?>
    </p>
</form>
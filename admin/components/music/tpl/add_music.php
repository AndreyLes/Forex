<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<?php if(!isset($opt)){ $opt="add_music"; } ?>
<form action="index.php?view=components&do=config&id=<?php echo $id;?>" method="post" enctype="multipart/form-data" name="addform" id="upload_movie_form">
    <table class="proptable" width="100%" cellpadding="15" cellspacing="2">
        <tr>
            <!-- главная ячейка -->
            <td valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <div><strong><?php echo $_LANG["MUSIC_NAME"]; ?></strong></div>
                            <input name="name" type="text" id="name" style="width:99%" value="<?php echo htmlspecialchars($mod['name']);?>" />
                        </td>
                        <td valign="top" width="180" style="padding:0 5px">
                            <div><strong><?php echo $_LANG["USER"]; ?></strong></div>
                            <select name="user_id" style="width:180px">
                            <?php
                                $inUser=cmsUser::getInstance();
                                if (isset($mod['user_id'])) {
                                    echo $inCore->getListItems('cms_users', $mod['user_id'], 'nickname', 'ASC', 'is_deleted=0 AND is_locked=0', 'id', 'nickname');
                                }else{
                                    echo $inCore->getListItems('cms_users', $inUser->id, 'nickname', 'ASC', 'is_deleted=0 AND is_locked=0', 'id', 'nickname');
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                        <td>
                            <div style="margin-top:12px"><strong><?php echo $_LANG["DESCRIPTION"]; ?></strong></div>
                            <div><?php $inCore->insertEditor('description', $mod['description'], '400', '100%'); ?></div>
                        </td>
                    </tr>
                </table>
                <div><strong><?php echo $_LANG["TAGS"]; ?></strong></div>
                <div><input name="tags" type="text" id="tags" style="width:99%" value="<?php if (isset($mod['id'])) { echo cmsTagLine('music', $mod['id'], false); } ?>" /></div>
                <div align="center">
                    <input type="radio" name="type" onclick="$('#mus_download').show(); $('#mus_url').hide();" <?php if($opt=="add_music" or $opt == "submit_music" or $mod['music_del_url']){ echo "checked";} ?> value="1"><?php echo $_LANG["DOWNLOAD"]; ?>
                    <input type="radio" name="type" onclick="$('#mus_url').show(); $('#mus_download').hide();" <?php if($opt=="edit_music" and !$mod['music_del_url']){ echo "checked";} ?> value="2"><?php echo $_LANG["MUSIC_URL"]; ?>
                </div>
                <div id="mus_download" <?php if($opt=="edit_music" and !$mod['music_del_url']){ echo "style=\"display:none;\"";} ?>>
                    <strong><?php echo $_LANG["SELECT_MUSIC_FILE"]; ?></strong><br/>
                    <input name="upmp3" type="file" id="upmp3" style="width:99%" value="" />
                </div>
                <div id="mus_url" <?php if($opt=="add_music" or $opt == "submit_music" or $mod['music_del_url']){ echo "style=\"display:none;\"";} ?> >
                    <strong><?php echo $_LANG["INPUT_MUSIC_URL"]; ?></strong><br/>
                    <input name="music_url" type="text" id="music_url" style="width:99%" value="<?php echo $mod['music_url']; ?>" />
                </div>
                <div><strong><?php echo $_LANG["DOWNLOAD_URL"]; ?></strong><div style="color:gray"><?php echo $_LANG["DOWNLOAD_URL_DESC"]; ?></div></div>
                <div><input name="download_url" type="text" id="download_url" style="width:99%" value="<?php echo $mod['dowmload_url'] ?>" /></div>
            </td>
            <!-- боковая ячейка -->
            <td width="300" valign="top" style="background:#ECECEC;">
                <?php ob_start(); ?>
                {tab=<?php echo $_LANG["PUBLICATION"]; ?>}
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="checklist">
                    <tr>
                        <td width="20"><input type="checkbox" name="published" id="published" value="1" <?php if ($mod['published'] or $opt=='add_music' or $opt == "submit_music") { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="published"><strong><?php echo $_LANG["PUBLISH"]; ?></strong></label></td>
                    </tr>
                    <tr>
                        <td width="20"><input type="checkbox" name="is_new" id="is_new" value="1" <?php if ($mod['is_new']) { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="is_new"><strong><?php echo $_LANG["NEW"]; ?></strong></label></td>
                    </tr>
                    <tr>
                        <td width="20"><input type="checkbox" name="comments" id="comments" value="1" <?php if ($mod['comments'] or $opt=='add_music' or $opt == "submit_music") { echo 'checked="checked"'; } ?>/></td>
                        <td><label for="comments"><strong><?php echo $_LANG["IS_COMMENTS"]; ?></strong></label></td>
                    </tr>
                </table>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["CAT_NAME"]; ?></strong>
                </div>
                <div>
                    <select name="cat_id" id="cat_id" style="width:100%">
                        <?php $rootid = $model->getRootCategoryId(); ?>
                        <option value="<?php echo $rootid; ?>" <?php if ($mod['cat_id']==$rootid || !isset($mod['cat_id'])) { echo 'selected'; }?>>-- <?php echo $_LANG["ROOT_CAT"]; ?> --</option>
                        <?php
                            if ($opt=='edit_music'){
                                echo $inCore->getListItemsNS("cms_music_category", $mod['cat_id']);
                            }else{
                                echo $inCore->getListItemsNS("cms_music_category", $cat_id);
                            }
                        ?>
                    </select>
                </div>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["SINGER"]; ?></strong>
                </div>
                <div>
                    <select name="singer_id" id="singer_id" style="width:100%" onchange="new_singer()">
                        <option value=""><?php echo $_LANG["SELECT"]; ?></option>
                        <?php $model->orderby("singer_name", "ASC");
                            foreach ($model->getSingers(TRUE) as $singer){ ?>
                            <option value="<?php echo $singer['id']; ?>" <?php
                            if ($opt == "edit_music"){
                                if($singer['id']==$mod['singer_id']){echo 'selected';}
                            }else{
                                if($singer['id']==$singer_id){echo 'selected';}
                            }
                            ?>><?php echo $singer['title']; ?></option>
                        <?php } ?>
                        <option value="--"><?php echo $_LANG["ADD_NEW_SINGER"]; ?></option>
                    </select>
                </div>
                <div id="add_new_singer" style="display:none">
                    <input type="text" name="newsinger" value="" id="newsinger" style="width:99%"/>
                </div>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["ALBUM"]; ?></strong>
                </div>
                <div>
                    <select name="album_id" id="album" style="width:100%" onchange="new_album()">
                        <option value=""><?php echo $_LANG["SELECT"]; ?></option>
                        <?php $model->orderby("album_name", "ASC");
                            foreach ($model->getAlbums(TRUE) as $album){ ?>
                            <option value="<?php echo $album['id']; ?>" <?php
                            if ($opt == "edit_music"){
                                if($album['id']==$mod['album_id']){echo 'selected';}
                            }else{
                                if($album['id']==$album_id){echo 'selected';}
                            }
                            ?>><?php echo $album['title']; ?></option>
                        <?php } ?>
                        <option value="--"><?php echo $_LANG["ADD_NEW_ALBUM"]; ?></option>
                    </select>
                </div>
                <div id="add_new_album_" style="display:none">
                    <input type="text" name="newalbum" value="" id="newalbum" style="width:99%"/><br/>
                    <select name="newalbum_access" style="width:97%">
                        <option value="1"><?php echo $_LANG["ACCESS_ALL"]; ?></option>
                        <option value="2"><?php echo $_LANG["ACCESS_MY_FRIEND_ADMIN"]; ?></option>
                        <option value="3"><?php echo $_LANG["ACCESS_ME_ADMIN"]; ?></option>
                    </select>
                </div>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["GENRE"]; ?></strong>
                </div>
                <div>
                    <select name="genre_id" id="genre_id" style="width:100%">
                        <option value=""><?php echo $_LANG["SELECT"]; ?></option>
                        <?php foreach ($model->getGenres() as $genre){ ?>
                            <option value="<?php echo $genre['id']; ?>" <?php if($genre['id']==$mod['genre_id']){echo 'selected';} ?>><?php echo $genre['genre_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php if ($opt=='edit_music') {  ?>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px">
                        <tr>
                            <td width="20"><input type="checkbox" name="update_seolink" id="update_seolink" value="1" onclick="$('.url_movie').slideToggle('fast');" /></td>
                            <td><label for="update_seolink"><strong><?php echo $_LANG["UPDATE_SEOLINK"]; ?></strong></label></td>
                        </tr>
                    </table>
                    <div class="url_movie" style="display:none;"><?php echo $_LANG["ATTENTION_404"]; ?></div>
                <?php } ?>
                <div style="margin-top:5px; <?php if ($opt=='edit_music') {  ?>display:none;<?php } ?>" class="url_movie">
                    <strong><?php echo $_LANG["URL"]; ?></strong><br/>
                    <div style="color:gray"><?php echo $_LANG["NO_MUSIC_URL"]; ?></div>
                    <div>
                        <input type="text" name="url" value="<?php echo $mod['url']; ?>" style="width:90%"/> .html
                    </div>
                </div>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["TEMPLATE"]; echo $_LANG['2_musics']; ?></strong>
                </div>
                <div>
                    <input id="tpl" type="text" name="tpl" value="<?php echo $mod['tpl']; ?>" style="width:99%" />
                </div>
                <div style="margin-top:15px">
                    <strong><?php echo $_LANG["YOUTUBE"]; ?></strong><br/>
                    <div style="color:gray"><?php echo $_LANG["YOUTUBE_DESC"]; ?></div>
                </div>
                <div>
                    <input id="video_url" type="text" name="video_url" value="<?php echo $mod['video_url']; ?>" style="width:99%" />
                </div>
                {tab=<?php echo $_LANG["SEO"]; ?>}
                <table style="width:100%">
                    <tr>
                        <td>
                            <div style="margin-top:5px">
                                <strong><?php echo $_LANG["TITLE"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["TITLE_NAME"]; ?></span>
                            </div>
                            <div>
                                <input name="pagetitle" type="text" id="pagetitle" style="width:99%" value="<?php echo $mod['pagetitle'] ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:20px">
                                <strong><?php echo $_LANG["META_KEYS"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["TAGS_DESC_OTHER"]; ?></span>
                            </div>
                            <div>
                                <textarea name="meta_keys" style="width:97%" rows="2" id="meta_keys"><?php echo $mod['meta_keys'] ?></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:20px">
                                <strong><?php echo $_LANG["META_DESC"]; ?></strong><br/>
                                <span class="hinttext"><?php echo $_LANG["META_DESC_DESC"]; ?></span>
                            </div>
                            <div>
                                <textarea name="meta_desc" style="width:97%" rows="4" id="meta_desc"><?php echo $mod['meta_desc'] ?></textarea>
                            </div>
                        </td>
                    </tr>
                </table>
                {/tabs}
                <?php echo jwTabs(ob_get_clean()); ?>
            </td>
        </tr>
    </table>
    <?php if ($opt=="add_music" or $opt == "submit_music"){?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="checklist">
            <tr>
                <td width="20"><input type="checkbox" name="add_again" id="add_again" value="1" <?php if ($inCore->inRequest('added')){ echo 'selected="selected"'; } ?>/></td>
                <td><label for="add_again"><?php echo $_LANG["ADD_AGAIN"]; ?></label></td>
            </tr>
        </table>
    <?php } ?>
    <p>
        <input name="add_mod" type="submit"  id="btnSubmit" value="<?php if ($opt=="add_music" or $opt == "submit_music"){ echo $_LANG["ADD_MUSIC"]; }else{  echo $_LANG["SAVE_MUSIC"]; } ?>" />
        <input name="back2" type="button" id="back2" value="<?php echo $_LANG["CANSEL"]; ?>" onclick="window.history.back();"/>
        <input name="opt" type="hidden" <?php if ($opt=="add_music" or $opt == "submit_music") { echo 'value="submit_music"'; } else { echo 'value="update_music"'; } ?> />
        <?php
        if ($opt=='edit_music'){
            echo '<input name="item_id" type="hidden" value="'.$mod['id'].'" />';
            echo '<input name="music_del_url" type="hidden" value="'.$mod['music_del_url'].'" />';
            echo '<input name="oldmusic_url" type="hidden" value="'.$mod['music_url'].'" />';
            echo '<input name="oldsinger" type="hidden" value="'.$mod['singer_id'].'" />';
            echo '<input name="oldalbum" type="hidden" value="'.$mod['album_id'].'" />';
            echo '<input name="oldgenre" type="hidden" value="'.$mod['genre_id'].'" />';
        }
        ?>
    </p>
</form>
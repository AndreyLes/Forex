<?php
if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); }
/******************************************************************************/
//                                                                            //
//                             CMS Vadyus v1.8                                //
//                        http://www.CMS Vadyus.ru/                           //
//                                                                            //
//                   written by CMS Vadyus Team, 2007-2011                    //
//                produced by VadyusSoft, (www.Vadyussoft.ru)               //
//                                                                            //
//                        LICENSED BY GNU/GPL v2                              //
//                                                                            //
/******************************************************************************/

	//LOAD CURRENT CONFIG
	$cfg = $inCore->loadComponentConfig('calendar');

	cpAddPathway('Календарь событий', '?view=components&do=config&id='.$_REQUEST['id']);
	echo '<h3>Календарь событий</h3>';
	if (isset($_REQUEST['opt'])) { $opt = $inCore->request('opt', 'str'); } else { $opt = 'config'; }
	
	if($opt=='saveconfig'){	

        $cfg = array();
		$cfg['users_can_add']   = $inCore->request('users_can_add', 'int');
		$cfg['extract_picture']   = $inCore->request('extract_picture', 'int');
		$cfg['reminder']   = $inCore->request('reminder', 'int');
		$cfg['comments']  = $inCore->request('comments', 'int');
		$cfg['color_cal_header']  = $inCore->request('color_cal_header', 'str');
		$cfg['color_def_day']   = $inCore->request('color_def_day', 'str');      
		$inCore->saveComponentConfig('calendar', $cfg);		
		$msg = 'Настройки сохранены.';
		$opt = 'config';

	}	


	if ($opt == 'config') {

		if (@$msg) { echo '<p class="success">'.$msg.'</p>'; }
			
        $GLOBALS['cp_page_head'][] = '<script type="text/javascript" src="/includes/jquery/jquery.form.js"></script>';

		//DEFAULT VALUES
		if (!isset($cfg['users_can_add'])) { $cfg['users_can_add'] = 1; }	
		if (!isset($cfg['extract_picture'])) { $cfg['extract_picture'] = 1; }
		if (!isset($cfg['reminder'])) { $cfg['reminder'] = 1; }
		if (!isset($cfg['comments'])) { $cfg['comments'] = 1; } 
		if (!isset($cfg['color_cal_header'])) { $cfg['color_cal_header'] = '#CCCCEE'; }
		if (!isset($cfg['color_def_day'])) { $cfg['color_def_day'] = '#BDCCE4'; }      
		?>			

<form action="index.php?view=components&amp;do=config&amp;id=<?php echo (int)$_REQUEST['id'];?>" method="post" name="optform" target="_self" id="form1">

            <table width="600" border="0" cellpadding="0" cellspacing="10" class="proptable" style="border:none">
                <tr>
                    <td><strong>Разрешить зарегистрированным пользователям добавлять события:</strong></td>
                    <td width="250">
                        <input name="users_can_add" type="radio" value="1" <?php if (@$cfg['users_can_add']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="users_can_add" type="radio" value="0" <?php if (@!$cfg['users_can_add']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                </tr>
                <tr>
                    <td><strong>Извлекать первое изображение текста события в списке месяца:</strong></td>
                    <td width="250">
                        <input name="extract_picture" type="radio" value="1" <?php if (@$cfg['extract_picture']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="extract_picture" type="radio" value="0" <?php if (@!$cfg['extract_picture']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                </tr> 
                <tr>
                    <td valign="top">
                        <strong>Система напоминаний по email:</strong><br/><i>(необходим модуль "Календарь" на главной странице)</i>
                    </td>
                    <td valign="top">
                        <input name="reminder" type="radio" value="1" <?php if (@$cfg['reminder']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="reminder" type="radio" value="0" <?php if (@!$cfg['reminder']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                </tr>                               
                <tr>
                    <td valign="top">
                        <strong>Комментарии для событий:</strong>
                    </td>
                    <td valign="top">
                        <input name="comments" type="radio" value="1" <?php if (@$cfg['comments']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="comments" type="radio" value="0" <?php if (@!$cfg['comments']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                </tr>
             </table>
             <fieldset style="width: 500px"><legend>Виджет календаря</legend>
             <table width="600" border="0" cellpadding="0" cellspacing="10" class="proptable" style="border:none">
                <tr>
                    <td valign="top">
                        <strong>Цвет шапки:</strong>
                    </td>
                    <td valign="top">
                        <?php echo '<input name="color_cal_header" class="color_input" type="text" value="'.$cfg['color_cal_header'].'" /><span class="color_show" style="margin-left:10px; font-size: 17px; background-color:'.$cfg['color_cal_header'].'; color:'.$cfg['color_cal_header'].'">Hi!</span><span class="color_show" style="font-size: 17px; background-color:'.$cfg['color_cal_header'].'; color:'.$cfg['color_cal_header'].'">Hi!</span><span class="color_value" style="margin-left:10px; color: #AAAAAA"></span>' ?>
                    </td>
                </tr> 
                <tr>
                    <td valign="top">
                        <strong>Цвет дня по умолчанию:</strong>
                    </td>
                    <td valign="top">
                        <?php echo '<input name="color_def_day" class="color_input" type="text" value="'.$cfg['color_def_day'].'" /><span class="color_show" style="margin-left:10px; font-size: 17px; background-color:'.$cfg['color_def_day'].'; color:'.$cfg['color_def_day'].'">Hi!</span><span  class="color_show" style="font-size: 17px; background-color:'.$cfg['color_def_day'].'; color:'.$cfg['color_def_day'].'">Hi!</span><span class="color_value" style="margin-left:10px; color: #AAAAAA"></span>' ?>
                    </td>
                </tr>                                              
            </table>
            </fieldset> 

    <p>
        <input name="opt" type="hidden" id="do" value="saveconfig" />
        <input name="save" type="submit" id="save" value="Сохранить" />
        <input name="back" type="button" id="back" value="Отмена" onclick="window.location.href='index.php?view=components';"/>
    </p>
</form>

<?php } ?>
<script type="text/javascript">
$(function() {
   $(".color_input").bind('keyup mouseout', function () {
	 $(this).next().next().css('background-color', $(this).val()).css('color', $(this).val());
   });
   $(".color_show").click(function () {  
	 var bg10=/rgb\(\s?(\d+)\s?,\s?(\d+)\s?,\s?(\d+)\s?\)/.exec($(this).css('background-color')), temp;
	 if (bg10) $(this).nextAll(".color_value").html('#'+((temp=Number(bg10[1]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp)+((temp=Number(bg10[2]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp)+((temp=Number(bg10[3]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp));
   });
	
});

</script>
CREATE TABLE IF NOT EXISTS `#__preferal` (
  `id` int(11) NOT NULL auto_increment COMMENT 'Идентификатор',
  `ref_id_from` int(11) NOT NULL COMMENT 'Идентификатор реферала',
  `ref_id_to` int(11) NOT NULL COMMENT 'Идентификатор нового пользователя',
  `valid` TINYINT(1) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL COMMENT 'Время и дата регистрации',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__preferal_count` (
  `user_id` int(11) NOT NULL COMMENT 'Идентефиактор рефа',
  `count_ref` int(11) NOT NULL COMMENT 'Кол-во рефералов',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
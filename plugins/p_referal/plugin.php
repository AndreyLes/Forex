<?php

class p_referal extends cmsPlugin {

// ==================================================================== //

    public function __construct(){
        
        parent::__construct();

        // Информация о плагине

        $this->info['plugin']           = 'p_referal';
        $this->info['title']            = 'Реферал';
        $this->info['description']      = 'Учет реферальных регистраций';
        $this->info['author']           = 'Tronin Dima';
        $this->info['version']          = '1.2';

        $this->config['Карма']           = 5;
		$this->config['Рейтинг']           = 5;
		// 1 - карме 2 - рейтингу 3 - карма и рейтинг
		$this->config['Опция']           = 1;
		// Проверка валидности реферала
		$this->config['КармыПолучил']  = 0; // 0 - не проверяем, 1 и более - проверяем колличество кармы
		$this->config['РейтингЗаработал']  = 0; // 0 - не проверяем, 1 и более - проверяем колличество кармы
		$this->config['АватарЗагружен']  = 1; // 0 - не проверяем 1 - проверяем
		$this->config['КомментарииОпубликованы']  = 0; // 0 - не проверяем 1 - проверяем
		$this->config['СтатьиОпубликовано']  = 0; // 0 - не проверяем 1 - проверяем
		$this->config['ФотоОбщие']  = 0; // 0 - не проверяем 1 - проверяем
		$this->config['ФотоЛичные']  = 0; // 0 - не проверяем 1 - проверяем
		$this->config['ФорумТема']  = 0; // 0 - не проверяем 1 - проверяем
		$this->config['ФорумПост']  = 0; // 0 - не проверяем 1 - проверяем
//		$this->config['СозданБлог']  = 0; // 0 - не проверяем 1 - проверяем
//		$this->config['СозданКлуб']  = 0; // 0 - не проверяем 1 - проверяем
		
		// События, которые будут отлавливаться плагином

        $this->events[]                 = 'USER_REGISTER';
		$this->events[]                 = 'PRINT_PAGE_BODY';

		
    }

// ==================================================================== //

    /**
     * Процедура установки плагина
     * @return bool
     */
    public function install(){
		
		$inConf     = cmsConfig::getInstance();
		
		include($_SERVER['DOCUMENT_ROOT'].'/includes/dbimport.inc.php');
		dbRunSQL($_SERVER['DOCUMENT_ROOT'].'/plugins/p_referal/install.sql', $inConf->db_prefix);
		     
        return parent::install();

    }

// ==================================================================== //

    /**
     * Процедура обновления плагина
     * @return bool
     */
    public function upgrade(){
		
    if (!$inDB->isFieldExists('cms_preferal', 'valid')){
        $inDB->query("ALTER TABLE `cms_preferal` ADD `valid` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ref_id_to`");
        echo '<p>Поле <strong>valid</strong> добавлено в таблицу <strong>cms_preferal</strong></p>';
    }
	

        return parent::upgrade();

    }

// ==================================================================== //

    /**
     * Обработка событий
     * @param string $event
     * @param mixed $item
     * @return mixed
     */
    public function execute($event, $item){

        parent::execute();
		
		$inCore     = cmsCore::getInstance();
		
        switch ($event){
            case 'USER_REGISTER': $this->AddReferalBase(); break;
			case 'PRINT_PAGE_BODY': $this->ChekRefValid(); break;
        }

        return $item;

    }

// ==================================================================== //


	//Функция устарела и оставленая не знаю зачем. Вынесена в файл ref.php
    private function GetReferalLink() {
		
		$inCore     = cmsCore::getInstance();
        if ($inCore->inRequest('ref')) {
			$ref= $inCore->request('ref', 'int', 0);
			cmsUser::sessionPut('ref', $ref);
		}
		return;

    }
    
	private function ChekRefValid(){
	$inDB = cmsDatabase::getInstance();
	$inUser = cmsUser::getInstance();
	if ($inUser->id){ //Если юзер авторизирован
		$user_id = $inUser->id;
		if ($this->ChekRefUser($user_id)){ // Если юзер авторизирован и является не валидным рефералом
			if ($this->CheckRef($user_id)){ //Если юзер авторизирован и является не валидным рефералом и прошел проверку валидности
				$inDB->query("UPDATE cms_preferal SET valid = 1 WHERE ref_id_to = $user_id"); //меняем его статус на валидного
				$result = $inDB->get_field('cms_preferal',"ref_id_to = $user_id",'ref_id_from');
				$ref = $result['ref_id_from']; 
				$karma = $this->config['Карма'];
				$rating = $this->config['Рейтинг'];
				$option_kr = $this->config['Опция'];
				$this->SendMessadgeRef($ref,$karma,$rating,$option_kr);
				$this->SendPresentRef($ref,$karma,$rating,$option_kr);
				$this->SumTopRef($ref);				
			}
		}
		}
	return;
	}
	
	// Проверяем пользователь является ли он валидным рефералом
	private function ChekRefUser($user_id){
		$inDB = cmsDatabase::getInstance();
		$result = $inDB->rows_count('cms_preferal', "ref_id_to=$user_id AND valid = 0");
		if ($result == null){return false;}
			else {return true;}
			
	return;
	}
	
	// Проверяем параметры валидности заданные в конфиге
	private function CheckRef($user_id){
		$inCore = cmsCore::getInstance();
        $inDB = cmsDatabase::getInstance();
		$inUser = cmsUser::getInstance();
		
		$ParmCheck = 9; //всего доступных параметров
		$ParmChecked = 0; //текущее колличтество проверенных параметров

			if  ($config['КармыПолучил'] >= 1){
				$result = $inDB->get_field('cms_user_profiles',"user_id = $user_id",'karma');
				if ($result['karma'] >= $config['КармыПолучил']) {$ParmChecked = $ParmChecked +1;}
				}
				else{ $ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки		
			
			// проверка Рейтинга 2
			if  ($config['РейтингЗаработал'] >= 1){
				$result = $inDB->get_field('cms_users',"user_id = $user_id",'rating');
				if ($config['РейтингЗаработал'] >= $result['rating']) {$ParmChecked = $ParmChecked +1;}
				}
				else{ $ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки		
		
			// проверка Загружен ли аватар 3
			if  ($config['АватарЗагружен'] >= 1){
				$result = $inDB->get_field('cms_user_profiles',"user_id = $user_id",'imageurl');
				if ($result['imageurl'] <> null) {$ParmChecked = $ParmChecked +1;}
				}else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки
		
			// проверяем кол-во комментариев 4
			if  ($config['КомментарииОпубликованы'] >= 1){
				$result = $inDB->$inDB->rows_count('cms_comments', "user_id=$user_id AND published = 1");
				if ($result >= $config['КомментарииОпубликованы']) {	$ParmChecked = $ParmChecked +1;}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки

			// проверяем кол-во статей 5
			if  ($config['СтатьиОпубликовано'] >= 1){
				$result = $inDB->rows_count('cms_content', "user_id=$user_id AND published = 1");
				if ($result >= $config['СтатьиОпубликовано']) {	$ParmChecked = $ParmChecked +1;}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки

			// загрузил фотографий в общие альбомы 6
			if  ($config['ФотоОбщие'] >= 1){
				$result = $inDB->rows_count('cms_photo_files', "user_id=$user_id AND published = 1");
				if ($result >= $config['ФотоОбщие']) {	$ParmChecked = $ParmChecked +1;echo'ФотоОбщие';}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки

			// загрузил фотографии в личные альбомы 7
			if  ($config['ФотоЛичные'] >= 1){
				$result = $inDB->rows_count('cms_user_photos', "user_id=$user_id");
				if ($result >= $config['ФотоЛичные']) {	$ParmChecked = $ParmChecked +1;}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки
		
			// проверяем кол-во тем на форуме 8
			if  ($config['ФорумТема'] >= 1){
				$result = $inDB->rows_count('cms_forum_threads', "user_id=$user_id");
				if ($result >= $config['ФорумТема']) {	$ParmChecked = $ParmChecked +1;}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки
		
		
			// проверяем кол-во постов на форуме 9
			if  ($config['ФорумПост'] >= 1){
				$result =  $inDB->rows_count('cms_forum_posts', "user_id=$user_id");
				if ($result >= $config['ФорумПост']) {	$ParmChecked = $ParmChecked +1;}
				}
				else{$ParmCheck = $ParmCheck - 1;} //Параметр не задан для проверки. Уменьшаем колличество параметров для проверки
		
		if ($ParmCheck == $ParmChecked){return true;} // Если все параметры совпадают с выполненными то считать пользователя валидным
			else {return false;}
		return;
	}
	
	//Добавляем нового реферала в базу и выставляем статус не валидного
	private function AddReferalBase() {
		$inUser = cmsUser::getInstance();		
		 if (cmsUser::sessionGet('ref')){		 
			$inDB   = cmsDatabase::getInstance();		
			$ref = cmsUser::sessionGet('ref');
			$ref_to = dbLastId('cms_users');			
			$inDB->query("INSERT INTO cms_preferal (id, ref_id_from, ref_id_to, valid,time) VALUES (null, '$ref', '$ref_to', '0',NOW())");
			}
        return;
	}
  	
	
	private function SendPresentRef($ref,$karma,$rating,$option_kr) {
		
        $inCore     = cmsCore::getInstance();
        $inDB   = cmsDatabase::getInstance();
		
		if ($option_kr < 3) {
				if ($option_kr == 1){
						$inDB->query("INSERT INTO cms_user_karma (user_id, sender_id, points, senddate) VALUES ('$ref', 1, '$karma', NOW())");
						$inDB->query("UPDATE cms_user_profiles SET karma = karma + '$karma' WHERE user_id = $ref");
						}
						else {
						$inDB->query("UPDATE cms_users SET rating = rating + '$rating' WHERE id = $ref");						
						}
			}
				else {
					$inDB->query("INSERT INTO cms_user_karma (user_id, sender_id, points, senddate) VALUES ('$ref', 1, '$karma', NOW())");
					$inDB->query("UPDATE cms_user_profiles SET karma = karma + '$karma' WHERE user_id = $ref");
					$inDB->query("UPDATE cms_users SET rating = rating + '$rating' WHERE id = $ref");
				}
				
        return;  
	}
// ==================================================================== //



	private function SendMessadgeRef($ref,$karma,$rating,$option_kr) {
        $inUser     = cmsUser::getInstance();		
		//    $letter_path    = PATH.'/plugins/p_referal/message_ref.txt';
   //     $letter         = file_get_contents($letter_path);
  //      $letter .= .$getProfileURL(). '/n';
		
		$letter = 'За привлечение нового пользователя вам начисленно ';		 
		 if ($option_kr <3){
			if ($option_kr == 1){
				$letter .='кармы '.$karma.'';
					}else{
				$letter .='рейтинга '.$rating.'';
				}}else{
				$letter .='кармы '.$karma.' и рейтинга '.$rating.'';
					 }		
		$letter .= ' С Уважением администрация сайта';
        //отправляем ему личное сообщение
        cmsUser::sendMessage(USER_UPDATER, $ref, $letter);		    
        return;  
	}
	
	private function SumTopRef($ref) {
		$inDB   = cmsDatabase::getInstance();
		$sql = "SELECT u.user_id
				FROM cms_preferal_count u
				WHERE u.user_id = ".$ref;		
		$result = $inDB->query($sql);
	
		if ($inDB->num_rows($result)){
			$inDB->query("UPDATE cms_preferal_count SET count_ref = count_ref + 1 WHERE user_id = $ref");
		}
			else {
				$inDB->query("INSERT INTO cms_preferal_count (user_id, count_ref) VALUES ('$ref', 1)");
				}	
	    
        return;  
	}

// ==================================================================== //

}

?>

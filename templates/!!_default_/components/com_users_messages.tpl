{if $friends || $is_admin}
    <div class="float_bar">
        <a href="javascript:void(0)" class="new_link" onclick="users.sendMess(0, 0, this);return false;" title="{$LANG.NEW_MESS}:"><span class="ajaxlink">{$LANG.WRITE}</span></a>
    </div>
{/if}
<div class="con_heading">{$LANG.MY_MESS}</div>
		<div class="usr_msgmenu_tabs">
			{if $opt == 'in'}
				<span class="usr_msgmenu_active in_span">{$page_title} {if $new_messages.messages}({$new_messages.messages}){/if}</span>
				<a class="usr_msgmenu_link out_link" href="/users/{$id}/messages-sent.html">{$LANG.SENT}</a>
				<a class="usr_msgmenu_link notices_link" href="/users/{$id}/messages-notices.html">{$LANG.NOTICES} {if $new_messages.notices}({$new_messages.notices}){/if}</a>
				<a class="usr_msgmenu_link history_link" href="/users/{$id}/messages-history.html">{$LANG.DIALOGS}</a>
			{elseif $opt == 'out'}
			<a class="usr_msgmenu_link in_link" href="/users/{$id}/messages-history.html">{$LANG.DIALOGS}</a>
				<span class="usr_msgmenu_active out_span">{$page_title}</span>
				<a class="usr_msgmenu_link notices_link" href="/users/{$id}/messages-notices.html">{$LANG.NOTICES} {if $new_messages.notices}({$new_messages.notices}){/if}</a>
		
			{elseif $opt == 'notices'}
				<a class="usr_msgmenu_link in_link" href="/users/{$id}/messages-history.html">{$LANG.DIALOGS}</a>
				<a class="usr_msgmenu_link out_link" href="/users/{$id}/messages-sent.html">{$LANG.SENT}</a>
				<span class="usr_msgmenu_active notices_span">{$page_title} {if $new_messages.notices}({$new_messages.notices}){/if}</span>
			{elseif $opt == 'history'}
			
				<span class="usr_msgmenu_active in_span">{$page_title}</span>
			
				<a class="usr_msgmenu_link out_link" href="/users/{$id}/messages-sent.html">{$LANG.SENT}</a>
				<a class="usr_msgmenu_link notices_link" href="/users/{$id}/messages-notices.html">{$LANG.NOTICES} {if $new_messages.notices}({$new_messages.notices}){/if}</a>
			
				
				
			{/if}
		</div>
<div class="usr_msgmenu_bar" style='height:20px'>
   <div style='padding-right: 10px;float: left;border-right: 1px dashed #999999;'> <strong style='font-size:15px;'>{$LANG.MESS_INBOX}:</strong> <span id="msg_count" style='font-size:15px;'>{$msg_count}</span></div>
	<form style='width:40%;float:left;margin-bottom:0px' id='message_img'>
		<input type='image' src='/images/extrennyj.png' name=''/><span>Экстренные</span>
		<input type='image' src='/images/important.png' name=''/><span>Важные</span>
		<input type='image' src='/images/info.png' name=''/><span>Информационные </span>
	</form>
{if ($opt!='history') && $msg_count>0}
    <div><a href="javascript:void(0)" onclick="users.cleanCat('/users/{$id}/delmessages-{$opt}.html');return false;">{$LANG.CLEAN_CAT}</a></div>
{/if}   

{if $opt=='history'}
	<div>
        <form action="" id="history" method="post">
            <select name="with_id" id="with_id" style="width:330px;height: 20px;" onchange="changeFriend();">
                <option value="0">{$LANG.FRIEND_FOR_DIALOGS}</option>
					{if $interlocutors}
						{$interlocutors}
					{/if}
            </select>
        </form>
    </div>
{/if}
</div>
{if $records}
    {foreach key=tid item=record from=$records}
    <div class="usr_msg_entry" style='margin-left:{$tid}0px' id="usr_msg_entry_id_{$record.id}">
        <table cellspacing="4">
        <tr>
			<td style='border-right: dashed 1px #999999;padding-left: 5px;padding-right: 5px;width: 20px;padding-left: 8px;padding-right: 9px;'>
				<input type='checkbox' id='checkbox-id{$tid}' value='v1' name='check1'/><label for="checkbox-id{$tid}"></label>
			</td>
			<td style='border-right: dashed 1px #999999;padding-left: 11px;padding-right: 11px;'>
				<img src='/images/info.png'/>
			</td>
            <td width="70" height="70" style='border-right: dashed 1px #999999;padding-left:18px;padding-right:18px' valign="middle" style='' align="center">
			{$record.authorlink}
                {if $record.sender_id > 0}
                    <a href="{profile_url login=$record.author_login}"><img border="0" class="usr_img_small" src="{$record.user_img}" /></a>
                {else}
                    <img border="0" class="usr_img_small" src="{$record.user_img}" />
                {/if}
                <div style="margin: 4px 0 0 0;">{$record.online_status}</div>
            </td>
            <td width="" valign="top" style='width: 60%;'>
				<span class="usr_msg_date" >{$record.fpubdate}</span>
				<div style="padding:6px">{$record.message}</div>
			</td>
			<td  valign="top">
			<table>
				<tr>
				<td style='width:98px;'>{if $record.from_id != $user_id}<a href="javascript:void(0)" class="msg_reply" onclick="users.sendMess('{$record.from_id}', '{$record.id}', this);return false;" title="{$LANG.NEW_MESS}: {$record.author|escape:'html'}"><span class="otvet">{$LANG.REPLY}</span></a>
			{/if}</td><td>
			<a class="msg_history" href="/users/{$id}/messages-history{$record.from_id}.html">{$LANG.HISTORY}</a>
			</td>
			<!--<a class="msg_delete" href="javascript:void(0)" onclick="users.deleteMessage('{$record.id}')"><span class="ajaxlink">{*$LANG.DELETE*}</span></a>-->
				</tr>
			</table>
			</td>
        </tr>
        </table>
    </div>
    {/foreach}
    {$pagebar}
			{else}
				<p style="padding:20px 10px">{$LANG.NOT_MESS_IN_CAT}</p>
			{/if}
{literal}
	<script type="text/javascript">
        function changeFriend(){
            fr_id = $("#with_id option:selected").val();
            if(fr_id != 0) {
                $("#history").attr("action", '/users/{/literal}{$id}{literal}/messages-history'+fr_id+'.html');
                $('#history').submit();
            }
        }
	</script>
{/literal}
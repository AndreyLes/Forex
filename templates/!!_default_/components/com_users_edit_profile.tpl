{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_css file='includes/jquery/tabs/tabs.css'}

{literal}
	<script type="text/javascript">
		$(function(){$(".uitabs").tabs();});
	</script>
{/literal}

	<div id='usr' style='padding-bottom: 6px;'>
 <div class="head_edit" id="nickname">
        {$usr.nickname} {if $usr.banned}<span style="color:red; font-size:12px;">{$LANG.USER_IN_BANLIST}</span>{/if}
    </div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
                            <img alt="{$usr.nickname|escape:'html'}" class="usr_img" src="{$usr.avatar}" />
                        </div>

							<div id="usermenu" style="">
                            <div class="usr_profile_menu">
							<table cellpadding="0" cellspacing="6" ><tr>
	
                                 <tr>
                                    <td><img src="/templates/{template}/images/icons/profile/avatar.png"  border="0"/></td>
                                    <td><a href="/users/{$usr.id}/avatar.html" title="{$LANG.SET_AVATAR}">{$LANG.SET_AVATAR}</a></td>
                                </tr>
						   </table></div>
                      	</td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">
						<div class="field">
							<div class="title">{$LANG.LAST_VISIT}:</div>
							<div class="value">{$usr.flogdate}</div>
							<div>, IP:{$usr.last_ip}</div>
						</div>
						<div>
							
						</div>
						 {if $usr.city}
						<div class="field">
							<div class="title">{$LANG.CITY}:</div>
                            <div class="value"><a href="/users/city/{$usr.cityurl|escape:'html'}">{$usr.city}</a></div>
						</div>
                        {/if}
						<div class="field">
							<div class="title">{$LANG.DATE_BIRTHDAY}:</div>
							<div class="value">
								{$usr.birthdate}
                            </div>
						</div>
                        {if $usr.inv_login}
                            <div class="field">
                                <div class="title">{$LANG.INVITED_BY}:</div>
                                <div class="value">
                                    <a href="{profile_url login=$usr.inv_login}">{$usr.inv_nickname}</a>
                                </div>
                            </div>
                        {/if}


						{if $usr.showbirth && $usr.fbirthdate}
						<div class="field">
							<div class="title">{$LANG.BIRTH}:</div>
							<div class="value">{$usr.fbirthdate}</div>
						</div>
						{/if}

								{add_js file='includes/jquery/jquery.nospam.js'}
							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="{$usr.email|NoSpam}" class="email">{$usr.email}</a></div>
							</div>
							{literal}
								<script>
										$('.email').nospam({ replaceText: true });
								</script>
							{/literal}
						<div class="field">
							<div class="title">Тел.:</div>
							<div class="value">
								{$usr.mobtelephone}
                            </div>
						</div>
				

						{if $cfg.privforms && $usr.form_fields}
							{foreach key=tid item=field from=$usr.form_fields}
                                <div class="field">
                                    <div class="title">{$field.title}:</div>
                                    <div class="value">{if $field.field}{$field.field}{else}<em>{$LANG.NOT_SET}</em>{/if}</div>
                                </div>
                            {/foreach}
						{/if}

					</div>

				</div>

                {foreach key=id item=plugin from=$plugins}
                    <div id="upr_{$plugin.name}">{$plugin.html}</div>
                {/foreach}

			</div>
	</td>
  </tr>
</table>

</div>



<div id='usr'>
<div class="head_edit">{$LANG.CONFIG_PROFILE}</div>
<div id="profiletabs" class="uitabs">
    <ul id="tabs">
        <li><a href="#about"><span>{$LANG.ABOUT_ME}</span></a></li>
        <li><a href="#notices"><span>{$LANG.NOTIFIC}</span></a></li>
        <li rel="hid"><a href="#change_password"><span>{$LANG.CHANGING_PASS}</span></a></li>
		<li><a href="#verification"><span>{$LANG.VERIFICATION}</span></a></li>
		<li><a href="#change_color"><span>{$LANG.CHANGE_COLOR}</span></a></li>
    </ul>

    <form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="opt" value="save" />
        <div id="about">
				
		<h4>
		Регистрационные данные
		</h4>
		<div id='underline'>
			<span>
			
			</span>
		</div>
            <table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<tr class='info'>
					<td>Ваш ник</td>
					<td>
					{$usr.login|escape:'html'}
					</td>
				</tr>
                <tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_NAME}: </strong><br />
                    </td>
                    <td valign="top"><label name="nickname" type="text" id="nickname" style="width:300px" >{$usr.nickname|escape:'html'}</label></td>
                </tr>
				<tr class='info'>
					<td>Обращение(предпочтительный язык обращения)</td>
					<td>русский</td>
				</tr>
                <tr class='info'>
                    <td valign="top">
                        <strong>{$LANG.CITY}:</strong><br />
               
                    </td>
                    <td valign="top">
                        {city_input value=$usr.city name="city" width="300px"}
                    </td>
                </tr>
				<tr class='info'>
					<td>Моб.телефон</td>
					<td>{$usr.mobtelephone}</td>
				
				
				</tr>
				
							<tr class='info'>
                    <td width="300" valign="top">
                        <strong>E-mail:</strong><br />
                      
                    </td>
                    <td valign="top">
                        <label name="email" type="text"  id="email" style="width:300px">{$usr.email}</label>
                    </td>
                </tr>
				<tr class='info'>
					<td>Ваш друг:</td>
					<td>{$usr.inv_nickname}</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align:center'>Красной звездочкой (<span style='color:red'> * </span>) выведелены поля, обязательные для заполения.</td>
				
				</tr>
             
            </table>
			
							
		<h4>
			Персональные данные
		</h4>
		<div id='underline'>
			<span>
			
			</span>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
                <tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_NAME}: </strong><br />
                        <span class="usr_edithint">{$LANG.YOUR_NAME_TEXT}</span>
                    </td>
                    <td valign="top"><input name="nickname" type="text" class="text-input" id="nickname" style="width:300px" value="{$usr.nickname|escape:'html'}"/></td>
                </tr>
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>E-mail:</strong><br />
                        <span class="usr_edithint">{$LANG.REALY_ADRESS_EMAIL}</span>
                    </td>
                    <td valign="top">
                        <input name="email" type="text" class="text-input" id="email" style="width:300px" value="{$usr.email}"/>
                    </td>
                </tr>
                <tr class='info'>
                    <td valign="top">
                        <strong>{$LANG.CITY}:</strong><br />
                        <span class="usr_edithint">{$LANG.CITY_TEXT}</span>
                    </td>
                    <td valign="top">
                        {city_input value=$usr.city name="city" width="300px"}
                    </td>
                </tr>
                <tr class='info select_td'>
                    <td valign="top"><strong>{$LANG.BIRTH}:</strong> </td>
                    <td valign="top">
                        {dateform seldate=$usr.birthdate}
                    </td>
                </tr>
                <tr class='info select_td'>
                    <td valign="top"><strong>{$LANG.DOCS}:</strong> </td>
                    <td valign="top"><input class="text-input" style='width:300px' type='text' /></td>
                </tr>
				<tr class='info'>	
					<td></td>
					<td><input style='width:300px' class="text-input" type='text' /></td>
				</tr>	
				<tr class='info'>	
					<td></td>
					<td>
						<input style='width:300px' class="text-input" type='text' />
					</td>
				</tr>
               	<tr class='info'>	
					<td width="300">Адрес</td>
					<td>
						<input style='width:300px' class="text-input" type='text' />
					</td>
				</tr>
					<tr class='info'>	
					<td >Индекс</td>
					<td>
						<input style='width:300px' class="text-input" type='text' />
					</td>
				</tr>
            </table>
			
        </div>
        <div id="notices">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="458" valign="top">
                        <strong class='form_size'>
                            {$LANG.NOTIFY_NEW_MESS}:
                        </strong><br/>
                        <span class="usr_edithint">
                            {$LANG.NOTIFY_NEW_MESS_TEXT}
                        </span>
                    </td>
                    <td valign="top">
                        <label style='margin-right:43px;'><input name="email_newmsg" type="radio" value="1" {if $usr.email_newmsg}checked{/if}/> {$LANG.YES} </label>
                        <label><input name="email_newmsg" type="radio" value="0" {if !$usr.email_newmsg}checked{/if}/> {$LANG.NO}</label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong class='form_size'>{$LANG.HOW_NOTIFY_NEW_MESS} </strong><br />
                        <span class="usr_edithint">{$LANG.WHERE_TO_SEND}</span>
                    </td>
                    <td valign="top">
					
		
					
									<span id="nav">
									{if $usr.cm_subscribe=='mail'}
										{$LANG.TO_EMAIL}
									{elseif $usr.cm_subscribe=='priv'}
										{$LANG.TO_PRIVATE_MESS}
									{elseif $usr.cm_subscribe=='both'}
										{$LANG.TO_EMAIL_PRIVATE_MESS}
									{elseif $usr.cm_subscribe=='none'}
										{$LANG.NOT_SEND}
									{/if}
									</span>
						<ol class="selectsubs">
						
							<li data-value="mail">{$LANG.TO_EMAIL}</li>
							<li data-value="priv">{$LANG.TO_PRIVATE_MESS}</li>
							<li data-value="both">{$LANG.TO_EMAIL_PRIVATE_MESS}</li>
							<li data-value="none">{$LANG.NOT_SEND}</li>
							
						</ol>
					
					<input type='hidden' id="cm_subscribe" name="cm_subscribe" />
					
					
					
					
					
                    </td>
                </tr>
            </table>
        </div>
		
		<!-- Вивід верифікації -->
	     <div id="verification">
		 {foreach key=tid item=result from=$results_file}
		 {if $tid eq 0}
		 <h4>
			Первый уровень верификации
		 </h4>
		 <div id='underline' style='width: 900px;'>
			<span></span>
		 </div>
		 <div style='margin-top:26px'>
		 <div style='float:left'> 
		 		<span class='ver_span'>Документ,	подтверждающий личность</span>
		 </div>
			 <div class='right_block'>
					{if $result.filename}
						<a href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'><img src='/images/users/docs/small/{$result.filename}'/></a>
									<p>{$LANG.FIRST_LEVEL}</p>
					{else}
						<input name="passport{$tid}" type="file" id="passport" size="30" />
						<p>{$LANG.FIRST_LEVEL}</p>
					{/if}
			</div>
		 </div>
		 
		 
		 
		 
		
			{elseif $tid eq 1}
		 <h4>
			Второй уровень верификации
		 </h4>
		 <div id='underline' style='width: 900px;'>
			<span></span>
		 </div>
			<p id='second_lvl'>{$LANG.CANT_VER}</p>
			<div style='float:left'>
				<span class='ver_span'>Фото пользователя с документом в руке</span>
			</div>
			<div class='right_block'>
				{if $result.filename}
				<a href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'>	<img src='/images/users/docs/small/{$result.filename}'/></a>
							
							<p>{$LANG.SECOND_LEVEL}</p>
					{else}
				<input name="passport{$tid}" type="file" id="passport" size="30" />
						<p>{$LANG.SECOND_LEVEL}</p>
				{/if}
		
			 </div>	
			{elseif $tid eq 2}
						 <h4>
							Подтвердить наличие банковской карты
						 </h4>
						 <div id='underline' style='width: 900px;margin-bottom:29px'>
							<span></span>
						 </div>
						<div style='float:left'>
							<span class='ver_span'>Банковская карта, подтверждающая наличие счета</span>
						</div>
						<div  class='right_block'>
								{if $result.filename}
									<a href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'><img src='/images/users/docs/small/{$result.filename}'/></a>
										<p>{$LANG.THIRD_LEVEL}</p>
									{else}
								<input name="passport{$tid}" type="file" id="passport" size="30" /><p>{$LANG.THIRD_LEVEL}</p>
								{/if}
						</div>
						 <div  id='last_block'>
								<div style='float:left'>
									<span class='ver_span' style='margin-top:10px'>Также Вы можете</span>
								</div>
							<div  class='right_block'>
								<button id='kartka'><span>Заказать банковскою карту</span> </button>
							</div>
						</div>
			{/if}
		{/foreach}
				
			
        </div>
					<div id='change_color'>

<!-- BEGIN STYLESHEET SWITCHER -->

				
							<ul id="stylesheets">
								<li>
									<a id='dark' href="javascript:switchStylestyle('dark');">
										<img src='/images/red.png'/>
										<input type='checkbox' id='checkbox-id' value='v1' name='check1'/>
										<label for="checkbox-id"></label>
									</a>
								</li>
								<li>
									<a id='blue' href="javascript:switchStylestyle('blue');">
										<img src='/images/blue.png'/>
										<input type='checkbox' id='checkbox-id1' value='v1' name='check1'/>
										<label for="checkbox-id1"></label>
									</a>
								</li>
								<li>
									<a id='green' href="javascript:switchStylestyle('green');">
										<img src='/images/green.png'/>
										<input type='checkbox' id='checkbox-id2' value='v1' name='check1'/>
										<label for="checkbox-id2"></label>
									</a>
								</li>
							</ul>
				
					</div>
		<div style="margin-top: 12px;text-align:center" id="submitform">
				<button name="save"  type="submit"  id="save2"><span>{$LANG.SAVE}</span></button>
				<button name="save" name="delbtn2"  type="button"  id="delbtn2" onclick="location.href='/users/{$usr.id}/delprofile.html';">
						<span>{$LANG.DEL_PROFILE}</span>
				</button>			 
        </div>
    </form>
    <div id="change_password">
        <form id="editform" name="editform" method="post" action="">
            <input type="hidden" name="opt" value="changepass" />
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="150" valign="top">
                        <strong>{$LANG.OLD_PASS}:</strong>
                    </td>
                    <td valign="top">
                        <input name="oldpass" type="password" id="oldpass" class="text-input" size="30" />
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS}:</strong></td>
                    <td valign="top"><input name="newpass" type="password"  id="newpass" class="passes text-input" size="30" />
					
					<a id='showpass'>Показать пароль</a></td>
					
				</tr>
				<tr>
					<td></td>
					<td>
						<ul id='galki'>
							<li><span>Минимум 2 латинские буквы</span></li>
							<li><span>Минимум 2 цифры</span></li>
							<li><span>Минимум 10 символов</span></li>
							<li><span>Максимум 16 символов</span></li>
						</ul>
					</td>
				</tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS_REPEAT}:</strong></td>
                    <td valign="top">
					<input name="newpass2" type="password" class="passes text-input" id="newpass2" size="30" /><span id='galkun'></span></td>
                </tr>
            </table>
            <div style="margin-top: 72px;text-align:center">
			<button name="save2" disabled='true' type="submit"  id="save2"><span>{$LANG.CHANGE_PASSWORD}</span></button>
			<button id="clear" type='button'><span>{$LANG.CLEAR_PASSWORD}</span></button>
               
            </div>
        </form>
    </div>
</div>
</div>




	<!-- Верификация -->


<!-- СМЕНА ПАРОЛЯ -->
{literal}
	<script type="text/javascript">
	$(document).ready(function(){
	$('#showpass').click(function(){
	
		$('#newpass').attr('type','text');
	});
	
	});
	$('#dark').click(function(){
		$('#checkbox-id').trigger('click');
	});
		
	$('#blue').click(function(){
	
		$('#checkbox-id1').trigger('click');
	});
	$('#green').click(function(){
		$('#checkbox-id2').trigger('click');
	});
	$('#clear').bind('click',function(){
			$('#oldpass').val('');
			$('#newpass').val('');
			$('#newpass2').val('');
	});
    $(function(){
            $( '#tabs li' ).click( function(){
                rel = $( this ).attr( "rel" );
                if(!rel){
                    $('#submitform').show();
                } else {
                    $('#submitform').hide();
                }
            });
        });
	</script>
{/literal}
{literal}
	<script>
	
$('.passes').bind("change keyup input click", function() {
		   if (this.value.match(/[^0-9a-zA-Z]/g)) {
				this.value = this.value.replace(/[^0-9a-zA-Z]/g, '');
		   }
  });
	
$('#newpass').focusout(function(){
				$('#galki li').removeClass('galochka');
						
				var test = $('#newpass').val();
				var errors = 0;
					if (--test.split(/[a-z]/).length>1)
					{
						$('#galki li:first-child').addClass('galochka');
					}
					else{
					  var errors = 1;
					}
						
					if (--test.split(/[0-9]/).length>1)
						{
							$('#galki li:nth-child(2)').addClass('galochka');
						}
					else
						{
						 var errors = 1;
						}
					
					if (--test.split(/[a-zA-z0-9]/).length>9 && --test.split(/[a-zA-z0-9]/).length<17)
						{
							$('#galki li:nth-child(3)').addClass('galochka');
						}
					else
						{
							 var errors = 1;
						}
					if (--test.split(/[a-zA-z0-9]/).length<17 && --test.split(/[a-zA-z0-9]/).length>1)
						{
							$('#galki li:last-child').addClass('galochka');
						}
					else	
						{
							var errors = 1;
						}
						var test2 = $('#newpass2').val();
					if(errors ==true)
					{
						$('#save2').prop('disabled',true);
					}
	});
	$('#newpass2').focusout(function()
	{
		var test = $('#newpass').val();
		var test2 = $('#newpass2').val();
		if(test == test2)
			{
				$('#save2').prop('disabled',false);
				$('#galkun').addClass('galkun');
			}
		else
			{
				$('#save2').prop('disabled',true);
			}
	});
	
	
	var nav = $('#nav');
	var selection = $('.selectsubs');
	var select = selection.find('li');
	nav.click(function(event) {
		if (nav.hasClass('active')) {
			nav.removeClass('active');
			selection.stop().slideUp(200);
		} else {
			nav.addClass('active');
			selection.stop().slideDown(200);
		}
		event.preventDefault();
		});
	select.click(function(event) {
		select.removeClass('active');
		nav.trigger('click');
		$('#cm_subscribe').attr('value', $(this).attr('data-value'));
	 });
	</script>
{/literal}

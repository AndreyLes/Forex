<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }
/* 
 * Created by Firs Yuriy
 * e-mail: firs.yura@gmail.com
 * site: firs.org.ua
 */
$_LANG['LATESTBOARD_NOT_ADV']         ='Нет объявлений для отображения.';
?>
<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }
/* 
 * Created by Firs Yuriy
 * e-mail: firs.yura@gmail.com
 * site: firs.org.ua
 */
$_LANG['FRIEND_ON_SITE']      = 'Друзья на сайте';
$_LANG['FRIEND_NO_SITE']      = 'Нет друзей на сайте';

?>
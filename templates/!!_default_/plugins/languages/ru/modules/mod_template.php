<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

$_LANG['TEMPLATE_DEFAULT']       = 'По умолчанию';
$_LANG['TEMPLATE_CHOOSE']        = 'Выбрать';

?>
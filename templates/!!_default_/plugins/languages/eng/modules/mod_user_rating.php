<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }
/* 
 * Created by Firs Yuriy
 * e-mail: firs.yura@gmail.com
 * site: firs.org.ua
 */
$_LANG['USER_RATING_NO_DATA']       = 'Нет данных для отображения.';
$_LANG['USER_RATING_RATING']        = 'Рейтинг';
$_LANG['USER_RATING_CARMA']         = 'Карма';

?>
<?php /* Smarty version 2.6.28, created on 2014-05-15 19:47:48
         compiled from com_forum_view_thread.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'template', 'com_forum_view_thread.tpl', 22, false),array('function', 'csrf_token', 'com_forum_view_thread.tpl', 37, false),array('function', 'profile_url', 'com_forum_view_thread.tpl', 58, false),array('modifier', 'escape', 'com_forum_view_thread.tpl', 25, false),array('modifier', 'rating', 'com_forum_view_thread.tpl', 75, false),array('modifier', 'spellcount', 'com_forum_view_thread.tpl', 92, false),array('modifier', 'truncate', 'com_forum_view_thread.tpl', 120, false),)), $this); ?>
<h1 class="con_heading" id="thread_title"><?php echo $this->_tpl_vars['thread']['title']; ?>
</h1>

<div id="thread_description" <?php if (! $this->_tpl_vars['thread']['description']): ?>style="display: none"<?php endif; ?>><?php echo $this->_tpl_vars['thread']['description']; ?>
</div>

<?php if ($this->_tpl_vars['user_id']): ?>
<table width="100%" cellspacing="0" cellpadding="5"  class="forum_toolbar"><tr>
    <td width="5">&nbsp;</td>
    <td class="forum_toollinks">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_toolbar.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </td>
</tr></table>
<?php endif; ?>

<?php if ($this->_tpl_vars['thread_poll']): ?>
    <div id="thread_poll"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_thread_poll.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php endif; ?>

<table class="posts_table" width="100%" cellspacing="2" cellpadding="5" border="0" bordercolor="#999999">
    <?php $_from = $this->_tpl_vars['posts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pid'] => $this->_tpl_vars['post']):
?>
    <tr>
        <td colspan="2" class="darkBlue-LightBlue">
            <div class="post_date"><?php if ($this->_tpl_vars['post']['pinned'] && $this->_tpl_vars['num'] > 1): ?><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/sticky.png" width="14px;" alt="<?php echo $this->_tpl_vars['LANG']['ATTACHED_MESSAGE']; ?>
" title="<?php echo $this->_tpl_vars['LANG']['ATTACHED_MESSAGE']; ?>
" />  <?php endif; ?><strong><a name="<?php echo $this->_tpl_vars['post']['id']; ?>
" href="/forum/thread<?php echo $this->_tpl_vars['thread']['id']; ?>
-<?php echo $this->_tpl_vars['page']; ?>
.html#<?php echo $this->_tpl_vars['post']['id']; ?>
">#<?php echo $this->_tpl_vars['num']; ?>
</a></strong> - <?php echo $this->_tpl_vars['post']['fpubdate']; ?>
, <?php echo $this->_tpl_vars['post']['wday']; ?>
</div>
            <?php if ($this->_tpl_vars['user_id'] && ! $this->_tpl_vars['thread']['closed']): ?>
                <div class="msg_links">
                    <a href="javascript:" onclick="forum.addQuoteText(this);return false;" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" class="ajaxlink" title="<?php echo $this->_tpl_vars['LANG']['ADD_SELECTED_QUOTE']; ?>
"><?php echo $this->_tpl_vars['LANG']['ADD_QUOTE_TEXT']; ?>
</a> | <a href="/forum/thread<?php echo $this->_tpl_vars['thread']['id']; ?>
-quote<?php echo $this->_tpl_vars['post']['id']; ?>
.html" title="<?php echo $this->_tpl_vars['LANG']['REPLY_FULL_QUOTE']; ?>
"><?php echo $this->_tpl_vars['LANG']['REPLY']; ?>
</a>
                    <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder'] || $this->_tpl_vars['post']['is_author_can_edit']): ?>
                        | <a href="/forum/editpost<?php echo $this->_tpl_vars['post']['id']; ?>
-<?php echo $this->_tpl_vars['page']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['EDIT']; ?>
</a>
                        <?php if ($this->_tpl_vars['num'] > 1): ?>
                            <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder']): ?>
                                | <a href="javascript:" onclick="forum.movePost('<?php echo $this->_tpl_vars['thread']['id']; ?>
','<?php echo $this->_tpl_vars['post']['id']; ?>
');return false;" class="ajaxlink" title="<?php echo $this->_tpl_vars['LANG']['MOVE_POST']; ?>
"><?php echo $this->_tpl_vars['LANG']['MOVE']; ?>
</a>
                                <?php if (! $this->_tpl_vars['post']['pinned']): ?>
                                | <a href="/forum/pinpost<?php echo $this->_tpl_vars['thread']['id']; ?>
-<?php echo $this->_tpl_vars['post']['id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['PIN']; ?>
</a>
                                <?php else: ?>
                                | <a href="/forum/unpinpost<?php echo $this->_tpl_vars['thread']['id']; ?>
-<?php echo $this->_tpl_vars['post']['id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['UNPIN']; ?>
</a>
                                <?php endif; ?>
                            <?php endif; ?>
                            | <a href="javascript:" class="ajaxlink" onclick="forum.deletePost(<?php echo $this->_tpl_vars['post']['id']; ?>
, '<?php echo smarty_function_csrf_token(array(), $this);?>
', <?php echo $this->_tpl_vars['page']; ?>
);"><?php echo $this->_tpl_vars['LANG']['DELETE']; ?>
</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </td>
    </tr>
    <tr class="posts_table_tr">
        <td class="post_usercell" width="140" align="center" valign="top" height="150">
            <div>
                <a class="post_userlink" href="javascript:" onclick="addNickname(this);return false;" title="<?php echo $this->_tpl_vars['LANG']['ADD_NICKNAME']; ?>
" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" ><?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a>
            </div>
            <div class="post_userrank">
                <?php if ($this->_tpl_vars['post']['userrank']['group']): ?>
                    <span class="<?php echo $this->_tpl_vars['post']['userrank']['class']; ?>
"><?php echo $this->_tpl_vars['post']['userrank']['group']; ?>
</span>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['post']['userrank']['rank']): ?>
                    <span class="<?php echo $this->_tpl_vars['post']['userrank']['class']; ?>
"><?php echo $this->_tpl_vars['post']['userrank']['rank']; ?>
</span>
                <?php endif; ?>
            </div>
            <div class="post_userimg">
                <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['post']['login']), $this);?>
" title="<?php echo $this->_tpl_vars['LANG']['GOTO_PROFILE']; ?>
"><img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['post']['avatar_url']; ?>
" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" /></a>
                <?php if ($this->_tpl_vars['post']['user_awards']): ?>
                    <div class="post_userawards">
                        <?php $_from = $this->_tpl_vars['post']['user_awards']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['aid'] => $this->_tpl_vars['award']):
?>
                            <img src="/images/icons/award.gif" border="0" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['award']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['award']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"/>
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="post_usermsgcnt"><?php echo $this->_tpl_vars['LANG']['MESSAGES']; ?>
: <?php echo $this->_tpl_vars['post']['post_count']; ?>
</div>
            <?php if ($this->_tpl_vars['post']['city']): ?>
                <div class="post_usermsgcnt"><?php echo $this->_tpl_vars['post']['city']; ?>
</div>
            <?php endif; ?>
            <div><?php echo $this->_tpl_vars['post']['flogdate']; ?>
</div>
        </td>
        <td width="" class="post_msgcell" align="left" valign="top">
        <?php if ($this->_tpl_vars['thread']['closed'] || ! $this->_tpl_vars['user_id'] || $this->_tpl_vars['post']['is_author'] || $this->_tpl_vars['post']['is_voted']): ?>
            <div class="votes_links"><?php echo ((is_array($_tmp=$this->_tpl_vars['post']['rating'])) ? $this->_run_mod_handler('rating', true, $_tmp) : smarty_modifier_rating($_tmp)); ?>
</div>
        <?php else: ?>
            <div class="votes_links" id="votes<?php echo $this->_tpl_vars['post']['id']; ?>
">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><?php echo ((is_array($_tmp=$this->_tpl_vars['post']['rating'])) ? $this->_run_mod_handler('rating', true, $_tmp) : smarty_modifier_rating($_tmp)); ?>
</td>
                <td><a href="javascript:void(0);" onclick="forum.votePost(<?php echo $this->_tpl_vars['post']['id']; ?>
, -1);"><img border="0" alt="-" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/comments/vote_down.gif" style="margin-left:8px"/></a></td>
                <td><a href="javascript:void(0);" onclick="forum.votePost(<?php echo $this->_tpl_vars['post']['id']; ?>
, 1);"><img border="0" alt="+" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/comments/vote_up.gif" style="margin-left:2px"/></a></td>
                </tr></table>
            </div>
        <?php endif; ?>
            <div class="post_content"><?php echo $this->_tpl_vars['post']['content_html']; ?>
</div>
            <?php if ($this->_tpl_vars['post']['attached_files'] && $this->_tpl_vars['cfg']['fa_on']): ?>
                <div id="attached_files_<?php echo $this->_tpl_vars['post']['id']; ?>
">
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_attached_files.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </div>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['post']['edittimes']): ?>
                <div class="post_editdate"><?php echo $this->_tpl_vars['LANG']['EDITED']; ?>
: <?php echo ((is_array($_tmp=$this->_tpl_vars['post']['edittimes'])) ? $this->_run_mod_handler('spellcount', true, $_tmp, $this->_tpl_vars['LANG']['COUNT1'], $this->_tpl_vars['LANG']['COUNT2'], $this->_tpl_vars['LANG']['COUNT1']) : smarty_modifier_spellcount($_tmp, $this->_tpl_vars['LANG']['COUNT1'], $this->_tpl_vars['LANG']['COUNT2'], $this->_tpl_vars['LANG']['COUNT1'])); ?>
 (<?php echo $this->_tpl_vars['LANG']['LAST_EDIT']; ?>
: <?php echo $this->_tpl_vars['post']['peditdate']; ?>
)</div>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['post']['signature_html']): ?>
                <div class="post_signature"><?php echo $this->_tpl_vars['post']['signature_html']; ?>
</div>
            <?php endif; ?>
        </td>
    </tr>
    <?php $this->assign('num', ($this->_tpl_vars['num']+1)); ?>
    <?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['page'] == $this->_tpl_vars['lastpage']): ?><a name="new"></a><?php endif; ?>

<?php if ($this->_tpl_vars['user_id']): ?>
<table width="100%" cellspacing="0" cellpadding="5"  class="forum_toolbar"><tr>
    <td><a href="#"><?php echo $this->_tpl_vars['LANG']['GOTO_BEGIN_PAGE']; ?>
</a></td>
    <td class="forum_toollinks">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_toolbar.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </td>
</tr>
</table>
<?php endif; ?>

<div class="forum_navbar">
    <table width="100%"><tr>
        <td align="left">
            <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-left:auto;margin-right:auto"><tr>
                <?php if ($this->_tpl_vars['prev_thread']): ?>
                    <td align="right" width="">
                        <div>&larr; <a href="/forum/thread<?php echo $this->_tpl_vars['prev_thread']['id']; ?>
.html" title="<?php echo $this->_tpl_vars['LANG']['PREVIOUS_THREAD']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['prev_thread']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30)); ?>
</a></div>
                    </td>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['prev_thread'] && $this->_tpl_vars['next_thread']): ?><td>|</td><?php endif; ?>
                <?php if ($this->_tpl_vars['next_thread']): ?>
                    <td align="left" width="">
                        <div><a href="/forum/thread<?php echo $this->_tpl_vars['next_thread']['id']; ?>
.html" title="<?php echo $this->_tpl_vars['LANG']['NEXT_THREAD']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['next_thread']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30)); ?>
</a> &rarr;</div>
                    </td>
                <?php endif; ?>
            </tr></table>
        </td>
        <td width="150" align="right"><?php echo $this->_tpl_vars['LANG']['GOTO_FORUM']; ?>
: </td>
        <td width="220" align="right">
            <select name="goforum" id="goforum" style="width:220px; margin:0px" onchange="window.location.href = '/forum/' + $(this).val();">
            <?php $_from = $this->_tpl_vars['forums']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fid'] => $this->_tpl_vars['item']):
?>
                <?php if ($this->_tpl_vars['item']['cat_title'] != $this->_tpl_vars['last_cat_title']): ?>
                    <?php if ($this->_tpl_vars['last_cat_title']): ?></optgroup><?php endif; ?>
                    <optgroup label="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['cat_title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
">
                <?php endif; ?>
                <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['forum']['id']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['item']['title']; ?>
</option>
                <?php if ($this->_tpl_vars['item']['sub_forums']): ?>
                    <?php $_from = $this->_tpl_vars['item']['sub_forums']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sid'] => $this->_tpl_vars['sub_forum']):
?>
                        <option value="<?php echo $this->_tpl_vars['sub_forum']['id']; ?>
" <?php if ($this->_tpl_vars['sub_forum']['id'] == $this->_tpl_vars['forum']['id']): ?> selected="selected" <?php endif; ?>>--- <?php echo $this->_tpl_vars['sub_forum']['title']; ?>
</option>
                    <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
                <?php $this->assign('last_cat_title', $this->_tpl_vars['item']['cat_title']); ?>
            <?php endforeach; endif; unset($_from); ?>
            </optgroup>
            </select>
        </td>
    </tr></table>
</div>

<div style="float: right;margin: 8px 0 0;"><?php echo $this->_tpl_vars['pagebar']; ?>
</div>

<?php if ($this->_tpl_vars['cfg']['fast_on'] && ! $this->_tpl_vars['thread']['closed']): ?>
<div class="forum_fast">
    <div class="forum_fast_header"><?php echo $this->_tpl_vars['LANG']['FAST_ANSWER']; ?>
</div>
    <?php if ($this->_tpl_vars['user_id'] && $this->_tpl_vars['is_can_add_post']): ?>
        <?php if ($this->_tpl_vars['cfg']['fast_bb']): ?>
            <div class="usr_msg_bbcodebox">
                <?php echo $this->_tpl_vars['bb_toolbar']; ?>

            </div>
            <?php echo $this->_tpl_vars['smilies']; ?>

        <?php endif; ?>
        <div class="forum_fast_form">
            <form action="/forum/reply<?php echo $this->_tpl_vars['thread']['id']; ?>
.html" method="post" id="msgform">
                <input type="hidden" name="gosend" value="1" />
                <input type="hidden" name="csrf_token" value="<?php echo smarty_function_csrf_token(array(), $this);?>
" />
                <div class="cm_editor">
                    <textarea id="message" name="message" rows="5"></textarea>
                </div>
                <div class="forum_fast_submit" style="float:right;padding:5px;"><input type="button" value="<?php echo $this->_tpl_vars['LANG']['SEND']; ?>
" onclick="$(this).prop('disabled', true);$('#msgform').submit();" /></div>
                <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder'] || $this->_tpl_vars['thread']['is_mythread']): ?>
                    <div style="float:right;padding:8px;">
                        <label><input type="checkbox" name="fixed" value="1" /> <?php echo $this->_tpl_vars['LANG']['TOPIC_FIXED_LABEL']; ?>
</label>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    <?php else: ?>
        <div style="padding:5px"><?php echo $this->_tpl_vars['LANG']['FOR_WRITE_ON_FORUM']; ?>
.</div>
    <?php endif; ?>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['user_id']): ?>
<?php echo '
<script type="text/javascript" language="JavaScript">
    $(document).ready(function(){
        $(\'.darkBlue-LightBlue .msg_links\').css({opacity:0.4, filter:\'alpha(opacity=40)\'});
        $(\'.posts_table_tr\').hover(
            function() {
                $(this).prev().find(\'.msg_links\').css({opacity:1.0, filter:\'alpha(opacity=100)\'});
            },
            function() {
                $(this).prev().find(\'.msg_links\').css({opacity:0.4, filter:\'alpha(opacity=40)\'});
            }
        );
        $(\'.msg_links\').hover(
            function() {
                $(this).css({opacity:1.0, filter:\'alpha(opacity=100)\'});
            },
            function() {
                $(this).css({opacity:0.4, filter:\'alpha(opacity=40)\'});
            }
        );
    });
</script>
'; ?>

<?php endif; ?>
<?php /* Smarty version 2.6.28, created on 2014-06-18 16:06:32
         compiled from com_users_info.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_info.tpl', 1, false),array('function', 'add_css', 'com_users_info.tpl', 2, false),array('function', 'template', 'com_users_info.tpl', 18, false),array('function', 'profile_url', 'com_users_info.tpl', 136, false),array('modifier', 'escape', 'com_users_info.tpl', 52, false),array('modifier', 'NoSpam', 'com_users_info.tpl', 159, false),)), $this); ?>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/tabs/jquery.ui.min.js'), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/jquery/tabs/tabs.css'), $this);?>


<?php echo '
	<script type="text/javascript">
        $(function(){$(".uitabs").tabs();});
	</script>
'; ?>


<div id="usertitle">

    <div id="user_ratings">
        <div class="karma" title="<?php echo $this->_tpl_vars['LANG']['KARMA']; ?>
">
        	<div class="<?php if ($this->_tpl_vars['usr']['karma'] >= 0): ?>value-positive<?php else: ?>value-negative<?php endif; ?>" id="u_karma_cont">
                <table cellpadding="2" cellspacing="0"><tr>
                    <td class="sign_link" style="color:green">
                    <?php if ($this->_tpl_vars['usr']['can_change_karma']): ?>
                        <a href="javascript:void(0)" onclick="users.changeKarma('<?php echo $this->_tpl_vars['usr']['id']; ?>
', 'plus');return false;" title="<?php echo $this->_tpl_vars['LANG']['KARMA']; ?>
 +"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/karma_up.png" border="0" alt="<?php echo $this->_tpl_vars['LANG']['KARMA']; ?>
 +"/></a>
                    <?php endif; ?>
                    </td>
                    <td><span class="user_karma_point" id="u_karma"><?php echo $this->_tpl_vars['usr']['karma']; ?>
</span></td>
                    <td style="color:red" class="sign_link">
                    <?php if ($this->_tpl_vars['usr']['can_change_karma']): ?>
                        <a href="javascript:void(0)" onclick="users.changeKarma('<?php echo $this->_tpl_vars['usr']['id']; ?>
', 'minus'); return false;" title="<?php echo $this->_tpl_vars['LANG']['KARMA']; ?>
 -"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/karma_down.png" border="0" alt="<?php echo $this->_tpl_vars['LANG']['KARMA']; ?>
 -"/></a>
                    <?php endif; ?>
                    </td>
                </tr></table>
            </div>
        </div>
        <div class="rating" title="<?php echo $this->_tpl_vars['LANG']['RATING']; ?>
">
            <div class="value"><?php echo $this->_tpl_vars['usr']['user_rating']; ?>
</div>
        </div>
    </div>

    <div class="user_group_name">
        <div class="<?php echo $this->_tpl_vars['usr']['group_alias']; ?>
"><?php echo $this->_tpl_vars['usr']['grp']; ?>
</div>
    </div>

    <div class="con_heading" id="nickname">
        <?php echo $this->_tpl_vars['usr']['nickname']; ?>
 <?php if ($this->_tpl_vars['usr']['banned']): ?><span style="color:red; font-size:12px;"><?php echo $this->_tpl_vars['LANG']['USER_IN_BANLIST']; ?>
</span><?php endif; ?>
    </div>

</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
                            <img alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" class="usr_img" src="<?php echo $this->_tpl_vars['usr']['avatar']; ?>
" />
                        </div>

						<?php if ($this->_tpl_vars['is_auth']): ?>
							<div id="usermenu" style="">
                            <div class="usr_profile_menu">
							<table cellpadding="0" cellspacing="6" ><tr>

					

                          
                         	<?php if ($this->_tpl_vars['myprofile']): ?>
                                 <tr>
                                    <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/avatar.png" border="0"/></td>
                                    <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/avatar.html" title="<?php echo $this->_tpl_vars['LANG']['SET_AVATAR']; ?>
"><?php echo $this->_tpl_vars['LANG']['SET_AVATAR']; ?>
</a></td>
                                </tr>
								<?php if ($this->_tpl_vars['usr']['invites_count']): ?>
                                <tr>
                                    <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/invites.png" border="0"/></td>
                                    <td><a href="/users/invites.html" title="<?php echo $this->_tpl_vars['LANG']['MY_INVITES']; ?>
"><?php echo $this->_tpl_vars['LANG']['MY_INVITES']; ?>
</a> <?php echo $this->_tpl_vars['usr']['invites_count']; ?>
</td>
                                </tr>
								<?php endif; ?>
                                <tr>
                                    <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/edit.png" border="0"/></td>
                                    <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/editprofile.html" title="<?php echo $this->_tpl_vars['LANG']['CONFIG_PROFILE']; ?>
"><?php echo $this->_tpl_vars['LANG']['MY_CONFIG']; ?>
</a></td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['is_admin'] && ! $this->_tpl_vars['myprofile']): ?>
                            <tr>
                                <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/edit.png" border="0"/></td>
                                <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/editprofile.html" title="<?php echo $this->_tpl_vars['LANG']['CONFIG_PROFILE']; ?>
"><?php echo $this->_tpl_vars['LANG']['CONFIG_PROFILE']; ?>
</a></td>
                            </tr>
                            <?php endif; ?>
                       		<?php if (! $this->_tpl_vars['myprofile']): ?>
                            	<?php if ($this->_tpl_vars['is_admin']): ?>
                                	<?php if (! $this->_tpl_vars['usr']['banned']): ?>
                                    <tr>
                                        <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/award.png" border="0"/></td>
                                        <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/giveaward.html" title="<?php echo $this->_tpl_vars['LANG']['TO_AWARD']; ?>
"><?php echo $this->_tpl_vars['LANG']['TO_AWARD']; ?>
</a></td>
                                    </tr>
                                    <?php if ($this->_tpl_vars['usr']['id'] != 1): ?>
                                    <tr>
                                        <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/ban.png" border="0"/></td>
                                        <td><a href="/admin/index.php?view=userbanlist&do=add&to=<?php echo $this->_tpl_vars['usr']['id']; ?>
" title="<?php echo $this->_tpl_vars['LANG']['TO_BANN']; ?>
"><?php echo $this->_tpl_vars['LANG']['TO_BANN']; ?>
</a></td>
                                    </tr>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                <?php if ($this->_tpl_vars['usr']['id'] != 1): ?>
                                    <tr>
                                        <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/delprofile.png" border="0"/></td>
                                        <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/delprofile.html" title="<?php echo $this->_tpl_vars['LANG']['DEL_PROFILE']; ?>
"><?php echo $this->_tpl_vars['LANG']['DEL_PROFILE']; ?>
</a></td>
                                    </tr>
                                <?php endif; ?>
                                <?php endif; ?>
                         	<?php endif; ?>

                            </table></div>
                            </div>
						<?php endif; ?>
					</td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">

						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['LAST_VISIT']; ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['usr']['flogdate']; ?>
</div>
						</div>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['DATE_REGISTRATION']; ?>
:</div>
							<div class="value">
                                <?php echo $this->_tpl_vars['usr']['fregdate']; ?>

                            </div>
						</div>
                        <?php if ($this->_tpl_vars['usr']['inv_login']): ?>
                            <div class="field">
                                <div class="title"><?php echo $this->_tpl_vars['LANG']['INVITED_BY']; ?>
:</div>
                                <div class="value">
                                    <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['usr']['inv_login']), $this);?>
"><?php echo $this->_tpl_vars['usr']['inv_nickname']; ?>
</a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->_tpl_vars['usr']['city']): ?>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['CITY']; ?>
:</div>
                            <div class="value"><a href="/users/city/<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['cityurl'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"><?php echo $this->_tpl_vars['usr']['city']; ?>
</a></div>
						</div>
                        <?php endif; ?>

						<?php if ($this->_tpl_vars['usr']['showbirth'] && $this->_tpl_vars['usr']['fbirthdate']): ?>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['BIRTH']; ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['usr']['fbirthdate']; ?>
</div>
						</div>
						<?php endif; ?>

					
						<?php if ($this->_tpl_vars['usr']['showmail']): ?>
							<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/jquery.nospam.js'), $this);?>

							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['email'])) ? $this->_run_mod_handler('NoSpam', true, $_tmp) : smarty_modifier_NoSpam($_tmp)); ?>
" class="email"><?php echo $this->_tpl_vars['usr']['email']; ?>
</a></div>
							</div>
							<?php echo '
								<script>
										$(\'.email\').nospam({ replaceText: true });
								</script>
							'; ?>

						<?php endif; ?>

				

						<?php if ($this->_tpl_vars['cfg']['privforms'] && $this->_tpl_vars['usr']['form_fields']): ?>
							<?php $_from = $this->_tpl_vars['usr']['form_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['field']):
?>
                                <div class="field">
                                    <div class="title"><?php echo $this->_tpl_vars['field']['title']; ?>
:</div>
                                    <div class="value"><?php if ($this->_tpl_vars['field']['field']): ?><?php echo $this->_tpl_vars['field']['field']; ?>
<?php else: ?><em><?php echo $this->_tpl_vars['LANG']['NOT_SET']; ?>
</em><?php endif; ?></div>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
						<?php endif; ?>

					</div>

				</div>

                <?php $_from = $this->_tpl_vars['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['plugin']):
?>
                    <div id="upr_<?php echo $this->_tpl_vars['plugin']['name']; ?>
"><?php echo $this->_tpl_vars['plugin']['html']; ?>
</div>
                <?php endforeach; endif; unset($_from); ?>

			</div>
	</td>
  </tr>
</table>
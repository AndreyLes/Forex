<?php /* Smarty version 2.6.28, created on 2014-06-04 16:54:07
         compiled from com_actions_friends.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'com_actions_friends.tpl', 6, false),array('function', 'template', 'com_actions_friends.tpl', 10, false),array('modifier', 'escape', 'com_actions_friends.tpl', 17, false),)), $this); ?>
<?php if ($this->_tpl_vars['friends_total']): ?>
<div id="fr_body">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="action_friends">
  <tr>
    <?php if ($this->_tpl_vars['page'] > 1): ?>
    <td width="20px" id="td<?php echo smarty_function_math(array('equation' => "z - 1",'z' => $this->_tpl_vars['page']), $this);?>
"><a href="javascript:void(0);" onclick="listFriends(<?php echo smarty_function_math(array('equation' => "z - 1",'z' => $this->_tpl_vars['page']), $this);?>
);" class="arr_btn">&laquo;</a></td>
	<?php else: ?>
    <td width="85px" id="fr0" <?php if (! $this->_tpl_vars['user_id']): ?>class="selected"<?php endif; ?>>
    	<div class="action_fr">
    		<a href="javascript:void(0);" onclick="selectUser(0);" title="<?php echo $this->_tpl_vars['LANG']['ALL_FRIENDS']; ?>
"><img alt="all" border="0" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/actions_people.png"  /></a>
        </div>
    </td>
    <?php endif; ?>
    <?php $_from = $this->_tpl_vars['friends']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['friend']):
?>
        <td width="85px" id="fr<?php echo $this->_tpl_vars['friend']['id']; ?>
" <?php if ($this->_tpl_vars['user_id'] == $this->_tpl_vars['friend']['id']): ?>class="selected"<?php endif; ?>>
            <div class="action_fr">
                <a href="javascript:void(0);" onclick="selectUser(<?php echo $this->_tpl_vars['friend']['id']; ?>
);" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['friend']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"><img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['friend']['avatar']; ?>
" /></a>
            </div>
        </td>
    <?php endforeach; endif; unset($_from); ?>
    <?php if ($this->_tpl_vars['page'] != $this->_tpl_vars['total_pages']): ?>
    <td align="right" id="td<?php echo smarty_function_math(array('equation' => "z + 1",'z' => $this->_tpl_vars['page']), $this);?>
"><a href="javascript:void(0);" onclick="listFriends(<?php echo smarty_function_math(array('equation' => "z + 1",'z' => $this->_tpl_vars['page']), $this);?>
);" class="arr_btn">&raquo;</a></td>
    <?php else: ?>
    <td>&nbsp;</td>
    <?php endif; ?>
  </tr>
</table>
<?php echo '
<script type="text/javascript">
function selectUser(user_id){
	$(\'#actions_list\').css({opacity:0.4, filter:\'alpha(opacity=40)\'});
	$(\'input[name=user_id]\').val(user_id);
	$(\'td.selected\').removeClass(\'selected\');
	$(\'#fr\'+user_id).addClass(\'selected\');
	$.post(\'/actions\', {user_id: user_id, \'do\': \'view_user_feed_only\'}, function(data){
		$(\'#actions_list\').html(data);
		$(\'#actions_list\').css({opacity:1.0, filter:\'alpha(opacity=100)\'});
	});
}
function listFriends(page, user_id){
	$(\'table.action_friends\').css({opacity:0.4, filter:\'alpha(opacity=40)\'});
	var user_id = $(\'input[name=user_id]\').val();
	$.post(\'/actions\', {page: page, user_id: user_id, \'do\': \'view_user_friends_only\'}, function(data){
		$(\'#fr_body\').html(data);
	});
}
</script>
'; ?>

</div>
<?php endif; ?>
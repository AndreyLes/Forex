<?php /* Smarty version 2.6.28, created on 2014-05-18 21:07:21
         compiled from mod_uc.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'mod_uc.tpl', 7, false),array('modifier', 'truncate', 'mod_uc.tpl', 12, false),)), $this); ?>
<?php if ($this->_tpl_vars['cfg']['showtype'] == 'thumb'): ?>
    <?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['item']):
?>
        <div class="uc_latest_item">
            <table border="0" cellspacing="2" cellpadding="0" width="100%">
                <tr><td height="110" align="center" valign="middle">
                    <a href="/catalog/item<?php echo $this->_tpl_vars['item']['id']; ?>
.html">
                        <img alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" src="/images/catalog/small/<?php echo $this->_tpl_vars['item']['imageurl']; ?>
" border="0" />
                    </a>
                </td></tr>

                <tr><td align="center" valign="middle">
                    <a class="uc_latest_link" href="/catalog/item<?php echo $this->_tpl_vars['item']['id']; ?>
.html"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30)); ?>
</a>
                </td></tr>

                <?php if ($this->_tpl_vars['item']['viewtype'] == 'shop'): ?>
                    <tr><td align="center" valign="middle">
                        <div id="uc_popular_price"><?php echo $this->_tpl_vars['item']['price']; ?>
 <?php echo $this->_tpl_vars['LANG']['CURRENCY']; ?>
</div>
                    </td></tr>
                <?php endif; ?>
            </table>
        </div>
    <?php endforeach; endif; unset($_from); ?>
    <div class="blog_desc"></div>
<?php endif; ?>

<?php if ($this->_tpl_vars['cfg']['showtype'] == 'list'): ?>
    <table width="100%" cellspacing="0" cellpadding="4" class="uc_latest_list">
        <?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['item']):
?>
            <tr>
                <td width="" valign="top">
                    <a class="uc_latest_link" href="/catalog/item<?php echo $this->_tpl_vars['item']['id']; ?>
.html"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30)); ?>
</a>
                </td>

                <?php unset($this->_sections['customer']);
$this->_sections['customer']['name'] = 'customer';
$this->_sections['customer']['start'] = (int)0;
$this->_sections['customer']['loop'] = is_array($_loop=$this->_tpl_vars['cfg']['showf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['customer']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['customer']['show'] = true;
$this->_sections['customer']['max'] = $this->_sections['customer']['loop'];
if ($this->_sections['customer']['start'] < 0)
    $this->_sections['customer']['start'] = max($this->_sections['customer']['step'] > 0 ? 0 : -1, $this->_sections['customer']['loop'] + $this->_sections['customer']['start']);
else
    $this->_sections['customer']['start'] = min($this->_sections['customer']['start'], $this->_sections['customer']['step'] > 0 ? $this->_sections['customer']['loop'] : $this->_sections['customer']['loop']-1);
if ($this->_sections['customer']['show']) {
    $this->_sections['customer']['total'] = min(ceil(($this->_sections['customer']['step'] > 0 ? $this->_sections['customer']['loop'] - $this->_sections['customer']['start'] : $this->_sections['customer']['start']+1)/abs($this->_sections['customer']['step'])), $this->_sections['customer']['max']);
    if ($this->_sections['customer']['total'] == 0)
        $this->_sections['customer']['show'] = false;
} else
    $this->_sections['customer']['total'] = 0;
if ($this->_sections['customer']['show']):

            for ($this->_sections['customer']['index'] = $this->_sections['customer']['start'], $this->_sections['customer']['iteration'] = 1;
                 $this->_sections['customer']['iteration'] <= $this->_sections['customer']['total'];
                 $this->_sections['customer']['index'] += $this->_sections['customer']['step'], $this->_sections['customer']['iteration']++):
$this->_sections['customer']['rownum'] = $this->_sections['customer']['iteration'];
$this->_sections['customer']['index_prev'] = $this->_sections['customer']['index'] - $this->_sections['customer']['step'];
$this->_sections['customer']['index_next'] = $this->_sections['customer']['index'] + $this->_sections['customer']['step'];
$this->_sections['customer']['first']      = ($this->_sections['customer']['iteration'] == 1);
$this->_sections['customer']['last']       = ($this->_sections['customer']['iteration'] == $this->_sections['customer']['total']);
?>
                    <td valign="top"><?php echo $this->_tpl_vars['item']['fdata'][$this->_sections['customer']['index']]; ?>
</td>
                <?php endfor; endif; ?>

                <td width="100" align="right" valign="top"><?php echo $this->_tpl_vars['item']['key']; ?>
</td>

                <td align="right" width="65">
                    <?php if ($this->_tpl_vars['item']['viewtype'] == 'shop'): ?>
                        <div id="uc_popular_price"><?php echo $this->_tpl_vars['item']['price']; ?>
 <?php echo $this->_tpl_vars['LANG']['CURRENCY']; ?>
</div>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; endif; unset($_from); ?>
    </table>
<?php endif; ?>

<?php if ($this->_tpl_vars['cfg']['fulllink']): ?>
    <div style="margin-top:5px; text-align:right; clear:both"><a style="text-decoration:underline" href="/catalog"><?php echo $this->_tpl_vars['LANG']['UC_MODULE_CATALOG']; ?>
</a> <?php echo $this->_tpl_vars['LANG']['UC_MODULE_ARR']; ?>
</div>
<?php endif; ?>
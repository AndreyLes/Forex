<?php /* Smarty version 2.6.28, created on 2014-09-12 02:04:30
         compiled from com_forum_user_activity.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'profile_url', 'com_forum_user_activity.tpl', 29, false),array('modifier', 'escape', 'com_forum_user_activity.tpl', 29, false),array('modifier', 'spellcount', 'com_forum_user_activity.tpl', 68, false),)), $this); ?>
<div class="float_bar">
<?php if ($this->_tpl_vars['sub_do'] == 'threads'): ?>
    <a class="ajaxlink" href="javascript:" onclick="forum.getUserActivity('threads', '<?php echo $this->_tpl_vars['link']; ?>
', '1');"><strong><?php echo $this->_tpl_vars['LANG']['THREADS']; ?>
 (<?php echo $this->_tpl_vars['thread_count']; ?>
)</strong></a> | <a class="ajaxlink" href="javascript:" onclick="forum.getUserActivity('posts', '<?php echo $this->_tpl_vars['link']; ?>
', '1');"><?php echo $this->_tpl_vars['LANG']['MESSAGES1']; ?>
 (<?php echo $this->_tpl_vars['post_count']; ?>
)</a>
<?php else: ?>
    <?php if ($this->_tpl_vars['thread_count']): ?><a class="ajaxlink" href="javascript:" onclick="forum.getUserActivity('threads', '<?php echo $this->_tpl_vars['link']; ?>
', '1');"><?php echo $this->_tpl_vars['LANG']['THREADS']; ?>
 (<?php echo $this->_tpl_vars['thread_count']; ?>
)</a> | <?php endif; ?><a class="ajaxlink" href="javascript:" onclick="forum.getUserActivity('posts', '<?php echo $this->_tpl_vars['link']; ?>
', '1');"><strong><?php echo $this->_tpl_vars['LANG']['MESSAGES1']; ?>
 (<?php echo $this->_tpl_vars['post_count']; ?>
)</strong></a>
<?php endif; ?>
</div>

<h1 class="con_heading"><?php echo $this->_tpl_vars['pagetitle']; ?>
</h1>

<?php if ($this->_tpl_vars['sub_do'] == 'threads'): ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_view.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>

    <?php if ($this->_tpl_vars['post_count']): ?>

    <table class="posts_table" width="100%" cellspacing="2" cellpadding="5" border="0" bordercolor="#999999">
        <?php $this->assign('last_thread_id', ''); ?>
        <?php $_from = $this->_tpl_vars['posts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pid'] => $this->_tpl_vars['post']):
?>
            <?php if ($this->_tpl_vars['post']['thread_id'] != $this->_tpl_vars['last_thread_id']): ?>
            <tr>
              <td colspan="2" class="darkBlue-LightBlue"><?php echo $this->_tpl_vars['LANG']['THREAD']; ?>
: <a  href="/forum/thread<?php echo $this->_tpl_vars['post']['thread_id']; ?>
.html" ><?php echo $this->_tpl_vars['post']['thread_title']; ?>
</a></td>
            </tr>
            <?php endif; ?>
            <?php $this->assign('last_thread_id', $this->_tpl_vars['post']['thread_id']); ?>
            <tr class="posts_table_tr">
                <td class="post_usercell" width="140" align="center" valign="top" height="150">
                    <div>
                        <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['post']['login']), $this);?>
" title="<?php echo $this->_tpl_vars['LANG']['GOTO_PROFILE']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a>
                    </div>
                    <div class="post_userrank">
                        <?php if ($this->_tpl_vars['post']['userrank']['group']): ?>
                            <span class="<?php echo $this->_tpl_vars['post']['userrank']['class']; ?>
"><?php echo $this->_tpl_vars['post']['userrank']['group']; ?>
</span>
                        <?php endif; ?>
                        <?php if ($this->_tpl_vars['post']['userrank']['rank']): ?>
                            <span class="<?php echo $this->_tpl_vars['post']['userrank']['class']; ?>
"><?php echo $this->_tpl_vars['post']['userrank']['rank']; ?>
</span>
                        <?php endif; ?>
                    </div>
                    <div class="post_userimg">
                        <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['post']['login']), $this);?>
" title="<?php echo $this->_tpl_vars['LANG']['GOTO_PROFILE']; ?>
"><img border="0" class="usr_img_small" src="<?php echo $this->_tpl_vars['post']['avatar_url']; ?>
" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['post']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" /></a>
                        <?php if ($this->_tpl_vars['post']['user_awards']): ?>
                            <div class="post_userawards">
                                <?php $_from = $this->_tpl_vars['post']['user_awards']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['aid'] => $this->_tpl_vars['award']):
?>
                                    <img src="/images/icons/award.gif" border="0" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['award']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['award']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"/>
                                <?php endforeach; endif; unset($_from); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="post_usermsgcnt"><?php echo $this->_tpl_vars['LANG']['MESSAGES']; ?>
: <?php echo $this->_tpl_vars['post']['post_count']; ?>
</div>
                    <?php if ($this->_tpl_vars['post']['city']): ?>
                        <div class="post_usermsgcnt"><?php echo $this->_tpl_vars['post']['city']; ?>
</div>
                    <?php endif; ?>
                    <div><?php echo $this->_tpl_vars['post']['flogdate']; ?>
</div>
                </td>
                <td width="" class="post_msgcell" align="left" valign="top">

                    <div class="post_user_date">
                        <?php echo $this->_tpl_vars['post']['fpubdate']; ?>
, <?php echo $this->_tpl_vars['post']['wday']; ?>

                    </div>

                    <div class="post_content"><?php echo $this->_tpl_vars['post']['content_html']; ?>
</div>
                    <?php if ($this->_tpl_vars['post']['attached_files'] && $this->_tpl_vars['cfg']['fa_on']): ?>
                        <div id="attached_files_<?php echo $this->_tpl_vars['post']['id']; ?>
">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'com_forum_attached_files.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['post']['edittimes']): ?>
                        <div class="post_editdate"><?php echo $this->_tpl_vars['LANG']['EDITED']; ?>
: <?php echo ((is_array($_tmp=$this->_tpl_vars['post']['edittimes'])) ? $this->_run_mod_handler('spellcount', true, $_tmp, $this->_tpl_vars['LANG']['COUNT1'], $this->_tpl_vars['LANG']['COUNT2'], $this->_tpl_vars['LANG']['COUNT1']) : smarty_modifier_spellcount($_tmp, $this->_tpl_vars['LANG']['COUNT1'], $this->_tpl_vars['LANG']['COUNT2'], $this->_tpl_vars['LANG']['COUNT1'])); ?>
 (<?php echo $this->_tpl_vars['LANG']['LAST_EDIT']; ?>
: <?php echo $this->_tpl_vars['post']['peditdate']; ?>
)</div>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['post']['signature_html']): ?>
                        <div class="post_signature"><?php echo $this->_tpl_vars['post']['signature_html']; ?>
</div>
                    <?php endif; ?>
                </td>
            </tr>
            <?php $this->assign('num', ($this->_tpl_vars['num']+1)); ?>
        <?php endforeach; endif; unset($_from); ?>
    </table>
    <?php echo $this->_tpl_vars['pagination']; ?>


    <?php else: ?>
        <p><?php echo $this->_tpl_vars['LANG']['NOT_POST_BY_USER']; ?>
</p>
    <?php endif; ?>

<?php endif; ?>
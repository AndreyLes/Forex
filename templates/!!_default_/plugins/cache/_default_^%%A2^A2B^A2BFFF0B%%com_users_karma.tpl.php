<?php /* Smarty version 2.6.28, created on 2014-06-04 18:41:19
         compiled from com_users_karma.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'profile_url', 'com_users_karma.tpl', 7, false),)), $this); ?>
<div class="con_heading"><?php echo $this->_tpl_vars['LANG']['KARMA_HISTORY']; ?>
 - <?php echo $this->_tpl_vars['usr']['nickname']; ?>
</div>
<?php if ($this->_tpl_vars['karma']): ?>
<table width="">
		<?php $_from = $this->_tpl_vars['karma']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['karm']):
?>
			<tr>
				<td style="border-bottom:solid 1px silver" width="150" valign="middle"><?php echo $this->_tpl_vars['karm']['fsenddate']; ?>
</td>
				<td style="border-bottom:solid 1px silver" width="200" valign="middle"><a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['karm']['login']), $this);?>
"><?php echo $this->_tpl_vars['karm']['nickname']; ?>
</a></td>
				<td style="border-bottom:solid 1px silver" width="100" valign="middle" align="center">
                	<?php if ($this->_tpl_vars['karm']['kpoints'] > 0): ?>
                		<span style="font-size:24px;color:green">+<?php echo $this->_tpl_vars['karm']['kpoints']; ?>
</span>
                    <?php else: ?>
                    	<span style="font-size:24px;color:red"><?php echo $this->_tpl_vars['karm']['kpoints']; ?>
</span>
                    <?php endif; ?>
                </td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
</table>
<?php else: ?>
<p><?php echo $this->_tpl_vars['LANG']['KARMA_NOT_MODIFY']; ?>
</p>
<p><?php echo $this->_tpl_vars['LANG']['KARMA_NOT_MODIFY_TEXT']; ?>
</p>
<p><?php echo $this->_tpl_vars['LANG']['KARMA_DESCRIPTION']; ?>
</p>
<?php endif; ?>
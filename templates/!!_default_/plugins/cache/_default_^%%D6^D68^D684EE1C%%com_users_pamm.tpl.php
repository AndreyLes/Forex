<?php /* Smarty version 2.6.28, created on 2014-06-25 16:57:01
         compiled from com_users_pamm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_pamm.tpl', 5, false),array('function', 'add_css', 'com_users_pamm.tpl', 7, false),)), $this); ?>

<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/tabs/jquery.ui.min.js'), $this);?>

<?php echo cmsSmartyAddJS(array('file' => "components/users/js/profile.js"), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/jquery/tabs/tabs.css'), $this);?>
					
<div class='kab-block' style='width:100%'>
	<header>
	<img style='margin:4px' src='/images/panteon.png'/><span style='color:#fff'>ПАММ - фонд «Консерватор»</span>
	</header>
	<div class='pamms_div'>
			<table class='pamm_table' width='440' style='' cellspacing="0" cellpadding="0">
						<tr class='pamms_title' style=''>
							<td style='width:155px'>Состав ПАММ-проекта</td>
							<td style='width:120px'>Доля</td>
							<td >Детали ПАММ-счета</td>
						</tr>
						<tr>
							<td><span>5000146 (Hermes)</span></td>
							<td>14%</td>
							<td><a href='' class='open'>Открыть</a></td>
						</tr>
						<tr>
							<td><span>5000146 (Maksim)</span></td>
							<td>14%</td>
							<td><a href='' class='open'>Открыть</a></td>
						</tr>
						<tr>
							<td><span>5000146 (Fenix)</span></td>
							<td>14%</td>
							<td><a href='' class='open'>Открыть</a></td>
						</tr>
						<tr>
							<td><span>5000146 (votfx)</span></td>
							<td>14%</td>
							<td><a href='' class='open'>Открыть</a></td>
						</tr>
									<tr>
							<td><span>5000146 (votfx)</span></td>
							<td>14%</td>
							<td><a href='' class='open'>Открыть</a></td>
						</tr>
			</table>
			<table class='pamm_table pamm_table1' width='492' style='' cellspacing="0" cellpadding="0">
				<tr class='pamms_title' style=''>
			<td colspan='2'>
				Динамика доходности фонда Консервативный 07.05.14
			</td>
			</tr>
				<tr>
					<td>
							Изменения за день,%
					</td>
					<td>
						-0,179%
					</td>
					</tr>
									<tr>
					<td>
							Изменения за неделю,%
					</td>
					<td>
						-0,179%
					</td>
					</tr>
														<tr>
					<td>
							Торговый период
					</td>
					<td>
						12 недель
					</td>
					</tr>
																			<tr>
					<td>
							Капитализация
					</td>
					<td>
						$ 2 200 200,09
					</td>
					</tr>
			</table>
	</div>
	
	
	
													<!-- ФИГНЯ С КРУЖЕЧКОМ -->
	
	
	
	<div id='krug'>  </div>
	<table style='text-align:center;float:left;margin-right:2px;'>
		<tr>
			<td class='weekend'>Недельная</td>
		</tr>
		
		<tr>
			<td style='border:1px solid #000;color:#000'>Период/доходность</td>
		</tr>
		
		<tr>
			<td class='left_td'></p>1-9.02.2014</p>
			<p>1.2%</p>
			</td>
		</tr>
		
		<tr>
			<td class='left_td'><p>10-16.02.2014</p>
			<p>1.2%</p>
			</td>
		</tr>
		
	<tr>
			<td class='left_td'></p>17-26.02.2014</p>
			<p>1.2%</p>
			</td>
	</tr>
	<tr>
			<td class='left_td'></p>27-31.02.2014</p>
			<p>1.2%</p>
			</td>
	</tr>
		
	</table>
	<table style='text-align:center;float:left;margin-right:2px;'>
		<tr>
			<td class='weekend'>Недельная</td>
		</tr>
		
		<tr>
			<td style='border:1px solid #000;color:#000'>Период/доходность</td>
		</tr>
		
		<tr>
			<td rowspan='4' class='center_td'><p>1-28.02.2014</p>
			<p>1.2%</p>
			</td>
		</tr>
	</table>
	<table style='text-align:center;'>
		<tr>
			<td class='weekend'>Недельная</td>
		</tr>
		
		<tr>
			<td style='border:1px solid #000;color:#000'>Период/доходность</td>
		</tr>
		
		<tr>
			<td rowspan='4' class='center_td'>
					
				<div class='krug'>
					<span>54%</span>
				</div>
			</td>
		</tr>
	</table>
	<div id='gen_pamm'>
	
	</div>
	<table id='gen_tab'>	
		<tr>
			<td style='width:168px;'>Номер операции</td>
			<td style='width:169px;'>Дата инвестирования</td>
			<td style='width:169px;'>Сумма</td>
			<td style='width:277px;'>Тип операции</td>
			<td style='width:157px;border-right:1px solid #BCBCBC'>
				<table style='width:157px;margin-bottom: -2px;'>
					<tr>
						<td style='border-right:0' colspan='2'>Доходность</td>
					</tr>
					<tr>
						<td >в USD</td>
						<td>в %</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
					<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
					<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
				<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
					<tr class='gen_tab_tr'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
	
	</table>
	
	
</div>
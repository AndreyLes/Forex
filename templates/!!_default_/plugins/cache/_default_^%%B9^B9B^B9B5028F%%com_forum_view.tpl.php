<?php /* Smarty version 2.6.28, created on 2014-09-12 00:16:44
         compiled from com_forum_view.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'template', 'com_forum_view.tpl', 14, false),array('function', 'profile_url', 'com_forum_view.tpl', 46, false),)), $this); ?>
<table class="threads_table" width="100%" cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td colspan="2" class="darkBlue-LightBlue"><?php echo $this->_tpl_vars['LANG']['THREADS']; ?>
</td>
        <td class="darkBlue-LightBlue"><?php echo $this->_tpl_vars['LANG']['AUTHOR']; ?>
</td>
        <td class="darkBlue-LightBlue"><?php echo $this->_tpl_vars['LANG']['FORUM_ACT']; ?>
</td>
        <td class="darkBlue-LightBlue"><?php echo $this->_tpl_vars['LANG']['LAST_POST']; ?>
</td>
    </tr>
<?php if ($this->_tpl_vars['threads']): ?>
    <?php $this->assign('row', 1); ?>
    <?php $_from = $this->_tpl_vars['threads']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['thread']):
?>
        <?php if ($this->_tpl_vars['row'] % 2): ?><?php $this->assign('class', 'row1'); ?><?php else: ?><?php $this->assign('class', 'row2'); ?><?php endif; ?>
        <tr>
            <?php if ($this->_tpl_vars['thread']['pinned']): ?>
                <td width="30" class="<?php echo $this->_tpl_vars['class']; ?>
" align="center" valign="middle"><img alt="<?php echo $this->_tpl_vars['LANG']['ATTACHED_THREAD']; ?>
" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/pinned.png" border="0" title="<?php echo $this->_tpl_vars['LANG']['ATTACHED_THREAD']; ?>
" /></td>
            <?php else: ?>
                <?php if ($this->_tpl_vars['thread']['closed']): ?>
                    <td width="30" class="<?php echo $this->_tpl_vars['class']; ?>
" align="center" valign="middle"><img alt="<?php echo $this->_tpl_vars['LANG']['THREAD_CLOSE']; ?>
" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/closed.png" border="0" title="<?php echo $this->_tpl_vars['LANG']['THREAD_CLOSE']; ?>
" /></td>
                <?php else: ?>
                    <?php if ($this->_tpl_vars['thread']['is_new']): ?>
                        <td width="30" class="<?php echo $this->_tpl_vars['class']; ?>
" align="center" valign="middle"><img alt="<?php echo $this->_tpl_vars['LANG']['HAVE_NEW_MESS']; ?>
" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/new.png" border="0" title="<?php echo $this->_tpl_vars['LANG']['HAVE_NEW_MESS']; ?>
" /></td>
                    <?php else: ?>
                        <td width="30" class="<?php echo $this->_tpl_vars['class']; ?>
" align="center" valign="middle"><img alt="<?php echo $this->_tpl_vars['LANG']['NOT_NEW_MESS']; ?>
" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/old.png" border="0" title="<?php echo $this->_tpl_vars['LANG']['NOT_NEW_MESS']; ?>
" /></td>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <td width="" class="<?php echo $this->_tpl_vars['class']; ?>
" align="left">
                <div class="thread_link"><a href="/forum/thread<?php echo $this->_tpl_vars['thread']['id']; ?>
.html"><?php echo $this->_tpl_vars['thread']['title']; ?>
</a>
                    <?php if ($this->_tpl_vars['thread']['pages'] > 1): ?>
                        <span class="thread_pagination" title="<?php echo $this->_tpl_vars['LANG']['PAGES']; ?>
"> (
                            <?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)1;
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['thread']['pages']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>
                                <?php if ($this->_sections['foo']['index'] > 5 && $this->_tpl_vars['thread']['pages'] > 6): ?>
                                    ...<a href="/forum/thread<?php echo $this->_tpl_vars['thread']['id']; ?>
-<?php echo $this->_tpl_vars['thread']['pages']; ?>
.html" title="<?php echo $this->_tpl_vars['LANG']['LAST']; ?>
"><?php echo $this->_tpl_vars['thread']['pages']; ?>
</a>
                                    <?php break; ?>
                                <?php else: ?>
                                    <a href="/forum/thread<?php echo $this->_tpl_vars['thread']['id']; ?>
-<?php echo $this->_sections['foo']['index']; ?>
.html" title="<?php echo $this->_tpl_vars['LANG']['PAGE']; ?>
 <?php echo $this->_sections['foo']['index']; ?>
"><?php echo $this->_sections['foo']['index']; ?>
</a>
                                    <?php if ($this->_sections['foo']['index'] < $this->_tpl_vars['thread']['pages']): ?>, <?php endif; ?>
                                <?php endif; ?>
                            <?php endfor; endif; ?>
                        ) </span>
                    <?php endif; ?>
                </div>
                <?php if ($this->_tpl_vars['thread']['description']): ?>
                    <div class="thread_desc"><?php echo $this->_tpl_vars['thread']['description']; ?>
</div>
                <?php endif; ?>
            </td>
            <td width="120" style="font-size:12px" class="<?php echo $this->_tpl_vars['class']; ?>
"><a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['thread']['login']), $this);?>
"><?php echo $this->_tpl_vars['thread']['nickname']; ?>
</a></td>
            <td width="120" style="font-size:12px; color:#375E93" class="<?php echo $this->_tpl_vars['class']; ?>
">
                <strong><?php echo $this->_tpl_vars['LANG']['HITS']; ?>
:</strong> <?php echo $this->_tpl_vars['thread']['hits']; ?>
<br/>
                <strong><?php echo $this->_tpl_vars['LANG']['REPLIES']; ?>
:</strong> <?php echo $this->_tpl_vars['thread']['answers']; ?>

            </td>
            <td width="200" style="font-size:12px" class="<?php echo $this->_tpl_vars['class']; ?>
">
                <?php if ($this->_tpl_vars['thread']['last_msg_array']): ?>
                    <a href="/forum/thread<?php echo $this->_tpl_vars['thread']['last_msg_array']['thread_id']; ?>
-<?php echo $this->_tpl_vars['thread']['last_msg_array']['lastpage']; ?>
.html#<?php echo $this->_tpl_vars['thread']['last_msg_array']['id']; ?>
"><img class="last_post_img" title="<?php echo $this->_tpl_vars['LANG']['GO_LAST_POST']; ?>
" alt="<?php echo $this->_tpl_vars['LANG']['GO_LAST_POST']; ?>
" src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/anchor.png"></a>
                    <?php echo $this->_tpl_vars['LANG']['FROM']; ?>
 <?php echo $this->_tpl_vars['thread']['last_msg_array']['user_link']; ?>
<br/>
                    <?php echo $this->_tpl_vars['thread']['last_msg_array']['fpubdate']; ?>

                <?php else: ?>
                    <?php echo $this->_tpl_vars['LANG']['NOT_POSTS']; ?>

                <?php endif; ?>
            </td>
        </tr>
        <?php $this->assign('row', ($this->_tpl_vars['row']+1)); ?>
    <?php endforeach; endif; unset($_from); ?>

<?php else: ?>
    <td colspan="7" align="center" valign="middle" class="row1">
        <p style="margin: 5px"><?php echo $this->_tpl_vars['LANG']['NOT_THREADS_IN_FORUM']; ?>
.</p>
    </td>

<?php endif; ?>
</table>
<?php echo $this->_tpl_vars['pagination']; ?>


<?php if ($this->_tpl_vars['show_panel']): ?>
<table class="threads_table" width="100%" cellspacing="0" cellpadding="5" border="0" style="margin: 10px 0 0 0; font-size: 12px">
    <tr>
        <td class="row1"><?php echo $this->_tpl_vars['LANG']['OPTIONS_VIEW']; ?>
</td>
        <?php if ($this->_tpl_vars['moderators']): ?>
            <td class="row1"><?php echo $this->_tpl_vars['LANG']['THIS_FORUM_MODERS']; ?>
</td>
        <?php endif; ?>
    </tr>
    <tr>
        <td>
            <form action="" method="post">
                <table cellspacing="1" cellpadding="5" border="0" style="color: #555">
                <tbody>
                    <tr valign="bottom">
                      <td>
                          <div><?php echo $this->_tpl_vars['LANG']['THREAD_ORDER']; ?>
</div>
                            <select name="order_by">
                              <option value="title" <?php if ($this->_tpl_vars['order_by'] == 'title'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['TITLE']; ?>
</option>
                              <option value="pubdate" <?php if ($this->_tpl_vars['order_by'] == 'pubdate'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['ORDER_DATE']; ?>
</option>
                              <option value="post_count" <?php if ($this->_tpl_vars['order_by'] == 'post_count'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['ANSWER_COUNT']; ?>
</option>
                              <option value="hits" <?php if ($this->_tpl_vars['order_by'] == 'hits'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['HITS_COUNT']; ?>
</option>
                            </select>
                      </td>
                      <td>
                        <div><?php echo $this->_tpl_vars['LANG']['ORDER_TO']; ?>
</div>
                        <select name="order_to">
                          <option value="asc" <?php if ($this->_tpl_vars['order_to'] == 'asc'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['ORDER_ASC']; ?>
</option>
                          <option value="desc" <?php if ($this->_tpl_vars['order_to'] == 'desc'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['ORDER_DESC']; ?>
</option>
                        </select>
                      </td>
                      <td>
                        <div><?php echo $this->_tpl_vars['LANG']['SHOW']; ?>
</div>
                        <select name="daysprune">
                          <option value="1" <?php if ($this->_tpl_vars['daysprune'] == 1): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['SHOW_DAY']; ?>
</option>
                          <option value="7" <?php if ($this->_tpl_vars['daysprune'] == 7): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['SHOW_W']; ?>
</option>
                          <option value="30" <?php if ($this->_tpl_vars['daysprune'] == 30): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['SHOW_MONTH']; ?>
</option>
                          <option value="365" <?php if ($this->_tpl_vars['daysprune'] == 365): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['SHOW_YEAR']; ?>
</option>
                          <option value="all" <?php if (! $this->_tpl_vars['daysprune']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['SHOW_ALL']; ?>
</option>
                        </select>
                      </td>
                      <td>
                        <div></div>
                        <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['SHOW_THREADS']; ?>
">
                      </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </td>
        <?php if ($this->_tpl_vars['moderators']): ?>
            <td style="vertical-align: top">
            <?php $_from = $this->_tpl_vars['moderators']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['moderator']):
?>
                <?php if ($this->_tpl_vars['q']): ?>, <?php endif; ?><a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['moderator']['login']), $this);?>
"><?php echo $this->_tpl_vars['moderator']['nickname']; ?>
</a>
                <?php $this->assign('q', '1'); ?>
            <?php endforeach; endif; unset($_from); ?>
        </td>
        <?php endif; ?>
    </tr>
</table>
<?php endif; ?>
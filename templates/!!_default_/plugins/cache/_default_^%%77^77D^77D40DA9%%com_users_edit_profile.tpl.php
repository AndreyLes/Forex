<?php /* Smarty version 2.6.28, created on 2014-07-02 11:11:02
         compiled from com_users_edit_profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_users_edit_profile.tpl', 1, false),array('function', 'add_css', 'com_users_edit_profile.tpl', 2, false),array('function', 'template', 'com_users_edit_profile.tpl', 31, false),array('function', 'profile_url', 'com_users_edit_profile.tpl', 60, false),array('function', 'city_input', 'com_users_edit_profile.tpl', 183, false),array('function', 'dateform', 'com_users_edit_profile.tpl', 189, false),array('modifier', 'escape', 'com_users_edit_profile.tpl', 23, false),array('modifier', 'NoSpam', 'com_users_edit_profile.tpl', 83, false),)), $this); ?>
<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/tabs/jquery.ui.min.js'), $this);?>

<?php echo cmsSmartyAddCSS(array('file' => 'includes/jquery/tabs/tabs.css'), $this);?>


<?php echo '
	<script type="text/javascript">
		$(function(){$(".uitabs").tabs();});
	</script>
'; ?>


	<div id='usr' style='padding-bottom: 6px;'>
 <div class="head_edit" id="nickname">
        <?php echo $this->_tpl_vars['usr']['nickname']; ?>
 <?php if ($this->_tpl_vars['usr']['banned']): ?><span style="color:red; font-size:12px;"><?php echo $this->_tpl_vars['LANG']['USER_IN_BANLIST']; ?>
</span><?php endif; ?>
    </div>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
                            <img alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" class="usr_img" src="<?php echo $this->_tpl_vars['usr']['avatar']; ?>
" />
                        </div>

							<div id="usermenu" style="">
                            <div class="usr_profile_menu">
							<table cellpadding="0" cellspacing="6" ><tr>
	
                                 <tr>
                                    <td><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/profile/avatar.png"  border="0"/></td>
                                    <td><a href="/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/avatar.html" title="<?php echo $this->_tpl_vars['LANG']['SET_AVATAR']; ?>
"><?php echo $this->_tpl_vars['LANG']['SET_AVATAR']; ?>
</a></td>
                                </tr>
						   </table></div>
                      	</td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">

						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['LAST_VISIT']; ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['usr']['flogdate']; ?>
</div>
						</div>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['DATE_REGISTRATION']; ?>
:</div>
							<div class="value">
                                <?php echo $this->_tpl_vars['usr']['fregdate']; ?>

                            </div>
						</div>
                        <?php if ($this->_tpl_vars['usr']['inv_login']): ?>
                            <div class="field">
                                <div class="title"><?php echo $this->_tpl_vars['LANG']['INVITED_BY']; ?>
:</div>
                                <div class="value">
                                    <a href="<?php echo cmsSmartyProfileURL(array('login' => $this->_tpl_vars['usr']['inv_login']), $this);?>
"><?php echo $this->_tpl_vars['usr']['inv_nickname']; ?>
</a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->_tpl_vars['usr']['city']): ?>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['CITY']; ?>
:</div>
                            <div class="value"><a href="/users/city/<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['cityurl'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"><?php echo $this->_tpl_vars['usr']['city']; ?>
</a></div>
						</div>
                        <?php endif; ?>

						<?php if ($this->_tpl_vars['usr']['showbirth'] && $this->_tpl_vars['usr']['fbirthdate']): ?>
						<div class="field">
							<div class="title"><?php echo $this->_tpl_vars['LANG']['BIRTH']; ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['usr']['fbirthdate']; ?>
</div>
						</div>
						<?php endif; ?>

					
						<?php if ($this->_tpl_vars['usr']['showmail']): ?>
							<?php echo cmsSmartyAddJS(array('file' => 'includes/jquery/jquery.nospam.js'), $this);?>

							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['email'])) ? $this->_run_mod_handler('NoSpam', true, $_tmp) : smarty_modifier_NoSpam($_tmp)); ?>
" class="email"><?php echo $this->_tpl_vars['usr']['email']; ?>
</a></div>
							</div>
							<?php echo '
								<script>
										$(\'.email\').nospam({ replaceText: true });
								</script>
							'; ?>

						<?php endif; ?>

				

						<?php if ($this->_tpl_vars['cfg']['privforms'] && $this->_tpl_vars['usr']['form_fields']): ?>
							<?php $_from = $this->_tpl_vars['usr']['form_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['field']):
?>
                                <div class="field">
                                    <div class="title"><?php echo $this->_tpl_vars['field']['title']; ?>
:</div>
                                    <div class="value"><?php if ($this->_tpl_vars['field']['field']): ?><?php echo $this->_tpl_vars['field']['field']; ?>
<?php else: ?><em><?php echo $this->_tpl_vars['LANG']['NOT_SET']; ?>
</em><?php endif; ?></div>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
						<?php endif; ?>

					</div>

				</div>

                <?php $_from = $this->_tpl_vars['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['plugin']):
?>
                    <div id="upr_<?php echo $this->_tpl_vars['plugin']['name']; ?>
"><?php echo $this->_tpl_vars['plugin']['html']; ?>
</div>
                <?php endforeach; endif; unset($_from); ?>

			</div>
	</td>
  </tr>
</table>

</div>


















<div id='usr'>
<div class="head_edit"><?php echo $this->_tpl_vars['LANG']['CONFIG_PROFILE']; ?>
</div>
<div id="profiletabs" class="uitabs">
    <ul id="tabs">
        <li><a href="#about"><span><?php echo $this->_tpl_vars['LANG']['ABOUT_ME']; ?>
</span></a></li>
        <li><a href="#notices"><span><?php echo $this->_tpl_vars['LANG']['NOTIFIC']; ?>
</span></a></li>
       
        <li rel="hid"><a href="#change_password"><span><?php echo $this->_tpl_vars['LANG']['CHANGING_PASS']; ?>
</span></a></li>
		<li><a href="#verification"><span><?php echo $this->_tpl_vars['LANG']['VERIFICATION']; ?>
</span></a></li>
		<li><a href="#change_color"><span><?php echo $this->_tpl_vars['LANG']['CHANGE_COLOR']; ?>
</span></a></li>
    </ul>

    <form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="opt" value="save" />
        <div id="about">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="300" valign="top">
                        <strong><?php echo $this->_tpl_vars['LANG']['YOUR_NAME']; ?>
: </strong><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['YOUR_NAME_TEXT']; ?>
</span>
                    </td>
                    <td valign="top"><input name="nickname" type="text" class="text-input" id="nickname" style="width:300px" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['usr']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"/></td>
                </tr>
				<tr>
                    <td width="300" valign="top">
                        <strong>E-mail:</strong><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['REALY_ADRESS_EMAIL']; ?>
</span>
                    </td>
                    <td valign="top">
                        <input name="email" type="text" class="text-input" id="email" style="width:300px" value="<?php echo $this->_tpl_vars['usr']['email']; ?>
"/>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['SEX']; ?>
:</strong></td>
                    <td valign="top">
                        <select name="gender" id="gender" style="width:307px">
                            <option value="0" <?php if ($this->_tpl_vars['usr']['gender'] == 0): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['LANG']['NOT_SPECIFIED']; ?>
</option>
                            <option value="m" <?php if ($this->_tpl_vars['usr']['gender'] == 'm'): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['LANG']['MALES']; ?>
</option>
                            <option value="f" <?php if ($this->_tpl_vars['usr']['gender'] == 'f'): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['LANG']['FEMALES']; ?>
</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong><?php echo $this->_tpl_vars['LANG']['CITY']; ?>
:</strong><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['CITY_TEXT']; ?>
</span>
                    </td>
                    <td valign="top">
                        <?php echo smarty_function_city_input(array('value' => $this->_tpl_vars['usr']['city'],'name' => 'city','width' => '300px'), $this);?>

                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['BIRTH']; ?>
:</strong> </td>
                    <td valign="top">
                        <?php echo smarty_function_dateform(array('seldate' => $this->_tpl_vars['usr']['birthdate']), $this);?>

                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong><?php echo $this->_tpl_vars['LANG']['HOBBY']; ?>
 (<?php echo $this->_tpl_vars['LANG']['TAGSS']; ?>
): </strong><br/>
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['YOUR_KEYWORDS']; ?>
</span><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['TAGSS_TEXT']; ?>
</span>
                    </td>
                    <td valign="top">
                        <textarea name="description" class="text-input" style="width:300px" rows="2" id="description"><?php echo $this->_tpl_vars['usr']['description']; ?>
</textarea>
                    </td>
                </tr>
                <?php if ($this->_tpl_vars['cfg_forum']['component_enabled']): ?>
                <tr>
                    <td valign="top">
                        <strong><?php echo $this->_tpl_vars['LANG']['SIGNED_FORUM']; ?>
:</strong><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['CAN_USE_BBCODE']; ?>
 </span>
                    </td>
                    <td valign="top">
                        <textarea name="signature" class="text-input" style="width:300px" rows="2" id="signature"><?php echo $this->_tpl_vars['usr']['signature']; ?>
</textarea>
                    </td>
                </tr>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['private_forms']): ?>
                    <?php $_from = $this->_tpl_vars['private_forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['field']):
?>
                    <tr>
                        <td valign="top">
                            <strong><?php echo $this->_tpl_vars['field']['title']; ?>
:</strong>
                            <?php if ($this->_tpl_vars['field']['description']): ?>
                                <br /><span class="usr_edithint"><?php echo $this->_tpl_vars['field']['description']; ?>
</span>
                            <?php endif; ?>
                        </td>
                        <td valign="top">
                            <?php echo $this->_tpl_vars['field']['field']; ?>

                        </td>
                    </tr>
                    <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
            </table>
        </div>

 
        <div id="notices">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="458" valign="top">
                        <strong class='form_size'>
                            <?php echo $this->_tpl_vars['LANG']['NOTIFY_NEW_MESS']; ?>
:
                        </strong><br/>
                        <span class="usr_edithint">
                            <?php echo $this->_tpl_vars['LANG']['NOTIFY_NEW_MESS_TEXT']; ?>

                        </span>
                    </td>
                    <td valign="top">
                        <label style='margin-right:43px;'><input name="email_newmsg" type="radio" value="1" <?php if ($this->_tpl_vars['usr']['email_newmsg']): ?>checked<?php endif; ?>/> <?php echo $this->_tpl_vars['LANG']['YES']; ?>
 </label>
                        <label><input name="email_newmsg" type="radio" value="0" <?php if (! $this->_tpl_vars['usr']['email_newmsg']): ?>checked<?php endif; ?>/> <?php echo $this->_tpl_vars['LANG']['NO']; ?>
</label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong class='form_size'><?php echo $this->_tpl_vars['LANG']['HOW_NOTIFY_NEW_MESS']; ?>
 </strong><br />
                        <span class="usr_edithint"><?php echo $this->_tpl_vars['LANG']['WHERE_TO_SEND']; ?>
</span>
                    </td>
                    <td valign="top">
					
		
					
									<span id="nav">
									<?php if ($this->_tpl_vars['usr']['cm_subscribe'] == 'mail'): ?>
										<?php echo $this->_tpl_vars['LANG']['TO_EMAIL']; ?>

									<?php elseif ($this->_tpl_vars['usr']['cm_subscribe'] == 'priv'): ?>
										<?php echo $this->_tpl_vars['LANG']['TO_PRIVATE_MESS']; ?>

									<?php elseif ($this->_tpl_vars['usr']['cm_subscribe'] == 'both'): ?>
										<?php echo $this->_tpl_vars['LANG']['TO_EMAIL_PRIVATE_MESS']; ?>

									<?php elseif ($this->_tpl_vars['usr']['cm_subscribe'] == 'none'): ?>
										<?php echo $this->_tpl_vars['LANG']['NOT_SEND']; ?>

									<?php endif; ?>
									</span>
						<ol class="selectsubs">
						
							<li data-value="mail"><?php echo $this->_tpl_vars['LANG']['TO_EMAIL']; ?>
</li>
							<li data-value="priv"><?php echo $this->_tpl_vars['LANG']['TO_PRIVATE_MESS']; ?>
</li>
							<li data-value="both"><?php echo $this->_tpl_vars['LANG']['TO_EMAIL_PRIVATE_MESS']; ?>
</li>
							<li data-value="none"><?php echo $this->_tpl_vars['LANG']['NOT_SEND']; ?>
</li>
							
						</ol>
					
					<input type='hidden' id="cm_subscribe" name="cm_subscribe" />
					
					
					
					
					
                    </td>
                </tr>
            </table>
        </div>
		
		<!-- Вивід верифікації -->
	     <div id="verification">
		 <?php $_from = $this->_tpl_vars['results_file']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tid'] => $this->_tpl_vars['result']):
?>
		 <?php if ($this->_tpl_vars['tid'] == 0): ?>
		 <h4>
			Первый уровень верификации
		 </h4>
		 <div id='underline' style='width: 900px;'>
			<span></span>
		 </div>
		 <div style='margin-top:26px'>
		 <div style='float:left'> 
		 		<span class='ver_span'>Документ,	подтверждающий личность</span>
		 </div>
			 <div class='right_block'>
					<?php if ($this->_tpl_vars['result']['filename']): ?>
						<a href='/images/users/docs/large/<?php echo $this->_tpl_vars['result']['filename']; ?>
' class='photobox cboxElement'><img src='/images/users/docs/small/<?php echo $this->_tpl_vars['result']['filename']; ?>
'/></a>
									<p><?php echo $this->_tpl_vars['LANG']['FIRST_LEVEL']; ?>
</p>
					<?php else: ?>
						<input name="passport<?php echo $this->_tpl_vars['tid']; ?>
" type="file" id="passport" size="30" />
						<p><?php echo $this->_tpl_vars['LANG']['FIRST_LEVEL']; ?>
</p>
					<?php endif; ?>
			</div>
		 </div>
		 
		 
		 
		 
		
			<?php elseif ($this->_tpl_vars['tid'] == 1): ?>
		 <h4>
			Второй уровень верификации
		 </h4>
		 <div id='underline' style='width: 900px;'>
			<span></span>
		 </div>
			<p id='second_lvl'><?php echo $this->_tpl_vars['LANG']['CANT_VER']; ?>
</p>
			<div style='float:left'>
				<span class='ver_span'>Фото пользователя с документом в руке</span>
			</div>
			<div class='right_block'>
				<?php if ($this->_tpl_vars['result']['filename']): ?>
				<a href='/images/users/docs/large/<?php echo $this->_tpl_vars['result']['filename']; ?>
' class='photobox cboxElement'>	<img src='/images/users/docs/small/<?php echo $this->_tpl_vars['result']['filename']; ?>
'/></a>
							
							<p><?php echo $this->_tpl_vars['LANG']['SECOND_LEVEL']; ?>
</p>
					<?php else: ?>
				<input name="passport<?php echo $this->_tpl_vars['tid']; ?>
" type="file" id="passport" size="30" />
						<p><?php echo $this->_tpl_vars['LANG']['SECOND_LEVEL']; ?>
</p>
				<?php endif; ?>
		
			 </div>	
			<?php elseif ($this->_tpl_vars['tid'] == 2): ?>
						 <h4>
							Подтвердить наличие банковской карты
						 </h4>
						 <div id='underline' style='width: 900px;margin-bottom:29px'>
							<span></span>
						 </div>
						<div style='float:left'>
							<span class='ver_span'>Банковская карта, подтверждающая наличие счета</span>
						</div>
						<div  class='right_block'>
								<?php if ($this->_tpl_vars['result']['filename']): ?>
									<a href='/images/users/docs/large/<?php echo $this->_tpl_vars['result']['filename']; ?>
' class='photobox cboxElement'><img src='/images/users/docs/small/<?php echo $this->_tpl_vars['result']['filename']; ?>
'/></a>
										<p><?php echo $this->_tpl_vars['LANG']['THIRD_LEVEL']; ?>
</p>
									<?php else: ?>
								<input name="passport<?php echo $this->_tpl_vars['tid']; ?>
" type="file" id="passport" size="30" /><p><?php echo $this->_tpl_vars['LANG']['THIRD_LEVEL']; ?>
</p>
								<?php endif; ?>
						</div>
						 <div  id='last_block'>
								<div style='float:left'>
									<span class='ver_span' style='margin-top:10px'>Также Вы можете</span>
								</div>
							<div  class='right_block'>
								<button id='kartka'><span>Заказать банковскою карту</span> </button>
							</div>
						</div>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
				
			
        </div>
					<div id='change_color'>

<!-- BEGIN STYLESHEET SWITCHER -->

				
							<ul id="stylesheets">
								<li>
									
									<a href="javascript:switchStylestyle('dark');">
										<img src='/images/red.png'/>
									</a>
								</li>
								<li>
									<a href="javascript:switchStylestyle('blue');">
										<img src='/images/blue.png'/>
									</a>
								</li>
								<li>
									<a href="javascript:switchStylestyle('green');">
										<img src='/images/green.png'/>
									</a>
								</li>
							</ul>
				
					</div>
		<div style="margin-top: 12px;text-align:center" id="submitform">
				<button name="save"  type="submit"  id="save2"><span><?php echo $this->_tpl_vars['LANG']['SAVE']; ?>
</span></button>
				<button name="save" name="delbtn2"  type="button"  id="delbtn2" onclick="location.href='/users/<?php echo $this->_tpl_vars['usr']['id']; ?>
/delprofile.html';">
						<span><?php echo $this->_tpl_vars['LANG']['DEL_PROFILE']; ?>
</span>
				</button>			 
        </div>
    </form>
    <div id="change_password">
        <form id="editform" name="editform" method="post" action="">
            <input type="hidden" name="opt" value="changepass" />
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="150" valign="top">
                        <strong><?php echo $this->_tpl_vars['LANG']['OLD_PASS']; ?>
:</strong>
                    </td>
                    <td valign="top">
                        <input name="oldpass" type="password" id="oldpass" class="text-input" size="30" />
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['NEW_PASS']; ?>
:</strong></td>
                    <td valign="top"><input name="newpass" type="password"  id="newpass" class="passes text-input" size="30" /></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<ul id='galki'>
							<li><span>Минимум 2 латинские буквы</span></li>
							<li><span>Минимум 2 цифры</span></li>
							<li><span>Минимум 10 символов</span></li>
							<li><span>Максимум 16 символов</span></li>
						
						
						
						</ul>
					
					
					
					
					</td>
				</tr>
                <tr>
                    <td valign="top"><strong><?php echo $this->_tpl_vars['LANG']['NEW_PASS_REPEAT']; ?>
:</strong></td>
                    <td valign="top">
					<input name="newpass2" type="password" class="passes text-input" id="newpass2" size="30" /><span id='galkun'></span></td>
                </tr>
            </table>
            <div style="margin-top: 72px;text-align:center">
			<button name="save2" disabled='true' type="submit"  id="save2"><span><?php echo $this->_tpl_vars['LANG']['CHANGE_PASSWORD']; ?>
</span></button>
			<button id="clear" type='button'><span><?php echo $this->_tpl_vars['LANG']['CLEAR_PASSWORD']; ?>
</span></button>
               
            </div>
        </form>
    </div>
</div>
</div>




	<!-- Верификация -->


<!-- СМЕНА ПАРОЛЯ -->
<?php echo '
	<script type="text/javascript">
	$(\'#clear\').bind(\'click\',function(){
			$(\'#oldpass\').val(\'\');
			$(\'#newpass\').val(\'\');
			$(\'#newpass2\').val(\'\');
	});
    $(function(){
            $( \'#tabs li\' ).click( function(){
                rel = $( this ).attr( "rel" );
                if(!rel){
                    $(\'#submitform\').show();
                } else {
                    $(\'#submitform\').hide();
                }
            });
        });
	</script>
'; ?>

<?php echo '
	<script>
	
$(\'.passes\').bind("change keyup input click", function() {
		   if (this.value.match(/[^0-9a-zA-Z]/g)) {
				this.value = this.value.replace(/[^0-9a-zA-Z]/g, \'\');
		   }
  });
	
$(\'#newpass\').focusout(function(){
				$(\'#galki li\').removeClass(\'galochka\');
						
				var test = $(\'#newpass\').val();
				var errors = 0;
					if (--test.split(/[a-z]/).length>1)
					{
						$(\'#galki li:first-child\').addClass(\'galochka\');
					
					}else{
					  var errors = 1;
					
					}
						
					if (--test.split(/[0-9]/).length>1)
						{
						$(\'#galki li:nth-child(2)\').addClass(\'galochka\');
						}
					else
						{
						 var errors = 1;
						}
					
					if (--test.split(/[a-zA-z0-9]/).length>9 && --test.split(/[a-zA-z0-9]/).length<17)
						{
							$(\'#galki li:nth-child(3)\').addClass(\'galochka\');
						}
					else
						{
							 var errors = 1;
						}
					if (--test.split(/[a-zA-z0-9]/).length<17 && --test.split(/[a-zA-z0-9]/).length>1)
						{
							$(\'#galki li:last-child\').addClass(\'galochka\');
						}
					else	
						{
							var errors = 1;
						}
						var test2 = $(\'#newpass2\').val();
					if(errors ==true)
					{
						$(\'#save2\').prop(\'disabled\',true);
					}
	});
	$(\'#newpass2\').focusout(function()
	{
		var test = $(\'#newpass\').val();
		var test2 = $(\'#newpass2\').val();
		if(test == test2)
			{
				$(\'#save2\').prop(\'disabled\',false);
				$(\'#galkun\').addClass(\'galkun\');
			}
		else
			{
				$(\'#save2\').prop(\'disabled\',true);
			}
	});
	
	
	var nav = $(\'#nav\');
	var selection = $(\'.selectsubs\');
	var select = selection.find(\'li\');
	nav.click(function(event) {
		if (nav.hasClass(\'active\')) {
			nav.removeClass(\'active\');
			selection.stop().slideUp(200);
		} else {
			nav.addClass(\'active\');
			selection.stop().slideDown(200);
		}
		event.preventDefault();
		});
	select.click(function(event) {
		select.removeClass(\'active\');
		nav.trigger(\'click\');
		$(\'#cm_subscribe\').attr(\'value\', $(this).attr(\'data-value\'));
	 });
	</script>
'; ?>

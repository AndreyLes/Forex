<?php /* Smarty version 2.6.28, created on 2014-09-12 10:24:24
         compiled from com_registration.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'add_js', 'com_registration.tpl', 67, false),array('function', 'csrf_token', 'com_registration.tpl', 70, false),array('function', 'city_input', 'com_registration.tpl', 138, false),array('function', 'captcha', 'com_registration.tpl', 209, false),array('modifier', 'escape', 'com_registration.tpl', 79, false),)), $this); ?>
<div id="modal" class="modal hide fade" style='width: 557px;'>
	

	
	
	<div class="modal-body" style='height: 400px;
	
border: 5px solid #ccc;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='margin-top: 10px;
display: inline-block;'>
<p>Уведомление о рисках</p>

Компания iMoneyBox., именуемая в дальнейшем Компания, с одной стороны и любое 
физическое и юридическое лицо, именуемое в дальнейшем Клиент, с другой стороны, 
заключили настоящий контракт о нижеследующем.

Контракт устанавливает общие условия и порядок, в соответствии с которыми Клиент 
получает от Компании информацию в виде консультаций на рынке iMoneyBox, а также 
индексов, металлов и других ресурсов посредством доступа к финансово-
информационной системе iMoneyBox.

Компания также предоставляет Клиенту услуги по доступу к потоку экономических 
новостей в реальном времени посредством ФИС - доступ к информационным лентам 
(экономическим, финансовым), аналитическим прогнозам и материалами, а так же 
проведения программ обучения и турниров.
Компания имеет право не выполнять самостоятельно свои обязательства по данному 
Контракту, а передать обязательства к исполнению другим родственным Компаниям или 
юридическим лицам, имеющим доступ к ФИС.

Сделки заключает Клиент от своего имени и на свой риск, в сети Интернет через 
торговый терминал Компании (далее по тексту "Терминал"). Время работы Компании – 
круглосуточно с 02 часа 00 минут  понедельника до 02 часа 00 минут  субботы – зимнее 
время, с 01 часа 00 минут  понедельника до 01 часа 00 минут  субботы – летнее время.

Понятия и термины, используемые при предоставлении информации, а также программ 
обучения и турниров, являются общепринятыми на международном валютном рынке
 iMoneyBox.</span>
	</div>
</div>

<div class="con_heading"><?php echo $this->_tpl_vars['pagetitle']; ?>
</div>
<?php echo $this->_tpl_vars['LANG']['REG_INFO']; ?>

<?php echo $this->_tpl_vars['LANG']['ZAPOLNY']; ?>

<?php if ($this->_tpl_vars['cfg']['is_on']): ?>

    <?php if ($this->_tpl_vars['cfg']['reg_type'] == 'invite' && ! $this->_tpl_vars['correct_invite']): ?>

        <p style="margin-bottom:15px; font-size: 14px"><?php echo $this->_tpl_vars['LANG']['INVITES_ONLY']; ?>
</p>

        <form id="regform" name="regform" method="post" action="/registration">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><strong><?php echo $this->_tpl_vars['LANG']['INVITE_CODE']; ?>
:</strong></td>
                <td style="padding-left:15px">
                    <input type="text" name="invite_code" class="text-input" value="" style="width:300px"/>
                </td>
                <td style="padding-left:5px">
                    <input type="submit" name="show_invite" value="<?php echo $this->_tpl_vars['LANG']['SHOW_INVITE']; ?>
" />
                </td>
            </tr>
        </table>
        </form>

    <?php else: ?>

        <?php echo cmsSmartyAddJS(array('file' => 'components/registration/js/check.js'), $this);?>


        <form id="regform" name="regform" method="post" action="/registration/add">
            <input type="hidden" name="csrf_token" value="<?php echo smarty_function_csrf_token(array(), $this);?>
" />
            <table  border="0" align="center" cellpadding="5" cellspacing="0">
	      <?php if ($this->_tpl_vars['cfg']['name_mode'] == 'nickname'): ?>
                    <tr>
                        <td valign="top" class="" width="269">
                            <div><strong><?php echo $this->_tpl_vars['LANG']['NICKNAME']; ?>
:</strong></div>
                            <small><?php echo $this->_tpl_vars['LANG']['NICKNAME_TEXT']; ?>
</small>
                        </td>
                        <td valign="top" class="">
                            <input name="nickname" id="nickinput" class="text-input" type="text" style="width:300px" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['nickname'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" />
                            <span class="regstar">*</span>
                        </td>
                    </tr>
                <?php else: ?>
				
				    <tr>
						<td valign="top" class="">
                            <div><strong><?php echo $this->_tpl_vars['LANG']['NAME1']; ?>
<span class="regstar">*</span>:</strong></div>
                        </td>
                        <td valign="top" class="">
                            <input name="realname1" id="realname1" class="text-input" type="text" style="width:212px" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['realname1'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" />
                          
                        </td>
						
                    </tr>
					     <tr>
                        <td valign="top" class="">
                            <div><strong><?php echo $this->_tpl_vars['LANG']['SURNAME']; ?>
  <span class="regstar">*</span>:</strong></div>
                        </td>
                        <td valign="top" class="">
                            <input name="realname2" id="realname2" class="text-input" type="text" style="width:212px" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['realname2'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" />
                       
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td width="530" valign="top" class="">
                        <div><strong><?php echo $this->_tpl_vars['LANG']['LOGIN']; ?>
  <span class="regstar">*</span>:</strong></div>
                        
                    </td>
                    <td valign="top" class="">
                        <input name="login" id="logininput" class="text-input" type="text" style="width:212px" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['login'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" onchange="" autocomplete="off"/>
						<input id='check' type='button' value='Проверить' onclick='checkLogin()' />
                      
                        <div id="logincheck"></div>
                    </td>
                </tr>
          
               
                <tr>
                    <td valign="top" class=""><strong><?php echo $this->_tpl_vars['LANG']['PASS']; ?>
   <span class="regstar">*</span>:</strong></td>
                    <td valign="top" class="">
                        <input name="pass" id="pass1input" class="text-input" type="password" style="width:212px" onchange="$('#passcheck').html('');"/>
                     
                    </td>
                </tr>
                <tr>
                    <td valign="top" class=""><strong><?php echo $this->_tpl_vars['LANG']['REPEAT_PASS']; ?>
   <span class="regstar">*</span>: </strong></td>
                    <td valign="top" class="">
                        <input name="pass2" id="pass2input" class="text-input" type="password" style="width:212px" onchange="checkPasswords()" />
                  
                        <div id="passcheck"></div>
                    </td>
                </tr>
				<?php if ($this->_tpl_vars['cfg']['ask_city']): ?>
                    <tr>
                        <td valign="top" class=""><strong><?php echo $this->_tpl_vars['LANG']['CITY']; ?>
:</strong></td>
                        <td valign="top" class="" >
                            <?php echo smarty_function_city_input(array('value' => $this->_tpl_vars['item']['city'],'name' => 'city','width' => '212px'), $this);?>

                        </td>
                    </tr>
                <?php endif; ?>
				
				
                    
       
                    <tr>
                        <td valign="top" class=""><strong>Моб. телефон:</strong></td>
						
                        <td valign="top" class="">
                            <input name="mobtelephone" type="text" class="text-input" id="mobtelephone" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['mobtelephone'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" style="width:212px"/>
                        </td>
                    </tr>
					   <tr>
                    <td valign="top" class="">
                        <div><strong><?php echo $this->_tpl_vars['LANG']['EMAIL']; ?>
     <span class="regstar">*</span>:</strong></div>
                       
                    </td>
                    <td valign="top" class="">
                        <input name="email" type="text" class="text-input" style="width:212px" value="<?php echo $this->_tpl_vars['item']['email']; ?>
"/>
                   
                    </td>
                </tr>
			<?php if ($this->_tpl_vars['user']): ?>	<tr>
						 <td valign="top" class="">
							 <div><strong><?php echo $this->_tpl_vars['LANG']['USER_REF']; ?>
</strong></div>
							</td>
						<td>
							<?php echo $this->_tpl_vars['user']; ?>
<?php if ($this->_tpl_vars['online']): ?><span> (<?php echo $this->_tpl_vars['online']; ?>
)</span><?php endif; ?>
						
						</td>
					
					</tr>
			<?php endif; ?>
				<tr><td></td><td></td></tr>
				<tr><td colspan='2' style='text-align:center;'><?php echo $this->_tpl_vars['LANG']['TABLE_TEXT']; ?>
</td></tr>
				<tr style='	border-bottom:1px dashed #000;'><td></td><td></td></tr>
               
				<tr><td>
				<span class='sogl' style='font-weight:bold;color:#f30000'><?php echo $this->_tpl_vars['LANG']['SOGL']; ?>
</span>
			
					<ol style='margin-top:20px'>
						<li><input type='checkbox' id='checkbox-id1' value='v1' name='check1'/><label for="checkbox-id1"><?php echo $this->_tpl_vars['LANG']['RISK']; ?>
</label></li>
						<li><input type='checkbox' id='checkbox-id2' value='v2' name='check2'/><label for="checkbox-id2"><?php echo $this->_tpl_vars['LANG']['DOGOVOR']; ?>
</label></li>
						<li><input type='checkbox' id='checkbox-id3' value='v3' name='check3'/><label for="checkbox-id3"><?php echo $this->_tpl_vars['LANG']['REGLAMENT']; ?>
</label></li>
				
					</ol></td>
					
					<td>
						<ul style='margin-top: 37px;list-style: none;float: left;margin-right: 85px;padding-left: 0;'>
							<li><input class='download' type='button' value='Скачать' /></li>
							<li><input class='download' type='button' value='Скачать' /></li>
							<li><input class='download' type='button' value='Скачать' /></li>
						</ul>
						<ul style='margin-top: 37px;list-style:none'>
						
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							
						</ul>
					</td>
				</tr>
				<tr><td></td><td></td></tr>
					<tr style='	border-bottom:1px dashed #000;'><td></td><td></td></tr>
				 <tr >
                    <td  valign="top" class="bortop">
                        <div style='width:250px'><strong><?php echo $this->_tpl_vars['LANG']['SECUR_SPAM']; ?>
: </strong></div>
                    </td>
                    <td valign="top" class=""><?php echo smarty_function_captcha(array(), $this);?>
</td>
                </tr>
                <tr>
                    <td valign="top" class="">&nbsp;</td>
                    <td valign="top" class="">
                        <input name="do" type="hidden" value="register" />
                        <input name="save" class="classname" type="submit" id="save" value="<?php echo $this->_tpl_vars['LANG']['REGISTRATION']; ?>
" />
                    </td>
                </tr>
            </table>
        </form>

				
    <?php endif; ?>

<?php else: ?>

    <div style="margin-top:10px"><?php echo $this->_tpl_vars['cfg']['offmsg']; ?>
</div>

<?php endif; ?>

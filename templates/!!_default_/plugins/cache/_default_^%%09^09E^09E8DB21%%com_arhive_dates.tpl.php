<?php /* Smarty version 2.6.28, created on 2014-07-01 18:10:44
         compiled from com_arhive_dates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'spellcount', 'com_arhive_dates.tpl', 7, false),)), $this); ?>
<div class="con_heading"><?php echo $this->_tpl_vars['pagetitle']; ?>
</div>

<?php if ($this->_tpl_vars['items']): ?>
    <ul class="arhive_list">
        <?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>
            <li>
                <a href="/arhive/<?php echo $this->_tpl_vars['item']['year']; ?>
/<?php echo $this->_tpl_vars['item']['month']; ?>
"><?php echo $this->_tpl_vars['item']['fmonth']; ?>
</a><?php if ($this->_tpl_vars['do'] == 'view'): ?>, <a href="/arhive/<?php echo $this->_tpl_vars['item']['year']; ?>
"><?php echo $this->_tpl_vars['item']['year']; ?>
</a><?php endif; ?> <span>(<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['num'])) ? $this->_run_mod_handler('spellcount', true, $_tmp, $this->_tpl_vars['LANG']['ARTICLE1'], $this->_tpl_vars['LANG']['ARTICLE2'], $this->_tpl_vars['LANG']['ARTICLE10']) : smarty_modifier_spellcount($_tmp, $this->_tpl_vars['LANG']['ARTICLE1'], $this->_tpl_vars['LANG']['ARTICLE2'], $this->_tpl_vars['LANG']['ARTICLE10'])); ?>
)</span>
            </li>
        <?php endforeach; endif; unset($_from); ?>
    </ul>
<?php else: ?>
    <p><?php echo $this->_tpl_vars['LANG']['ARHIVE_NO_MATERIALS']; ?>
</p>
<?php endif; ?>
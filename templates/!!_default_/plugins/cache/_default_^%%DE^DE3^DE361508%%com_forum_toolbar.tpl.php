<?php /* Smarty version 2.6.28, created on 2014-05-15 19:47:48
         compiled from com_forum_toolbar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'template', 'com_forum_toolbar.tpl', 5, false),array('function', 'csrf_token', 'com_forum_toolbar.tpl', 57, false),)), $this); ?>
<table cellspacing="2" cellpadding="3" align="right">
    <tr>
        <?php if (! $this->_tpl_vars['thread']['closed']): ?>
        <td width="16">
            <img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/add.png"/>
        </td>
        <td>
            <a href="/forum/reply<?php echo $this->_tpl_vars['thread']['id']; ?>
.html"><strong><?php echo $this->_tpl_vars['LANG']['NEW_MESSAGE']; ?>
</strong></a>
        </td>
        <?php if (! $this->_tpl_vars['is_subscribed']): ?>
            <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/subscribe.png"/></td>
            <td><a href="/forum/subscribe<?php echo $this->_tpl_vars['thread']['id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['SUBSCRIBE_THEME']; ?>
</a></td>
        <?php else: ?>
            <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/unsubscribe.png"/></td>
            <td><a href="/forum/unsubscribe<?php echo $this->_tpl_vars['thread']['id']; ?>
.html"><?php echo $this->_tpl_vars['LANG']['UNSUBSCRIBE']; ?>
</a></td>
        <?php endif; ?>
        <?php else: ?>
            <td><strong><?php echo $this->_tpl_vars['LANG']['THREAD_CLOSE']; ?>
</td>
        <?php endif; ?>

        <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder']): ?>
            <td width="16" class="closethread" <?php if ($this->_tpl_vars['thread']['closed']): ?>style="display: none"<?php endif; ?>>
                <img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/toolbar/lock_open.png"/>
            </td>
            <td class="closethread" <?php if ($this->_tpl_vars['thread']['closed']): ?>style="display: none"<?php endif; ?>>
                <a class="ajaxlink" href="javascript:" onclick="forum.ocThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
, 1);"><?php echo $this->_tpl_vars['LANG']['CLOSE']; ?>
</a>
            </td>
            <td width="16" class="openthread" <?php if (! $this->_tpl_vars['thread']['closed']): ?>style="display: none"<?php endif; ?>>
                <img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/toolbar/lock.png"/>
            </td>
            <td class="openthread" <?php if (! $this->_tpl_vars['thread']['closed']): ?>style="display: none"<?php endif; ?>>
                <a class="ajaxlink" href="javascript:" onclick="forum.ocThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
, 0);"><?php echo $this->_tpl_vars['LANG']['OPEN']; ?>
</a>
            </td>

            <td width="16" class="pinthread" <?php if ($this->_tpl_vars['thread']['pinned']): ?>style="display: none"<?php endif; ?>>
                <img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/toolbar/pinthread.png"/>
            </td>
            <td class="pinthread" <?php if ($this->_tpl_vars['thread']['pinned']): ?>style="display: none"<?php endif; ?>>
                <a class="ajaxlink" href="javascript:" onclick="forum.pinThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
, 1);"><?php echo $this->_tpl_vars['LANG']['PIN']; ?>
</a>
            </td>
            <td width="16" class="unpinthread" <?php if (! $this->_tpl_vars['thread']['pinned']): ?>style="display: none"<?php endif; ?>>
                <img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/toolbar/unpinthread.png"/>
            </td>
            <td class="unpinthread" <?php if (! $this->_tpl_vars['thread']['pinned']): ?>style="display: none"<?php endif; ?>>
                <a class="ajaxlink" href="javascript:" onclick="forum.pinThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
, 0);"><?php echo $this->_tpl_vars['LANG']['UNPIN']; ?>
</a>
            </td>

            <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/move.png"/></td>
            <td><a class="ajaxlink" href="javascript:" onclick="forum.moveThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
);"><?php echo $this->_tpl_vars['LANG']['MOVE']; ?>
</a></td>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder'] || $this->_tpl_vars['thread']['is_mythread']): ?>
            <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/edit.png"/></td>
            <td><a class="ajaxlink" href="javascript:" onclick="forum.renameThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
);"><?php echo $this->_tpl_vars['LANG']['RENAME']; ?>
</a></td>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['is_admin'] || $this->_tpl_vars['is_moder']): ?>
            <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/delete.png"/></td>
            <td><a class="ajaxlink" href="javascript:" onclick="forum.deleteThread(<?php echo $this->_tpl_vars['thread']['id']; ?>
, '<?php echo smarty_function_csrf_token(array(), $this);?>
');"><?php echo $this->_tpl_vars['LANG']['DELETE']; ?>
</a></td>
        <?php endif; ?>
        <td width="16"><img src="/templates/<?php echo cmsSmartyCurrentTemplate(array(), $this);?>
/images/icons/forum/toolbar/back.png"/></td>
        <td><a href="/forum/<?php echo $this->_tpl_vars['forum']['id']; ?>
"><?php echo $this->_tpl_vars['LANG']['BACKB']; ?>
</a></td>
    </tr>
</table>
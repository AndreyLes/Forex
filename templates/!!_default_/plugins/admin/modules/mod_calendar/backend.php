<?php
if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); }
/******************************************************************************/
//                                                                            //
//                             CMS Vadyus v1.8                                //
//                        http://www.CMS Vadyus.ru/                           //
//                                                                            //
//                   written by CMS Vadyus Team, 2007-2011                    //
//                produced by VadyusSoft, (www.Vadyussoft.ru)               //
//                                                                            //
//                        LICENSED BY GNU/GPL v2                              //
//                                                                            //
/******************************************************************************/

	//LOAD CURRENT CONFIG
    $cfg = $inCore->loadModuleConfig($_REQUEST['id']);
	$inCore->loadModel('calendar');
    $model      = new cms_model_calendar();	
	cpAddPathway('Календарь событий', '?view=modules&do=edit&id='.$_REQUEST['id']);
	cpAddPathway('Настройки', '?view=modules&do=config&id='.$_REQUEST['id']);	
	echo '<h3>Календарь событий</h3>';
	if (isset($_REQUEST['opt'])) { $opt = $inCore->request('opt', 'str'); } else { $opt = 'config'; }
	
	if($opt=='save'){	

        $cfg = array();
		$cfg['city']  = $inCore->request('city', 'str');
		$cat_id=substr($inCore->request('cat_id', 'str','cat_'),4);
		$cfg['cat_id']  = $cat_id ? $cat_id : '0';
		$cfg['users_can_add']   = $inCore->request('users_can_add', 'int');
		$cfg['extract_picture_mod']  = $inCore->request('extract_picture_mod', 'int');
		$cfg['datepicker_view']  = $inCore->request('datepicker_view', 'str');
		$cfg['datepicker_center']  = $inCore->request('datepicker_center', 'int');
		$cfg['color_cal_header']  = $inCore->request('color_cal_header', 'str');
		$cfg['color_def_day']   = $inCore->request('color_def_day', 'str');
		$cfg['color_events_day']  = $inCore->request('color_events_day', 'str');        
		$inCore->saveModuleConfig($_REQUEST['id'], $cfg);
		$msg = 'Настройки сохранены.';
		$opt = 'config';

	}	


	if ($opt == 'config') {

		if (@$msg) { echo '<p class="success">'.$msg.'</p>'; }
			
        $GLOBALS['cp_page_head'][] = '<script type="text/javascript" src="/includes/jquery/jquery.form.js"></script>';
		$GLOBALS['cp_page_head'][] = '<script type="text/javascript" src="/components/calendar/js/cat_tree.js"></script>';
		$GLOBALS['cp_page_head'][] = '<link href="/components/calendar/css/calendar.css" rel="stylesheet" type="text/css" />';

		//DEFAULT VALUES
		if (!isset($cfg['city'])) { $cfg['city'] = ''; }
		if (!isset($cfg['cat_id'])) { $cfg['cat_id'] = '0'; }
		if (!isset($cfg['users_can_add'])) { $cfg['users_can_add'] = 1; }
		if (!isset($cfg['extract_picture_mod'])) { $cfg['extract_picture_mod'] = 1; }
		if (!isset($cfg['datepicker_view'])) { $cfg['datepicker_view'] = 'block'; }
		if (!isset($cfg['datepicker_center'])) { $cfg['datepicker_center'] = 0; }
		if (!isset($cfg['color_cal_header'])) { $cfg['color_cal_header'] = '#CCCCEE'; }
		if (!isset($cfg['color_def_day'])) { $cfg['color_def_day'] = '#BDCCE4'; }
		if (!isset($cfg['color_events_day'])) { $cfg['color_events_day'] = '#B6E5C2'; }       
		?>			

<form action="index.php?view=modules&do=config&id=<?php echo $_REQUEST['id'];?>" method="post" name="optform" target="_self" id="form1">
            <fieldset style="width: 500px"><legend>Фильтр</legend>
             <table width="600" border="0" cellpadding="0" cellspacing="10" class="proptable" style="border:none">
                <tr>
                    <td valign="top">
                        <strong>Город:</strong>
                    </td>
                    <td valign="top">
                       <?php echo $model->calendarCities($cfg['city']); ?>
                    </td>
                <tr> 
                    <td valign="top">
                        <strong>Категория: &nbsp;<img id="cat_arrow" style="float:right; margin-top: 5px" src="/components/calendar/images/arrow_right.gif" /></strong>
                    </td>
                    <td valign="top"><input name="cat_ed" type="text" id="cat_ed" style="width:250px" value="<?php echo $model->categoryTitle($cfg['cat_id']); ?>" /><input id="cat_id" name="cat_id" style="display: none" value="cat_<?php echo $cfg['cat_id']; ?>" />
                    </td>
                </tr>
                <tr id="td_cat_tree" style="display: none">
                    <td></td>    
                    <td>
                       <div id="cat_tree" style="display: block"></div>
                     </td>			
		        </tr>		                                  
            </table>
            </fieldset>           
            <table width="600" border="0" cellpadding="0" cellspacing="10" class="proptable" style="border:none"> 
                 <tr>
                    <td><strong>Разрешить зарегистрированным пользователям добавлять события:</strong></td>
                    <td width="250">
                        <input name="users_can_add" type="radio" value="1" <?php if (@$cfg['users_can_add']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="users_can_add" type="radio" value="0" <?php if (@!$cfg['users_can_add']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                 </tr>
                 <tr>
                    <td><strong>Извлекать первое изображение текста события:</strong></td>
                    <td width="250">
                        <input name="extract_picture_mod" type="radio" value="1" <?php if (@$cfg['extract_picture_mod']) { echo 'checked="checked"'; } ?>/> Да
                        <input name="extract_picture_mod" type="radio" value="0" <?php if (@!$cfg['extract_picture_mod']) { echo 'checked="checked"'; } ?>/> Нет
                    </td>
                </tr>                             
             </table>
             <fieldset style="width: 500px"><legend>Виджет календаря</legend>
             <table width="600" border="0" cellpadding="0" cellspacing="10" class="proptable" style="border:none">
                <tr>
                    <td valign="top">
                        <strong>Вид:</strong>
                    </td>
                    <td valign="top">
                        <input name="datepicker_view" type="radio" value="block" <?php if (@$cfg['datepicker_view']=='block') { echo 'checked="checked"'; } ?>/> Классический
                        <input name="datepicker_view" type="radio" value="string" <?php if (@$cfg['datepicker_view']=='string') { echo 'checked="checked"'; } ?>/> Растянутый
                        <input name="datepicker_view" type="radio" value="simple" <?php if (@$cfg['datepicker_view']=='simple') { echo 'checked="checked"'; } ?>/> Простой
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong>По центру ширины модуля:</strong>
                    </td>
                    <td valign="top">
                        <input name="datepicker_center" type="checkbox" value="1" <?php if (@$cfg['datepicker_center']) { echo 'checked="checked"'; } ?>/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong>Цвет шапки:</strong>
                    </td>
                    <td valign="top">
                        <?php echo '<input name="color_cal_header" class="color_input" type="text" value="'.$cfg['color_cal_header'].'" /><span class="color_show" style="margin-left:10px; font-size: 17px; background-color:'.$cfg['color_cal_header'].'; color:'.$cfg['color_cal_header'].'">Hi!</span><span class="color_show" style="font-size: 17px; background-color:'.$cfg['color_cal_header'].'; color:'.$cfg['color_cal_header'].'">Hi!</span><span class="color_value" style="margin-left:10px; color: #AAAAAA"></span>' ?>
                    </td>
                </tr> 
                <tr>
                    <td valign="top">
                        <strong>Цвет дня по умолчанию:</strong>
                    </td>
                    <td valign="top">
                        <?php echo '<input name="color_def_day" class="color_input" type="text" value="'.$cfg['color_def_day'].'" /><span class="color_show" style="margin-left:10px; font-size: 17px; background-color:'.$cfg['color_def_day'].'; color:'.$cfg['color_def_day'].'">Hi!</span><span  class="color_show" style="font-size: 17px; background-color:'.$cfg['color_def_day'].'; color:'.$cfg['color_def_day'].'">Hi!</span><span class="color_value" style="margin-left:10px; color: #AAAAAA"></span>' ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong>Цвет дня с событиями:</strong>
                    </td>
                    <td valign="top">
                        <?php echo '<input name="color_events_day" class="color_input" type="text" value="'.$cfg['color_events_day'].'" /><span class="color_show" style="margin-left:10px; font-size: 17px; background-color:'.$cfg['color_events_day'].'; color:'.$cfg['color_events_day'].'">Hi!</span><span class="color_show"  style="font-size: 17px; background-color:'.$cfg['color_events_day'].'; color:'.$cfg['color_events_day'].'">Hi!</span><span class="color_value" style="margin-left:10px; color: #AAAAAA"></span>' ?>
                    </td>
                </tr>                              
            </table>
            </fieldset> 

    <p>
        <input name="opt" type="hidden" id="do" value="save" />
        <input name="save" type="submit" id="save" value="Сохранить" />
        <input name="back" type="button" id="back" value="Отмена" onclick="window.location.href='index.php?view=modules';"/>
    </p>
</form>

<?php } ?>
<script type="text/javascript">
$(function() {
   $(".color_input").bind('keyup mouseout', function () {
	 $(this).next().next().css('background-color', $(this).val()).css('color', $(this).val());
   });
   $(".color_show").click(function () {  
	 var bg10=/rgb\(\s?(\d+)\s?,\s?(\d+)\s?,\s?(\d+)\s?\)/.exec($(this).css('background-color')), temp;
	 if (bg10) $(this).nextAll(".color_value").html('#'+((temp=Number(bg10[1]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp)+((temp=Number(bg10[2]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp)+((temp=Number(bg10[3]).toString(16).toUpperCase()).length<2 ? '0'+temp : temp));
   });
   
    cur_cat_id=$("#cat_id").val();
	cur_cat_title=$("#cat_ed").val();
	if (!cur_cat_title) {
		$("#cat_ed").val('Все категории');
		cur_cat_title='Все категории';
	}
	refreshCatTree();
	$("#cat_arrow").click(function () {
		if (!is_cat_tree_visible) {
			$('#td_cat_tree').show(); 
			$("#cat_arrow").attr('src','/components/calendar/images/arrow_down.gif'); 
			is_cat_tree_visible=1; 
		} else {
			$('#td_cat_tree').hide(); 
			$("#cat_arrow").attr('src','/components/calendar/images/arrow_right.gif'); 
			is_cat_tree_visible=0;					   
		}				   
	}); 
	$("#cat_tree").bind('mousedown',openCloseGroup);
	$("#cat_ed").bind('keydown',function(e) { //очистка поля категории
		if (e.keyCode==8) { //backspace
		   $("#cat_ed").val('Все категории');
		   cur_cat_title='Все категории';
		   $("#cat_id").val('0');
		} else { //ничего не меняем
		   $("#cat_ed").val(cur_cat_title);
		}
		e.preventDefault();
		return false;				
	});	
});

</script>
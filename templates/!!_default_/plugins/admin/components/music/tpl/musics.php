<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top:2px">
    <tr>
        <td valign="top" width="240" id="cats_cell">
            <div class="cat_add_link">
                <div>
                    <a href="?view=components&do=config&id=<?php echo $id; ?>&opt=add_cat" style="color:#09C"><?php echo $_LANG["ADD_CAT"]; ?></a>
                </div>
            </div>
            <div class="cat_link">
                <div>
                <?php if ($category_id) { ?>
                    <a href="<?php echo $base_uri; ?>" style="font-weight:bold"><?php echo $_LANG["ALL_MUSIC"]; ?></a>
                <?php } else { $current_cat = $_LANG["ALL_MUSIC"]; echo $_LANG["ALL_MUSIC"]; } ?>
                </div>
            </div>
            <div class="cat_link">
                <div>
                <?php if ($category_id != 1) { ?>
                    <a href="<?php echo $base_uri . "&cat_id=1"; ?>" style="font-weight:bold"><?php echo $_LANG["ROOT_CAT"]; ?></a>
                <?php } else { $current_cat = $_LANG["ROOT_CAT"]; echo $_LANG["ROOT_CAT"]; } ?>
                </div>
            </div>
            <?php if($cats) { ?>
                <?php foreach($cats as $num=>$cat) { ?>
                    <div style="padding-left:<?php echo ($cat['NSLevel']-1)*20; ?>px" class="cat_link">
                        <div onmouseover="$(this).find('.music').show()" onmouseout="$(this).find('.music').hide()">
                            <?php if ($category_id != $cat['id']) { ?>
                                <a href="<?php echo $base_uri.'&cat_id='.$cat['id']; ?>" style="<?php if ($cat['NSLevel']==1){ echo 'font-weight:bold'; } ?>"><?php echo $cat['title']; ?></a>
                            <?php } else { ?>
                                <?php echo $cat['title']; $current_cat = $cat['title']; ?>
                            <?php } ?>
                            <span class="music" style="display:none">
                                <a href="?view=components&do=config&id=<?php echo $id; ?>&opt=move_cat&cat_id=<?php echo $cat['id']; ?>&dir=up" title="<?php echo $_LANG["UP"]; ?>"><img src="/admin/components/music/img/cat_up.png" border="0" /></a>
                                <a href="?view=components&do=config&id=<?php echo $id; ?>&opt=move_cat&cat_id=<?php echo $cat['id']; ?>&dir=down" title="<?php echo $_LANG["DOWN"]; ?>"><img src="/admin/components/music/img/cat_down.png" border="0" /></a>
                            </span>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </td>

        <td valign="top" id="slide_cell" class="<?php if ($hide_cats){ ?>unslided<?php } ?>" onclick="$('#cats_cell').toggle();$(this).toggleClass('unslided');$('#filter_form input[name=hide_cats]').val(1-$('#cats_cell:visible').length)">&nbsp;
        </td>

        <td valign="top" style="padding-left:2px">
            <form action="<?php echo $base_uri; ?>" method="GET" id="filter_form">
                <input type="hidden" name="view" value="components" />
                <input type="hidden" name="do" value="config" />
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="hidden" name="opt" value="list_cats" />
                <input type="hidden" name="cat_id" value="<?php echo $category_id; ?>" />
                <table class="toolmenu" cellpadding="5" border="0" width="100%" style="margin-bottom: 2px;">
                    <tr>
                        <td width="">
                            <span style="font-size:16px;color:#0099CC;font-weight:bold;">
                                <?php echo $current_cat . ' ('.$total.')'; ?>
                            </span>
                            <span style="padding-left: 15px;">
                                <a title="<?php echo $_LANG["ADD_MUSIC"]; ?>" href="?view=components&do=config&id=<?php echo $id; ?>&opt=add_music<?php if($category_id){ ?>&cat_id=<?php echo $category_id; } ?>">
                                    <img border="0" hspace="2" alt="<?php echo $_LANG["ADD_MUSIC"]; ?>" src="images/actions/add.gif"/>
                                </a>
                                <?php if($category_id > 1){ ?>
                                    <a title="<?php echo $_LANG["EDIT_CAT"]; ?>" href="?view=components&do=config&id=<?php echo $id; ?>&opt=edit_cat&item_id=<?php echo $category_id; ?>">
                                        <img border="0" hspace="2" alt="<?php echo $_LANG["EDIT_CAT"]; ?>" src="images/actions/edit.gif"/>
                                    </a>
                                    <a title="<?php echo $_LANG["DEL_CAT"]; ?>" onclick="jsmsg('<?php echo $_LANG["DEL_CAT"]; ?>?', '?view=components&do=config&id=<?php echo $id; ?>&opt=delete_cat&item_id=<?php echo $category_id; ?>')" href="#">
                                        <img border="0" hspace="2" alt="<?php echo $_LANG["DEL_CAT"]; ?>" src="images/actions/delete.gif"/>
                                    </a>
                                <?php } ?>
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="toolmenu">
                    <table cellpadding="5" border="0" style="margin-bottom: 2px; font-size:11px; vertical-align:middle;" id="filterpanel">
                        <tr>
                            <td width="110">
                                <select name="orderby" style="width:110px" onchange="$('#filter_form').submit()">
                                    <option value="name" <?php if($orderby=='name'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["BY_NAME"]; ?>
                                    </option>
                                    <option value="listen" <?php if($orderby=='listen'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["BY_LISTENED"]; ?>
                                    </option>
                                    <option value="rate_value" <?php if($orderby=='rate_value'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["BY_VOTES"]; ?>
                                    </option>
                                    <option value="download_num" <?php if($orderby=='download_num'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["BY_DOWNLOADS"]; ?>
                                    </option>
                                    <option value="pubdate" <?php if($orderby=='pubdate'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["BY_DATE"]; ?>
                                    </option>
                                </select>
                            </td>
                            <td width="115">
                                <select name="orderto" style="width:115px" onchange="$('#filter_form').submit()">
                                    <option value="asc" <?php if($orderto=='asc'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["ASC"]; ?>
                                    </option>
                                    <option value="desc" <?php if($orderto=='desc'){ ?>selected="selected"<?php } ?>>
                                        <?php echo $_LANG["DESC"]; ?>
                                    </option>
                                </select>
                            </td>
                            <td width="70">
                                <label for="is_not_publ"><?php echo $_LANG["HIDDEN"]; ?></label>
                                <input name="is_not_publ" id="is_not_publ" type="checkbox" value="1"<?php if($is_not_publ==1){ ?>checked="checked" onclick="javascript:document.location.href='<?php echo $base_uri.'&cat_id='.$category_id.'&is_not_publ=all'; ?>'"<?php } else { ?> onchange="$('#filter_form').submit()" <?php } ?> />
                            </td>
                        </tr>
                    </table>
                </div>
            </form>

            <form name="selform" action="index.php?view=components" method="post">
                <table id="listTable" class="tablesorter" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:0px">
                    <thead>
                        <tr>
                            <th class="lt_header" align="center" width="15">
                                <a class="lt_header_link" title="<?php echo $_LANG["INVERT"]; ?>" href="javascript:" onclick="javascript:invert()">#</a>
                            </th>
                            <th class="lt_header" width="67"><?php echo $_LANG["DATE"]; ?></th>
                            <th class="lt_header" width=""><?php echo $_LANG["NAME"]; ?></th>
                            <th class="lt_header" width=""><?php echo $_LANG["SINGER"]; ?></th>
                            <th class="lt_header" width=""><?php echo $_LANG["ALBUM"]; ?></th>
                            <th class="lt_header" width="55"><?php echo $_LANG["OPTIONS"]; ?></th>
                            <th class="lt_header" align="center" width="85"><?php echo $_LANG["DO"]; ?></th>
                        </tr>
                    </thead>
                    <?php if ($total){ ?>
                        <tbody>
                            <?php foreach($items as $num=>$item){ ?>
                                <tr id="<?php echo $item['id']; ?>" class="item_tr">
                                    <td><input type="checkbox" name="item[]" value="<?php echo $item['id']; ?>" /></td>
                                    <td style="font-size:9px"><?php echo $item['pubdate']; ?></td>
                                    <td>
                                        <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=edit_music&item_id=<?php echo $item['id']; ?>">
                                            <?php echo $item['name']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=view_singer&singer_id=<?php echo $item['singer_id']; ?>">
                                            <?php echo $item['singer']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=view_album&album_id=<?php echo $item['album_id']; ?>">
                                            <?php echo $item['album']; ?>
                                        </a>
                                    </td>
                                    <td class="flags">
                                        <?php if ($item['published']) { ?>
                                            <a id="publink<?php echo $item['id']; ?>" href="javascript:is_pub(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_item&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=show_item&item_id=<?php echo $item['id']; ?>', 'off', 'on');" title="<?php echo $_LANG["HIDE"]; ?>">
                                                <img id="pub<?php echo $item['id']; ?>" border="0" src="images/actions/on.gif"/>
                                            </a>
                                        <?php } else { ?>
                                            <a id="publink<?php echo $item['id']; ?>" href="javascript:is_pub(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=show_item&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_item&item_id=<?php echo $item['id']; ?>', 'on', 'off');" title="<?php echo $_LANG["SHOW"]; ?>">
                                                <img id="pub<?php echo $item['id']; ?>" border="0" src="images/actions/off.gif"/>
                                            </a>
                                        <?php } ?>
                                        <?php if ($item['is_new']) { ?>
                                            <a id="isnewlink<?php echo $item['id']; ?>" href="javascript:isnew(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_isnew&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=show_isnew&item_id=<?php echo $item['id']; ?>', 'hide_isnew', 'show_isnew');" title="<?php echo $_LANG["REMOVE_IS_NEW"]; ?>">
                                                <img id="isnew<?php echo $item['id']; ?>" border="0" src="/components/music/images/show_isnew.png"/>
                                            </a>
                                        <?php } else { ?>
                                            <a id="isnewlink<?php echo $item['id']; ?>" href="javascript:isnew(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=show_isnew&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_isnew&item_id=<?php echo $item['id']; ?>', 'show_isnew', 'hide_isnew');" title="<?php echo $_LANG["IS_NEW"]; ?>">
                                                <img id="isnew<?php echo $item['id']; ?>" border="0" src="/components/music/images/hide_isnew.png"/>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td align="right">
                                        <div style="padding-right: 3px;">
                                            <a title="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" href="<?php echo $item['seolink'];?>" target="_blank">
                                                <img border="0" hspace="2" alt="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" src="images/actions/search.gif"/>
                                            </a>
                                            <a title="<?php echo $_LANG["EDIT"]; ?>" href="?view=components&do=config&id=<?php echo $id; ?>&opt=edit_music&item_id=<?php echo $item['id']; ?>">
                                                <img border="0" hspace="2" alt="<?php echo $_LANG["EDIT"]; ?>" src="images/actions/edit.gif"/>
                                            </a>
                                            <a title="<?php echo $_LANG["DEL"]; ?>" onclick="jsmsg('<?php echo $_LANG["DEL"]; ?> <?php echo htmlspecialchars($item['title']); ?>?', '?view=components&do=config&id=<?php echo $id; ?>&opt=delete_music&item_id=<?php echo $item['id']; ?>')" href="#">
                                                <img border="0" hspace="2" alt="<?php echo $_LANG["DEL"]; ?>" src="images/actions/delete.gif"/>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } else { ?>
                        <tbody>
                            <td colspan="7" style="padding-left:5px">
                                <div style="padding:15px;padding-left:0px"><?php echo $_LANG["SONGS_NOT_FOUND"]; ?></div>
                            </td>
                        </tbody>
                    <?php } ?>
                </table>
                <?php if ($items){ ?>
                    <div style="margin-top:4px;padding-top:4px;">
                        <table class="" cellpadding="5" border="0" height="40">
                            <tr>
                                <td width="">
                                   <strong style="color:#09C"><?php echo $_LANG["MARKED"]; ?>:</strong>
                                </td>
                                <td width="" class="sel_pub">
                                    <input type="button" name="" value="<?php echo $_LANG["CHANGE"]; ?>" onclick="sendMusicForm(<?php echo $id; ?>, 'edit_music');" />
                                    <input type="button" name="" value="<?php echo $_LANG["MOVE"]; ?>" onclick="$('.sel_move').toggle();$('.sel_pub').toggle();" />
                                </td>
                                <td class="sel_move" style="display:none">
                                    <?php echo $_LANG["MOVE_TO_CAT"]; ?>
                                </td>
                                <td class="sel_move" style="display:none">
                                    <select id="move_cat_id" style="width:250px">
                                        <?php
                                           echo $inCore->getListItemsNS('cms_music_category', $category_id);
                                        ?>
                                    </select>
                                </td>
                                <td class="sel_move" style="display:none">
                                    <input type="button" name="" value="OK" onclick="sendMusicForm(<?php echo $id; ?>, 'move_items', $('select#move_cat_id').val());" />
                                    <input type="button" name="" value="<?php echo $_LANG["CANSEL"]; ?>" onclick="$('td.sel_move').toggle();$('td.sel_pub').toggle();" />
                                </td>
                                <td class="sel_pub">
                                    <input type="button" name="" value="<?php echo $_LANG["SHOW"]; ?>" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'show_item');" />
                                    <input type="button" name="" value="<?php echo $_LANG["HIDE"]; ?>" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'hide_item');" />
                                </td>
                                <td class="sel_pub">
                                    <input type="button" name="" value="<?php echo $_LANG["DEL"]; ?>" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'delete_music');" />
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
                <script type="text/javascript">highlightTableRows("listTable","hoverRow","clickedRow");</script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.lightbox').lightbox();
                    });
                </script>
            </form>
            <?php
                if ($pages>1){
                    echo cmsPage::getPagebar($total, $page, $perpage, $base_uri.'&cat_id='.$category_id.'&page=%page%');
                }
            ?>
        </td>
    </tr>
</table>
<?php if(!defined('VALID_CMS_ADMIN')) { die('ACCESS DENIED'); } ?>
<form action="<?php echo $base_uri; ?>" method="GET" id="filter_form">
    <input type="hidden" name="view" value="components" />
    <input type="hidden" name="do" value="config" />
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php
        if ($album_id){
            echo "<input type=\"hidden\" name=\"opt\" value=\"view_album\" /><input type=\"hidden\" name=\"album_id\" value=\"".$album_id."\" />";
        }else{
            echo "<input type=\"hidden\" name=\"opt\" value=\"view_singer\" /><input type=\"hidden\" name=\"singer_id\" value=\"".$singer_id."\" />";
        }
    ?>
    <table class="toolmenu" cellpadding="5" border="0" width="100%" style="margin-bottom: 2px; font-size:11px; vertical-align:middle;" id="filterpanel">
        <tr>
            <td width="110">
                <select name="orderby" style="width:110px" onchange="$('#filter_form').submit()">
                    <option value="name" <?php if($orderby=='name'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_NAME"]; ?>
                    </option>
                    <option value="listen" <?php if($orderby=='listen'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_LISTENED"]; ?>
                    </option>
                    <option value="rate_value" <?php if($orderby=='rate_value'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_VOTES"]; ?>
                    </option>
                    <option value="download_num" <?php if($orderby=='download_num'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_DOWNLOADS"]; ?>
                    </option>
                    <option value="pubdate" <?php if($orderby=='pubdate'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["BY_DATE"]; ?>
                    </option>
                </select>
            </td>
            <td width="115">
                <select name="orderto" style="width:115px" onchange="$('#filter_form').submit()">
                    <option value="asc" <?php if($orderto=='asc'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["ASC"]; ?>
                    </option>
                    <option value="desc" <?php if($orderto=='desc'){ ?>selected="selected"<?php } ?>>
                        <?php echo $_LANG["DESC"]; ?>
                    </option>
                </select>
            </td>
            <td width="70">
                <label for="is_not_publ"><?php echo $_LANG["HIDDEN"]; ?></label>
                <input name="is_not_publ" id="is_not_publ" type="checkbox" value="1"<?php if($is_not_publ == 1){ ?> checked="checked" onclick="javascript:document.location.href='<?php echo $base_uri.'&is_not_publ=all'; ?>'"<?php } else { ?> onchange="$('#filter_form').submit()" <?php } ?> />
            </td>
            <td>
                <a title="Добавить песню" href="?view=components&do=config&id=<?php echo $id; ?>&opt=add_music<?php if($album_id){ echo "&album_id=".$album_id; }else{ echo "&singer_id=".$singer_id; } ?>">
                    <img border="0" hspace="2" alt="<?php echo $_LANG["ADD_MUSIC"]; ?>" src="images/actions/add.gif"/><?php echo $_LANG["ADD_MUSIC"]; ?>
                </a>
            </td>
        </tr>
    </table>
</form>
<form name="selform" action="index.php?view=components" method="post">
    <table id="listTable" class="tablesorter" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:0px">
        <thead>
            <tr>
                <th class="lt_header" align="center" width="15">
                    <a class="lt_header_link" title="Инвертировать выделение" href="javascript:" onclick="javascript:invert()">#</a>
                </th>
                <th class="lt_header" width="67"><?php echo $_LANG["DATE"]; ?></th>
                <th class="lt_header" width=""><?php echo $_LANG["NAME"]; ?></th>
                <th class="lt_header" width=""><?php echo $_LANG["SINGER"]; ?></th>
                <th class="lt_header" width=""><?php echo $_LANG["ALBUM"]; ?></th>
                <th class="lt_header" width="55"><?php echo $_LANG["OPTIONS"]; ?></th>
                <th class="lt_header" align="center" width="85"><?php echo $_LANG["DO"]; ?></th>
            </tr>
        </thead>
        <?php if ($total){ ?>
        <tbody>
            <?php foreach($items as $num=>$item){ ?>
            <tr id="<?php echo $item['id']; ?>" class="item_tr">
                <td><input type="checkbox" name="item[]" value="<?php echo $item['id']; ?>" /></td>
                <td style="font-size:9px"><?php echo $item['pubdate']; ?></td>
                <td>
                    <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=edit_music&item_id=<?php echo $item['id']; ?>">
                        <?php echo $item['name']; ?>
                    </a>
                </td>
                <td>
                    <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=view_singer&singer_id=<?php echo $item['singer_id']; ?>">
                        <?php echo $item['singer']; ?>
                    </a>
                </td>
                <td>
                    <a href="index.php?view=components&do=config&id=<?php echo $id; ?>&opt=view_album&album_id=<?php echo $item['album_id']; ?>">
                        <?php echo $item['album']; ?>
                    </a>
                </td>
                <td class="flags">
                    <?php if ($item['published']) { ?>
                    <a id="publink<?php echo $item['id']; ?>" href="javascript:pub(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_item&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=show_item&item_id=<?php echo $item['id']; ?>', 'off', 'on');" title="<?php echo $_LANG["HIDE"]; ?>">
                        <img id="pub<?php echo $item['id']; ?>" border="0" src="images/actions/on.gif"/>
                    </a>
                    <?php } else { ?>
                    <a id="publink<?php echo $item['id']; ?>" href="javascript:pub(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=show_item&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_item&item_id=<?php echo $item['id']; ?>', 'on', 'off');" title="<?php echo $_LANG["SHOW"]; ?>">
                        <img id="pub<?php echo $item['id']; ?>" border="0" src="images/actions/off.gif"/>
                    </a>
                    <?php } ?>
                    <?php if ($item['is_new']) { ?>
                    <a id="isnewlink<?php echo $item['id']; ?>" href="javascript:isnew(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_isnew&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=show_isnew&item_id=<?php echo $item['id']; ?>', 'hide_isnew', 'show_isnew');" title="<?php echo $_LANG["REMOVE_IS_NEW"]; ?>">
                        <img id="isnew<?php echo $item['id']; ?>" border="0" src="/components/music/images/show_isnew.png"/>
                    </a>
                    <?php } else { ?>
                    <a id="isnewlink<?php echo $item['id']; ?>" href="javascript:isnew(<?php echo $item['id']; ?>, 'view=components&do=config&id=<?php echo $id; ?>&opt=show_isnew&item_id=<?php echo $item['id']; ?>', 'view=components&do=config&id=<?php echo $id; ?>&opt=hide_isnew&item_id=<?php echo $item['id']; ?>', 'show_isnew', 'hide_isnew');" title="<?php echo $_LANG["IS_NEW"]; ?>">
                        <img id="isnew<?php echo $item['id']; ?>" border="0" src="/components/music/images/hide_isnew.png"/>
                    </a>
                    <?php } ?>
                </td>
                <td align="right">
                    <div style="padding-right: 3px;">
                        <a title="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" href="<?php echo $item['seolink'];?>" target="_blank">
                            <img border="0" hspace="2" alt="<?php echo $_LANG["VIEW_ON_SITE"]; ?>" src="images/actions/search.gif"/>
                        </a>
                        <a title="<?php echo $_LANG["EDIT"]; ?>" href="?view=components&do=config&id=<?php echo $id; ?>&opt=edit_music&item_id=<?php echo $item['id']; ?>">
                            <img border="0" hspace="2" alt="<?php echo $_LANG["EDIT"]; ?>" src="images/actions/edit.gif"/>
                        </a>
                        <a title="<?php echo $_LANG["DEL_MUSIC"]; ?>" onclick="jsmsg('<?php echo $_LANG["DEL_MUSIC"]; ?> <?php echo htmlspecialchars($item['title']); ?>?', '?view=components&do=config&id=<?php echo $id; ?>&opt=delete_music&item_id=<?php echo $item['id']; ?>')" href="#">
                            <img border="0" hspace="2" alt="<?php echo $_LANG["DEL_MUSIC"]; ?>" src="images/actions/delete.gif"/>
                        </a>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        <?php } else { ?>
        <tbody>
            <td colspan="7" style="padding-left:5px">
                <div style="padding:15px;padding-left:0px">
                    <?php echo $_LANG["SONGS_NOT_FOUND"]; ?>
                </div>
            </td>
        </tbody>
        <?php } ?>
    </table>
    <?php if ($items){ ?>
    <div style="margin-top:4px;padding-top:4px;">
        <table class="" cellpadding="5" border="0" height="40">
            <tr>
                <td width="">
                    <strong style="color:#09C"><?php echo $_LANG["MARKED"]; ?>:</strong>
                </td>
                <td width="" class="sel_pub">
                    <input type="button" name="" value="<?php echo $_LANG["CHANGE"]; ?>" onclick="sendMusicForm(<?php echo $id; ?>, 'edit_music');" />
                    <input type="button" name="" value="<?php echo $_LANG["MOVE"]; ?>" onclick="$('.sel_move').toggle();$('.sel_pub').toggle();" />
                </td>
                <td class="sel_move" style="display:none">
                    <?php if ($singer_id){echo $_LANG["MOVE_TO_SINGER"];}else{echo $_LANG["MOVE_TO_ALBUM"];} ?>
                </td>
                <td class="sel_move" style="display:none">
                <?php if ($singer_id){ ?>
                    <select id="move_singer_id" style="width:250px">
                        <?php foreach ($model->getSingers($inUser->is_admin) as $singer){ ?>
                            <option value="<?php echo $singer['id']; ?>"><?php echo $singer['title']; ?></option>
                        <?php } ?>
                    </select>
                <?php }else{ ?>
                    <select id="move_album_id" style="width:250px">
                        <?php foreach ($model->getAlbums($inUser->is_admin) as $album){ ?>
                        <option value="<?php echo $album['id']; ?>"><?php echo $album['title']; ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
                </td>
                <td class="sel_move" style="display:none">
                    <?php
                    if ($singer_id){
                        echo "<input type=\"button\" name=\"\" value=\"OK\" onclick=\"sendMusicForm(".$id.", 'move_singer_items', $('select#move_singer_id').val());\" /><input type=\"hidden\" id=\"singer_id\" value=\"".$singer_id."\">";
                    }else{
                        echo "<input type=\"button\" name=\"\" value=\"OK\" onclick=\"sendMusicForm(".$id.", 'move_album_items', $('select#move_album_id').val());\" /><input type=\"hidden\" id=\"album_id\" value=\"".$album_id."\">";
                    }
                    ?>
                    <input type="button" name="" value="<?php echo $_LANG["CANSEL"]; ?>" onclick="$('td.sel_move').toggle();$('td.sel_pub').toggle();" />
                </td>
                <td class="sel_pub">
                    <input type="button" name="" value="<?php echo $_LANG["SHOW"]; ?>" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'show_item');" />
                    <input type="button" name="" value="<?php echo $_LANG["HIDE"]; ?>" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'hide_item');" />
                </td>
                <td class="sel_pub">
                    <input type="button" name="" value="Удалить" onclick="sendMusicForm(<?php echo $_REQUEST['id']; ?>, 'delete_music');" />
                </td>
            </tr>
        </table>
    </div>
    <?php } ?>
</form>
<script type="text/javascript">highlightTableRows("listTable","hoverRow","clickedRow");</script>
<?php
    if ($pages>1){
        echo cmsPage::getPagebar($total, $page, $perpage, $base_uri.'&cat_id='.$category_id.'&page=%page%');
    }
?>
<?php

    //
    // ВНИМАНИЕ! Если вы хотите добавить собственное правило, то создайте
    //           файл custom_rewrite.php и объявите в нем функцию
    //           custom_rewrite_rules() по аналогии с текущим файлом!
    //
    // В этом файле определены системные правила для редиректа и подмены адресов
    //
    //      source          : регулярное выражение, для сравнения с текущим URI
    //      target          : URI для перенаправления, при совпадении source
    //      action          : действие при совпадении source
    //
    // Возможные значения для action:
    //
    //      rewrite         : подменить URI перед определением компонента
    //      redirect        : редирект на target с кодом 303 See Other
    //      redirect-301    : редирект на target с кодом 301 Moved Permanently
    //      alias           : заинклудить файл target и остановить скрипт
    //

    function custom_rewrite_rules(){


        //
        // Регистрация / Активация
        //

        $rules[] = array(
                            'source'  => '/^ref\/([0-9]+)$/i',
                            'target'  => '/ref.php?ref={1}',
                            'action'  => 'redirect'
                         );
        return $rules;

    }

?>

<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

    function routes_forms(){

        $routes[] = array(
                            '_uri'  => '/^forms\/process$/i',
                            'do'    => 'view'
                         );

        return $routes;

    }

?>

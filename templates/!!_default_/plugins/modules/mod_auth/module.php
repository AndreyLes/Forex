<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function mod_auth($module_id, $cfg){

    $inUser = cmsUser::getInstance();

    if ($inUser->id){ return false; }

    cmsUser::sessionPut('auth_back_url', cmsCore::getBackURL());

    cmsPage::initTemplate('modules', 'mod_auth')->
            assign('cfg', $cfg)->
            display('mod_auth.tpl');

    return true;

}
?>
<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

	define('PATH', $_SERVER['DOCUMENT_ROOT']);
	include(PATH.'/core/ajax/ajax_core.php');

	cmsCore::loadLanguage('modules/mod_latest');

	$module_id = cmsCore::request('module_id', 'int', '');

	if(!$module_id) { cmsCore::halt(); }

	$cfg = $inCore->loadModuleConfig($module_id);
    // номер страницы передаем через конфиг
    $cfg['page'] = cmsCore::request('page', 'int', 1);

    cmsCore::includeFile('modules/mod_latest/module.php');

    mod_latest($module_id, $cfg);

?>

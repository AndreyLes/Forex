<?php
/******************************************************************************/
//                                                                            //
//                           CMS Vadyus v1.10.3                               //
//                                                                            //
/******************************************************************************/

function mod_search(){

    cmsCore::loadModel('search');
    cmsCore::loadLanguage('components/search');
    $model = cms_model_search::initModel();

    cmsPage::initTemplate('modules', 'mod_search')->
            assign('enable_components', $model->getEnableComponentsWithSupportSearch())->
            display('mod_search.tpl');

    return true;

}
?>



/**
* Styleswitch stylesheet switcher built on jQuery
* Under an Attribution, Share Alike License
* By Kelvin Luck ( http://www.kelvinluck.com/ )
**/

$(document).ready(function() {

		var c = readCookie('style');
       // if (c) switchStylestyle(c);
		
        $('.styleswitch').click(function()
        {
			switchStylestyle(this.getAttribute("rel"));
			return false;
        });
		//зміна виставленого памма
		$('.change-pamms').on('click', function(){
			$('.view-change-pamms').click();
			$('input[name="new-price"]').val($(this).attr('data-price'));
			$('input[name="change-count"]').val($(this).attr('data-count'));
			$('input[name="change-id"]').val($(this).attr('data-id'));
		});
		//видалення памму з продажі
		$('.remove-pamms').on('click', function(){
			$('.remove-change-pamms').click();
			$('#pammName').text($(this).attr('data-name'));
			$('input[name="remove-success"]').val($(this).attr('data-id'));
		});
});

function switchStylestyle(styleName)
{
        $(document).find("link[rel*=style][title]").each(function(i)
        {
                this.disabled = true;
                if (this.getAttribute('title') == styleName) this.disabled = false;
        });
        createCookie('style', styleName, 365);
}

// cookie functions http://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days)
{
        if (days)
        {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name)
{
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++)
        {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
}
function eraseCookie(name)
{
        createCookie(name,"",-1);
}
// /cookie functions

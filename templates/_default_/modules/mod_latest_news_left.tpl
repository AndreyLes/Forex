{if $cfg.is_pag}
	{literal}
	<script type="text/javascript">
		function conPage(page, module_id){
            $('div#module_ajax_'+module_id).css({opacity:0.4, filter:'alpha(opacity=40)'});
			$.post('/modules/mod_latest/ajax/latest.php', {'module_id': module_id, 'page':page}, function(data){
				$('div#module_ajax_'+module_id).html(data);
                $('div#module_ajax_'+module_id).css({opacity:1.0, filter:'alpha(opacity=100)'});
			});

		}
    </script>
	{/literal}
{/if}
{if !$is_ajax}<div id="module_ajax_{$module_id}">{/if}

<table id='newstable' >
<tr><td style='text-align:center;font-weight: bold;
font-size: 18px;'>Новости</td></tr>
{foreach key=aid item=article from=$articles}
	<tr>
      <td>
	    <a  href="{$article.url}" target="_self">{$article.title}</a>
	</td>
    
	</tr>
{/foreach}
<tr>
	<td style='text-align:center'><a class='allnews' href='/novosti'>Показать все новости</a></td>
</tr>
</table>
{if $cfg.is_pag && $pagebar_module}
    <div class="mod_latest_pagebar">{$pagebar_module}</div>
{/if}
{if !$is_ajax}</div>{/if}
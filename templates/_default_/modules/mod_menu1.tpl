
<!--ul id="{$menu}" class="menu2"-->
<!--ul id="menu-kab" class="menu2"-->
<ul id="menu-kab" class="menu2">
	
	<li><a href="/users/{$user}">
		<span>Главная</span>
	</a></li>
	
    {if $cfg.show_home}

	<li {if $menuid==1}class="selected"{/if}>
		<a href="/" {if $menuid==1}class="selected"{/if}><span>{$LANG.PATH_HOME}</span></a>
    </li>
	
    {/if}

    {foreach key=key item=item from=$items}

      {if $item.NSLevel == $last_level}</li>{/if}

        {math equation="x - y" x=$last_level y=$item.NSLevel assign="tail"}

        {section name=foo start=0 loop=$tail step=1}

            </li></ul></li>

        {/section}
        {if $item.NSLevel > 1 && $item.NSLevel > $last_level}<ul>{/if}

            <li {if $menuid==$item.id || ($currentmenu.NSLeft > $item.NSLeft && $currentmenu.NSRight < $item.NSRight)}class="selected"{/if}class='menuli lishka'>

                <a href="{$item.link}" target="{$item.target}" {if $menuid==$item.id}class="selected"{/if} title="{$item.title|escape:'html'}">


                        {if $item.iconurl}<img src="/images/menuicons/{$item.iconurl}" alt="{$item.title|escape:'html'}" />{/if}

                      <span>  {$item.title}</span>
                 
                </a>

        {assign var="last_level" value=$item.NSLevel}

    {/foreach}

    {section name=foo start=0 loop=$last_level step=1}

        </li>

    {/section}

</ul>
</ul>
<div id='undermenu'>
</div>
<div id='undermenur'>
</div>
{literal}
	<script type='text/javascript'>
		var str = window.location.pathname;
		reg=/(ordercard|editprofile|invite|messages-history|invest|obratnaja-svjaz|pamminfo|outpay|pamm|messages-sent|messages-notices)/;
		if(reg.test(str)){
			$('#undermenu').css('width','1224');
			$('#undermenur').css('width','1224');
		}	
	</script>
{/literal}
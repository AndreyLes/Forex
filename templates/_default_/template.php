<?php

    if(!defined('VALID_CMS')) { die('ACCESS DENIED'); }

    /*
     * Доступны объекты $inCore $inUser $inPage($this) $inConf $inDB
     */

    // Получаем количество модулей на нужные позиции
    $mod_count['top']     = $this->countModules('top');
    $mod_count['topmenu'] = $this->countModules('topmenu');
    $mod_count['topmenu-kab'] = $this->countModules('topmenu-kab');
    $mod_count['right-sidebar'] = $this->countModules('right-sidebar');
    $mod_count['left-sidebar'] = $this->countModules('left-sidebar');
    $mod_count['footer-sidebar'] = $this->countModules('footer-sidebar');
    $mod_count['avtorization'] = $this->countModules('avtorization');
    $mod_count['left-sidebar-kab'] = $this->countModules('left-sidebar-kab');

    // Подключаем стили шаблона
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/reset.css');
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/text.css');
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/960.css');
    // Подключаем colorbox (просмотр фото)
   $this->addHeadJS('includes/jquery/colorbox/jquery.colorbox.js');
    $this->addHeadCSS('includes/jquery/colorbox/colorbox.css');
    $this->addHeadJS('includes/jquery/colorbox/init_colorbox.js');
    // LANG фразы для colorbox
    $this->addHeadJsLang(array('CBOX_IMAGE','CBOX_FROM','CBOX_PREVIOUS','CBOX_NEXT','CBOX_CLOSE','CBOX_XHR_ERROR','CBOX_IMG_ERROR', 'CBOX_SLIDESHOWSTOP', 'CBOX_SLIDESHOWSTART'));

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="interkassa-verification" content="06816323de717fba15f23834c8f809e6" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <?php $this->printHead(); ?>
    <?php if($inUser->is_admin){ ?>
        <script src="/admin/js/modconfig.js" type="text/javascript"></script>
		<script src="/templates/<?php echo TEMPLATE; ?>/js/nyromodal.js" type="text/javascript"></script>
        <link href="/templates/<?php echo TEMPLATE; ?>/css/modconfig.css" rel="stylesheet" type="text/css" />
        <link href="/templates/<?php echo TEMPLATE; ?>/css/nyromodal.css" rel="stylesheet" type="text/css" />

    <?php } ?>
	
	<link rel="shortcut icon" href="/templates/<?php echo TEMPLATE; ?>/images/icons/favicon.png" type="image/x-icon"/>
	<link rel="stylesheet" href="/templates/<?php echo TEMPLATE; ?>/css/bootstrap.css">
	<script src="/templates/<?php echo TEMPLATE; ?>/js/bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script>

	<!--  ПЕРЕКЛЮЧЕНИЕ СТИЛЕЙ-->
	<?
	if($inUser->id){
		$color_code = $inDB->get_field('cms_users', 'id="'.$inUser->id.'"', 'color_system');
		$style = array();
		$style[0]['title'] = "dark";
		$style[0]['href'] = '/templates/_default_/css/styles.css';
		$style[1]['title'] = "blue";
		$style[1]['href'] = '/templates/_default_/css/stylesblue.css';
		$style[2]['title'] = 'green';
		$style[2]['href'] = "/templates/_default_/css/stylesgreen.css";
		foreach($style as $key=>$styles){
			if($key != $color_code){?>
				<link href="<? echo $styles['href']; ?>" title="<? echo $styles['title']; ?>" disabled="disabled" media="screen" rel="stylesheet" type="text/css" />
			<?}
			else{ ?> 
				<link href="<? echo $styles['href']; ?>" title="<? echo $styles['title']; ?>" media="screen" rel="stylesheet" type="text/css" />
			<?}
		}
	}
	/*elseif(isset($_COOKIE['style'])){ 
		$style = array();
		$style[0]['title'] = "dark";
		$style[0]['href'] = '/templates/_default_/css/styles.css';
		$style[1]['title'] = "blue";
		$style[1]['href'] = '/templates/_default_/css/stylesblue.css';
		$style[2]['title'] = 'green';
		$style[2]['href'] = "/templates/_default_/css/stylesgreen.css";
		
		foreach($style as $styles){
			if($styles['title'] != $_COOKIE['style']){?>
				<link href="<? echo $styles['href']; ?>" title="<? echo $styles['title']; ?>" disabled="disabled" media="screen" rel="stylesheet" type="text/css" />
			<?}
			else{?> 
				<link href="<? echo $styles['href']; ?>" title="<? echo $styles['title']; ?>" media="screen" rel="stylesheet" type="text/css" />
			<?}
		}
	}*/
	else{ 
		?>
		<link href="/templates/<?php echo TEMPLATE; ?>/css/styles.css" title="dark"  media="screen" rel="stylesheet" type="text/css" />
		<link href="/templates/<?php echo TEMPLATE; ?>/css/stylesblue.css" title="blue"  rel="alternate stylesheet" type="text/css" disabled="disabled"/>
		<link href="/templates/<?php echo TEMPLATE; ?>/css/stylesgreen.css" title="green"  rel="alternate stylesheet" type="text/css" disabled="disabled"/>
		<?
	}
	
	?>
	<script src="/templates/<?php echo TEMPLATE; ?>/js/styleswitch.js" type="text/javascript"></script>
	<script src="/includes/hint/hint.js" type="text/javascript"></script>
	<script type="text/javascript">
	  VK.init({apiId: 4421249, onlyWidgets: true});
	</script>
	
	
</head>
<?php

//var_dump($_GET);
if(isset($_POST['redirect_url']) && isset($_SESSION['user'])){
	cmsCore::redirect($_POST['redirect_url']);
}
elseif(isset($_POST['redirect_url'])){
	$_SESSION['bak_urlka'] = $_POST['redirect_url'];
}
if(isset($_GET['redirect_url']) && isset($_SESSION['user'])){
	cmsCore::redirect($_GET['redirect_url']);
}
elseif(isset($_GET['redirect_url'])){
	$_SESSION['bak_urlka'] = $_GET['redirect_url'];
	cmsCore::redirect('/');
}

//var_dump($_SESSION);
?>
<body>
<style>
	.sess_messages {
		background: rgba(0,0,0,0.5);
		position: fixed;
		width: 100%;
		height: 100%;
		border-radius: 0px;
		top: 0;
		left: 0;
		z-index: 998;
		opacity: 1;
	}
	.sess_messages .message_info, .sess_messages .message_success, .sess_messages .message_error {
		display: block;
		position: fixed;
		border: 5px solid #ccc;
		background-color: #fff;
		width: 557px;
		max-width: 560px;
		text-align: left;
		padding: 15px;
		padding-left: 25px;
		z-index: 999;
		top: 25%;
		left: 50%;
		margin-left: -278px;
	}
</style>
<!-- Start SiteHeart code -->

<!-- End SiteHeart code -->

<script>
	$(document).ready(function(){
		$('.menu2 > li:first-child').find('ul').addClass('show');
		$('.menu2 > li').mouseover(function () {
		if($(this).find('ul').hasClass('show'))
			{}
		else
			{
				$('.menu2 > li').find('ul').removeClass('show');
				$(this).find('ul').addClass('show');
			}
		});
		var hash = document.location.hash;
		if(hash == '#modal'){
			$('#mainmenu .menuli a[href="#modal"]').click();
			document.location.hash = "";
		}
		<?php if(isset($_POST['redirect_url'])){?>
			$('#authform #login').after('<input type="hidden" name="url_redirect" value="<?php echo $_POST['redirect_url'] ?>">');
		<?php } ?>
	});

</script>
<?php if ($_SERVER["REQUEST_URI"] == "/" OR $_SERVER["REQUEST_URI"] == "/index.php") { ?>
<?

?>
<div id='generalpage'>
	<div id='wrapglav'>

		<div id="modal" class="modal hide fade" style='width: 557px;'>
			<div class="modal-header">
				<button tabindex="6" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<?php if($mod_count['avtorization']) { ?>
					<?php $this->printModules('avtorization'); ?>
				<?php } ?>
			</div>
		</div>

		<div id='grad'>
			<div id='genwrap'>
				<header>
					<span>
						iMoneybox
					</span>
					<a href='/' id='lang'>
						<span></span>
						<span>RU</span>
					</a>
				</header>
				<div id='glavbody'>
					<p>Уважаемые посетители сайта!</p>
					<p>Вы находитесь на главной странице компании imoneybox</p>

					<img style='height: 400px;' src='/images/house.png'/>
				</div >
				<div class='glavmen'><footer>
					<ul id="mainmenu" class="">
						 <li class='menuli'>
							<a href="/info/o-nas.html" target="_self" title="Инвестиционный сервис">
								<span>Инвестиционный сервис</span>
								<span>Вход</span>
							</a>
						</li>
						<li class='menuli'>
							<a href="/novosti" target="_self" title="Депозитный сервис">
								<span>Депозитный сервис</span>
								<span>Вход</span>
							</a>
						</li>
						<?// print_r();?>
						<?php if ($inUser->id) { ?>

						<li class='menuli'>
							<a href="/users/<?echo $inUser->login;?>" target="_self" title="Инвестиционный кабинет">
								<span>Инвестиционный кабинет</span>
								<span>Вход</span>
							</a>
						</li>
						<?
						} else { ?>
						<li class='menuli'>
							<a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет">
								<span>Инвестиционный кабинет</span>
								<span>Вход</span>
							</a>
						</li>

						<?php } ?>

						<li class='menuli'>
							<a href="/novosti" target="_self" title="Депозитный кабинет">
								<span>Депозитный кабинет</span>
								<span>Вход</span>
							</a>
						</li>
					</ul>
				</footer></div>
			 <?//php $this->printModules('topmenu'); ?>
			</div>
		</div>
	</div>
</div>
<? } else{ ?>

<?php if ($inConf->siteoff && $inUser->is_admin) { ?>
<div style="margin:4px; padding:5px; border:solid 1px red; background:#FFF; position: fixed;opacity: 0.8; z-index:999"><?php echo $_LANG['SITE_IS_DISABLE']; ?></div>
<?php } ?>

    <div id="wrapper">
        <div id="header">
            <div class="container_12">
                <div class="grid_2">
                    <div id="sitename"><a href="/novosti">iMoneyBox</a></div>
                </div>
                <div class="grid_10">
					<span id='header-span'>Стабильность, надежность, открытость</span>
                    <?php if ($inConf->is_change_lang){

                        $langs = cmsCore::getDirsList('/languages'); ?>

						<? $lang['ru'] = 'Русский'; $lang['eng'] = 'Английский'; ?>

                        <div onclick="$('#langs-select').toggle().toggleClass('active_lang');$(this).toggleClass('active_lang'); return false;" title="<?php echo $_LANG['TEMPLATE_INTERFACE_LANG']; ?>" id="langs" style="background-image:  url(/templates/<?php echo TEMPLATE; ?>/images/icons/langs/<?php echo $inConf->lang; ?>.png); background-repeat: no-repeat;">
                            <span id='strelka'><img src='/images/strelka.png'/></span><?php echo $lang[$inConf->lang]; ?>
                            <ul id="langs-select">
                                <?php foreach ($langs as $lng) { ?>
                                <li onclick="setLang('<?php echo $lng; ?>'); return false;" style="background-image:  url(/templates/<?php echo TEMPLATE; ?>/images/icons/langs/<?php echo $lng; ?>.png); background-repeat: no-repeat;"><?php echo $lang[$lng]; ?></li>
                                <?php } ?>
                            </ul>
                        </div>

                    <?php } ?>

					<?  if(!$inUser->id){
                   $this->printModules('header');
					}else{}?>
				</div>
            </div>
        </div>

        <div id="page">
		<?/* if (stripos($_SERVER['REQUEST_URI'], "users") != 1 &&  stripos($_SERVER['REQUEST_URI'], "obratnaja-svjaz") != 1)
			{ ?>
            <?php $this->printModules('topmenu'); */?>

            <?//php } else { ?>

			   <?php $this->printModules('topmenu-kab'); ?>

			<?// } ?>
            <?php if ($mod_count['top']) { ?>
            <div class="clear"></div>

            <div id="topwide" class="container_12">
                <div class="grid_12" id="topmod"><?php $this->printModules('top'); ?></div>
            </div>
            <?php }	?>

            <div id="mainbody" class="container_12" <? if ( stripos($_SERVER['REQUEST_URI'], $inUser->login)){?>style=''  <?}?> >
			<? if (stripos($_SERVER['REQUEST_URI'], "registration") != 1  ){
			if (stripos($_SERVER['REQUEST_URI'], "users") != 1 ) {?>
			<!--  УСЛОВИЯ ДЛЯ ЛЕВОГО БЛОКА     -->
				<div id='leftsidebar' style='min-height: 800px;' >
					<?php if($inUser->id !=0 && stripos($_SERVER['REQUEST_URI'], "registration") != 1){
						$this->printModules('usermenu');
					} ?> <?php $this->printModules('left-sidebar');?>
				</div>
			<?} else{?>
				<div id='leftsidebar' >
						<?php  $this->printModules('usermenu'); ?>
						
						<? if ( stripos($_SERVER['REQUEST_URI'], $inUser->login) == true){?>
							<?php $this->printModule('106');?>
						<?}?>
						<? if (stripos($_SERVER['REQUEST_URI'], $inUser->login) != true) { $this->printModules('news+online'); }?>
				</div>
			<?} }?>
			<? if ($_SERVER['REQUEST_URI'] == "/registration/change_login"){ ?>
				<div id='leftsidebar' >
						<?php  $this->printModules('usermenu'); ?>
						
						<? if ( stripos($_SERVER['REQUEST_URI'], $inUser->login) == true){?>
							<?php $this->printModule('106');?>
						<?}?>
				</div>
			<? }?>

			   <!--  УСЛОВИЯ ДЛЯ ПРАВОГО БЛОКА     -->
			    <?/* if (stripos($_SERVER['REQUEST_URI'], "obratnaja-svjaz") != 1 ){?>
				<div id='rightsidebar' <? if (stripos($_SERVER['REQUEST_URI'], "users") != 1 ){ ?>style='top: -142px;'<? } ?>>
				<? if (stripos($_SERVER['REQUEST_URI'], "users/")   != 1 )
					{
						$this->printModules('right-sidebar');
						$this->printModules('news+online');
					}
					else
					{
						if (   stripos($_SERVER['REQUEST_URI'], 'users/'.$inUser->login.'') != 1 )
						{}
						else
						{
				   			$this->printModules('news+online');
						}
					}?>
				</div>
				<?}*/?>

				<div id="main"  <?php if (stripos($_SERVER['REQUEST_URI'], 'users/'.$inUser->login.'') != 1 && stripos($_SERVER['REQUEST_URI'], 'stati/') != 1 && stripos($_SERVER['REQUEST_URI'], 'novosti') != 1 && stripos($_SERVER['REQUEST_URI'], 'info') != 1 && stripos($_SERVER['REQUEST_URI'], 'partners') != 1 && stripos($_SERVER['REQUEST_URI'], 'pages') != 1 && stripos($_SERVER['REQUEST_URI'], 'faq') != 1 && stripos($_SERVER['REQUEST_URI'], 'forum') != 1 && stripos($_SERVER['REQUEST_URI'], 'login') != 1  && stripos($_SERVER['REQUEST_URI'], 'catalog') != 1 && stripos($_SERVER['REQUEST_URI'], 'passremind') != 1 && stripos($_SERVER['REQUEST_URI'], 'registration/change_login') != 1){
			?> style='width: 971px;'<?php } ?>
			<?php if ($mod_count['left-sidebar']) { ?> style='width:715px' <?}?> class="<?php if ($mod_count['sidebar']) { ?>grid_8<?php } else { ?>grid_12<?php } ?>">
                    <?php $this->printModules('maintop'); ?>

                    <?php $messages = cmsCore::getSessionMessages(); ?>
                    <?php if ($messages) { ?>

                    <div class="sess_messages">
                        <?php foreach($messages as $message){ ?>
                            <?php echo $message; ?>
                        <?php } ?>
                    </div>
	<script>
		text_info = '';
		$('.sess_messages div').each(function(){
			text_info += " - "+$(this).text()+"<br>";
		});
		$('.sess_messages').find('div').html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><span style="position: relative; top: 45%;display: block;width: 95%;">'+text_info+'</span>');
	</script>
                    <?php } ?>

                    <?php if($this->page_body){ ?>
                        <div  class="component" <? if (stripos($_SERVER['REQUEST_URI'], 'users/'.$inUser->login.'') != 1 ){?> style='padding-right:0;'<?}?>
						<? if (stripos($_SERVER['REQUEST_URI'], 'editprofile') != 0 ){?>id="editcomp"<?}?>
						>
                            <?php $this->printBody(); ?>
                        </div>
                    <?php } ?>
                    <?php $this->printModules('mainbottom'); ?>
                </div>

				<!--  УСЛОВИЯ ДЛЯ ПРАВОГО БЛОКА     -->
			    <? if (stripos($_SERVER['REQUEST_URI'], "obratnaja-svjaz") != 1 ){ ?>
				<div id='rightsidebar'>
				<?/*<div id='rightsidebar' <? if (stripos($_SERVER['REQUEST_URI'], "users") != 1 ){ ?>style='top: -139px;'<? } ?>>*/?>
				<? if (stripos($_SERVER['REQUEST_URI'], "users/")   != 1 )
					{
						$this->printModules('right-sidebar');
						$this->printModules('news+online');
					}
					else
					{
						if (   stripos($_SERVER['REQUEST_URI'], 'users/'.$inUser->login.'') != 1 )
						{}
						else
						{
				   			$this->printModules('news+online');
						}
					}?>
				</div>
				<?}?>

                <?php if ($mod_count['sidebar']) { ?>
                    <div class="grid_4" id="sidebar"></div>
                <?php } ?>
            </div>

        </div>
	<div id="footer">
        <div class="foot">

       <span>
	   Предупреждение о риске: Вы должны знать, что при маржинальной торговле существует вероятность того, что Вы можете потерять все или некоторую часть Ваших начальных инвестиций и поэтому Вы не должны вкладывать деньги, которые Вы не можете позволить себе потерять или утрата которых окажет значительное влияние на Ваше финансовое благосостояние. Интернет торговля включает в себя дополнительные риски. Пожалуйста, внимательно ознакомьтесь с <a class='foota' style='color:#0000FF' href="#modal" role="button" data-toggle="modal">«Уведомлением о рисках»</a> перед началом совершения торговли или инвестирования.
	   </span>
	   <?php $this->printModules('footer-sidebar'); ?>
        </div>
    </div>
    </div>

	<div id="modal" class="modal hide fade" style='width: 557px;'>
		<div class="modal-body" style='height: 400px; border: 5px solid #ccc;'>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<span style='margin-top: 10px; display: inline-block;'>
				<p>Уведомление о рисках</p>

				Компания iMoneyBox., именуемая в дальнейшем Компания, с одной стороны и любое
				физическое и юридическое лицо, именуемое в дальнейшем Клиент, с другой стороны,
				заключили настоящий контракт о нижеследующем.

				Контракт устанавливает общие условия и порядок, в соответствии с которыми Клиент
				получает от Компании информацию в виде консультаций на рынке iMoneyBox, а также
				индексов, металлов и других ресурсов посредством доступа к финансово-
				информационной системе iMoneyBox.

				Компания также предоставляет Клиенту услуги по доступу к потоку экономических
				новостей в реальном времени посредством ФИС - доступ к информационным лентам
				(экономическим, финансовым), аналитическим прогнозам и материалами, а так же
				проведения программ обучения и турниров.
				Компания имеет право не выполнять самостоятельно свои обязательства по данному
				Контракту, а передать обязательства к исполнению другим родственным Компаниям или
				юридическим лицам, имеющим доступ к ФИС.

				Сделки заключает Клиент от своего имени и на свой риск, в сети Интернет через
				торговый терминал Компании (далее по тексту "Терминал"). Время работы Компании –
				круглосуточно с 02 часа 00 минут  понедельника до 02 часа 00 минут  субботы – зимнее
				время, с 01 часа 00 минут  понедельника до 01 часа 00 минут  субботы – летнее время.

				Понятия и термины, используемые при предоставлении информации, а также программ
				обучения и турниров, являются общепринятыми на международном валютном рынке
				 iMoneyBox.
			</span>
		</div>
	</div>


    <script type="text/javascript">
        $(document).ready(function(){
            $('#topmenu .menu li').hover(
                function() {
                    $(this).find('ul:first').show();
                    $(this).find('a:first').addClass("hover");
                },
                function() {
                    $(this).find('ul:first').hide();
                    $(this).find('a:first').removeClass("hover");
                }
            );
        });
    </script>
    <?php if($inConf->debug && $inUser->is_admin){
            $time = $inCore->getGenTime(); ?>
        <div class="debug_info">
            <div class="debug_time">
                <?php echo $_LANG['DEBUG_TIME_GEN_PAGE'].' '.number_format($time, 4).' '.$_LANG['DEBUG_SEC']; ?>
            </div>
            <div class="debug_memory">
                <?php echo $_LANG['DEBUG_MEMORY'].' '.round(@memory_get_usage()/1024/1024, 2).' '.$_LANG['SIZE_MB']; ?>
            </div>
            <div class="debug_query_count">
                <a href="#debug_query_dump" class="ajaxlink" onclick="$('#debug_query_dump').toggle();"><?php echo $_LANG['DEBUG_QUERY_DB'].' '.$inDB->q_count; ?></a>
            </div>
            <div id="debug_query_dump">
                <?php echo $inDB->q_dump; ?>
            </div>
        </div>
    <?php } ?>
    <?php } ?>

<script>
	$(document).ready(function(){
		$('body').on('click', ".sess_messages", function(){
			$('.sess_messages div').animate({'top': '-300px'}, 1200);
			$('.sess_messages').animate({'opacity': '0'}, 1800).animate({'top': '100%'}, 1).animate({'left': '100%'}, 1);
		});
		$('body').on('click', ".sess_messages .close", function(){
			$('.sess_messages div').animate({'top': '-300px'}, 1200);
			$('.sess_messages').animate({'opacity': '0'}, 1800).animate({'top': '100%'}, 1).animate({'left': '100%'}, 1);
		});
		$('img').each(function(){
			if($(this).attr('src') == ""){
				$(this).attr('src', '/images/icons/no_image_available.png');
			}
		});
	});
</script>
<div id="background_modal"></div>
</body>
</html>
{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#save_paym_sys'){
				$("#modal_success_btn").click();
				$("#text_span").text("Номер счета сохранен.");
				document.location.hash = "";
			}
			if(hash == '#critical_error'){
				$("#modal_success_btn").click();
				$("#text_span").text("Вы пытаетесь обмануть систему перевода средств. При повторном обмане Ваш аккаунт будет заблокирован.");
				document.location.hash = "";
			}
			if(hash == '#success'){
				$("#modal_success_btn").click();
				$("#text_span").text("Cредства успешно переведены.");
				document.location.hash = "";
			}
			
			
			
				
		});
	function validation(){
		$('.design_valid p').css('border', '1px solid #fff');
		if($('#first').prop("checked") == false){ 
			$('#first_label').parent().css('border', '1px solid red');
			return false;
		}
		else{
			return true;
		}
	}	
		
		
	</script>
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:20px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>
<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Пополнение Гарантийного Фонда
		<div class="td_buttonu" style="float: right;margin-top: -14px;"><a href="/users/{$user_id}/payment_system.html">Создать заявку</a></div>
	</h4>
	<div id='underline'>
		<span>
		
		</span>
	</div>
</div>
<div class="kab-blockk">
	<span style="display: block; margin-top: 10px;">Размер Вашего ГФ на данный момент составляет: <span>{$garant_money} $</span></span>
	<div class="payment_systems">
	<header style="background-repeat: no-repeat; background-size: 100% 100%;">
		<span>Гарантийный Фонд</span>
	</header>
	<table class="pamm-info ">
	<thead>
		<tr height="45">
			<td>ПС</td>
			<td>% на снятие</td>
			<td>К-во доступных средств</td>
			<td>К-во перводимых средств</td>
			<td>Действие</td>
		</tr>
	</thead>
	<tbody>
	
	{foreach from=$inf_money_s key=tid item=result}
		<tr>
			<td><div><img src='{$result.img}'></div><span>{$result.name_syst}</span></td>
			<td>{$result.nalog}</td>
			<td>{$result.money}</td>
			<td><input class="aaaaaa" data-kurs="{$result.nalog}" type="text" name="k_vo_money"></td>
			<td class="td_buttonu"><a data-id="{$result.id}" data-money="{$result.money}" class="button_gf">Перевести</a></td>
		</tr>
	{/foreach}
	</tbody>
	</table>
	</div>
	
</div>
<form method="post" action="" id="otpravka">
	<input type="hidden" name="opt" value="perevod_gf">
	<input type="hidden" name="money" value="">
	<input type="hidden" name="id_sys" value="">
</form>
{literal}
<script>
	$(document).ready(function(){
		$('.pamm-info').on('keyup keydown', '.aaaaaa', function(){
		
		
		if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, '');
		
		});
		/*$('.pamm-info').on('keydown', '.aaaaaa', function(){
			if(this.value.toString().search(/[^0-9.,]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.,]/g, '');
			Value_1 = $(this).val();
			Value_1 = Value_1.replace(",", '.');
			kurs_num = $(this).attr('data-kurs');
			kurs_num = 1 - kurs_num*0.01
			if(Value_1 != ""){
				result = Value_1*kurs_num;
				//alert(result);
				$(this).parent().parent().find('.bbbbbb').val(result);
				//$(this).parent().parent().find('.bbbbbb').val() = Math.round(parseFloat($(this).parent().parent().find('.bbbbbb').val())*100)/100;
			}
			else{
				$(this).parent().parent().find('.bbbbbb').val('');
			}
		});*/
		$('.button_gf').click(function(){
			var money = parseFloat($(this).attr('data-money'));
			money2 = $(this).parent().parent().find('.aaaaaa').val();
			if(money2 != "" || money2 != 0){
				money2 = parseFloat($(this).parent().parent().find('.aaaaaa').val());
			}
			if(money2 == "" || money2 == 0){
				$("#modal_success_btn").click();
				$("#text_span").text("Введите к-во переводимых средств.");
			}
			else if(money2 > money){
				$("#modal_success_btn").click();
				$("#text_span").html("Вы не можете перевести столько средств.<br>Максималькое к-во: "+money+".");
			}
			else{
				$('#otpravka').find('input[name="money"]').val(money2);
				$('#otpravka').find('input[name="id_sys"]').val($(this).attr('data-id'));
				$('#otpravka').submit();
			}
		});
	});
</script>
{/literal}
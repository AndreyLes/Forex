<div>
	<div class="add-banners">
		<form enctype="multipart/form-data" method="post">
		<table>
			<tbody>
				<tr>
					<td>
						<input type="file" name="file" required />
						<input type="text" name="link" required />
					</td>
					<td>
						<input type="submit" />
					</td>					
				</tr>
			</tbody>
		</table>
		</form>
	</div>
	<div class="user-banners">
	{foreach from=$getImage item=item}
		<div>
			<img src="/{$item.image}" />
			<textarea>
				{$item.link}
			</textarea>
			<a class="change-banner" data-id="{$item.id}">Изменить</a>
		</div>
	{/foreach}
	</div>
</div>
<div id="modal_change" class="modal hide fade" style='width: 680px;left:45%;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<form enctype="multipart/form-data" method="post">
			<input type="file" name="file2" required />
			<input type="text" name="link2" required />
			<input type="hidden" name="bannerId" />
			<input type="submit" />
		</form>
	</div>
</div>
<a href="#modal_change" role="button" data-toggle="modal" id='modal_change_btn'></a>
{literal}
	<script>
		$('.change-banner').on('click', function(){
			$('input[name="bannerId"]').val($(this).attr('data-id'));
			$('#modal_change_btn').click();
		});
	</script>
{/literal}
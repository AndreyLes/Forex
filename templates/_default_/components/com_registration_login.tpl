{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#ACTIVATION_COMPLETE'){
				$("#modal_success_btn").click();
				$("#text_span").html("<p>Регистрация завершена.</p> <p>Теперь вы можете авторизоваться на сайте.</p><p> Используйте для этого свой логин и пароль.</p>");
				document.location.hash = "";
			}
		});
	</script>
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;padding-top: 10px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>

<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div style='width:716px;'>

<h1 class="con_heading">{$LANG.SITE_LOGIN}</h1>

{if $is_sess_back}
    <p class="lf_notice">{$LANG.PAGE_ACCESS_NOTICE}</p>
{/if}

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="login_form">
    <tr>
        <td valign="top" width="50%">
            <form method="post" action="">
                <div class="lf_title">{$LANG.LOGIN} {$LANG.OR} {$LANG.EMAIL}</div>
                <div class="lf_field">
                    <input type="text" name="login" id="login_field" tabindex="1"/> <a href="/registration" class="lf_link">{$LANG.REGISTRATION}</a>
                </div>
				
                <div class="lf_title">{$LANG.PASS}</div>
                <div class="lf_field">
                    <input type="password" name="pass" id="pass_field" tabindex="2"/><br> <a href="/passremind.html" class="lf_link">{$LANG.FORGOT_PASS}</a>
                </div>
                {if $anti_brute_force}
                    <div class="lf_title">{$LANG.SECUR_SPAM}</div>
                    <div class="lf_field">
                        {captcha}
                    </div>
                {/if}
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="20"><input style="position: inherit;" type="checkbox" name="remember" value="1" checked="checked" id="remember" tabindex="3" /></td>
                        <td>
                            <label for="remember">{$LANG.REMEMBER_ME}</label>
                        </td>
                    </tr>
		                </table>
                <p class="lf_submit">
                    <input id="save_btn" type="submit" name="login_btn" value="{$LANG.SITE_LOGIN_SUBMIT}" tabindex="4" />
                </p>
            </form>
        </td>
        <td valign="top">

            {php}cmsCore::callEvent('LOGINZA_BUTTON', array());{/php}

        </td>
    </tr>
</table>
</div>
<script type="text/javascript">
    {literal}
    $(document).ready(function(){
        $('.login_form #login_field').focus();
    });
    {/literal}
</script>
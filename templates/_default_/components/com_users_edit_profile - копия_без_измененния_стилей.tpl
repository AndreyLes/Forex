{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_css file='includes/jquery/tabs/tabs.css'}
{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}
{add_js file='includes/jquery/datepicker/jquery-ui.js'}


{literal}
	<script type="text/javascript">
		$(function(){$(".uitabs").tabs();});
		$(window).load(function() {
			$(function() {
				$( "#date_priem" ).datepicker({dateFormat: "yy-mm-dd"});
				$( "#date_card" ).datepicker({dateFormat:'yy/mm'});
			});
		});	
	</script>
	
{/literal}

<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Заказ успешно принят<u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

	<div id='usr' style='padding-bottom: 6px;'>
 <div class="head_edit" id="nickname">
        {$usr.nickname} {if $usr.banned}<span style="color:red; font-size:12px;">{$LANG.USER_IN_BANLIST}</span>{/if}
    </div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
                            <img alt="{$usr.nickname|escape:'html'}" class="usr_img" src="{$usr.avatar}" />
                        </div>

						<div id="usermenu" style="">
						<div class="usr_profile_menu">
						<table cellpadding="0" cellspacing="6" >
							 <tr>
								<td><img src="/templates/{template}/images/icons/profile/avatar.png"  border="0"/></td>
								<td><a href="/users/{$usr.id}/avatar.html" title="{$LANG.SET_AVATAR}">{$LANG.SET_AVATAR}</a></td>
							</tr>
					   </table></div>
                    </td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">
						<div class="field">
							<div class="title">{$LANG.LAST_VISIT}:</div>
							<div class="value">{$usr.flogdate}</div>
							<div>, IP:{$usr.last_ip}</div>
						</div>
						<div>
							
						</div>
						 {if $usr.city}
						<div class="field">
							<div class="title">{$LANG.CITY}:</div>
                            <div class="value"><a href="/users/city/{$usr.cityurl|escape:'html'}">{$usr.city}</a></div>
						</div>
                        {/if}
						<div class="field">
							<div class="title">{$LANG.DATE_BIRTHDAY}:</div>
							<div class="value">
								{$usr.birthdate}
                            </div>
						</div>
                        {if $usr.inv_login}
                            <div class="field">
                                <div class="title">{$LANG.INVITED_BY}:</div>
                                <div class="value">
                                    <a href="{profile_url login=$usr.inv_login}">{$usr.inv_nickname}</a>
                                </div>
                            </div>
                        {/if}


						{if $usr.showbirth && $usr.fbirthdate}
						<div class="field">
							<div class="title">{$LANG.BIRTH}:</div>
							<div class="value">{$usr.fbirthdate}</div>
						</div>
						{/if}

								{add_js file='includes/jquery/jquery.nospam.js'}
							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="{$usr.email|NoSpam}" class="email">{$usr.email}</a></div>
							</div>
							{literal}
								<script>
										$('.email').nospam({ replaceText: true });
								</script>
							{/literal}
						<div class="field">
							<div class="title">Тел.:</div>
							<div class="value">
								{$usr.mobtelephone}
                            </div>
						</div>
				

						{if $cfg.privforms && $usr.form_fields}
							{foreach key=tid item=field from=$usr.form_fields}
                                <div class="field">
                                    <div class="title">{$field.title}:</div>
                                    <div class="value">{if $field.field}{$field.field}{else}<em>{$LANG.NOT_SET}</em>{/if}</div>
                                </div>
                            {/foreach}
						{/if}

					</div>

				</div>

                {foreach key=id item=plugin from=$plugins}
                    <div id="upr_{$plugin.name}">{$plugin.html}</div>
                {/foreach}

			</div>
	</td>
  </tr>
</table>

</div>



<div id='usr'>
<div class="head_edit">{$LANG.CONFIG_PROFILE}</div>
<div id="profiletabs" class="uitabs">
    <ul id="tabs">
        <li><a href="#about"><span>{$LANG.ABOUT_ME}</span></a></li>
		<li><a href="#avatarka"><span>{$LANG.AVATAR}</span></a></li>
		<li><a href="#documents"><span>{$LANG.DOCUMENTS}</span></a></li>
		<li><a href="#soc_cety"><span>{$LANG.SOC_SET}</span></a></li>
        <li rel="hid"><a href="#change_password"><span>{$LANG.CHANGING_PASS}</span></a></li>
		<li><a href="#payment_inf"><span>{$LANG.PAYMENT_INFORMATION}</span></a></li>
		<li><a href="#change_color"><span>{$LANG.CHANGE_COLOR}</span></a></li>
    </ul>
	<form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="opt" value="save" />
        <div id="about">
				
		<h4 style="font-size: 20px;    font-weight: 100; padding-left: 8px;">
		{$LANG.MY_PROFILE}
		</h4>
		<div class="all_inf">
			<span>
			Всегда держите свои данные актуальными. Неправильные или неполные данные могут неправильно повлиять на обработку ваших заявок на получение или выдачу займов.
			</span>
		</div>
		
		
		
		
		
		<!--div id='underline'>
			<span>
			
			</span>
		</div-->
        <div class="title_table">
			<div class="image_title_table"></div>
			<div class="text_title_table">
				<h4>
					Персональная информация
				</h4>
			</div>
		</div>
		<!--div id='underline'>
			<span>
			
			</span>
		</div-->
		<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
                <!-- id юзера -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.NUM_USER}: <br />
                    </td>
                    <td valign="top"><input name="user_id" type="text" class="text-input" id="user_id" style="width:300px" value="{$usr.id|escape:'html'}"/></td>
                </tr>
				<!-- никнейм -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.NIK_NAME}: <br />
                        <span class="usr_edithint">{$LANG.NIK_NAME_TEXT}</span>
                    </td>
                    <td valign="top"><input name="nick" type="text" class="text-input" id="nick" style="width:300px" value="{$usr.login|escape:'html'}"/></td>
                </tr>
				<!-- отображать никнейм -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.SEE_NIK_NAME}: <br />
					</td>
                    <td valign="top"><input id="see_nik_name" value="1" {if $usr.visible_login == '1'} checked {/if} type="checkbox" name="see_nik_name"></td>
                </tr>
				<!-- Имя -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_NAME}: </strong><br />
                    </td>
                    <td valign="top"><input name="nickname" type="text"  class="text-input" id="nickname" style="width:300px" value="{$usr.nickname|escape:'html'}"/></td>
                </tr>
				<!-- Фамилия -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_FIRST_NAME}: </strong><br />
                    </td>
                    <td valign="top"><input name="first_name" type="text" class="text-input" id="first_name" style="width:300px" value="{$usr.first_name}"/></td>
                </tr>
				<!-- Отчество -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_FIRST_NAME}: </strong><br />
                    </td>
                    <td valign="top"><input name="old_name" type="text" class="text-input" id="old_name" style="width:300px" value="{$usr.old_name}"/></td>
                </tr>
				<!-- пол -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_GENDER}: </strong><br />
					</td>
                    <td valign="top">
					<input id="your_gender"  type="radio" {if $usr.gender_user != '2'} checked {/if} value="1" name="your_gender"><label>Мужской</label>
					<input id="your_gender"  type="radio" {if $usr.gender_user == '2'} checked {/if} value="2" name="your_gender"><label>Женский</label>
					</td>
				</tr>
				<!-- скайп -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_SKYPE}: </strong><br />
					</td>
                    <td valign="top"><input name="your_skype" type="text" class="text-input" id="your_skype" style="width:300px" value="{$usr.skype}"/></td>
                </tr>
				<!-- телефон -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_PHONE}: </strong><br />
					</td>
                    <td valign="top"><input name="your_phone" type="text" class="text-input" id="your_phone" style="width:300px" value="{$usr.mobtelephone}"/></td>
                </tr>
				<!-- ел.почта -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.YOUR_MAIL}:</strong><br />
                    </td>
                    <td valign="top">
                        <input name="email" type="text" class="text-input" id="email" style="width:300px" value="{$usr.email}"/>
                    </td>
                </tr>
				<!-- страна -->
                <tr class='info'>
                    <td valign="top">
                        <strong>{$LANG.COUNTRY}:</strong><br />
                    </td>
                    <td valign="top">
                        <input name="your_country" type="text" class="text-input" id="your_country" style="width:300px" value="{$usr2.country|escape:'html'}"/>
					</td>
                </tr>
				<!-- дата рождения -->
                <!--tr class='info select_td'>
                    <td valign="top"><strong>{$LANG.BIRTH}:</strong> </td>
                    <td valign="top">
                        {dateform seldate=$usr.birthdate}
                    </td>
                </tr-->
            </table>
			<div class="title_table">
			<div class="image_title_table"></div>
			<div class="text_title_table">
				<h4>
					Адрес
				</h4>
			</div>
		</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!-- Индекс -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.INDEX2}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_index" type="text" class="text-input" id="your_index" style="width:300px" value="{$usr2.index_user}"/></td>
                </tr>
				<!-- Город -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.CITY}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_city" type="text" class="text-input" id="your_city" style="width:300px" value="{$usr2.city}"/></td>
                </tr>
				<!-- Улица -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.STREET}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_street" type="text" class="text-input" id="your_street" style="width:300px" value="{$usr2.street}"/></td>
                </tr>
				<!-- дом -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.HOME}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_home" type="text" class="text-input" id="your_home" style="width:300px" value="{$usr2.home}"/></td>
                </tr>
				<!-- квартира -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.FLAT}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_flat" type="text" class="text-input" id="your_flat" style="width:300px" value="{$usr2.flat}"/></td>
                </tr>
			</table>
			
        </div>
        
		
		
		<div id="avatarka"></div>
		<!-- документация -->
		<div id="documents">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!--  -->
				<tr class='info'>
                    <td width="300" valign="top"></td>
                    <td valign="top"><strong>{$LANG.TEXT_DOCUMENTATION}</strong></td>
                </tr>
				<!-- серия -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.PASSPORT_SERIES}</strong><br />
                        </td>
                    <td valign="top"><input name="passport_series" type="text" class="text-input" id="passport_series" style="width:300px" value="{$usr2.passport_series}"/></td>
                </tr>
				<!-- номер -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.PASSPORT_NUMBER}</strong><br />
                        </td>
                    <td valign="top"><input name="passport_number" type="text" class="text-input" id="passport_number" style="width:300px" value="{$usr2.passport_number}"/></td>
                </tr>
				<!-- дата видачи -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.PASSPORT_DATE}</strong><br />
                        </td>
                    <td valign="top"><input name="passport_date" type="text" class="text-input" id="date_priem" style="width:300px" value="{$usr2.passport_date}"/></td>
                </tr>
				<!-- код подразделения -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.CODE_REGION}</strong><br />
                        </td>
                    <td valign="top"><input name="code_region" type="text" class="text-input" id="code_region" style="width:300px" value="{$usr2.code_region}"/></td>
                </tr>
				<!-- кем выдан -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.ISSUED_BY}</strong><br />
                        </td>
                    <td valign="top"><input name="issued_by" type="text" class="text-input" id="issued_by" style="width:300px" value="{$usr2.issued_by}"/></td>
                </tr>
			</table>
		</div>
		<!-- подключенніе соц сети -->
		<div id="soc_cety" class="soc_cety">
			<ul style="    list-style: none; display: table;    width: 100%; padding: 5px;" id="table_soc_cet">
				<li id="main_li">
					<div style="float:left;" class="img_table"></div>
					<div style="text-align:center;" class="text_table"><strong>{$LANG.SOC_SET_IN}</strong></div>
				</li>
				<li>
					<div class="my_soc_cet" style="text-align:center; display: inline-block;">
						<div data-net="od" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left; {if $usr2.odnokl == '1'}display:block;{/if} "></div>
						<div data-net="vk" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -144px -8px; width:50px; height:50px; float:left; {if $usr2.vk == '1'}display:block;{/if} "></div>
						<div data-net="tv" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -202px -8px; width:50px; height:50px; float:left; {if $usr2.tviter == '1'}display:block;{/if} "></div>
						<div data-net="g_plus" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left; {if $usr2.g_plus == '1'}display:block;{/if} "></div>
						<div data-net="fb" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left; {if $usr2.fasebook == '1'}display:block;{/if} "></div>
						<div data-net="mail" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -85px -124px; width:50px; height:50px; float:left; {if $usr2.mail == '1'}display:block;{/if} "></div>
					</div>
				</li>
				<li><div style="width:100%; text-align:center; ">Добавить Соц сеть:</div></li>
				<li style="height: 50px;">
					<div class="oll_soc_set" style="text-align:center;display: inline-block;">
						<div data-net="od" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px; float:left; {if $usr2.odnokl == '1'}display:none;{/if} "></div>
						<div data-net="vk" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -144px -8px; width:50px; height:50px; float:left; {if $usr2.vk == '1'}display:none;{/if} "></div>
						<div data-net="tv" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -202px -8px; width:50px; height:50px; float:left; {if $usr2.tviter == '1'}display:none;{/if} "></div>
						<div data-net="g_plus" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px; float:left; {if $usr2.g_plus == '1'}display:none;{/if} "></div>
						<div data-net="fb" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; float:left; {if $usr2.fasebook == '1'}display:none;{/if} "></div>
						<div data-net="mail" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -85px -124px; width:50px; height:50px; float:left; {if $usr2.mail == '1'}display:none;{/if} "></div>
					</div>
				</li>
			</ul>
			
		</div>
{literal}
<script>
	$('.oll_soc_set').on('click', 'div', function(){
		data_set = $(this).attr("data-net");
		$(this).css('display', 'none');
		$(this).parent().parent().parent().find('.my_soc_cet div').each(function(){
			data_set_user = $(this).attr("data-net");
			if(data_set == data_set_user){
			$(this).css('display', 'block');
			
			if(data_set_user == 'od'){href_soc_set = 'http://ok.ru/';}
			if(data_set_user == 'vk'){href_soc_set = 'https://vk.com/';}
			if(data_set_user == 'tv'){href_soc_set = 'https://twitter.com/';}
			if(data_set_user == 'g_plus'){href_soc_set = 'https://plus.google.com/';}
			if(data_set_user == 'fb'){href_soc_set = 'https://www.facebook.com/';}
			if(data_set_user == 'mail'){href_soc_set = 'https://my.mail.ru';}
			
			$(this).html('<input type="hidden" name="'+data_set_user+'" value="'+href_soc_set+'">');
			}
		});
		
		
	});
	$('.my_soc_cet').on('click', 'div', function(){
		data_set = $(this).attr("data-net");
		$(this).css('display', 'none');
		$(this).html('');
		$(this).parent().parent().parent().find('.oll_soc_set div').each(function(){
			data_set_user = $(this).attr("data-net");
			if(data_set == data_set_user){
			$(this).css('display', 'block');
			}
		});
		
		
	});
</script>
{/literal}
		<!-- платежные данные -->
		<div id="payment_inf">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!--  -->
				<tr class='info'>
                    <td width="300" valign="top"></td>
                    <td valign="top"><strong>{$LANG.PAYMENT_INFORMATION}</strong></td>
                </tr>
				<!-- номер карты -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.CARD_NUMBER}</strong><br />
                        </td>
                    <td valign="top"><input name="card_number" type="text" class="text-input" id="card_number" style="width:300px" value="{$usr2.card_number}"/></td>
                </tr>
				<!-- срок действия карты -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.DATE_CARD}</strong><br />
                        </td>
                    <td valign="top"><input name="date_card" type="text" class="text-input" id="date_card" style="width:300px" value="{$usr2.date_card}"/></td>
                </tr>
				<!-- W1 (USD) -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>W1 (USD)</strong><br />
                        </td>
                    <td valign="top"><input name="w1_usd" type="text" class="text-input" id="w1_usd" style="width:300px" value="{$usr2.w1_usd}"/></td>
                </tr>
				<!-- W1 (RUB) -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>W1 (RUB)</strong><br />
                        </td>
                    <td valign="top"><input name="w1_rub" type="text" class="text-input" id="w1_rub" style="width:300px" value="{$usr2.w1_rub}"/></td>
                </tr>
				<!-- PerfectMoney -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>PerfectMoney</strong><br />
                        </td>
                    <td valign="top"><input name="PerfectMoney" type="text" class="text-input" id="PerfectMoney" style="width:300px" value="{$usr2.PerfectMoney}"/></td>
                </tr>
				<!-- OKpay -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>OKpay</strong><br />
                        </td>
                    <td valign="top"><input name="OKpay" type="text" class="text-input" id="OKpay" style="width:300px" value="{$usr2.OKpay}"/></td>
                </tr>
				<!-- EGOpay -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>EGOpay</strong><br />
                        </td>
                    <td valign="top"><input name="EGOpay" type="text" class="text-input" id="EGOpay" style="width:300px" value="{$usr2.EGOpay}"/></td>
                </tr>
				<!-- QIWI -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>QIWI</strong><br />
                        </td>
                    <td valign="top"><input name="QIWI" type="text" class="text-input" id="QIWI" style="width:300px" value="{$usr2.QIWI}"/></td>
                </tr>
				<!-- Paypal -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Paypal</strong><br />
                        </td>
                    <td valign="top"><input name="Paypal" type="text" class="text-input" id="Paypal" style="width:300px" value="{$usr2.Paypal}"/></td>
                </tr>
				<!-- Tinkoff Wallet -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Tinkoff Wallet</strong><br />
                        </td>
                    <td valign="top"><input name="Tinkoff_Wallet" type="text" class="text-input" id="Tinkoff_Wallet" style="width:300px" value="{$usr2.Tinkoff_Wallet}"/></td>
                </tr>
				<!-- Webmoney -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Webmoney</strong><br />
                        </td>
                    <td valign="top"><input name="Webmoney" type="text" class="text-input" id="Webmoney" style="width:300px" value="{$usr2.Webmoney}"/></td>
                </tr>
				<!-- RBK Money -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>RBK Money</strong><br />
                        </td>
                    <td valign="top"><input name="RBK_Money" type="text" class="text-input" id="RBK_Money" style="width:300px" value="{$usr2.RBK_Money}"/></td>
                </tr>
				<!-- Деньги@mail.ru -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Деньги@mail.ru</strong><br />
                        </td>
                    <td valign="top"><input name="money_mail" type="text" class="text-input" id="money_mail" style="width:300px" value="{$usr2.money_mail}"/></td>
                </tr>
				<!-- Payeer -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Payeer</strong><br />
                        </td>
                    <td valign="top"><input name="Payeer" type="text" class="text-input" id="Payeer" style="width:300px" value="{$usr2.Payeer}"/></td>
                </tr>
				<!-- Yandex -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>Yandex</strong><br />
                        </td>
                    <td valign="top"><input name="Yandex" type="text" class="text-input" id="Yandex" style="width:300px" value="{$usr2.Yandex}"/></td>
                </tr>
			</table>
		</div>
		
		<div id='change_color'>

<!-- BEGIN STYLESHEET SWITCHER -->
{literal}
<script>
	var c = readCookie('style');
	$('#kartka').on('click',function(){
		location="ordercard.html";
	})
</script>
{/literal}
			<ul id="stylesheets">
				<li>
					<a id='dark' href="javascript:switchStylestyle('dark');">
						<img src='/images/red.png'/>
						<input type='checkbox' id='checkbox-id1' value='v1' name='check1'/>
						<label for="checkbox-id1"></label>
					</a>
				</li>
				<li>
					<a id='blue' href="javascript:switchStylestyle('blue');">
						<img src='/images/blue.png'/>
						<input type='checkbox' id='checkbox-id2' value='v1' name='check1'/>
						<label for="checkbox-id2"></label>
					</a>
				</li>
				<li>
					<a id='green' href="javascript:switchStylestyle('green');">
						<img src='/images/green.png'/>
						<input type='checkbox' id='checkbox-id3' value='v1' name='check1'/>
						<label for="checkbox-id3"></label>
					</a>
				</li>
			</ul>
		
		</div>
		<div style="margin-top: 12px;text-align:center" id="submitform">
			<button name="save"  type="submit"  id="save_btn"><span>{$LANG.SAVE}</span></button>
			<button name="save" name="delbtn2"  type="button"  id="delbtn2" onclick="location.href='/users/{$usr.id}/delprofile.html';">
				<span>{$LANG.DEL_PROFILE}</span>
			</button>
        </div>
    </form>
    <div id="change_password">
        <form id="editform" name="editform" method="post" action="">
            <input type="hidden" name="opt" value="changepass" />
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="150" valign="top">
                        
                    </td>
                    <td valign="top">Защита доступа</td>
                </tr>
				<tr>
                    <td style="width: 200px;" valign="top">
                        <input id="protection_user"  type="radio" {if $usr.protection_user == '1'} checked {/if} value="1" name="protection_user"><label>Отключено</label>
						<input id="protection_user"  type="radio" {if $usr.protection_user == '2'} checked {/if}  value="2" name="protection_user"><label>СМС</label>
						<input id="protection_user"  type="radio" {if $usr.protection_user == '3'} checked {/if}  value="3" name="protection_user"><label>Э-почта</label>
					</td>
                </tr>
				<tr>
                    <td width="150" valign="top">
                        
                    </td>
                    <td valign="top">Смена пароля</td>
                </tr>
				<tr>
                    <td width="150" valign="top">
                        <strong>{$LANG.OLD_PASS}:</strong>
                    </td>
                    <td valign="top">
                        <input name="oldpass" type="password" id="oldpass" class="text-input" size="30" />
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS}:</strong></td>
                    <td valign="top"><input name="newpass" type="password"  id="newpass" class="passes text-input" size="30" />
					
					<a id='showpass'>Показать пароль</a></td>
					
				</tr>
				<tr>
					<td></td>
					<td>
						<ul id='galki'>
							<li><span>Минимум 2 латинские буквы</span></li>
							<li><span>Минимум 2 цифры</span></li>
							<li><span>Минимум 10 символов</span></li>
							<li><span>Максимум 16 символов</span></li>
						</ul>
					</td>
				</tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS_REPEAT}:</strong></td>
                    <td valign="top">
					<input name="newpass2" type="password" class="passes text-input" id="newpass2" size="30" /><span id='galkun'></span></td>
                </tr>
            </table>
            <div style="margin-top: 72px;text-align:center">
			<button name="save2" disabled='true' type="submit"  id="save2"><span>{$LANG.CHANGE_PASSWORD}</span></button>
			<button id="clear" type='button'><span>{$LANG.CLEAR_PASSWORD}</span></button>
               
            </div>
        </form>
    </div>
</div>
</div>


	<!-- Верификация -->


<!-- СМЕНА ПАРОЛЯ -->
{literal}
	<script type="text/javascript">
	
		
		$(document).ready(function(){
		//$('#date_priem').DatePicker();
			$('#showpass').click(function(){
				$('#newpass').attr('type','text');
			});
			var c = readCookie('style');
			if (c){
				$('#stylesheets li a#'+c).find("input[type='checkbox']").prop("checked", true);
			}
			
		});
		
		/**/
		$(function(){
			$('#stylesheets li a').click(function(){
			//alert('111111');
				$(this).find("input[type='checkbox']").trigger('click');
				//alert($(this).attr("id"));
				switchStylestyle($(this).attr("id"));
				$(this).find("input[type='checkbox']").prop("disabled",true);
				$('#stylesheets').find("input[type='checkbox']").not($(this).find("input[type='checkbox']")).each(function(){
					$(this).prop("disabled",false);
					if($(this).prop("checked") == true) {
						$(this).prop("checked", false);
					}
				});
			});
		});
		/*
		$('#blue').click(function(){
			$('#checkbox-id2').trigger('click');
		});
		
		$('#green').click(function(){
			$('#checkbox-id3').trigger('click');
		});*/
		
		/**/
		
		$('#clear').bind('click',function(){
			$('#oldpass').val('');
			$('#newpass').val('');
			$('#newpass2').val('');
		});
		
		$(function(){
			$( '#tabs li' ).click( function(){
				rel = $( this ).attr( "rel" );
				if(!rel){
					$('#submitform').show();
				} else {
					$('#submitform').hide();
				}
			});
		});
	</script>
{/literal}
{literal}
	<script>
	
	
	//$('#wrapper').find('#date_priem').text('1111');
	
	
			var hash = document.location.hash;
			if(hash == '#successSend'){
				$("#modal_success_btn").click();
				document.location.hash = "";
			}
	
		$('.passes').bind("change keyup input click", function() {
			if (this.value.match(/[^0-9a-zA-Z]/g)) {
				this.value = this.value.replace(/[^0-9a-zA-Z]/g, '');
			}
		});
		
		$('#newpass').focusout(function(){
			$('#galki li').removeClass('galochka');
			
			var test = $('#newpass').val();
			var errors = 0;
			if (--test.split(/[a-z]/).length>1)
			{
				$('#galki li:first-child').addClass('galochka');
			}
			else{
			  var errors = 1;
			}
				
			if (--test.split(/[0-9]/).length>1)
				{
					$('#galki li:nth-child(2)').addClass('galochka');
				}
			else
				{
				 var errors = 1;
				}
			
			if (--test.split(/[a-zA-z0-9]/).length>9 && --test.split(/[a-zA-z0-9]/).length<17)
				{
					$('#galki li:nth-child(3)').addClass('galochka');
				}
			else
				{
					 var errors = 1;
				}
			if (--test.split(/[a-zA-z0-9]/).length<17 && --test.split(/[a-zA-z0-9]/).length>1)
				{
					$('#galki li:last-child').addClass('galochka');
				}
			else	
				{
					var errors = 1;
				}
				var test2 = $('#newpass2').val();
			if(errors ==true)
			{
				$('#save2').prop('disabled',true);
			}
		});
		$('#newpass2').focusout(function()
		{
			var test = $('#newpass').val();
			var test2 = $('#newpass2').val();
			if(test == test2)
				{
					$('#save2').prop('disabled',false);
					$('#galkun').addClass('galkun');
				}
			else
				{
					$('#save2').prop('disabled',true);
				}
		});
		
		
		var nav = $('#nav');
		var selection = $('.selectsubs');
		var select = selection.find('li');
		nav.click(function(event) {
			if (nav.hasClass('active')) {
				nav.removeClass('active');
				selection.stop().slideUp(200);
			} else {
				nav.addClass('active');
				selection.stop().slideDown(200);
			}
			event.preventDefault();
		});
		select.click(function(event) {
			select.removeClass('active');
			nav.trigger('click');
			$('#cm_subscribe').attr('value', $(this).attr('data-value'));
			$('#nav').text($(this).text());
		});
	</script>
{/literal}
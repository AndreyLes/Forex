{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_css file='includes/jquery/tabs/tabs.css'}

{literal}
	<script type="text/javascript">
        $(function(){$(".uitabs").tabs();});
	</script>
{/literal}

<div id="usertitle">

    <div id="user_ratings">
        <div class="karma" title="{$LANG.KARMA}">
        	<div class="{if $usr.karma >= 0}value-positive{else}value-negative{/if}" id="u_karma_cont">
                <table cellpadding="2" cellspacing="0"><tr>
                    <td class="sign_link" style="color:green">
                    {if $usr.can_change_karma}
                        <a href="javascript:void(0)" onclick="users.changeKarma('{$usr.id}', 'plus');return false;" title="{$LANG.KARMA} +"><img src="/templates/{template}/images/icons/karma_up.png" border="0" alt="{$LANG.KARMA} +"/></a>
                    {/if}
                    </td>
                    <td><span class="user_karma_point" id="u_karma">{$usr.karma}</span></td>
                    <td style="color:red" class="sign_link">
                    {if $usr.can_change_karma}
                        <a href="javascript:void(0)" onclick="users.changeKarma('{$usr.id}', 'minus'); return false;" title="{$LANG.KARMA} -"><img src="/templates/{template}/images/icons/karma_down.png" border="0" alt="{$LANG.KARMA} -"/></a>
                    {/if}
                    </td>
                </tr></table>
            </div>
        </div>
        <div class="rating" title="{$LANG.RATING}">
            <div class="value">{$usr.user_rating}</div>
        </div>
    </div>

    <div class="user_group_name">
        <div class="{$usr.group_alias}">{$usr.grp}</div>
    </div>

    <div class="con_heading" id="nickname">
        {$usr.nickname} {if $usr.banned}<span style="color:red; font-size:12px;">{$LANG.USER_IN_BANLIST}</span>{/if}
    </div>

</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
                            <img alt="{$usr.nickname|escape:'html'}" class="usr_img" src="{$usr.avatar}" />
                        </div>

						{if $is_auth}
							<div id="usermenu" style="">
                            <div class="usr_profile_menu">
							<table cellpadding="0" cellspacing="6" ><tr>

					

                          
                         	{if $myprofile}
                                 <tr>
                                    <td><img src="/templates/{template}/images/icons/profile/avatar.png" border="0"/></td>
                                    <td><a href="/users/{$usr.id}/avatar.html" title="{$LANG.SET_AVATAR}">{$LANG.SET_AVATAR}</a></td>
                                </tr>
								{if $usr.invites_count}
                                <tr>
                                    <td><img src="/templates/{template}/images/icons/profile/invites.png" border="0"/></td>
                                    <td><a href="/users/invites.html" title="{$LANG.MY_INVITES}">{$LANG.MY_INVITES}</a> {$usr.invites_count}</td>
                                </tr>
								{/if}
                                <tr>
                                    <td><img src="/templates/{template}/images/icons/profile/edit.png" border="0"/></td>
                                    <td><a href="/users/{$usr.id}/editprofile.html" title="{$LANG.CONFIG_PROFILE}">{$LANG.MY_CONFIG}</a></td>
                                </tr>
                            {/if}
                            {if $is_admin && !$myprofile}
                            <tr>
                                <td><img src="/templates/{template}/images/icons/profile/edit.png" border="0"/></td>
                                <td><a href="/users/{$usr.id}/editprofile.html" title="{$LANG.CONFIG_PROFILE}">{$LANG.CONFIG_PROFILE}</a></td>
                            </tr>
                            {/if}
                       		{if !$myprofile}
                            	{if $is_admin}
                                	{if !$usr.banned}
                                    <tr>
                                        <td><img src="/templates/{template}/images/icons/profile/award.png" border="0"/></td>
                                        <td><a href="/users/{$usr.id}/giveaward.html" title="{$LANG.TO_AWARD}">{$LANG.TO_AWARD}</a></td>
                                    </tr>
                                    {if $usr.id != 1}
                                    <tr>
                                        <td><img src="/templates/{template}/images/icons/profile/ban.png" border="0"/></td>
                                        <td><a href="/admin/index.php?view=userbanlist&do=add&to={$usr.id}" title="{$LANG.TO_BANN}">{$LANG.TO_BANN}</a></td>
                                    </tr>
                                    {/if}
                                    {/if}
                                {if $usr.id != 1}
                                    <tr>
                                        <td><img src="/templates/{template}/images/icons/profile/delprofile.png" border="0"/></td>
                                        <td><a href="/users/{$usr.id}/delprofile.html" title="{$LANG.DEL_PROFILE}">{$LANG.DEL_PROFILE}</a></td>
                                    </tr>
                                {/if}
                                {/if}
                         	{/if}

                            </table></div>
                            </div>
						{/if}
					</td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">

						<div class="field">
							<div class="title">{$LANG.LAST_VISIT}:</div>
							<div class="value">{$usr.flogdate}</div>
						</div>
						<div class="field">
							<div class="title">{$LANG.DATE_REGISTRATION}:</div>
							<div class="value">
                                {$usr.fregdate}
                            </div>
						</div>
                        {if $usr.inv_login}
                            <div class="field">
                                <div class="title">{$LANG.INVITED_BY}:</div>
                                <div class="value">
                                    <a href="{profile_url login=$usr.inv_login}">{$usr.inv_nickname}</a>
                                </div>
                            </div>
                        {/if}
                        {if $usr.city}
						<div class="field">
							<div class="title">{$LANG.CITY}:</div>
                            <div class="value"><a href="/users/city/{$usr.cityurl|escape:'html'}">{$usr.city}</a></div>
						</div>
                        {/if}

						{if $usr.showbirth && $usr.fbirthdate}
						<div class="field">
							<div class="title">{$LANG.BIRTH}:</div>
							<div class="value">{$usr.fbirthdate}</div>
						</div>
						{/if}

					
						{if $usr.showmail}
							{add_js file='includes/jquery/jquery.nospam.js'}
							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="{$usr.email|NoSpam}" class="email">{$usr.email}</a></div>
							</div>
							{literal}
								<script>
										$('.email').nospam({ replaceText: true });
								</script>
							{/literal}
						{/if}

				

						{if $cfg.privforms && $usr.form_fields}
							{foreach key=tid item=field from=$usr.form_fields}
                                <div class="field">
                                    <div class="title">{$field.title}:</div>
                                    <div class="value">{if $field.field}{$field.field}{else}<em>{$LANG.NOT_SET}</em>{/if}</div>
                                </div>
                            {/foreach}
						{/if}

					</div>

				</div>

                {foreach key=id item=plugin from=$plugins}
                    <div id="upr_{$plugin.name}">{$plugin.html}</div>
                {/foreach}

			</div>
	</td>
  </tr>
</table>

{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_css file='includes/jquery/tabs/tabs.css'}
<p class="ord-title">{$LANG.ORDER_TITLE}</p>
<div class="order-cont">
	<p class="order-title">{$LANG.ORDER_TITLE}</p>
	<form method="post">
		<div class="info">
			<p class="required-info">{$LANG.REQUIED_INPUT}</p>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td colspan="2">
								<p class="title">{$LANG.PRIVATE_INFO}</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.NAME}</p>
							</td>
							<td>
								<input type="text" name="firstname" required value="{$userName.0}" />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.LASTNAME}</p>
							</td>
							<td>
								<input type="text" name="lastname" required value="{$userName.1}" />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.CITIZENSHIP}</p>
							</td>
							<td>
								<select name="citizenship" required >
									<option value="0">{$LANG.CITIZEN_DEF}</option>
									<option value="{$LANG.CITIZEN_UA}" >{$LANG.CITIZEN_UA}</option>
									<option value="{$LANG.CITIZEN_RU}" >{$LANG.CITIZEN_RU}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>	
								<p>{$LANG.SEX}</p>
							</td>
							<td>
								<label>
									<input type="radio" checked name="sex" value="{$LANG.MALE}" />
									{$LANG.MALE}
								</label>
								<label>
									<input type="radio" name="sex" value="{$LANG.FEMALE}" />
									{$LANG.FEMALE}
								</label>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.BIRTHDAY}</p>
							</td>
							<td>
								<select name="day" required >
									<option value="0">{$LANG.DAY_DEF}</option>
										{foreach from=$dayArray item=day1}
											{if $day eq $day1}
												<option selected value="{$day1}">{$day1}</option>
											{else}
												<option value="{$day1}">{$day1}</option>
											{/if}
										{/foreach}
								</select>
								<select name="month" required >
									<option value="0">{$LANG.MONTH_DEF}</option>
									{foreach from=$monthArray item=month}
										{assign var=v value='MONTH_'|cat:$month}
										{if $month eq $monthUser}
											<option selected value="{$LANG.$v}" >{$LANG.$v}</option>
										{else}
											<option value="{$LANG.$v}" >{$LANG.$v}</option>
										{/if}
									{/foreach}
								</select>
								<select name="year" required >
									<option value="0">{$LANG.YEAR_DEF}</option>
									{foreach from=$yearArray item=year2}
										{if $year2 eq $year}
											<option selected value="{$year2}" >{$year2}</option>
										{else}
											<option value="{$year2}" >{$year2}</option>
										{/if}
									{/foreach}
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.PHONE}</p>
							</td>
							<td>
								<input type="tel" name="phone" required value="{$userInfo.mobtelephone}" />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="title">{$LANG.PRIVATE_DATA}</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.EMAIL}</p>
							</td>
							<td>
								<input type="email" name="email" required value="{$userInfo.email}" />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.CHECK_EMAIL}</p>
							</td>
							<td>
								<input type="email" name="check_email" required value="{$userInfo.email}" />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.PASSWORD}</p>
							</td>
							<td>
								<input type="password" name="password" required />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.CHECK_PASSWORD}</p>
							</td>
							<td>
								<input type="password" name="check_password" required />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.QUESTION}</p>
							</td>
							<td>
								<input type="text" name="question" required  />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.ANSWER}</p>
							</td>
							<td>
								<input type="text" name="answer" required />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="title">{$LANG.ORDER_TITLE2}</p>
							</td>
						</tr>
						<tr>
							<td>
								<div class="card-img"></div>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.CARD_CURRENCY}</p>
							</td>
							<td>
								<select name="currency" >
									<option value="{$LANG.CURRENCY_USA}">{$LANG.CURRENCY_USA}</option>
									<option value="{$LANG.CURRENCY_UA}">{$LANG.CURRENCY_UA}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.PAYMENT_METHOD}</p>
							</td>
							<td>
								<select name="shipping_method" >
									<option value="{$LANG.SHIPPING_1}">{$LANG.SHIPPING_1}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p class="title">{$LANG.PAYMENT_ADDRESS}</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.COUNTRY}</p>
							</td>
							<td>
								<select name="country" >
									<option value="{$LANG.COUNTRY_UA}">{$LANG.COUNTRY_UA}</option>
									<option value="{$LANG.COUNTRY_RU}">{$LANG.COUNTRY_RU}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.CITY}</p>
							</td>
							<td>
								<input type="text" name="city" required />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.INDEX}</p>
							</td>
							<td>
								<input type="text" name="index" required />
							</td>
						</tr>
						<tr>
							<td>
								<p>{$LANG.ADDRESS1}</p>
							</td>
							<td>
								<input name="address1"  type="text" required /> 
							</td>
						</tr>
					</tbody>
				</table>
				<img class="submit-img" id="sendmap" usemap="#sendmap" src="/images/button1.png" />
				<img usemap="#cancelmap" id="cancelmap" src="/images/button2.png" />
				<map name="sendmap">
					<area shape="poly" class="send" coords="0, 0, 155, 0, 213, 39, 0, 39" />
				</map>
				<map name="cancelmap">
					<area shape="poly" class="cancel" coords="0, 0, 210, 0, 210, 38, 50, 38" />
				</map>
			</div>
		</div>
	</form>
</div>
{literal}
	<script>
		$(document).ready(function(){
			
			$('form').submit( function(){
				
				$('.validate-error').remove();
				var error=false;
				
				$('input').each(function(){
					if($(this).val().length==0){
						$(this).parent().append("<p class='validate-error' >Введите данные</p>");
						error=true;
					}
				});
				
				if($('input[name="email"]').val()!=$('input[name="check_email"]').val()){
					$('input[name="check_email"]').parent().append("<p class='validate-error' >Почта не совпадает</p>");
					error=true;
				}
				if($('select[name="citizenship"]').val()=="0"){
					$('select[name="citizenship"]').parent().append("<p class='validate-error' >Выберите гражданство</p>");
					error=true;
				}
				if($('input[name="password"]').val()!=$('input[name="check_password"]').val()){
					$('input[name="check_password"]').parent().append("<p class='validate-error' >Пароли не совпадают</p>");
					error=true;
				}
				if($('select[name="day"]').val()=="0" || $('select[name="month"]').val()=="0" || $('select[name="year"]').val()=="0"){
					$('select[name="day"]').parent().append("<p class='validate-error' >Выберите дату</p>");
					error=true;
				}
				if(error)
					return false;
			});
			
			$('.send').on('click', function(){
				$('form').submit();
			})
			
			$('.send').hover(function(){
				$('#sendmap').css('opacity','0.5');
			}, function(){
				$('#sendmap').css('opacity','1');
			})
			
			$('.cancel').hover(function(){
				$('#cancelmap').css('opacity','0.5');
			}, function(){
				$('#cancelmap').css('opacity','1');
			})
			
			$('.cancel').on('click', function(){
				location="editprofile.html";
			})
		})
	</script>
{/literal}
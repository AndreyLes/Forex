{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}
{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#save_paym_sys'){
				$("#modal_success_btn").click();
				$("#text_span").text("Номер счета сохранен.");
				document.location.hash = "";
			}
		});
	</script>
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Платежные данные
	</h4>
	<div id='underline'>
		<span>
		
		</span>
	</div>
</div>
<div id="big_payment_system">


{literal}
<script>
	
</script>
{/literal}
<!-- Платежные данные -->
<div style=" border: 1px solid black; margin-top: 20px; width: 99.8%;" class='kab-block'>
	<div class="user_payment_system">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю</span>
		<ul>
		{foreach key=tid item=system from=$payments}
			{if $inf_moneys[$system.id].score_num != ''}
			<li data-inf="0" data-id="{$system.id}" data-relations="{$system.relations}" data-val="{$inf_moneys[$system.id].score_val}">
				<div style="width:105px; margin:0px 10px; float:left;"><img style="float:left;" src="{$system.img}"></div>
				<span style="float:left; text-align: left; padding-left: 0px; width: 105px;">{$system.name}</span>
				<label ></label>
				<input type="checkbox" >
				<a style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
			{/if}
		{/foreach}
			<li class="all_inf">
				<span style=" width: 220px;">Обменять свою ПС</span>
				<label class="user_system"></label>
				<input type="checkbox" >
				<a class="sdsdsdsd1" style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
		</ul>
	</div>
	<div class="payment_system">
		<span style="width:100%; display: block; float: none; padding:10px;">Получаю</span>
		<ul>
		{foreach key=tid item=system from=$payments}
			<li data-inf="0" data-id="{$system.id}" class="clicable">
			
				<div style="width:105px; margin:0px 10px; float:left;"><img style="float:left;" src="{$system.img}"></div>
				<span style="float:left; width: 105px;">{$system.name}</span>
				<label class="clicable"></label>
				<input type="checkbox" >
			<a class="clicable" style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
		{/foreach}
			<li class="other_ps">
				<span style="padding-left: 16px; width: 220px;">Другая ПС</span>
				<label class="user_system2"></label>
				<input type="checkbox" >
				<a class="sdsdsdsd2" style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
		</ul>
	</div>
	<!--div class="reserve">
	</div-->
</div>
<form method="post" action="/users/{$id}/design.html" onsubmit="return validateForm()">
<div style="display:none;" data-id="{$id}" class="verification">{$verification}</div>
<input type="hidden" name="opt" value="registration">
<input id="what_ps" name="what_ps" type="hidden" value="">
<div class="priem_system" >
	<div class="user_s">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю:</span>
		<div></div>
	</div>
	<div class="system_s">
		<span style="width:100%; display: block; float: none; padding:10px;">Получаю:</span>
		<div></div>
	</div>
</div>
<div class="garant_fond" data-garant="{$money_garant}" >
	<span style="float:left;margin-top: 8px;">Ваш гарантийный фонд: {$money_garant} $</span>
	<div style="float:left; margin-left:15px;" class="td_buttonu"><a href="/users/{$id}/guarante_fond.html">{if $money_garant == 0}Пополнить{else}Повысить{/if} гарантийный фонд</a></div>
</div>
<!-- выбор гаранта -->
<div class="vubor_garanta" style="position: relative;">
	<div id="garant1" class="hint_garant" data-inf2="Гарант" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom" style="border-right:1px solid black;">Гарант</div>
	<div class="hint_garant" data-inf2="Внутренняя конвертация" data-inf="consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in.Lorem ipsum dolor sit amet," data-class="bottom" id="garant2" style="border-right:1px solid black;">Внутренняя конвертация</div>
	<div class="hint_garant" data-inf2="Гарантийний Фонд" data-inf="consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in.Lorem ipsum dolor sit amet," data-class="bottom" id="garant3">Гарантийний Фонд</div>
	<input type="hidden" name="vubor_garanta" value="">
</div>
<div>
	<label id="sell_parts_l" style="float:left; margin-right: 10px;">Продать частями</label>
	<label style="float:left; margin-top: 2px;" id="sell_parts"></label>
	<input type="checkbox" name="sell_parts">
</div>
<div class="commission" data-userid="{$id}">
	<div>
		<span>Комиссия:</span>
		<input type="text" name="commission" value="">
	</div>
	<!--div>
		<span>Всего:</span>
		<input type="text" name="all_money">
	</div-->
</div>
<div style="text-align: center;">
	<input style="text-transform: uppercase;" id="save_btn" type="submit" value="оформить">
	<input style="color: #fff;text-transform: uppercase;" type="button" onclick="location.href='/users/{$nik_user}'" id="delbtn2" value="Назад">
</div>
</form>
</div>
{if $vubor_garanta}
{if $vubor_garanta == '3' && $what_ps != "satndart_ps"}

	{if $what_ps == "user_ps"}
	<div class="hidden_infa" style="display:none;">
		<div class="payment_user">{$payment_user}</div>
		<div class="payment_system_name">{$payment_system_name}</div>
		
		<div class="vubor_garanta">{$vubor_garanta}</div>
		<div class="sell_parts">{$sell_parts}</div>
		
		<div class="us_sys_name2">{$us_sys_name2}</div>
		<div class="kyrs">{$kyrs}</div>
		<div class="payment_system">{$payment_system}</div>
		<div class="score">{$score}</div>
	</div>
	{literal}
	<script>
		$(window).load(function() {
			
			sell_parts = $('.hidden_infa .sell_parts').text();
			garant = $('.hidden_infa .vubor_garanta').text();
			
			name1 = $('.hidden_infa .payment_system_name').text();
			money1 = $('.hidden_infa .payment_user').text();
			money1 = parseFloat(money1);
			
			name2 = $('.hidden_infa .us_sys_name2').text();
			kurs = $('.hidden_infa .kyrs').text();
			kurs = parseFloat(kurs);
			score = $('.hidden_infa .score').text();
			money2 = money1*kurs;
			
			$('.user_payment_system .user_system').click();
			$('.payment_system .user_system2').click();
			
			$('.user_s').find('.us_sys_name1').val(name1);
			$('.user_s').find('.hhhhhhh').val(money1);
			$('.system_s').find('.dddddd').val(kurs);
			$('.system_s').find('.us_sys_name2').val(name2);
			$('.system_s').find('.score_num').val(score);
			$('.system_s').find('.cccccc').val(money2);
			
			$('.vubor_garanta #garant3').click();
			
			if(sell_parts == 'on'){  $('#sell_parts').click(); }
			
			//покупка частями
		});
	</script>
	{/literal}
	{elseif $what_ps == "user_st_ps"}
	<div class="hidden_infa" style="display:none;">
		<div class="payment_user">{$payment_user}</div>
		<div class="payment_system_name">{$payment_system_name}</div>
		
		<div class="vubor_garanta">{$vubor_garanta}</div>
		<div class="sell_parts">{$sell_parts}</div>
		
		<div class="us_sys_name2">{$us_sys_name2}</div>
		<div class="kyrs">{$kurs}</div>
		<div class="payment_system">{$system_money}</div>
		<div class="score">{$score}</div>
		<div class="payment_system_id">{$payment_system_id}</div>
	</div>
	{literal}
		<script>
		
		$(window).load(function() {
			
			name1 = $('.hidden_infa .payment_system_name').text();
			money1 = $('.hidden_infa .payment_user').text();
			money1 = parseFloat(money1);
			
			text_id = $('.hidden_infa .payment_system_id').text();
			$('.user_payment_system li').each(function(){
				if(text_id == $(this).attr('data-id')){
					$(this).find('label').click();
					$('.hhhhhhh').val(money1);
				}
			});
			
			sell_parts = $('.hidden_infa .sell_parts').text();
			garant = $('.hidden_infa .vubor_garanta').text();
			
			name2 = $('.hidden_infa .us_sys_name2').text();
			kurs = $('.hidden_infa .kyrs').text();
			kurs = parseFloat(kurs);
			score = $('.hidden_infa .score').text();
			money2 = money1*kurs;
			
			$('.payment_system .user_system2').click();
			
			$('.system_s').find('.dddddd').val(kurs);
			$('.system_s').find('.us_sys_name2').val(name2);
			$('.system_s').find('.score_num').val(score);
			$('.system_s').find('.cccccc').val(money2);
			
			$('.vubor_garanta #garant3').click();
			
			if(sell_parts == 'on'){  $('#sell_parts').click(); }
			
			
			
		});
		</script>
	{/literal}
	{elseif $what_ps == "standart_user_ps"}
	<div class="hidden_infa" style="display:none;">
		<div class="payment_user">{$payment_user}</div>
		<div class="payment_system_name">{$payment_system_name}</div>
		
		<div class="vubor_garanta">{$vubor_garanta}</div>
		<div class="sell_parts">{$sell_parts}</div>
		{foreach key=tid item=inf from=$information}
		<span>
			<div class="priem_id">{$inf.id}</div>
			<div class="priem_kurs">{$inf.kurs}</div>
			<div class="priem_system_money">{$inf.system_money}</div>
		</span>
		{/foreach}
	</div>
	{literal}
		<script>
		
		$(window).load(function() {
			
			name1 = $('.hidden_infa .payment_system_name').text();
			money1 = $('.hidden_infa .payment_user').text();
			money1 = parseFloat(money1);
			
			$('.user_payment_system .user_system').click();
			
			$('.user_s').find('.us_sys_name1').val(name1);
			$('.user_s').find('.hhhhhhh').val(money1);
			
			$('.user_payment_system li').each(function(){
				text_id = $('.hidden_infa .payment_system_id').text();
				if(text_id == $(this).attr('data-id')){
					$(this).find('label').click();
					$('.hhhhhhh').val($('.hidden_infa .payment_user').text());
				}
			});
			$('.hidden_infa span').each(function(){
				text_id2 = $(this).find('.priem_id').text();
				//alert(text_id2);
				$('.payment_system li').each(function(){
					if(text_id2 == $(this).attr('data-id')){
						//alert($(this).find('label').attr('class'));
						$(this).find('label').click();
					}
				});
			});
			$('.hidden_infa span').each(function(){
				text_id = $(this).find('.priem_id').text();
				rrrrr = $(this).find('.priem_kurs').text();
				rrrrr = parseFloat(rrrrr);
				bbbbb = rrrrr*money1;
				$('.system_s .priem_inf_each').each(function(){
					if($(this).find('.dddddd').attr('data-id') == "payment_user_"+text_id){
						$(this).find('.dddddd').val(rrrrr);
						$(this).find('.cccccc').val(bbbbb);
					}
				});
			});
			
			$('.vubor_garanta #garant3').click(); 
			
			sell_parts = $('.hidden_infa .sell_parts').text();
			if(sell_parts == 'on'){  $('#sell_parts').click(); }
			
			//покупка частями
		});
		</script>
	{/literal}
	{/if}
{else}

	<div class="hidden_infa" style="display:none;">
		<div class="payment_user">{$payment_user}</div>
		<div class="payment_system_id">{$payment_system_id}</div>
		<div class="vubor_garanta">{$vubor_garanta}</div>
		<div class="sell_parts">{$sell_parts}</div>
		{foreach key=tid item=inf from=$information}
		<span>
			<div class="priem_id">{$inf.id}</div>
			<div class="priem_kurs">{$inf.kurs}</div>
			<div class="priem_system_money">{$inf.system_money}</div>
		</span>
		{/foreach}
	</div>
{literal}
	<script>
	
	$(window).load(function() {
		money1 = $('.hidden_infa .payment_user').text();
		money1 = parseFloat(money1);
		$('.user_payment_system li').each(function(){
			text_id = $('.hidden_infa .payment_system_id').text();
			if(text_id == $(this).attr('data-id')){
				$(this).find('label').click();
				$('.hhhhhhh').val($('.hidden_infa .payment_user').text());
			}
		});
		$('.hidden_infa span').each(function(){
			text_id2 = $(this).find('.priem_id').text();
			//alert(text_id2);
			$('.payment_system li').each(function(){
				if(text_id2 == $(this).attr('data-id')){
					//alert($(this).find('label').attr('class'));
					$(this).find('label').click();
				}
			});
		});
		$('.hidden_infa span').each(function(){
			text_id = $(this).find('.priem_id').text();
			rrrrr = $(this).find('.priem_kurs').text();
			rrrrr = parseFloat(rrrrr);
			bbbbb = rrrrr*money1;
			$('.system_s .priem_inf_each').each(function(){
				if($(this).find('.dddddd').attr('data-id') == "payment_user_"+text_id){
					$(this).find('.dddddd').val(rrrrr);
					$(this).find('.cccccc').val(bbbbb);
				}
			});
		});
		garant = $('.hidden_infa .vubor_garanta').text();
		if(garant == '1'){ $('.vubor_garanta #garant1').click(); }
		else if(garant == '2'){ $('.vubor_garanta #garant2').click(); }
		else if(garant == '3'){ $('.vubor_garanta #garant3').click(); }
		
		sell_parts = $('.hidden_infa .sell_parts').text();
		if(sell_parts == 'on'){  $('#sell_parts').click(); }
		
		//покупка частями
	});
	</script>
{/literal}
{/if}
{/if}
{literal}
<script>
	$('#sell_parts_l').click(function(){ $('#sell_parts').click(); });
	$('#sell_parts').click(function(){
		sell_parts = $(this).parent().find('input').prop("checked");
		if(sell_parts == false){
			$(this).css('background-position', '-1px -34px');
			$(this).parent().find('input').click();
		}
		else{
			$(this).css('background-position', '-1px -4px');
			$(this).parent().find('input').click();
		}	
	});
	$('.vubor_garanta #garant1').click(function(){
		$('.vubor_garanta').find('input').val('1');
		$(this).addClass('active');
		$('.commission').find('input[name="commission"]').val('0.5%');
		$('.vubor_garanta #garant2').removeClass('active');
		$('.vubor_garanta #garant3').removeClass('active');
	});
	$('.vubor_garanta #garant2').click(function(){
		$('.vubor_garanta').find('input').val('2');
		$(this).addClass('active');
		$('.commission').find('input[name="commission"]').val('0.1%');
		$('.vubor_garanta #garant1').removeClass('active');
		$('.vubor_garanta #garant3').removeClass('active');
	});
	$('.vubor_garanta #garant3').click(function(){
		$('.vubor_garanta').find('input').val('3');
		$(this).addClass('active');
		$('.commission').find('input[name="commission"]').val('0.5%');
		$('.vubor_garanta #garant1').removeClass('active');
		$('.vubor_garanta #garant2').removeClass('active');
	});

function validateForm(){
	var foo;
	
	if(gf == 1){$('.vubor_garanta').find('input').val('3');}
	if($('.user_s .priem_inf_each').html() == foo && $('.system_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите платежные системы!");
		return false;
	}
	else if($('.user_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите систему выплаты!");
		return false;
	}
	else if($('.system_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите систему получения!");
		return false;
	}
	else if(gf == 1){
		proverka = 0;
		$('.dddddd').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		$('.cccccc').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		if($('.hhhhhhh').val() == "" || proverka == 1 || $('.us_sys_name1').val == "" || $('.us_sys_name2').val == "" || $('.score_num').val() == ""){
			$("#modal_success_btn").click();
			$("#text_span").text("Заполните необходимые поля!");
			return false;
		}
	}
	else{
		proverka = 0;
		$('.dddddd').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		$('.cccccc').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		if($('.hhhhhhh').val() == "" || proverka == 1){
			$("#modal_success_btn").click();
			$("#text_span").text("Заполните необходимые поля!");
			return false;
		}
	}
	if($('.vubor_garanta input').val() == ""){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите тип заявки!");
		return false;
	}
	else if($('.vubor_garanta input').val() == "2"){
		var sum = $('.priem_system .user_s input[name="payment_user"]').val();
		if(varification == '1'){
			if(sum > 26){
				$("#modal_success_btn").click();
				$("#text_span").html('<p>У Вас недостаточный уровень верификации для создания заявки c суммой '+sum+'$! </p><p>Лимит:  20$</p><p>Используйте "Гарант" или <a href="/users/'+$('.verification').attr('data-id')+'/editprofile.html#verification" class="Up_varification">повысте уровень верификации</a> или измените сумму выдачи.</p>');
				return false;
			}
		}
		else if(varification == '2'){
			if(sum > 51){
				$("#modal_success_btn").click();
				$("#text_span").html('<p>У Вас недостаточный уровень верификации для создания заявки c суммой '+sum+'$! </p><p>Лимит:  50$</p><p>Используйте "Гарант" или <a href="/users/'+$('.verification').attr('data-id')+'/editprofile.html#verification" class="Up_varification">повысте уровень верификации</a> или измените сумму выдачи.</p>');
				return false;
			}
		}
		else if(varification == '3'){
			if(sum > 76){
				$("#modal_success_btn").click();
				$("#text_span").html('<p>У Вас недостаточный уровень верификации для создания заявки c суммой '+sum+'$! </p><p>Лимит:  50$</p><p>Используйте "Гарант" или <a href="/users/'+$('.verification').attr('data-id')+'/editprofile.html#verification" class="Up_varification">повысте уровень верификации</a> или измените сумму выдачи.</p>');
				return false;
			}
		}
		else if(varification == '4'){
			if(sum > 101){
				$("#modal_success_btn").click();
				$("#text_span").html('<p>У Вас недостаточный уровень верификации для создания заявки c суммой '+sum+'$! </p><p>Лимит:  50$</p><p>Используйте "Гарант" или <a href="/users/'+$('.verification').attr('data-id')+'/editprofile.html#verification" class="Up_varification">повысте уровень верификации</a> или измените сумму выдачи.</p>');
				return false;
			}
		}
		else{
			$("#modal_success_btn").click();
			$("#text_span").html('<p>У Вас недостаточный уровень верификации для создания заявки!</p><p>Используйте "Гарант" или <a href="/users/'+$('.verification').attr('data-id')+'/editprofile.html#verification" class="Up_varification">повысте уровень верификации</a>.</p>');
			return false;
		}
	}
	else if($('.vubor_garanta input').val() == "1"){
		var sum2 = $('.priem_system .user_s input[name="payment_user"]').val();
		var systems_is = $('.priem_system .user_s input[name="payment_user"]').attr('data-id');
		ggggg = parseInt(money[systems_is],10);
		
		
		if(sssa > ggggg){
			$("#modal_success_btn").click();
			$("#text_span").html('<p>У вас на счету: '+money[systems_is]+'</p><p>Вы не можете создать заявку з суммой: '+sssa+'</p><p><a class="popolnenie_score" href="/users/'+$('.commission').attr('data-userid')+'/investglav.html">Пополните свой счет на бирже</a> или измените суму заявки.</p>');
			return false;
		}
	}
	else if($('.vubor_garanta input').val() == "3"){
		hhhhf = parseFloat($('.hhhhhhh').val());
		if(hhhhf > garant_fond){
			$("#modal_success_btn").click();
			$("#text_span").html('<p>У Вас недостаточно средств на Гарантийном Фонде. Уменшите сумму заявки или <a href="/users/'+$('.verification').attr('data-id')+'/guarante_fond.html" class="Up_varification">повысте размер ГФ</a>.</p>');
			return false;
		}
		
	}
}


	
$(document).ready(function(){
	gf = '';
	garant_fond = $('.garant_fond').attr('data-garant');
	$('.garant_fond').attr('data-garant','');
	
	varification = $('.verification').text();
	$('.verification').text('');
	money = [];
	relation_mass = [];
	$('.user_payment_system li').each(function(){
		num = $(this).attr('data-id');
		
		relation_mass[num] = $(this).attr('data-relations');
		$(this).attr('data-relations', "");
		
		money[num] = $(this).attr('data-val');
		$(this).attr('data-val', '');
	});
	
	/*--------- клик на системы оплаты юзера ---------*/
	$('.user_payment_system a').not('.sdsdsdsd1').click(function(){
		$(this).parent().find('label').click();
	});
	$('.payment_system a').not('.sdsdsdsd2').click(function(){
		$(this).parent().find('label.clicable').click();
	});
//=====================================================================================
	$('.user_payment_system a.sdsdsdsd1').click(function(){
		$(this).parent().find('label').click();
	});
	$('.payment_system a.sdsdsdsd2').click(function(){
		$('.payment_system .user_system2').click();
	});
	$('.user_payment_system .user_system').click(function(){
		gf = 1;
		$('.vubor_garanta #garant3').click();
		
		$ttttr = 0;
		data_checed = $(this).parent().find('input').prop("checked");
		if(data_checed == false){
			$(this).css('background-position', '-1px -34px');
			$(this).parent().find('input').click();
			$(this).parent().addClass('active');
		}
		else{
			$(this).css('background-position', '-1px -4px');
			$(this).parent().find('input').click();
			$(this).parent().removeClass('active');
			$ttttr = 1;
		}
		
		user_payment_system_div = "";
		label_id = $(this).parent().attr('data-id');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') != label_id){
				if($(this).hasClass('active')){
					$(this).find('label').css('background-position', '-1px -4px');
					$(this).find('input').click();
					$(this).removeClass('active');
				}
			}
		});
		$('.user_payment_system input').each(function(){
			if($(this).prop("checked") == true){
				user_payment_system_div = '<div class="priem_inf_each"><div style="width:105px; margin:0px 10px; float:left;">Название ПС</div><input class="us_sys_name1" name="us_sys_name1" type="text"><br><div style="margin-top: 6px;"><span style="float: left;text-align: left;width: 105px;margin: 0px 10px;">Сумма USD</span><input type="text" class="hhhhhhh" autocomplete="off" name="payment_user" ></div></div>';
				//alert(user_payment_system_div);
				
			}
		});
		
		
		$('.payment_system li.active').each(function(){
			$(this).find('label').click();
		});
		
		$('.system_s div').html('');
		
		//////////////////
		if($ttttr == 1){
			$('.payment_system li.active').each(function(){
				//$(this).find('label').click();
				$(this).removeClass('active');
				$(this).find('label').css("background-position", "-1px -4px");
				$(this).find('input[type="checkbox"]').click();
			});
		}
		////////////////////
		$('.user_s div').html(user_payment_system_div);
	});
	
	$('.payment_system .user_system2').click(function(){
		gf = 1;
		$('#what_ps').val('user_ps');
		proverka_id = 0;
		proverka_active = 0;
		if($('.all_inf').find("input").prop("checked") == true){
			$('.payment_system li.active').each(function(){
				$(this).find("label").click();
			});
			
			if($(this).parent().find("input").prop("checked") == false){
				$(this).parent().find('label').css('background-position', '-1px -34px');
				$('.other_ps').addClass('backgr_fff');
				$('.other_ps').find('input').click();
				$(this).parent().find('label').addClass('active');
			}
			else{
				$(this).parent().find('label').css('background-position', '-1px -4px');
				$(this).parent().find('input').click();
				$('.other_ps').removeClass("backgr_fff");
				$(this).parent().find('label').removeClass('active');
			}
			if($(this).parent().find("input").prop("checked") == true){
				payment_system_div = '<div class="priem_inf_each" ><span style="float:left; margin-right: 7px; padding-top: 30px;">курс</span><input autocomplete="off" class="dddddd" style="width:50px; float:left;margin-top: 30px;" type="text" name="kyrs" ><div style="width:105px; margin:0px 10px; float:left;">Название ПС</div><input class="us_sys_name2" name="us_sys_name2" type="text"><br><div style="margin-top: 6px;"><span style="float: left;text-align: left;width: 105px;margin:0 10px;">Сумма USD</span><input type="text" class="cccccc" name="payment_system" ></div><div style="margin-top: 6px;"><span style="float: left;text-align: left;width: 105px;margin: 0 10px 0 113px;">Номер счета</span><input type="text" class="score_num" name="score" ></div></div>';
				$('.system_s div').html(payment_system_div);
			}
			else{
				payment_system_div = "";
				$('.system_s div').html(payment_system_div);
			}
		}
		else{
		
			$('#what_ps').val('user_st_ps');
			gf = 1;
			$('.vubor_garanta #garant3').click();
						$('.payment_system li.active').each(function(){
				$(this).find("label").click();
			});
			
			if($(this).parent().find("input").prop("checked") == false){
				$(this).parent().find('label').css('background-position', '-1px -34px');
				$('.other_ps').addClass('backgr_fff');
				$('.other_ps').find('input').click();
				$(this).parent().find('label').addClass('active');
			}
			else{
				$(this).parent().find('label').css('background-position', '-1px -4px');
				$(this).parent().find('input').click();
				$('.other_ps').removeClass("backgr_fff");
				$(this).parent().find('label').removeClass('active');
			}
			if($(this).parent().find("input").prop("checked") == true){
				payment_system_div = '<div class="priem_inf_each" ><span style="float:left; margin-right: 7px; padding-top: 30px;">курс</span><input autocomplete="off" class="dddddd" style="width:50px; float:left;margin-top: 30px;" type="text" name="kyrs" ><div style="width:105px; margin:0px 10px; float:left;">Название ПС</div><input class="us_sys_name2" name="us_sys_name2" type="text"><br><div style="margin-top: 6px;"><span style="float: left;text-align: left;width: 105px;margin:0 10px;">Сумма USD</span><input type="text" class="cccccc" name="payment_system" ></div><div style="margin-top: 6px;"><span style="float: left;text-align: left;width: 105px;margin: 0 10px 0 113px;">Номер счета</span><input type="text" class="score_num" name="score" ></div></div>';
				$('.system_s div').html(payment_system_div);
			}
			else{
				payment_system_div = "";
				$('.system_s div').html(payment_system_div);
			}

		}
	});
	
//=========================================================================	
	
	$('.user_payment_system label').not('.user_system').click(function(){
		if($('.other_ps input').prop("checked") == true){
			$('.payment_system .user_system2').click();
			$('.system_s div').html('');
		}
		if($('.all_inf').find('input').prop("checked") == true){
			$('.all_inf .user_system').click();
		}

		gf = 0;
		
		$ttttr = 0;
		data_checed = $(this).parent().find('input').prop("checked");
		if(data_checed == false){
			$(this).css('background-position', '-1px -34px');
			$(this).parent().find('input').click();
			$(this).parent().addClass('active');
		}
		else{
			$(this).css('background-position', '-1px -4px');
			$(this).parent().find('input').click();
			$(this).parent().removeClass('active');
			$ttttr = 1;
		}
		
		user_payment_system_div = "";
		label_id = $(this).parent().attr('data-id');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') != label_id){
				if($(this).hasClass('active')){
					$(this).find('label').css('background-position', '-1px -4px');
					$(this).find('input').click();
					$(this).removeClass('active');
				}
			}
		});
		$('.user_payment_system input').each(function(){
			if($(this).prop("checked") == true){
				img_system = $(this).parent().find('div').html();
				text_system = $(this).parent().find('div + span').text();
				payment_user_ = $(this).parent().attr('data-id');
				user_payment_system_div = '<div class="priem_inf_each"><div style="width:105px; margin:0px 10px; float:left;">'+img_system+'</div><input data-id="'+payment_user_+'" name="u_system_id['+payment_user_+']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input type="text" class="hhhhhhh" autocomplete="off" data-id="'+payment_user_+'" name="payment_user" id="payment_user_'+payment_user_+'"></div>';
				//alert(user_payment_system_div);
			}
		});
		/*                скрипт для ограничения отображения ПС               */
		/*id_prov = $(this).parent().attr('data-id');
		relations = relation_mass[id_prov];
		if(relations != ""){
			$('.payment_system li').each(function(){
				ffff = relations.indexOf(","+$(this).attr('data-id')+",");
				if(ffff == '-1'){
					$(this).removeClass('clicable');
					$(this).find('a').removeClass('clicable');
					$(this).find('label').removeClass('clicable');


					$(this).find('a').css("cursor", "default");
					$(this).css("opacity", "0.3");
				}
				else{
					$(this).addClass('clicable');
					$(this).find('a').addClass('clicable');
					$(this).find('label').addClass('clicable');
					$(this).css("opacity", "1");
					$(this).find('a').css("cursor", "pointer");
				}
			});
		}
		else{
			$('.payment_system li').each(function(){
				$(this).addClass('clicable');
				$(this).find('a').addClass('clicable');
				$(this).find('label').addClass('clicable');
				$(this).css("opacity", "1");
				$(this).find('a').css("cursor", "pointer");
			});
		}*/
		
		
		$('.payment_system li.active').each(function(){
			$(this).find('label').click();
		});
		
		
		
		$('.system_s div').html('');
		//////////////////
		if($ttttr == 1){
			$('.payment_system li.active').each(function(){
				//$(this).find('label').click();
				$(this).removeClass('active');
				$(this).find('label').css("background-position", "-1px -4px");
				$(this).find('input[type="checkbox"]').click();
			});
		}
		////////////////////
		$('.user_s div').html(user_payment_system_div);
	});
	/*--------- клик на системы оплаты общие ---------*/
	$('.payment_system label.clicable').not('.user_system2').click(function(){
		
		if($('.other_ps input').prop("checked") == true){
			$('.payment_system .user_system2').click();
			$('.system_s div').html('');
		}
		proverka_id = 0;
		proverka_active = 0;
		id_payment_system = $(this).parent().attr('data-id');
		name_payment_system = $(this).parent().find('span').text();
		$('.user_payment_system li').not('.g_f').each(function(){
			if(id_payment_system == $(this).attr('data-id')){proverka_id = 1;}
			if($(this).hasClass('active')){ proverka_active = 1; }
		});
		if($('.sdsdsdsd1').parent().find('input').prop("checked") == true){
			$('#what_ps').val('standart_user_ps');
			gf = 1;
		}
		else{
			$('#what_ps').val('satndart_ps');
			gf = 0;
		}
		if(proverka_active == 1){
		if(proverka_id == 1){
			data_checed = $(this).parent().find('input').prop("checked");
			if(data_checed == false){
				$(this).css('background-position', '-1px -34px');
				$(this).parent().find('input').click();
				$(this).parent().addClass('active');
			}
			else{
				$(this).css('background-position', '-1px -4px');
				$(this).parent().find('input').click();
				$(this).parent().removeClass('active');
			}
			payment_system_div = "";
			$('.payment_system input').each(function(){
				if($(this).prop("checked") == true){
					
					img_system = $(this).parent().find('div').html();
					text_system = $(this).parent().find('div + span').text();
					payment_user_ = $(this).parent().attr('data-id');
					kkk = 0;
					$('.system_s .priem_inf_each').each(function(){
					if($(this).attr('data-id') == payment_user_){ 
						kkk = 1;
						kurs_1 = $(this).find('.dddddd').val();
						mon_1 = $(this).find('.cccccc').val();
					}
					});
					
					if(kkk != 1){
						payment_system_div += '<div class="priem_inf_each" data-id="'+payment_user_+'"><label style="float:left; margin-right: 7px; padding-top: 2px;">курс</label><input autocomplete="off" class="dddddd" style="width:50px; float:left;" type="text" name="kyrs['+payment_user_+']" data-id="payment_user_'+payment_user_+'"><div style="width:105px; margin:0px 10px; float:left;">'+img_system+'</div><input data-id="'+payment_user_+'" name="system_id['+payment_user_+']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input style="border: 1px solid #ccc;" type="text" class="cccccc" name="payment_system['+payment_user_+']" data-id="payment_user_'+payment_user_+'"><div style="border-bottom: 1px dashed #ccc; width: 100%; height: 12px; margin-bottom: 20px; "></div></div>';
					//alert(payment_system_div);
					}
					else{
						payment_system_div += '<div class="priem_inf_each" data-id="'+payment_user_+'"><label style="float:left; margin-right: 7px; padding-top: 2px;">курс</label><input autocomplete="off" class="dddddd" value="'+kurs_1+'" style="width:50px; float:left;" type="text" name="kyrs['+payment_user_+']" data-id="payment_user_'+payment_user_+'"><div style="width:105px; margin:0px 10px; float:left;">'+img_system+'</div><input data-id="'+payment_user_+'" name="system_id['+payment_user_+']" type="checkbox" checked ><span style="float:left; text-align: left; padding-left: 0px; width: 80px;">Сумма USD</span><input style="border: 1px solid #ccc;" type="text" class="cccccc" value="'+mon_1+'" name="payment_system['+payment_user_+']" data-id="payment_user_'+payment_user_+'"><div style="border-bottom: 1px dashed #ccc; width: 100%; height: 8px; margin-bottom: 20px; "></div></div>';
					}
				}
			});
			$('.system_s div').html(payment_system_div);
		}
		else{
			$("#modal_success_btn").click();
			$("#text_span").html('<div style="margin-bottom:10px;">Добавление текущей систимы оплаты('+name_payment_system+')</div><form id="form_paym_syst" name="form_paym_syst" method="post" action=""><div><input type="hidden" name="opt" value="save_paym_sys" /><input type="hidden" name="system_id" value="'+id_payment_system+'" /><input type="text" id="score" name="score"> <input type="submit" value="Записать номер счета"></div></form>');
		}
		}
	});
	
	$('.user_s ').on('click', 'input[type="checkbox"]', function(){
		zzz = $(this).attr('data-id');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') == zzz){ $(this).find('label').click();}
		});
	});
	$('.system_s ').on('click', 'input[type="checkbox"]', function(){
		zzz = $(this).attr('data-id');
		$('.payment_system li').each(function(){
			if($(this).attr('data-id') == zzz){ $(this).find('label').click();}
		});
	});
	
	
	
	$('.user_s ').on('keyup keydown', '.hhhhhhh', function(){
		
		count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, '');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, '');
		Value_1 = $(this).val();
		Value_1 = Value_1.replace(",", '.');
		$('.system_s .dddddd').each(function(){
			ssss = $(this).attr('data-id');
			kurs_num = $(this).val();
			if(kurs_num != '' && Value_1 != ""){
				result = Value_1*kurs_num;
				$('.system_s .cccccc').each(function(){
					if(ssss == $(this).attr('data-id')){ 
						$(this).val(result);
						this.value = Math.round(parseFloat(this.value)*100)/100;
					}
				});
			}
			else{
				$('.system_s .cccccc').each(function(){
					if(ssss == $(this).attr('data-id')){ 
						$(this).val('');
					}
				});
			}
		});
		
	});
	$('.system_s ').on('focus', '.dddddd', function(){
		$(this).on('keyup keydown', function(){
		count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, '');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.,]/g, '');
			Value_1 = $('.user_s ').find('.hhhhhhh').val();
			Value_1=Value_1.replace(",", '.');
			kurs_num = $(this).val();
			kurs_num = kurs_num.replace(",", '.');
			eeee = $(this).attr('data-id');
			if(kurs_num != '' && Value_1 != ""){
				result = Value_1*kurs_num;
				$('.system_s .cccccc').each(function(){
					if(eeee == $(this).attr('data-id')){ 
						$(this).val(result);
						this.value = Math.round(parseFloat(this.value)*100)/100;						
					}
				});
			}
			else{
				$('.system_s .cccccc').each(function(){
					if(eeee == $(this).attr('data-id')){ 
						$(this).val(""); 
					}
				});
			}
		
		
		});
	});
	$('.system_s ').on('focus', '.cccccc', function(){
		$(this).on('keyup keydown', function(){
			count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, '');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.,]/g, '');
			Value_1 = $('.user_s ').find('.hhhhhhh').val();
			Value_1=Value_1.replace(",", '.');
			money2 = $(this).val();
			money2 = money2.replace(",", '.');
			eeee = $(this).attr('data-id');
			if(money2 != '' && Value_1 != ""){
				result = money2/Value_1;
				$('.system_s .dddddd').each(function(){
					if(eeee == $(this).attr('data-id')){ 
						$(this).val(result); 
					}
				});
			}
			else{
				$('.system_s .dddddd').each(function(){
					if(eeee == $(this).attr('data-id')){ 
						$(this).val(""); 
					}
				});
			}
		
		
		});
	});
});
</script>
{/literal}
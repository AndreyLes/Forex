{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}
{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#save_paym_sys'){
				$("#modal_success_btn").click();
				$("#text_span").text("Номер счета сохранен.");
				document.location.hash = "";
			}
		});
	</script>
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;padding-top: 10px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Обмен стандарт
	</h4>
	<div id='underline'>
		<span>
		
		</span>
	</div>
</div>
<div id="big_payment_system">
{literal}
<script>
	$('body').on('click', ".button_pay", function(){
		$(this).parent().find('form').submit();
	});
</script>
{/literal}
<div class='kab-block' style="position:relative; width:100%;">
	<header style="background-repeat: no-repeat; background-size: 127% 100%; width:100%;">
		<span style="float: left; margin-bottom: 10px;">Предложения на заявку {$number_aplication}</span>
		<div class="hint_standart" data-inf="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in." data-class="bottom"></div>
	</header>
	<div class="my-pamms" style="display: table; width: 100%;" >
		
		<table class="pamm-info" id="grid">
			<thead style="z-index: 100;">
				<tr>
					<td  style="position:relative" >Аватарка</td>
					<td  style="position:relative" >Имя:</td>
					<td >Фамилия:</td>
					<td >Страна</td>
					<td >Соц сети</td>
					<td>Действие</td>
				</tr>
			</thead>
			<tbody >
		{foreach from=$inf_stand_appl key=key item=result}
			<tr data-id="" class="zebra_table">
				<td><img src='/images/users/avatars/small/{if $result.infa.imageurl == ''}nopic.jpg{else}{$result.infa.imageurl}{/if}'></td>
				<td>{$result.inf_user.nickname}</td>
				<td class="td_li" >{$result.inf_user.first_name}</td>
				<td class="td_li" >{$result.infa.country}</td>
				<td>{$result.sotc_set}</td>
				<td class="td_buttonu">
					<a class="button_pay" >Обмен средств</a>
					<form style="display:none;" method='post' action='/users/{$login}'>
						<input type='hidden' name='opt' value='application_standart'>
						<input type='hidden' name='user_id' value='{$result.user_id_off}'>
						<input type='hidden' name='payment_id' value='{$number_aplication}'>
					</form>
				</td>
			</tr>
		{/foreach}
		</tbody>
		</table>
	</div>
</div>

{add_js file='includes/jquery/datepicker/jquery-ui.js'}
{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}

{*add_css file='includes/timer/tabs/tabs.css'*}


{literal}
<style>
#mainbody
{

	margin-top:25px;
	position:inherit;
}
#wrapper {
    position: inherit;
	}
</style>
	<script>
		$(document).ready(function(){
			
			var hash = document.location.hash;
			if(hash == '#no_application'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("У вас нет соответствующих заявок на обмен.");
				document.location.hash = "";
			}
			if(hash == '#application_change'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Извините, но эта заявка стала на обмен с другим пользователем.");
				document.location.hash = "";
			}
			if(hash == '#no_curs'){
				$("#modal_dif_kurs_btn").click();
				document.location.hash = "";
			}
			if(hash == '#obmen'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Обмен совершон успешно.");
				document.location.hash = "";
			}
			if(hash == '#error'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Неправильно введен код проверки!");
				document.location.hash = "";
			}
			if(hash == '#success_message'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text("Заявка на бирже обмена создана.");
				document.location.hash = "";
			}
			if(hash == '#dell_application'){
					$("#modal_success_all_btn").click();
					$("#modal_success_all #text_span").text("Заявка удалена.");
					document.location.hash = "";
				}
			if(hash == '#no_garant'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text('Не совпадает тип заявок!');
				document.location.hash = "";
			}
			if(hash == '#no_conditions'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text('Не совпадают условия обмена!');
				document.location.hash = "";
			}
			if(hash == '#zapros_standart'){
				$("#modal_success_all_btn").click();
				$("#modal_success_all #text_span").text('Вы подали заявку на обмен средств через внутренюю конвертацию. Ожидайте ответа владельца заявки.');
				document.location.hash = "";
			}
		
var grid = document.getElementById('grid');
 
    grid.onclick = function(e) {
      if (e.target.tagName != 'TH') return;

      // Если TH -- сортируем
	  sortGrid(e.target.cellIndex, e.target.getAttribute('data-type'), e.target.getAttribute('data-name'), e.target.getAttribute('data-click'));
	  
    };

    function sortGrid(colNum, type, name, click) {
	
	$('#grid thead th').each(function(){
		if($(this).attr("data-name") == name){
			if($(this).attr("data-click") == '1'){
				$(this).attr("data-click", "2");
			}
			else{$(this).attr("data-click", "1");}
		}
	});
	
      var tbody = grid.getElementsByTagName('tbody')[0];

      // Составить массив из TR
      var rowsArray = [].slice.call(tbody.rows);

      // определить функцию сравнения, в зависимости от типа
      var compare;

      switch (type) {
        case 'number':
			
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].innerHTML - rowA.cells[colNum].innerHTML;
			  };
			}
          break;
        case 'string':
		
			if(click == "1"){
			  compare = function(rowA, rowB) {
			  	return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML ? 1 : -1;
			  };
			}
          break;
		case 'masive':
			if(click == "1"){
			  compare = function(rowA, rowB) {
				return rowA.cells[colNum].getAttribute('data-title') - rowB.cells[colNum].getAttribute('data-title');
				
			  };
			}
			else{
			  compare = function(rowA, rowB) {
				return rowB.cells[colNum].getAttribute('data-title') - rowA.cells[colNum].getAttribute('data-title');
			  };
			}
		
		  break;
      }

      // сортировать
      rowsArray.sort(compare);

      // Убрать tbody из большого DOM документа для лучшей производительности
      grid.removeChild(tbody);

      // добавить результат в нужном порядке в TBODY
      // они автоматически будут убраны со старых мест и вставлены в правильном порядке
      for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
      }

      grid.appendChild(tbody);

    }
	
	/* удаление заявки */
	$('.zebra_table').on('click', '.button_del', function(){
		infa = $(this).parent().parent().find('.hiddenn_inf_del').html();
		
		//alert(infa);
		$('#modal_success2 #text_span').html(infa);
		$('#modal_success_btn2').click();
	});
	
	$('#modal_success2').on('click', '.otmenit', function(){
		$(this).parent().parent().parent().parent().parent().parent().parent().find('.close').click();
	});
	
$('.search_inf').on("keyup keydown", 'input[name="search_kurs"]', function(e){
	if(this.value.toString().search(/[^0-9.]/) != -1)
			this.value = this.value.toString().replace( /[^0-9.]/g, '');
});

/////////////////////////////////////////////////////////////////////
$('.border_top_none').on("keyup keydown", 'input[type="text"]', function(e){
	if(e.keyCode==13){
		window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).attr('data-records')+"&num=1&"+$(this).attr('name')+"="+$(this).val()+"";
	}
});
$("#search_garant").change(function(){
    if($(this).val() == "4"){
		window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).attr('data-records')+"&num=1";
	}
	else{
		window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).attr('data-records')+"&num=1&"+$(this).attr('name')+"="+$(this).val()+"";
	}
});
$("#search_part").change(function(){
    if($(this).val() == "3"){
		window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).attr('data-records')+"&num=1";
	}
	else{
		window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).attr('data-records')+"&num=1&"+$(this).attr('name')+"="+$(this).val()+"";
	}
});
$(".number_of_records").change(function(){
    window.location.href = "/users/"+$(this).attr('data-id')+"/exchange.html?r="+$(this).val()+"&num=1";
});	
//////////////////////////////////////////////////////////////////////
			$('.first_label').click(function(){
				data_checed = $(this).parent().find('input').prop("checked");
				if(data_checed == false){
					$(this).parent().find('#first_label').css('background-position', '-1px -34px');
					$(this).parent().find('input').click();
				}
				else{
					$(this).parent().find('#first_label').css('background-position', '-1px -4px');
					$(this).parent().find('input').click();
				}
			});
			$('.second_label').click(function(){
				data_checed = $(this).parent().find('input').prop("checked");
				if(data_checed == false){
					$(this).parent().find('#second_label').css('background-position', '-1px -34px');
					$(this).parent().find('input').click();
				}
				else{
					$(this).parent().find('#second_label').css('background-position', '-1px -4px');
					$(this).parent().find('input').click();
				}
			});
		
		
	$('.obr_timer').each(function(){
		var time = $(this).attr('data-time');
		$(this).text(time);
	});
	intervalID = setInterval(function() {
 
	 tiktak();	
	 
	 }, 60000);	
	 
	user_id = $('.number_of_records').attr('data-id');
	
	
		
});


function tiktak(){
var timer_off = 0;
	$('.obr_timer').not('.off').each(function(){
		timer_off++;
		var time = $(this).text();
		if(time == 1){
			$(this).addClass('off');
			application_ajax($(this).attr('data-id'));
		}
		else{
			time--;
			$(this).attr('data-time', time);
			$(this).text(time);
		}
	});
	if(timer_off == 0){
		clearInterval(intervalID);
	}
}
 

function application_ajax(id){
	jQuery.ajax({
		url:     '/components/users/ajax/change_application.php', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: {id_sust: id, user_id: user_id},
		success: function(response) {
			$('.obr_timer').each(function(){
				if(response == $(this).attr('data-id')){
					if($(this).parent().parent().attr('data-title') == '1'){
						$(this).parent().parent().find('.priem_ajax').html('<a class="button_change">Начать обмен</a>');
						$(this).parent().remove();
						
					}
					else{
						$(this).parent().parent().find('.priem_ajax').html('<a class="button_go_change this_zayavka_button" href="'+$(this).parent().parent().attr('data-href')+'" >Изменить</a><a class="button_del this_zayavka_button" >Убрать</a><a class="button_buy_serv this_zayavka_button" >Купить у сервиса</a>');
						$(this).parent().remove();
						
					}
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$('.obr_timer').each(function(){
				if(response == $(this).attr('data-id')){
				$(this).parent().parent().find('.priem_ajax').text("Ошибка при изменении статуса заявки");
				$(this).remove();
				}
			});
		}
	 });
}




function validation(){
		
		$('.design_valid p').css('border', '1px solid #fff');
		if($('#first').prop("checked") == false){ 
			$('#first_label').parent().css('border', '1px solid red');
			return false;
		}
		else{
			return true;
		}
	}
	$('.component').on('click', '.closes, .back_closes', function(){
		$('.isset_hesh').fadeOut();
		$('.back_closes').fadeOut();
	});
	

</script>
<div id="priem_style">

</div>
<!--div style="    height: 100px;" id="counter"></div>
<script>
$('#counter').countdown({
	image: "/includes/timer/img/digits.png",
    format: "mm:ss",
	startTime: "12:00"});
</script-->

{/literal}
<div id="modal_success2" class="modal hide fade" style='width: 600px; margin-left: -300px;'>
	<div class="modal-body" style='padding-top:15px; border: 5px solid #ccc; text-align: center; max-height: 600px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin:10px 0; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success2" role="button" data-toggle="modal" id='modal_success_btn2'></a>
<div id="modal_success_all" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success_all" role="button" data-toggle="modal" id='modal_success_all_btn'></a>
{* if $inf_visible}
<div style="z-index: 998; background: #000;opacity: 0.5; position: fixed; left: 0; top: 0; width: 100%; height: 100%;" class="back_closes"></div>
<div class="isset_hesh" >
<div class="closes">x</div>
	<div class="design_text">
		<p>заявка</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo scelerisque lacus, ac pretium arcu pellentesque in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vestibulum arcu et porta gravida. Morbi id elit neque. Etiam mauris dolor, interdum a augue vitae, consequat varius sem. Donec eu porttitor ipsum. Ut eget finibus lorem. Sed quis vehicula eros. Nam tristique eu augue ac porta. Praesent commodo luctus orci, eget sagittis mauris venenatis ac.</p>
	</div>
<form id="design" action="" method="post" onsubmit="return validation()">
	<input type="hidden" name="opt" value="click_zayavka">
	
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.user_id}">
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.id}">
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.id_system_minus}">
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.minus_money}">
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.id_system_plus}">
	<input type="hidden" name="inf_hidden_on[]" value="{$inf_hidden_on.plus_money}">
	
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.user_id}">
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.id}">
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.id_system_minus}">
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.minus_money}">
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.id_system_plus}">
	<input type="hidden" name="inf_hidden_off[]" value="{$inf_hidden_off.plus_money}">
	
	<ul style="list-style:none;">
		<li>Хочю получить средства через:</li>
		<li style="margin-bottom: 30px;">
			<div class="design_img"><img src="{$inf_visible.img}"></div>
			<div class="design_inf">
				<div style="padding-top: 10px;">{$inf_visible.name_sustem}: сумма USD - {$inf_visible.off_money}</div>
				<div><a class="href_editprofile" href="/users/{$id}/editprofile.html#payment_inf">Номер счета: {$inf_visible.score} <input type="button" style="margin-left: 10px; border: none; background-color: #D71411; color: #fff; font-weight: bold;" value="Редактировать"></a></div>
			</div>
		</li>
	</ul>
	<div class="design_valid">
		<p>
			<input style="float:left;" type="checkbox" id="first">
			<label id="first_label" class="first_label"></label>
			<label class="first_label" >Подтверждаю корректность указаных реквизитов.</label>
		</p>
		<p>
			<input style="float:left;" type="checkbox" name="second" id="second">
			<label id="second_label" class="second_label"></label>
			<label class="second_label" >Уведомить контрагента по СМС об оплате(стоимость 0.05 USD)</label>
		</p>
	</div>
	<div style="text-align: center;">
		<input style="text-transform: uppercase;" id="save_btn" type="submit" value="Подтвердить">
	</div>
</form>
</div>
{/if *}
<div class='kab-blockk' style="position:relative;width: 970px;">
	
	<header style="background-repeat: no-repeat; background-size: 100% 100%;">
		<span>Биржа обмена</span>
		<span style="font-size: 12px;">Количество записей на странице :</span>
		<select style="height: 21px;" data-id="{$id}" name="number_of_records" class="number_of_records" >
			<option data-id="{$id}" {if $records == '10'}selected{/if} value="10" name="number_of_records">10</option>
			<option data-id="{$id}" {if $records == '20'}selected{/if} value="20" name="number_of_records">20</option>
			<option data-id="{$id}" {if $records == '50'}selected{/if} value="50" name="number_of_records">50</option>
		</select>
		<a class="create_zayavk" href="/users/{$id}/payment_system.html">Создать заявку</a>
	</header>
	<div class="my-pamms" style="display: table; width: 100%;" >
		<table class="pamm-info " id="grid">
			<thead>
				<tr class="border_bottom_none">
					<th data-type="number" data-name="id" data-click="2" style="position:relative" >№ заявки ↑↓</th>
					<th data-type="string" data-name="have" data-click="1" style="position:relative" >Есть: ↑↓</th>
					<th data-type="masive" data-name="give" data-click="1" style="min-width: 220px;">Надо: ↑↓</th>
					<th data-type="masive" data-name="kurs" data-click="1">Курс / Обратный курс↑↓</th>
					<th data-type="string" data-name="country" data-click="1" style="position:relative" >Страна ↑↓</th>
					<!--td>BL</td>
					<td>Аттестат</td-->
					<th data-type="string" data-name="garant" data-click="1" style="position:relative">Гарант / Внутренняя конвертация ↑↓</th>
					<th data-type="masive" data-name="part" data-click="2" style="position:relative" >Продать частями ↑↓</th>
					<th data-type="masive" data-name="obmen" data-click="2">Начать обмен ↑↓</th>
				</tr>
				<tr class="border_top_none">
					<td>
						<input type="text" data-records="{$records}" data-id="{$id}" name="search_id" value="{$search_id}" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-records="{$records}" data-id="{$id}" name="search_have" value="{$search_have}" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-records="{$records}" data-id="{$id}" name="search_give" value="{$search_give}" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-records="{$records}" data-id="{$id}" name="search_kurs" value="{$search_kurs}" style="width:90%; height: 15px;">
					</td>
					<td>
						<input type="text" data-records="{$records}" data-id="{$id}" name="search_country" value="{$search_country}" style="width:90%; height: 15px;">
					</td>
					<!--td>
						<form method="post" action="" class="search_inf">
						<input type="hidden" name="opt" value="search">
						<input type="text" name="search_bl" style="width:90%; height: 15px;">
						</form>
					</td>
					<td>
						<form method="post" action="" class="search_inf">
						<input type="hidden" name="opt" value="search">
						<input type="text" name="search_atestat" style="width:90%; height: 15px;">
						</form>
					</td-->
					<td>
						<select style="height: 21px;" data-records="{$records}" data-id="{$id}" name="search_garant" id="search_garant" >
							<option data-records="{$records}" value="4" {if $search_garant}{else} selected{/if}>Фильтр</option>
							<option data-records="{$records}" data-id="{$id}" name="search_garant" {if $search_garant == 1}selected{/if} value="1">Гарант</option>
							<option data-records="{$records}" data-id="{$id}" name="search_garant" {if $search_garant == 2}selected{/if} value="2">Внутренняя конвертация</option>
							<option data-records="{$records}" data-id="{$id}" name="search_garant" {if $search_garant == 3}selected{/if} value="3">Гарантийный фонд</option>
						</select>
					</td>
					<td>
						<select style="height: 21px;" data-records="{$records}" data-id="{$id}" name="search_part" id="search_part" >
							<option data-records="{$records}" value="3" {if $search_part}{else} selected{/if}>Фильтр</option>
							<option data-records="{$records}" data-id="{$id}" name="search_part" {if $search_part == 1}selected{/if} value="1">+</option>
							<option data-records="{$records}" data-id="{$id}" name="search_part" {if $search_part == 2}selected{/if} value="2">-</option>
						</select>
					</td>
					<td><a class="sbros_filter" href="/users/{$id}/exchange.html?r={$records}&num=1">Сброс<br>фильтров</a></td>
				</tr>
			</thead>
			<tbody >
		{if $request_payments[0] != NULL}
		{foreach from=$request_payments key=key item=result}
				
				<tr class="{if $id == $result.user_id}this_user_application{/if} zebra_table hint_inf {*if $id == $result.user_id}this_zayavka{/if*}"  data-inf="{$result.div}" data-class="top" data-id="{$result.id}">
					<td>{$result.id}</td>
					<td>{$result.name_user_system}: {$result.money_off} USD</td>
					<td class="td_li zebra_fon1" data-title="{$result.masive|@count}">
						<ul style="list-style:none; padding: 0px;">
						{foreach from=$result.masive key=tid item=infa}
							<li class="zebra_liggg" data-id="{$tid}">
								<span class="name_paym_system_s">{$infa.name_paym_system}</span>: <span class="money_s">{$infa.money} USD </span>
							</li>
						{/foreach}
						</ul>
					</td>
					<td class="zebra_fon2" data-title="{$result.masive|@count}">
						<ul style="list-style:none; padding: 0px;">
						{foreach from=$result.masive key=tid item=infa}
							<li class="zebra_liggg" data-id="{$tid}">
								<span style="text-align: center;">{$infa.kurs}/{$infa.kurs_obr}</span>
							</li>
						{/foreach}
						</ul>
					</td>
					<td>{$result.country}</td>
					<!--td>/</td>
					<td>/</td-->
					<td>{if $result.garant == '1'}Гарант{elseif $result.garant == '2'}Внутренняя конвертация{elseif $result.garant == '3'}Гарантийный фонд{/if}</td>
					<td data-title="{$result.sell_parts}">{if $result.sell_parts == '1'}+{else}-{/if}</td>
					<td data-title="{if $id != $result.user_id}1{else}0{/if}" data-href="/users/{$result.login}#{$result.id}" class="td_buttonu">
						{if $result.status == '2'}
						<div class="clock_back" style="margin:auto;">
							<div class="fon_timer"></div>
							<div class="obr_timer" data-id="{$result.id}" data-time="{$result.time}"></div>
						</div>
						{else}
						{if $id != $result.user_id}
						<a class="button_change">Начать обмен</a>
						{else}
						<a class="button_go_change this_zayavka_button" href="/users/{$result.login}#{$result.id}" >Изменить</a>
						<a class="button_del this_zayavka_button" >Убрать</a>
						{if $result.garant == '1' || $result.garant == '3'}<a data-id="{$result.id}" class="button_buy_serv this_zayavka_button" >Купить у сервиса</a>{/if}
						{/if}
						{/if}
						<div class="priem_ajax"></div>
						<div class="hiddenn_inf_del">
							<form action="" method="post" >
							<input type="hidden" name="opt" value="del_application">
							<input type="hidden" name="id_application" value="{$result.id}">
							<span>Вы точно хотите удалить заявку № {$result.id} ?<span>
							<p style="margin-top: 10px;">
								<input type="submit" value="Удалить"> <input class="otmenit" type="button" value="Отменить"> 
							</p>
							</form>
						</div>
						<div class="hidden_information">
							<form action="" method="post" class="form_change">
							<div class="transmission_input">
								<input type="hidden" name="opt" value="request_change">
							<input type="hidden" name="garant" value="{if $result.garant == '1'}1{elseif $result.garant == '2'}2{elseif $result.garant == '3'}3{/if}">
							<input type="hidden" name="sell_parts" value="{if $result.sell_parts == '1'}+{else}-{/if}">
							<input type="hidden" name="idshka" value="{$result.id}">
							<input type="hidden" name="payment_system_id" value="{$result.name_user_system}">
							<input type="hidden" class="hhhhhhh" name="user_money" value="{$result.money_off}">
							{foreach from=$result.masive key=tid item=infa}
								<input type="hidden" name="name_paym_system[]" value="{$infa.name_paym_system}">
								<input data-id="{$tid}" class="dddddd" type="hidden" name="kurs_s[]" value="{$infa.kurs_obr}">
								<input type="hidden" data-id="{$tid}" class="cccccc" name="money[]" value="{$infa.money}" >
								<input type="hidden" name="score_s[]" value="{$infa.score}">
							{/foreach}
							</div>
							<input id="{$result.id}" type="submit" value="Подтвердить">
							</form>
						</div>
					</td>
				</tr>
			
			
		{/foreach}
		{/if}	
			</tbody>
		</table>
		<span style="float:left; margin-top: 15px; padding-right: 5px;">Количество записей на странице :</span>
		<select style=" height: 21px;float: left; margin-top: 15px;" data-id="{$id}" name="number_of_records" class="number_of_records" >
			<option data-id="{$id}" {if $records == '10'}selected{/if} value="10" name="number_of_records">10</option>
			<option data-id="{$id}" {if $records == '20'}selected{/if} value="20" name="number_of_records">20</option>
			<option data-id="{$id}" {if $records == '50'}selected{/if} value="50" name="number_of_records">50</option>
		</select>
		<div style="margin-top: 15px; font-size: 15px; width: 800px;" class="href_pagination">
			{$href_pagination}
		</div>
		<div style="text-align: center;padding-top: 30px;"><a class="see_all_exchange" href="/users/{$id}/exchange.html">Показать все предложения</a></div>
	</div>
</div>
{literal}
<script>
//$(document).ready(function(){
	$('.zebra_fon1').each(function(){
		jjjg = 0;
		$(this).find('ul li').each(function(){
			jjjg++;
		});
		if(jjjg == '1'){
			$(this).find('li').css("background","none");
		}
	});
	$('.zebra_fon2').each(function(){
		jjjg = 0;
		$(this).find('ul li').each(function(){
			jjjg++;
		});
		if(jjjg == '1'){
			$(this).find('li').css("background","none");
		}
	});
//});
</script>
{/literal}
{if $kurs_user_off}
{literal}
<script>
	
	
$(document).ready(function(){
	/*$('.hint_inf').mouseover(function(){
		$(this).find('tr').css("border-borrom", "none");
	});
	$('.hint_inf').mouseout(function(){
		$(this).find('tr').css("border-borrom", "1px solid black"); 
	});
	*/
	id_zayavki = $('#modal_diferense_kurs input[name="obmen"]').val();
	$('.kab-blockk tbody tr').each(function(){
		if($(this).attr("data-id") == id_zayavki){
			html_input = $(this).find('.hidden_information .transmission_input').html();
		}
	});
	//alert(html_input);
	$('#modal_diferense_kurs').find('.priem_input').html(html_input);
	$('#modal_dif_kurs_btn').click();
});
</script>
{/literal}
<div id="modal_diferense_kurs" class="modal hide fade" style='width: 600px; margin-left: -300px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center; max-height: 600px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block;  font-size: 15px;' id="text_span">
		<p>Разные курсы! Если Вы желаете произвести обмен, можете изменить свой курс или дождаться подходящей заявки.</p>
		<p><label>Встречный курс: </label><label id="vctrechnuy">{$kurs_user_off}</label></p>
		<p><label>Ваш курс: </label><label>{$kurs_user_on}</label></p>
		<form method="post" action="" onsubmit="return valid_change_curs()">
		<input type="hidden" name="opt2" value="change_kurs">
		<input type="hidden" name="id_zayavki" value="{$id_zamov}">
		<input type="hidden" name="obmen" value="{$obmen}">
		<input type="hidden" name="num_in_cycle" value="{$num_in_cycle}">
		<p><input type="text" name="change_kurs" id="change_kurs" value="{$kurs_user_on}"></p>
		<div class="priem_input"></div>
		<div style='display:table'>
			<span style='margin-right: -35px; width:190px; float:left;'>Введите код :</span>
		{captcha}
		</div>
		<input style="float: left; margin: 10px 15px 0px 122px;" type="submit" value="Изменить и обменять">
		<a id="history_back_href" href="/users/{$id}/exchange.html">Назад</a>
		
		</form>
		</span>
	</div>
</div>
<a href="#modal_diferense_kurs" role="button" data-toggle="modal" id='modal_dif_kurs_btn'></a>
{/if}
<div class="hidden_information_2">
	<form id="otpravka_to_servise" action="/users/{$id}/service.html" method="post">
		<input type="hidden" name="opt" value="inf_service">
		<input type="hidden" name="id_zapusa" value="">
	</form>
</div>
{literal}
<script>
$(document).ready(function(){
	$('.button_buy_serv').click(function(){
		$('#otpravka_to_servise').find('input[name="id_zapusa"]').val($(this).attr('data-id'));
		$('#otpravka_to_servise').submit();
	});
});
$('#modal_diferense_kurs2').on('click', "#to_exchange", function(){
	click_obmen = $(this).attr("data-id");
	$('.kab-blockk tbody').each(function(){
		if($(this).attr("data-id") == click_obmen){
			$(this).find('.button_change').click();
		}
	});
});
$('#modal_diferense_kurs').on('keyup keydown', '#change_kurs', function(){
	
		if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, '');
	
});
function valid_change_curs(){
	$('#change_kurs').css("border", "2px inset");
	$('input[name="code"]').css("border", "2px inset");
	zamena_komu = $('#vctrechnuy').text().replace(",", '.');
	//alert($('#vctrechnuy').text()+"=?"+$('#change_kurs').val());
	if($('#change_kurs').val() == ""){ 
		$('#change_kurs').css("border", "1px solid red");
		return false;
	}
	if($('#change_kurs').val() != $('#vctrechnuy').text() ){
		if($('#change_kurs').val() != zamena_komu){
			$('#change_kurs').css("border", "1px solid red");
			return false;
		}
	}
	if($('input[name="code"]').val() == ""){
		$('input[name="code"]').css("border", "1px solid red"); return false;
	}
	
}
$('.zebra_table').on('click', '.button_change', function(){
	infa = $(this).parent().parent().find('.hidden_information input[type="submit"]').click();
});
/*
<div id="modal_diferense_kurs2" class="modal hide fade" style='width: 600px; margin-left: -300px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center; max-height: 600px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block;  font-size: 15px;' id="text_span">
		<form action="" method="post" class="form_change" onsubmit="return validateForm()">
			<div class="priem_input"></div>
			<p>Для подтверждения обмена введите код с картинки:</p>
			<div style='display:table'>
				{captcha}
			</div>
			<input id="{$result.id}" type="submit" value="Обменять">
		</form>
		</span>
	</div>
</div>
<a href="#modal_diferense_kurs2" role="button" data-toggle="modal" id='modal_dif_kurs_btn2'></a>



$('.button_change').click(function(){
	infa = $(this).parent().find('.transmission_input').html();
	$('#modal_diferense_kurs2 .priem_input').html(infa);
	$('#modal_dif_kurs_btn2').click();
});
function validateForm(){
	$('#modal_diferense_kurs2 input[name="code"]').css("border", "2px inset");
	if($('#modal_diferense_kurs2 input[name="code"]').val() == ""){
		$('#modal_diferense_kurs2 input[name="code"]').css("border", "1px inset red");
		return false;
	}	
}*/
</script>
{/literal}
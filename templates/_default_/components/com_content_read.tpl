{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#limit'){
				$("#modal_success_btn").click();
				$("#text_span").text("Cумма средств превышает лимит обмена.");
				document.location.hash = "";
			}
		});
	</script>
{/literal}
{if $article.id != 37}
{if $article.showtitle}
    <h1 class="con_heading">{$article.title}</h1>
{/if}

{if $is_pages}
	<div class="con_pt" id="pt">
		<span class="con_pt_heading">
			<a class="con_pt_hidelink" href="javascript:void;" onClick="{literal}$('#pt_list').toggle();{/literal}">{$LANG.CONTENT}</a>
			{if $cfg.pt_hide} [<a href="javascript:void(0);" onclick="{literal}$('#pt').hide();{/literal}">{$LANG.HIDE}</a>] {/if}
		</span>
		<div id="pt_list" style="{if $cfg.pt_disp}display: block;{else}display: none;{/if} width:100%">
			<div>
				<ul id="con_pt_list">
				{foreach key=tid item=pages from=$pt_pages}
					{if ($tid+1 != $page)}
						{math equation="x + 1" x=$tid assign="key"}
						<li><a href="{$pages.url}">{$pages.title}</a></li>
					{else}
						<li>{$pages.title}</li>
					{/if}
				{/foreach}
				<ul>
			</div>
		</div>
	</div>
{/if}

<div class="con_text" style="overflow:hidden">
    {if $article.image}
        <div class="con_image" style="float:left;margin-top:10px;margin-right:20px;margin-bottom:20px">
            <img src="/images/photos/medium/{$article.image}" alt="{$article.title|escape:html}"/>
        </div>
    {/if}
    {$article.content}
</div>


{else}


{add_js file='includes/jquery/datepicker/jquery-ui.js'}
{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}
{add_js file='components/users/js/profile.js'}
{add_js file='includes/jquery/jquery.maskedinput.js'}

{literal}
	<script>
		$(document).ready(function(){
			var hash = document.location.hash;
			if(hash == '#application_service'){
				$("#modal_success_btn").click();
				$("#text_span").html("<p>Заявка создана.</p><p>На ваш електронный адрес было отослано сообщение</p>");
				document.location.hash = "";
			}
			if(hash == '#min_money'){
				$("#modal_success_btn").click();
				$("#text_span").text("Нарушено условие минимальной суммы.");
				document.location.hash = "";
			}
			if(hash == '#critical_error'){
				$("#modal_success_btn").click();
				$("#text_span").text("Вы пытаетесь обмануть систему перевода средств. При повторном обмане Ваш аккаунт будет заблокирован.");
				document.location.hash = "";
			}
			$("#your_phone").mask("999-999-99-99?");
		});
	</script>
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style=' border: 5px solid #ccc; text-align: center;padding-top: 10px;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Обмен через сервис
	</h4>
	<div id='underline'>
		<span>
		
		</span>
	</div>
</div>
<div id="big_payment_system">
{literal}
<script>
	
</script>
{/literal}
<!-- Платежные данные -->
<div style=" border: 1px solid black; margin-top: 20px; width: 99.8%;" class='kab-block'>
	<div class="user_payment_system">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю</span>
		<ul>
		{foreach key=tid item=system from=$payments}
			<li data-inf="0" data-score="{$system.score}" data-id="{$system.id}" data-before="{$system.before_score}" data-number="{$system.number_score}" data-after="{$system.after_score}" data-relations="{$system.relations}" data-tax="{$system.tax_money}">
				<div style="width:105px; margin:0px 10px; float:left;"><img class="clicable_img" data-id='{$system.id}' style="float:left;" src="{$system.img}"></div>
				<span style="float:left; text-align: left; padding-left: 0px; width: 105px;">{$system.name}</span>
				<label  style="margin-left: 50px;"></label>
				<input type="checkbox" >
				<a style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
		{/foreach}
		</ul>
	</div>
	
	<div class="payment_system">
		<span style="width:100%; display: block; float: none; padding:10px;">Получаю</span>
		<ul>
		{foreach key=tid item=system from=$payments}
			<li data-inf="0" data-score="{$system.score}" data-id="{$system.id}" data-before="{$system.before_score}" data-number="{$system.number_score}" data-after="{$system.after_score}" data-tax="{$system.tax_money}">
			
				<div style="width:105px; margin:0px 10px; float:left;"><img data-id='{$system.id}' class="clicable_img" style="float:left;" src="{$system.img}"></div>
				<span style="float:left; width: 105px;">{$system.name}</span>
				<label class="clicable" style="margin-left: 50px;"></label>
				<input type="checkbox" >
				<span class="fffggg" style="display:none;">{$system.money_admin}</span>
			<a class="clicable" style="position: relative; display:block; width:100%; height:100%;"></a>
			</li>
		{/foreach}
		</ul>
	</div>
	<!--div class="reserve">
	</div-->
</div>
<form method="post" action="/info/design-standart.html" onsubmit="return validateForm()">
<div style="display:none;" data-id="{$id}" class="verification">{$verification}</div>
<input type="hidden" name="opt" value="design_standart">
<div class="priem_system" >
	<div class="user_s" style="width: 40%;">
		<span style="width:100%; display: block; float: none; padding:10px;">Отдаю:</span>
		<div>
		
		</div>
	</div>
	<div class="system_s" style="width: 59%;">
		<span style="width:100%; display: block; float: none; padding:10px;">Получаю:</span>
		<div>
		
		</div>
	</div>
</div>

<div class="priem_system user_infa" style="margin-top:20px;">
	<div style="padding: 10px;">
		<span >Персональные данные:</span>
	</div>
	<div class="personal_inf">
		<span style="margin-right: 24px;">Ваше имя:</span>
		<input type="text" name="user_name" value="{$infa_user.nickname}">
	</div>
	<div class="personal_inf">
		<span>Ваш E-mail:</span>
		<input type="text" name="user_email" value="{$infa_user.email}">
	</div>
	<div style="width: 100%; padding: 10px;">
		<span>Ваш телефон:</span>
		<select id="format" name="your_phone_code" style="font-size:14px; height: 24px; width: 70px; border-radius: 0;  padding-left: 5px;">
			<option value="+(38)" {if $infa_user.code_phone == '+(38)'} selected{/if}>+(38)</option>
			<option value="+(37)" {if $infa_user.code_phone == '+(37)'} selected{/if}>+(37)</option>
			<option value="+(29)" {if $infa_user.code_phone == '+(29)'} selected{/if}>+(29)</option>
		</select>
		<input style="width:115px;" type="text" name="user_phone" id="your_phone" value="{$infa_user.phone}">
	</div>
</div>
<div class="commission">
	<div>
		<span>Комиссия сервиса(%):</span>
		<input type="text" name="commission" value="0.5" readonly>
	</div>
</div>
<div style="text-align: center;">
	<input style="text-transform: uppercase;padding-right: 40px;" id="save_btn" type="submit" value="обменять">
	<input style="color: #fff;text-transform: uppercase; padding-left: 50px;" type="button" id="delbtn2" value="очистить данные">
</div>
</form>

</div>
<div class="transfer_funds" data-server="{$server_name}" data-system1="{$sys_1}" data-system2="{$sys_2}"></div>

<div class='kab-blockk' id="kab_blockk_s" style="position:relative;     margin-top: 30px;">
	
	<header style="background-repeat: no-repeat; background-size: 100% 100%;">
		<span>Топ 10 предложений на "БИРЖЕ ОБМЕНА"</span>
		{if $id_user}
		<a style="width: 195px;" class="create_zayavk" href="/users/{$id_user}/exchange.html">Показать все</a>
		{else}
		<a style="width: 195px;" class="create_zayavk" href="/#modal" role="button" data-toggle="modal">Показать все</a>
		{/if}
	</header>
	<div class="my-pamms" style="display: table; width: 100%;" >
		<table class="pamm-info " id="grid">
			<thead>
				<tr class="border_bottom_none">
					<th data-type="number" data-name="id" data-click="2" style="position:relative" >№ заявки</th>
					<th data-type="string" data-name="have" data-click="1" style="position:relative" >Есть:</th>
					<th data-type="masive" data-name="give" data-click="1" style="min-width: 220px;">Надо:</th>
					<th data-type="masive" data-name="kurs" data-click="1">Курс / Обратный курс</th>
					<!--th data-type="string" data-name="country" data-click="1" style="position:relative" >Страна </th-->
					<!--td>BL</td>
					<td>Аттестат</td-->
					<th data-type="string" data-name="garant" data-click="1" style="position:relative">Гарант / Внутренняя конвертация</th>
					<!--th data-type="masive" data-name="part" data-click="2" style="position:relative" >Продать частями </th-->
					<th data-type="masive" data-name="obmen" data-click="2">Начать обмен</th>
				</tr>
			</thead>
			<tbody >
			
		{foreach from=$request_payments key=key item=result}
			
				<tr class="zebra_table {if $result.go_logins == '1'} grey_backgr {/if} {if $id_user} hint_inf {/if} {*if $id == $result.user_id}this_zayavka{/if*}" {if $id_user} data-inf="{$result.div}" data-class="top" {/if} data-id="{$result.id}">
					<td>{$result.id}</td>
					<td>{$result.name_user_system}: {$result.money_off} USD</td>
					<td class="td_li" data-title="{$result.masive|@count}">
						<ul style="list-style:none; padding: 0px;">
						{foreach from=$result.masive key=tid item=infa}
							<li class="" data-id="{$tid}">
								<span class="name_paym_system_s">{$infa.name_paym_system}</span>: <span class="money_s">{$infa.money} USD </span>
							</li>
						{/foreach}
						</ul>
					</td>
					<td data-title="{$result.masive|@count}">
						<ul style="list-style:none; padding: 0px;">
						{foreach from=$result.masive key=tid item=infa}
							<li  data-id="{$tid}"><!--class="zebra_li"-->
								<span style="text-align: center;">{$infa.kurs}/{$infa.kurs_obr}</span>
							</li>
						{/foreach}
						</ul>
					</td>
					<!--td>{$result.country}</td-->
					<!--td>/</td>
					<td>/</td-->
					<td>{if $result.garant == '1'}Гарант{else}Внутренняя конвертация{/if}</td>
					<!--td data-title="{$result.sell_parts}">{if $result.sell_parts == '1'}+{else}-{/if}</td-->
					<td data-title="{if $id != $result.user_id}1{else}0{/if}" data-href="/users/{$result.login}#{$result.id}" class="td_buttonu">
<!--=========================================таймери відключені==========================================-->
						{* if $result.status == '2'}
							<div class="clock_back" style="margin:auto;">
								<div class="fon_timer"></div>
								<div class="obr_timer" data-id="{$result.id}" data-time="{$result.time}"></div>
							</div>
						{/if *}
						{if $id_user}
							<a class="go_login" href="/users/{$id_user}/exchange.html?search_id={$result.id}"><span>Перейти </span<br>на биржу</a>
						{else}
							<a class="go_logins" data-id="{$result.id}" href="/#modal" role="button" data-toggle="modal">Вход/ Регистрация</a>
						{/if}
						<div class="priem_ajax"></div>
						<div class="hidden_information">
						</div>
					</td>
				</tr>
			
			
		{/foreach}
			
			</tbody>
		</table>
	</div>
</div>

{literal}
<style>
	.personal_inf {
		float: left;
		padding: 10px;
		width: 47%;
	}
	.personal_inf span {
	    margin-right: 10px;
	}
	.personal_inf input {
	    padding-left: 3px;
	}
	.commission span {
		width: 160px;
		display: block;
		float: left;
		padding-top: 12px;
	}
	.priem_inf_each img {
		margin-top: -7px;
		height: 30px;
		max-width: 80px;
	}
	.priem_inf_each {
		height: 90px;
		margin: 15px 0px;
	}
	.dddddd {
		width: 35px;
		float: left;
		margin-top: 5px;
		padding: 0;
	}
	.commission div {
		width: 54%;
		height: 50px;
	}
	.priem_inf_each input[type="checkbox"] {
		position: relative;
		left: -20px;
		top: -17px;
	}
	.ccccccss {
		margin-top: 2px;
	}
</style>
<script>

$(document).ready(function(){

	$('.zebra_table').click(function(){
		$('.kab-blockk .zebra_table.grey_backgr').each(function(){
			$(this).removeClass('grey_backgr');
		});
	});

	$('.obr_timer').each(function(){
		var time = $(this).attr('data-time');
		$(this).text(time);
	});
	intervalID = setInterval(function() {
 
	tiktak();	
	 
	}, 60000);
	 
});

function tiktak(){
var timer_off = 0;
	$('.obr_timer').not('.off').each(function(){
		timer_off++;
		var time = $(this).text();
		if(time == 1){
			$(this).addClass('off');
			application_ajax($(this).attr('data-id'));
		}
		else{
			time--;
			$(this).attr('data-time', time);
			$(this).text(time);
		}
	});
	if(timer_off == 0){
		clearInterval(intervalID);
	}
}
 

function application_ajax(id){
	jQuery.ajax({
		url:     '/components/users/ajax/change_application.php', //Адрес подгружаемой страницы
		type:     "POST", //Тип запроса
		dataType: "html", //Тип данных
		data: {id_sust: id, user_id: user_id},
		success: function(response) {
			$('.obr_timer').each(function(){
				if(response == $(this).attr('data-id')){
					if($(this).parent().parent().attr('data-title') == '1'){
						$(this).parent().parent().find('.priem_ajax').html('<a class="button_change">Начать обмен</a>');
						$(this).parent().remove();
						
					}
					else{
						$(this).parent().parent().find('.priem_ajax').html('<a class="button_go_change this_zayavka_button" href="'+$(this).parent().parent().attr('data-href')+'" >Изменить</a><a class="button_del this_zayavka_button" >Убрать</a><a class="button_buy_serv this_zayavka_button" >Купить у сервиса</a>');
						$(this).parent().remove();
						
					}
				}
			});
		},
		error: function(response) {  
		//Если ошибка
		$('.obr_timer').each(function(){
				if(response == $(this).attr('data-id')){
				$(this).parent().parent().find('.priem_ajax').text("Ошибка при изменении статуса заявки");
				$(this).remove();
				}
			});
		}
	 });
}

function validateForm(){
	var foo;
	var your_mail = $('.priem_system input[name="user_email"]').val();
	var atpos=your_mail.indexOf("@");
	var dotpos=your_mail.lastIndexOf(".");
	if($('.user_s .priem_inf_each').html() == foo && $('.system_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите платежные системы!");
		return false;
	}
	else if($('.user_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Выберите систему выплаты!");
		return false;
	}
	else if($('.system_s .priem_inf_each').html() == foo){
		$("#modal_success_btn").click();
		$("#text_span").text("Заполните необходимые поля!");
		return false;
	}
	else if($('.priem_system input[name="user_email"]').val() == "" || $('.priem_system input[name="user_name"]').val() == "" || $('#your_phone').val() == ""){
		$("#modal_success_btn").click();
		$("#text_span").text("Заполните необходимые поля!");
		return false;
	}
	else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=your_mail.length){
		$("#modal_success_btn").click();
		$("#text_span").text("Неправильно записан E-mail!");
		return false;
	}
	else{
		proverka = 0;
		$('.dddddd').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		
		$('.ccccccss').each(function(){
			if($(this).val() == ""){ proverka = 1;}
		});
		$('.cccccc').each(function(){
			if($(this).val() == ""){ proverka = 1; }
		});
		if($('.hhhhhhhsss').val == "" || $('.hhhhhhh').val() == "" || $('.hhhhhhh').val() == "0" || proverka == 1){
			$("#modal_success_btn").click();
			$("#text_span").text("Заполните необходимые поля!");
			return false;
		}
	}
}


	
$(document).ready(function(){
	
	tax_money_mass = [];
	relation_mass2 = [];
	{/literal}
	{foreach from=$inf_relation item=result key=tid}
	{literal}relation_mass2[{/literal}{$tid}{literal}] = [];
	tax_money_mass[{/literal}{$tid}{literal}] = [];{/literal}
		{foreach from=$result item=result2 key=tid2}
			{literal}relation_mass2[{/literal}{$tid}{literal}][{/literal}{$tid2}{literal}] = '{/literal}{$result2.min_money}{literal}';
			tax_money_mass[{/literal}{$tid}{literal}][{/literal}{$tid2}{literal}] = '{/literal}{$result2.kurs}{literal}';{/literal}
		{/foreach}
	{/foreach}
	{literal}
	
	$('#delbtn2').click(function(){
		$('.user_payment_system li.active').each(function(){
			$(this).find('label').click();
		});
		$('.user_infa .personal_inf').each(function(){
			$(this).find('input').val('');
		});
		
		//$('.priem_system .personal_inf').find('input[name="user_email"]').val('');
	});
	
	varification = $('.verification').text();
	$('.verification').text('');
	/*--------- клик на системы оплаты юзера ---------*/
	$('.user_payment_system a').click(function(){
		$(this).parent().find('label').click();
	});
	$('.payment_system a.clicable').click(function(){
		$(this).parent().find('label.clicable').click();
	});
	$('.user_payment_system label').click(function(){
		$ttttr = 0;
		$('.payment_system li').each(function(){
			$(this).css("display", "block");
		});
		data_checed = $(this).parent().find('input').prop("checked");
		if(data_checed == false){
			$(this).css('background-position', '-1px -34px');
			$(this).parent().find('input').click();
			$(this).parent().addClass('active');
		}
		else{
			$(this).css('background-position', '-1px -4px');
			$(this).parent().find('input').click();
			$(this).parent().removeClass('active');
			$ttttr = 1;
		}
		
		user_payment_system_div = "";
		label_id = $(this).parent().attr('data-id');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') != label_id){
				if($(this).hasClass('active')){
					$(this).find('label').css('background-position', '-1px -4px');
					$(this).find('input').click();
					$(this).removeClass('active');
				}
			}
		});
		bbb = 0;
		$('.user_payment_system input').each(function(){
			if($(this).prop("checked") == true){
				text_before = $(this).parent().attr('data-before');
				before = '';
				if(text_before){
					before = text_before;
				}
				text_after = $(this).parent().attr('data-after');
				after = '';
				if(text_after){
					after = text_after;
				}
				num_scores = $(this).parent().attr('data-number');
				var score_number = $(this).parent().attr('data-score');
				img_system = $(this).parent().find('div').html();
				text_system = $(this).parent().find('div + span').text();
				payment_user_ = $(this).parent().attr('data-id');
				user_payment_system_div = '<div class="priem_inf_each"><div style="width: 80px; margin:0px 10px; float:left;">'+img_system+'</div><input data-id="'+payment_user_+'" name="u_system_id['+payment_user_+']" type="checkbox" checked ><div style="width: 270px;display: block;margin: 10px 0 10px 10px;"> <div><span style="float:left; text-align: left; padding-left: 0px; width: 100px;">Сумма USD</span><input type="text" style="width: 110px;" class="hhhhhhh" autocomplete="off" data-id="'+payment_user_+'" name="payment_user" id="payment_user_'+payment_user_+'"></div><div><div style="width:100px; float:left; margin-top:3px;"><span style="float:left;text-align: left; padding-left: 0px; width: 80px;">Кошелек</span></div><input value="'+score_number+'" data-number="'+num_scores+'" data-before="'+before+'" data-after="'+after+'" type="text" style="width: 110px;" class="hhhhhhhsss" autocomplete="off" data-id="'+payment_user_+'" name="score_user" id="score_user_'+payment_user_+'"></div></div><input type="hidden" name="payment_system_user" value="'+payment_user_+'"></div>';
				//alert(user_payment_system_div);
				bbb = 1;
			}
		});
		/*                скрипт для ограничения отображения ПС (из БД) при обмене валют              */
		relations = $(this).parent().parent().parent().find('.active').attr('data-relations');
		if(relations && relations != "" && bbb == 1){
			$('.payment_system li').each(function(){
				ffff = relations.indexOf(","+$(this).attr('data-id')+",");
				if(ffff == '-1'){
					$(this).css("display", "none");
					$(this).removeClass('clicable');
					$(this).find('a').removeClass('clicable');
					$(this).find('label').removeClass('clicable');


					$(this).find('a').css("cursor", "default");
					$(this).css("opacity", "0.3");
				}
				else{
					$(this).css("display", "block");
					$(this).addClass('clicable');
					$(this).find('a').addClass('clicable');
					$(this).find('label').addClass('clicable');
					$(this).css("opacity", "1");
					$(this).find('a').css("cursor", "pointer");
				}
			});
		}
		else{
			$('.payment_system li').each(function(){
				$(this).addClass('clicable');
				$(this).find('a').addClass('clicable');
				$(this).find('label').addClass('clicable');
				$(this).css("opacity", "1");
				$(this).find('a').css("cursor", "pointer");
			});
		}
		
		
		$('.payment_system li.active').each(function(){
			$(this).find('label').click();
		});
		$('.system_s div').html('');
		//////////////////
		if($ttttr == 1){
			$('.payment_system li.active').each(function(){
				//$(this).find('label').click();
				$(this).removeClass('active');
				$(this).find('label').css("background-position", "-1px -4px");
				$(this).find('input[type="checkbox"]').click();
			});
		}
		////////////////////
		$('.user_s div').html(user_payment_system_div);
		
			length_number = $('.user_s').find('.hhhhhhhsss').attr('data-number');
			ghghg = "";
			for(i=0; i<length_number;i++){
				ghghg += "9";
			}
			$('.user_s .hhhhhhhsss').attr("placeholder", $('.user_s .hhhhhhhsss').attr('data-before')+ghghg+$('.user_s .hhhhhhhsss').attr('data-after'));
			$('.user_s .hhhhhhhsss').mask($('.user_s .hhhhhhhsss').attr('data-before')+ghghg+$('.user_s .hhhhhhhsss').attr('data-after'));
		
	});
	/*--------- клик на системы оплаты общие ---------*/
	$('.payment_system label.clicable').click(function(){
		proverka_id = 0;
		proverka_active = 0;
		//math_random = $(this).parent().attr('data-tax');
		id_payment_system = $(this).parent().attr('data-id');
		name_payment_system = $(this).parent().find('span').text();
		$('.user_payment_system li').each(function(){
			if(id_payment_system == $(this).attr('data-id')){proverka_id = 1;}
			if($(this).hasClass('active')){ proverka_active = 1;}
		});
		if(proverka_active == 1){
		if(proverka_id == 1){
			data_checed = $(this).parent().find('input').prop("checked");
			if(data_checed == false){
				$(this).css('background-position', '-1px -34px');
				$(this).parent().find('input').click();
				$(this).parent().addClass('active');
			}
			else{
				$(this).css('background-position', '-1px -4px');
				$(this).parent().find('input').click();
				$(this).parent().removeClass('active');
			}
			payment_system_div = "";
			// -------------- ограничения для выбора одной ПС --------------
			/*label_id = $(this).parent().attr('data-id');
			$('.payment_system li').each(function(){
				if($(this).attr('data-id') != label_id){
					if($(this).hasClass('active')){
						$(this).find('label').css('background-position', '-1px -4px');
						$(this).find('input').click();
						$(this).removeClass('active');
					}
				}
			});*/
			//
			$('.payment_system input').each(function(){
				if($(this).prop("checked") == true){
					text_before2 = $(this).parent().attr('data-before');
					before2 = '';
					var money_admin = $(this).parent().find('.fffggg').text();
					if(text_before2){
						before2 = text_before2;
					}
					text_after2 = $(this).parent().attr('data-after');
					after2 = '';
					if(text_after2){
						after2 = text_after2;
					}
					num_scores2 = $(this).parent().attr('data-number');
					var score_number2 = $(this).parent().attr('data-score');
					img_system = $(this).parent().find('div').html();
					text_system = $(this).parent().find('div + span').text();
					payment_user_ = $(this).parent().attr('data-id');
					kkk = 0;
					$('.system_s .priem_inf_each').each(function(){
						if($(this).attr('data-id') == payment_user_){ 
							kkk = 1;
							kurs_1 = $(this).find('.dddddd').val();
							mon_1 = $(this).find('.cccccc').val();
						}
					});
					var opopo = $('.user_s input[name="payment_system_user"]').val();
					if(tax_money_mass[opopo][$(this).parent().attr('data-id')]){
						math_random = tax_money_mass[opopo][$(this).parent().attr('data-id')];
					}
					else{
						math_random = '0';
					}
			//  общая комиссия
					kommission = parseFloat(0.5) + parseFloat(math_random);
					kommission = parseFloat(kommission).toFixed(2);
			// можна считать комиссию если 1 ПС
					//$('.commission input[name="commission"]').val(kommission);
					
					value_money = '';
					if($('.user_s .priem_inf_each') && $('.user_s .priem_inf_each').find('.hhhhhhh').val() != '' ){
						Value_1 = $('.user_s .priem_inf_each').find('.hhhhhhh').val();
						Value_1 = Value_1.replace(",", '.');
						procent_standart = Value_1*0.005;
						value_money = Value_1 - (Value_1*math_random*0.01);
						oll_summ = value_money - procent_standart;
			// можна считать деньги если 1 ПС
						//$('.commission input[name="all_money"]').val(oll_summ);
					}
					
					var min_summ = '';
					if(relation_mass2[opopo][$(this).parent().attr('data-id')]){
						min_summ = '<span style="padding-left:238px;" class="min_money">Минимальная сумма:<br>'+relation_mass2[opopo][$(this).parent().attr('data-id')]+' USD</span>'
						//alert(min_summ);
					}
					if(kkk != 1){
						payment_system_div += '<div style="height:135px" class="priem_inf_each" data-id="'+payment_user_+'"><label style="float:left; margin-right: 7px;">комиссия<br>на вывод(%)</label><input autocomplete="off" class="dddddd" style="padding:0; width:40px; float:left;margin-top: 5px;" type="text" name="kyrs['+payment_user_+']" data-id="payment_user_'+payment_user_+'" value="'+math_random+'"  readonly><div style="width:80px; margin:0px 10px; float:left;">'+img_system+'</div><input style="left: -150px;" data-id="'+payment_user_+'" name="system_id['+payment_user_+']" type="checkbox" checked ><span class="admin_money2">лимит: '+money_admin+' USD</span>'+min_summ+'<div style="width: 270px;display: block;margin: 10px 0 10px 135px;" ><div><span style="float:left; text-align: left; padding-left: 0px; width: 100px;">Сумма USD</span><input style="border: 1px solid #ccc;width: 120px;" type="text" class="cccccc" name="payment_system['+payment_user_+']" data-id="payment_user_'+payment_user_+'" readonly value="'+value_money+'"></div><div><div style="width:100px; float:left;margin-top: 3px;" ><span style="float:left;text-align: left; padding-left: 0px; width: 70px;">Кошелек</span></div><input value="'+score_number2+'" data-number="'+num_scores2+'" data-before="'+before2+'" data-after="'+after2+'" style="border: 1px solid #ccc;width: 120px;" type="text" class="ccccccss" name="score['+payment_user_+']" data-id="score_'+payment_user_+'"></div></div><input type="hidden" name="payment_system_admin" value="'+payment_user_+'"><div style="border-bottom: 1px dashed #ccc; width: 100%;  " ></div></div>';
					}
					else{
						payment_system_div += '<div style="height:135px" class="priem_inf_each" data-id="'+payment_user_+'"><label style="float:left; margin-right: 7px;">комиссия<br>на вывод(%)</label><input autocomplete="off" class="dddddd" value="'+kurs_1+'" style="padding:0; width:40px; float:left;margin-top: 5px;" type="text" name="kyrs['+payment_user_+']" data-id="payment_user_'+payment_user_+'"  readonly><div style="width:80px; margin:0px 10px; float:left;">'+img_system+'</div><input style="left: -150px;" data-id="'+payment_user_+'" name="system_id['+payment_user_+']" type="checkbox" checked ><span class="admin_money2" >лимит: '+money_admin+' USD</span>'+min_summ+'<div style="width: 270px;display: block;margin: 10px 0 10px 135px;" ><div><span style="float:left; text-align: left; padding-left: 0px; width: 100px;">Сумма USD</span><input style="border: 1px solid #ccc;width: 120px;" type="text" class="cccccc" value="'+mon_1+'" name="payment_system['+payment_user_+']" data-id="payment_user_'+payment_user_+'"  readonly ></div><div><div style="width:100px; float:left; margin-top: 3px;"><span style="float:left;text-align: left; padding-left: 0px; width: 70px;">Кошелек</span></div><input value="'+score_number2+'" data-number="'+num_scores2+'" data-before="'+before2+'" data-after="'+after2+'" style="border: 1px solid #ccc;width: 120px;" type="text" class="ccccccss" name="score['+payment_user_+']" data-id="score_'+payment_user_+'"></div></div><div style="border-bottom: 1px dashed #ccc; width: 100%; "></div></div>';
					}
				}
			});
			
			$('.system_s div').html(payment_system_div);
		}
		else{
			$("#modal_success_btn").click();
			$("#text_span").html('<div style="margin-bottom:10px;">Добавление текущей систимы оплаты('+name_payment_system+')</div><form id="form_paym_syst" name="form_paym_syst" method="post" action=""><div><input type="hidden" name="opt" value="save_paym_sys" /><input type="hidden" name="system_id" value="'+id_payment_system+'" /><input type="text" id="score" name="score"> <input type="submit" value="Записать номер счета"></div></form>');
		}
		}
		$('.system_s').find('.ccccccss').each(function(){
			length_number = $(this).attr('data-number');
			ghghg = "";
			for(i=0; i<length_number;i++){
				ghghg += "9";
			}
			$(this).attr("placeholder", $(this).attr('data-before')+ghghg+$(this).attr('data-after'));
			$(this).mask($(this).attr('data-before')+ghghg+$(this).attr('data-after'));
		});
	});
	
	$('.user_s ').on('click', 'input[type="checkbox"], .clicable_img', function(){
		zzz = $(this).attr('data-id');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') == zzz){ $(this).find('label').click();}
		});
	});
	$('.system_s ').on('click', 'input[type="checkbox"], .clicable_img', function(){
		zzz = $(this).attr('data-id');
		$('.payment_system li').each(function(){
			if($(this).attr('data-id') == zzz){ $(this).find('label').click();}
		});
	});
	
	
	/*$('.user_s ').on('keyup keydown', '.hhhhhhhsss', function(e){
		num_score = $(this).attr('data-number');
		
		if(this.value.toString().search(/[^0-9]/) != -1)
			this.value = this.value.toString().replace( /[^0-9]/g, '');
		if(this.value.length > (num_score - 1)){
		//alert(e.keyCode);
			if ( e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 39 || e.keyCode == 37 || e.keyCode == 9) {
					}
			else{event.preventDefault();}
			
		}
		Value_1 = $(this).val();
	});
	
	$('.system_s ').on('keyup keydown', '.ccccccss', function(e){
		num_score = $(this).attr('data-number');
		
		if(this.value.toString().search(/[^0-9]/) != -1)
			this.value = this.value.toString().replace( /[^0-9]/g, '');
		if(this.value.length > (num_score - 1)){
		//alert(e.keyCode);
			if ( e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 39 || e.keyCode == 37 || e.keyCode == 9) {
					}
			else{event.preventDefault();}
			
		}
		Value_1 = $(this).val();
	});*/
	
	$('.user_s ').on('keyup keydown', '.hhhhhhh', function(){
		
		count = 0;
		pos = $(this).val().indexOf(".");
		while ( pos != -1 ) {
		   count++;
		   pos = $(this).val().indexOf(".",pos+1);
		}
		pos2 = $(this).val().indexOf(",");
		while ( pos2 != -1 ) {
		   count++;
		   pos2 = $(this).val().indexOf(",",pos2+1);
		}
		
		if(count > 1 && this.value.toString().search(/[.,]/) != -1){
			this.value = this.value.toString().replace( /[.,]/, '');
		}
		else if(this.value.toString().search(/[^0-9.,]/) != -1)
		this.value = this.value.toString().replace( /[^0-9.,]/g, '');
		Value_1 = $(this).val();
		Value_1 = Value_1.replace(",", '.');
		$('.system_s .dddddd').each(function(){
			ssss = $(this).attr('data-id');
			kurs_num = $(this).val();
			if(kurs_num != '' && Value_1 != ""){
				procent_standart = Value_1*0.005;
				result = Value_1 - (Value_1*kurs_num*0.01);
				oll_summ = result - procent_standart;
				oll_summ = parseFloat(oll_summ).toFixed(2);
//$('.commission input[name="all_money"]').val(oll_summ);
				$('.system_s .cccccc').each(function(){
					if(ssss == $(this).attr('data-id')){ 
						$(this).val(result);
						this.value = Math.round(parseFloat(this.value)*100)/100;
					}
				});
			}
			else{
				$('.system_s .cccccc').each(function(){
					if(ssss == $(this).attr('data-id')){ 
						$(this).val('');
					}
				});
			}
		});
		
	});
	
	if($('.transfer_funds').attr('data-system1') != "" && $('.transfer_funds').attr('data-system2') != ""){
		var syst_1 = $('.transfer_funds').attr('data-system1');
		var syst_2 = $('.transfer_funds').attr('data-system2');
		$('.user_payment_system li').each(function(){
			if($(this).attr('data-id') == syst_1){
				$(this).find('a').click();
			}
		});
		$('.payment_system li').each(function(){
			if($(this).attr('data-id') == syst_2){
				$(this).find('a').click();
			}
		});
	}
	$('body').on('click', ".go_logins", function(){
		$('#go_logins').val($(this).attr('data-id'));
	});
		
});
</script>
{/literal}

{if $zayavka}
<div class="inf_birza" data-id="{$zayavka.id_syst_usr}" data-mon="{$zayavka.money_off}">
{foreach key=tid item=infa from=$zayavka.massive}
<div data-id="{$infa.id_sys}" data-sum="{$infa.money_sys}"></div>
{/foreach}
</div>
{literal}
<script>
	$(document).ready(function(){
		$('.user_payment_system li').each(function(){
			if($('.inf_birza').attr('data-id') == $(this).attr('data-id')){
				$(this).find('a').click();
			}
		});
		
		$('.hhhhhhh').val($('.inf_birza').attr('data-mon'));
		
		$('.inf_birza div').each(function(){
			id_ee = $(this).attr('data-id');
			$('.payment_system li').each(function(){
				if($(this).attr('data-id') == id_ee){
					$(this).find('a').click();
				}
			});
		});
		
		$('.inf_birza div').each(function(){
			money_ee = $(this).attr('data-sum');
			id_ee = $(this).attr('data-id');
			$('.system_s .priem_inf_each').each(function(){
				if($(this).attr('data-id') == id_ee){
					$(this).find('.cccccc').val(money_ee);
				}
			});
		});

	});
</script>
{/literal}
{/if}
<div id="modal" class="modal hide fade" style='width: 557px;'>
			<div class="modal-header">
				<button tabindex="6" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form action="/login" method="post" name="authform" style="margin:0px" target="_self" id="authform">
					<table class="authtable" width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td width=""><input placeholder="логин" autofocus name="login" placeholder='{$LANG.AUTH_LOGIN}' type="text" id="login" tabindex="1"/>
							<input type="hidden" name="opt" value="about_us">
							<input type="hidden" id="go_logins" name="go_logins" value="">
							</td>
											  
									<td>                            
							
												<a href="/registration" id='reg' tabindex="3"><span>Регистрация</span></a>
										 
										</td>			
						</tr>
						<tr>
								<td valign="top"><input placeholder="пароль" name="pass" placeholder='{$LANG.AUTH_PASS}' type="password" id="pass" tabindex="2"/></td>
							 <td width="13%" align="right"><input id="login_btn" type="submit" name="Submit" value="{$LANG.AUTH_ENTER}" tabindex="4"/></td>
								
						</tr>
						<tr>
						  <td width="87%">                            
											{if $cfg.passrem}
												<a href="/passremind.html" tabindex="5">{$LANG.AUTH_FORGOT}</a>
											{/if}
						  </td>
						</tr>
					 </table>
				</form>
			</div>
		</div>

{/if}
{add_js file='templates/_default_/js/styleswitch.js'}
<div class="con_heading">{$pagetitle}</div>
{$LANG.REG_INFO}
{$LANG.ZAPOLNY}
{if $cfg.is_on}

    {*if $cfg.reg_type == 'invite' && !$correct_invite}

        <p style="margin-bottom:15px; font-size: 14px">{$LANG.INVITES_ONLY}</p>

        <form id="regform" name="regform" method="post" action="/registration">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><strong>{$LANG.INVITE_CODE}:</strong></td>
                <td style="padding-left:15px">
                    <input type="text" name="invite_code" class="text-input" value="" style="width:300px"/>
                </td>
                <td style="padding-left:5px">
                    <input type="submit" name="show_invite" value="{$LANG.SHOW_INVITE}" />
                </td>
            </tr>
        </table>
        </form>

    {else*}

        {add_js file='components/registration/js/check.js'}

        <form id="regform" name="regform" method="post" action="/registration/add">
            <input type="hidden" name="csrf_token" value="{csrf_token}" />
            <table  border="0" align="center" cellpadding="5" cellspacing="0">
	      {if $cfg.name_mode == 'nickname'}
                    <tr>
                        <td valign="top" class="" width="269">
                            <div><strong>{$LANG.NICKNAME}:</strong></div>
                            <small>{$LANG.NICKNAME_TEXT}</small>
                        </td>
                        <td valign="top" class="">
                            <input name="nickname" id="nickinput" class="text-input" type="text" style="width:300px" value="{$item.nickname|escape:'html'}" />
                            <span class="regstar">*</span>
                        </td>
                    </tr>
                {else}
				
				    <tr>
						<td valign="top" class="">
                            <div><strong>{$LANG.NAME1}<span class="regstar">*</span>:</strong></div>
                        </td>
                        <td valign="top" class="">
                            <input name="realname1" id="realname1" class="text-input" type="text" style="width:212px" value="{$item.realname1|escape:'html'}" />
                          
                        </td>
						
                    </tr>
					     <tr>
                        <td valign="top" class="">
                            <div><strong>{$LANG.SURNAME}  <span class="regstar">*</span>:</strong></div>
                        </td>
                        <td valign="top" class="">
                            <input name="realname2" id="realname2" class="text-input" type="text" style="width:212px" value="{$item.realname2|escape:'html'}" />
                       
                        </td>
                    </tr>
                {/if}
                <tr>
                    <td width="530" valign="top" class="">
                        <div><strong>{$LANG.LOGIN}  <span class="regstar">*</span>:</strong></div>
                        
                    </td>
                    <td valign="top" class="">
                        <input name="login" id="logininput" class="text-input" type="text" style="width:212px" value="{$item.login|escape:'html'}" onchange="" autocomplete="off"/>
						<input id='check' type='button' value='Проверить' onclick='checkLogin()' />
                      
                        <div id="logincheck"></div>
                    </td>
                </tr>
          
               
                <tr>
                    <td valign="top" class=""><strong>{$LANG.PASS}   <span class="regstar">*</span>:</strong></td>
                    <td valign="top" class="">
                        <input name="pass" id="pass1input" class="text-input" type="password" style="width:212px" onchange="$('#passcheck').html('');"/>
                     
                    </td>
                </tr>
                <tr>
                    <td valign="top" class=""><strong>{$LANG.REPEAT_PASS}   <span class="regstar">*</span>: </strong></td>
                    <td valign="top" class="">
                        <input name="pass2" id="pass2input" class="text-input" type="password" style="width:212px" onchange="checkPasswords()" />
                  
                        <div id="passcheck"></div>
                    </td>
                </tr>
				{if $cfg.ask_city}
                    <tr>
                        <td valign="top" class=""><strong>{$LANG.CITY}:</strong></td>
                        <td valign="top" class="" >
                            {city_input value=$item.city name="city" width="212px"}
                        </td>
                    </tr>
                {/if}
				
				
                    
       
                    <tr>
                        <td valign="top" class=""><strong>Моб. телефон:</strong></td>
						
                        <td valign="top" class="">
                            <input name="mobtelephone" type="text" class="text-input" id="mobtelephone" value="{$item.mobtelephone|escape:'html'}" style="width:212px"/>
                        </td>
                    </tr>
					<tr>
						<td valign="top" class="">
							<div><strong>{$LANG.EMAIL}     <span class="regstar">*</span>:</strong></div>
						   
						</td>
						
						<td valign="top" class="">
							<input name="email" type="text" class="text-input" style="width:212px" value="{$item.email}"/>
					   
						</td>
					</tr>
					<tr>
                        <td valign="top" class=""><strong>Цветовая схема</strong></td>
						
                        <td valign="" class="">
                            <ul id="stylesheets">
								<li>
									<a id='dark' href="javascript:switchStylestyle('dark');">
										<div style="background:#f30000"></div>
										<input type='checkbox' id='checkbox-id4' value='v1' name='check1'/>
										<label for="checkbox-id4"></label>
									</a>
								</li>
								<li>
									<a id='blue' href="javascript:switchStylestyle('blue');">
										<div style="background:#0039FF"></div>
										<input type='checkbox' id='checkbox-id5' value='v1' name='check2'/>
										<label for="checkbox-id5"></label>
									</a>
								</li>
								<li>
									<a id='green' href="javascript:switchStylestyle('green');">
										<div style="background:#388E0F"></div>
										<input type='checkbox' id='checkbox-id6' value='v1' name='check3'/>
										<label for="checkbox-id6"></label>
									</a>
								</li>
							</ul>
                        </td>
                    </tr>
			{if $user}	<tr>
						 <td valign="top" class="">
							 <div><strong>{$LANG.USER_REF}</strong></div>
							</td>
						<td>
							{$user}{if $online}<span> ({$online})</span>{/if}
						
						</td>
					
					</tr>
			{/if}
				<tr><td></td><td></td></tr>
				<tr><td colspan='2' style='text-align:center;'>{$LANG.TABLE_TEXT}</td></tr>
				<tr style='	border-bottom:1px dashed #000;'><td></td><td></td></tr>
               
				<tr><td>
				<span class='sogl' style='font-weight:bold;color:#f30000'>{$LANG.SOGL}</span>
			
					<ol style='margin-top:20px'>
						<li><input type='checkbox' id='checkbox-id1' value='v1' name='check1'/><label for="checkbox-id1">{$LANG.RISK}</label></li>
						<li><input type='checkbox' id='checkbox-id2' value='v2' name='check2'/><label for="checkbox-id2">{$LANG.DOGOVOR}</label></li>
						<li><input type='checkbox' id='checkbox-id3' value='v3' name='check3'/><label for="checkbox-id3">{$LANG.REGLAMENT}</label></li>
				
					</ol></td>
					
					<td>
						<ul style='margin-top: 37px;list-style: none;float: left;margin-right: 85px;padding-left: 0;'>
							<li><input class='download' type='button' value='Скачать' /></li>
							<li><input class='download' type='button' value='Скачать' /></li>
							<li><input class='download' type='button' value='Скачать' /></li>
						</ul>
						<ul style='margin-top: 37px;list-style:none'>
						
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							<li><a href="#modal" role="button" data-toggle="modal" title="Инвестиционный кабинет" class='open_reg' type='button' >Открыть</a></li>
							
						</ul>
					</td>
				</tr>
				<tr><td></td><td></td></tr>
					<tr style='	border-bottom:1px dashed #000;'><td></td><td></td></tr>
				 <tr >
                    <td  valign="top" class="bortop">
                        <div style='width:250px'><strong>{$LANG.SECUR_SPAM}: </strong></div>
                    </td>
                    <td valign="top" class="">{captcha}</td>
                </tr>
                <tr>
                    <td valign="top" class="">&nbsp;</td>
                    <td valign="top" class="">
                        <input name="do" type="hidden" value="register" />
                        <input name="save" class="classname" type="submit" id="save" value="{$LANG.REGISTRATION}" />
                    </td>
                </tr>
            </table>
        </form>

    {*/if*}

{else}

    <div style="margin-top:10px">{$cfg.offmsg}</div>

{/if}
{literal}
<script>
	$(document).ready(function(){
		var c = '{/literal}{$cooke_param}{literal}';
		if (c){
			$('#stylesheets li a#'+c).find("input[type='checkbox']").prop("checked", true);
			$('#stylesheets li a#'+c).find("input[type='checkbox']").attr('disabled','disabled');
		}
	});
	$(function(){
		$('#stylesheets li a').click(function(){
		//alert('111111');
		if($(this).find("input[type='checkbox']").prop('checked') != true){
			$(this).find("input[type='checkbox']").trigger('click');
			switchStylestyle($(this).attr("id"));
			$(this).find("input[type='checkbox']").prop("disabled",true);
			$('#stylesheets').find("input[type='checkbox']").not($(this).find("input[type='checkbox']")).each(function(){
				$(this).prop("disabled",false);
				if($(this).prop("checked") == true) {
					$(this).prop("checked", false);
				}
			});
			}
		});
	});
</script>
{/literal}


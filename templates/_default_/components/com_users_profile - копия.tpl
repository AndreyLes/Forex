{* ================================================================================ *}
{* ========================= Профиль пользователя ================================= *}
{* ================================================================================ *}
{literal}
	<script>
		/*$(document).ready(function(){
			url = "http://fx-trend.com/pamm_indexes/";
			//text = $(url).find(".csn_message_list li").text();
			var mypage = $.ajax('http://fx-trend.com/pamm_indexes/');
			mypage.done(function (data) {
				$(data).find('.csn_message_list li').each(function(){
					res+=$(this).text()+'<br/>';
				})
				('#underline').html(res);
			});
			
			//alert(url_text);
		});*/
	</script>
{/literal}


<div id="modal1" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 400px;
	
border: 5px solid #ccc;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='margin-top: 10px;
display: inline-block;'>
<span style='display: inline-block;margin-top: 140px;margin-left: 26px;font-size: 20px;'>Данный раздел находиться в стадии разработки.</span>
	</div>
</div>
{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_js file="components/users/js/profile.js"}
{add_css file='includes/jquery/tabs/tabs.css'}
<h4 style='margin:0;padding-left:40px;'>
	Мои счета
</h4>
<div id='underline'>
	<span>
	
	</span>
</div>
<div id='section-div'>
	<section>
		<img src='/images/alarm.png'/> <span>- все операции завершены </span> 
	</section>
	<section>
		<img src='/images/alarm-red.png'/> <span>- все операции завершены </span> 
	</section>
</div>
<!-- Лицевой счет -->

<div class='kab-block'>
	<header>
		<span>{$expenses.main.expense_title}</span>
	</header>
	<div class='first'>
		<div>
		<span>
			№{$expenses.main.id}
		</span>
		</div>
		<div>
		<img src='/images/alarm.png'/><a href=''><span style='color:#030303'>История</span></a>
		</div>
	</div>
	<div class='second'>
		<table class='lych_table' style=''>
			<tr>
				<td><span style='float:left'>Валюта:</span>
					<span class='dotted'></span>
				</td>
				<td>
					{$expenses.main.currency}
				</td>
			</tr>
				<tr>
				<td>
					Общий бонус:
				</td>
				<td>
				{$expenses.main.expense_money}
				</td>
			</tr>
				<tr>
				<td><!--span style='float:left'>
					Бонусы:</span><span class='dotted'></span-->
					
				</td>
				<td>
				
				</td>
			</tr>
		</table>
	
		<ul class='lyc_schet'>
			<li><a href='#modal1' role="button" data-toggle="modal">Операции со счетами</a></li>
			<li><a href='#modal1' role="button" data-toggle="modal">Вывести средства</a></li>
			<!--li><a href=''>Управление бонусами</a></li-->
		</ul>
	
		<a href='/users/{$ids}/investglav.html' role="button" data-toggle="modal" class='button_box'><span>Пополнить СЧЕТ</span></a>
	
		
	</div>
</div>

<!-- Инвестиционный счет -->
{*foreach key=tid item=pam from=$pamms_related*}
<div class='kab-block'>
	<header>
		<span>{$expenses.invest.expense_title}</span>
	</header>
	<div class='first'>
		<div>
		<span>
			№{$expenses.invest.id}
		</span>
		</div>
		<div>
		<img src='/images/alarm.png'/><a href=''><span style='color:#030303'>История</span></a>
		</div>
	</div>
	<div class='second'>
	<table class='lych_table' style=''>
		<tr>
			<td><span style='float:left'>Инвестировано:</span>
				<span class='dotted1'></span>
			</td>
			<td>
				{$expenses.invest.currency}
			</td>
		</tr>
			<tr>
		<td><span style='float:left'>Выведено:</span>
				<span class='dotted2'></span>
			</td>
			<td>
				{$expenses.invest.expense_outpay}
			</td>
		</tr>
			<tr>
			<td><span style='float:left'>
				Начислено:</span><span class='dotted2'></span>
				
			</td>
			<td>
				{$expenses.invest.expense_inpay}
			</td>
		</tr>
				<tr>
			<td><span style='float:left'>
				Всего на счету:</span><span class='dotted1'></span>
				
			</td>
			<td>
				{$expenses.invest.expense_money}
			</td>
		</tr>
	</table>
	
	<ul class='lyc_schet1' style='margin-left:170px;'>
		<li class='schet' style='list-style:none;'><a href='/users/{$ids}/outpay.html'>Перевести на лицевой счет</a></li>
	</ul>
		<a href='/users/{$ids}/invest.html' style='margin-top:99px' class='button_box'><span>Пополнить СЧЕТ</span></a>
	</div>
</div>
{*/foreach*}
<!-- Демонстрационный счет -->
<div class='kab-block'>
	<header>
		<span>Демонстрационный счет</span>
	</header>
	
	<div class='demo'>
		<div></div><span>У Вас пока нет демонстрационного счета.</span>
		<a href='#modal1' role="button" data-toggle="modal" style='margin-top: 5px; margin-bottom: 5px;' class='button_box'><span>Создать СЧЕТ</span></a>
	</div>
</div>
<div class='kab-block'>
	<header>
		<span>Мои ПАММ - портфели</span>
	</header>
	
	<!-- вывод памм портфелей юзера -->
	{foreach key=tid item=pamm from=$user_pamms}
	
	<div class='pamms_div'>
		<div id='name_div'>
			<a href='/users/{$is_auth}/pamm.html'> <img src='{$pamm.pamm_info.image_url}'/>
			<span>{$pamm.pamm_info.title}</span>
			</a>
		</div>
		
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td style='width:135px;border:1px solid #CCCCCC;border-top: 0;'>
					<table style='width:100%'>
						<tr><td style='height: 60px;'><span id='invest_nymber'>№{$pamm.pamm_info.id}</span></td></tr>
						<tr><td style='background:#F5F5F5;height:64px;'> <object type="application/x-shockwave-flash" data="/includes/cabinet-menu-4.swf" height="40" style="visibility: visible;margin-left: 9px;" width='45'><param name="wmode" value="opaque" /></object><a class='invest_A' href=''><span style=''>История</span></a></td></tr>
					</table>
				</td>
				
				<td style='width:180px;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<p style='text-align: center;'>Прибыль проекта:</p>
					<table style='margin-left: 10px;'>
						<tr>
							<td>
								<span class='pamm_span'>недельная:</span>
								<span class='dotted2'></span>
							</td>
							<td class="red_td">
								{$pamm.pamm_info.week}%
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>месячная:</span>
								<span class='dotted2'></span>
							</td>
							<td class="orange_td">
								{$pamm.pamm_info.month}%
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>общая:</span>
								<span class='dotted2'></span>	
							</td>
							<td class="green_td">
								{$pamm.pamm_info.general}%
							</td>
						</tr>
					</table>
				</td>
				
				<td style='width:180px;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<p style='text-align: center;'>Информация по счету:</p>
					<table style='margin-left: 10px;'>
						<tr>
							<td>
								<span class='pamm_span'>Валюта:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								{$pamm.pamm_info.currency}
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>Общий баланс:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								{$pamm.pamm_info.money}
							</td>
						</tr>
						<tr>
							<td>
								<span class='pamm_span'>Доходность:</span>
								<span class='dotted2'></span>
							</td>
							<td>
								{$pamm.pamm_info.income}
							</td>
						</tr>
					</table>
				</td>
				<td style='border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>
					<!--a href='/users/{$ids}/outpay.html' style='color:#000;padding-left:34px' class='money'>Вывести средства</a-->
					<a href='#modal1' role="button" data-toggle="modal" style='color:#000;padding-left:34px' class='money'>Вывести средства</a>
					<a href='/users/{$ids}/invest_pamm.html' style='font-weight:normal; position:relative; top:0px;' class='button_box'><span>Пополнить СЧЕТ</span></a>
				</td>
			</tr>
			<tr>
				<td colspan='4' style='text-align:center;background:#CCCCCC;border-right:1px solid #CCCCCC;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC'>Дата окончания инвестиционного периода <span class="end_date">{$pamm.pamm_info.end_date}</span></td>
			</tr>
		</table>
	
	</div>
	{/foreach}
</div>
<div class='kab-block'>
	<header>
		<span>Рейтинг ПАММ - портфелей</span>
	</header>
	
	<!-- вывод всех памм портфелей -->
	{foreach key=tid item=pamm from=$all_pamms}
	<div class="sort_pamms">
		<span>Сортировать</span>
		<div><a>Тип торговли</a></div>
		<div><a>Доходность</a></div>
		<div><a>Брокер</a></div>
	</div>
	<div class='pamms_div'>
		<div id='name_div'>
			<a href='/users/{$is_auth}/pamm.html'> <img src='{$pamm.image_url}'/>
			<span>{$pamm.title}</span>
			</a>
		</div>
		
		<table cellspacing="0" cellpadding="0" style="border: 1px solid #ccc;">
			<tr>
				<td rowspan="3" style='width:135px;border:1px solid #CCCCCC;border-top: 0;' class="capit">
					<div>Капитализация ПАММ-портфеля:</div>
					<span>{$pamm.capitalization} {$pamm.currency}</span>
				</td>
				<td colspan="3" style=''>
					<div class="pamm_income">Доходность ПАММ-портфеля</div>
				</td>
			</tr>
			<tr>
				<td style='width:180px;'>
					<div class="income_head">За последнюю неделю</div>
					<div class="income_value red_td">{$pamm.week} %</div>
				</td>
				
				<td style='width:180px;'>
					<div class="income_head">За последний месяц</div>
					<div class="income_value green_td">{$pamm.month} %</div>
				</td>
				<td style='width:180px;' class="last_td">
					<div class="income_head">За все время</div>
					<div class="income_value green_td">{$pamm.general} %</div>
				</td>
			</tr>
			<tr>
				<td style='width:180px;'>
					<a class="income_btn" href='/users/{$is_auth}/pamm.html'>Детальная информация о проекте</a>
				</td>
				
				<td style='width:180px;'>
					<a class="income_btn" href='/stati/forum.html'>Обсудить на форуме</a>
				</td>
				<td style='width:180px;' class="last_td">
					<a class="income_btn" href="/users/{$ids}/invest_pamm.html">Пополнить ПАММ - портфель</a>
				</td>
			</tr>
		</table>
	
	</div>
	{/foreach}
</div>

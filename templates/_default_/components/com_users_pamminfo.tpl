<div class="pamm-accounts" style="margin-top:10px;" >
	<h4 style='margin:0;padding-left:40px;'>
		Биржа ПАММ портфелей
	</h4>
	<div id='underline'>
		<span>
		
		</span>
	</div>
	<div style="position:relative;top:-34px;left:327px;">
		<a class="new-link" id="sellSelfPamm">Продать ПАММ</a>
		<a class="new-link" id="buyDefault">Купить стандартные</a>
	</div>
	<input type="hidden" name="myCash" value="{$userCash}" />
	<p></p>
	<p></p>
	<table class="pamminfo-table">
		<thead>
			<tr>
				<td>
					<p>{$LANG.PAMM_NAME}</p>
				</td>
				<td>
					<p>{$LANG.PROFIT_MONTH}</p>
				</td>
				<td>
					<p>{$LANG.PROFIT_TOTAL}</p>
				</td>
				<td>
					<p>{$LANG.PAMM_TAX}</p>
				</td>
				<td>
					<p>{$LANG.PAMM_PRICE}</p>
				</td>
				<td>
					<p>{$LANG.PAMM_COUNT}</p>
				</td>
				<td>
					<p>{$LANG.PAMM_ACTION}</p>
				</td>
			</tr>
		</thead>
		<tbody>
			{foreach from=$getPammCustomer key=key item=result}
				{if $result.count ne '0'}
				<tr class="{$result.myself}">
					<td>
						<p>{$result.title}</p>
					</td>
					<td>
						<p>{$result.profitPerMonth}</p>
					</td>
					<td>
						<p>{$result.profitTotal}</p>
					</td>
					<td>
						<p>{$result.tax}</p>
					</td>
					<td>
						<p>{$result.price}</p>
					</td>
					<td>
						<p>{$result.count}</p>
					</td>
					<td>
						{if $result.myself eq 'myself'}
							<a data-count="{$result.count}" data-id="{$result.id}" data-price="{$result.price}" class="change-pamms">Изменить</a>
							<a data-count="{$result.count}" data-id="{$result.id}" data-name="{$result.title}" class="remove-pamms">Убрать</a>
						{else}
							<a data-count="{$result.count}" data-type="{$result.type}" data-price="{$result.price}" data-name="{$result.title}" data-id="{$result.id}" class="buy">Купить</a>
						{/if}
					</td>
				</tr>
				{/if}
			{/foreach}
		</tbody>
	</table>
</div>
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title"> Купить - <span></span></span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
		<form method="post">
			<table style="margin:auto;">
				<thead>
					<tr>
						<td colspan="2">
							Количество
						</td>
					</tr>
				</thead>
				<thead>
					<tr>
						<td colspan="2">
							<input type="number" class="pamm-input" min="1" name="count" />
						</td>
					</tr>
					{captcha2}
					<tr style="position:relative;top:10px;">
						<td colspan="2">
							<input type="submit" value="Купить" style="margin-top:10px;" />
						</td>
					</tr>
				</thead>
			</table>
			<input type="hidden" name="id" />
			<input type="hidden" name="type" />
		</form>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>


<div id="changePammsModal" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title" style="left:48%">Изменить</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
		<form method="post">
			<table style="margin:auto;">
				<thead>
					<tr>
						<td>
							Цена за единицу,$
						</td>
						<td>
							Количество,шт.
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="number" class="pamm-input" name="new-price" min="0.01" step="0.01" />
						</td>
						<td>
							<input type="number" class="pamm-input" name="change-count" min="1" />
						</td>
					</tr>
					{captcha2}
					<tr style="position:relative;top:10px;">
						<td colspan="2">
							<input type="submit" style="margin-top:10px;" value="Подтверждение" />
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="change-id" />
		</form>
	</div>
</div>
<a href="#changePammsModal" role="button" data-toggle="modal" class="view-change-pamms"></a>


<div id="removePammsModal" class="modal hide fade " style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<!--<span style='position: relative; top: 30%;'>-->
		<span style='position: absolute;left:0;width:100%;top:25%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Убрать <span id="pammName"></span> с биржи?</span>
		<form method="post" id="removePammAccount">
			<input style="width:42px;" type="submit" value="Да" />
			<input type="button" id="btnCnl" value="Нет" />
			<input type="hidden" name="remove-success" />
			
		</form>
	</div>
</div>
<a href="#removePammsModal" role="button" data-toggle="modal" class="remove-change-pamms"></a>

<div id="modal_success_new" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span id="textChange" style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"></span>
	</div>
</div>
<a href="#modal_success_new" role="button" data-toggle="modal" id='modal_success_btn_new'></a>


<div id="modal_success_new_default" class="modal hide fade" style='width: 900px;left:36%;'>
	<div class="modal-body" style='border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title" style="left:37%;">Купить стандартные ПАММ портфели</span>
		<span style='position: relative; top: 45%;'>
		<span id="textChange" style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"></span>
		<div style="margin-top:36px;">
		<table style="width:100%;margin-top:18px;" class="alert-buy">
			<thead>
				<tr>
					<td>
						<p>{$LANG.PAMM_NAME}</p>
					</td>
					<td>
						<p>{$LANG.PROFIT_MONTH}</p>
					</td>
					<td>
						<p>{$LANG.PROFIT_TOTAL}</p>
					</td>
					<td>
						<p>{$LANG.PAMM_TAX}</p>
					</td>
					<td>
						<p>{$LANG.PAMM_PRICE}</p>
					</td>
					<td>
						<p>{$LANG.PAMM_COUNT}</p>
					</td>
					<td>
						<p>{$LANG.PAMM_ACTION}</p>
					</td>
				</tr>
			</thead>
			<tbody>
				{foreach from=$pammDefault  item=res}
					<tr>
						<td>
							<p>{$res.title}</p>
						</td>
						<td>
							<p>{$res.profitPerMonth}</p>
						</td>
						<td>
							<p>{$res.profitTotal}</p>
						</td>
						<td>
							<p>{$res.tax}</p>
						</td>
						<td>
							<p>{$res.price}</p>
						</td>
						<td>
							<p>{$res.count}</p>
						</td>
						<td>
							<a data-count="{$res.count}" data-type="{$res.type}" data-price="{$res.price}" data-name="{$res.title}" data-id="{$res.id}" class="buy">Купить</a>
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
		</div>
	</div>
</div>
<a href="#modal_success_new_default" role="button" data-toggle="modal" id='modal_success_btn_new_default'></a>



<div id="modal_success_sell_pamm" class="modal hide fade" style='width: 900px;left:36%;'>
	<div class="modal-body" style='border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Продать ПАММ портфели</span>
		<span style='position: relative; top: 45%;'>
		<span id="textChange" style='display: inline-block; margin-left: 26px; font-size: 15px; margin-bottom:18px;' id="text_span"></span>
		<div>
		<div style="margin-top:18px;">
		{foreach from=$user_accounts key=key item=result}
			<table class="pamm-info">
				<thead>
					<tr>
						<td>Название</td>
						<td>Доход за месяц,$</td>
						<td>Общий доход,$</td>
						<td>Коммисия,%</td>
						<td>Цена,$</td>
						<td>Количество,шт.</td>
						<td>Действие</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{$key}</td>
						<td>{$result.profitPerMonth}</td>
						<td>{$result.totalProfit}</td>
						<td>{$result.tax}</td>
						<td>{$result.price}</td>
						<td>{$result.count}</td>
						<td>
							<a class="sell-pamm" data-id="{$result.id}" data-count="{$result.count}">Продать</a>
						</td>
					</tr>
				</tbody>
			</table>
		{/foreach}
		</div>
		</div>
	</div>
</div>
<a href="#modal_success_sell_pamm" role="button" data-toggle="modal" id='modal_success_btn_sell_pamm'></a>


<div id="modal_success_sell_exchange" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Продать на биржу</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
			<form method="post" >
				<table style="margin:auto;">
					<thead>
						<tr>
							<td>Цена за единицу,$</td>
							<td>Количество,шт.</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input type="number" min="0.01"  step="0.01" name="price" class="price pamm-input" required />
							</td>
							<td>
								<input type="number" min="1" name="count" class="count pamm-input" required />
							</td>
						</tr>
						{captcha2}
						<tr style="position:relative;top:10px;">
							<td colspan="2">
								<input type="submit" style="margin-top:10px;" value="Подтверждение" />
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="id" />
				<input type="hidden" name="sellPamm" value="1" />
			</form>
	</div>
</div>
<a href="#modal_success_sell_exchange" role="button" data-toggle="modal" id='modal_success_btn_exchange'></a>


<div id="modal_success_sell_service" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 150px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span class="message-title">Продать на сервис</span>
		<!--<span style='position: relative; top: 20%;'>-->
		<span style='position: absolute;left:0;width:100%;top:15%;'>
			<form method="post" >
				<table style="margin:auto;">
					<thead>
						<tr>
							<td colspan="2">Количество,шт.</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2">
								<input type="number" class="pamm-input count" min="1"  name="count"  required  />
							</td>
						</tr>
						{captcha2}
						<tr style="position:relative;top:10px;">
							<td colspan="2">
								<input type="submit" style="margin-top:10px;" value="Подтверждение" />
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="id" />
				<input type="hidden" name="sellPamm" value="1" />
			</form>
	</div>
</div>
<a href="#modal_success_sell_service" role="button" data-toggle="modal" id='modal_success_btn_service'></a>


<div id="modal_success_sell_type" class="modal hide fade" style='width: 680px;left:45%;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<!--<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span">Продажа</span>
		<br />-->
		<input style="margin-left:18px;" type="button" id="exchangeLink" value="Продажа ПАММ портфеля на биржу" />
		<input type="button" style="margin-left:7px" id="serviceLink" value="Продажа ПАММ портфеля сервису" />
	</div>
</div>
<a href="#modal_success_sell_type" role="button" data-toggle="modal" id='modal_success_btn_sell_type'></a>
{literal}
	<script>
	
		$(document).ready(function(){
			//повідомення про виконання операцій
			var hash = document.location.hash;
			if(hash == '#successBuy'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Покупка успешно выполнена");
				document.location.hash = "";
			}
			if(hash == '#successChange'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Статус успешно изменен");
				document.location.hash = "";
			}
			if(hash == '#successRemove'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Портфель убран из продажи");
				document.location.hash = "";
			}
			if(hash == '#errorCaptcha'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Неверный код с картинки");
				document.location.hash = "";
			}
			if(hash == '#successServise'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Портфель продан сервису");
				document.location.hash = "";
			}
			if(hash == '#successExchange'){
				$("#modal_success_btn_new").click();
				$('#textChange').text("Портфель выставлен на биржу");
				document.location.hash = "";
			}
			$('#btnCnl').on('click', function(){
				$('button.close').click();
			});
			
			$('#buyDefault').on('click', function(){
				$('#modal_success_btn_new_default').click();
			});
			
			$('#sellSelfPamm').on('click', function(){
				$('#modal_success_btn_sell_pamm').click();
			});
		})
	
		//купівля памму
		$('.buy').on('click', function(){
			$('.close').click();
			
			$('.limit-message').remove();
			$('input[type="number"]').val("1");
			$('input[name="id"]').attr('value',$(this).attr('data-id'));
			$('input[name="type"]').attr('value',$(this).attr('data-type'));
			var userCash=parseInt($('input[name="myCash"]').val());
			var maxItemBuy=(userCash/100)/parseFloat($(this).attr('data-price').trim());
			
			if(maxItemBuy.toString().indexOf('.')!=-1){
				var roundCount=parseInt(maxItemBuy.toString().substring(0,maxItemBuy.toString().indexOf('.')));
			}else{
				var roundCount=parseInt(maxItemBuy);
			}
			
			var count=parseInt($(this).attr('data-count').trim());
			//обмеження по наявності коштів
			if(roundCount<count){
				count=roundCount;
				$('input[name="count"]').after("<p class='limit-message'>Ваших средств достаточно на "+count+" штук.</p>");
			}
				
			
			$('input[type="number"]').attr('max',count);
			$("#modal_success_btn").click();
			$("#modal_success .message-title > span").text($(this).attr('data-name'));
		});
		
		
			//продаж паммів
		$('.sell-pamm').on('click', function(){
			$('.close').click();
			$('input[name="count"].count').attr('max',$(this).attr('data-count'));
			$('input[name="id"]').val($(this).attr('data-id'));
			$('input[name="count"]').val('1');
			$('input.price').val('0.01');
			$('#exchange, #service').slideUp('fast');
			$('#modal_success_btn_sell_type').click();
		});
		//продаж на біржу
		$('#exchangeLink').on('click', function(){
			$('button.close').click();
			setTimeout(function(){
				$('#modal_success_btn_exchange').click();
			},1000);
		});
		//продаж на сервіс
		$('#serviceLink').on('click', function(){
			$('button.close').click();
			setTimeout(function(){
				$('#modal_success_btn_service').click();
			},1000);
		});
	</script>
{/literal}
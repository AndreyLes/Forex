{literal}
	  <script>
			$(document).ready(function(){
				var hash = document.location.hash;
				if(hash == '#success'){
					$("#modal_success_btn").click();
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				if(hash == '#error'){
					$("#modal_success_btn").click();
					$("#text_span").text("Неправильно введен код проверки!");
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				if(hash == '#error_money'){
					$("#modal_success_btn").click();
					$("#text_span").text("Пожалуйста, введите правильную сумму!");
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				
				$('input[name="invest_money"]').keypress(function(b) {
					var C = /[0-9,.\x25\x27\x24\x23]/;
					var a = b.which;
					var c = String.fromCharCode(a);
					return !!(a==0||a==8||a==9||a==13||c.match(C));
				});
				
				function check_fields(){
					if($('input[name="invest_money"]').val() == '' || $('input[name="code"]').val() == ''){
						if($('input[name="invest_money"]').val() == ''){
							$('input[name="invest_money"]').css('border', '1px solid red');
						}
						else {
							$('input[name="invest_money"]').css('border', '1px solid #555');
						}
						
						if($('input[name="code"]').val() == '' || $('input[name="code"]').val() == ''){
							$('input[name="code"]').css('border', '1px solid red');
						}
						else {
							$('input[name="code"]').css('border', '1px solid #555');
						}
						return false;
						$('#invest_table').submit(function (){
							return false;
						});
					}
					else{
						$('input[name="invest_money"]').css('border', '1px solid #555');
						$('input[name="code"]').css('border', '1px solid #555');
						$('#invest_table').removeAttr('onsubmit');
						$('#invest_table').submit();
					}
				}
				
				$('#invest_do').click(function() {
					check_fields();
				});
			});
		</script>
{/literal}

<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='height: 100px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;'>ПАММ - портфель успешно пополнен на сумму <u>{$invest_money}</u> $</span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

<div id='underline'>
	<span></span>
</div>
	<h2 style='margin-top:60px;margin-bottom:60px;text-align:center'>Пополнение ПАММ - портфеля</h2>
<div>
	<div class='investl_block' style='width:400px;float:left' >
		<div id='invest_head'><span>{$expenses.invest.expense_title}</span></div>
		<div class='invest_thead'><span>{$expenses.invest.expense_title}</span></div>
		<div class='first'>
			<div>
				<span>
					№{$expenses.invest.id}
				</span>
			</div>
		</div>
		<div class='second'>
		<table style='position: relative; top: 20px; left: 20px;'>
			<tr>
				<td><span style='float:left'>Информация по счету:</span>
					<span class='dotted1'></span>
				</td>
				<td>
					
				</td>
			</tr>
			<tr>
				<td>
					<span style='float:left'>Валюта:</span>
					<span class='dotted2' style='width: 92px;'></span>
				</td>
				<td>
					{$expenses.invest.currency}
				</td>
			</tr>
			<tr>
				<td>
					<span style='float:left'>Общий баланс:</span>
					<span class='dotted2'></span>
				</td>
				<td>
					{$expenses.invest.expense_money}
				</td>
			</tr>
		</table>

		</div>
	</div>

	<div class='investl_block' style='width:400px;float:right' >
		<div id='invest_head'><span>ПАММ - портфель</span></div>
		<div class='invest_thead'><img style='margin-top:4px;margin-left: 5px;' src="{$user_pamm.image_url}"/></div>
		<div class='first'>
			<div>
			<span>
				№{$user_pamm.id}
			</span>
			</div>
		</div>
		<div class='second'>
			<table style='position: relative; top: 20px; left: 20px;'>
				<tr>
					<td><span style='float:left'>Информация по счету:</span>
						<span class='dotted1'></span>
					</td>
					<td>
						
					</td>
				</tr>
					<tr>
				<td><span style='float:left'>Валюта:</span>
						<span class='dotted2' style='width: 92px;'></span>
					</td>
					<td>
						{$user_pamm.currency}
					</td>
				</tr>
				<tr>
					<td>
						<span style='float:left'>Общий баланс:</span>
						<span class='dotted2'></span>
					</td>
					<td>
						{$user_pamm.money}
					</td>
				</tr>
			</table>
		</div>
	</div>
	<img id='invest_img' src='/images/invest_strelka.png'/>

</div>

<form id='invest_table' onsubmit="return false" method='POST' action=''>
		<div style='margin-bottom: 20px;'>
			<span style=''>Введите сумму в $:</span>
			<input type='text' class='text-input' name='invest_money' autocomplete="off" style=''/>
		</div>
		<div style='display:table'>
			<span style='padding-right:8px; width:190px; float:left;'>Введите код защиты —
		цифры, отображенные
		в виде картинки</span>
		{captcha}
		</div>
<button id='invest_do'><span>Пополнить</span></button> 
<a href='/users/{$ids}' id='invest_btn'><span>Вернуться в ЛИЧНЫЙ КАБИНЕТ</span></a> 
</form>

{add_js file='includes/jquery/tabs/jquery.ui.min.js'}
{add_css file='includes/jquery/tabs/tabs.css'}
{add_js file='includes/jquery/datepicker/jquery-1.10.2.js'}
{add_js file='includes/jquery/datepicker/jquery-ui.js'}
{add_js file='includes/jquery/jquery.maskedinput.js'}
{add_css file='includes/image_resider/css/imgareaselect-default.css'}
{add_js file='includes/image_resider/scripts/jquery.imgareaselect.pack.js'}


{literal}
	<script type="text/javascript">
		$(function(){$(".uitabs").tabs();});
		$(window).load(function() {
			$(function() {
				$( "#date_priem" ).datepicker({dateFormat: "yy-mm-dd"});
				$( "#date_card" ).datepicker({dateFormat:'yy/mm'});
			});
		});	
		$(document).ready(function(){
			var hash = document.location.hash;
				if(hash == '#success'){
					$("#modal_success_btn").click();
					document.location.hash = "";
				}
				if(hash == '#error_old_pass'){
					$("#modal_success_btn").click();
					$("#text_span").text("Старый пароль введен неверно.");
					document.location.hash = "";
				}
				if(hash == '#WRONG_PASS'){
					$("#modal_success_btn").click();
					$("#text_span").text("Пароли не совпали.");
					document.location.hash = "";
				}
				if(hash == '#PASS_SHORT'){
					$("#modal_success_btn").click();
					$("#text_span").text("Пароль должен быть не менее 6 символов!");
					document.location.hash = "";
				}
				if(hash == '#PASS_CHANGED'){
					$("#modal_success_btn").click();
					$("#text_span").text("Пароль успешно изменен.");
					document.location.hash = "";
				}
				if(hash == '#PROTECTION_USER'){
					$("#modal_success_btn").click();
					$("#text_span").text("Изменения успешно сохранены.");
					document.location.hash = "";
				}
				if(hash == '#ERROR'){
					$("#modal_success_btn").click();
					$("#text_span").text("Ошибка.");
					document.location.hash = "";
				}
				
				if(hash == '#SHORT_NICKNAME'){
					$("#modal_success_btn").click();
					$("#text_span").text("Никнейм не может быть короче 2 симв.");
					document.location.hash = "";
				}
				if(hash == '#ERR_NICK_EXISTS'){
					$("#modal_success_btn").click();
					$("#text_span").text("Выбранный никнейм запрещен администратором!");
					document.location.hash = "";
				}
				if(hash == '#LONG_CITY_NAME'){
					$("#modal_success_btn").click();
					$("#text_span").text("Слишком длинное название города.");
					document.location.hash = "";
				}
				if(hash == '#REALY_ADRESS_EMAIL'){
					$("#modal_success_btn").click();
					$("#text_span").text("Укажите настоящий адрес e-mail!");
					document.location.hash = "";
				}
				if(hash == '#ADRESS_EMAIL_IS_BUSY'){
					$("#modal_success_btn").click();
					$("#text_span").text("Указанный  адрес e-mail занят.");
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				if(hash == '#error_form'){
					$("#modal_success_btn").click();
					$("#text_span").text("Ошибка формы.");
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				if(hash == '#PROFILE_SAVED'){
					$("#modal_success_btn").click();
					$("#text_span").text("Профиль успешно сохранен.");
					document.location.hash = "";
					//window.history.pushState('obj', '', '/');
				}
				
				if(hash == '#error_money'){
					$("#modal_success_btn").click();
					$("#text_span").text("Пожалуйста, введите правильную сумму!");
					document.location.hash = "";
				}
				if(hash == '#changes_avatar'){
					$("#modal_success_btn").click();
					$("#text_span").text("Аватар изменен успешно.");
					document.location.hash = "";
				}
				
		});

	</script>
	
{/literal}
<div id="modal_success" class="modal hide fade" style='width: 557px;'>
	<div class="modal-body" style='padding:15px; border: 5px solid #ccc; text-align: center;'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<span style='position: relative; top: 45%;'>
		<span style='display: inline-block; margin-left: 26px; font-size: 15px;' id="text_span"><u></u></span>
	</div>
</div>
<a href="#modal_success" role="button" data-toggle="modal" id='modal_success_btn'></a>

	<div id='usr' style='padding-bottom: 6px;'>
	

 <div class="head_edit" id="nickname">
        {$usr.nickname} {if $usr.banned}<span style="color:red; font-size:12px;">{$LANG.USER_IN_BANLIST}</span>{/if}
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:14px">
	<tr>
		<td width="200" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="middle">
                        <div class="usr_avatar">
							<img alt="{$usr.nickname|escape:'html'}" class="usr_img" src="{$usr.avatar}" />
                        </div>

						<div id="usermenu" style="display:none;">
						<div class="usr_profile_menu">
						<table cellpadding="0" cellspacing="6" >
							 <tr>
								<td><img src="/templates/{template}/images/icons/profile/avatar.png"  border="0"/></td>
								<td><a href="/users/{$usr.id}/avatar.html" title="{$LANG.SET_AVATAR}">{$LANG.SET_AVATAR}</a></td>
							</tr>
					   </table></div>
                    </td>
				</tr>
			</table>
	    </td>
    	<td valign="top" style="padding-left:10px">
			<div id="profiletabs" class="uitabs">
		

				<div id="upr_profile">
					<div class="user_profile_data">
						<div class="field">
							<div class="title">{$LANG.LAST_VISIT}:</div>
							<div class="value">{$usr.flogdate}</div>
							<div>, IP:{$usr.last_ip}</div>
						</div>
						<div>
							
						</div>
						 {if $usr.city}
						<div class="field">
							<div class="title">{$LANG.CITY}:</div>
                            <div class="value"><a href="/users/city/{$usr.cityurl|escape:'html'}">{$usr.city}</a></div>
						</div>
                        {/if}
						<div class="field">
							<div class="title">{$LANG.DATE_BIRTHDAY}:</div>
							<div class="value">
								{$usr.birthdate}
                            </div>
						</div>
                        {if $usr.inv_login}
                            <div class="field">
                                <div class="title">{$LANG.INVITED_BY}:</div>
                                <div class="value">
                                    <a href="{profile_url login=$usr.inv_login}">{$usr.inv_nickname}</a>
                                </div>
                            </div>
                        {/if}


						{if $usr.showbirth && $usr.fbirthdate}
						<div class="field">
							<div class="title">{$LANG.BIRTH}:</div>
							<div class="value">{$usr.fbirthdate}</div>
						</div>
						{/if}

								{add_js file='includes/jquery/jquery.nospam.js'}
							<div class="field">
								<div class="title">E-mail:</div>
								<div class="value"><a href="#" rel="{$usr.email|NoSpam}" class="email">{$usr.email}</a></div>
							</div>
							{literal}
								<script>
										$('.email').nospam({ replaceText: true });
								</script>
							{/literal}
						<div class="field">
							<div class="title">Тел.:</div>
							<div class="value">
								{$usr.mobtelephone}
                            </div>
						</div>
				

						{if $cfg.privforms && $usr.form_fields}
							{foreach key=tid item=field from=$usr.form_fields}
                                <div class="field">
                                    <div class="title">{$field.title}:</div>
                                    <div class="value">{if $field.field}{$field.field}{else}<em>{$LANG.NOT_SET}</em>{/if}</div>
                                </div>
                            {/foreach}
						{/if}

					</div>

				</div>

                {foreach key=id item=plugin from=$plugins}
                    <div id="upr_{$plugin.name}">{$plugin.html}</div>
                {/foreach}

			</div>
	</td>
  </tr>
</table>
</div>
{literal}
<script>
	$(document).ready(function(){
			$("#your_phone").mask("999-999-99-99?");
			$('.info #see_nik_name + label').click(function(){
				data_checed = $(this).attr('data-checed');
				if(data_checed == '0'){
					$(this).attr('data-checed', '1');
					$(this).css('background-position', '-1px -34px');
				}
				if(data_checed == '1'){
					$(this).attr('data-checed', '0');
					$(this).css('background-position', '-1px -4px');
				}
			});
	});
	function validateForm()
	{
		var nick=document.forms["editform"]["nick"].value;
		var nickname=document.forms["editform"]["nickname"].value;
		var first_name=document.forms["editform"]["first_name"].value;
		var your_phone = document.forms["editform"]["your_phone"].value;
		var email = document.forms["editform"]["email"].value;
		var your_country = document.forms["editform"]["your_country"].value;
		var atpos=email.indexOf("@");
		var dotpos=email.lastIndexOf(".");
		
		if (nick == null || nick == "" || your_phone == null || your_phone == "" || your_phone == "___-___-__" || email == null || email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length || nickname == null || nickname == "" || first_name == null || first_name == "" || your_country == null || your_country == "")
		{
			if (nick == null || nick == "")
			{
				$("#about input[name='nick']").css("border", "2px solid red" );
			}
			if (your_phone == null || your_phone == "" || your_phone == "___-___-__")
			{
				$("#about input[name='your_phone']").css("border", "2px solid red" );
			}
			if (email == null || email == "")
			{
				$("#about input[name='email']").css("border", "2px solid red" );
			}
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
			{
				$("#about input[name='email']").css("border", "2px solid red" );
			}
			if (nickname == null || nickname == "")
			{
				$("about input[name='nickname']").css("border", "2px solid red" );
			}
			if (first_name == null || first_name == "")
			{
				$("#about input[name='first_name']").css("border", "2px solid red");
			}
			if (your_country == null || your_country == "")
			{
				$("#about input[name='your_country']").css("border", "2px solid red");
			}
			return false;
		}
		else{
			$('#profiletabs #editform .save_profile button[type="submit"]').trigger('click');
		};
	}
</script>
{/literal}
<div id='usr'>
<div class="head_edit">{$LANG.CONFIG_PROFILE}</div>
<div id="profiletabs" class="uitabs">
    <ul id="tabs">
        <li rel="hid"><a href="#about"><span>{$LANG.ABOUT_ME}</span></a></li>
		<li rel="hid"><a href="#avatarka"><span>{$LANG.AVATAR}</span></a></li>
		<li><a href="#documents"><span>{$LANG.DOCUMENTS}</span></a></li>
		<li><a href="#soc_cety"><span>{$LANG.SOC_SET}</span></a></li>
        <li rel="hid"><a href="#change_password"><span>{$LANG.CHANGING_PASS}</span></a></li>
		<li><a href="#verification"><span>{$LANG.VERIFICATION}</span></a></li>
		<li><a href="#payment_inf"><span>{$LANG.PAYMENT_INFORMATION}</span></a></li>
		<li><a href="#change_color"><span>{$LANG.CHANGE_COLOR}</span></a></li>
    </ul>
	<form id="editform" name="editform" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="opt" value="save" />
        <div id="about">
		<h4 style="font-size: 20px;    font-weight: 100; padding-left: 8px;">
		{$LANG.MY_PROFILE}
		</h4>
		<div class="all_inf">
			<span>
			Всегда держите свои данные актуальными. Неправильные или неполные данные могут неправильно повлиять на обработку ваших заявок на получение или выдачу займов.
			</span>
		</div>
		<!--div id='underline'>
			<span>
			
			</span>
		</div-->
        <div class="title_table">
			<div class="image_title_table"></div>
			<div class="text_title_table">
				<h4>
					{$LANG.PERSONAL_INFORMATION}
				</h4>
			</div>
		</div>
		<!--div id='underline'>
			<span>
			
			</span>
		</div-->
		<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
                <!-- id юзера -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.NUM_USER} <br />
                    </td>
                    <td valign="top"><input name="user_id" type="text" class="text-input" id="user_id" style="width:300px; border: 1px solid black; background-color: #fff;" value="{$usr.id|escape:'html'}" disabled/></td>
                </tr>
				<!-- никнейм -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.NIK_NAME}<span style="padding-left:5px;" class="regstar">*</span> <br />
                        <span class="usr_edithint">{$LANG.NIK_NAME_TEXT}</span>
                    </td>
                    <td valign="top"><input name="nick" type="text" class="text-input" id="nick" style="margin-top: 5px; width:300px" value="{$usr.login|escape:'html'}"/></td>
                </tr>
				<!-- отображать никнейм -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.SEE_NIK_NAME} <br />
					</td>
                    <td valign="top">
						<input  id="see_nik_name" value="1" {if $usr.visible_login == '1'} checked {/if} type="checkbox" name="see_nik_name">
						<label {if $usr.visible_login == '1'} style="background-position:-1px -34px;" data-checed="1" {else} style="background-position:-1px -4px;" data-checed="0" {/if} for="see_nik_name"></label>
					</td>
                </tr>
				<!-- Имя -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_NAME} <span style="padding-left:5px;" class="regstar">*</span><br />
                    </td>
                    <td valign="top"><input name="nickname" type="text"  class="text-input" id="nickname" style="width:300px" value="{$usr.nickname|escape:'html'}"/></td>
                </tr>
				<!-- Фамилия -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_FIRST_NAME} <span style="padding-left:5px;" class="regstar">*</span><br />
                    </td>
                    <td valign="top"><input name="first_name" type="text" class="text-input" id="first_name" style="width:300px" value="{$usr.first_name}"/></td>
                </tr>
				<!-- Отчество -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_SECOND_NAME} <br />
                    </td>
                    <td valign="top"><input name="old_name" type="text" class="text-input" id="old_name" style="width:300px" value="{$usr.old_name}"/></td>
                </tr>
				<!-- пол -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_GENDER}<br />
					</td>
                    <td valign="top">
					<input id="your_gender1"  type="radio" {if $usr.gender_user != '2'} checked {/if} value="1" name="your_gender"><label for="your_gender1">{$LANG.MAN_GENDER}</label>
					<input id="your_gender2"  type="radio" {if $usr.gender_user == '2'} checked {/if} value="2" name="your_gender"><label for="your_gender2">{$LANG.WOMEN_GENDER}</label>
					</td>
				</tr>
				<!-- скайп -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_SKYPE} <br />
					</td>
                    <td valign="top"><input name="your_skype" type="text" class="text-input" id="your_skype" style="width:300px" value="{$usr.skype}"/></td>
                </tr>
				<!-- телефон -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_PHONE} <span style="padding-left:5px;" class="regstar">*</span><br />
					</td>
                    <td valign="top">
					<select id="format" name="your_phone_code" style="font-size:14px; height: 24px; width: 100px; border-radius: 0;  padding-left: 14px;">
						<option value="+(38)" {if $usr.code_phone == '+(38)'} selected{/if}>+(38)</option>
						<option value="+(37)" {if $usr.code_phone == '+(37)'} selected{/if}>+(37)</option>
						<option value="+(29)" {if $usr.code_phone == '+(29)'} selected{/if}>+(29)</option>
					</select>
					<input name="your_phone" type="text" class="text-input" id="your_phone" style="width:295px !important;" value="{$usr.mobtelephone}"/>
					</td>
                </tr>
				<!-- ел.почта -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.YOUR_MAIL}<span style="padding-left:5px;" class="regstar">*</span><br />
                    </td>
                    <td valign="top">
                        <input name="email" type="text" class="text-input" id="email" style="width:300px" value="{$usr.email}"/>
                    </td>
                </tr>
				<!-- страна -->
				<tr class='info'>
                    <td valign="top">
                        {$LANG.COUNTRY}<span style="padding-left:5px;" class="regstar">*</span><br />
                    </td>
                    <td valign="top">
                        <input name="your_country" type="text" class="text-input" id="your_country" style="width:300px" value="{$usr.country|escape:'html'}"/>
					</td>
                </tr>
				<!-- дата рождения -->
                <!--tr class='info select_td'>
                    <td valign="top"><strong>{$LANG.BIRTH}:</strong> </td>
                    <td valign="top">
                        {dateform seldate=$usr.birthdate}
                    </td>
                </tr-->
            </table>
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						{$LANG.ADDRESS}
					</h4>
				</div>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!-- Индекс -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.INDEX2}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_index" type="text" class="text-input" id="your_index" style="width:300px" value="{$usr.index_user}"/></td>
                </tr>
				<!-- Город -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.CITY}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_city" type="text" class="text-input" id="your_city" style="width:300px" value="{$usr.city}"/></td>
                </tr>
				<!-- Улица -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.STREET}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_street" type="text" class="text-input" id="your_street" style="width:300px" value="{$usr.street}"/></td>
                </tr>
				<!-- дом -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.HOME}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_home" type="text" class="text-input" id="your_home" style="width:300px" value="{$usr.home}"/></td>
                </tr>
				<!-- квартира -->
				<tr class='info'>
                    <td width="300" valign="top">
                        <strong>{$LANG.FLAT}:</strong><br />
                        </td>
                    <td valign="top"><input name="your_flat" type="text" class="text-input" id="your_flat" style="width:300px" value="{$usr.flat}"/></td>
                </tr>
			</table>
			<div class="save_profile" style="margin-top: 12px;text-align:center" id="">
				<button type="button" onclick="validateForm()"  id="save_btn"><span>{$LANG.SAVE}</span></button>
				<button style="display:none;" name="save"  type="submit" id="save_btn"><span>{$LANG.SAVE}</span></button>
				<button name="save" name="delbtn2"  type="button"  id="delbtn2" onclick="location.href='/users/{$usr.id}/delprofile.html';">
					<span>{$LANG.DEL_PROFILE}</span>
				</button>
			</div>
		</div>
		<!-- документация -->
		<div id="documents">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						{$LANG.TEXT_DOCUMENTATION}
					</h4>
				</div>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!-- серия -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.PASSPORT_SERIES}<br />
                        </td>
                    <td valign="top"><input name="passport_series" type="text" class="text-input" id="passport_series" style="width:300px" value="{$usr.passport_series}"/></td>
                </tr>
				<!-- номер -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.PASSPORT_NUMBER}<br />
                        </td>
                    <td valign="top"><input name="passport_number" type="text" class="text-input" id="passport_number" style="width:300px" value="{$usr.passport_number}"/></td>
                </tr>
				<!-- дата видачи -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.PASSPORT_DATE}<br />
                        </td>
                    <td valign="top"><input name="passport_date" type="text" class="text-input" id="date_priem" style="width:300px" value="{$usr.passport_date}"/></td>
                </tr>
				<!-- код подразделения -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.CODE_REGION}<br />
                        </td>
                    <td valign="top"><input name="code_region" type="text" class="text-input" id="code_region" style="width:300px" value="{$usr.code_region}"/></td>
                </tr>
				<!-- кем выдан -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.ISSUED_BY}<br />
                        </td>
                    <td valign="top"><input name="issued_by" type="text" class="text-input" id="issued_by" style="width:300px" value="{$usr.issued_by}"/></td>
                </tr>
			</table>
		</div>
		<!-- подключенные соц сети -->
		<div id="soc_cety" class="soc_cety">
			<ul style="    list-style: none; display: table;    width: 100%; padding: 5px;" id="table_soc_cet">
				<li id="main_li">
					<div class="title_table1">
						<div class="image_title_table1"></div>
						<div class="text_title_table1">
							<h4>
								{$LANG.SOC_SET_IN}
							</h4>
						</div>
					</div>
				</li>
				<li style=" height:100%; padding-top:1px;">
					<div class="my_soc_cet" style="display: inline-block;">
						<div class="soc_cet_div" data-net="od" style=" {if $usr.odnokl != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -260px -8px; width:50px; height:50px;"></div>
							<input type="text" name="od" value="{$usr.odnokl}">
						</div>
						<div class="soc_cet_div" data-net="vk" style="{if $usr.vk != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -144px -8px; width:50px; height:50px; "></div>
							<input type="text" name="vk" value="{$usr.vk}">
						</div>
						<div class="soc_cet_div" data-net="tv" style="{if $usr.tviter != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -202px -8px; width:50px; height:50px; "></div>
							<input type="text" name="tv" value="{$usr.tviter}">
						</div>
						<div class="soc_cet_div" data-net="g_plus" style="{if $usr.g_plus != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -143px -240px; width:50px; height:50px;"></div>
							<input type="text" name="g_plus" value="{$usr.g_plus}">
						</div>
						<div class="soc_cet_div" data-net="fb" style=" {if $usr.fasebook != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -317px -8px; width:50px; height:50px; "></div>
							<input type="text" name="fb" value="{$usr.fasebook}">
						</div>
						<div class="soc_cet_div" data-net="mail" style="{if $usr.mail != ''}display:block;{/if} ">
							<div title="удалить соц сеть" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -85px -124px; width:50px; height:50px; "></div>
							<input type="text" name="mail" value="{$usr.mail}">
						</div>
					</div>
				</li>
				<li>
					<div class="title_table1">
						<div class="image_title_table1"></div>
						<div class="text_title_table1">
							<h4>
								{$LANG.ADD_SOC_SET}:
							</h4>
						</div>
					</div>
				</li>
				<li style="padding-bottom: 25px; height: 50px; padding-top:1px;">
					<div class="oll_soc_set" style="display: inline-block;">
						<div title="добавить соц сеть" data-net="od" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -265px -11px; width:50px; height:50px; float:left; {if $usr.odnokl != ''}display:none;{/if} "></div>
						<div title="добавить соц сеть" data-net="vk" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -149px -11px; width:50px; height:50px; float:left; {if $usr.vk != ''}display:none;{/if} "></div>
						<div title="добавить соц сеть" data-net="tv" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -207px -11px; width:50px; height:50px; float:left; {if $usr.tviter != ''}display:none;{/if} "></div>
						<div title="добавить соц сеть" data-net="g_plus" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -148px -243px; width:50px; height:50px; float:left; {if $usr.g_plus != ''}display:none;{/if} "></div>
						<div title="добавить соц сеть" data-net="fb" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -322px -11px; width:50px; height:50px; float:left; {if $usr.fasebook != ''}display:none;{/if} "></div>
						<div title="добавить соц сеть" data-net="mail" style="background: url({$usr.profile_link}/../../templates/_default_/images/sic-seti-icon.jpg) no-repeat -90px -127px; width:50px; height:50px; float:left; {if $usr.mail != ''}display:none;{/if} "></div>
					</div>
				</li>
			</ul>
		</div>
{literal}
<script>
	$('.oll_soc_set').on('click', 'div', function(){
		data_set = $(this).attr("data-net");
		$(this).fadeOut(500);
		$(this).parent().parent().parent().find('.my_soc_cet div').each(function(){
			data_set_user = $(this).attr("data-net");
			if(data_set == data_set_user){
			$(this).fadeIn(500);
			}
		});
	});
	$('.my_soc_cet .soc_cet_div').on('click', 'div', function(){
		data_set2 = $(this).parent().attr("data-net");
		$(this).parent().fadeOut(500);
		$(this).parent().find('input').val('');
		$(this).parent().parent().parent().parent().find('.oll_soc_set div').each(function(){
			data_set_user2 = $(this).attr("data-net");
			if(data_set2 == data_set_user2){
			$(this).fadeIn(500);
			}
		});
	});
</script>
{/literal}
		<!-- платежные данные -->
		<div id="payment_inf">
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						{$LANG.PAYMENT_INFORMATION}
					</h4>
				</div>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" style='margin-top: 10px;'>
				<!-- номер карты -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.CARD_NUMBER}<br />
                        </td>
                    <td valign="top"><input name="card_number" type="text" class="text-input" id="card_number" style="width:300px;" value="{$usr.card_number}"/></td>
                </tr>
				<!-- срок действия карты -->
				<tr class='info'>
                    <td width="300" valign="top">
                        {$LANG.DATE_CARD}<br />
                        </td>
                    <td valign="top"><input name="date_card" type="text" class="text-input" id="date_card" style="width:300px;" value="{$usr.date_card}"/></td>
                </tr>
				{foreach key=tid item=system from=$payments}
					<tr class='info'>
						<td width="300" valign="top">
							<div style="width:105px; float:left;"><img style="max-width:105px; height:40px;" src="{$system.img}"></div>
							<span style="float:left; padding-left:5px; padding-top:9px;">{$system.name}</span><br />
							</td>
						<td style="padding-top: 13px;" valign="top">
							<!--span style="width: 50px; text-align: right; display: block; float: left; margin-top: 3px; margin-right: 5px;">{$system.before_score}</span-->
							<input  data-number="{$system.number_score}" data-before="{$system.before_score}" data-after="{$system.after_score}" name="peyment[{$system.id}]" id="peyment[$system.id]" type="text" class="text-input score_num" style="width:300px; float: left;" value="{$inf_moneys[$system.id].score_num}"/>
							<!--span style="    margin: 3px 0 0 3px;">{$system.after_score}</span-->
						</td>
					</tr>
				{/foreach}
			</table>
		</div>
		<!-- Вивід верифікації -->
{literal}
<script>
$(document).ready(function(){
	
	$('#payment_inf .score_num').each(function(){
		length_number = $(this).attr('data-number');
		ghghg = "";
		for(i=0; i<length_number;i++){
			ghghg += "9";
		}
		
		//alert(ghghg);
		$(this).attr("placeholder", $(this).attr('data-before')+ghghg+$(this).attr('data-after'));
		$(this).mask($(this).attr('data-before')+ghghg+$(this).attr('data-after'));
	});

});
function del_photo1(){
	if($('.first_verification #del_photo_1').val() == '0'){
		$('.first_verification').find('.photobox').css('display', 'none');
		$('#del_photo_1').val('1');
		$('.first_verification .cliker_photo').text('отменить');
	}
	else{
		$('.first_verification').find('.photobox').css('display', 'block');
		$('#del_photo_1').val('0');
		$('.first_verification .cliker_photo').text('Удалить фото');
	}
}
function del_photo2(){
	if($('.second_verification #del_photo_2').val() == '0'){
		$('.second_verification').find('.photobox').css('display', 'none');
		$('#del_photo_2').val('1');
		$('.second_verification .cliker_photo').text('отменить');
	}
	else{
		$('.second_verification').find('.photobox').css('display', 'block');
		$('#del_photo_2').val('0');
		$('.second_verification .cliker_photo').text('Удалить фото');
	}
}
function del_photo3(){
	if($('.card_verification #del_photo_3').val() == '0'){
		$('.card_verification').find('.photobox').css('display', 'none');
		$('#del_photo_3').val('1');
		$('.card_verification .cliker_photo').text('отменить');
	}
	else{
		$('.card_verification').find('.photobox').css('display', 'block');
		$('#del_photo_3').val('0');
		$('.card_verification .cliker_photo').text('Удалить фото');
	}
}
</script>
{/literal}
	     <div id="verification">
		 {foreach key=tid item=result from=$results_file}
		 {if $tid eq 0}
		 <h4>
			{$LANG.FIRS_LVL_VERIFICATION}
		 </h4>
		 <div id='underline' style='width: 900px;'>
			<span></span>
		 </div>
		 <div style='margin-top:26px'>
		 <div style='float:left'> 
		 		<span class='ver_span'>Документ,	подтверждающий личность</span>
		 </div>
			<div class='right_block'>
				{if $result.filename}
					<div class="first_verification" style=" height: 75px;">
						<a style="float:left;" href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'><img src='/images/users/docs/small/{$result.filename}'/></a>
						<input id="del_photo_1" type="hidden" name="del_photo_1" value="0">
						<a class="cliker_photo" style="display:block; cursor:pointer; margin-top: 12px; float: left; margin-left: 10px;" onclick="del_photo1()">Удалить фото</a>
					</div>
					<p>{$LANG.FIRST_LEVEL}</p>
				{else}
					<input class="del_photo_100" name="passport{$tid}" type="file" id="passport" size="30" />
					<p>{$LANG.FIRST_LEVEL}</p>
				{/if}
			</div>
		</div>
{if $results_file[1].filename == ""}
	{literal}
		<script>		 
			$(document).ready(function(){
				$('.del_photo_100').on('change', function() {
					if ($('.del_photo_100').val() != '') {
						$('.zapret_photo2').css('display', 'block');
						$('#second_lvl').css('display', 'none');
					}
					else{
						$('.zapret_photo2').css('display', 'none');
						$('#second_lvl').css('display', 'block');
						}
				});
			});	 
		</script>
	{/literal}
{else}
	{literal}
		<script>		 
			$(document).ready(function(){
				$('.del_photo_100').on('change', function() {
					if ($('.del_photo_100').val() != '') {
						$('.second_verification').css('display', 'block');
						$('#second_lvl').css('display', 'none');
					}
					else{
						$('.second_verification').css('display', 'none');
						$('#second_lvl').css('display', 'block');
					}
				});
			});	 
		</script>
	{/literal}
{/if}

{if $results_file[0].filename == ""}
{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$('.second_verification').css('display', 'none');
		$('.zapret_photo2').css('display', 'none');
		$('#second_lvl').css('display', 'block');
		//$('.zapret_photo').css('display', 'block');
	});
</script>
{/literal}		 
{/if}		
			{elseif $tid eq 1}
			
		<h4>
			{$LANG.SECOND_LVL_VERIFICATION}
		</h4>
		 <div id='underline' style='width: 900px; margin-bottom: 15px;'>
			<span></span>
		 </div>
			<p id='second_lvl' style=" margin-top: -15px; display:none;">{$LANG.CANT_VER}</p>
			<div style='float:left'>
				<span class='ver_span'>Фото пользователя с документом в руке</span>
			</div>
			<div class='right_block'>
				{if $result.filename}
				<!--div class="zapret_photo" style="display:none;">
				Вы не можете претендовать на второй уровень верификации
					</div-->
				<div class="second_verification" style=" height: 75px;">
					
					
					<a style="float:left;" href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'>	<img src='/images/users/docs/small/{$result.filename}'/></a>
					<input id="del_photo_2" type="hidden" name="del_photo_2" value="0">
					<a class="cliker_photo" style="display:block; cursor:pointer; margin-top: 12px; float: left; margin-left: 10px;" onclick="del_photo2()">Удалить фото</a>
				</div>		
				<p>{$LANG.SECOND_LEVEL}</p>
					{else}
				<input class="zapret_photo2" name="passport{$tid}" type="file" id="passport" size="30" />
						<p>{$LANG.SECOND_LEVEL}</p>
				{/if}
		
			 </div>	
			 
			{elseif $tid eq 2}
			
						 <h4>
							Подтвердить наличие банковской карты
						 </h4>
						 <div id='underline' style='width: 900px;margin-bottom:29px'>
							<span></span>
						 </div>
						<div style='float:left'>
							<span class='ver_span'>Банковская карта, подтверждающая наличие счета</span>
						</div>
						<div  class='right_block'>
								{if $result.filename}
								<div class="card_verification" style=" height: 75px;">
									<a style="float:left;" href='/images/users/docs/large/{$result.filename}' class='photobox cboxElement'><img src='/images/users/docs/small/{$result.filename}'/></a>
									<input id="del_photo_3" type="hidden" name="del_photo_3" value="0">
									<a class="cliker_photo" style="display:block; cursor:pointer; margin-top: 12px; float: left; margin-left: 10px;" onclick="del_photo3()">Удалить фото</a>
								</div>
									<p>{$LANG.THIRD_LEVEL}</p>
									{else}
								<input name="passport{$tid}" type="file" id="passport" size="30" /><p>{$LANG.THIRD_LEVEL}</p>
								{/if}
						</div>
						 <div  id='last_block'>
								<div style='float:left'>
									<span class='ver_span' style='margin-top:10px'>Также Вы можете</span>
								</div>
							<div  class='right_block'>
								<!--button id='kartka'><span>Заказать банковскую карту</span> </button-->
								<a id='kartka'><span>{$LANG.ORDER_BANK_CARD}</span></a>
							</div>
						</div>
			{/if}
		{/foreach}
				
			
        </div>
		<!-- изменения цвета -->
		<div id='change_color'>

<!-- BEGIN STYLESHEET SWITCHER -->
{literal}
<script>
	var c = readCookie('style');
	$('#kartka').on('click',function(){
		location="ordercard.html";
	})
</script>
{/literal}
			<ul id="stylesheets" style="text-align:center;">
				<li>
					<a id='dark' href="javascript:switchStylestyle('dark');">
						<img src='/images/red.png'/>
						<input type='checkbox' id='checkbox-id1' value='v1' name='check1'/>
						<label for="checkbox-id1"></label>
					</a>
				</li>
				<li>
					<a id='blue' href="javascript:switchStylestyle('blue');">
						<img src='/images/blue.png'/>
						<input type='checkbox' id='checkbox-id2' value='v1' name='check2'/>
						<label for="checkbox-id2"></label>
					</a>
				</li>
				<li>
					<a id='green' href="javascript:switchStylestyle('green');">
						<img src='/images/green.png'/>
						<input type='checkbox' id='checkbox-id3' value='v1' name='check3'/>
						<label for="checkbox-id3"></label>
					</a>
				</li>
			</ul>
		</div>
		<div class="save_profile" style="margin-top: 12px;text-align:center;display:none;" id="submitform">
			<button type="button" onclick="validateForm()"  id="save_btn"><span>{$LANG.SAVE}</span></button>
			<button style="display:none;" name="save"  type="submit" id="save_btn"><span>{$LANG.SAVE}</span></button>
			<!--button name="save" name="delbtn2"  type="button"  id="delbtn2" onclick="location.href='/users/{$usr.id}/delprofile.html';">
				<span>{$LANG.DEL_PROFILE}</span>
			</button-->
			<button type="button"  id="delbtn2" onclick="location.href='/users/{$usr.login}';">
				<span>Отменить</span>
			</button>
        </div>
    </form>
	
	<!-- change pass -->
    <div id="change_password">
		<form id="editform" name="editform" method="post" action="">
			<input type="hidden" name="opt" value="protection_user" />
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						{$LANG.ACCESS_PROTECTION}
					</h4>
				</div>
			</div>
            <table width="55%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td style="width: 200px;" valign="top">
                        <input id="protection_user1"  type="radio" {if $usr.protection_user == '1'} checked {/if} value="1" name="protection_user"><label for="protection_user1">{$LANG.RESET_PROTECTION}</label>
						<input id="protection_user2"  type="radio" {if $usr.protection_user == '2'} checked {/if}  value="2" name="protection_user"><label for="protection_user2">{$LANG.CMC_PROTECTION}</label>
						<input id="protection_user3"  type="radio" {if $usr.protection_user == '3'} checked {/if}  value="3" name="protection_user"><label for="protection_user3">{$LANG.MAIL_PROTECTION}</label>
					</td>
                </tr>
				<tr>
					<td style="text-align:center;">
						<button name="save10" type="submit" class="save_protection" id="save2"><span>{$LANG.SAVENG}</span></button>
					</td>
				</tr>
			</table>
			
		</form>
        <form id="editform" name="editform" method="post" action="">
            <input type="hidden" name="opt" value="changepass" />
			<div class="title_table">
				<div class="image_title_table"></div>
				<div class="text_title_table">
					<h4>
						{$LANG.CHANGE_PASSWORDS}
					</h4>
				</div>
			</div>
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
                    <td width="150" valign="top">
                        <strong>{$LANG.OLD_PASS}:</strong>
                    </td>
                    <td valign="top">
                        <input name="oldpass" type="password" id="oldpass" class="text-input" size="30" />
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS}:</strong></td>
                    <td valign="top"><input style="width: 206px !important; padding-left: 0px;" name="newpass" type="password"  id="newpass" class="passes text-input" size="30" />
					
					<a id='showpass'>{$LANG.SEE_PASSWORD}</a></td>
					
				</tr>
				<tr>
					<td></td>
					<td>
						<ul id='galki'>
							<li><span>Минимум 2 латинские буквы</span></li>
							<li><span>Минимум 2 цифры</span></li>
							<li><span>Минимум 10 символов</span></li>
							<li><span>Максимум 16 символов</span></li>
						</ul>
					</td>
				</tr>
                <tr>
                    <td valign="top"><strong>{$LANG.NEW_PASS_REPEAT}:</strong></td>
                    <td valign="top">
					<input name="newpass2" type="password" class="passes text-input" id="newpass2" size="30" /><span id='galkun'></span></td>
                </tr>
				<tr>
					<td></td>
                    <td valign="top">
					<button name="save2" type="submit"  id="save2"><span>{$LANG.SAVENG}</span></button>
					<button id="delbtn3" type='button'><span>{$LANG.CLEAR_PASSWORD}</span></button>
					</td>
				</tr>
            </table>
        </form>
    </div>
	<!-- аватарка -->
	
	<div id="avatarka">
	<div id="image_preview" ><span style="    position: relative;" data-inf="Кликните по картинке, чтобы изменить ее." data-class="top" class="hint_img_size" ><img id="previewing" src="{$usr.avatar}" /></span></div>
	<div style="display:none;">
	<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
		<div id="selectImage">
		<input type="file" name="file" id="file" required />
		<input style="display:none;" type="submit" value="Upload" class="submit" />
		</div>
	</form>
	<h4 id='loading' ></h4>
	<div id="message"></div>
	</div>
	<div style="text-align:center; margin:10px 0; ">
		<div id="message"></div>
	</div>
	<form action="" method="post" id="form_upload_photo" onsubmit='return validateXY();'>
		<input type="hidden" name="opt" value="form_upload_photo">
		<input type="hidden" name="x1" value="" />
		<input type="hidden" name="y1" value="" />
		<input type="hidden" name="x2" value="" />
		<input type="hidden" name="y2" value="" />
		<input type="hidden" name="file_name" value="" />
		<input id="save2" type="submit" name="submit" value="Сохранить" disabled />
		<button type="button" style="color: #fff;text-align: right;padding-right: 10px;" id="delbtn2" onclick="location.href='/users/{$usr.id}/select-avatar.html';">
			Выбрать из колекции
		</button>
	</form>
	<!--form enctype="multipart/form-data" action="" method="POST">
		<input type="hidden" name="opt" value="avatarka" />
		<p>{$LANG.SELECT_UPLOAD_FILE}: </p>
			<input name="upload" type="hidden" value="1"/>
			<input name="userid" type="hidden" value="{$id}"/>
			<input name="picture" type="file" id="picture" size="30" />
		<p style="margin-top:10px">
			<button name="save2" type="submit"  id="save2"><span style="margin-left:0px;">{$LANG.UPLOAD}</span></button>
		</p>
	</form-->
	</div>
</div>
</div>


	


<!-- СМЕНА ПАРОЛЯ -->
{literal}
	<script type="text/javascript">
		
$(document).ready(function (e) {
$('#image_preview').on('click','#previewing',function(){
	$('#uploadimage #file').click();
});
$("#uploadimage").on('submit',(function(e) {
	e.preventDefault();
	$("#message").empty();
	$.ajax({
		url: "/components/users/ajax/ajax_php_file.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data)   // A function to be called if request succeeds
		{
			
			if(data != 'error'){
			$('#form_upload_photo input[type="submit"]').removeAttr('disabled');
			$("#previewing").attr('src',data);
			$('#form_upload_photo input[name="file_name"]').val(data);
			blabla();
			}
			else{
				$('#avatarka #message').text('Данное изображение не подходит поформату или размеру.');
				console.log(data);
				console.log('Данное изображение не подходит по формату или размеру.');
				$('#form_upload_photo input[type="submit"]').attr('disabled','disabled');
			}
		}
	});
}));

// Function to preview image after validation
$(function() {
$("#file").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#previewing').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
$("#file").css("color","green");
$('#image_preview').css("display", "block");
$('#previewing').attr('src', e.target.result);
$('#uploadimage .submit').click();
/*$('#previewing').attr('width', '250px');
$('#previewing').attr('height', '230px');*/
};
});
function blabla(){
	$('img#previewing').imgAreaSelect({
		x1: 0, y1: 0, x2: 55, y2: 55,
		maxWidth: 200, maxHeight: 200, aspectRatio: '1:1', handles: true,				
		onSelectEnd: function (img, selection) {
			$('input[name="x1"]').val(selection.x1);
			$('input[name="y1"]').val(selection.y1);
			$('input[name="x2"]').val(selection.x2);
			$('input[name="y2"]').val(selection.y2);            
		}			
	});
}
function validationAddPC(){
	$('#form_add_pc input[name="name_pc"]').css('border','1px solid black');
	var name_new_pc = $('#form_add_pc input[name="name_pc"]').val();
	if(name_new_pc == ""){ 
		$('#form_add_pc input[name="name_pc"]').css('border','1px solid red'); return false;
	}
	
}
function validateXY(){
	var x1 = $('#form_upload_photo input[name="x1"]').val();
	var x2 = $('#form_upload_photo input[name="x2"]').val();
	var y1 = $('#form_upload_photo input[name="y1"]').val();
	var y2 = $('#form_upload_photo input[name="y2"]').val();
	var file_name = $('#form_upload_photo input[name="file_name"]').val();
	if(x1 == "" || x2 == "" || y1 == "" || y2 == "" || file_name == ""){
		return false;
	}
};	
		
		$(document).ready(function(){
			$('#showpass').click(function(){
				type_pass = $('#newpass').attr('type');
				if(type_pass == 'password'){
					$('#newpass').attr('type','text');
					$('#showpass').text('Спрятать пароль');
				}
				else{
					$('#newpass').attr('type','password');
					$('#showpass').text('Показать пароль');
				}
			});
			var c = '{/literal}{$style_cooke}{literal}';//readCookie('style');
			if (c){
				$('#stylesheets li a#'+c).find("input[type='checkbox']").prop("checked", true);
			}
			
		});
		
		/**/
		$(function(){
			$('#stylesheets li a').click(function(){
			//alert('111111');
			if($(this).find("input[type='checkbox']").prop('checked') != true){
				$(this).find("input[type='checkbox']").trigger('click');
				//alert($(this).attr("id"));
				switchStylestyle($(this).attr("id"));
				$(this).find("input[type='checkbox']").prop("disabled",true);
				$('#stylesheets').find("input[type='checkbox']").not($(this).find("input[type='checkbox']")).each(function(){
					$(this).prop("disabled",false);
					if($(this).prop("checked") == true) {
						$(this).prop("checked", false);
					}
				});
			}
				//alert($(this).find("input[type='checkbox']").prop("checked"));
			});
		});
		/*
		$('#blue').click(function(){
			$('#checkbox-id2').trigger('click');
		});
		
		$('#green').click(function(){
			$('#checkbox-id3').trigger('click');
		});*/
		
		/**/
		
		$('#clear').bind('click',function(){
			$('#oldpass').val('');
			$('#newpass').val('');
			$('#newpass2').val('');
		});
		
		$(function(){
			$( '#tabs li' ).click( function(){
				rel = $( this ).attr( "rel" );
				if(!rel){
					$('#submitform').show();
				} else {
					$('#submitform').hide();
				}
			});
		});
	</script>
{/literal}
{literal}
	<script>
	
	
	//$('#wrapper').find('#date_priem').text('1111');
	
	
			var hash = document.location.hash;
			if(hash == '#successSend'){
				$("#modal_success_btn").click();
				document.location.hash = "";
			}
	
		$('.passes').bind("change keyup input click", function() {
			if (this.value.match(/[^0-9a-zA-Z]/g)) {
				this.value = this.value.replace(/[^0-9a-zA-Z]/g, '');
			}
		});
		
		$('#newpass').focusout(function(){
			$('#galki li').removeClass('galochka');
			
			var test = $('#newpass').val();
			var errors = 0;
			if (--test.split(/[a-z]/).length>1)
			{
				$('#galki li:first-child').addClass('galochka');
			}
			else{
			  var errors = 1;
			}
				
			if (--test.split(/[0-9]/).length>1)
				{
					$('#galki li:nth-child(2)').addClass('galochka');
				}
			else
				{
				 var errors = 1;
				}
			
			if (--test.split(/[a-zA-z0-9]/).length>9 && --test.split(/[a-zA-z0-9]/).length<17)
				{
					$('#galki li:nth-child(3)').addClass('galochka');
				}
			else
				{
					 var errors = 1;
				}
			if (--test.split(/[a-zA-z0-9]/).length<17 && --test.split(/[a-zA-z0-9]/).length>1)
				{
					$('#galki li:last-child').addClass('galochka');
				}
			else	
				{
					var errors = 1;
				}
				var test2 = $('#newpass2').val();
			if(errors ==true)
			{
				$('#save2').prop('disabled',true);
			}
		});
		$('#newpass2').focusout(function()
		{
			var test = $('#newpass').val();
			var test2 = $('#newpass2').val();
			if(test == test2)
				{
					$('#save2').prop('disabled',false);
					$('#galkun').addClass('galkun');
				}
			else
				{
					$('#save2').prop('disabled',true);
				}
		});
		
		
		var nav = $('#nav');
		var selection = $('.selectsubs');
		var select = selection.find('li');
		nav.click(function(event) {
			if (nav.hasClass('active')) {
				nav.removeClass('active');
				selection.stop().slideUp(200);
			} else {
				nav.addClass('active');
				selection.stop().slideDown(200);
			}
			event.preventDefault();
		});
		select.click(function(event) {
			select.removeClass('active');
			nav.trigger('click');
			$('#cm_subscribe').attr('value', $(this).attr('data-value'));
			$('#nav').text($(this).text());
		});
	</script>
{/literal}